                                        


/****** Object:  Table [dbo].[vendortype]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vendortype](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,                     
	[FullName] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[ParentRef_ListID] [varchar](255) NULL,
	[ParentRef_FullName] [varchar](255) NULL,
	[Sublevel] [int] NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vendorcredit]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vendorcredit](
	[TxnID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[TxnNumber] [int] NULL,
	[VendorRef_ListID] [varchar](255) NULL,
	[VendorRef_FullName] [varchar](255) NULL,
	[APAccountRef_ListID] [varchar](255) NULL,
	[APAccountRef_FullName] [varchar](255) NULL,
	[TxnDate] [datetime] NULL,
	[CreditAmount] [float] NULL,
	[CurrencyRef_ListID] [varchar](255) NULL,
	[CurrencyRef_FullName] [varchar](255) NULL,
	[ExchangeRate] [float] NULL,
	[CreditAmountInHomeCurrency] [float] NULL,
	[RefNumber] [varchar](255) NULL,
	[Memo] [varchar](max) NULL,
	[OpenAmount] [float] NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vendor]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vendor](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[CompanyName] [varchar](255) NULL,
	[Salutation] [varchar](255) NULL,
	[FirstName] [varchar](255) NULL,
	[MiddleName] [varchar](255) NULL,
	[LastName] [varchar](255) NULL,
	[Suffix] [varchar](255) NULL,
	[VendorAddress_Addr1] [varchar](255) NULL,
	[VendorAddress_Addr2] [varchar](255) NULL,
	[VendorAddress_Addr3] [varchar](255) NULL,
	[VendorAddress_Addr4] [varchar](255) NULL,
	[VendorAddress_Addr5] [varchar](255) NULL,
	[VendorAddress_City] [varchar](255) NULL,
	[VendorAddress_State] [varchar](255) NULL,
	[VendorAddress_PostalCode] [varchar](255) NULL,
	[VendorAddress_Country] [varchar](255) NULL,
	[VendorAddress_Note] [varchar](255) NULL,
	[Phone] [varchar](255) NULL,
	[Mobile] [varchar](255) NULL,
	[Pager] [varchar](255) NULL,
	[AltPhone] [varchar](255) NULL,
	[Fax] [varchar](255) NULL,
	[Email] [varchar](255) NULL,
	[Contact] [varchar](255) NULL,
	[AltContact] [varchar](255) NULL,
	[NameOnCheck] [varchar](255) NULL,
	[Notes] [varchar](2000) NULL,
	[AccountNumber] [varchar](255) NULL,
	[VendorTypeRef_ListID] [varchar](255) NULL,
	[VendorTypeRef_FullName] [varchar](255) NULL,
	[TermsRef_ListID] [varchar](255) NULL,
	[TermsRef_FullName] [varchar](255) NULL,
	[CreditLimit] [float] NULL,
	[VendorTaxIdent] [varchar](255) NULL,
	[IsVendorEligibleFor1099] [bit] NULL,
	[Balance] [float] NULL,
	[IsSalesTaxAgency] [bit] NULL,
	[CurrencyRef_ListID] [varchar](255) NULL,
	[CurrencyRef_FullName] [varchar](255) NULL,
	[BillingRateRef_ListID] [varchar](255) NULL,
	[BillingRateRef_FullName] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vehiclemileage]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vehiclemileage](
	[TxnID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[VehicleRef_ListID] [varchar](255) NULL,
	[VehicleRef_FullName] [varchar](255) NULL,
	[CustomerRef_ListID] [varchar](255) NULL,
	[CustomerRef_FullName] [varchar](255) NULL,
	[ItemRef_ListID] [varchar](255) NULL,
	[ItemRef_FullName] [varchar](255) NULL,
	[ClassRef_ListID] [varchar](255) NULL,
	[ClassRef_FullName] [varchar](255) NULL,
	[TripStartDate] [datetime] NULL,
	[TripEndDate] [datetime] NULL,
	[OdometerStart] [varchar](255) NULL,
	[OdometerEnd] [varchar](255) NULL,
	[TotalMiles] [varchar](255) NULL,
	[Notes] [varchar](2000) NULL,
	[BillableStatus] [varchar](255) NULL,
	[StandardMileageRate] [varchar](255) NULL,
	[StandardMileageTotalAmount] [float] NULL,
	[BillableRate] [varchar](255) NULL,
	[BillableAmount] [float] NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vehicle]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vehicle](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[Desc] [varchar](255) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vacationhoursdetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vacationhoursdetail](
	[HoursAvailable] [varchar](255) NULL,
	[AccrualPeriod] [varchar](255) NULL,
	[HoursAccrued] [varchar](255) NULL,
	[MaximumHours] [varchar](255) NULL,
	[IsResettingHoursEachNewYear] [bit] NULL,
	[HoursUsed] [varchar](255) NULL,
	[AccrualStartDate] [datetime] NULL,
	[IDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[userconfig]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[userconfig](
	[Name] [varchar](200) NULL,
	[Password] [varchar](200) NULL,
	[Email] [varchar](200) NULL,
	[Type] [varchar](100) NULL,
	[LastLogin] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[unitofmeasureset]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[unitofmeasureset](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[UnitOfMeasureType] [varchar](255) NULL,
	[BaseUnitName] [varchar](255) NULL,
	[BaseUnitAbbreviation] [varchar](255) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[txnpaymentlinedetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[txnpaymentlinedetail](
	[TxnID] [varchar](255) NULL,
	[TxnType] [varchar](255) NULL,
	[TxnDate] [datetime] NULL,
	[RefNumber] [varchar](255) NULL,
	[TotalAmount] [float] NULL,
	[AppliedAmount] [float] NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[txnitemlinedetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[txnitemlinedetail](
	[TxnLineID] [varchar](255) NULL,
	[ItemRef_ListID] [varchar](255) NULL,
	[ItemRef_FullName] [varchar](255) NULL,
	[InventorySiteRef_ListID] [varchar](255) NULL,
	[InventorySiteRef_FullName] [varchar](255) NULL,
	[Desc] [varchar](max) NULL,
	[Quantity] [varchar](255) NULL,
	[UnitOfMeasure] [varchar](255) NULL,
	[OverrideUOMSetRef_ListID] [varchar](255) NULL,
	[OverrideUOMSetRef_FullName] [varchar](255) NULL,
	[Cost] [varchar](255) NULL,
	[Amount] [float] NULL,
	[CustomerRef_ListID] [varchar](255) NULL,
	[CustomerRef_FullName] [varchar](255) NULL,
	[ClassRef_ListID] [varchar](255) NULL,
	[ClassRef_FullName] [varchar](255) NULL,
	[SalesTaxCodeRef_ListID] [varchar](255) NULL,
	[SalesTaxCodeRef_FullName] [varchar](255) NULL,
	[BillableStatus] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[txnitemgrouplinedetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[txnitemgrouplinedetail](
	[TxnLineID] [varchar](255) NULL,
	[ItemGroupRef_ListID] [varchar](255) NULL,
	[ItemGroupRef_FullName] [varchar](255) NULL,
	[Desc] [varchar](255) NULL,
	[Quantity] [varchar](255) NULL,
	[TotalAmount] [float] NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[txnexpenselinedetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[txnexpenselinedetail](
	[TxnLineID] [varchar](255) NULL,
	[AccountRef_ListID] [varchar](255) NULL,
	[AccountRef_FullName] [varchar](255) NULL,
	[Amount] [float] NULL,
	[Memo] [varchar](max) NULL,
	[CustomerRef_ListID] [varchar](255) NULL,
	[CustomerRef_FullName] [varchar](255) NULL,
	[ClassRef_ListID] [varchar](255) NULL,
	[ClassRef_FullName] [varchar](255) NULL,
	[SalesTaxCodeRef_ListID] [varchar](255) NULL,
	[SalesTaxCodeRef_FullName] [varchar](255) NULL,
	[BillableStatus] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[transferReport]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[transferReport](
	[TxnNumber] [int] NULL,
	[TxnType] [varchar](255) NULL,
	[Date] [datetime] NULL,
	[RefNumber] [varchar](255) NULL,
	[Customer_ListID] [varchar](255) NULL,
	[Customer_Name] [varchar](255) NULL,
	[SourceName] [varchar](255) NULL,
	[Account_ListID] [varchar](255) NULL,
	[Account] [varchar](255) NULL,
	[Class_ListID] [varchar](255) NULL,
	[Class] [varchar](255) NULL,
	[ClearedStatus] [varchar](255) NULL,
	[SplitAccount_ListID] [varchar](250) NULL,
	[SplitAccount] [varchar](255) NULL,
	[Quantity] [float] NULL,
	[Debit] [float] NULL,
	[Credit] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[transferinventorylinedetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[transferinventorylinedetail](
	[TxnLineID] [varchar](255) NULL,
	[ItemRef_ListID] [varchar](255) NULL,
	[ItemRef_FullName] [varchar](255) NULL,
	[QuantityTransferred] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[transferinventory]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[transferinventory](
	[TxnID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[TxnNumber] [int] NULL,
	[TxnDate] [datetime] NULL,
	[RefNumber] [varchar](255) NULL,
	[FromInventorySiteRef_ListID] [varchar](255) NULL,
	[FromInventorySiteRef_FullName] [varchar](255) NULL,
	[ToInventorySiteRef_ListID] [varchar](255) NULL,
	[ToInventorySiteRef_FullName] [varchar](255) NULL,
	[Memo] [varchar](255) NULL,
	[ExternalGUID] [varchar](255) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[transfer]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[transfer](
	[TxnID] [nvarchar](max) NULL,
	[TimeCreated] [nvarchar](max) NULL,
	[TimeModified] [nvarchar](max) NULL,
	[EditSequence] [nvarchar](max) NULL,
	[TxnNumber] [int] NULL,
	[TxnDate] [datetime] NULL,
	[Amount] [float] NULL,
	[ClassRef_ListID] [nvarchar](max) NULL,
	[ClassRef_FullName] [nvarchar](max) NULL,
	[FromAccountBalance] [float] NULL,
	[ToAccountBalance] [float] NULL,
	[Memo] [nvarchar](max) NULL,
	[TransferFromAccountRef_ListID] [nvarchar](max) NULL,
	[TransferFromAccountRef_FullName] [nvarchar](max) NULL,
	[TransferToAccountRef_ListID] [nvarchar](max) NULL,
	[TransferToAccountRef_FullName] [nvarchar](max) NULL,
	[CustomField1] [nvarchar](max) NULL,
	[CustomField2] [nvarchar](max) NULL,
	[CustomField3] [nvarchar](max) NULL,
	[CustomField4] [nvarchar](max) NULL,
	[CustomField5] [nvarchar](max) NULL,
	[CustomField6] [nvarchar](max) NULL,
	[CustomField7] [nvarchar](max) NULL,
	[CustomField8] [nvarchar](max) NULL,
	[CustomField9] [nvarchar](max) NULL,
	[CustomField10] [nvarchar](max) NULL,
	[Status] [nvarchar](max) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[transaction]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[transaction](
	[TxnID] [varchar](255) NULL,
	[TxnType] [varchar](255) NULL,
	[TxnLineID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EntityRef_ListID] [varchar](255) NULL,
	[EntityRef_FullName] [varchar](255) NULL,
	[AccountRef_ListID] [varchar](255) NULL,
	[AccountRef_FullName] [varchar](255) NULL,
	[TxnDate] [datetime] NULL,
	[RefNumber] [varchar](255) NULL,
	[Amount] [float] NULL,
	[CurrencyRef_ListID] [varchar](255) NULL,
	[CurrencyRef_FullName] [varchar](255) NULL,
	[ExchangeRate] [float] NULL,
	[AmountInHomeCurrency] [float] NULL,
	[Memo] [varchar](max) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[todo]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[todo](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Notes] [varchar](2000) NULL,
	[IsActive] [bit] NULL,
	[IsDone] [bit] NULL,
	[ReminderDate] [datetime] NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[timetracking]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[timetracking](
	[TxnID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[TxnNumber] [int] NULL,
	[TxnDate] [datetime] NULL,
	[EntityRef_ListID] [varchar](255) NULL,
	[EntityRef_FullName] [varchar](255) NULL,
	[CustomerRef_ListID] [varchar](255) NULL,
	[CustomerRef_FullName] [varchar](255) NULL,
	[ItemServiceRef_ListID] [varchar](255) NULL,
	[ItemServiceRef_FullName] [varchar](255) NULL,
	[Rate] [varchar](255) NULL,
	[Duration] [varchar](255) NULL,
	[ClassRef_ListID] [varchar](255) NULL,
	[ClassRef_FullName] [varchar](255) NULL,
	[PayrollItemWageRef_ListID] [varchar](255) NULL,
	[PayrollItemWageRef_FullName] [varchar](255) NULL,
	[Notes] [varchar](max) NULL,
	[IsBillable] [bit] NULL,
	[IsBilled] [bit] NULL,
	[BillableStatus] [varchar](255) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Time]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Time](
	[PK_Date] [datetime] NOT NULL,
	[Date_Name] [nvarchar](50) NULL,
	[Year] [datetime] NULL,
	[Year_Name] [nvarchar](50) NULL,
	[Quarter] [datetime] NULL,
	[Quarter_Name] [nvarchar](50) NULL,
	[Month] [datetime] NULL,
	[Month_Name] [nvarchar](50) NULL,
	[Week] [datetime] NULL,
	[Week_Name] [nvarchar](50) NULL,
	[Day_Of_Year] [int] NULL,
	[Day_Of_Year_Name] [nvarchar](50) NULL,
	[Day_Of_Quarter] [int] NULL,
	[Day_Of_Quarter_Name] [nvarchar](50) NULL,
	[Day_Of_Month] [int] NULL,
	[Day_Of_Month_Name] [nvarchar](50) NULL,
	[Day_Of_Week] [int] NULL,
	[Day_Of_Week_Name] [nvarchar](50) NULL,
	[Week_Of_Year] [int] NULL,
	[Week_Of_Year_Name] [nvarchar](50) NULL,
	[Month_Of_Year] [int] NULL,
	[Month_Of_Year_Name] [nvarchar](50) NULL,
	[Month_Of_Quarter] [int] NULL,
	[Month_Of_Quarter_Name] [nvarchar](50) NULL,
	[Quarter_Of_Year] [int] NULL,
	[Quarter_Of_Year_Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Time] PRIMARY KEY CLUSTERED 
(
	[PK_Date] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'AllowGen', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'PK_Date'
GO
EXEC sys.sp_addextendedproperty @name=N'DSVColumn', @value=N'Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'PK_Date'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowGen', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Date_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'DSVColumn', @value=N'Date_Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Date_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowGen', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Year'
GO
EXEC sys.sp_addextendedproperty @name=N'DSVColumn', @value=N'Year' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Year'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowGen', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Year_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'DSVColumn', @value=N'Year_Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Year_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowGen', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Quarter'
GO
EXEC sys.sp_addextendedproperty @name=N'DSVColumn', @value=N'Quarter' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Quarter'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowGen', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Quarter_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'DSVColumn', @value=N'Quarter_Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Quarter_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowGen', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Month'
GO
EXEC sys.sp_addextendedproperty @name=N'DSVColumn', @value=N'Month' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Month'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowGen', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Month_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'DSVColumn', @value=N'Month_Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Month_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowGen', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Week'
GO
EXEC sys.sp_addextendedproperty @name=N'DSVColumn', @value=N'Week' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Week'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowGen', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Week_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'DSVColumn', @value=N'Week_Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Week_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowGen', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Day_Of_Year'
GO
EXEC sys.sp_addextendedproperty @name=N'DSVColumn', @value=N'Day_Of_Year' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Day_Of_Year'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowGen', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Day_Of_Year_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'DSVColumn', @value=N'Day_Of_Year_Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Day_Of_Year_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowGen', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Day_Of_Quarter'
GO
EXEC sys.sp_addextendedproperty @name=N'DSVColumn', @value=N'Day_Of_Quarter' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Day_Of_Quarter'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowGen', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Day_Of_Quarter_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'DSVColumn', @value=N'Day_Of_Quarter_Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Day_Of_Quarter_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowGen', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Day_Of_Month'
GO
EXEC sys.sp_addextendedproperty @name=N'DSVColumn', @value=N'Day_Of_Month' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Day_Of_Month'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowGen', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Day_Of_Month_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'DSVColumn', @value=N'Day_Of_Month_Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Day_Of_Month_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowGen', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Day_Of_Week'
GO
EXEC sys.sp_addextendedproperty @name=N'DSVColumn', @value=N'Day_Of_Week' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Day_Of_Week'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowGen', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Day_Of_Week_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'DSVColumn', @value=N'Day_Of_Week_Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Day_Of_Week_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowGen', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Week_Of_Year'
GO
EXEC sys.sp_addextendedproperty @name=N'DSVColumn', @value=N'Week_Of_Year' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Week_Of_Year'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowGen', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Week_Of_Year_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'DSVColumn', @value=N'Week_Of_Year_Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Week_Of_Year_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowGen', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Month_Of_Year'
GO
EXEC sys.sp_addextendedproperty @name=N'DSVColumn', @value=N'Month_Of_Year' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Month_Of_Year'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowGen', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Month_Of_Year_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'DSVColumn', @value=N'Month_Of_Year_Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Month_Of_Year_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowGen', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Month_Of_Quarter'
GO
EXEC sys.sp_addextendedproperty @name=N'DSVColumn', @value=N'Month_Of_Quarter' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Month_Of_Quarter'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowGen', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Month_Of_Quarter_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'DSVColumn', @value=N'Month_Of_Quarter_Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Month_Of_Quarter_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowGen', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Quarter_Of_Year'
GO
EXEC sys.sp_addextendedproperty @name=N'DSVColumn', @value=N'Quarter_Of_Year' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Quarter_Of_Year'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowGen', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Quarter_Of_Year_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'DSVColumn', @value=N'Quarter_Of_Year_Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'COLUMN',@level2name=N'Quarter_Of_Year_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowGen', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time'
GO
EXEC sys.sp_addextendedproperty @name=N'DSVTable', @value=N'Time' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time'
GO
EXEC sys.sp_addextendedproperty @name=N'Project', @value=N'275b6ca9-9e45-4b5a-9ad0-29f0a56bb80d' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time'
GO
EXEC sys.sp_addextendedproperty @name=N'AllowGen', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Time', @level2type=N'CONSTRAINT',@level2name=N'PK_Time'
GO
/****** Object:  Table [dbo].[terms]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[terms](
	[ListID] [varchar](200) NULL,
	[FullName] [varchar](200) NULL,
	[TableName] [varchar](200) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[template]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[template](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[TemplateType] [varchar](255) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[standardterms]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[standardterms](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[StdDueDays] [int] NULL,
	[StdDiscountDays] [int] NULL,
	[DiscountPct] [varchar](255) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sickhoursdetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sickhoursdetail](
	[HoursAvailable] [varchar](255) NULL,
	[AccrualPeriod] [varchar](255) NULL,
	[HoursAccrued] [varchar](255) NULL,
	[MaximumHours] [varchar](255) NULL,
	[IsResettingHoursEachNewYear] [bit] NULL,
	[HoursUsed] [varchar](255) NULL,
	[AccrualStartDate] [datetime] NULL,
	[IDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[shipmethod]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[shipmethod](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[salestaxpaymentchecklinedetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[salestaxpaymentchecklinedetail](
	[TxnLineID] [varchar](255) NULL,
	[ItemSalesTaxRef_ListID] [varchar](255) NULL,
	[ItemSalesTaxRef_FullName] [varchar](255) NULL,
	[Amount] [float] NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[salestaxpaymentcheck]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[salestaxpaymentcheck](
	[TxnID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[TxnNumber] [int] NULL,
	[PayeeEntityRef_ListID] [varchar](255) NULL,
	[PayeeEntityRef_FullName] [varchar](255) NULL,
	[TxnDate] [datetime] NULL,
	[BankAccountRef_ListID] [varchar](255) NULL,
	[BankAccountRef_FullName] [varchar](255) NULL,
	[Amount] [float] NULL,
	[RefNumber] [varchar](255) NULL,
	[Memo] [varchar](max) NULL,
	[Address_Addr1] [varchar](255) NULL,
	[Address_Addr2] [varchar](255) NULL,
	[Address_Addr3] [varchar](255) NULL,
	[Address_Addr4] [varchar](255) NULL,
	[Address_Addr5] [varchar](255) NULL,
	[Address_City] [varchar](255) NULL,
	[Address_State] [varchar](255) NULL,
	[Address_PostalCode] [varchar](255) NULL,
	[Address_Country] [varchar](255) NULL,
	[Address_Note] [varchar](255) NULL,
	[IsToBePrinted] [bit] NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[salestaxcode]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[salestaxcode](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[IsTaxable] [bit] NULL,
	[Desc] [varchar](max) NULL,
	[ItemPurchaseTaxRef_ListID] [varchar](255) NULL,
	[ItemPurchaseTaxRef_FullName] [varchar](255) NULL,
	[ItemSalesTaxRef_ListID] [varchar](255) NULL,
	[ItemSalesTaxRef_FullName] [varchar](255) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[salesrep]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[salesrep](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Initial] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[SalesRepEntityRef_ListID] [varchar](255) NULL,
	[SalesRepEntityRef_FullName] [varchar](255) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[salesreceiptlinegroupdetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[salesreceiptlinegroupdetail](
	[TxnLineID] [varchar](255) NULL,
	[ItemGroupRef_ListID] [varchar](255) NULL,
	[ItemGroupRef_FullName] [varchar](255) NULL,
	[Desc] [varchar](255) NULL,
	[Quantity] [varchar](255) NULL,
	[IsPrintItemsInGroup] [bit] NULL,
	[TotalAmount] [float] NULL,
	[ServiceDate] [datetime] NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[salesreceiptlinedetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[salesreceiptlinedetail](
	[TxnLineID] [varchar](255) NULL,
	[ItemRef_ListID] [varchar](255) NULL,
	[ItemRef_FullName] [varchar](255) NULL,
	[Desc] [varchar](max) NULL,
	[Quantity] [varchar](255) NULL,
	[UnitOfMeasure] [varchar](255) NULL,
	[OverrideUOMSetRef_ListID] [varchar](255) NULL,
	[OverrideUOMSetRef_FullName] [varchar](255) NULL,
	[Rate] [varchar](255) NULL,
	[RatePercent] [varchar](255) NULL,
	[ClassRef_ListID] [varchar](255) NULL,
	[ClassRef_FullName] [varchar](255) NULL,
	[Amount] [float] NULL,
	[InventorySiteRef_ListID] [varchar](255) NULL,
	[InventorySiteRef_FullName] [varchar](255) NULL,
	[ServiceDate] [datetime] NULL,
	[SalesTaxCodeRef_ListID] [varchar](255) NULL,
	[SalesTaxCodeRef_FullName] [varchar](255) NULL,
	[Other1] [varchar](255) NULL,
	[Other2] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[salesreceipt]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[salesreceipt](
	[TxnID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[TxnNumber] [int] NULL,
	[CustomerRef_ListID] [varchar](255) NULL,
	[CustomerRef_FullName] [varchar](255) NULL,
	[ClassRef_ListID] [varchar](255) NULL,
	[ClassRef_FullName] [varchar](255) NULL,
	[TemplateRef_ListID] [varchar](255) NULL,
	[TemplateRef_FullName] [varchar](255) NULL,
	[TxnDate] [datetime] NULL,
	[RefNumber] [varchar](255) NULL,
	[BillAddress_Addr1] [varchar](255) NULL,
	[BillAddress_Addr2] [varchar](255) NULL,
	[BillAddress_Addr3] [varchar](255) NULL,
	[BillAddress_Addr4] [varchar](255) NULL,
	[BillAddress_Addr5] [varchar](255) NULL,
	[BillAddress_City] [varchar](255) NULL,
	[BillAddress_State] [varchar](255) NULL,
	[BillAddress_PostalCode] [varchar](255) NULL,
	[BillAddress_Country] [varchar](255) NULL,
	[BillAddress_Note] [varchar](255) NULL,
	[ShipAddress_Addr1] [varchar](255) NULL,
	[ShipAddress_Addr2] [varchar](255) NULL,
	[ShipAddress_Addr3] [varchar](255) NULL,
	[ShipAddress_Addr4] [varchar](255) NULL,
	[ShipAddress_Addr5] [varchar](255) NULL,
	[ShipAddress_City] [varchar](255) NULL,
	[ShipAddress_State] [varchar](255) NULL,
	[ShipAddress_PostalCode] [varchar](255) NULL,
	[ShipAddress_Country] [varchar](255) NULL,
	[ShipAddress_Note] [varchar](255) NULL,
	[IsPending] [bit] NULL,
	[CheckNumber] [varchar](255) NULL,
	[PaymentMethodRef_ListID] [varchar](255) NULL,
	[PaymentMethodRef_FullName] [varchar](255) NULL,
	[DueDate] [datetime] NULL,
	[SalesRepRef_ListID] [varchar](255) NULL,
	[SalesRepRef_FullName] [varchar](255) NULL,
	[ShipDate] [datetime] NULL,
	[ShipMethodRef_ListID] [varchar](255) NULL,
	[ShipMethodRef_FullName] [varchar](255) NULL,
	[FOB] [varchar](255) NULL,
	[Subtotal] [float] NULL,
	[ItemSalesTaxRef_ListID] [varchar](255) NULL,
	[ItemSalesTaxRef_FullName] [varchar](255) NULL,
	[SalesTaxPercentage] [varchar](255) NULL,
	[SalesTaxTotal] [float] NULL,
	[TotalAmount] [float] NULL,
	[CurrencyRef_ListID] [varchar](255) NULL,
	[CurrencyRef_FullName] [varchar](255) NULL,
	[ExchangeRate] [float] NULL,
	[TotalAmountInHomeCurrency] [float] NULL,
	[Memo] [varchar](max) NULL,
	[CustomerMsgRef_ListID] [varchar](255) NULL,
	[CustomerMsgRef_FullName] [varchar](255) NULL,
	[IsToBePrinted] [bit] NULL,
	[IsToBeEmailed] [bit] NULL,
	[IsTaxIncluded] [bit] NULL,
	[CustomerSalesTaxCodeRef_ListID] [varchar](255) NULL,
	[CustomerSalesTaxCodeRef_FullName] [varchar](255) NULL,
	[DepositToAccountRef_ListID] [varchar](255) NULL,
	[DepositToAccountRef_FullName] [varchar](255) NULL,
	[Other] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[salesorpurchasedetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[salesorpurchasedetail](
	[Desc] [varchar](max) NULL,
	[Price] [varchar](255) NULL,
	[PricePercent] [varchar](255) NULL,
	[AccountRef_ListID] [varchar](255) NULL,
	[AccountRef_FullName] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[salesorderlinegroupdetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[salesorderlinegroupdetail](
	[TxnLineID] [varchar](255) NULL,
	[ItemGroupRef_ListID] [varchar](255) NULL,
	[ItemGroupRef_FullName] [varchar](255) NULL,
	[Desc] [varchar](255) NULL,
	[Quantity] [varchar](255) NULL,
	[IsPrintItemsInGroup] [bit] NULL,
	[TotalAmount] [float] NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[salesorderlinedetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[salesorderlinedetail](
	[TxnLineID] [varchar](255) NULL,
	[ItemRef_ListID] [varchar](255) NULL,
	[ItemRef_FullName] [varchar](255) NULL,
	[Desc] [varchar](max) NULL,
	[Quantity] [varchar](255) NULL,
	[UnitOfMeasure] [varchar](255) NULL,
	[OverrideUOMSetRef_ListID] [varchar](255) NULL,
	[OverrideUOMSetRef_FullName] [varchar](255) NULL,
	[Rate] [varchar](255) NULL,
	[RatePercent] [varchar](255) NULL,
	[ClassRef_ListID] [varchar](255) NULL,
	[ClassRef_FullName] [varchar](255) NULL,
	[Amount] [float] NULL,
	[InventorySiteRef_ListID] [varchar](255) NULL,
	[InventorySiteRef_FullName] [varchar](255) NULL,
	[SalesTaxCodeRef_ListID] [varchar](255) NULL,
	[SalesTaxCodeRef_FullName] [varchar](255) NULL,
	[Invoiced] [varchar](255) NULL,
	[IsManuallyClosed] [bit] NULL,
	[Other1] [varchar](255) NULL,
	[Other2] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[salesorder]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[salesorder](
	[TxnID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[TxnNumber] [int] NULL,
	[CustomerRef_ListID] [varchar](255) NULL,
	[CustomerRef_FullName] [varchar](255) NULL,
	[ClassRef_ListID] [varchar](255) NULL,
	[ClassRef_FullName] [varchar](255) NULL,
	[TemplateRef_ListID] [varchar](255) NULL,
	[TemplateRef_FullName] [varchar](255) NULL,
	[TxnDate] [datetime] NULL,
	[RefNumber] [varchar](255) NULL,
	[BillAddress_Addr1] [varchar](255) NULL,
	[BillAddress_Addr2] [varchar](255) NULL,
	[BillAddress_Addr3] [varchar](255) NULL,
	[BillAddress_Addr4] [varchar](255) NULL,
	[BillAddress_Addr5] [varchar](255) NULL,
	[BillAddress_City] [varchar](255) NULL,
	[BillAddress_State] [varchar](255) NULL,
	[BillAddress_PostalCode] [varchar](255) NULL,
	[BillAddress_Country] [varchar](255) NULL,
	[BillAddress_Note] [varchar](255) NULL,
	[ShipAddress_Addr1] [varchar](255) NULL,
	[ShipAddress_Addr2] [varchar](255) NULL,
	[ShipAddress_Addr3] [varchar](255) NULL,
	[ShipAddress_Addr4] [varchar](255) NULL,
	[ShipAddress_Addr5] [varchar](255) NULL,
	[ShipAddress_City] [varchar](255) NULL,
	[ShipAddress_State] [varchar](255) NULL,
	[ShipAddress_PostalCode] [varchar](255) NULL,
	[ShipAddress_Country] [varchar](255) NULL,
	[ShipAddress_Note] [varchar](255) NULL,
	[PONumber] [varchar](255) NULL,
	[TermsRef_ListID] [varchar](255) NULL,
	[TermsRef_FullName] [varchar](255) NULL,
	[DueDate] [datetime] NULL,
	[SalesRepRef_ListID] [varchar](255) NULL,
	[SalesRepRef_FullName] [varchar](255) NULL,
	[FOB] [varchar](255) NULL,
	[ShipDate] [datetime] NULL,
	[ShipMethodRef_ListID] [varchar](255) NULL,
	[ShipMethodRef_FullName] [varchar](255) NULL,
	[Subtotal] [float] NULL,
	[ItemSalesTaxRef_ListID] [varchar](255) NULL,
	[ItemSalesTaxRef_FullName] [varchar](255) NULL,
	[SalesTaxPercentage] [varchar](255) NULL,
	[SalesTaxTotal] [float] NULL,
	[TotalAmount] [float] NULL,
	[CurrencyRef_ListID] [varchar](255) NULL,
	[CurrencyRef_FullName] [varchar](255) NULL,
	[ExchangeRate] [float] NULL,
	[TotalAmountInHomeCurrency] [float] NULL,
	[IsManuallyClosed] [bit] NULL,
	[IsFullyInvoiced] [bit] NULL,
	[Memo] [varchar](max) NULL,
	[CustomerMsgRef_ListID] [varchar](255) NULL,
	[CustomerMsgRef_FullName] [varchar](255) NULL,
	[IsToBePrinted] [bit] NULL,
	[IsToBeEmailed] [bit] NULL,
	[IsTaxIncluded] [bit] NULL,
	[CustomerSalesTaxCodeRef_ListID] [varchar](255) NULL,
	[CustomerSalesTaxCodeRef_FullName] [varchar](255) NULL,
	[Other] [varchar](255) NULL,
	[LinkedTxn] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[salesandpurchasedetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[salesandpurchasedetail](
	[SalesDesc] [varchar](255) NULL,
	[SalesPrice] [varchar](255) NULL,
	[IncomeAccountRef_ListID] [varchar](255) NULL,
	[IncomeAccountRef_FullName] [varchar](255) NULL,
	[PurchaseDesc] [varchar](255) NULL,
	[PurchaseCost] [varchar](255) NULL,
	[PurchaseTaxCodeRef_ListID] [varchar](255) NULL,
	[PurchaseTaxCodeRef_FullName] [varchar](255) NULL,
	[ExpenseAccountRef_ListID] [varchar](255) NULL,
	[ExpenseAccountRef_FullName] [varchar](255) NULL,
	[PrefVendorRef_ListID] [varchar](255) NULL,
	[PrefVendorRef_FullName] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[relatedunitdetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[relatedunitdetail](
	[Name] [varchar](255) NULL,
	[Abbreviation] [varchar](255) NULL,
	[ConversionRatio] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[refundappliedtotxndetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[refundappliedtotxndetail](
	[TxnID] [varchar](255) NULL,
	[TxnType] [varchar](255) NULL,
	[TxnDate] [datetime] NULL,
	[RefNumber] [varchar](255) NULL,
	[CreditRemaining] [float] NULL,
	[RefundAmount] [float] NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[receivepaymenttodeposit]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[receivepaymenttodeposit](
	[TxnID] [varchar](255) NULL,
	[TxnLineID] [varchar](255) NULL,
	[TxnType] [varchar](255) NULL,
	[CustomerRef_ListID] [varchar](255) NULL,
	[CustomerRef_FullName] [varchar](255) NULL,
	[TxnDate] [datetime] NULL,
	[RefNumber] [varchar](255) NULL,
	[Amount] [float] NULL,
	[CurrencyRef_ListID] [varchar](255) NULL,
	[CurrencyRef_FullName] [varchar](255) NULL,
	[ExchangeRate] [float] NULL,
	[AmountInHomeCurrency] [float] NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[receivepayment]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[receivepayment](
	[TxnID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[TxnNumber] [int] NULL,
	[CustomerRef_ListID] [varchar](255) NULL,
	[CustomerRef_FullName] [varchar](255) NULL,
	[ARAccountRef_ListID] [varchar](255) NULL,
	[ARAccountRef_FullName] [varchar](255) NULL,
	[TxnDate] [datetime] NULL,
	[RefNumber] [varchar](255) NULL,
	[TotalAmount] [float] NULL,
	[CurrencyRef_ListID] [varchar](255) NULL,
	[CurrencyRef_FullName] [varchar](255) NULL,
	[ExchangeRate] [float] NULL,
	[TotalAmountInHomeCurrency] [float] NULL,
	[PaymentMethodRef_ListID] [varchar](255) NULL,
	[PaymentMethodRef_FullName] [varchar](255) NULL,
	[Memo] [varchar](max) NULL,
	[DepositToAccountRef_ListID] [varchar](255) NULL,
	[DepositToAccountRef_FullName] [varchar](255) NULL,
	[UnusedPayment] [float] NULL,
	[UnusedCredits] [float] NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[purchaseorderlinegroupdetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[purchaseorderlinegroupdetail](
	[TxnLineID] [varchar](255) NULL,
	[ItemGroupRef_ListID] [varchar](255) NULL,
	[ItemGroupRef_FullName] [varchar](255) NULL,
	[Desc] [varchar](255) NULL,
	[Quantity] [varchar](255) NULL,
	[IsPrintItemsInGroup] [bit] NULL,
	[TotalAmount] [float] NULL,
	[ServiceDate] [datetime] NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[purchaseorderlinedetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[purchaseorderlinedetail](
	[TxnLineID] [varchar](255) NULL,
	[ItemRef_ListID] [varchar](255) NULL,
	[ItemRef_FullName] [varchar](255) NULL,
	[ManufacturerPartNumber] [varchar](255) NULL,
	[Desc] [varchar](max) NULL,
	[Quantity] [varchar](255) NULL,
	[UnitOfMeasure] [varchar](255) NULL,
	[OverrideUOMSetRef_ListID] [varchar](255) NULL,
	[OverrideUOMSetRef_FullName] [varchar](255) NULL,
	[Rate] [varchar](255) NULL,
	[ClassRef_ListID] [varchar](255) NULL,
	[ClassRef_FullName] [varchar](255) NULL,
	[Amount] [float] NULL,
	[CustomerRef_ListID] [varchar](255) NULL,
	[CustomerRef_FullName] [varchar](255) NULL,
	[ServiceDate] [datetime] NULL,
	[SalesTaxCodeRef_ListID] [varchar](255) NULL,
	[SalesTaxCodeRef_FullName] [varchar](255) NULL,
	[ReceivedQuantity] [varchar](255) NULL,
	[Other1] [varchar](255) NULL,
	[Other2] [varchar](255) NULL,
	[IsManuallyClosed] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[purchaseorder]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[purchaseorder](
	[TxnID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[TxnNumber] [int] NULL,
	[VendorRef_ListID] [varchar](255) NULL,
	[VendorRef_FullName] [varchar](255) NULL,
	[ClassRef_ListID] [varchar](255) NULL,
	[ClassRef_FullName] [varchar](255) NULL,
	[InventorySiteRef_ListID] [varchar](255) NULL,
	[InventorySiteRef_FullName] [varchar](255) NULL,
	[ShipToEntityRef_ListID] [varchar](255) NULL,
	[ShipToEntityRef_FullName] [varchar](255) NULL,
	[TemplateRef_ListID] [varchar](255) NULL,
	[TemplateRef_FullName] [varchar](255) NULL,
	[TxnDate] [datetime] NULL,
	[RefNumber] [varchar](255) NULL,
	[VendorAddress_Addr1] [varchar](255) NULL,
	[VendorAddress_Addr2] [varchar](255) NULL,
	[VendorAddress_Addr3] [varchar](255) NULL,
	[VendorAddress_Addr4] [varchar](255) NULL,
	[VendorAddress_Addr5] [varchar](255) NULL,
	[VendorAddress_City] [varchar](255) NULL,
	[VendorAddress_State] [varchar](255) NULL,
	[VendorAddress_PostalCode] [varchar](255) NULL,
	[VendorAddress_Country] [varchar](255) NULL,
	[VendorAddress_Note] [varchar](255) NULL,
	[ShipAddress_Addr1] [varchar](255) NULL,
	[ShipAddress_Addr2] [varchar](255) NULL,
	[ShipAddress_Addr3] [varchar](255) NULL,
	[ShipAddress_Addr4] [varchar](255) NULL,
	[ShipAddress_Addr5] [varchar](255) NULL,
	[ShipAddress_City] [varchar](255) NULL,
	[ShipAddress_State] [varchar](255) NULL,
	[ShipAddress_PostalCode] [varchar](255) NULL,
	[ShipAddress_Country] [varchar](255) NULL,
	[ShipAddress_Note] [varchar](255) NULL,
	[TermsRef_ListID] [varchar](255) NULL,
	[TermsRef_FullName] [varchar](255) NULL,
	[DueDate] [datetime] NULL,
	[ExpectedDate] [datetime] NULL,
	[ShipMethodRef_ListID] [varchar](255) NULL,
	[ShipMethodRef_FullName] [varchar](255) NULL,
	[FOB] [varchar](255) NULL,
	[TotalAmount] [float] NULL,
	[CurrencyRef_ListID] [varchar](255) NULL,
	[CurrencyRef_FullName] [varchar](255) NULL,
	[ExchangeRate] [float] NULL,
	[TotalAmountInHomeCurrency] [float] NULL,
	[Memo] [varchar](max) NULL,
	[VendorMsg] [varchar](255) NULL,
	[IsToBePrinted] [bit] NULL,
	[IsToBeEmailed] [bit] NULL,
	[IsTaxIncluded] [bit] NULL,
	[SalesTaxCodeRef_ListID] [varchar](255) NULL,
	[SalesTaxCodeRef_FullName] [varchar](255) NULL,
	[Other1] [varchar](255) NULL,
	[Other2] [varchar](255) NULL,
	[IsManuallyClosed] [varchar](255) NULL,
	[IsFullyReceived] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pricelevelperitemdetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pricelevelperitemdetail](
	[ItemRef_ListID] [varchar](255) NULL,
	[ItemRef_FullName] [varchar](255) NULL,
	[CustomPrice] [varchar](255) NULL,
	[CustomPricePercent] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pricelevel]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pricelevel](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[PriceLevelType] [varchar](255) NULL,
	[PriceLevelFixedPercentage] [varchar](255) NULL,
	[CurrencyRef_ListID] [varchar](255) NULL,
	[CurrencyRef_FullName] [varchar](255) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PayrollTransactionReport]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PayrollTransactionReport](
	[Date] [datetime] NULL,
	[TxnNumber] [int] NULL,
	[TxnType] [nvarchar](max) NULL,
	[Employee_ListID] [varchar](250) NULL,
	[Name] [nvarchar](max) NULL,
	[SourceName] [nvarchar](max) NULL,
	[PayrollItem_ListID] [varchar](250) NULL,
	[PayrollItem] [nvarchar](max) NULL,
	[WageBase] [decimal](18, 4) NULL,
	[Account_ListID] [varchar](250) NULL,
	[Account] [nvarchar](max) NULL,
	[Class_ListID] [varchar](255) NULL,
	[Class] [nvarchar](max) NULL,
	[BillingStatus] [nvarchar](max) NULL,
	[SplitAccount_ListID] [varchar](250) NULL,
	[SplitAccount] [nvarchar](max) NULL,
	[Quantity] [nvarchar](max) NULL,
	[Amount] [decimal](18, 4) NULL,
	[RefNumber] [nvarchar(255)] NULL,
	[ModifiedTime] [datetime] NULL,
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[payrollitemwage]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[payrollitemwage](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[WageType] [varchar](255) NULL,
	[ExpenseAccountRef_ListID] [varchar](255) NULL,
	[ExpenseAccountRef_FullName] [varchar](255) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[payrollitemnonwage]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[payrollitemnonwage](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[NonWageType] [varchar](255) NULL,
	[ExpenseAccountRef_ListID] [varchar](255) NULL,
	[ExpenseAccountRef_FullName] [varchar](255) NULL,
	[LiabilityAccountRef_ListID] [varchar](255) NULL,
	[LiabilityAccountRef_FullName] [varchar](255) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[paymentmethod]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[paymentmethod](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[PaymentMethodType] [varchar](255) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[othername]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[othername](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[CompanyName] [varchar](255) NULL,
	[Salutation] [varchar](255) NULL,
	[FirstName] [varchar](255) NULL,
	[MiddleName] [varchar](255) NULL,
	[LastName] [varchar](255) NULL,
	[Suffix] [varchar](255) NULL,
	[OtherNameAddress_Addr1] [varchar](255) NULL,
	[OtherNameAddress_Addr2] [varchar](255) NULL,
	[OtherNameAddress_Addr3] [varchar](255) NULL,
	[OtherNameAddress_Addr4] [varchar](255) NULL,
	[OtherNameAddress_Addr5] [varchar](255) NULL,
	[OtherNameAddress_City] [varchar](255) NULL,
	[OtherNameAddress_State] [varchar](255) NULL,
	[OtherNameAddress_PostalCode] [varchar](255) NULL,
	[OtherNameAddress_Country] [varchar](255) NULL,
	[OtherNameAddress_Note] [varchar](255) NULL,
	[Phone] [varchar](255) NULL,
	[Mobile] [varchar](255) NULL,
	[Pager] [varchar](255) NULL,
	[AltPhone] [varchar](255) NULL,
	[Fax] [varchar](255) NULL,
	[Email] [varchar](255) NULL,
	[Contact] [varchar](255) NULL,
	[AltContact] [varchar](255) NULL,
	[AccountNumber] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[os_log]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[os_log](
	[TableName] [varchar](255) NULL,
	[QBWFile] [varchar](255) NULL,
	[Operation] [varchar](255) NULL,
	[Result] [varchar](255) NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[RecordsProcessed] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mvw_detailtrans]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mvw_detailtrans](
	[ExtID] [varchar](255) NULL,
	[TxnLineID] [varchar](511) NULL,
	[TxnDate] [datetime] NULL,
	[ReferenceNumber] [varchar](255) NULL,
	[TXNTYPE] [varchar](255) NULL,
	[AccountRef_ListID] [varchar](255) NULL,
	[CustomerRef_ListID] [varchar](255) NULL,
	[VendorRef_ListID] [varchar](255) NULL,
	[ClassRef_ListID] [varchar](255) NOT NULL,
	[ItemRef_ListID] [varchar](255) NOT NULL,
	[CurrencyRef_ListID] [varchar](255) NULL,
	[ServiceDate] [datetime] NULL,
	[Desc] [varchar](1024) NULL,
	[quantity] [float] NULL,
	[RATE] [float] NULL,
	[RatePercent] [float] NULL,
	[AmountInNativeCurrency] [float] NULL,
	[Amount] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[linkedtxndetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[linkedtxndetail](
	[TxnID] [varchar](255) NULL,
	[TxnType] [varchar](255) NULL,
	[TxnDate] [datetime] NULL,
	[RefNumber] [varchar](255) NULL,
	[LinkType] [varchar](255) NULL,
	[Amount] [float] NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[journalentry]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[journalentry](
	[TxnID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[TxnNumber] [int] NULL,
	[TxnDate] [datetime] NULL,
	[RefNumber] [varchar](255) NULL,
	[Memo] [varchar](max) NULL,
	[IsAdjustment] [bit] NULL,
	[IsHomeCurrencyAdjustment] [bit] NULL,
	[IsAmountsEnteredInHomeCurrency] [bit] NULL,
	[CurrencyRef_ListID] [varchar](255) NULL,
	[CurrencyRef_FullName] [varchar](255) NULL,
	[ExchangeRate] [float] NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL,
	[Name] [varchar](200) NULL,
	[TxnType] [varchar](200) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[journaldebitlinedetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[journaldebitlinedetail](
	[TxnLineID] [varchar](255) NULL,
	[AccountRef_ListID] [varchar](255) NULL,
	[AccountRef_FullName] [varchar](255) NULL,
	[Amount] [float] NULL,
	[Memo] [varchar](max) NULL,
	[EntityRef_ListID] [varchar](255) NULL,
	[EntityRef_FullName] [varchar](255) NULL,
	[ClassRef_ListID] [varchar](255) NULL,
	[ClassRef_FullName] [varchar](255) NULL,
	[ItemSalesTaxRef_ListID] [varchar](255) NULL,
	[ItemSalesTaxRef_FullName] [varchar](255) NULL,
	[BillableStatus] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[journalcreditlinedetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[journalcreditlinedetail](
	[TxnLineID] [varchar](255) NULL,
	[AccountRef_ListID] [varchar](255) NULL,
	[AccountRef_FullName] [varchar](255) NULL,
	[Amount] [float] NULL,
	[Memo] [varchar](max) NULL,
	[IsAdjustment] [bit] NULL,
	[EntityRef_ListID] [varchar](255) NULL,
	[EntityRef_FullName] [varchar](255) NULL,
	[ClassRef_ListID] [varchar](255) NULL,
	[ClassRef_FullName] [varchar](255) NULL,
	[ItemSalesTaxRef_ListID] [varchar](255) NULL,
	[ItemSalesTaxRef_FullName] [varchar](255) NULL,
	[BillableStatus] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[jobtype]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[jobtype](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[FullName] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[ParentRef_ListID] [varchar](255) NULL,
	[ParentRef_FullName] [varchar](255) NULL,
	[Sublevel] [int] NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[itemsubtotal]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[itemsubtotal](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[ItemDesc] [varchar](255) NULL,
	[SpecialItemType] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[itemsites]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[itemsites](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[ItemInventoryAssemblyRef_ListID] [varchar](255) NULL,
	[ItemInventoryAssemblyRef_FullName] [varchar](255) NULL,
	[ItemInventoryRef_ListID] [varchar](255) NULL,
	[ItemInventoryRef_FullName] [varchar](255) NULL,
	[InventorySiteRef_ListID] [varchar](255) NULL,
	[InventorySiteRef_FullName] [varchar](255) NULL,
	[ReorderLevel] [varchar](255) NULL,
	[QuantityOnHand] [varchar](255) NULL,
	[QuantityOnPurchaseOrders] [varchar](255) NULL,
	[QuantityOnSalesOrders] [varchar](255) NULL,
	[QuantityToBeBuiltByPendingBuildTxns] [varchar](255) NULL,
	[QuantityRequiredByPendingBuildTxns] [varchar](255) NULL,
	[QuantityOnPendingTransfers] [varchar](255) NULL,
	[AssemblyBuildPoint] [varchar](255) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[itemservice]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[itemservice](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[FullName] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[ParentRef_ListID] [varchar](255) NULL,
	[ParentRef_FullName] [varchar](255) NULL,
	[Sublevel] [int] NULL,
	[UnitOfMeasureSetRef_ListID] [varchar](255) NULL,
	[UnitOfMeasureSetRef_FullName] [varchar](255) NULL,
	[IsTaxIncluded] [bit] NULL,
	[SalesTaxCodeRef_ListID] [varchar](255) NULL,
	[SalesTaxCodeRef_FullName] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[itemsalestaxgrouplinedetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[itemsalestaxgrouplinedetail](
	[ItemSalesTaxRef_ListID] [varchar](255) NULL,
	[ItemSalesTaxRef_FullName] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[itemsalestaxgroup]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[itemsalestaxgroup](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[ItemDesc] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[itemsalestax]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[itemsalestax](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[ItemDesc] [varchar](255) NULL,
	[IsUsedOnPurchaseTransaction] [bit] NULL,
	[TaxRate] [varchar](255) NULL,
	[TaxVendorRef_ListID] [varchar](255) NULL,
	[TaxVendorRef_FullName] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[items]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[items](
	[ListID] [varchar](200) NULL,
	[FullName] [varchar](200) NULL,
	[TableName] [varchar](200) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[itemreceipt]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[itemreceipt](
	[TxnID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[TxnNumber] [int] NULL,
	[VendorRef_ListID] [varchar](255) NULL,
	[VendorRef_FullName] [varchar](255) NULL,
	[APAccountRef_ListID] [varchar](255) NULL,
	[APAccountRef_FullName] [varchar](255) NULL,
	[TxnDate] [datetime] NULL,
	[TotalAmount] [float] NULL,
	[CurrencyRef_ListID] [varchar](255) NULL,
	[CurrencyRef_FullName] [varchar](255) NULL,
	[ExchangeRate] [float] NULL,
	[TotalAmountInHomeCurrency] [float] NULL,
	[RefNumber] [varchar](255) NULL,
	[Memo] [varchar](max) NULL,
	[LinkedTxn] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[itempayment]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[itempayment](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[ItemDesc] [varchar](255) NULL,
	[DepositToAccountRef_ListID] [varchar](255) NULL,
	[DepositToAccountRef_FullName] [varchar](255) NULL,
	[PaymentMethodRef_ListID] [varchar](255) NULL,
	[PaymentMethodRef_FullName] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[itemothercharge]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[itemothercharge](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[FullName] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[ParentRef_ListID] [varchar](255) NULL,
	[ParentRef_FullName] [varchar](255) NULL,
	[Sublevel] [int] NULL,
	[IsTaxIncluded] [bit] NULL,
	[SalesTaxCodeRef_ListID] [varchar](255) NULL,
	[SalesTaxCodeRef_FullName] [varchar](255) NULL,
	[SpecialItemType] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[itemnoninventory]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[itemnoninventory](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[FullName] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[ParentRef_ListID] [varchar](255) NULL,
	[ParentRef_FullName] [varchar](255) NULL,
	[Sublevel] [int] NULL,
	[ManufacturerPartNumber] [varchar](255) NULL,
	[UnitOfMeasureSetRef_ListID] [varchar](255) NULL,
	[UnitOfMeasureSetRef_FullName] [varchar](255) NULL,
	[IsTaxIncluded] [bit] NULL,
	[SalesTaxCodeRef_ListID] [varchar](255) NULL,
	[SalesTaxCodeRef_FullName] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[iteminventoryassemblylinedetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[iteminventoryassemblylinedetail](
	[ItemInventoryRef_ListID] [varchar](255) NULL,
	[ItemInventoryRef_FullName] [varchar](255) NULL,
	[Quantity] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[iteminventoryassembly]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[iteminventoryassembly](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[FullName] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[ParentRef_ListID] [varchar](255) NULL,
	[ParentRef_FullName] [varchar](255) NULL,
	[Sublevel] [int] NULL,
	[SalesTaxCodeRef_ListID] [varchar](255) NULL,
	[SalesTaxCodeRef_FullName] [varchar](255) NULL,
	[IsTaxIncluded] [bit] NULL,
	[SalesDesc] [varchar](255) NULL,
	[SalesPrice] [varchar](255) NULL,
	[IncomeAccountRef_ListID] [varchar](255) NULL,
	[IncomeAccountRef_FullName] [varchar](255) NULL,
	[PurchaseDesc] [varchar](255) NULL,
	[PurchaseCost] [varchar](255) NULL,
	[PurchaseTaxCodeRef_ListID] [varchar](255) NULL,
	[PurchaseTaxCodeRef_FullName] [varchar](255) NULL,
	[COGSAccountRef_ListID] [varchar](255) NULL,
	[COGSAccountRef_FullName] [varchar](255) NULL,
	[PrefVendorRef_ListID] [varchar](255) NULL,
	[PrefVendorRef_FullName] [varchar](255) NULL,
	[AssetAccountRef_ListID] [varchar](255) NULL,
	[AssetAccountRef_FullName] [varchar](255) NULL,
	[BuildPoint] [varchar](255) NULL,
	[QuantityOnHand] [varchar](255) NULL,
	[AverageCost] [varchar](255) NULL,
	[QuantityOnOrder] [varchar](255) NULL,
	[QuantityOnSalesOrder] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[iteminventory]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[iteminventory](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[FullName] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[ParentRef_ListID] [varchar](255) NULL,
	[ParentRef_FullName] [varchar](255) NULL,
	[Sublevel] [int] NULL,
	[ManufacturerPartNumber] [varchar](255) NULL,
	[UnitOfMeasureSetRef_ListID] [varchar](255) NULL,
	[UnitOfMeasureSetRef_FullName] [varchar](255) NULL,
	[IsTaxIncluded] [bit] NULL,
	[SalesTaxCodeRef_ListID] [varchar](255) NULL,
	[SalesTaxCodeRef_FullName] [varchar](255) NULL,
	[SalesDesc] [varchar](255) NULL,
	[SalesPrice] [varchar](255) NULL,
	[IncomeAccountRef_ListID] [varchar](255) NULL,
	[IncomeAccountRef_FullName] [varchar](255) NULL,
	[PurchaseDesc] [varchar](255) NULL,
	[PurchaseCost] [varchar](255) NULL,
	[COGSAccountRef_ListID] [varchar](255) NULL,
	[COGSAccountRef_FullName] [varchar](255) NULL,
	[PrefVendorRef_ListID] [varchar](255) NULL,
	[PrefVendorRef_FullName] [varchar](255) NULL,
	[AssetAccountRef_ListID] [varchar](255) NULL,
	[AssetAccountRef_FullName] [varchar](255) NULL,
	[ReorderPoint] [varchar](255) NULL,
	[QuantityOnHand] [varchar](255) NULL,
	[AverageCost] [varchar](255) NULL,
	[QuantityOnOrder] [varchar](255) NULL,
	[QuantityOnSalesOrder] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[itemgrouplinedetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[itemgrouplinedetail](
	[ItemRef_ListID] [varchar](255) NULL,
	[ItemRef_FullName] [varchar](255) NULL,
	[Quantity] [varchar](255) NULL,
	[UnitOfMeasure] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[itemgroup]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[itemgroup](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[ItemDesc] [varchar](255) NULL,
	[IsPrintItemsInGroup] [bit] NULL,
	[SpecialItemType] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[itemfixedasset]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[itemfixedasset](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[AcquiredAs] [varchar](255) NULL,
	[PurchaseDesc] [varchar](255) NULL,
	[PurchaseDate] [datetime] NULL,
	[PurchaseCost] [varchar](255) NULL,
	[VendorOrPayeeName] [varchar](255) NULL,
	[AssetAccountRef_ListID] [varchar](255) NULL,
	[AssetAccountRef_FullName] [varchar](255) NULL,
	[AssetDesc] [varchar](255) NULL,
	[Location] [varchar](255) NULL,
	[PONumber] [varchar](255) NULL,
	[SerialNumber] [varchar](255) NULL,
	[WarrantyExpDate] [datetime] NULL,
	[Notes] [varchar](max) NULL,
	[AssetNumber] [varchar](255) NULL,
	[CostBasis] [float] NULL,
	[YearEndAccumulatedDepreciation] [float] NULL,
	[YearEndBookValue] [float] NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[itemdiscount]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[itemdiscount](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[FullName] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[ParentRef_ListID] [varchar](255) NULL,
	[ParentRef_FullName] [varchar](255) NULL,
	[Sublevel] [int] NULL,
	[ItemDesc] [varchar](255) NULL,
	[SalesTaxCodeRef_ListID] [varchar](255) NULL,
	[SalesTaxCodeRef_FullName] [varchar](255) NULL,
	[DiscountRate] [varchar](255) NULL,
	[DiscountRatePercent] [varchar](255) NULL,
	[AccountRef_ListID] [varchar](255) NULL,
	[AccountRef_FullName] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[invoicelinegroupdetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[invoicelinegroupdetail](
	[TxnLineID] [varchar](255) NULL,
	[ItemGroupRef_ListID] [varchar](255) NULL,
	[ItemGroupRef_FullName] [varchar](255) NULL,
	[Desc] [varchar](255) NULL,
	[Quantity] [varchar](255) NULL,
	[IsPrintItemsInGroup] [bit] NULL,
	[TotalAmount] [float] NULL,
	[ServiceDate] [datetime] NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[invoicelinedetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[invoicelinedetail](
	[TxnLineID] [varchar](255) NULL,
	[ItemRef_ListID] [varchar](255) NULL,
	[ItemRef_FullName] [varchar](255) NULL,
	[Desc] [varchar](max) NULL,
	[Quantity] [varchar](255) NULL,
	[UnitOfMeasure] [varchar](255) NULL,
	[OverrideUOMSetRef_ListID] [varchar](255) NULL,
	[OverrideUOMSetRef_FullName] [varchar](255) NULL,
	[Rate] [varchar](255) NULL,
	[RatePercent] [varchar](255) NULL,
	[ClassRef_ListID] [varchar](255) NULL,
	[ClassRef_FullName] [varchar](255) NULL,
	[Amount] [float] NULL,
	[InventorySiteRef_ListID] [varchar](255) NULL,
	[InventorySiteRef_FullName] [varchar](255) NULL,
	[ServiceDate] [datetime] NULL,
	[SalesTaxCodeRef_ListID] [varchar](255) NULL,
	[SalesTaxCodeRef_FullName] [varchar](255) NULL,
	[Other1] [varchar](255) NULL,
	[Other2] [varchar](255) NULL,
	[LinkedTxnID] [varchar](255) NULL,
	[LinkedTxnLineID] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[invoice]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[invoice](
	[TxnID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[TxnNumber] [int] NULL,
	[CustomerRef_ListID] [varchar](255) NULL,
	[CustomerRef_FullName] [varchar](255) NULL,
	[ClassRef_ListID] [varchar](255) NULL,
	[ClassRef_FullName] [varchar](255) NULL,
	[ARAccountRef_ListID] [varchar](255) NULL,
	[ARAccountRef_FullName] [varchar](255) NULL,
	[TemplateRef_ListID] [varchar](255) NULL,
	[TemplateRef_FullName] [varchar](255) NULL,
	[TxnDate] [datetime] NULL,
	[RefNumber] [varchar](255) NULL,
	[BillAddress_Addr1] [varchar](255) NULL,
	[BillAddress_Addr2] [varchar](255) NULL,
	[BillAddress_Addr3] [varchar](255) NULL,
	[BillAddress_Addr4] [varchar](255) NULL,
	[BillAddress_Addr5] [varchar](255) NULL,
	[BillAddress_City] [varchar](255) NULL,
	[BillAddress_State] [varchar](255) NULL,
	[BillAddress_PostalCode] [varchar](255) NULL,
	[BillAddress_Country] [varchar](255) NULL,
	[BillAddress_Note] [varchar](255) NULL,
	[ShipAddress_Addr1] [varchar](255) NULL,
	[ShipAddress_Addr2] [varchar](255) NULL,
	[ShipAddress_Addr3] [varchar](255) NULL,
	[ShipAddress_Addr4] [varchar](255) NULL,
	[ShipAddress_Addr5] [varchar](255) NULL,
	[ShipAddress_City] [varchar](255) NULL,
	[ShipAddress_State] [varchar](255) NULL,
	[ShipAddress_PostalCode] [varchar](255) NULL,
	[ShipAddress_Country] [varchar](255) NULL,
	[ShipAddress_Note] [varchar](255) NULL,
	[IsPending] [bit] NULL,
	[IsFinanceCharge] [bit] NULL,
	[PONumber] [varchar](255) NULL,
	[TermsRef_ListID] [varchar](255) NULL,
	[TermsRef_FullName] [varchar](255) NULL,
	[DueDate] [datetime] NULL,
	[SalesRepRef_ListID] [varchar](255) NULL,
	[SalesRepRef_FullName] [varchar](255) NULL,
	[FOB] [varchar](255) NULL,
	[ShipDate] [datetime] NULL,
	[ShipMethodRef_ListID] [varchar](255) NULL,
	[ShipMethodRef_FullName] [varchar](255) NULL,
	[Subtotal] [float] NULL,
	[ItemSalesTaxRef_ListID] [varchar](255) NULL,
	[ItemSalesTaxRef_FullName] [varchar](255) NULL,
	[SalesTaxPercentage] [varchar](255) NULL,
	[SalesTaxTotal] [float] NULL,
	[AppliedAmount] [float] NULL,
	[BalanceRemaining] [float] NULL,
	[CurrencyRef_ListID] [varchar](255) NULL,
	[CurrencyRef_FullName] [varchar](255) NULL,
	[ExchangeRate] [float] NULL,
	[BalanceRemainingInHomeCurrency] [float] NULL,
	[Memo] [varchar](max) NULL,
	[IsPaid] [bit] NULL,
	[CustomerMsgRef_ListID] [varchar](255) NULL,
	[CustomerMsgRef_FullName] [varchar](255) NULL,
	[IsToBePrinted] [bit] NULL,
	[IsToBeEmailed] [bit] NULL,
	[IsTaxIncluded] [bit] NULL,
	[CustomerSalesTaxCodeRef_ListID] [varchar](255) NULL,
	[CustomerSalesTaxCodeRef_FullName] [varchar](255) NULL,
	[SuggestedDiscountAmount] [float] NULL,
	[SuggestedDiscountDate] [datetime] NULL,
	[Other] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[inventorysite]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[inventorysite](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[IsDefaultSite] [bit] NULL,
	[SiteDesc] [varchar](255) NULL,
	[Contact] [varchar](255) NULL,
	[Phone] [varchar](255) NULL,
	[Fax] [varchar](255) NULL,
	[Email] [varchar](255) NULL,
	[SiteAddress_Addr1] [varchar](255) NULL,
	[SiteAddress_Addr2] [varchar](255) NULL,
	[SiteAddress_Addr3] [varchar](255) NULL,
	[SiteAddress_Addr4] [varchar](255) NULL,
	[SiteAddress_Addr5] [varchar](255) NULL,
	[SiteAddress_City] [varchar](255) NULL,
	[SiteAddress_State] [varchar](255) NULL,
	[SiteAddress_PostalCode] [varchar](255) NULL,
	[SiteAddress_Country] [varchar](255) NULL,
	[SiteAddressBlock_Addr1] [varchar](255) NULL,
	[SiteAddressBlock_Addr2] [varchar](255) NULL,
	[SiteAddressBlock_Addr3] [varchar](255) NULL,
	[SiteAddressBlock_Addr4] [varchar](255) NULL,
	[SiteAddressBlock_Addr5] [varchar](255) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[inventoryadjustmentlinedetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[inventoryadjustmentlinedetail](
	[TxnLineID] [varchar](255) NULL,
	[ItemRef_ListID] [varchar](255) NULL,
	[ItemRef_FullName] [varchar](255) NULL,
	[QuantityDifference] [varchar](255) NULL,
	[ValueDifference] [float] NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[inventoryadjustment]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[inventoryadjustment](
	[TxnID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[TxnNumber] [int] NULL,
	[AccountRef_ListID] [varchar](255) NULL,
	[AccountRef_FullName] [varchar](255) NULL,
	[InventorySiteRef_ListID] [varchar](255) NULL,
	[InventorySiteRef_FullName] [varchar](255) NULL,
	[TxnDate] [datetime] NULL,
	[RefNumber] [varchar](255) NULL,
	[CustomerRef_ListID] [varchar](255) NULL,
	[CustomerRef_FullName] [varchar](255) NULL,
	[ClassRef_ListID] [varchar](255) NULL,
	[ClassRef_FullName] [varchar](255) NULL,
	[Memo] [varchar](max) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[history]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[history](
	[TableName] [varchar](200) NULL,
	[ModifiedDate] [datetime] NULL,
	[QBWFile] [varchar](200) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[fixedassetsalesinfodetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fixedassetsalesinfodetail](
	[SalesDesc] [varchar](255) NULL,
	[SalesDate] [datetime] NULL,
	[SalesPrice] [varchar](255) NULL,
	[SalesExpense] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[estimatelinegroupdetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[estimatelinegroupdetail](
	[TxnLineID] [varchar](255) NULL,
	[ItemGroupRef_ListID] [varchar](255) NULL,
	[ItemGroupRef_FullName] [varchar](255) NULL,
	[Desc] [varchar](255) NULL,
	[Quantity] [varchar](255) NULL,
	[IsPrintItemsInGroup] [bit] NULL,
	[TotalAmount] [float] NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[estimatelinedetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[estimatelinedetail](
	[TxnLineID] [varchar](255) NULL,
	[ItemRef_ListID] [varchar](255) NULL,
	[ItemRef_FullName] [varchar](255) NULL,
	[Desc] [varchar](max) NULL,
	[Quantity] [varchar](255) NULL,
	[UnitOfMeasure] [varchar](255) NULL,
	[OverrideUOMSetRef_ListID] [varchar](255) NULL,
	[OverrideUOMSetRef_FullName] [varchar](255) NULL,
	[Rate] [varchar](255) NULL,
	[RatePercent] [varchar](255) NULL,
	[ClassRef_ListID] [varchar](255) NULL,
	[ClassRef_FullName] [varchar](255) NULL,
	[Amount] [float] NULL,
	[InventorySiteRef_ListID] [varchar](255) NULL,
	[InventorySiteRef_FullName] [varchar](255) NULL,
	[SalesTaxCodeRef_ListID] [varchar](255) NULL,
	[SalesTaxCodeRef_FullName] [varchar](255) NULL,
	[MarkupRate] [varchar](255) NULL,
	[MarkupRatePercent] [varchar](255) NULL,
	[Other1] [varchar](255) NULL,
	[Other2] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[estimate]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[estimate](
	[TxnID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[TxnNumber] [int] NULL,
	[CustomerRef_ListID] [varchar](255) NULL,
	[CustomerRef_FullName] [varchar](255) NULL,
	[ClassRef_ListID] [varchar](255) NULL,
	[ClassRef_FullName] [varchar](255) NULL,
	[TemplateRef_ListID] [varchar](255) NULL,
	[TemplateRef_FullName] [varchar](255) NULL,
	[TxnDate] [datetime] NULL,
	[RefNumber] [varchar](255) NULL,
	[BillAddress_Addr1] [varchar](255) NULL,
	[BillAddress_Addr2] [varchar](255) NULL,
	[BillAddress_Addr3] [varchar](255) NULL,
	[BillAddress_Addr4] [varchar](255) NULL,
	[BillAddress_Addr5] [varchar](255) NULL,
	[BillAddress_City] [varchar](255) NULL,
	[BillAddress_State] [varchar](255) NULL,
	[BillAddress_PostalCode] [varchar](255) NULL,
	[BillAddress_Country] [varchar](255) NULL,
	[BillAddress_Note] [varchar](255) NULL,
	[ShipAddress_Addr1] [varchar](255) NULL,
	[ShipAddress_Addr2] [varchar](255) NULL,
	[ShipAddress_Addr3] [varchar](255) NULL,
	[ShipAddress_Addr4] [varchar](255) NULL,
	[ShipAddress_Addr5] [varchar](255) NULL,
	[ShipAddress_City] [varchar](255) NULL,
	[ShipAddress_State] [varchar](255) NULL,
	[ShipAddress_PostalCode] [varchar](255) NULL,
	[ShipAddress_Country] [varchar](255) NULL,
	[ShipAddress_Note] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[PONumber] [varchar](255) NULL,
	[TermsRef_ListID] [varchar](255) NULL,
	[TermsRef_FullName] [varchar](255) NULL,
	[DueDate] [datetime] NULL,
	[SalesRepRef_ListID] [varchar](255) NULL,
	[SalesRepRef_FullName] [varchar](255) NULL,
	[FOB] [varchar](255) NULL,
	[Subtotal] [float] NULL,
	[ItemSalesTaxRef_ListID] [varchar](255) NULL,
	[ItemSalesTaxRef_FullName] [varchar](255) NULL,
	[SalesTaxPercentage] [varchar](255) NULL,
	[SalesTaxTotal] [float] NULL,
	[TotalAmount] [float] NULL,
	[CurrencyRef_ListID] [varchar](255) NULL,
	[CurrencyRef_FullName] [varchar](255) NULL,
	[ExchangeRate] [float] NULL,
	[TotalAmountInHomeCurrency] [float] NULL,
	[Memo] [varchar](max) NULL,
	[CustomerMsgRef_ListID] [varchar](255) NULL,
	[CustomerMsgRef_FullName] [varchar](255) NULL,
	[IsToBeEmailed] [bit] NULL,
	[CustomerSalesTaxCodeRef_ListID] [varchar](255) NULL,
	[CustomerSalesTaxCodeRef_FullName] [varchar](255) NULL,
	[Other] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[error_table]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[error_table](
	[Error_Num] [varchar](200) NULL,
	[Error_Table] [varchar](200) NULL,
	[Error_Field] [varchar](200) NULL,
	[IDKEY] [varchar](40) NULL,
	[Error_Desc] [varchar](500) NULL,
	[TIME_DATE] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[entity]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[entity](
	[ListID] [varchar](200) NULL,
	[FullName] [varchar](200) NULL,
	[TableName] [varchar](200) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[employeepayrollinfodetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[employeepayrollinfodetail](
	[PayPeriod] [varchar](255) NULL,
	[ClassRef_ListID] [varchar](255) NULL,
	[ClassRef_FullName] [varchar](255) NULL,
	[IsUsingTimeDataToCreatePaychecks] [bit] NULL,
	[UseTimeDataToCreatePaychecks] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[employee]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[employee](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[Salutation] [varchar](255) NULL,
	[FirstName] [varchar](255) NULL,
	[MiddleName] [varchar](255) NULL,
	[LastName] [varchar](255) NULL,
	[Suffix] [varchar](255) NULL,
	[EmployeeAddress_Addr1] [varchar](255) NULL,
	[EmployeeAddress_Addr2] [varchar](255) NULL,
	[EmployeeAddress_Addr3] [varchar](255) NULL,
	[EmployeeAddress_Addr4] [varchar](255) NULL,
	[EmployeeAddress_City] [varchar](255) NULL,
	[EmployeeAddress_State] [varchar](255) NULL,
	[EmployeeAddress_PostalCode] [varchar](255) NULL,
	[EmployeeAddress_Country] [varchar](255) NULL,
	[PrintAs] [varchar](255) NULL,
	[Phone] [varchar](255) NULL,
	[Mobile] [varchar](255) NULL,
	[Pager] [varchar](255) NULL,
	[PagerPIN] [varchar](255) NULL,
	[AltPhone] [varchar](255) NULL,
	[Fax] [varchar](255) NULL,
	[SSN] [varchar](255) NULL,
	[Email] [varchar](255) NULL,
	[EmployeeType] [varchar](255) NULL,
	[Gender] [varchar](255) NULL,
	[HiredDate] [datetime] NULL,
	[ReleasedDate] [datetime] NULL,
	[BirthDate] [datetime] NULL,
	[AccountNumber] [varchar](255) NULL,
	[Notes] [varchar](max) NULL,
	[BillingRateRef_ListID] [varchar](255) NULL,
	[BillingRateRef_FullName] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ecfo_txn_type_lkup]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ecfo_txn_type_lkup](
	[listid] [varchar](20) NOT NULL,
	[ParentRef_ListID] [int] NULL,
	[name] [varchar](28) NOT NULL,
	[PlanParentRef_ListID] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ECFO_MISCTXN]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ECFO_MISCTXN](
	[BLANK] [nvarchar](255) NULL,
	[ACCOUNT] [nvarchar](255) NULL,
	[AMOUNT] [nvarchar](255) NULL,
	[CLASS] [nvarchar](255) NULL,
	[DATES] [nvarchar](255) NULL,
	[ITEM] [nvarchar](255) NULL,
	[ITEMDESCRIPTION] [nvarchar](255) NULL,
	[MEMOS] [nvarchar](255) NULL,
	[NAME] [nvarchar](255) NULL,
	[NUM] [nvarchar](255) NULL,
	[TRANS] [nvarchar](255) NULL,
	[TYPE] [nvarchar](255) NULL,
	[RowType] [nvarchar](255) NULL,
	[RowDataType] [nvarchar](255) NULL,
	[RowDataValue] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ECFO_MISCREV]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ECFO_MISCREV](
	[BLANK] [nvarchar](255) NULL,
	[ACCOUNT] [nvarchar](255) NULL,
	[AMOUNT] [nvarchar](255) NULL,
	[CLASS] [nvarchar](255) NULL,
	[DATES] [nvarchar](255) NULL,
	[ITEM] [nvarchar](255) NULL,
	[ITEMDESCRIPTION] [nvarchar](255) NULL,
	[MEMOS] [nvarchar](255) NULL,
	[NAME] [nvarchar](255) NULL,
	[QTY] [nvarchar](255) NULL,
	[NUM] [nvarchar](255) NULL,
	[SPLIT] [nvarchar](255) NULL,
	[TYPE] [nvarchar](255) NULL,
	[TXNID] [nvarchar](255) NULL,
	[RowType] [nvarchar](255) NULL,
	[RowDataType] [nvarchar](255) NULL,
	[RowDataValue] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ECFO_LOG]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ECFO_LOG](
	[Process] [nvarchar](255) NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Result] [nvarchar](255) NULL,
	[LoadedInd] [bit] NULL,
	[LoadedTime] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ECFO_INVENTORYTXN]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ECFO_INVENTORYTXN](
	[BLANK] [nvarchar](255) NULL,
	[ACCOUNT] [nvarchar](255) NULL,
	[AMOUNT] [nvarchar](255) NULL,
	[CLASS] [nvarchar](255) NULL,
	[DATES] [nvarchar](255) NULL,
	[ITEM] [nvarchar](255) NULL,
	[ITEMDESCRIPTION] [nvarchar](255) NULL,
	[MEMOS] [nvarchar](255) NULL,
	[NAME] [nvarchar](255) NULL,
	[QTY] [nvarchar](255) NULL,
	[NUM] [nvarchar](255) NULL,
	[SPLIT] [nvarchar](255) NULL,
	[TYPE] [nvarchar](255) NULL,
	[TXNID] [nvarchar](255) NULL,
	[RowType] [nvarchar](255) NULL,
	[RowDataType] [nvarchar](255) NULL,
	[RowDataValue] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ecfo_hierarchy_lkup]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ecfo_hierarchy_lkup](
	[source] [varchar](25) NOT NULL,
	[listid] [varchar](255) NULL,
	[parentref_listid] [varchar](21) NULL,
	[name] [varchar](255) NULL,
	[unary_operator] [varchar](5) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ECFO_CONFIG]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ECFO_CONFIG](
	[row_id] [int] IDENTITY(1,1) NOT NULL,
	[user_name] [varchar](50) NOT NULL,
	[qb_filename] [varchar](255) NOT NULL,
	[config_file] [xml] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ecfo_coa_lkup]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ecfo_coa_lkup](
	[listID] [varchar](255) NULL,
	[ParentRef_ListID] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[unary] [varchar](1) NOT NULL,
	[accountnumber] [varchar](255) NULL,
	[sortorder] [varchar](50) NULL,
	[alt_unary] [varchar](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ECFO_CMTXN]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ECFO_CMTXN](
	[BLANK] [nvarchar](255) NULL,
	[ACCOUNT] [nvarchar](255) NULL,
	[AMOUNT] [nvarchar](255) NULL,
	[CLASS] [nvarchar](255) NULL,
	[DATES] [nvarchar](255) NULL,
	[ITEM] [nvarchar](255) NULL,
	[ITEMDESCRIPTION] [nvarchar](255) NULL,
	[MEMOS] [nvarchar](255) NULL,
	[NAME] [nvarchar](255) NULL,
	[QTY] [nvarchar](255) NULL,
	[NUM] [nvarchar](255) NULL,
	[SPLIT] [nvarchar](255) NULL,
	[TYPE] [nvarchar](255) NULL,
	[TXNID] [nvarchar](255) NULL,
	[RowType] [nvarchar](255) NULL,
	[RowDataType] [nvarchar](255) NULL,
	[RowDataValue] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ecfo_ar_status_lkup]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ecfo_ar_status_lkup](
	[status] [varchar](1) NOT NULL,
	[Name] [varchar](12) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[earningsdetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[earningsdetail](
	[PayrollItemWageRef_ListID] [varchar](255) NULL,
	[PayrollItemWageRef_FullName] [varchar](255) NULL,
	[Rate] [varchar](255) NULL,
	[RatePercent] [varchar](255) NULL,
	[IDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[depositlinedetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[depositlinedetail](
	[TxnType] [varchar](255) NULL,
	[TxnID] [varchar](255) NULL,
	[TxnLineID] [varchar](255) NULL,
	[PaymentTxnLineID] [varchar](255) NULL,
	[EntityRef_ListID] [varchar](255) NULL,
	[EntityRef_FullName] [varchar](255) NULL,
	[AccountRef_ListID] [varchar](255) NULL,
	[AccountRef_FullName] [varchar](255) NULL,
	[Memo] [varchar](max) NULL,
	[CheckNumber] [varchar](255) NULL,
	[PaymentMethodRef_ListID] [varchar](255) NULL,
	[PaymentMethodRef_FullName] [varchar](255) NULL,
	[ClassRef_ListID] [varchar](255) NULL,
	[ClassRef_FullName] [varchar](255) NULL,
	[Amount] [float] NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[deposit]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[deposit](
	[TxnID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[TxnNumber] [int] NULL,
	[TxnDate] [datetime] NULL,
	[DepositToAccountRef_ListID] [varchar](255) NULL,
	[DepositToAccountRef_FullName] [varchar](255) NULL,
	[Memo] [varchar](max) NULL,
	[DepositTotal] [float] NULL,
	[CurrencyRef_ListID] [varchar](255) NULL,
	[CurrencyRef_FullName] [varchar](255) NULL,
	[ExchangeRate] [float] NULL,
	[DepositTotalInHomeCurrency] [float] NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[defaultunitdetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[defaultunitdetail](
	[UnitUsedFor] [varchar](255) NULL,
	[Unit] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[datedriventerms]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[datedriventerms](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[DayOfMonthDue] [int] NULL,
	[DueNextMonthDays] [int] NULL,
	[DiscountDayOfMonth] [int] NULL,
	[DiscountPct] [varchar](255) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dataextdetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dataextdetail](
	[OwnerID] [varchar](255) NULL,
	[DataExtName] [varchar](255) NULL,
	[DataExtType] [varchar](255) NULL,
	[DataExtValue] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL,
	[OwnerType] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dataextdef]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dataextdef](
	[OwnerID] [varchar](255) NULL,
	[DataExtName] [varchar](255) NULL,
	[DataExtType] [varchar](255) NULL,
	[AssignToObject] [varchar](255) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[customertype]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[customertype](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[FullName] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[ParentRef_ListID] [varchar](255) NULL,
	[ParentRef_FullName] [varchar](255) NULL,
	[Sublevel] [int] NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[customermsg]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[customermsg](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[customer]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[customer](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[FullName] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[ParentRef_ListID] [varchar](255) NULL,
	[ParentRef_FullName] [varchar](255) NULL,
	[Sublevel] [int] NULL,
	[CompanyName] [varchar](255) NULL,
	[Salutation] [varchar](255) NULL,
	[FirstName] [varchar](255) NULL,
	[MiddleName] [varchar](255) NULL,
	[LastName] [varchar](255) NULL,
	[Suffix] [varchar](255) NULL,
	[BillAddress_Addr1] [varchar](255) NULL,
	[BillAddress_Addr2] [varchar](255) NULL,
	[BillAddress_Addr3] [varchar](255) NULL,
	[BillAddress_Addr4] [varchar](255) NULL,
	[BillAddress_Addr5] [varchar](255) NULL,
	[BillAddress_City] [varchar](255) NULL,
	[BillAddress_State] [varchar](255) NULL,
	[BillAddress_PostalCode] [varchar](255) NULL,
	[BillAddress_Country] [varchar](255) NULL,
	[BillAddress_Note] [varchar](255) NULL,
	[ShipAddress_Addr1] [varchar](255) NULL,
	[ShipAddress_Addr2] [varchar](255) NULL,
	[ShipAddress_Addr3] [varchar](255) NULL,
	[ShipAddress_Addr4] [varchar](255) NULL,
	[ShipAddress_Addr5] [varchar](255) NULL,
	[ShipAddress_City] [varchar](255) NULL,
	[ShipAddress_State] [varchar](255) NULL,
	[ShipAddress_PostalCode] [varchar](255) NULL,
	[ShipAddress_Country] [varchar](255) NULL,
	[ShipAddress_Note] [varchar](255) NULL,
	[PrintAs] [varchar](255) NULL,
	[Phone] [varchar](255) NULL,
	[Mobile] [varchar](255) NULL,
	[Pager] [varchar](255) NULL,
	[AltPhone] [varchar](255) NULL,
	[Fax] [varchar](255) NULL,
	[Email] [varchar](255) NULL,
	[Contact] [varchar](255) NULL,
	[AltContact] [varchar](255) NULL,
	[CustomerTypeRef_ListID] [varchar](255) NULL,
	[CustomerTypeRef_FullName] [varchar](255) NULL,
	[TermsRef_ListID] [varchar](255) NULL,
	[TermsRef_FullName] [varchar](255) NULL,
	[SalesRepRef_ListID] [varchar](255) NULL,
	[SalesRepRef_FullName] [varchar](255) NULL,
	[Balance] [float] NULL,
	[TotalBalance] [float] NULL,
	[SalesTaxCodeRef_ListID] [varchar](255) NULL,
	[SalesTaxCodeRef_FullName] [varchar](255) NULL,
	[ItemSalesTaxRef_ListID] [varchar](255) NULL,
	[ItemSalesTaxRef_FullName] [varchar](255) NULL,
	[SalesTaxCountry] [varchar](255) NULL,
	[ResaleNumber] [varchar](255) NULL,
	[AccountNumber] [varchar](255) NULL,
	[CreditLimit] [float] NULL,
	[PreferredPaymentMethodRef_ListID] [varchar](255) NULL,
	[PreferredPaymentMethodRef_FullName] [varchar](255) NULL,
	[CreditCardNumber] [varchar](255) NULL,
	[ExpirationMonth] [int] NULL,
	[ExpirationYear] [int] NULL,
	[NameOnCard] [varchar](255) NULL,
	[CreditCardAddress] [varchar](255) NULL,
	[CreditCardPostalCode] [varchar](255) NULL,
	[JobStatus] [varchar](255) NULL,
	[JobStartDate] [datetime] NULL,
	[JobProjectedEndDate] [datetime] NULL,
	[JobEndDate] [datetime] NULL,
	[JobDesc] [varchar](255) NULL,
	[JobTypeRef_ListID] [varchar](255) NULL,
	[JobTypeRef_FullName] [varchar](255) NULL,
	[Notes] [varchar](2000) NULL,
	[PriceLevelRef_ListID] [varchar](255) NULL,
	[PriceLevelRef_FullName] [varchar](255) NULL,
	[TaxRegistrationNumber] [varchar](255) NULL,
	[CurrencyRef_ListID] [varchar](255) NULL,
	[CurrencyRef_FullName] [varchar](255) NULL,
	[IsStatementWithParent] [bit] NULL,
	[DeliveryMethod] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[currency]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[currency](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[CurrencyCode] [varchar](255) NULL,
	[ThousandSeparator] [varchar](255) NULL,
	[ThousandSeparatorGrouping] [varchar](255) NULL,
	[DecimalPlaces] [varchar](255) NULL,
	[DecimalSeparator] [varchar](255) NULL,
	[IsUserDefinedCurrency] [bit] NULL,
	[ExchangeRate] [float] NULL,
	[AsOfDate] [datetime] NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[creditmemolinegroupdetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[creditmemolinegroupdetail](
	[TxnLineID] [varchar](255) NULL,
	[ItemGroupRef_ListID] [varchar](255) NULL,
	[ItemGroupRef_FullName] [varchar](255) NULL,
	[Desc] [varchar](255) NULL,
	[Quantity] [varchar](255) NULL,
	[IsPrintItemsInGroup] [bit] NULL,
	[TotalAmount] [float] NULL,
	[ServiceDate] [datetime] NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[creditmemolinedetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[creditmemolinedetail](
	[TxnLineID] [varchar](255) NULL,
	[ItemRef_ListID] [varchar](255) NULL,
	[ItemRef_FullName] [varchar](255) NULL,
	[Desc] [varchar](max) NULL,
	[Quantity] [varchar](255) NULL,
	[UnitOfMeasure] [varchar](255) NULL,
	[OverrideUOMSetRef_ListID] [varchar](255) NULL,
	[OverrideUOMSetRef_FullName] [varchar](255) NULL,
	[Rate] [varchar](255) NULL,
	[RatePercent] [varchar](255) NULL,
	[ClassRef_ListID] [varchar](255) NULL,
	[ClassRef_FullName] [varchar](255) NULL,
	[Amount] [float] NULL,
	[InventorySiteRef_ListID] [varchar](255) NULL,
	[InventorySiteRef_FullName] [varchar](255) NULL,
	[ServiceDate] [datetime] NULL,
	[SalesTaxCodeRef_ListID] [varchar](255) NULL,
	[SalesTaxCodeRef_FullName] [varchar](255) NULL,
	[Other1] [varchar](255) NULL,
	[Other2] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[creditmemo]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[creditmemo](
	[TxnID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[TxnNumber] [int] NULL,
	[CustomerRef_ListID] [varchar](255) NULL,
	[CustomerRef_FullName] [varchar](255) NULL,
	[ClassRef_ListID] [varchar](255) NULL,
	[ClassRef_FullName] [varchar](255) NULL,
	[ARAccountRef_ListID] [varchar](255) NULL,
	[ARAccountRef_FullName] [varchar](255) NULL,
	[TemplateRef_ListID] [varchar](255) NULL,
	[TemplateRef_FullName] [varchar](255) NULL,
	[TxnDate] [datetime] NULL,
	[RefNumber] [varchar](255) NULL,
	[BillAddress_Addr1] [varchar](255) NULL,
	[BillAddress_Addr2] [varchar](255) NULL,
	[BillAddress_Addr3] [varchar](255) NULL,
	[BillAddress_Addr4] [varchar](255) NULL,
	[BillAddress_Addr5] [varchar](255) NULL,
	[BillAddress_City] [varchar](255) NULL,
	[BillAddress_State] [varchar](255) NULL,
	[BillAddress_PostalCode] [varchar](255) NULL,
	[BillAddress_Country] [varchar](255) NULL,
	[BillAddress_Note] [varchar](255) NULL,
	[ShipAddress_Addr1] [varchar](255) NULL,
	[ShipAddress_Addr2] [varchar](255) NULL,
	[ShipAddress_Addr3] [varchar](255) NULL,
	[ShipAddress_Addr4] [varchar](255) NULL,
	[ShipAddress_Addr5] [varchar](255) NULL,
	[ShipAddress_City] [varchar](255) NULL,
	[ShipAddress_State] [varchar](255) NULL,
	[ShipAddress_PostalCode] [varchar](255) NULL,
	[ShipAddress_Country] [varchar](255) NULL,
	[ShipAddress_Note] [varchar](255) NULL,
	[IsPending] [bit] NULL,
	[PONumber] [varchar](255) NULL,
	[TermsRef_ListID] [varchar](255) NULL,
	[TermsRef_FullName] [varchar](255) NULL,
	[DueDate] [datetime] NULL,
	[SalesRepRef_ListID] [varchar](255) NULL,
	[SalesRepRef_FullName] [varchar](255) NULL,
	[FOB] [varchar](255) NULL,
	[ShipDate] [datetime] NULL,
	[ShipMethodRef_ListID] [varchar](255) NULL,
	[ShipMethodRef_FullName] [varchar](255) NULL,
	[Subtotal] [float] NULL,
	[ItemSalesTaxRef_ListID] [varchar](255) NULL,
	[ItemSalesTaxRef_FullName] [varchar](255) NULL,
	[SalesTaxPercentage] [varchar](255) NULL,
	[SalesTaxTotal] [float] NULL,
	[TotalAmount] [float] NULL,
	[CreditRemaining] [float] NULL,
	[CurrencyRef_ListID] [varchar](255) NULL,
	[CurrencyRef_FullName] [varchar](255) NULL,
	[ExchangeRate] [float] NULL,
	[CreditRemainingInHomeCurrency] [float] NULL,
	[Memo] [varchar](max) NULL,
	[CustomerMsgRef_ListID] [varchar](255) NULL,
	[CustomerMsgRef_FullName] [varchar](255) NULL,
	[IsToBePrinted] [bit] NULL,
	[IsToBeEmailed] [bit] NULL,
	[IsTaxIncluded] [bit] NULL,
	[CustomerSalesTaxCodeRef_ListID] [varchar](255) NULL,
	[CustomerSalesTaxCodeRef_FullName] [varchar](255) NULL,
	[Other] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[creditcardcredit]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[creditcardcredit](
	[TxnID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[TxnNumber] [int] NULL,
	[AccountRef_ListID] [varchar](255) NULL,
	[AccountRef_FullName] [varchar](255) NULL,
	[PayeeEntityRef_ListID] [varchar](255) NULL,
	[PayeeEntityRef_FullName] [varchar](255) NULL,
	[TxnDate] [datetime] NULL,
	[Amount] [float] NULL,
	[CurrencyRef_ListID] [varchar](255) NULL,
	[CurrencyRef_FullName] [varchar](255) NULL,
	[ExchangeRate] [float] NULL,
	[AmountInHomeCurrency] [float] NULL,
	[RefNumber] [varchar](255) NULL,
	[Memo] [varchar](max) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[creditcardcharge]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[creditcardcharge](
	[TxnID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[TxnNumber] [int] NULL,
	[AccountRef_ListID] [varchar](255) NULL,
	[AccountRef_FullName] [varchar](255) NULL,
	[PayeeEntityRef_ListID] [varchar](255) NULL,
	[PayeeEntityRef_FullName] [varchar](255) NULL,
	[TxnDate] [datetime] NULL,
	[Amount] [float] NULL,
	[CurrencyRef_ListID] [varchar](255) NULL,
	[CurrencyRef_FullName] [varchar](255) NULL,
	[ExchangeRate] [float] NULL,
	[AmountInHomeCurrency] [float] NULL,
	[RefNumber] [varchar](255) NULL,
	[Memo] [varchar](max) NULL,
	[IsTaxIncluded] [bit] NULL,
	[SalesTaxCodeRef_ListID] [varchar](255) NULL,
	[SalesTaxCodeRef_FullName] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[componentitemlinedetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[componentitemlinedetail](
	[ItemRef_ListID] [varchar](255) NULL,
	[ItemRef_FullName] [varchar](255) NULL,
	[InventorySiteRef_ListID] [varchar](255) NULL,
	[InventorySiteRef_FullName] [varchar](255) NULL,
	[Desc] [varchar](255) NULL,
	[QuantityOnHand] [varchar](255) NULL,
	[QuantityNeeded] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[company]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[company](
	[CompanyName] [varchar](255) NULL,
	[LegalCompanyName] [varchar](255) NULL,
	[Address_Addr1] [varchar](255) NULL,
	[Address_Addr2] [varchar](255) NULL,
	[Address_Addr3] [varchar](255) NULL,
	[Address_Addr4] [varchar](255) NULL,
	[Address_Addr5] [varchar](255) NULL,
	[Address_City] [varchar](255) NULL,
	[Address_State] [varchar](255) NULL,
	[Address_PostalCode] [varchar](255) NULL,
	[Address_Country] [varchar](255) NULL,
	[Address_Note] [varchar](255) NULL,
	[LegalAddress_Addr1] [varchar](255) NULL,
	[LegalAddress_Addr2] [varchar](255) NULL,
	[LegalAddress_Addr3] [varchar](255) NULL,
	[LegalAddress_Addr4] [varchar](255) NULL,
	[LegalAddress_Addr5] [varchar](255) NULL,
	[LegalAddress_City] [varchar](255) NULL,
	[LegalAddress_State] [varchar](255) NULL,
	[LegalAddress_PostalCode] [varchar](255) NULL,
	[LegalAddress_Country] [varchar](255) NULL,
	[LegalAddress_Note] [varchar](255) NULL,
	[CompanyAddressForCustomer_Addr1] [varchar](255) NULL,
	[CompanyAddressForCustomer_Addr2] [varchar](255) NULL,
	[CompanyAddressForCustomer_Addr3] [varchar](255) NULL,
	[CompanyAddressForCustomer_Addr4] [varchar](255) NULL,
	[CompanyAddressForCustomer_Addr5] [varchar](255) NULL,
	[CompanyAddressForCustomer_City] [varchar](255) NULL,
	[CompanyAddressForCustomer_State] [varchar](255) NULL,
	[CompanyAddressForCustomer_PostalCode] [varchar](255) NULL,
	[CompanyAddressForCustomer_Country] [varchar](255) NULL,
	[CompanyAddressForCustomer_Note] [varchar](255) NULL,
	[Phone] [varchar](255) NULL,
	[Fax] [varchar](255) NULL,
	[Email] [varchar](255) NULL,
	[CompanyEmailForCustomer] [varchar](255) NULL,
	[CompanyWebSite] [varchar](255) NULL,
	[FirstMonthFiscalYear] [varchar](255) NULL,
	[FirstMonthIncomeTaxYear] [varchar](255) NULL,
	[CompanyType] [varchar](255) NULL,
	[EIN] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[COGSReport]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[COGSReport](
	[TxnNumber] [int] NULL,
	[Type] [varchar](255) NULL,
	[Date] [datetime] NULL,
	[RefNumber] [varchar](250) NULL,
	[PONumber] [varchar](250) NULL,
	[Customer_ListID] [varchar](250) NULL,
	[Name] [varchar](250) NULL,
	[SourceName] [varchar](250) NULL,
	[Memo] [varchar](max) NULL,
	[ShipDate] [datetime] NULL,
	[DeliveryDate] [datetime] NULL,
	[FOB] [varchar](250) NULL,
	[ShipMethod] [varchar](250) NULL,
	[Item_ListID] [varchar](250) NULL,
	[Item] [varchar](250) NULL,
	[Account_ListID] [varchar](250) NULL,
	[Account] [varchar](250) NULL,
	[Class_ListID] [varchar](255) NULL,
	[Class] [varchar](250) NULL,
	[SalesTaxCode] [varchar](250) NULL,
	[ClearedStatus] [varchar](250) NULL,
	[SplitAccount_ListID] [varchar](250) NULL,
	[SplitAccount] [varchar](250) NULL,
	[Quantity] [float] NULL,
	[Debit] [float] NULL,
	[Credit] [float] NULL,
	[ModifiedTime] [datetime] NULL,
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[class]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[class](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[FullName] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[ParentRef_ListID] [varchar](255) NULL,
	[ParentRef_FullName] [varchar](255) NULL,
	[Sublevel] [int] NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[check]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[check](
	[TxnID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[TxnNumber] [int] NULL,
	[AccountRef_ListID] [varchar](255) NULL,
	[AccountRef_FullName] [varchar](255) NULL,
	[PayeeEntityRef_ListID] [varchar](255) NULL,
	[PayeeEntityRef_FullName] [varchar](255) NULL,
	[RefNumber] [varchar](255) NULL,
	[TxnDate] [datetime] NULL,
	[Amount] [float] NULL,
	[CurrencyRef_ListID] [varchar](255) NULL,
	[CurrencyRef_FullName] [varchar](255) NULL,
	[ExchangeRate] [float] NULL,
	[AmountInHomeCurrency] [float] NULL,
	[Memo] [varchar](max) NULL,
	[Address_Addr1] [varchar](255) NULL,
	[Address_Addr2] [varchar](255) NULL,
	[Address_Addr3] [varchar](255) NULL,
	[Address_Addr4] [varchar](255) NULL,
	[Address_Addr5] [varchar](255) NULL,
	[Address_City] [varchar](255) NULL,
	[Address_State] [varchar](255) NULL,
	[Address_PostalCode] [varchar](255) NULL,
	[Address_Country] [varchar](255) NULL,
	[Address_Note] [varchar](255) NULL,
	[IsToBePrinted] [bit] NULL,
	[IsTaxIncluded] [bit] NULL,
	[SalesTaxCodeRef_ListID] [varchar](255) NULL,
	[SalesTaxCodeRef_FullName] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[charge]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[charge](
	[TxnID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[TxnNumber] [int] NULL,
	[CustomerRef_ListID] [varchar](255) NULL,
	[CustomerRef_FullName] [varchar](255) NULL,
	[TxnDate] [datetime] NULL,
	[RefNumber] [varchar](255) NULL,
	[ItemRef_ListID] [varchar](255) NULL,
	[ItemRef_FullName] [varchar](255) NULL,
	[InventorySiteRef_ListID] [varchar](255) NULL,
	[InventorySiteRef_FullName] [varchar](255) NULL,
	[Quantity] [varchar](255) NULL,
	[UnitOfMeasure] [varchar](255) NULL,
	[OverrideUOMSetRef_ListID] [varchar](255) NULL,
	[OverrideUOMSetRef_FullName] [varchar](255) NULL,
	[Rate] [varchar](255) NULL,
	[Amount] [float] NULL,
	[BalanceRemaining] [float] NULL,
	[Desc] [varchar](max) NULL,
	[ARAccountRef_ListID] [varchar](255) NULL,
	[ARAccountRef_FullName] [varchar](255) NULL,
	[ClassRef_ListID] [varchar](255) NULL,
	[ClassRef_FullName] [varchar](255) NULL,
	[BilledDate] [datetime] NULL,
	[DueDate] [datetime] NULL,
	[IsPaid] [bit] NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cashbackinfodetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cashbackinfodetail](
	[TxnLineID] [varchar](255) NULL,
	[AccountRef_ListID] [varchar](255) NULL,
	[AccountRef_FullName] [varchar](255) NULL,
	[Memo] [varchar](255) NULL,
	[Amount] [float] NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[buildassembly]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[buildassembly](
	[TxnID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[TxnNumber] [int] NULL,
	[ItemInventoryAssemblyRef_ListID] [varchar](255) NULL,
	[ItemInventoryAssemblyRef_FullName] [varchar](255) NULL,
	[InventorySiteRef_ListID] [varchar](255) NULL,
	[InventorySiteRef_FullName] [varchar](255) NULL,
	[TxnDate] [datetime] NULL,
	[RefNumber] [varchar](255) NULL,
	[Memo] [varchar](255) NULL,
	[IsPending] [bit] NULL,
	[QuantityToBuild] [varchar](255) NULL,
	[QuantityCanBuild] [varchar](255) NULL,
	[QuantityOnHand] [varchar](255) NULL,
	[QuantityOnSalesOrder] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[billpaymentcreditcard]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[billpaymentcreditcard](
	[TxnID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[TxnNumber] [int] NULL,
	[PayeeEntityRef_ListID] [varchar](255) NULL,
	[PayeeEntityRef_FullName] [varchar](255) NULL,
	[APAccountRef_ListID] [varchar](255) NULL,
	[APAccountRef_FullName] [varchar](255) NULL,
	[TxnDate] [datetime] NULL,
	[CreditCardAccountRef_ListID] [varchar](255) NULL,
	[CreditCardAccountRef_FullName] [varchar](255) NULL,
	[Amount] [float] NULL,
	[CurrencyRef_ListID] [varchar](255) NULL,
	[CurrencyRef_FullName] [varchar](255) NULL,
	[ExchangeRate] [float] NULL,
	[AmountInHomeCurrency] [float] NULL,
	[RefNumber] [varchar](255) NULL,
	[Memo] [varchar](max) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[billpaymentcheck]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[billpaymentcheck](
	[TxnID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[TxnNumber] [int] NULL,
	[PayeeEntityRef_ListID] [varchar](255) NULL,
	[PayeeEntityRef_FullName] [varchar](255) NULL,
	[APAccountRef_ListID] [varchar](255) NULL,
	[APAccountRef_FullName] [varchar](255) NULL,
	[TxnDate] [datetime] NULL,
	[BankAccountRef_ListID] [varchar](255) NULL,
	[BankAccountRef_FullName] [varchar](255) NULL,
	[Amount] [float] NULL,
	[CurrencyRef_ListID] [varchar](255) NULL,
	[CurrencyRef_FullName] [varchar](255) NULL,
	[ExchangeRate] [float] NULL,
	[AmountInHomeCurrency] [float] NULL,
	[RefNumber] [varchar](255) NULL,
	[Memo] [varchar](max) NULL,
	[Address_Addr1] [varchar](255) NULL,
	[Address_Addr2] [varchar](255) NULL,
	[Address_Addr3] [varchar](255) NULL,
	[Address_Addr4] [varchar](255) NULL,
	[Address_Addr5] [varchar](255) NULL,
	[Address_City] [varchar](255) NULL,
	[Address_State] [varchar](255) NULL,
	[Address_PostalCode] [varchar](255) NULL,
	[Address_Country] [varchar](255) NULL,
	[Address_Note] [varchar](255) NULL,
	[IsToBePrinted] [bit] NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[billingrateperitemdetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[billingrateperitemdetail](
	[ItemRef_ListID] [varchar](255) NULL,
	[ItemRef_FullName] [varchar](255) NULL,
	[CustomRate] [varchar](255) NULL,
	[CustomRatePercent] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[billingrate]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[billingrate](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[BillingRateType] [varchar](255) NULL,
	[FixedBillingRate] [varchar](255) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[bill]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[bill](
	[TxnID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[TxnNumber] [int] NULL,
	[VendorRef_ListID] [varchar](255) NULL,
	[VendorRef_FullName] [varchar](255) NULL,
	[APAccountRef_ListID] [varchar](255) NULL,
	[APAccountRef_FullName] [varchar](255) NULL,
	[TxnDate] [datetime] NULL,
	[DueDate] [datetime] NULL,
	[AmountDue] [float] NULL,
	[CurrencyRef_ListID] [varchar](255) NULL,
	[CurrencyRef_FullName] [varchar](255) NULL,
	[ExchangeRate] [float] NULL,
	[AmountDueInHomeCurrency] [float] NULL,
	[RefNumber] [varchar](255) NULL,
	[TermsRef_ListID] [varchar](255) NULL,
	[TermsRef_FullName] [varchar](255) NULL,
	[Memo] [varchar](255) NULL,
	[IsTaxIncluded] [bit] NULL,
	[SalesTaxCodeRef_ListID] [varchar](255) NULL,
	[SalesTaxCodeRef_FullName] [varchar](255) NULL,
	[IsPaid] [bit] NULL,
	[OpenAmount] [float] NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[arrefundcreditcard]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[arrefundcreditcard](
	[TxnID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[TxnNumber] [int] NULL,
	[CustomerRef_ListID] [varchar](255) NULL,
	[CustomerRef_FullName] [varchar](255) NULL,
	[RefundFromAccountRef_ListID] [varchar](255) NULL,
	[RefundFromAccountRef_FullName] [varchar](255) NULL,
	[ARAccountRef_ListID] [varchar](255) NULL,
	[ARAccountRef_FullName] [varchar](255) NULL,
	[TxnDate] [datetime] NULL,
	[RefNumber] [varchar](255) NULL,
	[TotalAmount] [float] NULL,
	[Address_Addr1] [varchar](255) NULL,
	[Address_Addr2] [varchar](255) NULL,
	[Address_Addr3] [varchar](255) NULL,
	[Address_Addr4] [varchar](255) NULL,
	[Address_Addr5] [varchar](255) NULL,
	[Address_City] [varchar](255) NULL,
	[Address_State] [varchar](255) NULL,
	[Address_PostalCode] [varchar](255) NULL,
	[Address_Country] [varchar](255) NULL,
	[Address_Note] [varchar](255) NULL,
	[AddressBlock_Addr1] [varchar](255) NULL,
	[AddressBlock_Addr2] [varchar](255) NULL,
	[AddressBlock_Addr3] [varchar](255) NULL,
	[AddressBlock_Addr4] [varchar](255) NULL,
	[AddressBlock_Addr5] [varchar](255) NULL,
	[PaymentMethodRef_ListID] [varchar](255) NULL,
	[PaymentMethodRef_FullName] [varchar](255) NULL,
	[Memo] [varchar](max) NULL,
	[CreditCardTxnInfo] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[appliedtotxndetail]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[appliedtotxndetail](
	[TxnID] [varchar](255) NULL,
	[TxnType] [varchar](255) NULL,
	[TxnDate] [datetime] NULL,
	[RefNumber] [varchar](255) NULL,
	[BalanceRemaining] [float] NULL,
	[Amount] [float] NULL,
	[DiscountAmount] [float] NULL,
	[DiscountAccountRef_ListID] [varchar](255) NULL,
	[DiscountAccountRef_FullName] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[IDKEY] [varchar](255) NULL,
	[GroupIDKEY] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[account]    Script Date: 05/28/2013 17:08:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[account](
	[ListID] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[EditSequence] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[FullName] [varchar](255) NULL,
	[IsActive] [bit] NULL,
	[ParentRef_ListID] [varchar](255) NULL,
	[ParentRef_FullName] [varchar](255) NULL,
	[Sublevel] [int] NULL,
	[AccountType] [varchar](255) NULL,
	[DetailAccountType] [varchar](255) NULL,
	[AccountNumber] [varchar](255) NULL,
	[BankNumber] [varchar](255) NULL,
	[LastCheckNumber] [varchar](255) NULL,
	[Desc] [varchar](max) NULL,
	[Balance] [float] NULL,
	[TotalBalance] [float] NULL,
	[CashFlowClassification] [varchar](255) NULL,
	[SpecialAccountType] [varchar](255) NULL,
	[SalesTaxCodeRef_ListID] [varchar](255) NULL,
	[SalesTaxCodeRef_FullName] [varchar](255) NULL,
	[IsTaxAccount] [bit] NULL,
	[TaxLineID] [int] NULL,
	[TaxLineInfo] [varchar](255) NULL,
	[CurrencyRef_ListID] [varchar](255) NULL,
	[CurrencyRef_FullName] [varchar](255) NULL,
	[CustomField1] [varchar](50) NULL,
	[CustomField2] [varchar](50) NULL,
	[CustomField3] [varchar](50) NULL,
	[CustomField4] [varchar](50) NULL,
	[CustomField5] [varchar](50) NULL,
	[CustomField6] [varchar](50) NULL,
	[CustomField7] [varchar](50) NULL,
	[CustomField8] [varchar](50) NULL,
	[CustomField9] [varchar](50) NULL,
	[CustomField10] [varchar](50) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
