USE [EOAPIv2]
GO
/****** Object:  Table [dbo].[AuthTokens_Members]    Script Date: 02/18/2014 11:46:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuthTokens_Members](
	[Token] [uniqueidentifier] NOT NULL,
	[EncToken] [nvarchar](50) NULL,
	[MemberId] [uniqueidentifier] NOT NULL,
	[ClientId] [nvarchar](50) NOT NULL,
	[ExpiresOn] [datetime] NOT NULL,
 CONSTRAINT [PK_AuthTokens_Members] PRIMARY KEY CLUSTERED 
(
	[Token] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AuthTokens]    Script Date: 02/18/2014 11:46:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuthTokens](
	[Token] [uniqueidentifier] NOT NULL,
	[EncToken] [nvarchar](50) NULL,
	[MemberId] [uniqueidentifier] NOT NULL,
	[ClientId] [nvarchar](50) NOT NULL,
	[ExpiresOn] [datetime] NOT NULL,
 CONSTRAINT [PK_AuthTokens] PRIMARY KEY CLUSTERED 
(
	[Token] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Feeds]    Script Date: 02/18/2014 11:46:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Feeds](
	[FeedId] [int] IDENTITY(1,1) NOT NULL,
	[FeedName] [nvarchar](255) NOT NULL,
	[FeedUrl] [nvarchar](255) NOT NULL,
	[FeedType] [nvarchar](20) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Emails]    Script Date: 02/18/2014 11:46:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Emails](
	[EmailId] [int] IDENTITY(1,1) NOT NULL,
	[EmailName] [nvarchar](50) NOT NULL,
	[EmailFrom] [nvarchar](50) NOT NULL,
	[EmailTo] [nvarchar](255) NOT NULL,
	[EmailCC] [nvarchar](255) NULL,
	[EmailSubject] [nvarchar](255) NULL,
	[EmailText] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Members]    Script Date: 02/18/2014 11:46:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Members](
	[MemberId] [uniqueidentifier] NOT NULL,
	[IsLocked] [bit] NOT NULL,
	[LockedOn] [datetime] NULL,
	[LockedUntil] [datetime] NULL,
	[LockedReason] [nvarchar](255) NULL,
	[LastLogin] [date] NULL,
	[LastIP] [nvarchar](50) NULL,
	[FailedAttempts] [int] NOT NULL,
 CONSTRAINT [PK_Members] PRIMARY KEY CLUSTERED 
(
	[MemberId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LogContent]    Script Date: 02/18/2014 11:46:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogContent](
	[LogContentId] [int] IDENTITY(1,1) NOT NULL,
	[RequestContentId] [uniqueidentifier] NOT NULL,
	[ContentName] [nvarchar](50) NOT NULL,
	[ContentValue] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_LogContent] PRIMARY KEY CLUSTERED 
(
	[LogContentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Log]    Script Date: 02/18/2014 11:46:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Log](
	[LogId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[Action] [nvarchar](50) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Error] [text] NULL,
	[Results] [text] NULL,
	[IP] [nvarchar](50) NOT NULL,
	[MemberId] [uniqueidentifier] NULL,
	[RequestContentId] [uniqueidentifier] NULL,
	[RequestUri] [nvarchar](2000) NULL,
	[RequestAuthHeader] [nvarchar](2000) NULL,
 CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED 
(
	[LogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 30) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[SelectToken]    Script Date: 02/18/2014 11:49:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SelectToken]
	@token uniqueidentifier
AS

	SELECT Token, DateExpires, DateGiven, DateUsed FROM Tokens WHERE Token = @token AND DateExpires > GETDATE()
GO
/****** Object:  StoredProcedure [dbo].[DeleteToken]    Script Date: 02/18/2014 11:49:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteToken]
	@token uniqueidentifier
AS

	DELETE FROM Tokens WHERE Token = @token
GO
/****** Object:  StoredProcedure [dbo].[AddToken]    Script Date: 02/18/2014 11:49:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddToken]
	@token uniqueidentifier,
	@dateGiven	datetime,
	@dateUsed	datetime,
	@dateExpires datetime
AS

	INSERT INTO Tokens (Token, DateGiven, DateUsed, DateExpires) VALUES (@token, @dateGiven, @dateUsed, @dateExpires)
	
	--DELETE FROM Tokens WHERE DateExpires < DateAdd(hh, 1, GETDATE())
GO
/****** Object:  Table [dbo].[UsersChapters]    Script Date: 02/18/2014 11:46:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsersChapters](
	[UserId] [uniqueidentifier] NOT NULL,
	[ChapterId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_UsersChapters] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[ChapterId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CRM Chapter Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UsersChapters', @level2type=N'COLUMN',@level2name=N'ChapterId'
GO
/****** Object:  Table [dbo].[Users]    Script Date: 02/18/2014 11:46:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users](
	[UserId] [uniqueidentifier] NOT NULL,
	[EncKey] [varbinary](50) NULL,
	[EncKeyString] [nvarchar](255) NULL,
	[ClientId] [nvarchar](50) NULL,
	[Version] [decimal](18, 0) NOT NULL,
	[IsLocked] [bit] NOT NULL,
	[IsValidForMemberAuth] [bit] NOT NULL,
	[AuthUrl] [nvarchar](500) NULL,
	[IsLimitByChapter] [bit] NOT NULL,
	[IsAuthForInternal] [bit] NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 30) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[UpdateToken]    Script Date: 02/18/2014 11:49:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateToken]
	@token uniqueidentifier,
	@dateUsed	datetime,
	@dateExpires datetime
AS

	UPDATE Tokens SET DateUsed = @dateUsed, DateExpires = @dateExpires WHERE Token = @token
GO
/****** Object:  StoredProcedure [dbo].[UpdateFeed]    Script Date: 02/18/2014 11:49:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateFeed]
	@feedId int,
	@feedName nvarchar(255),
	@feedUrl nvarchar(255),
	@feedType nvarchar(20)
AS

 UPDATE Feeds SET FeedName = @feedName, FeedUrl = @feedUrl, FeedType = @feedType
 WHERE FeedId = @feedId
GO
/****** Object:  StoredProcedure [dbo].[UnLockUser]    Script Date: 02/18/2014 11:49:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UnLockUser]
	@memberId uniqueidentifier
	
AS

	UPDATE Users SET
		IsLocked = 0
	WHERE
		UserId = @memberId
GO
/****** Object:  StoredProcedure [dbo].[UpdateUser]    Script Date: 02/18/2014 11:49:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateUser]
	@memberId uniqueidentifier,
	@clientId nvarchar(50),
	@version decimal(18,0),
	@isLocked bit = 0
	
AS

	UPDATE Users SET
		ClientId = @clientId,
		[Version] = @version,
		IsLocked = @isLocked
	WHERE
		UserId = @memberId
GO
/****** Object:  StoredProcedure [dbo].[SwitchValidMemberAuth]    Script Date: 02/18/2014 11:49:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SwitchValidMemberAuth]
	@memberId uniqueidentifier
	
AS

	UPDATE Users SET
		IsValidForMemberAuth = CASE WHEN IsValidForMemberAuth = 0 THEN 1 ELSE 0 END
	WHERE
		UserId = @memberId
GO
/****** Object:  StoredProcedure [dbo].[SwitchLock]    Script Date: 02/18/2014 11:49:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SwitchLock]
	@memberId uniqueidentifier
	
AS

	UPDATE Users SET
		IsLocked = CASE WHEN IsLocked = 0 THEN 1 ELSE 0 END
	WHERE
		UserId = @memberId
GO
/****** Object:  StoredProcedure [dbo].[AddLogContent]    Script Date: 02/18/2014 11:49:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddLogContent]
	@RequestContentId uniqueidentifier,
	@ContentName nvarchar(50),
	@ContentValue nvarchar(max)
AS

	INSERT INTO [LogContent] (RequestContentId, ContentName, ContentValue) VALUES (@RequestContentId, @ContentName, @ContentValue)
GO
/****** Object:  StoredProcedure [dbo].[AddLog]    Script Date: 02/18/2014 11:49:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddLog]
	@userId uniqueidentifier,
	@memberId uniqueidentifier,
	@action nvarchar(50),
	@ip nvarchar(50),
	@date datetime,	
	@requestContentId uniqueidentifier,
	@requestUri nvarchar(2000),
	@requestAuthHeader nvarchar(2000),
	@error text = null,
	@results text = null
AS

	INSERT INTO [Log] (UserId, MemberId, [Action], IP, [Date], [Error], Results, RequestContentId, RequestUri, RequestAuthHeader) VALUES (@userId, @memberId, @action, @ip, @date, @error, @results, @RequestContentId, @RequestUri, @RequestAuthHeader)
GO
/****** Object:  StoredProcedure [dbo].[AddFeed]    Script Date: 02/18/2014 11:49:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddFeed]
	@feedName nvarchar(255),
	@feedUrl nvarchar(255),
	@feedType nvarchar(20)
AS

 INSERT INTO Feeds(FeedName, FeedUrl, FeedType)
 VALUES (@feedName, @feedUrl, @feedType)
 
 SELECT @@IDENTITY
GO
/****** Object:  StoredProcedure [dbo].[LockUser]    Script Date: 02/18/2014 11:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[LockUser]
	@memberId uniqueidentifier
	
AS

	UPDATE Users SET
		IsLocked = 1
	WHERE
		UserId = @memberId
GO
/****** Object:  StoredProcedure [dbo].[InsertUserKey]    Script Date: 02/18/2014 11:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertUserKey]
	@memberId uniqueidentifier,
	@encKey varbinary(50),
	@encKeyString nvarchar(255)
	
AS

	UPDATE Users SET
		EncKey = @encKey,
		EncKeyString = @encKeyString
	WHERE
		UserId = @memberId
GO
/****** Object:  StoredProcedure [dbo].[GetUserIdFromClientId]    Script Date: 02/18/2014 11:49:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetUserIdFromClientId]
	@clientId nvarchar(50)
AS

	SELECT
		UserId
	FROM Users
	WHERE ClientId = @clientId
GO
/****** Object:  StoredProcedure [dbo].[GetStatus]    Script Date: 02/18/2014 11:49:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetStatus]
	@clientId nvarchar(50)
	
AS

	SELECT
		IsLocked,
		IsValidForMemberAuth
	FROM
		Users
	WHERE
		ClientId = @clientId
GO
/****** Object:  StoredProcedure [dbo].[GetKey]    Script Date: 02/18/2014 11:49:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetKey]
	@clientId nvarchar(50),
	@version decimal
AS

	SELECT
		UserId,
		EncKey,
		EncKeyString,
		ClientId,
		[Version]
	FROM Users
	WHERE ClientId = @clientId AND IsLocked = 0
GO
/****** Object:  StoredProcedure [dbo].[GetFeeds]    Script Date: 02/18/2014 11:49:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetFeeds]

AS

 SELECT
	FeedId,
	FeedName,
	FeedType,
	FeedUrl
 FROM Feeds
GO
/****** Object:  StoredProcedure [dbo].[GetFeed]    Script Date: 02/18/2014 11:49:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetFeed]
	@feedId int
AS

 SELECT
	FeedId,
	FeedName,
	FeedType,
	FeedUrl
 FROM Feeds
 WHERE FeedId = @feedId
GO
/****** Object:  StoredProcedure [dbo].[GetEmailTemplate]    Script Date: 02/18/2014 11:49:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetEmailTemplate]
	@emailName nvarchar(50)
AS

	SELECT
		EmailId,
		EmailName,
		EmailFrom,
		EmailTo,
		EmailCC,
		EmailSubject,
		EmailText
	FROM
		Emails
	WHERE EmailName = @emailName
GO
/****** Object:  StoredProcedure [dbo].[DeleteFeed]    Script Date: 02/18/2014 11:49:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteFeed]
	@feedId int
AS

 DELETE FROM Feeds
 WHERE FeedId = @feedId
GO
/****** Object:  StoredProcedure [dbo].[AuthSetEncToken]    Script Date: 02/18/2014 11:49:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AuthSetEncToken]
 @Token uniqueidentifier,
 @EncToken nvarchar(50)
AS

	UPDATE AuthTokens SET EncToken = @EncToken WHERE Token = @Token
GO
/****** Object:  StoredProcedure [dbo].[AuthGetNewToken_Members]    Script Date: 02/18/2014 11:49:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AuthGetNewToken_Members]
 @ClientId nvarchar(50),
 @MemberId uniqueidentifier,
 @ExpiresOn datetime
AS

DECLARE @Token uniqueidentifier
DECLARE @Url nvarchar(500)

IF (SELECT COUNT(*) FROM AuthTokens_Members WHERE ClientId = @ClientId AND MemberId = @MemberId AND ExpiresOn > GETDATE()) = 0 
BEGIN
	SELECT @Token = NEWID()

	INSERT INTO AuthTokens_Members (Token, MemberId, ClientId, ExpiresOn) VALUES (@Token, @MemberId, @ClientId, @ExpiresOn)
END
ELSE
BEGIN
	SELECT @Token = Token FROM AuthTokens_Members WHERE ClientId = @ClientId AND MemberId = @MemberId AND ExpiresOn > GETDATE() 
	
	UPDATE AuthTokens_Members SET ExpiresOn = @ExpiresOn WHERE Token = @Token AND ClientId = @ClientId AND MemberId = @MemberId

END

/*take out following line for production*/
--UPDATE AuthTokens SET ExpiresOn = DATEADD(d,10,ExpiresOn) WHERE Token = @Token AND ClientId = @ClientId-- AND MemberId = @MemberId

SELECT @Token AS Token, LOWER(c.eo_LogonName) AS userName, @ExpiresOn AS ExpiresOn, @Url AS AuthUrl FROM EOCRM_MSCRM.dbo.Contact c WHERE c.eo_ExternalUserID = @MemberId
GO
/****** Object:  StoredProcedure [dbo].[AuthGetNewToken]    Script Date: 02/18/2014 11:49:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AuthGetNewToken]
 @ClientId nvarchar(50),
 @MemberId uniqueidentifier,
 @ExpiresOn datetime
AS

DECLARE @Token uniqueidentifier
DECLARE @Url nvarchar(500)

IF (SELECT COUNT(*) FROM AuthTokens WHERE ClientId = @ClientId AND MemberId = @MemberId AND ExpiresOn > GETDATE()) = 0 
BEGIN
	SELECT @Token = NEWID(), @Url = AuthUrl FROM Users WHERE ClientId = @ClientId

	INSERT INTO AuthTokens (Token, MemberId, ClientId, ExpiresOn) VALUES (@Token, @MemberId, @ClientId, @ExpiresOn)
END
ELSE
BEGIN
	SELECT @Token = Token FROM AuthTokens WHERE ClientId = @ClientId AND MemberId = @MemberId AND ExpiresOn > GETDATE() 
	SELECT @Url = AuthUrl FROM Users WHERE ClientId = @ClientId
	
	IF (LOWER(CONVERT(nvarchar(50),@MemberId)) LIKE '%d3330075-38ea-43ee-9624-404dbd6fb97e%')
	BEGIN
		UPDATE AuthTokens SET ExpiresOn = @ExpiresOn WHERE Token = @Token AND ClientId = @ClientId AND MemberId = @MemberId
	END
END

/*take out following line for production*/
UPDATE AuthTokens SET ExpiresOn = DATEADD(d,10,ExpiresOn) WHERE Token = @Token AND ClientId = @ClientId-- AND MemberId = @MemberId

SELECT @Token AS Token, LOWER(c.eo_LogonName) AS userName, @ExpiresOn AS ExpiresOn, @Url AS AuthUrl FROM EOCRM_MSCRM.dbo.Contact c WHERE c.eo_ExternalUserID = @MemberId
GO
/****** Object:  StoredProcedure [dbo].[AuthCheckToken_MembersNoSlide]    Script Date: 02/18/2014 11:49:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AuthCheckToken_MembersNoSlide]
 @ClientId nvarchar(50),
 @Token uniqueidentifier
AS

IF (SELECT COUNT(*) FROM AuthTokens_Members WHERE ClientId = @ClientId AND Token = @Token AND GETDATE() <= ExpiresOn) > 0
BEGIN
	
	DECLARE @formattedMemberGuid nvarchar(50)

	SELECT @formattedMemberGuid = convert(nvarchar(50),'{' + convert(nvarchar(50),a.MemberId) + '}') FROM AuthTokens_Members a WHERE Token = @Token 
	
	SELECT a.MemberId, c.eo_LogonName AS UserName, a.ExpiresOn FROM AuthTokens_Members a 
	LEFT OUTER JOIN EOCRM_MSCRM.dbo.Contact c ON convert(nvarchar(50),'{' + convert(nvarchar(50),a.MemberId) + '}') COLLATE DATABASE_DEFAULT = c.eo_ExternalUserID COLLATE DATABASE_DEFAULT
	 WHERE a.ClientId = @ClientId AND a.Token = @Token
END
GO
/****** Object:  StoredProcedure [dbo].[AuthCheckToken_Members]    Script Date: 02/18/2014 11:49:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AuthCheckToken_Members]
 @ClientId nvarchar(50),
 @Token uniqueidentifier,
 @NewExpiresOn datetime
AS

IF (SELECT COUNT(*) FROM AuthTokens_Members WHERE ClientId = @ClientId AND Token = @Token AND GETDATE() <= ExpiresOn) > 0
BEGIN

	UPDATE AuthTokens_Members SET ExpiresOn = @NewExpiresOn WHERE ClientId = @ClientId AND Token = @Token
	
	DECLARE @formattedMemberGuid nvarchar(50)

	SELECT @formattedMemberGuid = convert(nvarchar(50),'{' + convert(nvarchar(50),a.MemberId) + '}') FROM AuthTokens_Members a WHERE Token = @Token 
	
	SELECT a.MemberId, c.eo_LogonName AS UserName, ExpiresOn FROM AuthTokens_Members a 
	LEFT OUTER JOIN EOCRM_MSCRM.dbo.Contact c ON convert(nvarchar(50),'{' + convert(nvarchar(50),a.MemberId) + '}') COLLATE DATABASE_DEFAULT = c.eo_ExternalUserID COLLATE DATABASE_DEFAULT
	 WHERE a.ClientId = @ClientId AND a.Token = @Token
END
GO
/****** Object:  StoredProcedure [dbo].[AuthCheckToken]    Script Date: 02/18/2014 11:49:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AuthCheckToken]
 @ClientId nvarchar(50),
 @Token uniqueidentifier,
 @ExpiresOn datetime
AS

DECLARE @formattedMemberGuid nvarchar(50)

SELECT @formattedMemberGuid = convert(nvarchar(50),'{' + convert(nvarchar(50),a.MemberId) + '}') FROM AuthTokens a WHERE Token = @Token 

SELECT @formattedMemberGuid as MemberId, c.eo_LogonName AS UserName FROM AuthTokens a 
LEFT OUTER JOIN EOCRM_MSCRM.dbo.Contact c ON convert(nvarchar(50),'{' + convert(nvarchar(50),a.MemberId) + '}') COLLATE DATABASE_DEFAULT = c.eo_ExternalUserID COLLATE DATABASE_DEFAULT
 WHERE a.ClientId = @ClientId AND a.Token = @Token AND @ExpiresOn <= ExpiresOn
GO
/****** Object:  StoredProcedure [dbo].[AddUser]    Script Date: 02/18/2014 11:49:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddUser]
	@memberId uniqueidentifier,
	@encKey varbinary(50),
	@clientId nvarchar(50),
	@version decimal(18,0),
	@isLocked bit = 0
	
AS

	INSERT INTO Users (UserId, EncKey, ClientId, [Version], IsLocked)
	VALUES (@memberId, @encKey, @clientId, @version, @isLocked)
GO
/****** Object:  Default [DF_AuthTokens_Token]    Script Date: 02/18/2014 11:46:35 ******/
ALTER TABLE [dbo].[AuthTokens] ADD  CONSTRAINT [DF_AuthTokens_Token]  DEFAULT (newid()) FOR [Token]
GO
/****** Object:  Default [DF_AuthTokens_Token_Members]    Script Date: 02/18/2014 11:46:35 ******/
ALTER TABLE [dbo].[AuthTokens_Members] ADD  CONSTRAINT [DF_AuthTokens_Token_Members]  DEFAULT (newid()) FOR [Token]
GO
/****** Object:  Default [DF_Log_Date]    Script Date: 02/18/2014 11:46:35 ******/
ALTER TABLE [dbo].[Log] ADD  CONSTRAINT [DF_Log_Date]  DEFAULT (getdate()) FOR [Date]
GO
/****** Object:  Default [DF_Members_IsLocked]    Script Date: 02/18/2014 11:46:35 ******/
ALTER TABLE [dbo].[Members] ADD  CONSTRAINT [DF_Members_IsLocked]  DEFAULT ((0)) FOR [IsLocked]
GO
/****** Object:  Default [DF_Members_FailedAttempts]    Script Date: 02/18/2014 11:46:35 ******/
ALTER TABLE [dbo].[Members] ADD  CONSTRAINT [DF_Members_FailedAttempts]  DEFAULT ((0)) FOR [FailedAttempts]
GO
/****** Object:  Default [DF_Users_IsLocked]    Script Date: 02/18/2014 11:46:35 ******/
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_IsLocked]  DEFAULT ((0)) FOR [IsLocked]
GO
/****** Object:  Default [DF_Users_IsValidForMemberAuth]    Script Date: 02/18/2014 11:46:35 ******/
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_IsValidForMemberAuth]  DEFAULT ((0)) FOR [IsValidForMemberAuth]
GO
/****** Object:  Default [DF_Users_IsLimitByChapter]    Script Date: 02/18/2014 11:46:35 ******/
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_IsLimitByChapter]  DEFAULT ((0)) FOR [IsLimitByChapter]
GO
/****** Object:  Default [DF_Users_IsAuthForInternal]    Script Date: 02/18/2014 11:46:35 ******/
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_IsAuthForInternal]  DEFAULT ((0)) FOR [IsAuthForInternal]
GO
