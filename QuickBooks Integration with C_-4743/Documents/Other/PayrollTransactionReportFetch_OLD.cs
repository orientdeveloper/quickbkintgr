﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QBFC12Lib;
using System.Configuration;
using QBDataFetcher;
using QBDataFetcher.DAL;
using QBDataFetcher.Helpers;
using QBDataFetcher.Model;
using QBDataFetcher.Dal;
using QBDataFetcher.Logger;

namespace QBDataFetcher.Fetcher
{
    public class PayrollTransactionReportFetch : QBSession
    {
        string _qbfile;
        bool _nextchunk;
        IPayrollDetailReportQuery _IPayrollDetailReportQuery;
        List<PayrollTransactionReport> _PayrollTransactionReports;

        public PayrollTransactionReportFetch(string qbfile, Logger.ISynLogger _Logger, DateTime? lastmodified)
            : base(qbfile)
        {
            Logger = _Logger;
            _qbfile = qbfile;
            _nextchunk = true;
            _IPayrollDetailReportQuery = RequestMsgSet.AppendPayrollDetailReportQueryRq();
            _IPayrollDetailReportQuery.PayrollDetailReportType.SetValue(ENPayrollDetailReportType.pdrtPayrollTransactionDetail);

            _IPayrollDetailReportQuery.DisplayReport.SetValue(false);

            _IPayrollDetailReportQuery.ReportDetailLevelFilter.SetValue(ENReportDetailLevelFilter.rdlfAll);
            _IPayrollDetailReportQuery.ReportPostingStatusFilter.SetValue(ENReportPostingStatusFilter.rpsfEither);
            _IPayrollDetailReportQuery.ReportDetailLevelFilter.SetValue(ENReportDetailLevelFilter.rdlfAllExceptSummary);

            _IPayrollDetailReportQuery.IncludeColumnList.Add(ENIncludeColumn.icDate);
            _IPayrollDetailReportQuery.IncludeColumnList.Add(ENIncludeColumn.icTxnNumber);
            _IPayrollDetailReportQuery.IncludeColumnList.Add(ENIncludeColumn.icTxnType);
            _IPayrollDetailReportQuery.IncludeColumnList.Add(ENIncludeColumn.icName);
            _IPayrollDetailReportQuery.IncludeColumnList.Add(ENIncludeColumn.icSourceName);
            _IPayrollDetailReportQuery.IncludeColumnList.Add(ENIncludeColumn.icPayrollItem);
            _IPayrollDetailReportQuery.IncludeColumnList.Add(ENIncludeColumn.icWageBase);
            _IPayrollDetailReportQuery.IncludeColumnList.Add(ENIncludeColumn.icAccount);
            _IPayrollDetailReportQuery.IncludeColumnList.Add(ENIncludeColumn.icClass);
            _IPayrollDetailReportQuery.IncludeColumnList.Add(ENIncludeColumn.icBillingStatus);
            _IPayrollDetailReportQuery.IncludeColumnList.Add(ENIncludeColumn.icSplitAccount);
            _IPayrollDetailReportQuery.IncludeColumnList.Add(ENIncludeColumn.icQuantity);
            _IPayrollDetailReportQuery.IncludeColumnList.Add(ENIncludeColumn.icAmount);
            _IPayrollDetailReportQuery.IncludeColumnList.Add(ENIncludeColumn.icRefNumber);
            _IPayrollDetailReportQuery.IncludeColumnList.Add(ENIncludeColumn.icModifiedTime);

            _IPayrollDetailReportQuery.IncludeAccounts.SetValue(ENIncludeAccounts.iaAll);

            if (lastmodified.HasValue)
            {
                _IPayrollDetailReportQuery.ORReportModifiedDate.ReportModifiedDateRangeFilter.FromReportModifiedDate.SetValue(lastmodified.Value.Date);
                _IPayrollDetailReportQuery.ORReportModifiedDate.ReportModifiedDateRangeFilter.ToReportModifiedDate.SetValue(System.DateTime.Now.Date);
                _IPayrollDetailReportQuery.ORReportPeriod.ReportDateMacro.SetValue(ENReportDateMacro.rdmAll);
            }
            else
            {
                _IPayrollDetailReportQuery.ORReportPeriod.ReportDateMacro.SetValue(ENReportDateMacro.rdmAll);
            }
        }

        public List<PayrollTransactionReport> FetchNext()
        {
            try
            {
                _nextchunk = false;
                //LogFetchStart
                Logger.LogFetchStart(TableName.PayrollTransactionReport);

                _PayrollTransactionReports = new List<PayrollTransactionReport>();

                IMsgSetResponse responseMsgSet = SessionManager.DoRequests(RequestMsgSet);

                WalkPayrollDetailReportQueryRs(responseMsgSet);

                //LogFetchedRecords
                Logger.LogFetchedRecords(TableName.PayrollTransactionReport, _PayrollTransactionReports.Count());

                //LogFetchEnd
                Logger.LogFetchCompleted(TableName.PayrollTransactionReport);
                return _PayrollTransactionReports;

            }
            catch (Exception ex)
            {
                Logger.LogFetchError(ex.Message, TableName.PayrollTransactionReport, ex);
                throw ex;
            }
            finally
            {
                EndSession();
            }
        }

        public void WalkPayrollDetailReportQueryRs(IMsgSetResponse responseMsgSet)
        {
            if (responseMsgSet == null) return;

            IResponseList responseList = responseMsgSet.ResponseList;
            if (responseList == null) return;

            string repstring = responseMsgSet.ToXMLString();

            //StringReader reader = new StringReader(repstring);
            //XDocument xdoc = XDocument.Load(reader);

            //var responseData = from s in xdoc.Descendants("")
            //                   select new { };

            //if we sent only one request, there is only one response, we'll walk the list for this sample
            for (int i = 0; i < responseList.Count; i++)
            {
                IResponse response = responseList.GetAt(i);
                //check the status code of the response, 0=ok, >0 is warning
                //LogFetching
                Logger.LogFetching(TableName.PayrollTransactionReport, response.StatusCode, response.StatusMessage, response.StatusSeverity);
                if (response.StatusCode >= 0)
                {
                    //the request-specific response is in the details, make sure we have some
                    if (response.Detail != null)
                    {
                        //make sure the response is the type we're expecting
                        ENResponseType responseType = (ENResponseType)response.Type.GetValue();
                        if (responseType == ENResponseType.rtPayrollDetailReportQueryRs)
                        {
                            //upcast to more specific type here, this is safe because we checked with response.Type check above

                            IReportRet ReportRet = (IReportRet)response.Detail;
                            WalkReportRet(ReportRet);
                        }
                    }
                }
            }
        }

        public void WalkReportRet(IReportRet ReportRet)
        {
            try
            {
                if (ReportRet == null) return;
                if (ReportRet.ReportData != null)
                {
                    if (ReportRet.ReportData.ORReportDataList != null)
                    {
                        string Date = string.Empty;
                        int? TxnNumber = 0;
                        string RefNumber = string.Empty;
                        string TxnType = string.Empty;
                        string ModifiedTime = string.Empty;
                        for (int i56 = 0; i56 < ReportRet.ReportData.ORReportDataList.Count; i56++)
                        {
                            IORReportData ORReportData = ReportRet.ReportData.ORReportDataList.GetAt(i56);
                            PayrollTransactionReport _PayrollDetailReport = new PayrollTransactionReport();
                            if (ORReportData.DataRow != null)
                            {
                                if (ORReportData.DataRow.ColDataList != null)
                                {
                                    for (int i57 = 0; i57 < ORReportData.DataRow.ColDataList.Count; i57++)
                                    {
                                        IColData ColData = ORReportData.DataRow.ColDataList.GetAt(i57);

                                        if (ColData.colID.GetValue() == 2)
                                        {
                                            if (!ColData.value.GetValue().IsNull())
                                                Date = ColData.value.GetValue();
                                            _PayrollDetailReport.Date = DateTime.Parse(Date);
                                        }
                                        else if (!_PayrollDetailReport.Date.HasValue)
                                            _PayrollDetailReport.Date = DateTime.Parse(Date);

                                        if (ColData.colID.GetValue() == 3)
                                        {
                                            if (!ColData.colID.GetValue().IsNull())
                                                TxnNumber = int.Parse(ColData.value.GetValue());
                                            _PayrollDetailReport.TxnNumber = TxnNumber;
                                        }
                                        else if (_PayrollDetailReport.TxnNumber.IsNull())
                                            _PayrollDetailReport.TxnNumber = TxnNumber;

                                        if (ColData.colID.GetValue() == 4)
                                        {
                                            if (!ColData.colID.GetValue().IsNull())
                                                TxnType = ColData.value.GetValue();
                                            _PayrollDetailReport.TxnType = TxnType;
                                        }
                                        else if (_PayrollDetailReport.TxnType.IsNull())
                                            _PayrollDetailReport.TxnType = TxnType;
                                        if (ColData.colID.GetValue() == 5)
                                            _PayrollDetailReport.Name = ColData.value.GetValue();
                                        else if (_PayrollDetailReport.Name.IsNull())
                                            _PayrollDetailReport.Name = string.Empty;
                                        if (ColData.colID.GetValue() == 6)
                                            _PayrollDetailReport.SourceName = ColData.value.GetValue();
                                        else if (_PayrollDetailReport.SourceName.IsNull())
                                            _PayrollDetailReport.SourceName = string.Empty;
                                        if (ColData.colID.GetValue() == 7)
                                            _PayrollDetailReport.PayrollItem = ColData.value.GetValue();
                                        else if (_PayrollDetailReport.PayrollItem.IsNull())
                                            _PayrollDetailReport.PayrollItem = string.Empty;
                                        if (ColData.colID.GetValue() == 8)
                                            _PayrollDetailReport.WageBase = double.Parse(ColData.value.GetValue());
                                        else if (_PayrollDetailReport.WageBase.IsNull())
                                            _PayrollDetailReport.WageBase = null;
                                        if (ColData.colID.GetValue() == 9)
                                            _PayrollDetailReport.Account = ColData.value.GetValue();
                                        else if (_PayrollDetailReport.Account.IsNull())
                                            _PayrollDetailReport.Account = string.Empty;
                                        if (ColData.colID.GetValue() == 10)
                                            _PayrollDetailReport.Class = ColData.value.GetValue();
                                        else if (_PayrollDetailReport.Class.IsNull())
                                            _PayrollDetailReport.Class = string.Empty;
                                        if (ColData.colID.GetValue() == 11)
                                            _PayrollDetailReport.BillingStatus = ColData.value.GetValue();
                                        else if (_PayrollDetailReport.BillingStatus.IsNull())
                                            _PayrollDetailReport.BillingStatus = string.Empty;
                                        if (ColData.colID.GetValue() == 12)
                                            _PayrollDetailReport.Split = ColData.value.GetValue();
                                        else if (_PayrollDetailReport.Split.IsNull())
                                            _PayrollDetailReport.Split = string.Empty;
                                        if (ColData.colID.GetValue() == 13)
                                            _PayrollDetailReport.Quanity = ColData.value.GetValue();
                                        else if (_PayrollDetailReport.Quanity.IsNull())
                                            _PayrollDetailReport.Quanity = string.Empty;
                                        if (ColData.colID.GetValue() == 14)
                                            _PayrollDetailReport.Amount = double.Parse(ColData.value.GetValue());
                                        else if (_PayrollDetailReport.Amount.IsNull())
                                            _PayrollDetailReport.Amount = null;

                                        if (ColData.colID.GetValue() == 15)
                                        {
                                            if (!ColData.colID.GetValue().IsNull())
                                                RefNumber = ColData.value.GetValue();
                                            _PayrollDetailReport.RefNumber = RefNumber;
                                        }
                                        else if (_PayrollDetailReport.RefNumber.IsNull())
                                            _PayrollDetailReport.RefNumber = RefNumber;

                                        if (ColData.colID.GetValue() == 16)
                                        {
                                            if (!ColData.value.GetValue().IsNull())
                                                ModifiedTime = ColData.value.GetValue();
                                            _PayrollDetailReport.ModifiedTime = DateTime.Parse(ModifiedTime);
                                        }
                                        else if (!_PayrollDetailReport.ModifiedTime.HasValue && !string.IsNullOrWhiteSpace(ModifiedTime))
                                            _PayrollDetailReport.ModifiedTime = DateTime.Parse(ModifiedTime);
                                    }
                                }
                                _PayrollTransactionReports.Add(_PayrollDetailReport);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogFetchError(ex.Message, TableName.PayrollTransactionReport + " ---- Method -- WalkReportRet", ex);
            }
        }

        public bool IsNextChunck
        {
            get
            {
                return _nextchunk;
            }
        }

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion
    }
}
