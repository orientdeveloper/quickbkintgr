﻿using QBFC12Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Push.PurchaseOrder.Helper;
using Push.PurchaseOrder.QBPushService;

namespace Push.PurchaseOrder
{
    public class ItemReceiptPushOperation : QBSession
    {
        string _qbfile;
        ServiceQBItemReceipt _qbitemreceipt;

        public ItemReceiptPushOperation(string qbfile, ServiceQBItemReceipt qbitemreceipt, QBConnection connection, string country, byte qbXMLMarjorVersion, byte qbXMLMinorVersion, LoginModesenum loginModesenum)
            : base(qbfile, connection, country, qbXMLMarjorVersion, qbXMLMinorVersion, loginModesenum)
        {
            _qbfile = qbfile;
            _qbitemreceipt = qbitemreceipt;
        }

        public void Push(out Exception exception)
        {
            try
            {
                exception = null;
                IItemReceiptAdd ItemReceiptAdd = RequestMsgSet.AppendItemReceiptAddRq();
                if (_qbitemreceipt != null)
                {
                    if (!_qbitemreceipt.defMacro.IsNull())
                        ItemReceiptAdd.defMacro.SetValue(_qbitemreceipt.defMacro);
                    if (!_qbitemreceipt.VendorRef_ListID.IsNull())
                        ItemReceiptAdd.VendorRef.ListID.SetValue(_qbitemreceipt.VendorRef_ListID);
                    if (!_qbitemreceipt.VendorRef_FullName.IsNull())
                        ItemReceiptAdd.VendorRef.FullName.SetValue(_qbitemreceipt.VendorRef_FullName);
                    if (!_qbitemreceipt.APAccountRef_ListID.IsNull())
                        ItemReceiptAdd.APAccountRef.ListID.SetValue(_qbitemreceipt.APAccountRef_ListID);
                    if (!_qbitemreceipt.FullName.IsNull())
                        ItemReceiptAdd.APAccountRef.FullName.SetValue(_qbitemreceipt.APAccountRef_FullName);
                    if (!_qbitemreceipt.TxnDate.IsNull())
                        ItemReceiptAdd.TxnDate.SetValue(_qbitemreceipt.TxnDate);
                    if (!_qbitemreceipt.RefNumber.IsNull())
                        ItemReceiptAdd.RefNumber.SetValue(_qbitemreceipt.RefNumber);
                    if (!_qbitemreceipt.Memo.IsNull())
                        ItemReceiptAdd.Memo.SetValue(_qbitemreceipt.Memo);
                    if (!_qbitemreceipt.ExchangeRate.IsNull())
                        ItemReceiptAdd.ExchangeRate.SetValue(_qbitemreceipt.ExchangeRate);

                    ItemReceiptAdd.ExternalGUID.SetValue(Guid.NewGuid().ToString());

                    if (!_qbitemreceipt.LinkToTxnIDList.IsNull())
                        ItemReceiptAdd.LinkToTxnIDList.Add(_qbitemreceipt.LinkToTxnIDList);

                    if (_qbitemreceipt.QBItemReceiptlinedetail != null)
                    {
                        List<ServiceQBPushItemreceiptlinedetail> _qbitemreceiptlinedetails = _qbitemreceipt.QBPushItemreceiptlinedetail;
                        if (_qbitemreceiptlinedetails != null)
                        {
                            foreach (ServiceQBPushItemReceiptlinedetail _qbpushitemreceiptlinedetail in _qbitemreceiptlinedetails)
                            {
                                IORItemLineAdd ORItemReceiptLineAddListElement = ItemReceiptAdd.ORItemLineAddList.Append();
                                if(!_qbitemreceiptlinedetails.ItemRef_ListID.IsNull())
                                    ORItemReceiptLineAddListElement.ItemLineAdd.ItemRef.ListID.SetValue(_qbitemreceiptlinedetails.ItemRef_ListID);
                                if (!_qbitemreceiptlinedetails.ItemRef_FullName.IsNull())
                                    ORItemReceiptLineAddListElement.ItemLineAdd.ItemRef.FullName.SetValue(_qbitemreceiptlinedetails.ItemRef_FullName.IsNull());
                                if (!_qbitemreceiptlinedetails.ItemRef_ListID.IsNull())
                                ORItemReceiptLineAddListElement.ItemLineAdd.InventorySiteRef.ListID.SetValue("200000-1011023419");
                                if (!_qbitemreceiptlinedetails.ItemRef_ListID.IsNull())
                                ORItemReceiptLineAddListElement.ItemLineAdd.InventorySiteRef.FullName.SetValue("ab");
                                if (!_qbitemreceiptlinedetails.ItemRef_ListID.IsNull())
                                ORItemReceiptLineAddListElement.ItemLineAdd.InventorySiteLocationRef.ListID.SetValue("200000-1011023419");
                                if (!_qbitemreceiptlinedetails.ItemRef_ListID.IsNull())
                                ORItemReceiptLineAddListElement.ItemLineAdd.InventorySiteLocationRef.FullName.SetValue("ab");
                                if (!_qbitemreceiptlinedetails.ItemRef_ListID.IsNull())
                                ORItemReceiptLineAddListElement.ItemLineAdd.ORSerialLotNumber.SerialNumber.SetValue("ab");
                                if (!_qbitemreceiptlinedetails.ItemRef_ListID.IsNull())
                                ORItemReceiptLineAddListElement.ItemLineAdd.ORSerialLotNumber.LotNumber.SetValue("ab");
                                if (!_qbitemreceiptlinedetails.ItemRef_ListID.IsNull())
                                ORItemReceiptLineAddListElement.ItemLineAdd.Desc.SetValue("ab");
                                if (!_qbitemreceiptlinedetails.ItemRef_ListID.IsNull())
                                ORItemReceiptLineAddListElement.ItemLineAdd.Quantity.SetValue(2);
                                if (!_qbitemreceiptlinedetails.ItemRef_ListID.IsNull())
                                ORItemReceiptLineAddListElement.ItemLineAdd.UnitOfMeasure.SetValue("ab");
                                if (!_qbitemreceiptlinedetails.ItemRef_ListID.IsNull())
                                ORItemReceiptLineAddListElement.ItemLineAdd.Cost.SetValue(15.65);
                                if (!_qbitemreceiptlinedetails.ItemRef_ListID.IsNull())
                                ORItemReceiptLineAddListElement.ItemLineAdd.Amount.SetValue(10.01);
                                if (!_qbitemreceiptlinedetails.ItemRef_ListID.IsNull())
                                ORItemReceiptLineAddListElement.ItemLineAdd.CustomerRef.ListID.SetValue("200000-1011023419");
                                if (!_qbitemreceiptlinedetails.ItemRef_ListID.IsNull())
                                ORItemReceiptLineAddListElement.ItemLineAdd.CustomerRef.FullName.SetValue("ab");
                                if (!_qbitemreceiptlinedetails.ItemRef_ListID.IsNull())
                                ORItemReceiptLineAddListElement.ItemLineAdd.ClassRef.ListID.SetValue("200000-1011023419");
                                if (!_qbitemreceiptlinedetails.ItemRef_ListID.IsNull())
                                ORItemReceiptLineAddListElement.ItemLineAdd.ClassRef.FullName.SetValue("ab");
                                if (!_qbitemreceiptlinedetails.ItemRef_ListID.IsNull())
                                ORItemReceiptLineAddListElement.ItemLineAdd.BillableStatus.SetValue(ENBillableStatus.bsBillable);
                                if (!_qbitemreceiptlinedetails.ItemRef_ListID.IsNull())
                                ORItemReceiptLineAddListElement.ItemLineAdd.OverrideItemAccountRef.ListID.SetValue("200000-1011023419");
                                if (!_qbitemreceiptlinedetails.ItemRef_ListID.IsNull())
                                ORItemReceiptLineAddListElement.ItemLineAdd.OverrideItemAccountRef.FullName.SetValue("ab");
                                if (!_qbitemreceiptlinedetails.ItemRef_ListID.IsNull())
                                ORItemReceiptLineAddListElement.ItemLineAdd.LinkToTxn.TxnID.SetValue("200000-1011023419");
                                if (!_qbitemreceiptlinedetails.ItemRef_ListID.IsNull())
                                ORItemReceiptLineAddListElement.ItemLineAdd.LinkToTxn.TxnLineID.SetValue("200000-1011023419");
                               
                            }
                        }
                    }
                    if (_qbitemreceipt.QBPushPurchaseorderlinegroupdetail != null)
                    {
                        List<ServiceQBPushPurchaseorderlinegroupdetail> _qbpushpurchaseorderlinegroupdetails = new List<ServiceQBPushPurchaseorderlinegroupdetail>();
                        _qbpushpurchaseorderlinegroupdetails = _qbitemreceipt.QBPushPurchaseorderlinegroupdetail;
                        if (_qbpushpurchaseorderlinegroupdetails != null)
                        {
                            foreach (ServiceQBPushPurchaseorderlinegroupdetail _qbpushpurchaseorderlinegroupdetail in _qbpushpurchaseorderlinegroupdetails)
                            {
                                IORPurchaseOrderLineAdd ORItemReceiptLineAddListElement = ItemReceiptAdd.ORPurchaseOrderLineAddList.Append();

                                if (!_qbpushpurchaseorderlinegroupdetail.ItemGroupRef_ListID.IsNull())
                                    ORItemReceiptLineAddListElement.PurchaseOrderLineGroupAdd.ItemGroupRef.ListID.SetValue(_qbpushpurchaseorderlinegroupdetail.ItemGroupRef_ListID);
                                if (!_qbpushpurchaseorderlinegroupdetail.ItemGroupRef_FullName.IsNull())
                                    ORItemReceiptLineAddListElement.PurchaseOrderLineGroupAdd.ItemGroupRef.FullName.SetValue(_qbpushpurchaseorderlinegroupdetail.ItemGroupRef_FullName);
                                if (!_qbpushpurchaseorderlinegroupdetail.Quantity.IsNull())
                                    ORItemReceiptLineAddListElement.PurchaseOrderLineGroupAdd.Quantity.SetValue(double.Parse(_qbpushpurchaseorderlinegroupdetail.Quantity.ToString()));
                                if (!_qbpushpurchaseorderlinegroupdetail.UnitOfMeasure.IsNull())
                                    ORItemReceiptLineAddListElement.PurchaseOrderLineGroupAdd.UnitOfMeasure.SetValue(_qbpushpurchaseorderlinegroupdetail.UnitOfMeasure);
                                if (!_qbpushpurchaseorderlinegroupdetail.InventorySiteLocationRef_ListID.IsNull())
                                    ORItemReceiptLineAddListElement.PurchaseOrderLineGroupAdd.InventorySiteLocationRef.ListID.SetValue(_qbpushpurchaseorderlinegroupdetail.InventorySiteLocationRef_ListID);
                                if (!_qbpushpurchaseorderlinegroupdetail.InventorySiteLocationRef_FullName.IsNull())
                                    ORItemReceiptLineAddListElement.PurchaseOrderLineGroupAdd.InventorySiteLocationRef.FullName.SetValue(_qbpushpurchaseorderlinegroupdetail.InventorySiteLocationRef_FullName);
                            }
                        }
                    }
                }
                IMsgSetResponse responseMsgSet = SessionManager.DoRequests(RequestMsgSet);
                IResponse response = responseMsgSet.ResponseList.GetAt(0);
                if (response.StatusCode > 0)
                {
                    //Object field value specified in the request can not be found.
                    if (response.StatusCode == 3240 || response.StatusCode == 3140)
                    {
                        string message = response.StatusMessage;
                        string[] substr = message.Split('"');
                        if (substr.Length >= 2)
                        {
                            string id = substr[1];
                            PurchaseOrderListItemColumns _info = PurchaseOrderPushCatchError.GetFriendlyName(_qbitemreceipt, id);
                            throw new Exception(string.Format("{0}:{1} specified in the request cannot be found.", _info.FriendlyName, _info.FullName_Value), new Exception(string.Format("{0}.{1}:{2}({3}) specified in the request cannot be found.", _info.TableName, _info.FriendlyName, _info.FullName_Value, _info.ListId_Value)));
                        }
                    }
                    throw new Exception(response.StatusMessage);
                }
            }
            catch (Exception ex)
            {
                exception = ex;
            }
            finally
            {
                EndSession();
            }
        }
    }
}
