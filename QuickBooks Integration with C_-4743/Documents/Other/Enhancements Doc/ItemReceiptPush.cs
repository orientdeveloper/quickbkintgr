﻿using System;
using System.Net;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using QBFC12Lib;

namespace com.intuit.idn.samples
{
    public class ItemreciptPush
    {
        public void DoItemReceiptAdd()
        {
            bool sessionBegun = false;
            bool connectionOpen = false;
            QBSessionManager sessionManager = null;

            try
            {
                //Create the session Manager object
                sessionManager = new QBSessionManager();

                //Create the message set request object to hold our request
                IMsgSetRequest requestMsgSet = sessionManager.CreateMsgSetRequest("US", 11, 0);
                requestMsgSet.Attributes.OnError = ENRqOnError.roeContinue;

                BuildItemReceiptAddRq(requestMsgSet);

                //Connect to QuickBooks and begin a session
                sessionManager.OpenConnection("", "Sample Code from OSR");
                connectionOpen = true;
                sessionManager.BeginSession("", ENOpenMode.omDontCare);
                sessionBegun = true;

                //Send the request and get the response from QuickBooks
                IMsgSetResponse responseMsgSet = sessionManager.DoRequests(requestMsgSet);

                //End the session and close the connection to QuickBooks
                sessionManager.EndSession();
                sessionBegun = false;
                sessionManager.CloseConnection();
                connectionOpen = false;

                WalkItemReceiptAddRs(responseMsgSet);

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error");
                if (sessionBegun)
                {
                    sessionManager.EndSession();
                }
                if (connectionOpen)
                {
                    sessionManager.CloseConnection();
                }
            }
        }

        void BuildItemReceiptAddRq(IMsgSetRequest requestMsgSet)
        {
            IItemReceiptAdd ItemReceiptAddRq = requestMsgSet.AppendItemReceiptAddRq();
            //Set attributes
            //Set field value for defMacro
            ItemReceiptAddRq.defMacro.SetValue("IQBStringType");
            //Set field value for ListID
            ItemReceiptAddRq.VendorRef.ListID.SetValue("8000031D-1325793374");
            //Set field value for FullName
            ItemReceiptAddRq.VendorRef.FullName.SetValue("ab");
            //Set field value for ListID
            ItemReceiptAddRq.APAccountRef.ListID.SetValue("200000-1011023419");
            //Set field value for FullName
            ItemReceiptAddRq.APAccountRef.FullName.SetValue("ab");
            //Set field value for TxnDate
            ItemReceiptAddRq.TxnDate.SetValue(DateTime.Now);
            //Set field value for RefNumber
            ItemReceiptAddRq.RefNumber.SetValue("ab");
            //Set field value for Memo
            ItemReceiptAddRq.Memo.SetValue("ab");
            //Set field value for ExchangeRate
            ItemReceiptAddRq.ExchangeRate.SetValue(0.0f);
            //Set field value for ExternalGUID
            ItemReceiptAddRq.ExternalGUID.SetValue(Guid.NewGuid().ToString());
            //Set field value for LinkToTxnIDList
            //May create more than one of these if needed
            ItemReceiptAddRq.LinkToTxnIDList.Add("200000-1011023419");

            {               
                IExpenseLineAdd ExpenseLineAdd101 = ItemReceiptAddRq.ExpenseLineAddList.Append();
                ////Set attributes
                ////Set field value for defMacro
                ExpenseLineAdd101.defMacro.SetValue("IQBStringType");
                ////Set field value for ListID
                ExpenseLineAdd101.AccountRef.ListID.SetValue("200000-1011023419");
                ////Set field value for FullName
                ExpenseLineAdd101.AccountRef.FullName.SetValue("ab");
                ////Set field value for Amount
                ExpenseLineAdd101.Amount.SetValue(10.01);
                ////Set field value for Memo
                ExpenseLineAdd101.Memo.SetValue("ab");
                ////Set field value for ListID
                ExpenseLineAdd101.CustomerRef.ListID.SetValue("200000-1011023419");
                ////Set field value for FullName
                ExpenseLineAdd101.CustomerRef.FullName.SetValue("ab");
                ////Set field value for ListID
                ExpenseLineAdd101.ClassRef.ListID.SetValue("200000-1011023419");
                ////Set field value for FullName
                ExpenseLineAdd101.ClassRef.FullName.SetValue("ab");
                ////Set field value for BillableStatus
                ExpenseLineAdd101.BillableStatus.SetValue(ENBillableStatus.bsBillable);
            }

            //multiple itemline
            {
                IORItemLineAdd ORItemLineAddListElement102 = ItemReceiptAddRq.ORItemLineAddList.Append();
                //Set field value for ListID
                ORItemLineAddListElement102.ItemLineAdd.ItemRef.ListID.SetValue("200000-1011023419");
                //Set field value for FullName
                ORItemLineAddListElement102.ItemLineAdd.ItemRef.FullName.SetValue("ab");
                //Set field value for ListID
                ORItemLineAddListElement102.ItemLineAdd.InventorySiteRef.ListID.SetValue("200000-1011023419");
                //Set field value for FullName
                ORItemLineAddListElement102.ItemLineAdd.InventorySiteRef.FullName.SetValue("ab");
                //Set field value for ListID
                ORItemLineAddListElement102.ItemLineAdd.InventorySiteLocationRef.ListID.SetValue("200000-1011023419");
                //Set field value for FullName
                ORItemLineAddListElement102.ItemLineAdd.InventorySiteLocationRef.FullName.SetValue("ab");
                string ORSerialLotNumberElementType104 = "SerialNumber";
                if (ORSerialLotNumberElementType104 == "SerialNumber")
                {
                    //Set field value for SerialNumber
                    ORItemLineAddListElement102.ItemLineAdd.ORSerialLotNumber.SerialNumber.SetValue("ab");
                }
                if (ORSerialLotNumberElementType104 == "LotNumber")
                {
                    //Set field value for LotNumber
                    ORItemLineAddListElement102.ItemLineAdd.ORSerialLotNumber.LotNumber.SetValue("ab");
                }
                //Set field value for Desc
                ORItemLineAddListElement102.ItemLineAdd.Desc.SetValue("ab");
                //Set field value for Quantity
                ORItemLineAddListElement102.ItemLineAdd.Quantity.SetValue(2);
                //Set field value for UnitOfMeasure
                ORItemLineAddListElement102.ItemLineAdd.UnitOfMeasure.SetValue("ab");
                //Set field value for Cost
                ORItemLineAddListElement102.ItemLineAdd.Cost.SetValue(15.65);
                //Set field value for Amount
                ORItemLineAddListElement102.ItemLineAdd.Amount.SetValue(10.01);
                //Set field value for ListID
                ORItemLineAddListElement102.ItemLineAdd.CustomerRef.ListID.SetValue("200000-1011023419");
                //Set field value for FullName
                ORItemLineAddListElement102.ItemLineAdd.CustomerRef.FullName.SetValue("ab");
                //Set field value for ListID
                ORItemLineAddListElement102.ItemLineAdd.ClassRef.ListID.SetValue("200000-1011023419");
                //Set field value for FullName
                ORItemLineAddListElement102.ItemLineAdd.ClassRef.FullName.SetValue("ab");
                //Set field value for BillableStatus
                ORItemLineAddListElement102.ItemLineAdd.BillableStatus.SetValue(ENBillableStatus.bsBillable);
                //Set field value for ListID
                ORItemLineAddListElement102.ItemLineAdd.OverrideItemAccountRef.ListID.SetValue("200000-1011023419");
                //Set field value for FullName
                ORItemLineAddListElement102.ItemLineAdd.OverrideItemAccountRef.FullName.SetValue("ab");
                //Set field value for TxnID
                ORItemLineAddListElement102.ItemLineAdd.LinkToTxn.TxnID.SetValue("200000-1011023419");
                //Set field value for TxnLineID
                ORItemLineAddListElement102.ItemLineAdd.LinkToTxn.TxnLineID.SetValue("200000-1011023419");
            }

            //multiple itemgroupline
            {
                IORItemLineAdd ORItemLineAddListElement102 = ItemReceiptAddRq.ORItemLineAddList.Append();
                //Set field value for ListID
                ORItemLineAddListElement102.ItemGroupLineAdd.ItemGroupRef.ListID.SetValue("200000-1011023419");
                //Set field value for FullName
                ORItemLineAddListElement102.ItemGroupLineAdd.ItemGroupRef.FullName.SetValue("ab");
                //Set field value for Quantity
                ORItemLineAddListElement102.ItemGroupLineAdd.Quantity.SetValue(2);
                //Set field value for UnitOfMeasure
                ORItemLineAddListElement102.ItemGroupLineAdd.UnitOfMeasure.SetValue("ab");
                //Set field value for ListID
                ORItemLineAddListElement102.ItemGroupLineAdd.InventorySiteRef.ListID.SetValue("200000-1011023419");
                //Set field value for FullName
                ORItemLineAddListElement102.ItemGroupLineAdd.InventorySiteRef.FullName.SetValue("ab");
                //Set field value for ListID
                ORItemLineAddListElement102.ItemGroupLineAdd.InventorySiteLocationRef.ListID.SetValue("200000-1011023419");
                //Set field value for FullName
                ORItemLineAddListElement102.ItemGroupLineAdd.InventorySiteLocationRef.FullName.SetValue("ab");
            }

            //Set field value for IncludeRetElementList
            //May create more than one of these if needed
            ItemReceiptAddRq.IncludeRetElementList.Add("ab");
        }

        void WalkItemReceiptAddRs(IMsgSetResponse responseMsgSet)
        {
            if (responseMsgSet == null) return;

            IResponseList responseList = responseMsgSet.ResponseList;
            if (responseList == null) return;

            //if we sent only one request, there is only one response, we'll walk the list for this sample
            for (int i = 0; i < responseList.Count; i++)
            {
                IResponse response = responseList.GetAt(i);
                //check the status code of the response, 0=ok, >0 is warning
                if (response.StatusCode >= 0)
                {
                    //the request-specific response is in the details, make sure we have some
                    if (response.Detail != null)
                    {
                        //make sure the response is the type we're expecting
                        ENResponseType responseType = (ENResponseType)response.Type.GetValue();
                        if (responseType == ENResponseType.rtItemReceiptAddRs)
                        {
                            //upcast to more specific type here, this is safe because we checked with response.Type check above
                            IItemReceiptRet ItemReceiptRet = (IItemReceiptRet)response.Detail;
                            //WalkItemReceiptRet(ItemReceiptRet);
                        }
                    }
                }
            }
        }
    }
}