﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EssentialCFO.SSO.Models
{
    public class AppModal
    {
        [Required]
        [StringLength(50)]
        public string AppName { get; set; }
        public string AuthUrl { get; set; }
    }

    public class AppEditModal
    {
        [Required]
        public long Id { get; set; }
        [Required]
        [StringLength(50)]
        public string AppName { get; set; }
        public string AuthUrl { get; set; }
        public bool IsLocked { get; set; }
        public bool IsValidForMemberAuth { get; set; }
    }
}