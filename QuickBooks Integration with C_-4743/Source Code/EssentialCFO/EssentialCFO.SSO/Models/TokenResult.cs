﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace EssentialCFO.SSO.Models
{
    [CollectionDataContract]
    public class CheckTokenResults : List<CheckTokenResult>
    {
    }

    [XmlRoot("CheckTokenResult")]
    public class CheckTokenResult
    {
        public Guid Token { get; set; }
        public Guid MemberId { get; set; }
        public DateTime ExpiresOn { get; set; }
    }
}