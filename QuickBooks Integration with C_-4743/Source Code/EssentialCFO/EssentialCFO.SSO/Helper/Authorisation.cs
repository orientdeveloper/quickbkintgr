﻿///
/// Name : AuthorizationAttribute 
/// User : 1296 
/// Description : Custom filter for Authorization
/// date : 18/02/2014
///

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace EssentialCFO.SSO.Helper
{
    /// <summary>
    /// Class for custom attribute for Authorization
    /// </summary>
    public class AuthorizationAttribute : AuthorizeAttribute
    {

        /// <summary>
        /// Constructor for custom attribute for Admin Authorization
        /// </summary>
        public AuthorizationAttribute()
            : base()
        {
        }

        /// <summary>
        /// AuthorizeCore Method for checking the session have value or not.
        /// </summary>
        /// <param name="httpContext">Get current instance of HTTP Context</param>
        /// <returns></returns>
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            try
            {
                var isAuthorized = base.AuthorizeCore(httpContext);

                return !string.IsNullOrEmpty(SessionHelper.Password);
            }
            catch { return false; }
        }

        /// <summary>
        /// HandleUnauthorizedRequest Method for redirecting the user to login page if not authenticated.
        /// </summary>
        /// <param name="filterContext">Get current instance of Authorization Context</param>
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            try
            {
                filterContext.Result = new RedirectToRouteResult(new
                         RouteValueDictionary(new { controller = "account", action = "login" }));
            }
            catch { }
        }
    }
}
