﻿// 
//
// 
//

using EssentialCFO.SSO.Models;
using EssentialCFO.SSO.Models.Dal;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Web;
using System.Xml.Serialization;

namespace EssentialCFO.SSO.Helper
{
    /// <summary>
    /// InternalAuthResult
    /// </summary>
    [XmlRoot("InternalAuthResult")]
    public class InternalAuthResult
    {
        public InternalAuthResult()
        {
        }

        /// <summary>
        /// StatusCode
        /// </summary>
        public HttpStatusCode StatusCode { get; set; }

        /// <summary>
        /// StatusDescription
        /// </summary>
        public string StatusDescription { get; set; }
    }

    /// <summary>
    /// Helper functions class
    /// </summary>
    public class Functions
    {
        /// <summary>
        /// Method to check url header
        /// </summary>
        /// <param name="appid">AppId for the application</param>
        /// <returns></returns>
        public static bool CheckToken(Guid appid)
        {
            HttpContext context = HttpContext.Current;
            HttpRequest incomingRequestContext = context.Request;

            string requestUri = incomingRequestContext.Url.ToString();
            string authorizationHeader = incomingRequestContext.Headers[HttpRequestHeader.Authorization.ToString()];

            string authenticationToekn =
                string.Format("{1}{0}{2}",
                    "\\",
                    authorizationHeader,
                    requestUri);

            ECFOSSOEntities _Context = new ECFOSSOEntities();
            var keyQuery = _Context.Apps.Where(x => x.AppId == appid);
            byte[] ba = null;
            string key = string.Empty;
            foreach (App result in keyQuery)
            {
                if (result.EncKey != null)
                {
                    ba = Convert.FromBase64String(result.EncKeyString);
                }
            }

            if (ba == null)
                return false;

            bool isValidHash = ValidateHash(
                ba,
                authenticationToekn,
                "\\",
                UTF8Encoding.UTF8);

            if (!isValidHash)
            {
                Functions.AddToLog("", appid.ToString(), string.Empty, "CheckToken", "", "Not Valid Hash :- " + string.Format("CheckToken ClientId:{0}, clientAuthHeader:{1}, requestUri:{2}", appid, authenticationToekn, requestUri), "");
            }
            return isValidHash;
        }

        /// <summary>
        /// Method for checking the AppId Lock and ValidForMemberAuth status
        /// </summary>
        /// <param name="clientId">AppId for the application</param>
        /// <returns></returns>
        public static bool CheckThirdPartyTokenStatus(Guid clientId)
        {
            using (ECFOSSOEntities _Context = new ECFOSSOEntities())
            {
                App app = new App();
                app = _Context.Apps.Where(x => x.AppId == clientId).FirstOrDefault();
                
                if (app.IsLocked || !app.IsValidForMemberAuth)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            };
        }

        /// <summary>
        /// ValidateHash
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="message">message</param>
        /// <param name="delimeter">delimeter</param>
        /// <param name="encoding">encoding</param>
        /// <returns></returns>
        public static bool ValidateHash(byte[] key, string message, string delimeter, Encoding encoding)
        {
            HMACMD5 hmacMD5 = new HMACMD5(key);
            string[] messageParts =
                message.Split(new string[] { delimeter },
                StringSplitOptions.RemoveEmptyEntries);

            if (messageParts == null || messageParts.Length < 2)
            {
                return false;
            }

            string encodedText = messageParts[0];
            string text = messageParts[1];
            byte[] textBytes = encoding.GetBytes(text);
            byte[] computedHash = hmacMD5.ComputeHash(textBytes);
            string computedHashString = Convert.ToBase64String(computedHash);
            Functions.AddToLog("", "0000", string.Empty, "ValidateHash", "", "ValidateHash :- " + string.Format("Encodedtext={0};text={1};computedhashstring={2}", encodedText, text, computedHash), "");
            return encodedText.Equals(computedHashString);
        }

        /// <summary>
        /// ValidateMemberKeyHash
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="hashedMemberKey">hashedMemberKey</param>
        /// <param name="memberKey">memberKey</param>
        /// <param name="encoding">encoding</param>
        /// <returns></returns>
        public static bool ValidateMemberKeyHash(byte[] key, string hashedMemberKey, string memberKey, Encoding encoding)
        {
            HMACMD5 hmacMD5 = new HMACMD5(key);

            string encodedText = hashedMemberKey;
            string text = memberKey;
            byte[] textBytes = encoding.GetBytes(text);
            byte[] computedHash = hmacMD5.ComputeHash(textBytes);
            string computedHashString = Convert.ToBase64String(computedHash);
            return encodedText.Equals(computedHashString);
        }

        /// <summary>
        /// GenerateHash
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="input">input</param>
        /// <param name="encoding">encoding</param>
        /// <returns></returns>
        public static string GenerateHash(byte[] key, string input, Encoding encoding)
        {
            HMACMD5 hmacMD5 = new HMACMD5(key);

            byte[] textBytes = encoding.GetBytes(input);
            byte[] computedHash = hmacMD5.ComputeHash(textBytes);
            return Convert.ToBase64String(computedHash);
        }

        /// <summary>
        /// ValidateText
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="clearText"></param>
        /// <param name="encodedText"></param>
        /// <param name="encoding"></param>
        /// <returns></returns>
        public static bool ValidateText(Guid clientId, string clearText, string encodedText, Encoding encoding)
        {

            HttpContext context = HttpContext.Current;
            ECFOSSOEntities _Context = new ECFOSSOEntities();
            var keyQuery = _Context.Apps.Where(x => x.AppId == clientId);
            byte[] ba = null;
            string key = string.Empty;
            foreach (App result in keyQuery)
            {
                if (result.EncKey != null)
                {
                    ba = Convert.FromBase64String(result.EncKeyString);
                }
            }

            HMACMD5 hmacMD5 = new HMACMD5(ba);

            byte[] textBytes = encoding.GetBytes(clearText);
            byte[] computedHash = hmacMD5.ComputeHash(textBytes);
            string computedHashString = Convert.ToBase64String(computedHash);
            return encodedText.Equals(computedHashString);
        }

        /// <summary>
        /// EncodeText
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="clearText"></param>
        /// <param name="encoding"></param>
        /// <returns></returns>
        public static string EncodeText(Guid clientId, string clearText, Encoding encoding)
        {

            HttpContext context = HttpContext.Current;
            ECFOSSOEntities _Context = new ECFOSSOEntities();
            var keyQuery = _Context.Apps.Where(x => x.AppId == clientId);
            byte[] ba = null;
            string key = string.Empty;
            foreach (App result in keyQuery)
            {
                if (result.EncKey != null)
                {
                    ba = Convert.FromBase64String(result.EncKeyString);
                }
            }

            HMACMD5 hmacMD5 = new HMACMD5(ba);

            byte[] textBytes = encoding.GetBytes(clearText);
            byte[] computedHash = hmacMD5.ComputeHash(textBytes);
            string computedHashString = Convert.ToBase64String(computedHash);
            return computedHashString;
        }

        /// <summary>
        /// ValidateMemberKey
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="memberGuidEncrypted"></param>
        /// <param name="memberGuid"></param>
        /// <returns></returns>
        public static bool ValidateMemberKey(string clientId, string memberGuidEncrypted, string memberGuid)
        {

            HttpContext context = HttpContext.Current;

            //EOAPIDataClassesDataContext eoContext = new EOAPIDataClassesDataContext();
            var keyQuery = new List<Guid>(); //eoContext.GetKey(clientId, 0M);
            byte[] ba = null;
            string key = string.Empty;
            //foreach (GetKeyResult result in keyQuery)
            //{
            //    if (result.EncKey != null)
            //    {
            //        ba = Convert.FromBase64String(result.EncKeyString);
            //    }
            //}

            bool isValidHash = ValidateMemberKeyHash(
                ba,
                memberGuidEncrypted,
                memberGuid,
                UTF8Encoding.UTF8);

            return isValidHash;
        }

        /// <summary>
        /// AddToLog
        /// </summary>
        /// <param name="userId">userId</param>
        /// <param name="appid">appid</param>
        /// <param name="token">token</param>
        /// <param name="methodname">methodname</param>
        /// <param name="state">state</param>
        /// <param name="message">message</param>
        /// <param name="error">error</param>
        /// <returns></returns>
        public static bool AddToLog(string userId, string appid, string token, string methodname, string state, string message, string error)
        {
            using(ECFOSSOEntities context = new ECFOSSOEntities())
            {
                Log log = new Log();
                log.AppId = appid;
                log.UserId = userId;
                log.token = token;
                log.MethodName = methodname;
                log.State = state;
                log.Message = message;
                log.Error = error;
                log.ClientIP = GetClientAddressDetail();
                log.CreatedDate = DateTime.Now;
                context.Logs.Add(log);
                context.SaveChanges();
            }
            return true;
        }

        /// <summary>
        /// Method ByteArrayToString
        /// </summary>
        /// <param name="ba">ba</param>
        /// <returns></returns>
        public static string ByteArrayToString(byte[] ba)
        {
            // concat the bytes into one long string
            return ba.Aggregate(new StringBuilder(32),
                                    (sb, b) => sb.Append(b.ToString("X2"))
                                    ).ToString();
        }

        /// <summary>
        /// Method GetClientAddressDetail
        /// </summary>
        /// <returns></returns>
        public static string GetClientAddressDetail()
        {
            string ClientAddressDetail = string.Empty;
            if (!string.IsNullOrEmpty(HttpContext.Current.Request.UserHostAddress))
                ClientAddressDetail = HttpContext.Current.Request.UserHostAddress + " ";
            if (!string.IsNullOrEmpty(HttpContext.Current.Request.UserAgent))
                ClientAddressDetail = HttpContext.Current.Request.UserAgent + " ";
            if (!string.IsNullOrEmpty(HttpContext.Current.Request.Url.OriginalString))
                ClientAddressDetail = HttpContext.Current.Request.Url.OriginalString;
            return ClientAddressDetail;
        }
    }
}