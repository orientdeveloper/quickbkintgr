﻿///
/// Name : AdminAuthorizationAttribute 
/// User : 1296 
/// Description : Class for geting the values of session.
/// date : 05/02/2014
///

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EssentialCFO.SSO.Helper
{
    /// <summary>
    /// Class for geting the values of session.
    /// </summary>
    public class SessionHelper
    {
        /// <summary>
        /// Property for get and set the value of AdminName in sesssion variable.
        /// </summary>
        public static string Password
        {
            get
            {
                return GetFromSession<string>("Password");
            }
            set
            {
                SetInSession<string>("Password", value);
            }
        }

        /// <summary>
        /// Method for clearing the value in session variable.
        /// </summary>
        public static void Abandon()
        {
            if (HttpContext.Current != null && HttpContext.Current.Session != null)
                HttpContext.Current.Session.Abandon();
        }

        /// <summary>
        /// Method for get the value in session variable.
        /// </summary>
        private static T GetFromSession<T>(string key)
        {
            if (HttpContext.Current != null && HttpContext.Current.Session == null)
            {
                return default(T);
            }
            object obj = HttpContext.Current.Session[key];
            if (obj == null)
            {
                return default(T);
            }
            return (T)obj;
        }

        /// <summary>
        /// Method for set the value in session variable.
        /// </summary>
        private static void SetInSession<T>(string key, T value)
        {
            if (value == null)
            {
                HttpContext.Current.Session.Remove(key);
            }
            else
            {
                HttpContext.Current.Session[key] = value;
            }
        }

        /// <summary>
        /// Method for get the value in application variable.
        /// </summary>
        private static T GetFromApplication<T>(string key)
        {
            return (T)HttpContext.Current.Application[key];
        }

        /// <summary>
        /// Method for set the value in application variable.
        /// </summary>
        private static void SetInApplication<T>(string key, T value)
        {
            if (value == null)
            {
                HttpContext.Current.Application.Remove(key);
            }
            else
            {
                HttpContext.Current.Application[key] = value;
            }
        }
    }
}