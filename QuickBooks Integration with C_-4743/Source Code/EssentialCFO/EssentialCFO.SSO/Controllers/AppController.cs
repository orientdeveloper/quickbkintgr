﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EssentialCFO.SSO.Models.Dal;
using EssentialCFO.SSO.Models;
using System.Security.Cryptography;
using EssentialCFO.SSO.Helper;
using System.IO;

namespace EssentialCFO.SSO.Controllers
{
    /// <summary>
    /// AppController class
    /// </summary>
    [Authorization]
    public class AppController : Controller
    {
        private ECFOSSOEntities db = new ECFOSSOEntities();

        // GET: /App/
        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(db.Apps.ToList());
        }

        // GET: /App/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            App app = db.Apps.Find(id);
            if (app == null)
            {
                return HttpNotFound();
            }
            return View(app);
        }

        /// <summary>
        /// GET: /App/Create
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            return View();
        }

        // POST: /App/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AppModal model)
        {
            App app = new App();
            if (ModelState.IsValid)
            {
                byte[] secretkey = new Byte[24];
                RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
                rng.GetBytes(secretkey);
                string keyString = Convert.ToBase64String(secretkey);

                app.AppId = Guid.NewGuid();
                app.AppName = model.AppName;
                app.AuthUrl = model.AuthUrl;
                app.EncKey = secretkey;
                app.EncKeyString = keyString;
                app.CreatedDate = DateTime.Now;
                app.ModifiedDate = DateTime.Now;
                app.IsLocked = false;
                app.IsValidForMemberAuth = true;
                db.Apps.Add(app);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: /App/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            App app = db.Apps.Find(id);
            AppEditModal appeditmodal = new AppEditModal();
            appeditmodal.AppName = app.AppName;
            appeditmodal.AuthUrl = app.AuthUrl;
            appeditmodal.IsLocked = app.IsLocked;
            appeditmodal.IsValidForMemberAuth = app.IsValidForMemberAuth;
            appeditmodal.Id = app.Id;
            if (app == null)
            {
                return HttpNotFound();
            }
            return View(appeditmodal);
        }

        // POST: /App/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AppEditModal model)
        {
            if (ModelState.IsValid)
            {
                App app = new App();
                app = db.Apps.Find(model.Id);
                app.ModifiedDate = DateTime.Now;
                app.AppName = model.AppName;
                app.AuthUrl = model.AuthUrl;
                app.IsValidForMemberAuth = model.IsValidForMemberAuth;
                app.IsLocked = model.IsLocked;
                db.Apps.Attach(app);
                var entry = db.Entry(app);
                entry.Property(x => x.AppName).IsModified = true;
                entry.Property(x => x.IsLocked).IsModified = true;
                entry.Property(x => x.IsValidForMemberAuth).IsModified = true;
                entry.Property(x => x.ModifiedDate).IsModified = true;
                entry.Property(x => x.AuthUrl).IsModified = true;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            App app = db.Apps.Find(id);
            if (app == null)
            {
                return HttpNotFound();
            }
            return View(app);
        }

        /// <summary>
        /// POST: /App/Delete/5
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            App app = db.Apps.Find(id);
            List<AppToken> apptokenlist = db.AppTokens.Where(x => x.AppId == app.Id).ToList();
            foreach (AppToken item in apptokenlist)
            {
                db.AppTokens.Remove(item);
            }
            db.Apps.Remove(app);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        /// <summary>
        /// GenrateNewKey
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GenrateNewKey(long id)
        {
            byte[] secretkey = new Byte[24];
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            rng.GetBytes(secretkey);
            string keyString = Convert.ToBase64String(secretkey);

            App app = new App();
            app = db.Apps.Find(id);
            app.ModifiedDate = DateTime.Now;
            app.EncKeyString = keyString;
            app.EncKey = secretkey;
            app.AppId = Guid.NewGuid();
            db.Apps.Attach(app);
            var entry = db.Entry(app);
            entry.Property(x => x.EncKeyString).IsModified = true;
            entry.Property(x => x.EncKey).IsModified = true;
            entry.Property(x => x.AppId).IsModified = true;
            entry.Property(x => x.ModifiedDate).IsModified = true;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public FileResult Sample()
        {
            return File(Server.MapPath("~/content/SSOTest.zip"), "application/zip", "samplecode.zip");
        }

        /// <summary>
        /// Dispose
        /// </summary>
        /// <param name="disposing">disposing</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
