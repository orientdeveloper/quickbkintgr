﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EssentialCFO.SSO.Helper;

namespace EssentialCFO.SSO.Controllers
{
    public class AccountController : Controller
    {
        //
        // GET: /Account/
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string txtpassword)
        {
            if (txtpassword == ConfigurationHelper.LoginPassword)
            {
                SessionHelper.Password = txtpassword;
                return RedirectToAction("index", "app");
            }
            else
            {
                ViewData["LoginError"] = "Invalid password!";
                return View();
            }
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("login", "account");
        }
	}
}