﻿using EssentialCFO.SSO.Helper;
using EssentialCFO.SSO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using EssentialCFO.SSO.ECFOWebService;
using EssentialCFO.SSO.Models.Dal;
using System.Data;

namespace EssentialCFO.SSO.Controllers
{
    /// <summary>
    /// Service for authenticating the user and getting the details
    /// </summary>
    public class LoginSrvController : ApiController
    {
        /// <summary>
        /// Service method for autheticating the user by appid and token.
        /// </summary>
        /// <param name="appid">AppId for your application</param>
        /// <param name="token">Token for your application</param>
        /// <returns>Login result object</returns>
        [HttpGet]
        public LoginResult Authenticate(Guid appid, Guid token)
        {
            LoginResult loginresult = new LoginResult();
            try
            {
                if (token == null || token == Guid.Empty || appid == null || appid == Guid.Empty)
                {
                    Functions.AddToLog(string.Empty, appid.ToString(), token.ToString(), "Authenticate", "1", "Required Parameters are appid, token and apiSig",
                        "");

                    loginresult.Authenticated = false;
                    loginresult.LoginResponseReason = LoginResponseReason.EachParameterRequired;
                    loginresult.LoginResponse = "Required Parameters are appid, token and apiSig";
                    return loginresult;
                }

                bool validAuth = Functions.CheckThirdPartyTokenStatus(appid);

                if (!validAuth)
                {
                    Functions.AddToLog(string.Empty, appid.ToString(), token.ToString(), "Authenticate", "2", "API Account is locked or not valid for 3rd party authentication",
                        "");

                    loginresult.Authenticated = false;
                    loginresult.LoginResponseReason = LoginResponseReason.AccountLocked;
                    loginresult.LoginResponse = "API Account is locked or not valid for 3rd party authentication";
                    return loginresult;
                }

                bool validToken = Functions.CheckToken(appid);

                if (!validToken)
                {
                    Functions.AddToLog(string.Empty, appid.ToString(), token.ToString(), "Authenticate", "3", "Token is invalid",
                        "");

                    loginresult.Authenticated = false;
                    loginresult.LoginResponseReason = LoginResponseReason.TokenIsInvalid;
                    loginresult.LoginResponse = "Token is invalid";
                    return loginresult;
                }

                string encToken = Functions.EncodeText(appid, token.ToString(), Encoding.UTF8);
                bool validSignature = Functions.ValidateText(appid, token.ToString(), encToken, Encoding.UTF8);

                if (!validSignature)
                {
                    Functions.AddToLog(string.Empty, appid.ToString(), token.ToString(), "Authenticate", "4", "Signature Is Not Valid",
                        "");

                    loginresult.Authenticated = false;
                    loginresult.LoginResponseReason = LoginResponseReason.TokenIsInvalid;
                    loginresult.LoginResponse = "Signature Is Not Valid";
                    return loginresult;
                }

                ECFOSSOEntities ecfoContext = new ECFOSSOEntities();
                App app = ecfoContext.Apps.Where(x => x.AppId == appid).FirstOrDefault();
                AppToken apptoken = ecfoContext.AppTokens.Where(x => x.Token == token && x.AppId == app.Id && x.ExpiresOn > DateTime.Now).FirstOrDefault();

                if (apptoken == null)
                {
                    Functions.AddToLog(
                        "", appid.ToString(), token.ToString(), "Authenticate", "5", "Token expire",
                    "");

                    loginresult.Authenticated = false;
                    loginresult.LoginResponseReason = LoginResponseReason.TokenIsInvalid;
                    loginresult.LoginResponse = "Token expire";
                    return loginresult;
                }

                DataTable UserDetail = new DataTable();
                using (ECFOWebServiceTestSoapClient ecfoclient = new ECFOWebServiceTestSoapClient())
                {
                    UserDetail = ecfoclient.GetUserBasedonUserID(apptoken.UserId);
                }

                if (UserDetail != null && UserDetail.Rows.Count > 0)
                {
                    Functions.AddToLog(UserDetail.Rows[0]["UserID"].ToString(), appid.ToString(), token.ToString(), "Authenticate", "6", "Valid Login",
                        "");

                    loginresult.Title = UserDetail.Rows[0]["Title"].ToString();
                    loginresult.LastName = UserDetail.Rows[0]["LastName"].ToString();
                    loginresult.Mobile = UserDetail.Rows[0]["Mobile"].ToString();
                    loginresult.PostalCode = UserDetail.Rows[0]["PostalCode"].ToString();
                    loginresult.State = UserDetail.Rows[0]["State"].ToString();
                    loginresult.Town = UserDetail.Rows[0]["Town"].ToString();
                    loginresult.City = UserDetail.Rows[0]["City"].ToString();
                    loginresult.Country = UserDetail.Rows[0]["Country"].ToString();
                    loginresult.Email = UserDetail.Rows[0]["Email"].ToString();
                    loginresult.HomePhone = UserDetail.Rows[0]["HomePhone"].ToString();
                    loginresult.OrganizationName = UserDetail.Rows[0]["OrganizationName"].ToString();
                    loginresult.OrganizationDescription = UserDetail.Rows[0]["OrganizationDescription"].ToString();
                    loginresult.UserID = UserDetail.Rows[0]["UserID"].ToString();
                    loginresult.Authenticated = true;
                    loginresult.LoginResponseReason = LoginResponseReason.ValidLogin;
                    loginresult.LoginResponse = "Valid Login";
                }
                else
                {
                    Functions.AddToLog(string.Empty, appid.ToString(), token.ToString(), "Authenticate", "7", "No user detail found!",
                        "");

                    loginresult.Authenticated = false;
                    loginresult.LoginResponseReason = LoginResponseReason.InValidLoginDetails;
                    loginresult.LoginResponse = "No user detail found!";
                }
                return loginresult;
            }
            catch (Exception ex)
            {
                Functions.AddToLog(string.Empty, appid.ToString(), token.ToString(), "Authenticate", "7", ex.Message,
                        ex.StackTrace);

                loginresult.Authenticated = false;
                loginresult.LoginResponseReason = LoginResponseReason.InternalServerException;
                loginresult.LoginResponse = "An Internal Error Has Occurred";
                return loginresult;
            }
        }

        /// <summary>
        /// Service method for autheticating the user by appid, username and password.
        /// </summary>
        /// <param name="appid">AppId for your application</param>
        /// <param name="loginrequest">Username and Password to access the main essentialcfo website</param>
        /// <returns>Login result object</returns>
        [HttpPost]
        public LoginResult AuthenticateMember(Guid appid, LoginRequest loginrequest)
        {
            LoginResult loginresult = new LoginResult();
            try
            {
                if (string.IsNullOrEmpty(loginrequest.Password) || string.IsNullOrEmpty(loginrequest.UserName) || appid == null || appid == Guid.Empty)
                {
                    Functions.AddToLog(
                        "", 
                        appid.ToString(),
                        "UserName : " + loginrequest.UserName == null ? "" : loginrequest.UserName + " Password : " + loginrequest.Password == null ? "" : loginrequest.Password,
                        "AuthenticateMember", "1",
                        "Required Parameters are appid, username and password",
                        "");

                    loginresult.Authenticated = false;
                    loginresult.LoginResponseReason = LoginResponseReason.EachParameterRequired;
                    loginresult.LoginResponse = "Required Parameters are appid, username and password";
                    return loginresult;
                }

                bool validAuth = Functions.CheckThirdPartyTokenStatus(appid);

                if (!validAuth)
                {
                    Functions.AddToLog(
                       "",
                       appid.ToString(),
                       "UserName : " + loginrequest.UserName == null ? "" : loginrequest.UserName + " Password : " + loginrequest.Password == null ? "" : loginrequest.Password,
                       "AuthenticateMember", "2",
                       "API Account is locked or not valid for 3rd party authentication",
                       "");

                    loginresult.Authenticated = false;
                    loginresult.LoginResponseReason = LoginResponseReason.AccountLocked;
                    loginresult.LoginResponse = "API Account is locked or not valid for 3rd party authentication";
                    return loginresult;
                }

                bool validToken = Functions.CheckToken(appid);

                if (!validToken)
                {
                    Functions.AddToLog(
                        "",
                        appid.ToString(),
                        "UserName : " + loginrequest.UserName == null ? "" : loginrequest.UserName + " Password : " + loginrequest.Password == null ? "" : loginrequest.Password,
                        "AuthenticateMember", "2",
                        "Token is invalid",
                        "");

                    loginresult.Authenticated = false;
                    loginresult.LoginResponseReason = LoginResponseReason.TokenIsInvalid;
                    loginresult.LoginResponse = "Token is invalid";
                    return loginresult;
                }

                DataTable UserDetail = new DataTable();
                using (ECFOWebServiceTestSoapClient ecfoclient = new ECFOWebServiceTestSoapClient())
                {
                    UserDetail = ecfoclient.GetAuthenticateMember(loginrequest.UserName, loginrequest.Password);
                }

                if (UserDetail != null && UserDetail.Rows.Count > 0)
                {
                    Functions.AddToLog(
                        UserDetail.Rows[0]["UserID"].ToString(),
                        appid.ToString(),
                        "UserName : " + loginrequest.UserName == null ? "" : loginrequest.UserName + " Password : " + loginrequest.Password == null ? "" : loginrequest.Password,
                        "AuthenticateMember", "3",
                        "Valid Login",
                        "");

                    loginresult.Title = UserDetail.Rows[0]["Title"].ToString();
                    loginresult.LastName = UserDetail.Rows[0]["LastName"].ToString();
                    loginresult.Mobile = UserDetail.Rows[0]["Mobile"].ToString();
                    loginresult.PostalCode = UserDetail.Rows[0]["PostalCode"].ToString();
                    loginresult.State = UserDetail.Rows[0]["State"].ToString();
                    loginresult.Town = UserDetail.Rows[0]["Town"].ToString();
                    loginresult.City = UserDetail.Rows[0]["City"].ToString();
                    loginresult.Country = UserDetail.Rows[0]["Country"].ToString();
                    loginresult.Email = UserDetail.Rows[0]["Email"].ToString();
                    loginresult.HomePhone = UserDetail.Rows[0]["HomePhone"].ToString();
                    loginresult.OrganizationName = UserDetail.Rows[0]["OrganizationName"].ToString();
                    loginresult.OrganizationDescription = UserDetail.Rows[0]["OrganizationDescription"].ToString();
                    loginresult.UserID = UserDetail.Rows[0]["UserID"].ToString();
                    loginresult.Authenticated = true;
                    loginresult.LoginResponseReason = LoginResponseReason.ValidLogin;
                    loginresult.LoginResponse = "Valid Login";
                }
                else
                {
                    Functions.AddToLog(
                        string.Empty,
                        appid.ToString(),
                        "UserName : " + loginrequest.UserName == null ? "" : loginrequest.UserName + " Password : " + loginrequest.Password == null ? "" : loginrequest.Password,
                        "AuthenticateMember", "4",
                        "No user detail found!",
                        "");

                    loginresult.Authenticated = false;
                    loginresult.LoginResponseReason = LoginResponseReason.InValidLoginDetails;
                    loginresult.LoginResponse = "No user detail found!";
                }
                return loginresult;
            }
            catch (Exception ex)
            {
                Functions.AddToLog(
                    string.Empty,
                    appid.ToString(),
                    "UserName : " + loginrequest.UserName == null ? "" : loginrequest.UserName + " Password : " + loginrequest.Password == null ? "" : loginrequest.Password,
                    "AuthenticateMember", "5",
                    ex.Message,
                    ex.StackTrace);

                loginresult.Authenticated = false;
                loginresult.LoginResponseReason = LoginResponseReason.InternalServerException;
                loginresult.LoginResponse = "An Internal Error Has Occurred";
                return loginresult;
            }
        }
    }
}
