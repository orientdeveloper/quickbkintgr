﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EssentialCFO.SSO.Models.Dal;

namespace EssentialCFO.SSO.Controllers
{
    public class LogController : Controller
    {
        private ECFOSSOEntities db = new ECFOSSOEntities();

        // GET: /Log/
        public ActionResult Index()
        {
            return View(db.Logs.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
