﻿using EssentialCFO.SSO.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using EssentialCFO.SSO.Models.Dal;

namespace EssentialCFO.SSO.Controllers
{
    /// <summary>
    /// Service method to get url with token
    /// </summary>
    public class TokenSrvController : ApiController
    {
        /// <summary>
        /// Service method to get url with token
        /// </summary>
        /// <param name="AppId">AppId provided to third party application</param>
        /// <param name="userid">UserId of current login user in the main website(www.essentialcfo.com)</param>
        /// <param name="username">Internal username</param>
        /// <param name="password">Internal password</param>
        /// <returns></returns>
        public HttpResponseMessage GetTokenForUrl(Guid AppId, string userid, string username, string password)
        {
            try
            {
                if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password) || username != ConfigurationHelper.ServiceUserName || password != ConfigurationHelper.ServicePassword)
                {
                    Functions.AddToLog(
                        userid == null ? "" : userid,
                        AppId == null ? "" : AppId.ToString(),
                        "UserName : " + username == null ? "" : username + " Password : " + password == null ? "" : password,
                        "GetTokenForUrl", "1", "You are not authorized to access this service method!",
                        "");

                    var response = Request.CreateResponse(HttpStatusCode.Unauthorized, "You are not authorized to access this service method!");
                    return response;
                }

                if (AppId == Guid.Empty || AppId == null)
                {
                    Functions.AddToLog(
                        userid == null ? "" : userid,
                        AppId == null ? "" : AppId.ToString(),
                        "UserName : " + username == null ? "" : username + " Password : " + password == null ? "" : password,
                        "GetTokenForUrl", "2", "You are not authorized to access this service method!",
                        "");

                    var response = Request.CreateResponse(HttpStatusCode.BadRequest, "Required Parameters are AppId");
                    return response;
                }

                if (!Functions.CheckThirdPartyTokenStatus(AppId))
                {
                    Functions.AddToLog(
                        userid,
                        AppId.ToString(),
                        "UserName : " + username == null ? "" : username + " Password : " + password == null ? "" : password,
                        "GetTokenForUrl", "3", "API Account is locked or not valid for 3rd party authentication",
                        "");

                    var response = Request.CreateResponse(HttpStatusCode.Unauthorized, "API Account is locked or not valid for 3rd party authentication");
                    return response;
                }

                ECFOSSOEntities ecfoContext = new ECFOSSOEntities();
                App app = ecfoContext.Apps.Where(x => x.AppId == AppId && x.IsLocked == false).FirstOrDefault();

                if (app == null)
                {
                    Functions.AddToLog(
                        userid,
                        AppId.ToString(),
                        "UserName : " + username == null ? "" : username + " Password : " + password == null ? "" : password,
                        "GetTokenForUrl", "4", "API Account is locked or not valid for 3rd party authentication",
                        "");

                    var response = Request.CreateResponse(HttpStatusCode.Unauthorized, "API Account is locked or not valid for 3rd party authentication");
                    return response;
                }

                byte[] ba = null;
                string key = string.Empty;
                if (app.EncKey != null)
                {
                    ba = Convert.FromBase64String(app.EncKeyString);
                }

                if (ba == null)
                {
                    Functions.AddToLog(
                        userid,
                        AppId.ToString(),
                        "UserName : " + username == null ? "" : username + " Password : " + password == null ? "" : password,
                        "GetTokenForUrl", "5", "API Account is locked or not valid for 3rd party authentication",
                        "");

                    var response = Request.CreateResponse(HttpStatusCode.Unauthorized, "API Account is locked or not valid for 3rd party authentication");
                    return response;
                }

                DateTime validUntil = DateTime.Now.AddHours(ConfigurationHelper.TokenValidHours);
                Guid token = Guid.Empty;
                string authUrl = string.Empty;

                AppToken authResult = ecfoContext.AppTokens.Where(x => x.AppId == app.Id && x.ExpiresOn > DateTime.Now).FirstOrDefault();
                if (authResult != null)
                {
                    token = authResult.Token;
                    authUrl = app.AuthUrl;

                    Functions.AddToLog(
                        userid,
                        AppId.ToString(),
                        "token : " + token + " UserName : " + username == null ? "" : username + " Password : " + password == null ? "" : password,
                        "GetTokenForUrl", "6", "old token for member",
                        "");
                }
                else
                {
                    token = Guid.NewGuid();

                    Functions.AddToLog(
                        userid,
                        AppId.ToString(),
                        "token : " + token + " UserName : " + username == null ? "" : username + " Password : " + password == null ? "" : password,
                        "GetTokenForUrl", "7", "new token for member",
                        "");
                    
                    authResult = new AppToken();
                    authResult.Token = token;
                    authResult.ExpiresOn = validUntil;
                    authResult.CreatedDate = DateTime.Now;
                    authResult.AppId = app.Id;
                    authResult.UserId = userid;
                    ecfoContext.AppTokens.Add(authResult);
                    ecfoContext.SaveChanges();
                }

                if (authResult == null)
                {
                    Functions.AddToLog(
                        userid,
                        AppId.ToString(),
                        "UserName : " + username == null ? "" : username + " Password : " + password == null ? "" : password,
                        "GetTokenForUrl", "8", "Could not generate token for member",
                        "");

                    var response = Request.CreateResponse(HttpStatusCode.InternalServerError, "Could not generate token for member");
                    return response;
                }

                if (token == Guid.Empty || token == null || userid == string.Empty)
                {
                    Functions.AddToLog(
                        userid,
                        AppId.ToString(),
                        "UserName : " + username == null ? "" : username + " Password : " + password == null ? "" : password,
                        "GetTokenForUrl", "9", "Error linking user id to token",
                        "");

                    var response = Request.CreateResponse(HttpStatusCode.InternalServerError, "Error linking user id to token");
                    return response;
                }

                string hashToken = Uri.EscapeDataString(Functions.GenerateHash(ba, token.ToString(), UTF8Encoding.UTF8));
                string hashValidator = Uri.EscapeDataString(Functions.GenerateHash(ba, userid + token.ToString(), UTF8Encoding.UTF8));
                //string hashApiSig = Uri.EscapeDataString(Functions.EncodeText(Guid.Empty, token.ToString(), Encoding.UTF8));

                authUrl = string.Format(authUrl, userid, token.ToString(), hashValidator);

                Functions.AddToLog(
                        userid,
                        AppId.ToString(),
                        "token : " + token + " authUrl : " + authUrl + " UserName : " + username == null ? "" : username + " Password : " + password == null ? "" : password,
                        "GetTokenForUrl", "10", "token url for member",
                        "");

                var response1 = Request.CreateResponse(HttpStatusCode.OK, authUrl);
                return response1;
            }
            catch (Exception ex)
            {
                Functions.AddToLog(
                        userid,
                        AppId.ToString(),
                        "UserName : " + username == null ? "" : username + " Password : " + password == null ? "" : password,
                        "GetTokenForUrl", "11", ex.Message, ex.StackTrace);

                var response = Request.CreateResponse(HttpStatusCode.InternalServerError, "An Internal Error Has Occurred" + "<br><br>" + ex.InnerException + "<br><br>" + ex.StackTrace);
                return response;
            }
        }
    }
}
