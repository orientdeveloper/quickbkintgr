﻿//
// Class	:	WcfAppender.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:42 AM
//
using log4net.Appender;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using log4net.Core;
using log4net.Filter;
using WCFLogger.ServiceLogger;
using System.Reflection;

namespace WCFLogger
{
    /// <summary>
    /// Log4net custom appender
    /// </summary>
    public class WcfAppender : AppenderSkeleton
    {
        #region Private Variable
        private string servicepassword = "p@ssw0rd1111";

        private string serviceusername = @"essentialcfo\pranav";

        private LevelRangeFilter filter;

        private ServiceLogger.LoggerServiceClient _serviceLogging = null;

        private string url;

        private object lockobject = new object();
        #endregion

        #region Public Property
        public LevelRangeFilter Filter
        {
            get
            {
                return filter;
            }
            set
            {
                filter = value;
            }
        }

        public string Url
        {
            get
            {
                return url;
            }
            set
            {
                url = value;
            }
        }
        #endregion

        #region Override Methods
        protected override void Append(LoggingEvent loggingEvent)
        {
            try
            {
                if (filter == null || (loggingEvent.Level >= filter.LevelMin && loggingEvent.Level <= filter.LevelMax))
                {
                    Guid company = Guid.Empty;
                    if (Guid.TryParse(loggingEvent.LoggerName, out company) && company != Guid.Empty)
                    {
                        LogInfo info = new LogInfo();
                        info.ExceptionMessage = loggingEvent.ExceptionObject == null ? loggingEvent.RenderedMessage : string.Format("{0}\n{1}", loggingEvent.ExceptionObject.ToString(), loggingEvent.ExceptionObject.StackTrace);
                        info.Level = loggingEvent.Level.DisplayName;
                        info.Company = company;
                        info.Message = string.Format("{0} Client Version : {1}", loggingEvent.RenderedMessage, AssemblyName.GetAssemblyName("QBSynApp.exe").Version);
                        info.Password = servicepassword;
                        info.TimeStamp = loggingEvent.TimeStamp;
                        info.UserName = serviceusername;
#if (!DEBUG)
                        lock (lockobject)
                        {
                            if (_serviceLogging.State == CommunicationState.Faulted)
                            {
                                _serviceLogging = null;
                                InitializeServiceConnection();
                            }
                            _serviceLogging.LogAsync(info);
                        }
#endif
                    }
                }
            }
            catch (Exception exc)
            {
                ErrorHandler.Error("Unable to send loggingEvent to Logging Service", exc, ErrorCode.WriteFailure);
            }
        }

        public override void ActivateOptions()
        {
            base.ActivateOptions();
            InitializeServiceConnection();
        }

        protected override bool RequiresLayout
        {
            get
            {
                return false;
            }
        }

        protected override void OnClose()
        {
            base.OnClose();
            if (_serviceLogging != null)
            {
                if (_serviceLogging.State == CommunicationState.Opened)
                {
                    _serviceLogging.Close();
                }
            }
        }
        #endregion

        #region Private method
        private void InitializeServiceConnection()
        {
            try
            {
                if (_serviceLogging == null)
                {                   
                    EndpointAddress address = new EndpointAddress(new Uri(url));
                    BasicHttpBinding binding = new BasicHttpBinding();
                    binding.Security.Mode = BasicHttpSecurityMode.Transport;
                    _serviceLogging = new LoggerServiceClient(binding, address);
                    _serviceLogging.LogCompleted += _serviceLogging_LogCompleted;
                }
            }
            catch (Exception exc)
            {
                ErrorHandler.Error("Could not initialize the service", exc, ErrorCode.GenericFailure);
            }
        }

        private void _serviceLogging_LogCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                ErrorHandler.Error("Unable to send loggingEvent to Logging Service", e.Error, ErrorCode.WriteFailure);
            }
        }
        #endregion
    }
}
