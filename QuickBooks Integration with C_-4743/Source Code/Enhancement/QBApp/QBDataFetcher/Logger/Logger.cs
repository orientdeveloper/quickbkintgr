﻿//
// Class	:	Logger.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:42 AM
//
using System;
using System.Configuration;
using System.Globalization;
using log4net;
using log4net.Repository;

namespace QBDataFetcher.Logging
{
    /// <summary>
    /// Class for logger
    /// </summary>
    public class Logger
    {
        #region Datamembers

        private static ILog log = null;

        #endregion

        #region Class Initializer

        static Logger()
        {
            log = LogManager.GetLogger(typeof(Logger));
            log4net.GlobalContext.Properties["host"] = Environment.MachineName;
        }

        #endregion

        #region ILogger Members

        public static void LogException(Exception exception)
        {
            if (log.IsErrorEnabled)
                log.Error(string.Format(CultureInfo.InvariantCulture, "{0}", exception.Message), exception);
        }

        public static void LogError(string message)
        {
            if (log.IsErrorEnabled)
                log.Error(string.Format(CultureInfo.InvariantCulture, "{0}", message));
        }

        public static void LogWarningMessage(string message)
        {
            if (log.IsWarnEnabled)
                log.Warn(string.Format(CultureInfo.InvariantCulture, "{0}", message));
        }

        public static void LogInfoMessage(string message)
        {
            if (log.IsInfoEnabled)
                log.Info(string.Format(CultureInfo.InvariantCulture, "{0}", message));
        }

        public static void LogFetcherResponseMessage(QBFC12Lib.IResponse response)
        {
            if (log.IsInfoEnabled && response.StatusCode > 0)
                log.Info(string.Format(CultureInfo.InvariantCulture, "Status Code: {0} Severity: {1} Message: {2}", response.StatusCode, response.StatusSeverity, response.StatusMessage));
        }
        #endregion

    }
}
