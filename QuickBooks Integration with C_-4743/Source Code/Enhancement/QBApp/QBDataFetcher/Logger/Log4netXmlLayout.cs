﻿//
// Class	:	ISynLogger.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:42 AM
//
using System;
using System.Xml;
using log4net.Core;
using log4net.Layout;

namespace QBDataFetcher.Logger
{
    /// <summary>
    /// Class to write the xml
    /// </summary>
    public class Log4netXmlLayout : XmlLayoutBase
    {
        protected override void FormatXml(XmlWriter writer, LoggingEvent loggingEvent)
        {
            writer.WriteStartElement("LogEntry");

            writer.WriteStartElement("GroupId");
            writer.WriteString(SynLogger.LogGroup.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("Company");
            writer.WriteString(loggingEvent.LoggerName);
            writer.WriteEndElement();
           
            writer.WriteStartElement("Level");
            writer.WriteString(loggingEvent.Level.DisplayName);
            writer.WriteEndElement();

            writer.WriteStartElement("Message");
            writer.WriteString(loggingEvent.RenderedMessage);
            writer.WriteEndElement();

            writer.WriteStartElement("Details");
            if (loggingEvent.ExceptionObject != null)
                writer.WriteString(loggingEvent.ExceptionObject.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("StackTrace");
            if (loggingEvent.ExceptionObject != null)
                writer.WriteString(string.IsNullOrEmpty(loggingEvent.ExceptionObject.StackTrace) ? string.Empty : loggingEvent.ExceptionObject.StackTrace);
            writer.WriteEndElement();

            writer.WriteStartElement("TimeStamp");
            writer.WriteString(loggingEvent.TimeStamp.ToString());
            writer.WriteEndElement();

            writer.WriteEndElement();
        }
    }
}
