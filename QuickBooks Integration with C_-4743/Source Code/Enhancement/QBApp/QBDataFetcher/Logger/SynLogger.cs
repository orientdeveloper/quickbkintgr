﻿//
// Class	:	Logger.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:42 AM
//
using System;
using System.Configuration;
using System.Globalization;
using log4net;
using log4net.Repository;
using log4net.Config;
using log4net.Appender;
using System.IO;
using QBDataFetcher.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml;
namespace QBDataFetcher.Logger
{
    /// <summary>
    /// Class for looging
    /// </summary>
    public class SynLogger : ISynLogger
    {
        public static Guid LogGroup = Guid.Empty;
        private static ILog log = null;
        private string CompanyName;
        public event DelSynLogger Logger;

        public SynLogger(string companyname, Guid companyid, string LogFolder)
        {
            //Dummy code to load the assembly only
            WCFLogger.WcfAppender appender = new WCFLogger.WcfAppender();

            log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "log4net.config")).OpenRead());
            log = LogManager.GetLogger(companyid.ToString());

            ChangeFilePath(LogFolder);

            log4net.GlobalContext.Properties["host"] = Environment.MachineName;
            CompanyName = companyname;
            Logger += new DelSynLogger((l, m) => { });
        }

        public void LogSynStarted()
        {
            if (log.IsInfoEnabled)
            {
                string logmessage = string.Format(CultureInfo.InvariantCulture, "{0}: Syn Started", CompanyName);
                Logger(logmessage);
                log.Info(logmessage);
            }
        }

        public void LogSynCompleted()
        {
            if (log.IsInfoEnabled)
            {
                string logmessage = string.Format(CultureInfo.InvariantCulture, "{0}: Syn Completed", CompanyName);
                Logger(logmessage);
                log.Info(logmessage);
            }
        }

        public void LogTableSynStarted(string tablename)
        {
            if (log.IsInfoEnabled)
            {
                tablename = ExtensionMethods.SetAlternateTableName(tablename);
                string logmessage = string.Format(CultureInfo.InvariantCulture, "{0}.{1}: Syn Started", CompanyName, tablename);

                Logger(logmessage);
                log.Info(logmessage);
            }
        }

        public void LogTableSynCompleted(string tablename)
        {
            if (log.IsInfoEnabled)
            {
                tablename = ExtensionMethods.SetAlternateTableName(tablename);
                string logmessage = string.Format(CultureInfo.InvariantCulture, "{0}.{1}: Syn Completed", CompanyName, tablename);
                Logger(logmessage);
                log.Info(logmessage);
            }
        }

        public void LogTableRecords(string tablename, int totalrecords)
        {
            if (log.IsInfoEnabled)
            {
                tablename = ExtensionMethods.SetAlternateTableName(tablename);
                string logmessage = string.Format(CultureInfo.InvariantCulture, "{0}.{1}: {2} Record(s)", CompanyName, tablename, totalrecords);
                Logger(logmessage);
                log.Info(logmessage);
            }
        }

        public void LogTableDeleteRecords(string tablename, int totalrecords)
        {
            if (log.IsInfoEnabled)
            {
                tablename = ExtensionMethods.SetAlternateTableName(tablename);
                string logmessage = string.Format(CultureInfo.InvariantCulture, "{0}.{1}: {2} Record(s) Deleted", CompanyName, tablename, totalrecords);
                Logger(logmessage);
                log.Info(logmessage);
            }
        }

        public void LogFetchStart(string tableName)
        {
            if (log.IsInfoEnabled)
            {
                tableName = ExtensionMethods.SetAlternateTableName(tableName);
                string logmessage = string.Format(CultureInfo.InvariantCulture, "{0}.{1}: Fetch Started", CompanyName, tableName);
                Logger(logmessage);
                log.Info(logmessage);
            }
        }

        public void LogFetching(string tableName, int StatusCode, string StatusMessage, string StatusSeverity, string IterationID = "")
        {
            if (log.IsInfoEnabled)
            {
                tableName = ExtensionMethods.SetAlternateTableName(tableName);
                if (IterationID != "")
                {
                    string logmessage = string.Format(CultureInfo.InvariantCulture, "{0}.{1}.{2}.{3} : Fetching", CompanyName, tableName, StatusMessage, StatusSeverity, IterationID);
                    log.Info(logmessage);
                    Logger(logmessage);
                }
                else
                {
                    string logmessage = string.Format(CultureInfo.InvariantCulture, "{0}.{1}.{2} : Fetching", CompanyName, tableName, StatusMessage, StatusSeverity);
                    log.Info(logmessage);
                    Logger(logmessage);
                }
            }
        }

        public void LogFetchedRecords(string tablename, int totalrecords, int chunksize = 0, string IterationID = "")
        {
            if (log.IsInfoEnabled)
            {
                tablename = ExtensionMethods.SetAlternateTableName(tablename);
                if (IterationID != "")
                {
                    string logmessage = string.Format(CultureInfo.InvariantCulture, "{0}.{1}.[ChunkSize: {2}]: {3} Record(s) Fetched.", CompanyName, tablename, chunksize, totalrecords);
                    log.Info(logmessage);
                    Logger(logmessage);
                }
                else
                {
                    string logmessage = string.Format(CultureInfo.InvariantCulture, "{0}.{1}.[ChunkSize: {2}]: {3} Record(s) Fetched.", CompanyName, tablename, chunksize, totalrecords);
                    log.Info(logmessage);
                    Logger(logmessage);
                }
            }
        }

        public void LogFetchedRecords(string tablename, int totalrecords, string IterationID = "")
        {
            if (log.IsInfoEnabled)
            {
                tablename = ExtensionMethods.SetAlternateTableName(tablename);
                if (IterationID != "")
                {
                    string logmessage = string.Format(CultureInfo.InvariantCulture, "{0}.{1}: {2} Record(s) Fetched.", CompanyName, tablename, totalrecords);
                    log.Info(logmessage);
                    Logger(logmessage);
                }
                else
                {
                    string logmessage = string.Format(CultureInfo.InvariantCulture, "{0}.{1}: {2} Record(s) Fetched.", CompanyName, tablename, totalrecords);
                    log.Info(logmessage);
                    Logger(logmessage);
                }
            }
        }

        public void LogFetchCompleted(string tableName)
        {
            if (log.IsInfoEnabled)
            {
                tableName = ExtensionMethods.SetAlternateTableName(tableName);
                string logmessage = string.Format(CultureInfo.InvariantCulture, "{0}.{1}: Fetch Completed", CompanyName, tableName);
                log.Info(logmessage);
                Logger(logmessage);
            }
        }

        public void LogFetchError(string message, string tablename)
        {
            if (log.IsErrorEnabled)
            {
                tablename = ExtensionMethods.SetAlternateTableName(tablename);
                string logmessage = string.Format(CultureInfo.InvariantCulture, "{0}.{1}: {2}", CompanyName, tablename, message);
                log.Error(logmessage);
                Logger(logmessage, true);
            }
        }

        public void LogFetchError(string message, string tablename, Exception ex)
        {
            if (log.IsErrorEnabled)
            {
                tablename = ExtensionMethods.SetAlternateTableName(tablename);
                string logmessage = string.Format(CultureInfo.InvariantCulture, "{0}.{1}: {2}", CompanyName, tablename, message);
                log.Error(logmessage, ex);
                Logger(logmessage, true);
            }
        }

        public void SyncToRemoteStarted()
        {
            if (log.IsInfoEnabled)
            {
                string logmessage = string.Format(CultureInfo.InvariantCulture, "{0}: Sync to remote server started", CompanyName);
                Logger(logmessage);
                log.Info(logmessage);
            }
        }

        public void SyncToRemoteCompleted()
        {
            if (log.IsInfoEnabled)
            {
                string logmessage = string.Format(CultureInfo.InvariantCulture, "{0}: Sync to remote server completed", CompanyName);
                Logger(logmessage);
                log.Info(logmessage);
            }
        }

        public void LogUploadDatatoRemoteServerStart(string tableName)
        {
            if (log.IsInfoEnabled)
            {
                tableName = ExtensionMethods.SetAlternateTableName(tableName);
                string logmessage = string.Format(CultureInfo.InvariantCulture, "{0}.{1}: Sync to remote server Started", CompanyName, tableName);
                Logger(logmessage);
                log.Info(logmessage);
            }
        }

        public void LogUploadDatatoRemoteServerRecords(string tableName, int recordSynced, int totalRecords)
        {
            if (log.IsInfoEnabled)
            {
                tableName = ExtensionMethods.SetAlternateTableName(tableName);
                string logmessage = string.Format(CultureInfo.InvariantCulture, "{0}.{1}:{2} of {3} Record(s) Synced", CompanyName, tableName, recordSynced, totalRecords);
                Logger(logmessage);
                log.Info(logmessage);
            }
        }

        public void LogUploadDatatoRemoteServerCompleted(string tableName)
        {
            if (log.IsInfoEnabled)
            {
                tableName = ExtensionMethods.SetAlternateTableName(tableName);
                string logmessage = string.Format(CultureInfo.InvariantCulture, "{0}.{1}: Sync to remote server Completed", CompanyName, tableName);
                Logger(logmessage);
                log.Info(logmessage);
            }
        }

        public void LogError(string message, Exception ex)
        {
            if (log.IsErrorEnabled)
            {
                log.Error(message, ex);
            }
        }

        public void SSISExecuteStarted()
        {
            if (log.IsInfoEnabled)
            {
                string logmessage = string.Format(CultureInfo.InvariantCulture, "{0}: Package(s) execution started", CompanyName);
                Logger(logmessage);
                log.Info(logmessage);
            }
        }

        public void SSISExecuteCompleted()
        {
            if (log.IsInfoEnabled)
            {
                string logmessage = string.Format(CultureInfo.InvariantCulture, "{0}: Package(s) execution completed", CompanyName);
                Logger(logmessage);
                log.Info(logmessage);
            }
        }

        public static void ChangeFilePath(string newFilename)
        {
            string appenderName = "RollingLogFileAppender";
            log4net.Repository.ILoggerRepository repository = log4net.LogManager.GetRepository();
            foreach (log4net.Appender.IAppender appender in repository.GetAppenders())
            {
                if (appender.Name.CompareTo(appenderName) == 0 && appender is log4net.Appender.FileAppender)
                {
                    log4net.Appender.FileAppender fileAppender = (log4net.Appender.FileAppender)appender;
                    fileAppender.File = System.IO.Path.Combine(fileAppender.File, newFilename);
                    fileAppender.ImmediateFlush = true;
                    fileAppender.LockingModel = new FileAppender.MinimalLock();
                    fileAppender.ActivateOptions();
                }
            }
        }

        public List<string> RemoteLogs()
        {
            try
            {
                string appenderName = "RollingLogFileAppender";
                log4net.Repository.ILoggerRepository repository = log4net.LogManager.GetRepository();
                IAppender appender = repository.GetAppenders().SingleOrDefault(s => s.Name.CompareTo(appenderName) == 0 && s is log4net.Appender.FileAppender);
                if (appender != null)
                {
                    log4net.Appender.FileAppender fileappender = appender as log4net.Appender.FileAppender;
                    if (fileappender != null)
                    {
                        string filepath = fileappender.File;

                        XmlReaderSettings xrs = new XmlReaderSettings();
                        xrs.ConformanceLevel = ConformanceLevel.Fragment;

                        XDocument doc = new XDocument(new XElement("Log"));
                        XElement root = doc.Descendants().First();

                        using (StreamReader fs = new StreamReader(filepath))
                        using (XmlReader xr = XmlReader.Create(fs, xrs))
                        {
                            while (xr.Read())
                            {
                                if (xr.NodeType == XmlNodeType.Element)
                                {
                                    root.Add(XElement.Load(xr.ReadSubtree()));
                                }
                            }
                        }

                        return (from s in doc.Descendants("LogEntry")
                                where string.Compare(s.Element("GroupId").Value, SynLogger.LogGroup.ToString(), true) == 0
                                select (s.ToString())).ToList();
                                    //new RemoteLogInfo
                                    //{
                                    //    Company = Guid.Parse(s.Element("Company").Value),
                                    //    Details = s.Element("Details").Value,
                                    //    GroupId = Guid.Parse(s.Element("GroupId").Value),
                                    //    Level = s.Element("Level").Value,
                                    //    Message = s.Element("Message").Value,
                                    //    StackTrace = s.Element("StackTrace").Value,
                                    //    TimeStamp = DateTime.Parse(s.Element("TimeStamp").Value)
                                    //}).ToList();

                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void LogError(string message, Exception ex, ErrorType type)
        {
            if (type == ErrorType.INFO)
            {
                string logmessage = string.Format(CultureInfo.InvariantCulture, message, CompanyName);
                Logger(logmessage);
                log.Info(logmessage, ex);
            }
            else if (type == ErrorType.ERROR)
            {
                string logmessage = string.Format(CultureInfo.InvariantCulture, message, CompanyName);
                Logger(logmessage);
                log.Error(logmessage, ex);
            }
            else if (type == ErrorType.FATAL)
            {
                string logmessage = string.Format(CultureInfo.InvariantCulture, message, CompanyName);
                Logger(logmessage);
                log.Fatal(logmessage, ex);
            }
        }

        public void LogFetchStartReport(string tableName, DateTime? startdate, DateTime? endtime)
        {
            if (log.IsInfoEnabled)
            {
                tableName = ExtensionMethods.SetAlternateTableName(tableName);
                string logmessage = string.Empty;
                if (startdate.HasValue && endtime.HasValue)
                {
                    logmessage = string.Format(CultureInfo.InvariantCulture, "{0}.{1}: Fetch Started ({2} to {3})", CompanyName, tableName, startdate.Value.ToString("dd-MMM-yyyy"),endtime.Value.ToString("dd-MMM-yyy"));
                }
                else
                {
                    logmessage = string.Format(CultureInfo.InvariantCulture, "{0}.{1}: Fetch Started", CompanyName, tableName);
                }
                Logger(logmessage);
                log.Info(logmessage);
            }
        }
    }

    public class RemoteLogInfo
    {
        public Guid GroupId { get; set; }
        public Guid Company { get; set; }
        public string Level { get; set; }
        public string Message { get; set; }
        public string Details { get; set; }
        public string StackTrace { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
