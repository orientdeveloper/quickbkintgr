﻿//
// Class	:	ISynLogger.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QBDataFetcher.Logger
{
    public delegate void DelSynLogger(string log, bool iserror = false);

    /// <summary>
    /// Interface for SynLogger
    /// </summary>
    public interface ISynLogger
    {
        event DelSynLogger Logger;

        void LogSynStarted();
        void LogSynCompleted();

        void LogTableSynStarted(string tablename);
        void LogTableSynCompleted(string tablename);
        void LogTableRecords(string tablename, int totalrecords);
        void LogTableDeleteRecords(string tablename, int totalrecords);

        void LogFetchStart(string tableName);
        void LogFetching(string tableName, int StatusCode, string StatusMessage, string StatusSeverity, string IterationID = "");
        void LogFetchedRecords(string tablename, int totalrecords, int chunksize, string IterationID = "");
        void LogFetchedRecords(string tablename, int totalrecords, string IterationID = "");
        void LogFetchCompleted(string tableName);
        void LogFetchError(string message, string tablename);
        void LogFetchError(string message, string tablename, Exception ex);
        void SyncToRemoteStarted();
        void SyncToRemoteCompleted();

        void LogUploadDatatoRemoteServerStart(string tableName);
        void LogUploadDatatoRemoteServerRecords(string tableName,int recordSynced, int totalRecords);
        void LogUploadDatatoRemoteServerCompleted(string tableName);

        void SSISExecuteStarted();
        void SSISExecuteCompleted();

        void LogError(string message, Exception ex);

        void LogError(string message, Exception ex, ErrorType type);

        void LogFetchStartReport(string tableName,DateTime? startdate,DateTime? endtime);

    }

    public enum ErrorType
    {
        INFO,
        ERROR,
        FATAL
    }
}
