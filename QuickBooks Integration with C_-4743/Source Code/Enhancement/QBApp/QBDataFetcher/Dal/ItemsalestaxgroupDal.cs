//
// Class	:	ItemsalestaxgroupDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/6/2013 10:11:52 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class ItemsalestaxgroupDal : IDisposable, IDal
    {
        #region Class Level Variables
        bool _fullsyn;
        string _qbfile;
        DateTime? _fromsyn;
        DateTime? _tosyn;
        int _chunksize;
        string _constring;
        itemsalestaxgroupTableAdapter _adpater;
        #endregion

        #region Constructors / Destructors
         public ItemsalestaxgroupDal(string qbfile, int chunksize, DateTime? fromsyn, DateTime? tosyn, bool fullsyn, string constring)
        {
            _adpater = new itemsalestaxgroupTableAdapter();
            _adpater.Connection.ConnectionString = constring;
            _adpater.ClearBeforeFill = false;
            _fullsyn = fullsyn;
            _tosyn = tosyn;
            _fromsyn = fromsyn;
            _qbfile = qbfile;
            _chunksize = chunksize;
            _constring = constring;
        }
        #endregion
        
        #region Methods (Public)
        public void Synchronize()
        {
            try
            {
                using (ItemSalesTaxGroupFetch fetcher = new ItemSalesTaxGroupFetch(_qbfile, _chunksize, _fromsyn, _tosyn, Logger))
                {
                    while (fetcher.IsNextChunck)
                    {
                        QBdb.itemsalestaxgroupDataTable table = new Dal.QBdb.itemsalestaxgroupDataTable();
                        foreach (Itemsalestaxgroup instance in fetcher.FetchNext())
                        {
                            #region Save Master
                            if (_fullsyn)
                            {
                                DataRow dr = table.NewRow();
                                table.Rows.Add(dr);
                                Bind(dr, instance);
                            }
                            else
                            {
                                _adpater.FillBy(table, instance.ListID);

                                DataRow dr = table.Rows.Cast<DataRow>().SingleOrDefault(s => string.Compare(s.Field<string>(ItemsalestaxgroupFields.ListID), instance.ListID, true) == 0);

                                if (object.ReferenceEquals(dr, null))
                                {
                                    dr = table.NewRow();
                                    table.Rows.Add(dr);
                                }
                                Bind(dr, instance);
                            }
                            #endregion

                            #region Save Child
                            if (instance.Items.Count > 0)
                            {
                                using (ItemDal ItemDal = new ItemDal(_fullsyn, _constring, TableName.itemsalestaxgroup, instance.ListID))
                                {
                                    ItemDal.Save(instance.Items);
                                }
                            }

                            if (instance.Itemsalestaxgrouplinedetails.Count > 0)
                            {
                                using (ItemsalestaxgrouplinedetailDal ItemsalestaxgrouplinedetailDal = new ItemsalestaxgrouplinedetailDal(_fullsyn, _constring, TableName.itemsalestaxgroup, instance.ListID))
                                {
                                    ItemsalestaxgrouplinedetailDal.Save(instance.Itemsalestaxgrouplinedetails);
                                }
                            }

                            if (instance.DataExtDetails.Count > 0)
                            {
                                using (DataextdetailDal DataextdetailDal = new DataextdetailDal(_fullsyn, _constring, TableName.itemsalestaxgroup, instance.ListID))
                                {
                                    DataextdetailDal.Save(instance.DataExtDetails);
                                }
                            }
                            #endregion
                        }
                        _adpater.Update(table);
                    }
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Itemsalestaxgroup itemsalestaxgroup)
        {
            try
            {
                dr[ItemsalestaxFields.ListID] = itemsalestaxgroup.ListID;
                dr[ItemsalestaxFields.TimeCreated] = itemsalestaxgroup.TimeCreated;
                dr[ItemsalestaxFields.TimeModified] = itemsalestaxgroup.TimeModified;
                dr[ItemsalestaxFields.EditSequence] = itemsalestaxgroup.EditSequence;
                dr[ItemsalestaxFields.Name] = itemsalestaxgroup.Name;
                dr[ItemsalestaxFields.IsActive] = itemsalestaxgroup.IsActive.GetShort().GetDbType();
                dr[ItemsalestaxFields.ItemDesc] = itemsalestaxgroup.ItemDesc;
                dr[ItemsalestaxFields.CustomField1] = itemsalestaxgroup.CustomField1;
                dr[ItemsalestaxFields.CustomField2] = itemsalestaxgroup.CustomField2;
                dr[ItemsalestaxFields.CustomField3] = itemsalestaxgroup.CustomField3;
                dr[ItemsalestaxFields.CustomField4] = itemsalestaxgroup.CustomField4;
                dr[ItemsalestaxFields.CustomField5] = itemsalestaxgroup.CustomField5;
                dr[ItemsalestaxFields.CustomField6] = itemsalestaxgroup.CustomField6;
                dr[ItemsalestaxFields.CustomField7] = itemsalestaxgroup.CustomField7;
                dr[ItemsalestaxFields.CustomField8] = itemsalestaxgroup.CustomField8;
                dr[ItemsalestaxFields.CustomField9] = itemsalestaxgroup.CustomField9;
                dr[ItemsalestaxFields.CustomField10] = itemsalestaxgroup.CustomField10;
                dr[ItemsalestaxFields.Status] = itemsalestaxgroup.Status;
                dr[ItemsalestaxFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[ItemsalestaxFields.SynDate] = DBNull.Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adpater.Dispose();
                    _adpater = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion
    }
}
