//
// Class	:	AppliedtotxndetailDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class AppliedtotxndetailDal : IDisposable
    {
        #region Class Level Variables       
        bool _fullsyn;
        string _mastertable;
        string _idkey;
        appliedtotxndetailTableAdapter _adpater;
        #endregion

        #region Constructors / Destructors
        /// <summary>
        /// Constructor for initialize objects
        /// </summary>
        /// <param name="qbfile">Quickbook company file location on your system.</param>
        /// <param name="chunksize">Chunk size</param>
        /// <param name="fromsyn">Date from, used for filter</param>
        /// <param name="toSyn">Date to, used for filter</param>
        /// <param name="fullsyn">Status for fullsync</param>
        /// <param name="conString">Connection string</param>
        /// <param name="mastertable">Master table name</param>
        public AppliedtotxndetailDal(bool fullsyn, string conString, string mastertable, string idkey)
        {
            _adpater = new appliedtotxndetailTableAdapter();
            _adpater.Connection.ConnectionString = conString;
            _fullsyn = fullsyn;
            _idkey = idkey;
            _mastertable = mastertable;
        }
        #endregion

        #region Methods (Public)
        /// <summary>
        /// This method is used for calling the fetcher class for getting the data and storing it in acccess database.
        /// </summary>
        public void Save(List<Appliedtotxndetail> Appliedtotxndetails)
        {
            try
            {
                QBdb.appliedtotxndetailDataTable table = new Dal.QBdb.appliedtotxndetailDataTable();

                if (!_fullsyn)
                {
                    _adpater.DeleteBy(_idkey, _mastertable);
                }

                foreach (Appliedtotxndetail instance in Appliedtotxndetails)
                {                   
                    DataRow dr = table.NewRow();
                    table.Rows.Add(dr);
                    Bind(dr, instance);
                }
                _adpater.Update(table);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Appliedtotxndetail appliedtotxndetail)
        {
            try
            {
                dr[AppliedtotxndetailFields.TxnID] = appliedtotxndetail.TxnID;
                dr[AppliedtotxndetailFields.TxnType] = appliedtotxndetail.TxnType;
                dr[AppliedtotxndetailFields.TxnDate] = appliedtotxndetail.TxnDate.GetDbType();
                dr[AppliedtotxndetailFields.RefNumber] = appliedtotxndetail.RefNumber;
                dr[AppliedtotxndetailFields.BalanceRemaining] = appliedtotxndetail.BalanceRemaining.GetDbType();
                dr[AppliedtotxndetailFields.Amount] = appliedtotxndetail.Amount.GetDbType();
                dr[AppliedtotxndetailFields.DiscountAmount] = appliedtotxndetail.DiscountAmount.GetDbType();
                dr[AppliedtotxndetailFields.DiscountAccountRef_ListID] = appliedtotxndetail.DiscountAccountRef_ListID;
                dr[AppliedtotxndetailFields.DiscountAccountRef_FullName] = appliedtotxndetail.DiscountAccountRef_FullName;
                dr[AppliedtotxndetailFields.CustomField1] = appliedtotxndetail.CustomField1;
                dr[AppliedtotxndetailFields.CustomField2] = appliedtotxndetail.CustomField2;
                dr[AppliedtotxndetailFields.CustomField3] = appliedtotxndetail.CustomField3;
                dr[AppliedtotxndetailFields.CustomField4] = appliedtotxndetail.CustomField4;
                dr[AppliedtotxndetailFields.CustomField5] = appliedtotxndetail.CustomField5;
                dr[AppliedtotxndetailFields.CustomField6] = appliedtotxndetail.CustomField6;
                dr[AppliedtotxndetailFields.CustomField7] = appliedtotxndetail.CustomField7;
                dr[AppliedtotxndetailFields.CustomField8] = appliedtotxndetail.CustomField8;
                dr[AppliedtotxndetailFields.CustomField9] = appliedtotxndetail.CustomField9;
                dr[AppliedtotxndetailFields.CustomField10] = appliedtotxndetail.CustomField10;
                dr[AppliedtotxndetailFields.Idkey] = appliedtotxndetail.Idkey;
                dr[AppliedtotxndetailFields.GroupIDKEY] = appliedtotxndetail.GroupIDKEY;
                dr[AppliedtotxndetailFields.TableName] = appliedtotxndetail.TableName;
                dr[AppliedtotxndetailFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[AppliedtotxndetailFields.SynDate] = DBNull.Value;
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adpater.Dispose();
                    _adpater = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
