﻿//
// Class	:	COGSReportDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;
using QBDataFetcher.Dal.ReportQuery;

namespace QBDataFetcher.Dal
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class InventoryOverSoldReportDal : IDisposable, IDal
    {
        Dictionary<string, object> CustomerId;

        Dictionary<string, object> AccountId;
        Dictionary<string, object> ItemId;
        Dictionary<string, object> ClassId;
        Dictionary<string, object> AccountType;
        AcessDbQuery query;
        bool _fullSynRequired;
        int _slotmonth;
        int _noofpreviousyear;

        #region Class Level Variables
        string _qbfile;
        COGSReportTableAdapter _adapter;
        #endregion

        #region Constructors / Destructors
        public InventoryOverSoldReportDal(string qbfile, string conString, bool fullSynRequired, int slotmonth, int noofpreviousyear)
        {
            _adapter = new COGSReportTableAdapter();
            _adapter.Connection.ConnectionString = conString;
            _qbfile = qbfile;
            query = new AcessDbQuery(_adapter.Connection);
            CustomerId = new Dictionary<string, object>();
            AccountId = new Dictionary<string, object>();
            ItemId = new Dictionary<string, object>();
            ClassId = new Dictionary<string, object>();
            AccountType = new Dictionary<string, object>();
            _fullSynRequired = fullSynRequired;
            _slotmonth = slotmonth;
            _noofpreviousyear = noofpreviousyear;
        }
        #endregion

        #region Methods (Public)
        public void Synchronize()
        {
            try
            {
                DateTime? dateLastModified = null;

                if (_fullSynRequired)
                {
                    _adapter.DeleteALL(COGSTransactionType.Bill);               
                }
                else
                {
                    dateLastModified = (DateTime?)_adapter.RecentModifiedTime(COGSTransactionType.Bill);
                    if (dateLastModified.HasValue)
                    {
                        _adapter.DeleteByDate(dateLastModified.Value.Date.Year.ToString(), dateLastModified.Value.Date.Month.ToString(), dateLastModified.Value.Date.Day.ToString(), COGSTransactionType.Bill);
                    }
                    else
                    {
                        _adapter.DeleteALL(COGSTransactionType.Bill);
                    }
                }

                if (_fullSynRequired && _slotmonth == 0)
                {
                    using (InventoryOverSoldReportFetch fetcher = new InventoryOverSoldReportFetch(_qbfile, Logger, null, null))
                    {
                        QBdb.COGSReportDataTable table = new QBdb.COGSReportDataTable();
                        foreach (COGSReport instance in fetcher.FetchNext())
                        {
                            DataRow dr = table.NewRow();
                            table.Rows.Add(dr);
                            Bind(dr, instance);
                        }
                        _adapter.Update(table);
                    }
                }
                else if (_fullSynRequired && _slotmonth != 0)
                {
                    int norecordfoundcount = 0;
                    int norecordfoundcountmax = _noofpreviousyear * 12 / _slotmonth;
                    DateTime startdate = DateTime.Now;
                    while (norecordfoundcount < norecordfoundcountmax)
                    {
                        DateTime todate = startdate;
                        DateTime fromdate = startdate.AddMonths(-1 * _slotmonth);
                        startdate = fromdate.AddSeconds(-1);

                        using (InventoryOverSoldReportFetch fetcher = new InventoryOverSoldReportFetch(_qbfile, Logger, fromdate, todate))
                        {
                            List<COGSReport> report = fetcher.FetchNext();
                            if (report.Count > 0)
                            {
                                norecordfoundcount = 0;

                                QBdb.COGSReportDataTable table = new QBdb.COGSReportDataTable();
                                foreach (COGSReport instance in report)
                                {
                                    DataRow dr = table.NewRow();
                                    table.Rows.Add(dr);
                                    Bind(dr, instance);
                                }
                                _adapter.Update(table);
                            }
                            else
                            {
                                norecordfoundcount++;
                            }
                        }
                    }
                }
                else
                {
                    using (InventoryOverSoldReportFetch fetcher = new InventoryOverSoldReportFetch(_qbfile, Logger, dateLastModified, DateTime.Today))
                    {
                        QBdb.COGSReportDataTable table = new QBdb.COGSReportDataTable();
                        foreach (COGSReport instance in fetcher.FetchNext())
                        {
                            DataRow dr = table.NewRow();
                            table.Rows.Add(dr);
                            Bind(dr, instance);
                        }
                        _adapter.Update(table);
                    }
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, COGSReport _COGSReport)
        {
            try
            {
                dr[COGSReportFields.Date] = _COGSReport.Date.GetDbType();
                dr[COGSReportFields.TxnNumber] = _COGSReport.TxnNumber.GetDbType();
                dr[COGSReportFields.TxnType] = _COGSReport.TxnType;
                dr[COGSReportFields.RefNumber] = _COGSReport.RefNumber;
                dr[COGSReportFields.PONumber] = _COGSReport.PONumber;

                if (!CustomerId.Keys.Contains(_COGSReport.SourceName))
                {
                    CustomerId.Add(_COGSReport.SourceName, query.GetId(TableName.customer, CustomerFields.FullName, CustomerFields.ListID, _COGSReport.SourceName));
                }
                dr[COGSReportFields.Customer_ListID] = CustomerId[_COGSReport.SourceName];
                dr[COGSReportFields.Name] = _COGSReport.Name;
                dr[COGSReportFields.SourceName] = _COGSReport.SourceName;
                dr[COGSReportFields.Memo] = _COGSReport.Memo;
                dr[COGSReportFields.ShipDate] = _COGSReport.ShipDate.GetDbType();
                dr[COGSReportFields.DeliveryDate] = _COGSReport.DeliveryDate.GetDbType();
                dr[COGSReportFields.FOB] = _COGSReport.FOB;
                dr[COGSReportFields.ShipMethod] = _COGSReport.ShipMethod;

                if (!ItemId.Keys.Contains(_COGSReport.Item))
                {
                    ItemId.Add(_COGSReport.Item, query.GetId(TableName.items, ItemFields.FullName, ItemFields.ListID, _COGSReport.Item));
                }
                dr[COGSReportFields.Item_ListID] = ItemId[_COGSReport.Item];
                dr[COGSReportFields.Item] = _COGSReport.Item;

                string account = GetAccountName(_COGSReport.Account);
                if (!AccountId.Keys.Contains(account))
                {
                    AccountId.Add(account, query.GetId(TableName.account, AccountFields.Name, AccountFields.ListID, account));
                }
                dr[COGSReportFields.Account_ListID] = AccountId[account];
                dr[COGSReportFields.Account] = _COGSReport.Account;


                if (AccountId[account] != DBNull.Value)
                {
                    if (!AccountType.Keys.Contains(AccountId[account].ToString()))
                    {
                        AccountType.Add(AccountId[account].ToString(), query.GetAccountType(AccountId[account].ToString()));
                    }
                }
                dr[COGSReportFields.AccountType] = AccountType[AccountId[account].ToString()];

                if (!ClassId.Keys.Contains(_COGSReport.Class))
                {
                    ClassId.Add(_COGSReport.Class, query.GetId(TableName.Class, ClasFields.Name, ClasFields.ListID, _COGSReport.Class));
                }
                dr[COGSReportFields.Class_ListID] = ClassId[_COGSReport.Class];
                dr[COGSReportFields.Class] = _COGSReport.Class;
                dr[COGSReportFields.SalesTaxCode] = _COGSReport.SalesTaxCode;
                dr[COGSReportFields.ClearedStatus] = _COGSReport.ClearedStatus;
                dr[COGSReportFields.SplitAccount] = _COGSReport.SplitAccount;

                string splitAccount = GetAccountName(_COGSReport.SplitAccount);
                if (!AccountId.Keys.Contains(splitAccount))
                {
                    AccountId.Add(splitAccount, query.GetId(TableName.account, AccountFields.Name, AccountFields.ListID, splitAccount));
                }
                dr[COGSReportFields.SplitAccount_ListID] = AccountId[splitAccount];
                dr[COGSReportFields.Quantity] = _COGSReport.Quanity.GetDbType();
                dr[COGSReportFields.Debit] = _COGSReport.Debit.GetDbType();
                dr[COGSReportFields.Credit] = _COGSReport.Credit.GetDbType();
                dr[COGSReportFields.GroupIDKEY] = _COGSReport.GroupIDKEY;
                dr[COGSReportFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[COGSReportFields.SynDate] = DBNull.Value;

                dr[COGSReportFields.ModifiedTime] = _COGSReport.ModifiedTime.GetDbType();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message, ex);
                throw ex;
            }
        }

        private string GetAccountName(string account)
        {
            if (account.Contains('·'))
            {
                string[] array = account.Split('·');
                return array[1].Trim();
            }
            else
            {
                return account;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adapter.Dispose();
                    CustomerId = null;
                    AccountId = null;
                    _adapter = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion
    }
}
