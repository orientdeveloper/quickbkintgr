//
// Class	:	ItemDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:51 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class ItemDal : IDisposable
    {
        #region Class Level Variables
        bool _fullsyn;
        string _mastertable;
        string _idkey;
        itemsTableAdapter _adapter;
        #endregion

        #region Constructors / Destructors
        public ItemDal(bool fullsyn, string conString, string mastertable, string idkey)
        {
            _adapter = new itemsTableAdapter();
            _adapter.Connection.ConnectionString = conString;
            _adapter.ClearBeforeFill = false;
            _fullsyn = fullsyn;
            _idkey = idkey;
            _mastertable = mastertable;
        }
        #endregion

        #region Methods (Public)
        public void Save(List<Item> Items)
        {
            try
            {
                QBdb.itemsDataTable table = new Dal.QBdb.itemsDataTable();
                foreach (Item instance in Items)
                {
                    if (!_fullsyn)
                    {
                        _adapter.DeleteBy(instance.ListID, _mastertable);
                    }

                    DataRow dr = table.NewRow();
                    table.Rows.Add(dr);
                    Bind(dr, instance);
                }
                _adapter.Update(table);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Item items)
        {
            try
            {
                dr[ItemFields.ListID] = items.ListID;
                dr[ItemFields.FullName] = items.FullName;
                dr[ItemFields.TableName] = items.TableName;
                dr[ItemFields.GroupIDKEY] = items.GroupIDKEY;
                dr[ItemFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[ItemFields.SynDate] = DBNull.Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adapter.Dispose();
                    _adapter = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
