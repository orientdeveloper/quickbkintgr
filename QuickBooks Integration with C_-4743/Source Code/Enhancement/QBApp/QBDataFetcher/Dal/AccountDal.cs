//
// Class	:	AccountDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class AccountDal : IDisposable, IDal
    {
        #region Class Level Variables
        /// <summary>
        /// Global variables
        /// </summary>
        bool _fullsyn;
        string _qbfile;
        string _constring;
        DateTime? _fromdate;
        DateTime? _todate;
        int _chunksize;
        accountTableAdapter _adpater;
        #endregion

        #region Constructors / Destructors
        /// <summary>
        /// Constructure for the class
        /// </summary>
        /// <param name="qbfile"></param>
        /// <param name="chunksize"></param>
        /// <param name="fromdate"></param>
        /// <param name="todate"></param>
        /// <param name="fullsyn"></param>
        /// <param name="conString"></param>
        public AccountDal(string qbfile, int chunksize, DateTime? fromdate, DateTime? todate, bool fullsyn, string constring)
        {
            _adpater = new accountTableAdapter();
            _adpater.Connection.ConnectionString = constring;
            _adpater.ClearBeforeFill = false;
            _fullsyn = fullsyn;
            _fromdate = fromdate;
            _todate = todate;
            _qbfile = qbfile;
            _chunksize = chunksize;
            _constring = constring;
        }
        #endregion

        #region Methods (Public)
        /// <summary>
        /// Synchronize method for the class AccountDal
        /// </summary>
        public void Synchronize()
        {
            try
            {
                using (AccountFetch fetcher = new AccountFetch(_qbfile, _chunksize, _fromdate, _todate, Logger))
                {
                    while (fetcher.IsNextChunck)
                    {
                        QBdb.accountDataTable table = new Dal.QBdb.accountDataTable();

                        foreach (Account instance in fetcher.FetchNext())
                        {
                            #region Master Save
                            if (_fullsyn)
                            {
                                DataRow dr = table.NewRow();
                                table.Rows.Add(dr);
                                Bind(dr, instance);
                            }
                            else
                            {
                                _adpater.FillBy(table, instance.ListID);

                                DataRow dr = table.Rows.Cast<DataRow>().SingleOrDefault(s => string.Compare(s.Field<string>(AccountFields.ListID), instance.ListID, true) == 0);

                                if (object.ReferenceEquals(dr, null))
                                {
                                    dr = table.NewRow();
                                    table.Rows.Add(dr);
                                }
                                Bind(dr, instance);
                            }
                            #endregion

                            #region Save Child
                            if (instance.DataExtDetails.Count > 0)
                            {
                                using (DataextdetailDal DataextdetailDal = new DataextdetailDal(_fullsyn, _constring, TableName.account, instance.ListID))
                                {
                                    DataextdetailDal.Save(instance.DataExtDetails);
                                }
                            }
                            #endregion

                        }
                        _adpater.Update(table);
                    }
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Account account)
        {
            try
            {
                dr[AccountFields.AccountNumber] = account.AccountNumber;
                dr[AccountFields.AccountType] = account.AccountType;
                dr[AccountFields.Balance] = account.Balance.GetDbType();
                dr[AccountFields.BankNumber] = account.BankNumber;
                dr[AccountFields.CashFlowClassification] = account.CashFlowClassification;
                dr[AccountFields.CurrencyRef_FullName] = account.CurrencyRef_FullName;
                dr[AccountFields.CurrencyRef_ListID] = account.CurrencyRef_ListID;
                dr[AccountFields.CustomField1] = account.CustomField1;
                dr[AccountFields.CustomField10] = account.CustomField10;
                dr[AccountFields.CustomField2] = account.CustomField2;
                dr[AccountFields.CustomField3] = account.CustomField3;
                dr[AccountFields.CustomField4] = account.CustomField4;
                dr[AccountFields.CustomField5] = account.CustomField5;
                dr[AccountFields.CustomField6] = account.CustomField6;
                dr[AccountFields.CustomField7] = account.CustomField7;
                dr[AccountFields.CustomField8] = account.CustomField8;
                dr[AccountFields.CustomField9] = account.CustomField9;
                dr[AccountFields.Desc] = account.Desc;
                dr[AccountFields.DetailAccountType] = account.DetailAccountType;
                dr[AccountFields.EditSequence] = account.EditSequence;
                dr[AccountFields.FullName] = account.FullName;
                dr[AccountFields.IsActive] = account.IsActive.GetShort().GetDbType();
                dr[AccountFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[AccountFields.IsTaxAccount] = account.IsTaxAccount.GetShort().GetDbType();
                dr[AccountFields.LastCheckNumber] = account.LastCheckNumber;
                dr[AccountFields.ListID] = account.ListID;
                dr[AccountFields.Name] = account.Name;
                dr[AccountFields.ParentRef_FullName] = account.ParentRef_FullName;
                dr[AccountFields.ParentRef_ListID] = account.ParentRef_ListID;
                dr[AccountFields.SalesTaxCodeRef_FullName] = account.SalesTaxCodeRef_FullName;
                dr[AccountFields.SalesTaxCodeRef_ListID] = account.SalesTaxCodeRef_ListID;
                dr[AccountFields.SpecialAccountType] = account.SpecialAccountType;
                dr[AccountFields.Status] = account.Status;
                dr[AccountFields.Sublevel] = account.Sublevel.GetDbType();
                dr[AccountFields.SynDate] = DBNull.Value;
                dr[AccountFields.TaxLineID] = account.TaxLineID.GetDbType();
                dr[AccountFields.TaxLineInfo] = account.TaxLineInfo;
                dr[AccountFields.TimeCreated] = account.TimeCreated;
                dr[AccountFields.TimeModified] = account.TimeModified;
                dr[AccountFields.TotalBalance] = account.TotalBalance.GetDbType();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message, ex);
                throw ex;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        /// <summary>
        /// Dispose method for the class
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adpater.Dispose();
                    _adpater = null;
                }
                disposed = true;
            }
        }

        /// <summary>
        /// Dispose method for the class
        /// </summary>
        /// <param name="disposing"></param>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        #region Implementation IDal
        /// <summary>
        /// Logger property for the class
        /// </summary>
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion
    }
}
