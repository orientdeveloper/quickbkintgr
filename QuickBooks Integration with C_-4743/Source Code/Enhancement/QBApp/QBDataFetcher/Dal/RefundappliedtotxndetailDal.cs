//
// Class	:	RefundappliedtotxndetailDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/6/2013 10:11:54 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class RefundappliedtotxndetailDal : IDisposable
	{
		#region Class Level Variables
        bool _fullsyn;
        string _mastertable;
        string _idkey;
        refundappliedtotxndetailTableAdapter _adapter;
        #endregion

        #region Constructors / Destructors
        public RefundappliedtotxndetailDal(bool fullsyn, string conString, string mastertable, string idkey)
        {
            _adapter = new refundappliedtotxndetailTableAdapter();
            _adapter.Connection.ConnectionString = conString;
            _fullsyn = fullsyn;
            _idkey = idkey;
            _mastertable = mastertable;
        }
        #endregion

        #region Methods (Public)
        public void Save(List<Refundappliedtotxndetail> Refundappliedtotxndetails)
        {
            try
            {
                QBdb.refundappliedtotxndetailDataTable table = new Dal.QBdb.refundappliedtotxndetailDataTable();

                if (!_fullsyn)
                {
                    _adapter.DeleteBy(_idkey, _mastertable);
                }

                foreach (Refundappliedtotxndetail instance in Refundappliedtotxndetails)
                {                    
                    DataRow dr = table.NewRow();
                    table.Rows.Add(dr);
                    Bind(dr, instance);
                }
                _adapter.Update(table);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Refundappliedtotxndetail refundappliedtotxndetail)
        {
            try
            {
                dr[RefundappliedtotxndetailFields.TxnID] = refundappliedtotxndetail.TxnID;
                dr[RefundappliedtotxndetailFields.TxnType] = refundappliedtotxndetail.TxnType;
                dr[RefundappliedtotxndetailFields.TxnDate] = refundappliedtotxndetail.TxnDate.GetDbType();
                dr[RefundappliedtotxndetailFields.RefNumber] = refundappliedtotxndetail.RefNumber;
                dr[RefundappliedtotxndetailFields.CreditRemaining] = refundappliedtotxndetail.CreditRemaining.GetDbType();
                dr[RefundappliedtotxndetailFields.RefundAmount] = refundappliedtotxndetail.RefundAmount.GetDbType();
                dr[RefundappliedtotxndetailFields.CustomField1] = refundappliedtotxndetail.CustomField1;
                dr[RefundappliedtotxndetailFields.CustomField2] = refundappliedtotxndetail.CustomField2;
                dr[RefundappliedtotxndetailFields.CustomField3] = refundappliedtotxndetail.CustomField3;
                dr[RefundappliedtotxndetailFields.CustomField4] = refundappliedtotxndetail.CustomField4;
                dr[RefundappliedtotxndetailFields.CustomField5] = refundappliedtotxndetail.CustomField5;
                dr[RefundappliedtotxndetailFields.CustomField6] = refundappliedtotxndetail.CustomField6;
                dr[RefundappliedtotxndetailFields.CustomField7] = refundappliedtotxndetail.CustomField7;
                dr[RefundappliedtotxndetailFields.CustomField8] = refundappliedtotxndetail.CustomField8;
                dr[RefundappliedtotxndetailFields.CustomField9] = refundappliedtotxndetail.CustomField9;
                dr[RefundappliedtotxndetailFields.CustomField10] = refundappliedtotxndetail.CustomField10;
                dr[RefundappliedtotxndetailFields.Idkey] = refundappliedtotxndetail.Idkey;
                dr[RefundappliedtotxndetailFields.GroupIDKEY] = refundappliedtotxndetail.GroupIDKEY;
                dr[RefundappliedtotxndetailFields.TableName] = refundappliedtotxndetail.TableName;
                dr[RefundappliedtotxndetailFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[RefundappliedtotxndetailFields.SynDate] = DBNull.Value;
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adapter.Dispose();
                    _adapter = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion
	}
}
