﻿//
// Class	:	AcessDbQuery.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:42 AM
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;

namespace QBDataFetcher.Dal.ReportQuery
{
    /// <summary>
    /// This class is used to get the number of records in each table of the database
    /// </summary>
    public class AcessDbQuery
    {
        OleDbConnection _connection;
        public AcessDbQuery(OleDbConnection connection)
        {
            _connection = connection;
        }
        public object GetId(string table, string coloumn, string targetcoloumn, string value)
        {
            if (_connection.State == System.Data.ConnectionState.Closed)
                _connection.Open();
            string strcommand = string.Format("select [{0}] from [{1}] where [{2}] = ?", targetcoloumn, table, coloumn);
            OleDbCommand command = new OleDbCommand(strcommand, _connection);
            command.Parameters.AddWithValue(coloumn,value);
            object result = command.ExecuteScalar();
            if (result != null)
            {
                return result;
            }
            else
            {
                return DBNull.Value;
            }
        }

        public object GetAccountType(string value)
        {
            if (_connection.State == System.Data.ConnectionState.Closed)
                _connection.Open();
            string strcommand = string.Format("select [AccountType] from account where [ListID] = ?", value);
            OleDbCommand command = new OleDbCommand(strcommand, _connection);
            command.Parameters.AddWithValue("ListID", value);
            object result = command.ExecuteScalar();
            if (result != null)
            {
                return result;
            }
            else
            {
                return DBNull.Value;
            }
        }
    }
}
