//
// Class	:	SalesandpurchasedetailDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/8/2013 10:11:55 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class SalesandpurchasedetailDal : IDisposable
	{
        #region Class Level Variables
        bool _fullsyn;
        string _mastertable;
        string _idkey;
        salesandpurchasedetailTableAdapter _adapter;
        #endregion

        #region Constructors / Destructors
        public SalesandpurchasedetailDal(bool fullsyn, string conString, string mastertable, string idkey)
        {
            _adapter = new salesandpurchasedetailTableAdapter();
            _adapter.Connection.ConnectionString = conString;
            _fullsyn = fullsyn;
            _idkey = idkey;
            _mastertable = mastertable;
        }
        #endregion

        #region Methods (Public)
        public void Save(List<Salesandpurchasedetail> Salesandpurchasedetails)
        {
            try
            {
                QBdb.salesandpurchasedetailDataTable table = new Dal.QBdb.salesandpurchasedetailDataTable();

                if (!_fullsyn)
                {
                    _adapter.DeleteBy(_idkey, _mastertable);
                }

                foreach (Salesandpurchasedetail instance in Salesandpurchasedetails)
                {                    
                    DataRow dr = table.NewRow();
                    table.Rows.Add(dr);
                    Bind(dr, instance);
                }
                _adapter.Update(table);
            }
            catch 
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Salesandpurchasedetail salesandpurchasedetail)
        {
            try
            {
                dr[SalesandpurchasedetailFields.SalesDesc] = salesandpurchasedetail.SalesDesc;
                dr[SalesandpurchasedetailFields.SalesPrice] = salesandpurchasedetail.SalesPrice;
                dr[SalesandpurchasedetailFields.IncomeAccountRef_ListID] = salesandpurchasedetail.IncomeAccountRef_ListID;
                dr[SalesandpurchasedetailFields.IncomeAccountRef_FullName] = salesandpurchasedetail.IncomeAccountRef_FullName;
                dr[SalesandpurchasedetailFields.PurchaseDesc] = salesandpurchasedetail.PurchaseDesc;
                dr[SalesandpurchasedetailFields.PurchaseCost] = salesandpurchasedetail.PurchaseCost;
                dr[SalesandpurchasedetailFields.PurchaseTaxCodeRef_ListID] = salesandpurchasedetail.PurchaseTaxCodeRef_ListID;
                dr[SalesandpurchasedetailFields.PurchaseTaxCodeRef_FullName] = salesandpurchasedetail.PurchaseTaxCodeRef_FullName;
                dr[SalesandpurchasedetailFields.ExpenseAccountRef_ListID] = salesandpurchasedetail.ExpenseAccountRef_ListID;
                dr[SalesandpurchasedetailFields.ExpenseAccountRef_FullName] = salesandpurchasedetail.ExpenseAccountRef_FullName;
                dr[SalesandpurchasedetailFields.PrefVendorRef_ListID] = salesandpurchasedetail.PrefVendorRef_ListID;
                dr[SalesandpurchasedetailFields.PrefVendorRef_FullName] = salesandpurchasedetail.PrefVendorRef_FullName;

                dr[SalesandpurchasedetailFields.CustomField1] = salesandpurchasedetail.CustomField1;
                dr[SalesandpurchasedetailFields.CustomField2] = salesandpurchasedetail.CustomField2;
                dr[SalesandpurchasedetailFields.CustomField3] = salesandpurchasedetail.CustomField3;
                dr[SalesandpurchasedetailFields.CustomField4] = salesandpurchasedetail.CustomField4;
                dr[SalesandpurchasedetailFields.CustomField5] = salesandpurchasedetail.CustomField5;
                dr[SalesandpurchasedetailFields.CustomField6] = salesandpurchasedetail.CustomField6;
                dr[SalesandpurchasedetailFields.CustomField7] = salesandpurchasedetail.CustomField7;
                dr[SalesandpurchasedetailFields.CustomField8] = salesandpurchasedetail.CustomField8;
                dr[SalesandpurchasedetailFields.CustomField9] = salesandpurchasedetail.CustomField9;
                dr[SalesandpurchasedetailFields.CustomField10] = salesandpurchasedetail.CustomField10;
                dr[SalesandpurchasedetailFields.Idkey] = salesandpurchasedetail.Idkey;
                dr[SalesandpurchasedetailFields.GroupIDKEY] = salesandpurchasedetail.GroupIDKEY;

                dr[SalesandpurchasedetailFields.TableName] = salesandpurchasedetail.TableName;
                dr[SalesandpurchasedetailFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[SalesandpurchasedetailFields.SynDate] = DBNull.Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adapter.Dispose();
                    _adapter = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
	}
}
