﻿//
// Class	:	CompanyDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:42 AM
//
using QBDataFetcher.Dal.QBCompaniesTableAdapters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;

namespace QBDataFetcher.Dal.UI
{
    /// <summary>
    /// This class is used to create, edit and delete comapny from the database.
    /// </summary>
    public class CompanyDal
    {
        string _constr;

        public CompanyDal(string constr)
        {
            _constr = constr;
        }

        public QBCompanies.CompaniesDataTable GetCompanies()
        {
            try
            {
                CompaniesTableAdapter adpater = new CompaniesTableAdapter();
                adpater.Connection.ConnectionString = _constr;
                QBCompanies.CompaniesDataTable table = new QBCompanies.CompaniesDataTable();
                adpater.Fill(table);
                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public QBCompanies.CompaniesDataTable GetCompanyId(Guid companyId)
        {
            try
            {
                CompaniesTableAdapter adpater = new CompaniesTableAdapter();
                adpater.Connection.ConnectionString = _constr;
                QBCompanies.CompaniesDataTable table = adpater.GetDataByCompanyId(companyId);
                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateSynDate(Guid CompanyId, DateTime? Date)
        {
            try
            {
                CompaniesTableAdapter adpater = new CompaniesTableAdapter();
                adpater.Connection.ConnectionString = _constr;
                adpater.UpdateLastSyn(Date, CompanyId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Insert(Guid companyId, string companyname, string filepath, string companykey, List<string> tables)
        {
            try
            {
                OleDbConnection connection = new OleDbConnection(_constr);
                connection.Open();
                OleDbTransaction transaction = connection.BeginTransaction();

                try
                {
                    CompaniesTableAdapter adpater = new CompaniesTableAdapter();
                    adpater.Connection = connection;
                    adpater.Transaction = transaction;

                    string connectionString = string.Format(@"\{0}\qbw.mdb", companyId);
                    adpater.Insert(companyId, companyname, filepath, connectionString, DateTime.Now, DateTime.Now, "true", null);

                    transaction.Commit();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                        connection.Dispose();
                        connection = null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ClearPreviousLog(Guid companyId)
        {
            try
            {
                OleDbConnection connection = new OleDbConnection(_constr);
                connection.Open();
                OleDbTransaction transaction = connection.BeginTransaction();

                try
                {
                    //delete the logs
                    SyncLogTableAdapter synclogadapter = new SyncLogTableAdapter();
                    synclogadapter.Connection = connection;
                    synclogadapter.Transaction = transaction;

                    SyncLogDetailsTableAdapter synclogdetailadpater = new SyncLogDetailsTableAdapter();
                    synclogdetailadpater.Connection = connection;
                    synclogdetailadpater.Transaction = transaction;

                    foreach (var synclog in synclogadapter.GetSyncLogByCompany(companyId.ToString()))
                    {
                        synclogdetailadpater.DeleteAllBySyncId(synclog.SyncId);
                    }

                    synclogadapter.DeleteAllByCompanyId(companyId.ToString());

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                        connection.Dispose();
                        connection = null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid companyId)
        {
            try
            {
                OleDbConnection connection = new OleDbConnection(_constr);
                connection.Open();
                OleDbTransaction transaction = connection.BeginTransaction();

                try
                {
                    //delete the company
                    CompaniesTableAdapter adpater = new CompaniesTableAdapter();
                    adpater.Connection = connection;
                    adpater.Transaction = transaction;
                    adpater.DeleteByCompanyID(companyId);

                    //delete the logs
                    SyncLogTableAdapter synclogadapter = new SyncLogTableAdapter();
                    synclogadapter.Connection = connection;
                    synclogadapter.Transaction = transaction;

                    SyncLogDetailsTableAdapter synclogdetailadpater = new SyncLogDetailsTableAdapter();
                    synclogdetailadpater.Connection = connection;
                    synclogdetailadpater.Transaction = transaction;

                    foreach (var synclog in synclogadapter.GetSyncLogByCompany(companyId.ToString()))
                    {
                        synclogdetailadpater.DeleteAllBySyncId(synclog.SyncId);
                    }

                    synclogadapter.DeleteAllByCompanyId(companyId.ToString());
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                        connection.Dispose();
                        connection = null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateFilePath(Guid CompanyId, string Path)
        {
            try
            {
                CompaniesTableAdapter adpater = new CompaniesTableAdapter();
                adpater.Connection.ConnectionString = _constr;
                adpater.UpdateFilePath(Path, CompanyId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public QBCompanies.SettingsDataTable GetSettings()
        {
            using (SettingsTableAdapter _adpater = new SettingsTableAdapter())
            {
                _adpater.Connection.ConnectionString = _constr;
                return _adpater.GetData();
            }
        }

        public string GetSettingValue(string settingname)
        {
            using (SettingsTableAdapter _adpater = new SettingsTableAdapter())
            {
                _adpater.Connection.ConnectionString = _constr;
                return _adpater.GetSettingValue(settingname);
            }
        }

        public void SetSettingValue(string settingname, string settingvalue)
        {
            using (SettingsTableAdapter _adpater = new SettingsTableAdapter())
            {
                _adpater.Connection.ConnectionString = _constr;
                if (string.IsNullOrEmpty(_adpater.GetSettingValue(settingname)))
                {
                    _adpater.Insert(settingname, settingvalue);
                }
                else
                {
                    _adpater.UpdateSettingValue(settingvalue, settingname);
                }
            }
        }

        public void InsertSyncLog(Guid companyId, string SyncId, string SyncInitiateType, DateTime? SyncStartTime, DateTime? SyncEndTime, short Status, short GeneralLogSent, DateTime? SyncDate, string iscompletesync)
        {
            try
            {
                OleDbConnection connection = new OleDbConnection(_constr);
                connection.Open();
                OleDbTransaction transaction = connection.BeginTransaction();

                try
                {
                    SyncLogTableAdapter adpater = new SyncLogTableAdapter();
                    adpater.Connection = connection;
                    adpater.Transaction = transaction;

                    string connectionString = string.Format(@"\{0}\qbw.mdb", companyId);
                    adpater.Insert(SyncId, SyncInitiateType, SyncStartTime, SyncEndTime, Status, GeneralLogSent, SyncDate, companyId.ToString(), iscompletesync);
                    SyncLogTableAdapter adaptertable = new SyncLogTableAdapter();
                    adaptertable.Connection = connection;
                    adaptertable.Transaction = transaction;
                    transaction.Commit();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                        connection.Dispose();
                        connection = null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertSyncLogDetail(Guid companyId, string Id, string SyncId, string tablename, Byte? LocalSynStatus)
        {
            try
            {
                OleDbConnection connection = new OleDbConnection(_constr);
                connection.Open();
                OleDbTransaction transaction = connection.BeginTransaction();

                try
                {
                    SyncLogDetailsTableAdapter adpater = new SyncLogDetailsTableAdapter();
                    adpater.Connection = connection;
                    adpater.Transaction = transaction;

                    string connectionString = string.Format(@"\{0}\qbw.mdb", companyId);
                    adpater.InsertSyncLogDetails(Id, SyncId, tablename, LocalSynStatus);
                    SyncLogDetailsTableAdapter adaptertable = new SyncLogDetailsTableAdapter();
                    adaptertable.Connection = connection;
                    adaptertable.Transaction = transaction;
                    transaction.Commit();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                        connection.Dispose();
                        connection = null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateSyncMasterCompleteLog(Guid syncid, byte? status, DateTime? syncendtime)
        {
            try
            {
                SyncLogTableAdapter adpater = new SyncLogTableAdapter();
                adpater.Connection.ConnectionString = _constr;
                adpater.SyncMasterCompleteLogQuery(status, syncendtime, syncid.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SyncLocalStartLog(Guid companyId, string Id, string SyncId, string TableName, DateTime? LocalSyncStartTime)
        {
            try
            {
                OleDbConnection connection = new OleDbConnection(_constr);
                connection.Open();
                OleDbTransaction transaction = connection.BeginTransaction();

                try
                {
                    SyncLogDetailsTableAdapter adpater = new SyncLogDetailsTableAdapter();
                    adpater.Connection = connection;
                    adpater.Transaction = transaction;

                    string connectionString = string.Format(@"\{0}\qbw.mdb", companyId);
                    adpater.SyncLocalStartLogQuery(LocalSyncStartTime, SyncId, TableName);
                    SyncLogDetailsTableAdapter adaptertable = new SyncLogDetailsTableAdapter();
                    adaptertable.Connection = connection;
                    adaptertable.Transaction = transaction;
                    transaction.Commit();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                        connection.Dispose();
                        connection = null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SyncLocalCompleted(string syncid, string tablename, DateTime endtime, byte status)
        {
            try
            {
                SyncLogDetailsTableAdapter adpater = new SyncLogDetailsTableAdapter();
                adpater.Connection.ConnectionString = _constr;
                adpater.SyncLocalCompletedLogQuery(endtime, status, syncid, tablename);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SyncRemoteStart(string syncid, string tablename, DateTime starttime)
        {
            try
            {
                SyncLogDetailsTableAdapter adpater = new SyncLogDetailsTableAdapter();
                adpater.Connection.ConnectionString = _constr;
                adpater.SyncRemoteStartQuery(starttime, syncid, tablename);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SyncRemoteCompleted(string syncid, string tablename, DateTime endtime)
        {
            try
            {
                SyncLogDetailsTableAdapter adpater = new SyncLogDetailsTableAdapter();
                adpater.Connection.ConnectionString = _constr;
                adpater.SyncRemoteCompletedQuery(endtime, syncid, tablename);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SyncUpdateStartTime(string syncid, DateTime starttime)
        {
            try
            {
                SyncLogTableAdapter adaptertable = new SyncLogTableAdapter();
                adaptertable.Connection.ConnectionString = _constr;
                adaptertable.UpdateSynStartTime(starttime, syncid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertRecentLog(Guid companyId, string Id, string GroupId, string Type, string Message, string Details, string StackTrace, DateTime? TimeStamp, DateTime? EntryDate)
        {
            try
            {
                OleDbConnection connection = new OleDbConnection(_constr);
                connection.Open();
                OleDbTransaction transaction = connection.BeginTransaction();

                try
                {
                    RecentLogTableAdapter adpater = new RecentLogTableAdapter();
                    adpater.Connection = connection;
                    adpater.Transaction = transaction;

                    string connectionString = string.Format(@"\{0}\qbw.mdb", companyId);
                    adpater.Insert(Id, GroupId, companyId.ToString(), Type, Message, Details, StackTrace, TimeStamp, EntryDate);
                    RecentLogTableAdapter adaptertable = new RecentLogTableAdapter();
                    adaptertable.Connection = connection;
                    adaptertable.Transaction = transaction;
                    transaction.Commit();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                        connection.Dispose();
                        connection = null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertRecentErrorLog(string Id, Guid companyId, string ExceptionMessage, String Message, String ExceptionLevel, DateTime? Dated)
        {
            try
            {
                OleDbConnection connection = new OleDbConnection(_constr);
                connection.Open();
                OleDbTransaction transaction = connection.BeginTransaction();

                try
                {
                    RecentErrorLogTableAdapter adpater = new RecentErrorLogTableAdapter();
                    adpater.Connection = connection;
                    adpater.Transaction = transaction;

                    string connectionString = string.Format(@"\{0}\qbw.mdb", companyId);
                    adpater.Insert(Id, companyId.ToString(), ExceptionMessage, Message, ExceptionLevel, Dated);
                    RecentErrorLogTableAdapter adaptertable = new RecentErrorLogTableAdapter();
                    adaptertable.Connection = connection;
                    adaptertable.Transaction = transaction;
                    transaction.Commit();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                        connection.Dispose();
                        connection = null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateSyncLog(Guid companyid, string syncid, DateTime? SyncEndTime)
        {
            try
            {
                SyncLogTableAdapter adpater = new SyncLogTableAdapter();
                adpater.Connection.ConnectionString = _constr;
                adpater.UpdateSyncLog(SyncEndTime, companyid.ToString(), syncid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public QBCompanies.SyncLogDataTable GetSyncLog(Guid companyid)
        {
            try
            {
                SyncLogTableAdapter adpater = new SyncLogTableAdapter();
                adpater.Connection.ConnectionString = _constr;
                return adpater.GetSyncLogByCompany(companyid.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public QBCompanies.SyncLogDetailsDataTable GetSyncDetailLogBySyncId(string syncid)
        {
            try
            {
                SyncLogDetailsTableAdapter adpater = new SyncLogDetailsTableAdapter();
                adpater.Connection.ConnectionString = _constr;
                return adpater.GetDataBySyncId(syncid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
