//
// Class	:	ItemotherchargeDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/5/2013 10:11:51 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class ItemotherchargeDal : IDisposable, IDal
    {
        #region Class Level Variables
        bool _fullsyn;
        string _qbfile;
        DateTime? _fromsyn;
        DateTime? _tosyn;
        int _chunksize;
        string _constring;
        itemotherchargeTableAdapter _adpater;
        #endregion

        #region Constructors / Destructors
        public ItemotherchargeDal(string qbfile, int chunksize, DateTime? fromsyn, DateTime? tosyn, bool fullsyn, string constring)
        {
            _adpater = new itemotherchargeTableAdapter();
            _adpater.Connection.ConnectionString = constring;
            _adpater.ClearBeforeFill = false;
            _fullsyn = fullsyn;
            _fromsyn = fromsyn;
            _tosyn = tosyn;
            _qbfile = qbfile;
            _chunksize = chunksize;
            _constring = constring;
        }
        #endregion

        #region Methods (Public)
        public void Synchronize()
        {
            try
            {
                using (ItemOtherChargeFetch fetcher = new ItemOtherChargeFetch(_qbfile, _chunksize, _fromsyn, _tosyn, Logger))
                {
                    while (fetcher.IsNextChunck)
                    {
                        QBdb.itemotherchargeDataTable table = new Dal.QBdb.itemotherchargeDataTable();
                        foreach (Itemothercharge instance in fetcher.FetchNext())
                        {
                            #region Save Master
                            if (_fullsyn)
                            {
                                DataRow dr = table.NewRow();
                                table.Rows.Add(dr);
                                Bind(dr, instance);
                            }
                            else
                            {
                                _adpater.FillBy(table, instance.ListID);

                                DataRow dr = table.Rows.Cast<DataRow>().SingleOrDefault(s => string.Compare(s.Field<string>(ItemotherchargeFields.ListID), instance.ListID, true) == 0);

                                if (object.ReferenceEquals(dr, null))
                                {
                                    dr = table.NewRow();
                                    table.Rows.Add(dr);
                                }
                                Bind(dr, instance);
                            }
                            #endregion                            

                            #region Save Child
                            if (instance.Items.Count > 0)
                            {
                                using (ItemDal ItemDal = new ItemDal(_fullsyn, _constring, TableName.itemothercharge, instance.ListID))
                                {
                                    ItemDal.Save(instance.Items);
                                }
                            }

                            if (instance.Salesandpurchasedetails.Count > 0)
                            {
                                using (SalesandpurchasedetailDal SalesandpurchasedetailDal = new SalesandpurchasedetailDal(_fullsyn, _constring, TableName.itemothercharge, instance.ListID))
                                {
                                    SalesandpurchasedetailDal.Save(instance.Salesandpurchasedetails);
                                }
                            }

                            if (instance.Salesorpurchasedetails.Count > 0)
                            {
                                using (SalesorpurchasedetailDal SalesorpurchasedetailDal = new SalesorpurchasedetailDal(_fullsyn, _constring, TableName.itemothercharge, instance.ListID))
                                {
                                    SalesorpurchasedetailDal.Save(instance.Salesorpurchasedetails);
                                }
                            }

                            if (instance.DataExtDetails.Count > 0)
                            {
                                using (DataextdetailDal DataextdetailDal = new DataextdetailDal(_fullsyn, _constring, TableName.itemothercharge, instance.ListID))
                                {
                                    DataextdetailDal.Save(instance.DataExtDetails);
                                }
                            }
                            #endregion
                        }
                        _adpater.Update(table);
                    }
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Itemothercharge itemothercharge)
        {
            try
            {
                dr[ItemotherchargeFields.ListID] = itemothercharge.ListID;
                dr[ItemotherchargeFields.TimeCreated] = itemothercharge.TimeCreated;
                dr[ItemotherchargeFields.TimeModified] = itemothercharge.TimeModified;
                dr[ItemotherchargeFields.EditSequence] = itemothercharge.EditSequence;
                dr[ItemotherchargeFields.Name] = itemothercharge.Name;
                dr[ItemotherchargeFields.FullName] = itemothercharge.FullName;
                dr[ItemotherchargeFields.IsActive] = itemothercharge.IsActive.GetShort().GetDbType();
                dr[ItemotherchargeFields.ParentRef_FullName] = itemothercharge.ParentRef_FullName;
                dr[ItemotherchargeFields.ParentRef_ListID] = itemothercharge.ParentRef_ListID;
                dr[ItemotherchargeFields.Sublevel] = itemothercharge.Sublevel.GetDbType();
                dr[ItemotherchargeFields.IsTaxIncluded] = itemothercharge.IsTaxIncluded.GetShort().GetDbType();
                dr[ItemotherchargeFields.SalesTaxCodeRef_FullName] = itemothercharge.SalesTaxCodeRef_FullName;
                dr[ItemotherchargeFields.SalesTaxCodeRef_ListID] = itemothercharge.SalesTaxCodeRef_ListID;
                dr[ItemotherchargeFields.SpecialItemType] = itemothercharge.SpecialItemType;
                dr[ItemotherchargeFields.CustomField1] = itemothercharge.CustomField1;
                dr[ItemotherchargeFields.CustomField10] = itemothercharge.CustomField10;
                dr[ItemotherchargeFields.CustomField2] = itemothercharge.CustomField2;
                dr[ItemotherchargeFields.CustomField2] = itemothercharge.CustomField3;
                dr[ItemotherchargeFields.CustomField2] = itemothercharge.CustomField4;
                dr[ItemotherchargeFields.CustomField2] = itemothercharge.CustomField5;
                dr[ItemotherchargeFields.CustomField2] = itemothercharge.CustomField6;
                dr[ItemotherchargeFields.CustomField2] = itemothercharge.CustomField7;
                dr[ItemotherchargeFields.CustomField2] = itemothercharge.CustomField8;
                dr[ItemotherchargeFields.CustomField2] = itemothercharge.CustomField9;
                dr[ItemotherchargeFields.Status] = itemothercharge.Status;
                dr[ItemotherchargeFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[ItemotherchargeFields.SynDate] = DBNull.Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adpater.Dispose();
                    _adpater = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion
    }
}
