//
// Class	:	BillpaymentcheckpaymentcheckDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:43 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class BillpaymentcheckDal : IDisposable, IDal
    {
        #region Class Level Variables
        bool _fullsyn;
        string _qbfile;
        DateTime? _fromsyn;
        DateTime? _tosyn;
        int _chunksize;
        string _conString;
        billpaymentcheckTableAdapter _adpater;
        #endregion

        #region Constructors / Destructors
        public BillpaymentcheckDal(string qbfile, int chunksize, DateTime? fromsyn, DateTime? tosyn, bool fullsyn, string conString)
        {
            _adpater = new billpaymentcheckTableAdapter();
            _adpater.Connection.ConnectionString = conString;
            _adpater.ClearBeforeFill = false;
            _fullsyn = fullsyn;
            _fromsyn = fromsyn;
            _tosyn = tosyn;
            _qbfile = qbfile;
            _chunksize = chunksize;
            _conString = conString;
        }
        #endregion

        #region Methods (Public)
        public void Synchronize()
        {
            try
            {
                using (BillPaymentCheckFetch fetcher = new BillPaymentCheckFetch(_qbfile, _chunksize, _fromsyn, _tosyn, Logger))
                {
                    while (fetcher.IsNextChunck)
                    {
                        QBdb.billpaymentcheckDataTable table = new Dal.QBdb.billpaymentcheckDataTable();
                        foreach (Billpaymentcheck instance in fetcher.FetchNext())
                        {
                            #region Save Master
                            if (_fullsyn)
                            {
                                DataRow dr = table.NewRow();
                                table.Rows.Add(dr);
                                Bind(dr, instance);
                            }
                            else
                            {
                                _adpater.FillBy(table, instance.TxnID);

                                DataRow dr = table.Rows.Cast<DataRow>().SingleOrDefault(s => string.Compare(s.Field<string>(BillpaymentcheckFields.TxnID), instance.TxnID, true) == 0);

                                if (object.ReferenceEquals(dr, null))
                                {
                                    dr = table.NewRow();
                                    table.Rows.Add(dr);
                                }
                                Bind(dr, instance);
                            }
                            #endregion

                            #region Save Child
                            if (instance.Appliedtotxndetails.Count > 0)
                            {
                                using (AppliedtotxndetailDal AppliedtotxndetailDal = new AppliedtotxndetailDal(_fullsyn, _conString, TableName.billpaymentcheck, instance.TxnID))
                                {
                                    AppliedtotxndetailDal.Save(instance.Appliedtotxndetails);
                                }
                            }

                            if (instance.Linkedtxndetails.Count > 0)
                            {
                                using (LinkedtxndetailDal LinkedtxndetailDal = new LinkedtxndetailDal(_fullsyn, _conString, TableName.billpaymentcheck, instance.TxnID))
                                {
                                    LinkedtxndetailDal.Save(instance.Linkedtxndetails);
                                }
                            }
                            #endregion
                        }
                        _adpater.Update(table);
                    }
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Billpaymentcheck billpaymentcheck)
        {
            try
            {
                dr[BillpaymentcheckFields.Address_Addr1] = billpaymentcheck.Address_Addr1;
                dr[BillpaymentcheckFields.Address_Addr2] = billpaymentcheck.Address_Addr2;
                dr[BillpaymentcheckFields.Address_Addr3] = billpaymentcheck.Address_Addr3;
                dr[BillpaymentcheckFields.Address_Addr4] = billpaymentcheck.Address_Addr4;
                dr[BillpaymentcheckFields.Address_Addr5] = billpaymentcheck.Address_Addr5;
                dr[BillpaymentcheckFields.Address_City] = billpaymentcheck.Address_City;
                dr[BillpaymentcheckFields.Address_Country] = billpaymentcheck.Address_Country;
                dr[BillpaymentcheckFields.Address_Note] = billpaymentcheck.Address_Note;
                dr[BillpaymentcheckFields.Address_PostalCode] = billpaymentcheck.Address_PostalCode;
                dr[BillpaymentcheckFields.Address_State] = billpaymentcheck.Address_State;
                dr[BillpaymentcheckFields.Amount] = billpaymentcheck.Amount.GetDbType();
                dr[BillpaymentcheckFields.AmountInHomeCurrency] = billpaymentcheck.AmountInHomeCurrency.GetDbType();
                dr[BillpaymentcheckFields.APAccountRef_ListID] = billpaymentcheck.APAccountRef_ListID;
                dr[BillpaymentcheckFields.APAccountRef_FullName] = billpaymentcheck.APAccountRef_FullName;
                dr[BillpaymentcheckFields.BankAccountRef_FullName] = billpaymentcheck.BankAccountRef_FullName;
                dr[BillpaymentcheckFields.BankAccountRef_ListID] = billpaymentcheck.BankAccountRef_ListID;
                dr[BillpaymentcheckFields.CurrencyRef_FullName] = billpaymentcheck.CurrencyRef_FullName;
                dr[BillpaymentcheckFields.CurrencyRef_ListID] = billpaymentcheck.CurrencyRef_ListID;
                dr[BillpaymentcheckFields.CustomField1] = billpaymentcheck.CustomField1;
                dr[BillpaymentcheckFields.CustomField2] = billpaymentcheck.CustomField2;
                dr[BillpaymentcheckFields.CustomField3] = billpaymentcheck.CustomField3;
                dr[BillpaymentcheckFields.CustomField4] = billpaymentcheck.CustomField4;
                dr[BillpaymentcheckFields.CustomField5] = billpaymentcheck.CustomField5;
                dr[BillpaymentcheckFields.CustomField6] = billpaymentcheck.CustomField6;
                dr[BillpaymentcheckFields.CustomField7] = billpaymentcheck.CustomField7;
                dr[BillpaymentcheckFields.CustomField8] = billpaymentcheck.CustomField8;
                dr[BillpaymentcheckFields.CustomField9] = billpaymentcheck.CustomField9;
                dr[BillpaymentcheckFields.CustomField10] = billpaymentcheck.CustomField10;
                dr[BillpaymentcheckFields.IsToBePrinted] = billpaymentcheck.IsToBePrinted.GetShort().GetDbType();
                dr[BillpaymentcheckFields.EditSequence] = billpaymentcheck.EditSequence;
                dr[BillpaymentcheckFields.ExchangeRate] = billpaymentcheck.ExchangeRate.GetDbType();
                dr[BillpaymentcheckFields.Memo] = billpaymentcheck.Memo;
                dr[BillpaymentcheckFields.RefNumber] = billpaymentcheck.RefNumber;
                dr[BillpaymentcheckFields.Status] = billpaymentcheck.Status;
                dr[BillpaymentcheckFields.TimeCreated] = billpaymentcheck.TimeCreated;
                dr[BillpaymentcheckFields.TimeModified] = billpaymentcheck.TimeModified;
                dr[BillpaymentcheckFields.TxnDate] = billpaymentcheck.TxnDate.GetDbType();
                dr[BillpaymentcheckFields.TxnID] = billpaymentcheck.TxnID;
                dr[BillpaymentcheckFields.TxnNumber] = billpaymentcheck.TxnNumber.GetDbType();
                dr[BillpaymentcheckFields.PayeeEntityRef_FullName] = billpaymentcheck.PayeeEntityRef_FullName;
                dr[BillpaymentcheckFields.PayeeEntityRef_ListID] = billpaymentcheck.PayeeEntityRef_ListID;
                dr[BillpaymentcheckFields.IsSyn] = (short)SynStatus.Synchronize;
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adpater.Dispose();
                    _adpater = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion
    }
}
