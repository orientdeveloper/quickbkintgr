//
// Class	:	ReceivepaymentDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:54 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class ReceivepaymentDal : IDisposable, IDal
    {
        #region Class Level Variables
        bool _fullsyn;
        string _qbfile;
        DateTime? _fromsyn;
        DateTime? _tosyn;
        int _chunksize;
        string _constring;
        receivepaymentTableAdapter _adpater;
        #endregion

        #region Constructors / Destructors
        public ReceivepaymentDal(string qbfile, int chunksize, DateTime? fromsyn, DateTime? tosyn, bool fullsyn, string constring)
        {
            _adpater = new receivepaymentTableAdapter();
            _adpater.Connection.ConnectionString = constring;
            _adpater.ClearBeforeFill = false;
            _fullsyn = fullsyn;
            _fromsyn = fromsyn;
            _tosyn = tosyn;
            _qbfile = qbfile;
            _chunksize = chunksize;
            _constring = constring;
        }
        #endregion

        #region Methods (Public)
        public void Synchronize()
        {
            try
            {
                using (ReceivePaymentFetch fetcher = new ReceivePaymentFetch(_qbfile, _chunksize, _fromsyn, _tosyn, Logger))
                {
                    while (fetcher.IsNextChunck)
                    {
                        QBdb.receivepaymentDataTable table = new Dal.QBdb.receivepaymentDataTable();
                        foreach (Receivepayment instance in fetcher.FetchNext())
                        {
                            #region Save Master
                            if (_fullsyn)
                            {
                                DataRow dr = table.NewRow();
                                table.Rows.Add(dr);
                                Bind(dr, instance);
                            }
                            else
                            {
                                _adpater.FillBy(table, instance.TxnID);

                                DataRow dr = table.Rows.Cast<DataRow>().SingleOrDefault(s => string.Compare(s.Field<string>(ReceivepaymentFields.TxnID), instance.TxnID, true) == 0);

                                if (object.ReferenceEquals(dr, null))
                                {
                                    dr = table.NewRow();
                                    table.Rows.Add(dr);
                                }
                                Bind(dr, instance);
                            }
                            #endregion

                            #region Save Child
                            if (instance.Appliedtotxndetails.Count > 0)
                            {
                                using (AppliedtotxndetailDal AppliedtotxndetailDal = new AppliedtotxndetailDal(_fullsyn, _constring, TableName.receivepayment, instance.TxnID))
                                {
                                    AppliedtotxndetailDal.Save(instance.Appliedtotxndetails);
                                }
                            }
                            if (instance.Linkedtxndetails.Count > 0)
                            {
                                using (LinkedtxndetailDal LinkedtxndetailDal = new LinkedtxndetailDal(_fullsyn, _constring, TableName.receivepayment, instance.TxnID))
                                {
                                    LinkedtxndetailDal.Save(instance.Linkedtxndetails);
                                }
                            }
                            #endregion
                        }
                        _adpater.Update(table);
                    }
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Receivepayment receivepayment)
        {
            try
            {
                dr[ReceivepaymentFields.TxnID] = receivepayment.TxnID;
                dr[ReceivepaymentFields.TimeCreated] = receivepayment.TimeCreated;
                dr[ReceivepaymentFields.TimeModified] = receivepayment.TimeModified;
                dr[ReceivepaymentFields.EditSequence] = receivepayment.EditSequence;
                dr[ReceivepaymentFields.TxnNumber] = receivepayment.TxnNumber.GetDbType();
                dr[ReceivepaymentFields.CustomerRef_ListID] = receivepayment.CustomerRef_ListID;
                dr[ReceivepaymentFields.CustomerRef_FullName] = receivepayment.CustomerRef_FullName;
                dr[ReceivepaymentFields.ARAccountRef_ListID] = receivepayment.ARAccountRef_ListID;
                dr[ReceivepaymentFields.ARAccountRef_FullName] = receivepayment.ARAccountRef_FullName;
                dr[ReceivepaymentFields.TxnDate] = receivepayment.TxnDate.GetDbType();
                dr[ReceivepaymentFields.RefNumber] = receivepayment.RefNumber;
                dr[ReceivepaymentFields.TotalAmount] = receivepayment.TotalAmount.GetDbType();
                dr[ReceivepaymentFields.CurrencyRef_ListID] = receivepayment.CurrencyRef_ListID;
                dr[ReceivepaymentFields.CurrencyRef_FullName] = receivepayment.CurrencyRef_FullName;
                dr[ReceivepaymentFields.ExchangeRate] = receivepayment.ExchangeRate.GetDbType();
                dr[ReceivepaymentFields.TotalAmountInHomeCurrency] = receivepayment.TotalAmountInHomeCurrency.GetDbType();
                dr[ReceivepaymentFields.PaymentMethodRef_ListID] = receivepayment.PaymentMethodRef_ListID;
                dr[ReceivepaymentFields.PaymentMethodRef_FullName] = receivepayment.PaymentMethodRef_FullName;
                dr[ReceivepaymentFields.Memo] = receivepayment.Memo;
                dr[ReceivepaymentFields.DepositToAccountRef_ListID] = receivepayment.DepositToAccountRef_ListID;
                dr[ReceivepaymentFields.DepositToAccountRef_FullName] = receivepayment.DepositToAccountRef_FullName;
                dr[ReceivepaymentFields.UnusedPayment] = receivepayment.UnusedPayment.GetDbType();
                dr[ReceivepaymentFields.UnusedCredits] = receivepayment.UnusedCredits.GetDbType();
                dr[ReceivepaymentFields.CustomField1] = receivepayment.CustomField1;
                dr[ReceivepaymentFields.CustomField2] = receivepayment.CustomField2;
                dr[ReceivepaymentFields.CustomField3] = receivepayment.CustomField3;
                dr[ReceivepaymentFields.CustomField4] = receivepayment.CustomField4;
                dr[ReceivepaymentFields.CustomField5] = receivepayment.CustomField5;
                dr[ReceivepaymentFields.CustomField6] = receivepayment.CustomField6;
                dr[ReceivepaymentFields.CustomField7] = receivepayment.CustomField7;
                dr[ReceivepaymentFields.CustomField8] = receivepayment.CustomField8;
                dr[ReceivepaymentFields.CustomField9] = receivepayment.CustomField9;
                dr[ReceivepaymentFields.CustomField10] = receivepayment.CustomField10;
                dr[ReceivepaymentFields.Status] = receivepayment.Status;
                dr[ReceivepaymentFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[ReceivepaymentFields.SynDate] = DBNull.Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adpater.Dispose();
                    _adpater = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion
    }
}
