//
// Class	:	SalesrepDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class SalesrepDal : IDisposable, IDal
    {
        #region Class Level Variables
        bool _fullsyn;
        string _qbfile;
        DateTime? _fromsyn;
        DateTime? _tosyn;
        int _chunksize;
        salesrepTableAdapter _adpater;
        #endregion

        #region Constructors / Destructors
        public SalesrepDal(string qbfile, int chunksize, DateTime? fromsyn, DateTime? tosyn, bool fullsyn, string conString)
        {
            _adpater = new salesrepTableAdapter();
            _adpater.Connection.ConnectionString = conString;
            _adpater.ClearBeforeFill = false;
            _fullsyn = fullsyn;
            _fromsyn = fromsyn;
            _tosyn = tosyn;
            _qbfile = qbfile;
            _chunksize = chunksize;
        }
        #endregion

        #region Methods (Public)
        public void Synchronize()
        {
            try
            {
                using (SalesrepFetch fetcher = new SalesrepFetch(_qbfile, _chunksize, _fromsyn, _tosyn, Logger))
                {
                    while (fetcher.IsNextChunck)
                    {

                        QBdb.salesrepDataTable table = new Dal.QBdb.salesrepDataTable();

                        foreach (Salesrep instance in fetcher.FetchNext())
                        {
                            if (_fullsyn)
                            {
                                DataRow dr = table.NewRow();
                                table.Rows.Add(dr);
                                Bind(dr, instance);
                            }
                            else
                            {
                                _adpater.FillBy(table, instance.ListID);

                                DataRow dr = table.Rows.Cast<DataRow>().SingleOrDefault(s => string.Compare(s.Field<string>(SalesrepFields.ListID), instance.ListID, true) == 0);

                                if (object.ReferenceEquals(dr, null))
                                {
                                    dr = table.NewRow();
                                    table.Rows.Add(dr);
                                }
                                Bind(dr, instance);
                            }
                        }
                        _adpater.Update(table);
                    }
                }
            }
            catch (Exception ex)
            {
                //1264logLogger.LogError(ex.Message, ex);
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Salesrep salesrep)
        {
            try
            {
                dr[SalesrepFields.ListID] = salesrep.ListID;
                dr[SalesrepFields.TimeCreated] = salesrep.TimeCreated;
                dr[SalesrepFields.TimeModified] = salesrep.TimeModified;
                dr[SalesrepFields.EditSequence] = salesrep.EditSequence;
                dr[SalesrepFields.Initial] = salesrep.Initial;
                dr[SalesrepFields.IsActive] = salesrep.IsActive.GetShort().GetDbType();
                dr[SalesrepFields.SalesRepEntityRef_ListID] = salesrep.SalesRepEntityRef_ListID;
                dr[SalesrepFields.SalesRepEntityRef_FullName] = salesrep.SalesRepEntityRef_FullName;
                dr[SalesrepFields.Status] = salesrep.Status;
                dr[SalesrepFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[SalesrepFields.SynDate] = DBNull.Value;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message, ex);
                throw ex;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adpater.Dispose();
                    _adpater = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion
    }
}
