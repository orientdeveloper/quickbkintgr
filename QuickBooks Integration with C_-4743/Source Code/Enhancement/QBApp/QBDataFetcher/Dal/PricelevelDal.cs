//
// Class	:	PricelevelDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class PricelevelDal : IDisposable, IDal
    {
        #region Class Level Variables
        bool _fullsyn;
        string _qbfile;
        DateTime? _fromsyn;
        DateTime? _tosyn;
        int _chunksize;
        string _constring;
        pricelevelTableAdapter _adpater;
        #endregion

        #region Constructors / Destructors
        public PricelevelDal(string qbfile, int chunksize, DateTime? fromsyn, DateTime? tosyn, bool fullsyn, string constring)
        {
            _adpater = new pricelevelTableAdapter();
            _adpater.Connection.ConnectionString = constring;
            _adpater.ClearBeforeFill = false;
            _fullsyn = fullsyn;
            _fromsyn = fromsyn;
            _tosyn = tosyn;
            _qbfile = qbfile;
            _chunksize = chunksize;
            _constring = constring;
        }
        #endregion

        #region Methods (Public)
        public void Synchronize()
        {
            try
            {
                using (PricelevelFetch fetcher = new PricelevelFetch(_qbfile, _chunksize, _fromsyn, _tosyn, Logger))
                {
                    while (fetcher.IsNextChunck)
                    {
                        QBdb.pricelevelDataTable table = new Dal.QBdb.pricelevelDataTable();
                        foreach (Pricelevel instance in fetcher.FetchNext())
                        {
                            #region Save Master
                            if (_fullsyn)
                            {
                                DataRow dr = table.NewRow();
                                table.Rows.Add(dr);
                                Bind(dr, instance);
                            }
                            else
                            {
                                _adpater.FillBy(table, instance.ListID);
                                DataRow dr = table.Rows.Cast<DataRow>().SingleOrDefault(s => string.Compare(s.Field<string>(PricelevelFields.ListID), instance.ListID, true) == 0);
                                if (object.ReferenceEquals(dr, null))
                                {
                                    dr = table.NewRow();
                                    table.Rows.Add(dr);
                                }
                                Bind(dr, instance);
                            }
                            #endregion

                            #region Save Child
                            if (instance.Pricelevelperitemdetails.Count > 0)
                            {
                                using (PricelevelperitemdetailDal PricelevelperitemdetailDal = new PricelevelperitemdetailDal(_fullsyn, _constring, TableName.pricelevel, instance.ListID))
                                {
                                    PricelevelperitemdetailDal.Save(instance.Pricelevelperitemdetails);
                                }
                            }
                            #endregion
                        }
                        _adpater.Update(table);
                    }
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Pricelevel pricelevel)
        {
            try
            {
                dr[PricelevelFields.ListID] = pricelevel.ListID;
                dr[PricelevelFields.TimeCreated] = pricelevel.TimeCreated;
                dr[PricelevelFields.TimeModified] = pricelevel.TimeModified;
                dr[PricelevelFields.EditSequence] = pricelevel.EditSequence;
                dr[PricelevelFields.Name] = pricelevel.Name;
                dr[PricelevelFields.IsActive] = pricelevel.IsActive.GetShort().GetDbType();
                dr[PricelevelFields.PriceLevelType] = pricelevel.PriceLevelType;
                dr[PricelevelFields.PriceLevelFixedPercentage] = pricelevel.PriceLevelFixedPercentage;
                dr[PricelevelFields.CurrencyRef_ListID] = pricelevel.CurrencyRef_ListID;
                dr[PricelevelFields.CurrencyRef_FullName] = pricelevel.CurrencyRef_FullName;
                dr[PricelevelFields.Status] = pricelevel.Status;
                dr[PricelevelFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[PricelevelFields.SynDate] = DBNull.Value;
                
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message, ex);
                throw ex;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adpater.Dispose();
                    _adpater = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion
    }
}
