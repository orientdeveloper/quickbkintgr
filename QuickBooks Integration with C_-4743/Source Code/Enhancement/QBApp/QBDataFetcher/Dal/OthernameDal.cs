//
// Class	:	OthernameDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:53 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class OthernameDal : IDisposable, IDal
    {

        #region Class Level Variables
       bool _fullsyn;
        string _qbfile;
        DateTime? _fromsyn;
        DateTime? _tosyn;
        int _chunksize;
        othernameTableAdapter _adpater;
        #endregion

        #region Constructors / Destructors
       public OthernameDal(string qbfile, int chunksize, DateTime? fromsyn, DateTime? tosyn, bool fullsyn, string conString)
        {
            _adpater = new othernameTableAdapter();
            _adpater.Connection.ConnectionString = conString;
            _adpater.ClearBeforeFill = false;
            _fullsyn = fullsyn;
            _fromsyn = fromsyn;
            _tosyn = tosyn;
            _qbfile = qbfile;
            _chunksize = chunksize;
        }
        #endregion
        
        #region Methods (Public)
        public void Synchronize()
        {
            try
            {
                using (OtherNameFetch fetcher = new OtherNameFetch(_qbfile, _fromsyn, _tosyn, Logger))
                {
                    while (fetcher.IsNextChunck)
                    {
                        QBdb.othernameDataTable table = new Dal.QBdb.othernameDataTable();
                        foreach (Othername instance in fetcher.FetchNext())
                        {
                            if (_fullsyn)
                            {
                                DataRow dr = table.NewRow();
                                table.Rows.Add(dr);
                                Bind(dr, instance);
                            }
                            else
                            {
                                _adpater.FillBy(table, instance.ListID);

                                DataRow dr = table.Rows.Cast<DataRow>().SingleOrDefault(s => string.Compare(s.Field<string>(OthernameFields.ListID), instance.ListID, true) == 0);

                                if (object.ReferenceEquals(dr, null))
                                {
                                    dr = table.NewRow();
                                    table.Rows.Add(dr);
                                }
                                Bind(dr, instance);
                            }
                        }
                        _adpater.Update(table);
                    }
                }
            }
            catch (Exception ex)
            {
                //1264logLogger.LogError(ex.Message, ex);
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Othername othername)
        {
            try
            {
                dr[OthernameFields.ListID] = othername.ListID;
                dr[OthernameFields.TimeCreated] = othername.TimeCreated;
                dr[OthernameFields.TimeModified] = othername.TimeModified;
                dr[OthernameFields.EditSequence] = othername.EditSequence;
                dr[OthernameFields.Name] = othername.Name;
                dr[OthernameFields.IsActive] = othername.IsActive.GetShort().GetDbType();
                dr[OthernameFields.CompanyName] = othername.CompanyName;
                dr[OthernameFields.Salutation] = othername.Salutation;
                dr[OthernameFields.FirstName] = othername.FirstName;
                dr[OthernameFields.MiddleName] = othername.MiddleName;
                dr[OthernameFields.LastName] = othername.LastName;
                dr[OthernameFields.Suffix] = othername.Suffix;
                dr[OthernameFields.OtherNameAddress_Addr1] = othername.OtherNameAddress_Addr1;
                dr[OthernameFields.OtherNameAddress_Addr2] = othername.OtherNameAddress_Addr2;
                dr[OthernameFields.OtherNameAddress_Addr3] = othername.OtherNameAddress_Addr3;
                dr[OthernameFields.OtherNameAddress_Addr4] = othername.OtherNameAddress_Addr4;
                dr[OthernameFields.OtherNameAddress_Addr5] = othername.OtherNameAddress_Addr5;
                dr[OthernameFields.OtherNameAddress_City] = othername.OtherNameAddress_City;
                dr[OthernameFields.OtherNameAddress_State] = othername.OtherNameAddress_State;
                dr[OthernameFields.OtherNameAddress_PostalCode] = othername.OtherNameAddress_PostalCode;
                dr[OthernameFields.OtherNameAddress_Country] = othername.OtherNameAddress_Country;
                dr[OthernameFields.OtherNameAddress_Note] = othername.OtherNameAddress_Note;
                dr[OthernameFields.Phone] = othername.Phone;
                dr[OthernameFields.Mobile] = othername.Mobile;
                dr[OthernameFields.Pager] = othername.Pager;
                dr[OthernameFields.AltPhone] = othername.AltPhone;
                dr[OthernameFields.Fax] = othername.Fax;
                dr[OthernameFields.Email] = othername.Email;
                dr[OthernameFields.Contact] = othername.Contact;
                dr[OthernameFields.AltContact] = othername.AltContact;
                dr[OthernameFields.AccountNumber] = othername.AccountNumber;
                dr[OthernameFields.CustomField1] = othername.CustomField1;
                dr[OthernameFields.CustomField2] = othername.CustomField2;
                dr[OthernameFields.CustomField3] = othername.CustomField3;
                dr[OthernameFields.CustomField4] = othername.CustomField4;
                dr[OthernameFields.CustomField5] = othername.CustomField5;
                dr[OthernameFields.CustomField6] = othername.CustomField6;
                dr[OthernameFields.CustomField7] = othername.CustomField7;
                dr[OthernameFields.CustomField8] = othername.CustomField8;
                dr[OthernameFields.CustomField9] = othername.CustomField9;
                dr[OthernameFields.CustomField10] = othername.CustomField10;
                dr[OthernameFields.Status] = othername.Status;
                dr[OthernameFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[OthernameFields.SynDate] = DBNull.Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adpater.Dispose();
                    _adpater = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion

    }
}
