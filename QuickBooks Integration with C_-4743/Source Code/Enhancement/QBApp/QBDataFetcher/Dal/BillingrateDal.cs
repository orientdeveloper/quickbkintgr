//
// Class	:	BillingrateDal.cs
// Author	:  	Synapse © 2013
// Date		:	4/2/2013 10:11:43 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class BillingrateDal : IDisposable, IDal
    {
        #region Class Level Variables
        bool _fullsyn;
        string _qbfile;
        DateTime? _fromsyn;
        DateTime? _tosyn;
        int _chunksize;
        string _conString;
        billingrateTableAdapter _adpater;
        #endregion

        #region Constructors / Destructors
        public BillingrateDal(string qbfile, int chunksize, DateTime? fromsyn, DateTime? tosyn, bool fullsyn, string conString)
        {
            _adpater = new billingrateTableAdapter();
            _adpater.Connection.ConnectionString = conString;
            _adpater.ClearBeforeFill = false;
            _fullsyn = fullsyn;
            _fromsyn = fromsyn;
            _tosyn = tosyn;
            _qbfile = qbfile;
            _chunksize = chunksize;
            _conString = conString;
        }
        #endregion

        #region Methods (Public)
        public void Synchronize()
        {
            try
            {
                using (BillingRateFetch fetcher = new BillingRateFetch(_qbfile, _fromsyn, _tosyn, Logger))
                {
                    while (fetcher.IsNextChunck)
                    {
                        QBdb.billingrateDataTable table = new QBdb.billingrateDataTable();
                        foreach (Billingrate instance in fetcher.FetchNext())
                        {
                            #region Save Master
                            if (_fullsyn)
                            {
                                DataRow dr = table.NewRow();
                                table.Rows.Add(dr);
                                Bind(dr, instance);
                            }
                            else
                            {
                                _adpater.FillBy(table, instance.ListID);

                                DataRow dr = table.Rows.Cast<DataRow>().SingleOrDefault(s => string.Compare(s.Field<string>(BillingrateFields.ListID), instance.ListID, true) == 0);

                                if (object.ReferenceEquals(dr, null))
                                {
                                    dr = table.NewRow();
                                    table.Rows.Add(dr);
                                }
                                Bind(dr, instance);
                            }
                            #endregion

                            #region Save Child
                            if (instance.Billingrateperitemdetail.Count > 0)
                            {
                                using (BillingrateperitemdetailDal BillingrateperitemdetailDal = new BillingrateperitemdetailDal(_fullsyn, _conString, TableName.billingrate, instance.ListID))
                                {
                                    BillingrateperitemdetailDal.Save(instance.Billingrateperitemdetail);
                                }
                            }
                            #endregion
                        }
                        _adpater.Update(table);
                    }
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Billingrate billingrate)
        {
            try
            {
                dr[BillingrateFields.ListID] = billingrate.ListID;
                dr[BillingrateFields.TimeCreated] = billingrate.TimeCreated;
                dr[BillingrateFields.TimeModified] = billingrate.TimeModified;
                dr[BillingrateFields.EditSequence] = billingrate.EditSequence;
                dr[BillingrateFields.Name] = billingrate.Name;
                dr[BillingrateFields.BillingRateType] = billingrate.BillingRateType;
                dr[BillingrateFields.FixedBillingRate] = billingrate.FixedBillingRate;
                dr[BillingrateFields.Status] = billingrate.Status;
                dr[BillingrateFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[BillingrateFields.SynDate] = DBNull.Value;
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adpater.Dispose();
                    _adpater = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion
    }
}
