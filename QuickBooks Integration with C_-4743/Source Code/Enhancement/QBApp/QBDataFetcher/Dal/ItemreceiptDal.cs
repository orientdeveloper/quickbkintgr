//
// Class	:	ItemreceiptDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/6/2013 10:11:51 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;
namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class ItemreceiptDal : IDisposable, IDal
    {
        #region Class Level Variables
         bool _fullsyn;
        string _qbfile;
        DateTime? _fromsyn;
        DateTime? _tosyn;
        int _chunksize;
        string _constring;
        itemreceiptTableAdapter _adpater;
        #endregion

        #region Constructors / Destructors
        public ItemreceiptDal(string qbfile, int chunksize, DateTime? fromsyn, DateTime? tosyn, bool fullsyn, string constring)
        {
            _adpater = new itemreceiptTableAdapter();
            _adpater.Connection.ConnectionString = constring;
            _adpater.ClearBeforeFill = false;
            _fullsyn = fullsyn;
            _fromsyn = fromsyn;
            _tosyn = tosyn;
            _qbfile = qbfile;
            _chunksize = chunksize;
            _constring = constring;
        }
        #endregion
        
        #region Methods (Public)
        public void Synchronize()
        {
            try
            {
               using (ItemReceiptFetch fetcher = new ItemReceiptFetch(_qbfile, _chunksize, _fromsyn, _tosyn, Logger))
                {
                    while (fetcher.IsNextChunck)
                    {
                        QBdb.itemreceiptDataTable table = new Dal.QBdb.itemreceiptDataTable();
                        foreach (Itemreceipt instance in fetcher.FetchNext())
                        {
                            #region Save Master
                            if (_fullsyn)
                            {
                                DataRow dr = table.NewRow();
                                table.Rows.Add(dr);
                                Bind(dr, instance);
                            }
                            else
                            {
                                _adpater.FillBy(table, instance.TxnID);

                                DataRow dr = table.Rows.Cast<DataRow>().SingleOrDefault(s => string.Compare(s.Field<string>(ItemreceiptFields.TxnID), instance.TxnID, true) == 0);

                                if (object.ReferenceEquals(dr, null))
                                {
                                    dr = table.NewRow();
                                    table.Rows.Add(dr);
                                }
                                Bind(dr, instance);
                            }
                            #endregion

                            #region Save Child
                            if (instance.Txnexpenselinedetails.Count > 0)
                            {
                                using (TxnexpenselinedetailDal TxnexpenselinedetailDal = new TxnexpenselinedetailDal(_fullsyn, _constring, TableName.itemreceipt, instance.TxnID))
                                {
                                    TxnexpenselinedetailDal.Save(instance.Txnexpenselinedetails);
                                }
                            }

                            if (instance.Txnitemlinedetails.Count > 0)
                            {
                                using (TxnitemlinedetailDal TxnitemlinedetailDal = new TxnitemlinedetailDal(_fullsyn, _constring, TableName.itemreceipt, instance.TxnID))
                                {
                                    TxnitemlinedetailDal.Save(instance.Txnitemlinedetails);
                                }
                            }

                            if (instance.Txnitemgrouplinedetails.Count > 0)
                            {
                                using (TxnitemgrouplinedetailDal TxnitemgrouplinedetailDal = new TxnitemgrouplinedetailDal(_fullsyn, _constring, TableName.itemreceipt, instance.TxnID))
                                {
                                    TxnitemgrouplinedetailDal.Save(instance.Txnitemgrouplinedetails);
                                }
                            }

                            if (instance.Linkedtxndetails.Count > 0)
                            {
                                using (LinkedtxndetailDal LinkedtxndetailDal = new LinkedtxndetailDal(_fullsyn, _constring, TableName.itemreceipt, instance.TxnID))
                                {
                                    LinkedtxndetailDal.Save(instance.Linkedtxndetails);
                                }
                            }
                            #endregion
                        }
                        _adpater.Update(table);
                    }
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Itemreceipt itemreceipt)
        {
            try
            {
                dr[ItemreceiptFields.TxnID] = itemreceipt.TxnID;
                dr[ItemreceiptFields.TimeCreated] = itemreceipt.TimeCreated;
                dr[ItemreceiptFields.TimeModified] = itemreceipt.TimeModified;
                dr[ItemreceiptFields.EditSequence] = itemreceipt.EditSequence;
                dr[ItemreceiptFields.TxnNumber] = itemreceipt.TxnNumber.GetDbType();
                dr[ItemreceiptFields.VendorRef_ListID] = itemreceipt.VendorRef_ListID;
                dr[ItemreceiptFields.VendorRef_FullName] = itemreceipt.VendorRef_FullName;
                dr[ItemreceiptFields.APAccountRef_ListID] = itemreceipt.APAccountRef_ListID;
                dr[ItemreceiptFields.APAccountRef_FullName] = itemreceipt.APAccountRef_FullName;
                dr[ItemreceiptFields.TxnDate] = itemreceipt.TxnDate.GetDbType();
                dr[ItemreceiptFields.TotalAmount] = itemreceipt.TotalAmount.GetDbType();
                dr[ItemreceiptFields.CurrencyRef_ListID] = itemreceipt.CurrencyRef_ListID;
                dr[ItemreceiptFields.CurrencyRef_FullName] = itemreceipt.CurrencyRef_FullName;
                dr[ItemreceiptFields.ExchangeRate] = itemreceipt.ExchangeRate.GetDbType();
                dr[ItemreceiptFields.TotalAmountInHomeCurrency] = itemreceipt.TotalAmountInHomeCurrency.GetDbType();
                dr[ItemreceiptFields.RefNumber] = itemreceipt.RefNumber;
                dr[ItemreceiptFields.Memo] = itemreceipt.Memo;
                dr[ItemreceiptFields.LinkedTxn] = itemreceipt.LinkedTxn;
                dr[ItemreceiptFields.CustomField1] = itemreceipt.CustomField1;
                dr[ItemreceiptFields.CustomField2] = itemreceipt.CustomField2;
                dr[ItemreceiptFields.CustomField3] = itemreceipt.CustomField3;
                dr[ItemreceiptFields.CustomField4] = itemreceipt.CustomField4;
                dr[ItemreceiptFields.CustomField5] = itemreceipt.CustomField5;
                dr[ItemreceiptFields.CustomField6] = itemreceipt.CustomField6;
                dr[ItemreceiptFields.CustomField7] = itemreceipt.CustomField7;
                dr[ItemreceiptFields.CustomField8] = itemreceipt.CustomField8;
                dr[ItemreceiptFields.CustomField9] = itemreceipt.CustomField9;
                dr[ItemreceiptFields.CustomField10] = itemreceipt.CustomField10;
                dr[ItemreceiptFields.Status] = itemreceipt.Status;
                dr[ItemreceiptFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[ItemreceiptFields.SynDate] = DBNull.Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adpater.Dispose();
                    _adpater = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion

    }
}
