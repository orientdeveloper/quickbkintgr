//
// Class	:	ItemsubtotalDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/6/2013 10:11:52 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class ItemsubtotalDal : IDisposable, IDal
    {
        #region Class Level Variables
        bool _fullsyn;
        string _qbfile;
        DateTime? _fromsyn;
        DateTime? _tosyn;
        int _chunksize;
        string _constring;
        itemsubtotalTableAdapter _adpater;
        #endregion

        #region Constructors / Destructors
        public ItemsubtotalDal(string qbfile, int chunksize, DateTime? fromsyn, DateTime? tosyn, bool fullsyn, string constring)
        {
            _adpater = new itemsubtotalTableAdapter();
            _adpater.Connection.ConnectionString = constring;
            _adpater.ClearBeforeFill = false;
            _fullsyn = fullsyn;
            _fromsyn = fromsyn;
            _tosyn = tosyn;
            _qbfile = qbfile;
            _chunksize = chunksize;
            _constring = constring;
        }
        #endregion

        #region Methods (Public)
        public void Synchronize()
        {
            try
            {
                using (ItemSubTotalFetch fetcher = new ItemSubTotalFetch(_qbfile, _chunksize, _fromsyn, _tosyn, Logger))
                {
                    while (fetcher.IsNextChunck)
                    {
                        QBdb.itemsubtotalDataTable table = new Dal.QBdb.itemsubtotalDataTable();
                        foreach (Itemsubtotal instance in fetcher.FetchNext())
                        {
                            #region Save Master
                            if (_fullsyn)
                            {
                                DataRow dr = table.NewRow();
                                table.Rows.Add(dr);
                                Bind(dr, instance);
                            }
                            else
                            {
                                _adpater.FillBy(table, instance.ListID);

                                DataRow dr = table.Rows.Cast<DataRow>().SingleOrDefault(s => string.Compare(s.Field<string>(ItemsubtotalFields.ListID), instance.ListID, true) == 0);

                                if (object.ReferenceEquals(dr, null))
                                {
                                    dr = table.NewRow();
                                    table.Rows.Add(dr);
                                }
                                Bind(dr, instance);
                            }
                            #endregion

                            #region Save Child
                            if (instance.Items.Count > 0)
                            {
                                using (ItemDal ItemDal = new ItemDal(_fullsyn, _constring, TableName.itemsubtotal, instance.ListID))
                                {
                                    ItemDal.Save(instance.Items);
                                }
                            }

                            if (instance.DataExtDetails.Count > 0)
                            {
                                using (DataextdetailDal DataextdetailDal = new DataextdetailDal(_fullsyn, _constring, TableName.itemsubtotal, instance.ListID))
                                {
                                    DataextdetailDal.Save(instance.DataExtDetails);
                                }
                            }
                            #endregion
                        }
                        _adpater.Update(table);
                    }
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Itemsubtotal itemsubtotal)
        {
            try
            {
                dr[ItemsubtotalFields.ListID] = itemsubtotal.ListID;
                dr[ItemsubtotalFields.TimeCreated] = itemsubtotal.TimeCreated;
                dr[ItemsubtotalFields.TimeModified] = itemsubtotal.TimeModified;
                dr[ItemsubtotalFields.EditSequence] = itemsubtotal.EditSequence;
                dr[ItemsubtotalFields.Name] = itemsubtotal.Name;
                dr[ItemsubtotalFields.IsActive] = itemsubtotal.IsActive.GetShort().GetDbType();
                dr[ItemsubtotalFields.ItemDesc] = itemsubtotal.ItemDesc;
                dr[ItemsubtotalFields.SpecialItemType] = itemsubtotal.SpecialItemType;
                dr[ItemsubtotalFields.CustomField1] = itemsubtotal.CustomField1;
                dr[ItemsubtotalFields.CustomField2] = itemsubtotal.CustomField2;
                dr[ItemsubtotalFields.CustomField3] = itemsubtotal.CustomField3;
                dr[ItemsubtotalFields.CustomField4] = itemsubtotal.CustomField4;
                dr[ItemsubtotalFields.CustomField5] = itemsubtotal.CustomField5;
                dr[ItemsubtotalFields.CustomField6] = itemsubtotal.CustomField6;
                dr[ItemsubtotalFields.CustomField7] = itemsubtotal.CustomField7;
                dr[ItemsubtotalFields.CustomField8] = itemsubtotal.CustomField8;
                dr[ItemsubtotalFields.CustomField9] = itemsubtotal.CustomField9;
                dr[ItemsubtotalFields.CustomField10] = itemsubtotal.CustomField10;
                dr[ItemsubtotalFields.Status] = itemsubtotal.Status;
                dr[ItemsubtotalFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[ItemsubtotalFields.SynDate] = DBNull.Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adpater.Dispose();
                    _adpater = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion
    }
}
