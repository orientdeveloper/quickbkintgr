//
// Class	:	IteminventoryassemblyDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/5/2013 10:11:50 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;
namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class IteminventoryassemblyDal : IDisposable, IDal
    {
        #region Class Level Variables
        bool _fullsyn;
        string _qbfile;
        DateTime? _fromsyn;
        DateTime? _tosyn;
        int _chunksize;
        string _constring;
        iteminventoryassemblyTableAdapter _adpater;
        #endregion

        #region Constructors / Destructors
        public IteminventoryassemblyDal(string qbfile, int chunksize, DateTime? fromsyn,DateTime? tosyn, bool fullsyn, string constring)
        {
            _adpater = new iteminventoryassemblyTableAdapter();
            _adpater.Connection.ConnectionString = constring;
            _adpater.ClearBeforeFill = false;
            _fullsyn = fullsyn;
            _fromsyn = fromsyn;
            _tosyn = tosyn;
            _qbfile = qbfile;
            _chunksize = chunksize;
            _constring = constring;
        }
        #endregion

        #region Methods (Public)
        public void Synchronize()
        {
            try
            {
                using (ItemInventoryAssemblyFetch fetcher = new ItemInventoryAssemblyFetch(_qbfile, _chunksize, _fromsyn, _tosyn, Logger))
                {
                    while (fetcher.IsNextChunck)
                    {
                        QBdb.iteminventoryassemblyDataTable table = new Dal.QBdb.iteminventoryassemblyDataTable();
                        foreach (Iteminventoryassembly instance in fetcher.FetchNext())
                        {
                            #region Save Master
                            if (_fullsyn)
                            {
                                DataRow dr = table.NewRow();
                                table.Rows.Add(dr);
                                Bind(dr, instance);
                            }
                            else
                            {
                                _adpater.FillBy(table, instance.ListID);

                                DataRow dr = table.Rows.Cast<DataRow>().SingleOrDefault(s => string.Compare(s.Field<string>(IteminventoryassemblyFields.ListID), instance.ListID, true) == 0);

                                if (object.ReferenceEquals(dr, null))
                                {
                                    dr = table.NewRow();
                                    table.Rows.Add(dr);
                                }
                                Bind(dr, instance);
                            }
                            #endregion

                            #region Save Child
                            if (instance.Items.Count > 0)
                            {
                                using (ItemDal ItemDal = new ItemDal(_fullsyn, _constring, TableName.iteminventoryassembly, instance.ListID))
                                {
                                    ItemDal.Save(instance.Items);
                                }
                            }

                            if (instance.DataExtDetails.Count > 0)
                            {
                                using (DataextdetailDal DataextdetailDal = new DataextdetailDal(_fullsyn, _constring, TableName.iteminventoryassembly, instance.ListID))
                                {
                                    DataextdetailDal.Save(instance.DataExtDetails);
                                }
                            }
                            #endregion
                        }
                        _adpater.Update(table);
                    }
                }
            }
            catch 
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Iteminventoryassembly iteminventoryassembly)
        {
            try
            {
                dr[IteminventoryassemblyFields.ListID] = iteminventoryassembly.ListID;
                dr[IteminventoryassemblyFields.TimeCreated] = iteminventoryassembly.TimeCreated;
                dr[IteminventoryassemblyFields.TimeModified] = iteminventoryassembly.TimeModified;
                dr[IteminventoryassemblyFields.EditSequence] = iteminventoryassembly.EditSequence;
                dr[IteminventoryassemblyFields.Name] = iteminventoryassembly.Name;
                dr[IteminventoryassemblyFields.FullName] = iteminventoryassembly.FullName;
                dr[IteminventoryassemblyFields.IsActive] = iteminventoryassembly.IsActive.GetShort().GetDbType();
                dr[IteminventoryassemblyFields.ParentRef_FullName] = iteminventoryassembly.ParentRef_FullName;
                dr[IteminventoryassemblyFields.ParentRef_ListID] = iteminventoryassembly.ParentRef_ListID;
                dr[IteminventoryassemblyFields.Sublevel] = iteminventoryassembly.Sublevel.GetDbType();
                dr[IteminventoryassemblyFields.SalesTaxCodeRef_FullName] = iteminventoryassembly.SalesTaxCodeRef_FullName;
                dr[IteminventoryassemblyFields.SalesTaxCodeRef_ListID] = iteminventoryassembly.SalesTaxCodeRef_ListID;
                dr[IteminventoryassemblyFields.IsTaxIncluded] = iteminventoryassembly.IsTaxIncluded.GetShort().GetDbType();
                dr[IteminventoryassemblyFields.SalesDesc] = iteminventoryassembly.SalesDesc;
                dr[IteminventoryassemblyFields.SalesPrice] = iteminventoryassembly.SalesPrice;
                dr[IteminventoryassemblyFields.IncomeAccountRef_ListID] = iteminventoryassembly.IncomeAccountRef_ListID;
                dr[IteminventoryassemblyFields.IncomeAccountRef_FullName] = iteminventoryassembly.IncomeAccountRef_FullName;
                dr[IteminventoryassemblyFields.PurchaseDesc] = iteminventoryassembly.PurchaseDesc;
                dr[IteminventoryassemblyFields.PurchaseCost] = iteminventoryassembly.PurchaseCost;
                dr[IteminventoryassemblyFields.PurchaseTaxCodeRef_ListID] = iteminventoryassembly.PurchaseTaxCodeRef_ListID;
                dr[IteminventoryassemblyFields.PurchaseTaxCodeRef_FullName] = iteminventoryassembly.PurchaseTaxCodeRef_FullName;
                dr[IteminventoryassemblyFields.COGSAccountRef_ListID] = iteminventoryassembly.COGSAccountRef_ListID;
                dr[IteminventoryassemblyFields.COGSAccountRef_FullName] = iteminventoryassembly.COGSAccountRef_FullName;
                dr[IteminventoryassemblyFields.PrefVendorRef_ListID] = iteminventoryassembly.PrefVendorRef_ListID;
                dr[IteminventoryassemblyFields.PrefVendorRef_FullName] = iteminventoryassembly.PrefVendorRef_FullName;
                dr[IteminventoryassemblyFields.AssetAccountRef_ListID] = iteminventoryassembly.AssetAccountRef_ListID;
                dr[IteminventoryassemblyFields.AssetAccountRef_FullName] = iteminventoryassembly.AssetAccountRef_FullName;
                dr[IteminventoryassemblyFields.BuildPoint] = iteminventoryassembly.BuildPoint;
                dr[IteminventoryassemblyFields.QuantityOnHand] = iteminventoryassembly.QuantityOnHand;
                dr[IteminventoryassemblyFields.AverageCost] = iteminventoryassembly.AverageCost;
                dr[IteminventoryassemblyFields.QuantityOnOrder] = iteminventoryassembly.QuantityOnOrder;
                dr[IteminventoryassemblyFields.QuantityOnSalesOrder] = iteminventoryassembly.QuantityOnSalesOrder;
                dr[IteminventoryassemblyFields.CustomField1] = iteminventoryassembly.CustomField1;
                dr[IteminventoryassemblyFields.CustomField10] = iteminventoryassembly.CustomField10;
                dr[IteminventoryassemblyFields.CustomField2] = iteminventoryassembly.CustomField2;
                dr[IteminventoryassemblyFields.CustomField2] = iteminventoryassembly.CustomField3;
                dr[IteminventoryassemblyFields.CustomField2] = iteminventoryassembly.CustomField4;
                dr[IteminventoryassemblyFields.CustomField2] = iteminventoryassembly.CustomField5;
                dr[IteminventoryassemblyFields.CustomField2] = iteminventoryassembly.CustomField6;
                dr[IteminventoryassemblyFields.CustomField2] = iteminventoryassembly.CustomField7;
                dr[IteminventoryassemblyFields.CustomField2] = iteminventoryassembly.CustomField8;
                dr[IteminventoryassemblyFields.CustomField2] = iteminventoryassembly.CustomField9;
                dr[IteminventoryassemblyFields.Status] = iteminventoryassembly.Status;
                dr[IteminventoryassemblyFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[IteminventoryassemblyFields.SynDate] = DBNull.Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adpater.Dispose();
                    _adpater = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion

    }
}
