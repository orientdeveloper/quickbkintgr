//
// Class	:	ItemsalestaxgrouplinedetailDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:52 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class ItemsalestaxgrouplinedetailDal : IDisposable
	{
		#region Class Level Variables
        bool _fullsyn;
        string _mastertable;
        string _idkey;
        itemsalestaxgrouplinedetailTableAdapter _adapter;
        #endregion

        #region Constructors / Destructors
        public ItemsalestaxgrouplinedetailDal(bool fullsyn, string conString, string mastertable, string idkey)
        {
            _adapter = new itemsalestaxgrouplinedetailTableAdapter();
            _adapter.Connection.ConnectionString = conString;
            _fullsyn = fullsyn;
            _idkey = idkey;
            _mastertable = mastertable;
        }
        #endregion

        #region Methods (Public)
        public void Save(List<Itemsalestaxgrouplinedetail> Itemsalestaxgrouplinedetails)
        {
            try
            {
                QBdb.itemsalestaxgrouplinedetailDataTable table = new Dal.QBdb.itemsalestaxgrouplinedetailDataTable();

                if (!_fullsyn)
                {
                    _adapter.DeleteBy(_idkey, _mastertable);
                }

                foreach (Itemsalestaxgrouplinedetail instance in Itemsalestaxgrouplinedetails)
                {                    
                    DataRow dr = table.NewRow();
                    table.Rows.Add(dr);
                    Bind(dr, instance);
                }
                _adapter.Update(table);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Itemsalestaxgrouplinedetail itemsalestaxgrouplinedetail)
        {
            try
            {
                dr[ItemsalestaxgrouplinedetailFields.ItemSalesTaxRef_ListID] = itemsalestaxgrouplinedetail.ItemSalesTaxRef_ListID;
                dr[ItemsalestaxgrouplinedetailFields.ItemSalesTaxRef_FullName] = itemsalestaxgrouplinedetail.ItemSalesTaxRef_FullName;
                dr[ItemsalestaxgrouplinedetailFields.CustomField1] = itemsalestaxgrouplinedetail.CustomField1;
                dr[ItemsalestaxgrouplinedetailFields.CustomField2] = itemsalestaxgrouplinedetail.CustomField2;
                dr[ItemsalestaxgrouplinedetailFields.CustomField3] = itemsalestaxgrouplinedetail.CustomField3;
                dr[ItemsalestaxgrouplinedetailFields.CustomField4] = itemsalestaxgrouplinedetail.CustomField4;
                dr[ItemsalestaxgrouplinedetailFields.CustomField5] = itemsalestaxgrouplinedetail.CustomField5;
                dr[ItemsalestaxgrouplinedetailFields.CustomField6] = itemsalestaxgrouplinedetail.CustomField6;
                dr[ItemsalestaxgrouplinedetailFields.CustomField7] = itemsalestaxgrouplinedetail.CustomField7;
                dr[ItemsalestaxgrouplinedetailFields.CustomField8] = itemsalestaxgrouplinedetail.CustomField8;
                dr[ItemsalestaxgrouplinedetailFields.CustomField9] = itemsalestaxgrouplinedetail.CustomField9;
                dr[ItemsalestaxgrouplinedetailFields.CustomField10] = itemsalestaxgrouplinedetail.CustomField10;
                dr[ItemsalestaxgrouplinedetailFields.Idkey] = itemsalestaxgrouplinedetail.Idkey;
                dr[ItemsalestaxgrouplinedetailFields.GroupIDKEY] = itemsalestaxgrouplinedetail.GroupIDKEY;
                dr[ItemsalestaxgrouplinedetailFields.TableName] = itemsalestaxgrouplinedetail.TableName;
                dr[ItemsalestaxgrouplinedetailFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[ItemsalestaxgrouplinedetailFields.SynDate] = DBNull.Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adapter.Dispose();
                    _adapter = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
	}
}
