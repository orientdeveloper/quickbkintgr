//
// Class	:	InventoryadjustmentDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/5/2013 10:11:49 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class InventoryadjustmentDal : IDisposable, IDal
    {
        #region Class Level Variables
        bool _fullsyn;
        string _qbfile;
        DateTime? _fromsyn;
        DateTime? _tosyn;
        int _chunksize;
        string _constring;
        inventoryadjustmentTableAdapter _adpater;
        #endregion

        #region Constructors / Destructors
        public InventoryadjustmentDal(string qbfile, int chunksize, DateTime? fromsyn,DateTime? tosyn, bool fullsyn, string constring)
        {
            _adpater = new inventoryadjustmentTableAdapter();
            _adpater.Connection.ConnectionString = constring;
            _adpater.ClearBeforeFill = false;
            _fullsyn = fullsyn;
            _fromsyn = fromsyn;
            _tosyn = tosyn;
            _qbfile = qbfile;
            _chunksize = chunksize;
            _constring = constring;
        }
        #endregion

        #region Methods (Public)
        public void Synchronize()
        {
            try
            {
                using (InventoryAdjustmentFetch fetcher = new InventoryAdjustmentFetch(_qbfile, _chunksize, _fromsyn, _tosyn, Logger))
                {
                    while (fetcher.IsNextChunck)
                    {
                        QBdb.inventoryadjustmentDataTable table = new Dal.QBdb.inventoryadjustmentDataTable();
                        foreach (Inventoryadjustment instance in fetcher.FetchNext())
                        {
                            #region Save Master
                            if (_fullsyn)
                            {
                                DataRow dr = table.NewRow();
                                table.Rows.Add(dr);
                                Bind(dr, instance);
                            }
                            else
                            {
                                _adpater.FillBy(table, instance.TxnID);

                                DataRow dr = table.Rows.Cast<DataRow>().SingleOrDefault(s => string.Compare(s.Field<string>(InventoryadjustmentFields.TxnID), instance.TxnID, true) == 0);

                                if (object.ReferenceEquals(dr, null))
                                {
                                    dr = table.NewRow();
                                    table.Rows.Add(dr);
                                }
                                Bind(dr, instance);
                            }
                            #endregion

                            #region Save Child
                            if (instance.Inventoryadjustmentlinedetails.Count > 0)
                            {
                                using (InventoryadjustmentlinedetailDal InventoryadjustmentlinedetailDal = new InventoryadjustmentlinedetailDal(_fullsyn, _constring, TableName.inventoryadjustment, instance.TxnID))
                                {
                                    InventoryadjustmentlinedetailDal.Save(instance.Inventoryadjustmentlinedetails);
                                }
                            }
                            #endregion
                        }
                        _adpater.Update(table);
                    }
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Inventoryadjustment inventoryadjustment)
        {
            dr[InventoryadjustmentFields.TxnID] = inventoryadjustment.TxnID;
            dr[InventoryadjustmentFields.TimeCreated] = inventoryadjustment.TimeCreated;
            dr[InventoryadjustmentFields.TimeModified] = inventoryadjustment.TimeModified;
            dr[InventoryadjustmentFields.EditSequence] = inventoryadjustment.EditSequence;
            dr[InventoryadjustmentFields.TxnNumber] = inventoryadjustment.TxnNumber.GetDbType();
            dr[InventoryadjustmentFields.AccountRef_ListID] = inventoryadjustment.AccountRef_ListID;
            dr[InventoryadjustmentFields.AccountRef_FullName] = inventoryadjustment.AccountRef_FullName;
            dr[InventoryadjustmentFields.InventorySiteRef_ListID] = inventoryadjustment.InventorySiteRef_ListID;
            dr[InventoryadjustmentFields.InventorySiteRef_FullName] = inventoryadjustment.InventorySiteRef_FullName;
            dr[InventoryadjustmentFields.TxnDate] = inventoryadjustment.TxnDate.GetDbType();
            dr[InventoryadjustmentFields.RefNumber] = inventoryadjustment.RefNumber;
            dr[InventoryadjustmentFields.CustomerRef_ListID] = inventoryadjustment.CustomerRef_ListID;
            dr[InventoryadjustmentFields.CustomerRef_FullName] = inventoryadjustment.CustomerRef_FullName;
            dr[InventoryadjustmentFields.ClassRef_ListID] = inventoryadjustment.ClassRef_ListID;
            dr[InventoryadjustmentFields.ClassRef_FullName] = inventoryadjustment.ClassRef_FullName;
            dr[InventoryadjustmentFields.Memo] = inventoryadjustment.Memo;
            dr[InventoryadjustmentFields.CustomField1] = inventoryadjustment.CustomField1;
            dr[InventoryadjustmentFields.CustomField2] = inventoryadjustment.CustomField2;
            dr[InventoryadjustmentFields.CustomField3] = inventoryadjustment.CustomField3;
            dr[InventoryadjustmentFields.CustomField4] = inventoryadjustment.CustomField4;
            dr[InventoryadjustmentFields.CustomField5] = inventoryadjustment.CustomField5;
            dr[InventoryadjustmentFields.CustomField6] = inventoryadjustment.CustomField6;
            dr[InventoryadjustmentFields.CustomField7] = inventoryadjustment.CustomField7;
            dr[InventoryadjustmentFields.CustomField8] = inventoryadjustment.CustomField8;
            dr[InventoryadjustmentFields.CustomField9] = inventoryadjustment.CustomField9;
            dr[InventoryadjustmentFields.CustomField10] = inventoryadjustment.CustomField10;
            dr[InventoryadjustmentFields.IsSyn] = (short)SynStatus.Synchronize;
            dr[InventoryadjustmentFields.Status] = inventoryadjustment.Status;
            dr[InventoryadjustmentFields.SynDate] = DBNull.Value;

        }
        #endregion
            
        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adpater.Dispose();
                    _adpater = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion
    }
}
