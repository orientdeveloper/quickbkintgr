//
// Class	:	TxnitemlinedetailDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:58 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class TxnitemlinedetailDal : IDisposable
    {
        #region Class Level Variables
        bool _fullsyn;
        string _mastertable;
        txnitemlinedetailTableAdapter _adapter;
        string _idkey;
        #endregion

        #region Constructors / Destructors
        public TxnitemlinedetailDal(bool fullsyn, string conString, string mastertable, string idkey)
        {
            _adapter = new txnitemlinedetailTableAdapter();
            _adapter.Connection.ConnectionString = conString;
            _fullsyn = fullsyn;
            _idkey = idkey;
            _mastertable = mastertable;
        }
        #endregion

        #region Methods (Public)
        public void Save(List<Txnitemlinedetail> Txnitemlinedetails)
        {
            try
            {
                QBdb.txnitemlinedetailDataTable table = new Dal.QBdb.txnitemlinedetailDataTable();

                if (!_fullsyn)
                {
                    _adapter.DeleteBy(_idkey, _mastertable);
                }

                foreach (Txnitemlinedetail instance in Txnitemlinedetails)
                {
                    DataRow dr = table.NewRow();
                    table.Rows.Add(dr);
                    Bind(dr, instance);
                }

                _adapter.Update(table);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Txnitemlinedetail txnitemlinedetail)
        {
            try
            {
                dr[TxnitemlinedetailFields.TxnLineID] = txnitemlinedetail.TxnLineID;
                dr[TxnitemlinedetailFields.ItemRef_ListID] = txnitemlinedetail.ItemRef_ListID;
                dr[TxnitemlinedetailFields.ItemRef_FullName] = txnitemlinedetail.ItemRef_FullName;
                dr[TxnitemlinedetailFields.InventorySiteRef_ListID] = txnitemlinedetail.InventorySiteRef_ListID;
                dr[TxnitemlinedetailFields.InventorySiteRef_FullName] = txnitemlinedetail.InventorySiteRef_FullName;
                dr[TxnitemlinedetailFields.Desc] = txnitemlinedetail.Desc;
                dr[TxnitemlinedetailFields.Quantity] = txnitemlinedetail.Quantity;
                dr[TxnitemlinedetailFields.UnitOfMeasure] = txnitemlinedetail.UnitOfMeasure;
                dr[TxnitemlinedetailFields.OverrideUOMSetRef_ListID] = txnitemlinedetail.OverrideUOMSetRef_ListID;
                dr[TxnitemlinedetailFields.OverrideUOMSetRef_FullName] = txnitemlinedetail.OverrideUOMSetRef_FullName;
                dr[TxnitemlinedetailFields.Cost] = txnitemlinedetail.Cost;
                dr[TxnitemlinedetailFields.Amount] = txnitemlinedetail.Amount.GetDbType();
                dr[TxnitemlinedetailFields.CustomerRef_ListID] = txnitemlinedetail.CustomerRef_ListID;
                dr[TxnitemlinedetailFields.CustomerRef_FullName] = txnitemlinedetail.CustomerRef_FullName;
                dr[TxnitemlinedetailFields.ClassRef_ListID] = txnitemlinedetail.ClassRef_ListID;
                dr[TxnitemlinedetailFields.ClassRef_FullName] = txnitemlinedetail.ClassRef_FullName;
                dr[TxnitemlinedetailFields.SalesTaxCodeRef_ListID] = txnitemlinedetail.SalesTaxCodeRef_ListID;
                dr[TxnitemlinedetailFields.SalesTaxCodeRef_FullName] = txnitemlinedetail.SalesTaxCodeRef_FullName;
                dr[TxnitemlinedetailFields.BillableStatus] = txnitemlinedetail.BillableStatus;

                dr[TxnitemlinedetailFields.CustomField1] = txnitemlinedetail.CustomField1;
                dr[TxnitemlinedetailFields.CustomField2] = txnitemlinedetail.CustomField2;
                dr[TxnitemlinedetailFields.CustomField3] = txnitemlinedetail.CustomField3;
                dr[TxnitemlinedetailFields.CustomField4] = txnitemlinedetail.CustomField4;
                dr[TxnitemlinedetailFields.CustomField5] = txnitemlinedetail.CustomField5;
                dr[TxnitemlinedetailFields.CustomField6] = txnitemlinedetail.CustomField6;
                dr[TxnitemlinedetailFields.CustomField7] = txnitemlinedetail.CustomField7;
                dr[TxnitemlinedetailFields.CustomField8] = txnitemlinedetail.CustomField8;
                dr[TxnitemlinedetailFields.CustomField9] = txnitemlinedetail.CustomField9;
                dr[TxnitemlinedetailFields.CustomField10] = txnitemlinedetail.CustomField10;
                dr[TxnitemlinedetailFields.Idkey] = txnitemlinedetail.Idkey;
                dr[TxnitemlinedetailFields.GroupIDKEY] = txnitemlinedetail.GroupIDKEY;

                dr[TxnitemlinedetailFields.TableName] = txnitemlinedetail.TableName;
                dr[TxnitemlinedetailFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[TxnitemlinedetailFields.SynDate] = DBNull.Value;
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adapter.Dispose();
                    _adapter = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
