//
// Class	:	CreditcardcreditDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:45 AM
//
using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class CreditcardcreditDal : IDisposable, IDal
    {
        #region Class Level Variables
        bool _fullsyn;
        string _qbfile;
        DateTime? _fromsyn;
        DateTime? _tosyn;
        int _chunksize;
        creditcardcreditTableAdapter _adpater;
        string _constring;
        #endregion

        #region Constructors / Destructors
        public CreditcardcreditDal(string qbfile, int chunksize, DateTime? fromsyn, DateTime? tosyn, bool fullsyn, string constring)
        {
            _adpater = new creditcardcreditTableAdapter();
            _adpater.Connection.ConnectionString = constring;
            _adpater.ClearBeforeFill = false;
            _fullsyn = fullsyn;
            _fromsyn = fromsyn;
            _tosyn = tosyn;
            _qbfile = qbfile;
            _chunksize = chunksize;
            _constring = constring;
        }
        #endregion

        #region Methods (Public)
        public void Synchronize()
        {
            try
            {
                using (CreditCardCreditFetch fetcher = new CreditCardCreditFetch(_qbfile, _chunksize, _fromsyn, _tosyn, Logger))
                {
                    while (fetcher.IsNextChunck)
                    {
                        QBdb.creditcardcreditDataTable table = new Dal.QBdb.creditcardcreditDataTable();
                        foreach (Creditcardcredit instance in fetcher.FetchNext())
                        {
                            #region Save Master
                            if (_fullsyn)
                            {
                                DataRow dr = table.NewRow();
                                table.Rows.Add(dr);
                                Bind(dr, instance);
                            }
                            else
                            {
                                _adpater.FillBy(table, instance.TxnID);

                                DataRow dr = table.Rows.Cast<DataRow>().SingleOrDefault(s => string.Compare(s.Field<string>(CreditcardcreditFields.TxnID), instance.TxnID, true) == 0);

                                if (object.ReferenceEquals(dr, null))
                                {
                                    dr = table.NewRow();
                                    table.Rows.Add(dr);
                                }
                                Bind(dr, instance);
                            }
                            #endregion

                            #region Save Child
                            if (instance.Txnexpenselinedetails.Count > 0)
                            {
                                using (TxnexpenselinedetailDal TxnexpenselinedetailDal = new TxnexpenselinedetailDal(_fullsyn, _constring, TableName.creditcardcredit, instance.TxnID))
                                {
                                    TxnexpenselinedetailDal.Save(instance.Txnexpenselinedetails);
                                }
                            }

                            if (instance.Txnitemlinedetails.Count > 0)
                            {
                                using (TxnitemlinedetailDal TxnitemlinedetailDal = new TxnitemlinedetailDal(_fullsyn, _constring, TableName.creditcardcredit, instance.TxnID))
                                {
                                    TxnitemlinedetailDal.Save(instance.Txnitemlinedetails);
                                }
                            }

                            if (instance.Txnitemgrouplinedetails.Count > 0)
                            {
                                using (TxnitemgrouplinedetailDal TxnitemgrouplinedetailDal = new TxnitemgrouplinedetailDal(_fullsyn, _constring, TableName.creditcardcredit, instance.TxnID))
                                {
                                    TxnitemgrouplinedetailDal.Save(instance.Txnitemgrouplinedetails);
                                }
                            }
                            #endregion
                        }
                        _adpater.Update(table);
                    }
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Creditcardcredit creditcardcredit)
        {
            try
            {
                dr[CreditcardcreditFields.TxnID] = creditcardcredit.TxnID;
                dr[CreditcardcreditFields.TimeCreated] = creditcardcredit.TimeCreated;
                dr[CreditcardcreditFields.TimeModified] = creditcardcredit.TimeModified;
                dr[CreditcardcreditFields.EditSequence] = creditcardcredit.EditSequence;
                dr[CreditcardcreditFields.TxnNumber] = creditcardcredit.TxnNumber.GetDbType();
                dr[CreditcardcreditFields.AccountRef_ListID] = creditcardcredit.AccountRef_ListID;
                dr[CreditcardcreditFields.AccountRef_FullName] = creditcardcredit.AccountRef_FullName;
                dr[CreditcardcreditFields.PayeeEntityRef_ListID] = creditcardcredit.PayeeEntityRef_ListID;
                dr[CreditcardcreditFields.PayeeEntityRef_FullName] = creditcardcredit.PayeeEntityRef_FullName;
                dr[CreditcardcreditFields.TxnDate] = creditcardcredit.TxnDate.GetDbType();
                dr[CreditcardcreditFields.Amount] = creditcardcredit.Amount.GetDbType();
                dr[CreditcardcreditFields.CurrencyRef_FullName] = creditcardcredit.CurrencyRef_FullName;
                dr[CreditcardcreditFields.CurrencyRef_ListID] = creditcardcredit.CurrencyRef_ListID;
                dr[CreditcardcreditFields.ExchangeRate] = creditcardcredit.ExchangeRate.GetDbType();
                dr[CreditcardcreditFields.AmountInHomeCurrency] = creditcardcredit.AmountInHomeCurrency.GetDbType();
                dr[CreditcardcreditFields.RefNumber] = creditcardcredit.RefNumber;
                dr[CreditcardcreditFields.Memo] = creditcardcredit.Memo;
                dr[CreditcardcreditFields.CustomField1] = creditcardcredit.CustomField1;
                dr[CreditcardcreditFields.CustomField2] = creditcardcredit.CustomField2;
                dr[CreditcardcreditFields.CustomField3] = creditcardcredit.CustomField3;
                dr[CreditcardcreditFields.CustomField4] = creditcardcredit.CustomField4;
                dr[CreditcardcreditFields.CustomField5] = creditcardcredit.CustomField5;
                dr[CreditcardcreditFields.CustomField6] = creditcardcredit.CustomField6;
                dr[CreditcardcreditFields.CustomField7] = creditcardcredit.CustomField7;
                dr[CreditcardcreditFields.CustomField8] = creditcardcredit.CustomField8;
                dr[CreditcardcreditFields.CustomField9] = creditcardcredit.CustomField9;
                dr[CreditcardcreditFields.CustomField10] = creditcardcredit.CustomField10;
                dr[CreditcardcreditFields.Status] = creditcardcredit.Status;
                dr[CreditcardcreditFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[CreditcardcreditFields.SynDate] = DBNull.Value;
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adpater.Dispose();
                    _adpater = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion
    }
}
