//
// Class	:	PricelevelperitemdetailDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:48 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class PricelevelperitemdetailDal : IDisposable
    {
        #region Class Level Variables
        bool _fullsyn;
        string _mastertable;
        string _idkey;
        pricelevelperitemdetailTableAdapter _adapter;
        #endregion

        #region Constructors / Destructors
        public PricelevelperitemdetailDal(bool fullsyn, string conString, string mastertable, string idkey)
        {
            _adapter = new pricelevelperitemdetailTableAdapter();
            _adapter.Connection.ConnectionString = conString;
            _adapter.ClearBeforeFill = false;
            _fullsyn = fullsyn;
            _idkey = idkey;
            _mastertable = mastertable;
        }
        #endregion

        #region Methods (Public)
        public void Save(List<Pricelevelperitemdetail> Pricelevelperitemdetails)
        {
            try
            {
                QBdb.pricelevelperitemdetailDataTable table = new Dal.QBdb.pricelevelperitemdetailDataTable();

                if (!_fullsyn)
                {
                    _adapter.DeleteBy(_idkey, _mastertable);
                }

                foreach (Pricelevelperitemdetail instance in Pricelevelperitemdetails)
                {
                    DataRow dr = table.NewRow();
                    table.Rows.Add(dr);
                    Bind(dr, instance);
                }
                _adapter.Update(table);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Pricelevelperitemdetail pricelevelperitemdetail)
        {
            try
            {
                dr[PricelevelperitemdetailFields.ItemRef_ListID] = pricelevelperitemdetail.ItemRef_ListID;
                dr[PricelevelperitemdetailFields.ItemRef_FullName] = pricelevelperitemdetail.ItemRef_FullName;
                dr[PricelevelperitemdetailFields.CustomPrice] = pricelevelperitemdetail.CustomPrice;
                dr[PricelevelperitemdetailFields.CustomPricePercent] = pricelevelperitemdetail.CustomPricePercent;
                dr[PricelevelperitemdetailFields.CustomField1] = pricelevelperitemdetail.CustomField1;
                dr[PricelevelperitemdetailFields.CustomField2] = pricelevelperitemdetail.CustomField2;
                dr[PricelevelperitemdetailFields.CustomField3] = pricelevelperitemdetail.CustomField3;
                dr[PricelevelperitemdetailFields.CustomField4] = pricelevelperitemdetail.CustomField4;
                dr[PricelevelperitemdetailFields.CustomField5] = pricelevelperitemdetail.CustomField5;
                dr[PricelevelperitemdetailFields.CustomField6] = pricelevelperitemdetail.CustomField6;
                dr[PricelevelperitemdetailFields.CustomField7] = pricelevelperitemdetail.CustomField7;
                dr[PricelevelperitemdetailFields.CustomField8] = pricelevelperitemdetail.CustomField8;
                dr[PricelevelperitemdetailFields.CustomField9] = pricelevelperitemdetail.CustomField9;
                dr[PricelevelperitemdetailFields.CustomField10] = pricelevelperitemdetail.CustomField10;
                dr[PricelevelperitemdetailFields.Idkey] = pricelevelperitemdetail.Idkey;
                dr[PricelevelperitemdetailFields.GroupIDKEY] = pricelevelperitemdetail.GroupIDKEY;
                dr[PricelevelperitemdetailFields.TableName] = pricelevelperitemdetail.TableName;
                dr[PricelevelperitemdetailFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[PricelevelperitemdetailFields.SynDate] = DBNull.Value;
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adapter.Dispose();
                    _adapter = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
