//
// Class	:	VendorDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	27/05/2013 11:30:59 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class VendorDal : IDisposable, IDal
    {
        #region Class Level Variables
        bool _fullsyn;
        string _qbfile;
        DateTime? _fromsyn;
        DateTime? _tosyn;
        int _chunksize;
        string _constring;
        vendorTableAdapter _adpater;
        #endregion

        #region Constructors / Destructors
        public VendorDal(string qbfile, int chunksize, DateTime? fromsyn, DateTime? tosyn, bool fullsyn, string constring)
        {
            _adpater = new vendorTableAdapter();
            _adpater.Connection.ConnectionString = constring;
            _adpater.ClearBeforeFill = false;
            _fullsyn = fullsyn;
            _fromsyn = fromsyn;
            _tosyn = tosyn;
            _qbfile = qbfile;
            _chunksize = chunksize;
            _constring = constring;
        }
        #endregion

        #region Methods (Public)
        public void Synchronize()
        {
            try
            {
                using (VendorFetch fetcher = new VendorFetch(_qbfile, _chunksize, _fromsyn, _tosyn, Logger))
                {
                    while (fetcher.IsNextChunck)
                    {

                        QBdb.vendorDataTable table = new Dal.QBdb.vendorDataTable();

                        foreach (Vendor instance in fetcher.FetchNext())
                        {
                            #region Master Save
                            if (_fullsyn)
                            {
                                DataRow dr = table.NewRow();
                                table.Rows.Add(dr);
                                Bind(dr, instance);
                            }
                            else
                            {
                                _adpater.FillBy(table, instance.ListID);

                                DataRow dr = table.Rows.Cast<DataRow>().SingleOrDefault(s => string.Compare(s.Field<string>(VendorFields.ListID), instance.ListID, true) == 0);

                                if (object.ReferenceEquals(dr, null))
                                {
                                    dr = table.NewRow();
                                    table.Rows.Add(dr);
                                }
                                Bind(dr, instance);
                            }
                            #endregion
                           

                            #region Save Child
                            if (instance.DataExtDetails.Count > 0)
                            {
                                using (DataextdetailDal DataextdetailDal = new DataextdetailDal(_fullsyn, _constring, TableName.Vendor, instance.ListID))
                                {
                                    DataextdetailDal.Save(instance.DataExtDetails);
                                }
                            }
                            #endregion
                        }
                        _adpater.Update(table);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message, ex);
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Vendor vendor)
        {
            try
            {
                dr[VendorFields.ListID] = vendor.ListID;
                dr[VendorFields.TimeCreated] = vendor.TimeCreated;
                dr[VendorFields.TimeModified] = vendor.TimeModified;
                dr[VendorFields.EditSequence] = vendor.EditSequence;
                dr[VendorFields.Name] = vendor.Name;
                dr[VendorFields.IsActive] = vendor.IsActive.GetShort().GetDbType();
                dr[VendorFields.CompanyName] = vendor.CompanyName;
                dr[VendorFields.Salutation] = vendor.Salutation;
                dr[VendorFields.FirstName] = vendor.FirstName;
                dr[VendorFields.MiddleName] = vendor.MiddleName;
                dr[VendorFields.LastName] = vendor.LastName;
                dr[VendorFields.Suffix] = vendor.Suffix;
                dr[VendorFields.VendorAddress_Addr1] = vendor.VendorAddress_Addr1;
                dr[VendorFields.VendorAddress_Addr2] = vendor.VendorAddress_Addr2;
                dr[VendorFields.VendorAddress_Addr3] = vendor.VendorAddress_Addr3;
                dr[VendorFields.VendorAddress_Addr4] = vendor.VendorAddress_Addr4;
                dr[VendorFields.VendorAddress_Addr5] = vendor.VendorAddress_Addr5;
                dr[VendorFields.VendorAddress_City] = vendor.VendorAddress_City;
                dr[VendorFields.VendorAddress_State] = vendor.VendorAddress_State;
                dr[VendorFields.VendorAddress_PostalCode] = vendor.VendorAddress_PostalCode;
                dr[VendorFields.VendorAddress_Country] = vendor.VendorAddress_Country;
                dr[VendorFields.VendorAddress_Note] = vendor.VendorAddress_Note;
                dr[VendorFields.Phone] = vendor.Phone;
                dr[VendorFields.Mobile] = vendor.Mobile;
                dr[VendorFields.Pager] = vendor.Pager;
                dr[VendorFields.AltPhone] = vendor.AltPhone;
                dr[VendorFields.Fax] = vendor.Fax;
                dr[VendorFields.Email] = vendor.Email;
                dr[VendorFields.Contact] = vendor.Contact;
                dr[VendorFields.AltContact] = vendor.AltContact;
                dr[VendorFields.NameOnCheck] = vendor.NameOnCheck;
                dr[VendorFields.Notes] = vendor.Notes;
                dr[VendorFields.AccountNumber] = vendor.AccountNumber;
                dr[VendorFields.VendorTypeRef_ListID] = vendor.VendorTypeRef_ListID;
                dr[VendorFields.VendorTypeRef_FullName] = vendor.VendorTypeRef_FullName;
                dr[VendorFields.TermsRef_ListID] = vendor.TermsRef_ListID;
                dr[VendorFields.TermsRef_FullName] = vendor.TermsRef_FullName;
                dr[VendorFields.CreditLimit] = vendor.CreditLimit.GetDbType();
                dr[VendorFields.VendorTaxIdent] = vendor.VendorTaxIdent;
                dr[VendorFields.IsVendorEligibleFor1099] = vendor.IsVendorEligibleFor1099.GetShort().GetDbType();
                dr[VendorFields.Balance] = vendor.Balance.GetDbType();
                dr[VendorFields.IsSalesTaxAgency] = vendor.IsSalesTaxAgency.GetShort().GetDbType();
                dr[VendorFields.CurrencyRef_ListID] = vendor.CurrencyRef_ListID;
                dr[VendorFields.CurrencyRef_FullName] = vendor.CurrencyRef_FullName;
                dr[VendorFields.BillingRateRef_ListID] = vendor.BillingRateRef_ListID;
                dr[VendorFields.BillingRateRef_FullName] = vendor.BillingRateRef_FullName;
                dr[VendorFields.CustomField1] = vendor.CustomField1;
                dr[VendorFields.CustomField2] = vendor.CustomField2;
                dr[VendorFields.CustomField3] = vendor.CustomField3;
                dr[VendorFields.CustomField4] = vendor.CustomField4;
                dr[VendorFields.CustomField5] = vendor.CustomField5;
                dr[VendorFields.CustomField6] = vendor.CustomField6;
                dr[VendorFields.CustomField7] = vendor.CustomField7;
                dr[VendorFields.CustomField8] = vendor.CustomField8;
                dr[VendorFields.CustomField9] = vendor.CustomField9;
                dr[VendorFields.CustomField10] = vendor.CustomField10;
                dr[VendorFields.Status] = vendor.Status;
                dr[VendorFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[VendorFields.SynDate] = DBNull.Value;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message, ex);
                throw;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adpater.Dispose();
                    _adpater = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion
    }
}
