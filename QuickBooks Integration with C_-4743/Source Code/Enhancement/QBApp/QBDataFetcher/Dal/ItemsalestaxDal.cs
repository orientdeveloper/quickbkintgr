//
// Class	:	Itemsalestaxs.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/6/2013 10:11:51 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class ItemsalestaxDal : IDisposable, IDal
    {
        #region Class Level Variables
        bool _fullsyn;
        string _qbfile;
        DateTime? _fromsyn;
        DateTime? _tosyn;
        int _chunksize;
        string _constring;
        itemsalestaxTableAdapter _adpater;
        #endregion

        #region Constructors / Destructors
        public ItemsalestaxDal(string qbfile, int chunksize, DateTime? fromsyn, DateTime? tosyn, bool fullsyn, string constring)
        {
            _adpater = new itemsalestaxTableAdapter();
            _adpater.Connection.ConnectionString = constring;
            _adpater.ClearBeforeFill = false;
            _fullsyn = fullsyn;
            _fromsyn = fromsyn;
            _tosyn = tosyn;
            _qbfile = qbfile;
            _chunksize = chunksize;
            _constring = constring;
        }
        #endregion

        #region Methods (Public)
        public void Synchronize()
        {
            try
            {
                using (ItemSalesTaxFetch fetcher = new ItemSalesTaxFetch(_qbfile, _chunksize, _fromsyn, _tosyn, Logger))
                {
                    while (fetcher.IsNextChunck)
                    {
                        QBdb.itemsalestaxDataTable table = new Dal.QBdb.itemsalestaxDataTable();
                        foreach (Itemsalestax instance in fetcher.FetchNext())
                        {
                            #region Save Master
                            if (_fullsyn)
                            {
                                DataRow dr = table.NewRow();
                                table.Rows.Add(dr);
                                Bind(dr, instance);
                            }
                            else
                            {
                                _adpater.FillBy(table, instance.ListID);

                                DataRow dr = table.Rows.Cast<DataRow>().SingleOrDefault(s => string.Compare(s.Field<string>(ItemsalestaxFields.ListID), instance.ListID, true) == 0);

                                if (object.ReferenceEquals(dr, null))
                                {
                                    dr = table.NewRow();
                                    table.Rows.Add(dr);
                                }
                                Bind(dr, instance);
                            }
                            #endregion

                            #region Save Child
                            if (instance.Items.Count > 0)
                            {
                                using (ItemDal ItemDal = new ItemDal(_fullsyn, _constring, TableName.itemsalestax, instance.ListID))
                                {
                                    ItemDal.Save(instance.Items);
                                }
                            }

                            if (instance.DataExtDetails.Count > 0)
                            {
                                using (DataextdetailDal DataextdetailDal = new DataextdetailDal(_fullsyn, _constring, TableName.itemsalestax, instance.ListID))
                                {
                                    DataextdetailDal.Save(instance.DataExtDetails);
                                }
                            }
                            #endregion
                        }
                        _adpater.Update(table);
                    }
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Itemsalestax itemsalestax)
        {
            try
            {
                dr[ItemsalestaxFields.ListID] = itemsalestax.ListID;
                dr[ItemsalestaxFields.TimeCreated] = itemsalestax.TimeCreated;
                dr[ItemsalestaxFields.TimeModified] = itemsalestax.TimeModified;
                dr[ItemsalestaxFields.EditSequence] = itemsalestax.EditSequence;
                dr[ItemsalestaxFields.Name] = itemsalestax.Name;
                dr[ItemsalestaxFields.IsActive] = itemsalestax.IsActive.GetShort().GetDbType();
                dr[ItemsalestaxFields.ItemDesc] = itemsalestax.ItemDesc;
                dr[ItemsalestaxFields.IsUsedOnPurchaseTransaction] = itemsalestax.IsUsedOnPurchaseTransaction.GetShort().GetDbType();
                dr[ItemsalestaxFields.TaxRate] = itemsalestax.TaxRate;
                dr[ItemsalestaxFields.TaxVendorRef_ListID] = itemsalestax.TaxVendorRef_ListID;
                dr[ItemsalestaxFields.TaxVendorRef_FullName] = itemsalestax.TaxVendorRef_FullName;
                dr[ItemsalestaxFields.CustomField1] = itemsalestax.CustomField1;
                dr[ItemsalestaxFields.CustomField2] = itemsalestax.CustomField2;
                dr[ItemsalestaxFields.CustomField3] = itemsalestax.CustomField3;
                dr[ItemsalestaxFields.CustomField4] = itemsalestax.CustomField4;
                dr[ItemsalestaxFields.CustomField5] = itemsalestax.CustomField5;
                dr[ItemsalestaxFields.CustomField6] = itemsalestax.CustomField6;
                dr[ItemsalestaxFields.CustomField7] = itemsalestax.CustomField7;
                dr[ItemsalestaxFields.CustomField8] = itemsalestax.CustomField8;
                dr[ItemsalestaxFields.CustomField9] = itemsalestax.CustomField9;
                dr[ItemsalestaxFields.CustomField10] = itemsalestax.CustomField10;
                dr[ItemsalestaxFields.Status] = itemsalestax.Status;
                dr[ItemsalestaxFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[ItemsalestaxFields.SynDate] = DBNull.Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adpater.Dispose();
                    _adpater = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion
    }
}
