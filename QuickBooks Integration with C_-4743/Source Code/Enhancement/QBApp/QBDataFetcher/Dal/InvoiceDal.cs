//
// Class	:	InvoiceDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:49 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class InvoiceDal : IDisposable, IDal
    {
        #region Class Level Variables
        bool _fullsyn;
        string _qbfile;
        DateTime? _fromsyn;
        DateTime? _tosyn;
        int _chunksize;
        string _constring;
        invoiceTableAdapter _adpater;
        #endregion

        #region Constructors / Destructors
        public InvoiceDal(string qbfile, int chunksize, DateTime? fromsyn,DateTime? tosyn, bool fullsyn, string constring)
        {
            _adpater = new invoiceTableAdapter();
            _adpater.Connection.ConnectionString = constring;
            _adpater.ClearBeforeFill = false;
            _fullsyn = fullsyn;
            _fromsyn = fromsyn;
            _tosyn = tosyn;
            _qbfile = qbfile;
            _chunksize = chunksize;
            _constring = constring;
        }
        #endregion
        
        #region Methods (Public)
        public void Synchronize()
        {
            try
            {
                using (InvoiceFetch fetcher = new InvoiceFetch(_qbfile, _chunksize, _fromsyn,_tosyn, Logger))
                {
                    while (fetcher.IsNextChunck)
                    {
                        QBdb.invoiceDataTable table = new Dal.QBdb.invoiceDataTable();
                        foreach (Invoice instance in fetcher.FetchNext())
                        {
                            #region Save Master
                            if (_fullsyn)
                            {
                                DataRow dr = table.NewRow();
                                table.Rows.Add(dr);
                                Bind(dr, instance);
                            }
                            else
                            {
                                _adpater.FillBy(table, instance.TxnID);

                                DataRow dr = table.Rows.Cast<DataRow>().SingleOrDefault(s => string.Compare(s.Field<string>(InvoiceFields.TxnID), instance.TxnID, true) == 0);

                                if (object.ReferenceEquals(dr, null))
                                {
                                    dr = table.NewRow();
                                    table.Rows.Add(dr);
                                }
                                Bind(dr, instance);
                            }
                            #endregion

                            #region Save Child
                            if (instance.Invoicelinedetails.Count > 0)
                            {
                                using (InvoicelinedetailDal InvoicelinedetailDal = new InvoicelinedetailDal(_fullsyn, _constring, TableName.invoice, instance.TxnID))
                                {
                                    InvoicelinedetailDal.Save(instance.Invoicelinedetails);
                                }
                            }

                            if (instance.Invoicelinegroupdetails.Count > 0)
                            {
                                using (InvoicelinegroupdetailDal InvoicelinegroupdetailDal = new InvoicelinegroupdetailDal(_fullsyn, _constring, TableName.invoice, instance.TxnID))
                                {
                                    InvoicelinegroupdetailDal.Save(instance.Invoicelinegroupdetails);
                                }
                            }

                            if (instance.Linkedtxndetails.Count > 0)
                            {
                                using (LinkedtxndetailDal LinkedtxndetailDal = new LinkedtxndetailDal(_fullsyn, _constring, TableName.invoice, instance.TxnID))
                                {
                                    LinkedtxndetailDal.Save(instance.Linkedtxndetails);
                                }
                            }
                            #endregion
                        }
                        _adpater.Update(table);
                    }
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Invoice invoice)
        {
            //dr[InvoiceFields.CustomField2] = invoice.CustomField5;
            dr[InvoiceFields.TxnID] = invoice.TxnID;
            dr[InvoiceFields.TimeCreated] = invoice.TimeCreated;
            dr[InvoiceFields.TimeModified] = invoice.TimeModified;
            dr[InvoiceFields.EditSequence] = invoice.EditSequence;
            dr[InvoiceFields.TxnNumber] = invoice.TxnNumber.GetDbType();
            dr[InvoiceFields.CustomerRef_ListID] = invoice.CustomerRef_ListID;
            dr[InvoiceFields.CustomerRef_FullName] = invoice.CustomerRef_FullName;
            dr[InvoiceFields.ClassRef_ListID] = invoice.ClassRef_ListID;
            dr[InvoiceFields.ClassRef_FullName] = invoice.ClassRef_FullName;
            dr[InvoiceFields.ARAccountRef_ListID] = invoice.ARAccountRef_ListID;
            dr[InvoiceFields.ARAccountRef_FullName] = invoice.ARAccountRef_FullName;
            dr[InvoiceFields.TemplateRef_ListID] = invoice.TemplateRef_ListID;
            dr[InvoiceFields.TemplateRef_FullName] = invoice.TemplateRef_FullName;
            dr[InvoiceFields.TxnDate] = invoice.TxnDate.GetDbType();
            dr[InvoiceFields.RefNumber] = invoice.RefNumber;
            dr[InvoiceFields.BillAddress_Addr1] = invoice.BillAddress_Addr1;
            dr[InvoiceFields.BillAddress_Addr2] = invoice.BillAddress_Addr2;
            dr[InvoiceFields.BillAddress_Addr3] = invoice.BillAddress_Addr3;
            dr[InvoiceFields.BillAddress_Addr4] = invoice.BillAddress_Addr4;
            dr[InvoiceFields.BillAddress_Addr5] = invoice.BillAddress_Addr5;
            dr[InvoiceFields.BillAddress_City] = invoice.BillAddress_City;
            dr[InvoiceFields.BillAddress_State] = invoice.BillAddress_State;
            dr[InvoiceFields.BillAddress_PostalCode] = invoice.BillAddress_PostalCode;
            dr[InvoiceFields.BillAddress_Country] = invoice.BillAddress_Country;
            dr[InvoiceFields.BillAddress_Note] = invoice.BillAddress_Note;
            dr[InvoiceFields.ShipAddress_Addr1] = invoice.ShipAddress_Addr1;
            dr[InvoiceFields.ShipAddress_Addr2] = invoice.ShipAddress_Addr2;
            dr[InvoiceFields.ShipAddress_Addr3] = invoice.ShipAddress_Addr3;
            dr[InvoiceFields.ShipAddress_Addr4] = invoice.ShipAddress_Addr4;
            dr[InvoiceFields.ShipAddress_Addr5] = invoice.ShipAddress_Addr5;
            dr[InvoiceFields.ShipAddress_City] = invoice.ShipAddress_City;
            dr[InvoiceFields.ShipAddress_State] = invoice.ShipAddress_State;
            dr[InvoiceFields.ShipAddress_PostalCode] = invoice.ShipAddress_PostalCode;
            dr[InvoiceFields.ShipAddress_Country] = invoice.ShipAddress_Country;
            dr[InvoiceFields.ShipAddress_Note] = invoice.ShipAddress_Note;
            dr[InvoiceFields.IsPending] = invoice.IsPending.GetShort().GetDbType();
            dr[InvoiceFields.IsFinanceCharge] = invoice.IsFinanceCharge.GetShort().GetDbType();
            dr[InvoiceFields.PONumber] = invoice.PONumber;
            dr[InvoiceFields.TermsRef_ListID] = invoice.TermsRef_ListID;
            dr[InvoiceFields.TermsRef_FullName] = invoice.TermsRef_FullName;
            dr[InvoiceFields.DueDate] = invoice.DueDate.GetDbType();
            dr[InvoiceFields.SalesRepRef_ListID] = invoice.SalesRepRef_ListID;
            dr[InvoiceFields.SalesRepRef_FullName] = invoice.SalesRepRef_FullName;
            dr[InvoiceFields.Fob] = invoice.Fob;
            dr[InvoiceFields.ShipDate] = invoice.ShipDate.GetDbType();
            dr[InvoiceFields.ShipMethodRef_ListID] = invoice.ShipMethodRef_ListID;
            dr[InvoiceFields.ShipMethodRef_FullName] = invoice.ShipMethodRef_FullName;
            dr[InvoiceFields.Subtotal] = invoice.Subtotal.GetDbType();
            dr[InvoiceFields.ItemSalesTaxRef_ListID] = invoice.ItemSalesTaxRef_ListID;
            dr[InvoiceFields.ItemSalesTaxRef_FullName] = invoice.ItemSalesTaxRef_FullName;
            dr[InvoiceFields.SalesTaxPercentage] = invoice.SalesTaxPercentage;
            dr[InvoiceFields.SalesTaxTotal] = invoice.SalesTaxTotal.GetDbType();
            dr[InvoiceFields.AppliedAmount] = invoice.AppliedAmount.GetDbType();
            dr[InvoiceFields.BalanceRemaining] = invoice.BalanceRemaining.GetDbType();
            dr[InvoiceFields.CurrencyRef_ListID] = invoice.CurrencyRef_ListID;
            dr[InvoiceFields.CurrencyRef_FullName] = invoice.CurrencyRef_FullName;
            dr[InvoiceFields.ExchangeRate] = invoice.ExchangeRate.GetDbType();
            dr[InvoiceFields.BalanceRemainingInHomeCurrency] = invoice.BalanceRemainingInHomeCurrency.GetDbType();
            dr[InvoiceFields.Memo] = invoice.Memo;
            dr[InvoiceFields.IsPaid] = invoice.IsPaid.GetShort().GetDbType();
            dr[InvoiceFields.CustomerMsgRef_ListID] = invoice.CustomerMsgRef_ListID;
            dr[InvoiceFields.CustomerMsgRef_FullName] = invoice.CustomerMsgRef_FullName;
            dr[InvoiceFields.IsToBePrinted] = invoice.IsToBePrinted.GetShort().GetDbType();
            dr[InvoiceFields.IsToBeEmailed] = invoice.IsToBeEmailed.GetShort().GetDbType();
            dr[InvoiceFields.IsTaxIncluded] = invoice.IsTaxIncluded.GetShort().GetDbType();
            dr[InvoiceFields.CustomerSalesTaxCodeRef_ListID] = invoice.CustomerSalesTaxCodeRef_ListID;
            dr[InvoiceFields.CustomerSalesTaxCodeRef_FullName] = invoice.CustomerSalesTaxCodeRef_FullName;
            dr[InvoiceFields.SuggestedDiscountAmount] = invoice.SuggestedDiscountAmount.GetDbType();
            dr[InvoiceFields.SuggestedDiscountDate] = invoice.SuggestedDiscountDate.GetDbType();
            dr[InvoiceFields.Other] = invoice.Other;
            dr[InvoiceFields.CustomField1] = invoice.CustomField1;
            dr[InvoiceFields.CustomField2] = invoice.CustomField2;
            dr[InvoiceFields.CustomField3] = invoice.CustomField3;
            dr[InvoiceFields.CustomField4] = invoice.CustomField4;
            dr[InvoiceFields.CustomField5] = invoice.CustomField5;
            dr[InvoiceFields.CustomField6] = invoice.CustomField6;
            dr[InvoiceFields.CustomField7] = invoice.CustomField7;
            dr[InvoiceFields.CustomField8] = invoice.CustomField8;
            dr[InvoiceFields.CustomField9] = invoice.CustomField9;
            dr[InvoiceFields.CustomField10] = invoice.CustomField10;
            dr[InvoiceFields.Status] = invoice.Status;
            dr[InvoiceFields.IsSyn] = (short)SynStatus.Synchronize;
            dr[InvoiceFields.SynDate] = DBNull.Value;
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adpater.Dispose();
                    _adpater = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion
    }
}
