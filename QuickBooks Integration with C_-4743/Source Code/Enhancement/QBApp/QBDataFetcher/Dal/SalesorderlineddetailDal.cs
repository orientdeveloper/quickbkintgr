﻿//
// Class	:	SalesorderlinedetailDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;


namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class SalesorderlinedetailDal : IDisposable
    {
		#region Class Level Variables
        bool _fullsyn;
        string _mastertable;
        string _idkey;
        salesorderlinedetailTableAdapter _adapter;
        #endregion

        #region Constructors / Destructors
        public SalesorderlinedetailDal(bool fullsyn, string conString, string mastertable, string idkey)
        {
            _adapter = new salesorderlinedetailTableAdapter();
            _adapter.Connection.ConnectionString = conString;
            _fullsyn = fullsyn;
            _idkey = idkey;
            _mastertable = mastertable;
        }
        #endregion

        #region Methods (Public)
        public void Save(List<Salesorderlinedetail> Salesorderlinedetails)
        {
            try
            {
                QBdb.salesorderlinedetailDataTable table = new Dal.QBdb.salesorderlinedetailDataTable();

                if (!_fullsyn)
                {
                    _adapter.DeleteBy(_idkey, _mastertable);
                }

                foreach (Salesorderlinedetail instance in Salesorderlinedetails)
                {                    
                    DataRow dr = table.NewRow();
                    table.Rows.Add(dr);
                    Bind(dr, instance);
                }
                _adapter.Update(table);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Salesorderlinedetail salesorderlinedetail)
        {
            try
            {
                dr[SalesorderlinedetailFields.TxnLineID] = salesorderlinedetail.TxnLineID;
                dr[SalesorderlinedetailFields.ItemRef_ListID] = salesorderlinedetail.ItemRef_ListID;
                dr[SalesorderlinedetailFields.ItemRef_FullName] = salesorderlinedetail.ItemRef_FullName;
                dr[SalesorderlinedetailFields.Desc] = salesorderlinedetail.Desc;
                dr[SalesorderlinedetailFields.Quantity] = salesorderlinedetail.Quantity;
                dr[SalesorderlinedetailFields.UnitOfMeasure] = salesorderlinedetail.UnitOfMeasure;
                dr[SalesorderlinedetailFields.OverrideUOMSetRef_ListID] = salesorderlinedetail.OverrideUOMSetRef_ListID;
                dr[SalesorderlinedetailFields.OverrideUOMSetRef_FullName] = salesorderlinedetail.OverrideUOMSetRef_FullName;
                dr[SalesorderlinedetailFields.Rate] = salesorderlinedetail.Rate;
                dr[SalesorderlinedetailFields.RatePercent] = salesorderlinedetail.RatePercent;
                dr[SalesorderlinedetailFields.ClassRef_ListID] = salesorderlinedetail.ClassRef_ListID;
                dr[SalesorderlinedetailFields.ClassRef_FullName] = salesorderlinedetail.ClassRef_FullName;
                dr[SalesorderlinedetailFields.Amount] = salesorderlinedetail.Amount.GetDbType();
                dr[SalesorderlinedetailFields.InventorySiteRef_ListID] = salesorderlinedetail.InventorySiteRef_ListID;
                dr[SalesorderlinedetailFields.InventorySiteRef_FullName] = salesorderlinedetail.InventorySiteRef_FullName;
                dr[SalesorderlinedetailFields.SalesTaxCodeRef_ListID] = salesorderlinedetail.SalesTaxCodeRef_ListID;
                dr[SalesorderlinedetailFields.SalesTaxCodeRef_FullName] = salesorderlinedetail.SalesTaxCodeRef_FullName;
                dr[SalesorderlinedetailFields.Invoiced] = salesorderlinedetail.Invoiced;
                dr[SalesorderlinedetailFields.IsManuallyClosed] = salesorderlinedetail.IsManuallyClosed.GetShort().GetDbType();
                dr[SalesorderlinedetailFields.Other1] = salesorderlinedetail.Other1;
                dr[SalesorderlinedetailFields.Other2] = salesorderlinedetail.Other2;
                dr[SalesorderlinedetailFields.CustomField1] = salesorderlinedetail.CustomField1;
                dr[SalesorderlinedetailFields.CustomField2] = salesorderlinedetail.CustomField2;
                dr[SalesorderlinedetailFields.CustomField3] = salesorderlinedetail.CustomField3;
                dr[SalesorderlinedetailFields.CustomField4] = salesorderlinedetail.CustomField4;
                dr[SalesorderlinedetailFields.CustomField5] = salesorderlinedetail.CustomField5;
                dr[SalesorderlinedetailFields.CustomField6] = salesorderlinedetail.CustomField6;
                dr[SalesorderlinedetailFields.CustomField7] = salesorderlinedetail.CustomField7;
                dr[SalesorderlinedetailFields.CustomField8] = salesorderlinedetail.CustomField8;
                dr[SalesorderlinedetailFields.CustomField9] = salesorderlinedetail.CustomField9;
                dr[SalesorderlinedetailFields.CustomField10] = salesorderlinedetail.CustomField10;
                dr[SalesorderlinedetailFields.Idkey] = salesorderlinedetail.Idkey;
                dr[SalesorderlinedetailFields.GroupIDKEY] = salesorderlinedetail.GroupIDKEY;
                dr[SalesorderlinedetailFields.TableName] = salesorderlinedetail.TableName;
                dr[SalesorderlinedetailFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[SalesorderlinedetailFields.SynDate] = DBNull.Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion				

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adapter.Dispose();
                    _adapter = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
