//
// Class	:	DepositlinedetailDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:46 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class DepositlinedetailDal : IDisposable
    {
        #region Class Level Variables
        bool _fullsyn;
        string _mastertable;
        string _idkey;
        depositlinedetailTableAdapter _adapter;
        #endregion

        #region Constructors / Destructors
        public DepositlinedetailDal(bool fullsyn, string conString, string mastertable, string idkey)
        {
            _adapter = new depositlinedetailTableAdapter();
            _adapter.Connection.ConnectionString = conString;
            _fullsyn = fullsyn;
            _idkey = idkey;
            _mastertable = mastertable;
        }
        #endregion

        #region Methods (Public)
        public void Save(List<Depositlinedetail> Depositlinedetails)
        {
            try
            {
                QBdb.depositlinedetailDataTable table = new Dal.QBdb.depositlinedetailDataTable();

                if (!_fullsyn)
                {
                    _adapter.DeleteBy(_idkey, _mastertable);
                }

                foreach (Depositlinedetail instance in Depositlinedetails)
                {                    
                    DataRow dr = table.NewRow();
                    table.Rows.Add(dr);
                    Bind(dr, instance);
                }
                _adapter.Update(table);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Depositlinedetail depositlinedetail)
        {
            try
            {
                dr[DepositlinedetailFields.TxnType] = depositlinedetail.TxnType;
                dr[DepositlinedetailFields.TxnID] = depositlinedetail.TxnID;
                dr[DepositlinedetailFields.TxnLineID] = depositlinedetail.TxnLineID;
                dr[DepositlinedetailFields.PaymentTxnLineID] = depositlinedetail.PaymentTxnLineID;
                dr[DepositlinedetailFields.EntityRef_ListID] = depositlinedetail.EntityRef_ListID;
                dr[DepositlinedetailFields.EntityRef_FullName] = depositlinedetail.EntityRef_FullName;
                dr[DepositlinedetailFields.AccountRef_ListID] = depositlinedetail.AccountRef_ListID;
                dr[DepositlinedetailFields.AccountRef_FullName] = depositlinedetail.AccountRef_FullName;
                dr[DepositlinedetailFields.Memo] = depositlinedetail.Memo;
                dr[DepositlinedetailFields.CheckNumber] = depositlinedetail.CheckNumber;
                dr[DepositlinedetailFields.PaymentMethodRef_ListID] = depositlinedetail.PaymentMethodRef_ListID;
                dr[DepositlinedetailFields.PaymentMethodRef_FullName] = depositlinedetail.PaymentMethodRef_FullName;
                dr[DepositlinedetailFields.ClassRef_ListID] = depositlinedetail.ClassRef_ListID;
                dr[DepositlinedetailFields.ClassRef_FullName] = depositlinedetail.ClassRef_FullName;
                dr[DepositlinedetailFields.Amount] = depositlinedetail.Amount.GetDbType();
                dr[DepositlinedetailFields.CustomField1] = depositlinedetail.CustomField1;
                dr[DepositlinedetailFields.CustomField2] = depositlinedetail.CustomField2;
                dr[DepositlinedetailFields.CustomField3] = depositlinedetail.CustomField3;
                dr[DepositlinedetailFields.CustomField4] = depositlinedetail.CustomField4;
                dr[DepositlinedetailFields.CustomField5] = depositlinedetail.CustomField5;
                dr[DepositlinedetailFields.CustomField6] = depositlinedetail.CustomField6;
                dr[DepositlinedetailFields.CustomField7] = depositlinedetail.CustomField7;
                dr[DepositlinedetailFields.CustomField8] = depositlinedetail.CustomField8;
                dr[DepositlinedetailFields.CustomField9] = depositlinedetail.CustomField9;
                dr[DepositlinedetailFields.CustomField10] = depositlinedetail.CustomField10;
                dr[DepositlinedetailFields.Idkey] = depositlinedetail.Idkey;
                dr[DepositlinedetailFields.GroupIDKEY] = depositlinedetail.GroupIDKEY;
                dr[DepositlinedetailFields.TableName] = depositlinedetail.TableName;
                dr[DepositlinedetailFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[DepositlinedetailFields.SynDate] = DBNull.Value;
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adapter.Dispose();
                    _adapter = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
