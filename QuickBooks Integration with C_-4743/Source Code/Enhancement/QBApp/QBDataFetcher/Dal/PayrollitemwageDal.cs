﻿//
// Class	:	PayrollitemwageDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:53 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;
using QBDataFetcher.DAL;

namespace QBDataFetcher.Dal
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class PayrollitemwageDal : IDisposable, IDal
    {
        #region Class Level Variables
        bool _fullsyn;
        string _qbfile;
        DateTime? _fromsyn;
        DateTime? _tosyn;
        int _chunksize;
        string _constring;
        payrollitemwageTableAdapter _adpater;
        #endregion

        #region Constructors / Destructors
        public PayrollitemwageDal(string qbfile, int chunksize, DateTime? fromsyn, DateTime? tosyn, bool fullsyn, string constring)
        {
            _adpater = new payrollitemwageTableAdapter();
            _adpater.Connection.ConnectionString = constring;
            _adpater.ClearBeforeFill = false;
            _fullsyn = fullsyn;
            _fromsyn = fromsyn;
            _tosyn = tosyn;
            _qbfile = qbfile;
            _chunksize = chunksize;
            _constring = constring;
        }
        #endregion

        #region Methods (Public)
        public void Synchronize()
        {
            try
            {
                using (PayrollitemwageFetch fetcher = new PayrollitemwageFetch(_qbfile, _chunksize, _fromsyn, _tosyn, Logger))
                {
                    while (fetcher.IsNextChunck)
                    {
                        QBdb.payrollitemwageDataTable table = new Dal.QBdb.payrollitemwageDataTable();
                        foreach (Payrollitemwage instance in fetcher.FetchNext())
                        {
                            #region Save Master
                            if (_fullsyn)
                            {
                                DataRow dr = table.NewRow();
                                table.Rows.Add(dr);
                                Bind(dr, instance);
                            }
                            else
                            {
                                _adpater.FillBy(table, instance.ListID);

                                DataRow dr = table.Rows.Cast<DataRow>().SingleOrDefault(s => string.Compare(s.Field<string>(PayrollitemwageFields.ListID), instance.ListID, true) == 0);

                                if (object.ReferenceEquals(dr, null))
                                {
                                    dr = table.NewRow();
                                    table.Rows.Add(dr);
                                }
                                Bind(dr, instance);
                            }
                            #endregion

                            #region Save Child
                            if (instance.Items.Count > 0)
                            {
                                using (ItemDal ItemDal = new ItemDal(_fullsyn, _constring, TableName.Payrollitemwage, instance.ListID))
                                {
                                    ItemDal.Save(instance.Items);
                                }
                            }
                            #endregion
                        }
                        _adpater.Update(table);
                    }
                }
            }
            catch 
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Payrollitemwage payrollitemwage)
        {
            try
            {
                dr[PayrollitemwageFields.ListID] = payrollitemwage.ListID;
                dr[PayrollitemwageFields.TimeCreated] = payrollitemwage.TimeCreated;
                dr[PayrollitemwageFields.TimeModified] = payrollitemwage.TimeModified;
                dr[PayrollitemwageFields.EditSequence] = payrollitemwage.EditSequence;
                dr[PayrollitemwageFields.Name] = payrollitemwage.Name;
                dr[PayrollitemwageFields.IsActive] = payrollitemwage.IsActive.GetShort().GetDbType();
                dr[PayrollitemwageFields.WageType] = payrollitemwage.WageType;
                dr[PayrollitemwageFields.ExpenseAccountRef_ListID] = payrollitemwage.ExpenseAccountRef_ListID;
                dr[PayrollitemwageFields.ExpenseAccountRef_FullName] = payrollitemwage.ExpenseAccountRef_FullName;
                dr[PayrollitemwageFields.Status] = payrollitemwage.Status;
                dr[PayrollitemwageFields.SynDate] = DBNull.Value;
                dr[PayrollitemwageFields.IsSyn] = (short)SynStatus.Synchronize;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adpater.Dispose();
                    _adpater = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion
    }
}
