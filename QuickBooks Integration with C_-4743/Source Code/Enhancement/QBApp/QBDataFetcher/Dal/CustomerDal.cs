//
// Class	:	CustomerDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:45 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class CustomerDal : IDisposable, IDal
    {

        #region Class Level Variables
         bool _Isfirsttime;
        string _qbfile;
        DateTime? _fromsyn;
        DateTime? _tosyn;
        int _chunksize;
        string _constring;
        customerTableAdapter _adpater;
        #endregion

        #region Constructors / Destructors
        public CustomerDal(string qbfile, int chunksize, DateTime? fromsyn,DateTime? tosyn, bool isfirsttime, string constring)
        {
            _adpater = new customerTableAdapter();
            _adpater.Connection.ConnectionString = constring;
            _adpater.ClearBeforeFill = false;
            _Isfirsttime = isfirsttime;
            _fromsyn = fromsyn;
            _tosyn = tosyn;
            _qbfile = qbfile;
            _chunksize = chunksize;
            _constring = constring;
        }
        #endregion
    
        #region Methods (Public)
        public void Synchronize()
        {
            try
            {
                using (CustomerFetch fetcher = new CustomerFetch(_qbfile, _chunksize, _fromsyn, _tosyn, Logger))
                {
                    while (fetcher.IsNextChunck)
                    {
                        QBdb.customerDataTable table = new Dal.QBdb.customerDataTable();
                        foreach (Customer instance in fetcher.FetchNext())
                        {
                            #region Master Save
                            if (_Isfirsttime)
                            {
                                DataRow dr = table.NewRow();
                                table.Rows.Add(dr);
                                Bind(dr, instance);
                            }
                            else
                            {
                                _adpater.FillBy(table, instance.ListID);

                                DataRow dr = table.Rows.Cast<DataRow>().SingleOrDefault(s => string.Compare(s.Field<string>(CustomerFields.ListID), instance.ListID, true) == 0);

                                if (object.ReferenceEquals(dr, null))
                                {
                                    dr = table.NewRow();
                                    table.Rows.Add(dr);
                                }
                                Bind(dr, instance);
                            }
                            #endregion

                            #region Save Child
                            if (instance.DataExtDetails.Count > 0)
                            {
                                using (DataextdetailDal DataextdetailDal = new DataextdetailDal(_Isfirsttime, _constring, TableName.customer, instance.ListID))
                                {
                                    DataextdetailDal.Save(instance.DataExtDetails);
                                }
                            }
                            #endregion
                        }
                        _adpater.Update(table);
                    }
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Customer customer)
        {
            dr[CustomerFields.ListID] = customer.ListID;
            dr[CustomerFields.TimeCreated] = customer.TimeCreated;
            dr[CustomerFields.TimeModified] = customer.TimeModified;
            dr[CustomerFields.EditSequence] = customer.EditSequence;
            dr[CustomerFields.Name] = customer.Name;
            dr[CustomerFields.FullName] = customer.FullName;
            dr[CustomerFields.IsActive] = customer.IsActive.GetShort().GetDbType();
            dr[CustomerFields.ParentRef_ListID] = customer.ParentRef_ListID;
            dr[CustomerFields.ParentRef_FullName] = customer.ParentRef_FullName;
            dr[CustomerFields.Sublevel] = customer.Sublevel.GetDbType();
            dr[CustomerFields.CompanyName] = customer.CompanyName;
            dr[CustomerFields.Salutation] = customer.Salutation;
            dr[CustomerFields.FirstName] = customer.FirstName;
            dr[CustomerFields.MiddleName] = customer.MiddleName;
            dr[CustomerFields.LastName] = customer.LastName;
            dr[CustomerFields.Suffix] = customer.Suffix;
            dr[CustomerFields.BillAddress_Addr1] = customer.BillAddress_Addr1;
            dr[CustomerFields.BillAddress_Addr2] = customer.BillAddress_Addr2;
            dr[CustomerFields.BillAddress_Addr3] = customer.BillAddress_Addr3;
            dr[CustomerFields.BillAddress_Addr4] = customer.BillAddress_Addr4;
            dr[CustomerFields.BillAddress_Addr5] = customer.BillAddress_Addr5;
            dr[CustomerFields.BillAddress_City] = customer.BillAddress_City;
            dr[CustomerFields.BillAddress_PostalCode] = customer.BillAddress_PostalCode;
            dr[CustomerFields.BillAddress_State] = customer.BillAddress_State;
            dr[CustomerFields.BillAddress_Country] = customer.BillAddress_Country;
            dr[CustomerFields.BillAddress_Note] = customer.BillAddress_Note;
            dr[CustomerFields.ShipAddress_Addr1] = customer.ShipAddress_Addr1;
            dr[CustomerFields.ShipAddress_Addr2] = customer.ShipAddress_Addr2;
            dr[CustomerFields.ShipAddress_Addr3] = customer.ShipAddress_Addr3;
            dr[CustomerFields.ShipAddress_Addr4] = customer.ShipAddress_Addr4;
            dr[CustomerFields.ShipAddress_Addr5] = customer.ShipAddress_Addr5;
            dr[CustomerFields.ShipAddress_City] = customer.ShipAddress_City;
            dr[CustomerFields.ShipAddress_State] = customer.ShipAddress_State;
            dr[CustomerFields.ShipAddress_PostalCode] = customer.ShipAddress_PostalCode;
            dr[CustomerFields.ShipAddress_Country] = customer.ShipAddress_Country;
            dr[CustomerFields.ShipAddress_Note] = customer.ShipAddress_Note;
            dr[CustomerFields.PrintAs] = customer.PrintAs;
            dr[CustomerFields.Phone] = customer.Phone;
            dr[CustomerFields.Mobile] = customer.Mobile;
            dr[CustomerFields.Pager] = customer.Pager;
            dr[CustomerFields.AltPhone] = customer.AltPhone;
            dr[CustomerFields.Fax] = customer.Fax;
            dr[CustomerFields.Email] = customer.Email;
            dr[CustomerFields.Contact] = customer.Contact;
            dr[CustomerFields.CustomerTypeRef_ListID] = customer.CustomerTypeRef_ListID;
            dr[CustomerFields.CustomerTypeRef_FullName] = customer.CustomerTypeRef_FullName;
            dr[CustomerFields.TermsRef_ListID] = customer.TermsRef_ListID;
            dr[CustomerFields.TermsRef_FullName] = customer.TermsRef_FullName;
            dr[CustomerFields.SalesRepRef_ListID] = customer.SalesRepRef_ListID;
            dr[CustomerFields.SalesRepRef_FullName] = customer.SalesRepRef_FullName;
            dr[CustomerFields.Balance] = customer.Balance.GetDbType();
            dr[CustomerFields.TotalBalance] = customer.TotalBalance.GetDbType();
            dr[CustomerFields.SalesTaxCodeRef_ListID] = customer.SalesTaxCodeRef_ListID;
            dr[CustomerFields.SalesTaxCodeRef_FullName] = customer.SalesTaxCodeRef_FullName;
            dr[CustomerFields.ItemSalesTaxRef_ListID] = customer.ItemSalesTaxRef_ListID;
            dr[CustomerFields.ItemSalesTaxRef_FullName] = customer.ItemSalesTaxRef_FullName;
            dr[CustomerFields.SalesTaxCountry] = customer.SalesTaxCountry;
            dr[CustomerFields.ResaleNumber] = customer.ResaleNumber;
            dr[CustomerFields.AccountNumber] = customer.AccountNumber;
            dr[CustomerFields.PreferredPaymentMethodRef_ListID] = customer.PreferredPaymentMethodRef_ListID;
            dr[CustomerFields.PreferredPaymentMethodRef_FullName] = customer.PreferredPaymentMethodRef_FullName;
            dr[CustomerFields.CreditCardNumber] = customer.CreditCardNumber;
            dr[CustomerFields.ExpirationMonth] = customer.ExpirationMonth.GetDbType();
            dr[CustomerFields.ExpirationYear] = customer.ExpirationYear.GetDbType();
            dr[CustomerFields.NameOnCard] = customer.NameOnCard;
            dr[CustomerFields.CreditCardAddress] = customer.CreditCardAddress;
            dr[CustomerFields.CreditCardPostalCode] = customer.CreditCardPostalCode;
            dr[CustomerFields.JobStatus] = customer.JobStatus;
            dr[CustomerFields.JobStartDate] = customer.JobStartDate.GetDbType();
            dr[CustomerFields.JobProjectedEndDate] = customer.JobProjectedEndDate.GetDbType();
            dr[CustomerFields.JobEndDate] = customer.JobEndDate.GetDbType();
            dr[CustomerFields.JobDesc] = customer.JobDesc;
            dr[CustomerFields.JobTypeRef_ListID] = customer.JobTypeRef_ListID;
            dr[CustomerFields.JobTypeRef_FullName] = customer.JobTypeRef_FullName;
            dr[CustomerFields.Notes] = customer.Notes;
            dr[CustomerFields.PriceLevelRef_ListID] = customer.PriceLevelRef_ListID;
            dr[CustomerFields.PriceLevelRef_FullName] = customer.PriceLevelRef_FullName;
            dr[CustomerFields.TaxRegistrationNumber] = customer.TaxRegistrationNumber;
            dr[CustomerFields.CurrencyRef_ListID] = customer.CurrencyRef_ListID;
            dr[CustomerFields.CurrencyRef_FullName] = customer.CurrencyRef_FullName;
            dr[CustomerFields.IsStatementWithParent] = customer.IsStatementWithParent.GetShort().GetDbType();
            dr[CustomerFields.DeliveryMethod] = customer.DeliveryMethod;
            dr[CustomerFields.CustomField1] = customer.CustomField1;
            dr[CustomerFields.CustomField2] = customer.CustomField2;
            dr[CustomerFields.CustomField3] = customer.CustomField3;
            dr[CustomerFields.CustomField4] = customer.CustomField4;
            dr[CustomerFields.CustomField5] = customer.CustomField5;
            dr[CustomerFields.CustomField6] = customer.CustomField6;
            dr[CustomerFields.CustomField7] = customer.CustomField7;
            dr[CustomerFields.CustomField8] = customer.CustomField8;
            dr[CustomerFields.CustomField9] = customer.CustomField9;
            dr[CustomerFields.CustomField10] = customer.CustomField10;
            dr[CustomerFields.Status] = customer.Status;
            dr[CustomerFields.IsSyn] = (short)SynStatus.Synchronize;
            dr[CustomerFields.SynDate] = DBNull.Value;

        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adpater.Dispose();
                    _adpater = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion
    }
}
