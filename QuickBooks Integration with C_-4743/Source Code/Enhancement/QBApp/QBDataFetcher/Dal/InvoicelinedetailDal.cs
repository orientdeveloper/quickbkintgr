//
// Class	:	InvoicelinedetailDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:50 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;
namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class InvoicelinedetailDal : IDisposable
    {
        #region Class Level Variables
        bool _fullsyn;
        string _mastertable;
        string _idkey;
        invoicelinedetailTableAdapter _adapter;
        #endregion

        #region Constructors / Destructors
        public InvoicelinedetailDal(bool fullsyn, string conString, string mastertable, string idkey)
        {
            _adapter = new invoicelinedetailTableAdapter();
            _adapter.Connection.ConnectionString = conString;
            _fullsyn = fullsyn;
            _idkey = idkey;
            _mastertable = mastertable;
        }
        #endregion

        #region Methods (Public)
        public void Save(List<Invoicelinedetail> Invoicelinedetails)
        {
            try
            {
                QBdb.invoicelinedetailDataTable table = new Dal.QBdb.invoicelinedetailDataTable();

                if (!_fullsyn)
                {
                    _adapter.DeleteBy(_idkey, _mastertable);
                }

                foreach (Invoicelinedetail instance in Invoicelinedetails)
                {                    
                    DataRow dr = table.NewRow();
                    table.Rows.Add(dr);
                    Bind(dr, instance);
                }
                _adapter.Update(table);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Invoicelinedetail invoicelinedetail)
        {
            try
            {
                dr[InvoicelinedetailFields.TxnLineID] = invoicelinedetail.TxnLineID;
                dr[InvoicelinedetailFields.ItemRef_ListID] = invoicelinedetail.ItemRef_ListID;
                dr[InvoicelinedetailFields.ItemRef_FullName] = invoicelinedetail.ItemRef_FullName;
                dr[InvoicelinedetailFields.Desc] = invoicelinedetail.Desc;
                dr[InvoicelinedetailFields.Quantity] = invoicelinedetail.Quantity;
                dr[InvoicelinedetailFields.UnitOfMeasure] = invoicelinedetail.UnitOfMeasure;
                dr[InvoicelinedetailFields.OverrideUOMSetRef_ListID] = invoicelinedetail.OverrideUOMSetRef_ListID;
                dr[InvoicelinedetailFields.OverrideUOMSetRef_FullName] = invoicelinedetail.OverrideUOMSetRef_FullName;
                dr[InvoicelinedetailFields.Rate] = invoicelinedetail.Rate;
                dr[InvoicelinedetailFields.RatePercent] = invoicelinedetail.RatePercent;
                dr[InvoicelinedetailFields.ClassRef_ListID] = invoicelinedetail.ClassRef_ListID;
                dr[InvoicelinedetailFields.ClassRef_FullName] = invoicelinedetail.ClassRef_FullName;
                dr[InvoicelinedetailFields.Amount] = invoicelinedetail.Amount.GetDbType();
                dr[InvoicelinedetailFields.InventorySiteRef_ListID] = invoicelinedetail.InventorySiteRef_ListID;
                dr[InvoicelinedetailFields.InventorySiteRef_FullName] = invoicelinedetail.InventorySiteRef_FullName;
                dr[InvoicelinedetailFields.ServiceDate] = invoicelinedetail.ServiceDate.GetDbType();
                dr[InvoicelinedetailFields.SalesTaxCodeRef_ListID] = invoicelinedetail.SalesTaxCodeRef_ListID;
                dr[InvoicelinedetailFields.SalesTaxCodeRef_FullName] = invoicelinedetail.SalesTaxCodeRef_FullName;
                dr[InvoicelinedetailFields.Other1] = invoicelinedetail.Other1;
                dr[InvoicelinedetailFields.Other2] = invoicelinedetail.Other2;
                dr[InvoicelinedetailFields.LinkedTxnID] = invoicelinedetail.LinkedTxnID;
                dr[InvoicelinedetailFields.LinkedTxnLineID] = invoicelinedetail.LinkedTxnLineID;
                dr[InvoicelinedetailFields.CustomField1] = invoicelinedetail.CustomField1;
                dr[InvoicelinedetailFields.CustomField2] = invoicelinedetail.CustomField2;
                dr[InvoicelinedetailFields.CustomField3] = invoicelinedetail.CustomField3;
                dr[InvoicelinedetailFields.CustomField4] = invoicelinedetail.CustomField4;
                dr[InvoicelinedetailFields.CustomField5] = invoicelinedetail.CustomField5;
                dr[InvoicelinedetailFields.CustomField6] = invoicelinedetail.CustomField6;
                dr[InvoicelinedetailFields.CustomField7] = invoicelinedetail.CustomField7;
                dr[InvoicelinedetailFields.CustomField8] = invoicelinedetail.CustomField8;
                dr[InvoicelinedetailFields.CustomField9] = invoicelinedetail.CustomField9;
                dr[InvoicelinedetailFields.CustomField10] = invoicelinedetail.CustomField10;
                dr[InvoicelinedetailFields.Idkey] = invoicelinedetail.Idkey;
                dr[InvoicelinedetailFields.GroupIDKEY] = invoicelinedetail.GroupIDKEY;
                dr[InvoicelinedetailFields.TableName] = invoicelinedetail.TableName;
                dr[InvoicelinedetailFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[InvoicelinedetailFields.SynDate] = DBNull.Value;
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adapter.Dispose();
                    _adapter = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion
    }
}
