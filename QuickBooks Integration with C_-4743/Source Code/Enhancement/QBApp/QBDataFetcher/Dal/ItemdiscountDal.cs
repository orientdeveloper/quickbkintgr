//
// Class	:	ItemdiscountDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/5/2013 10:11:50 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class ItemdiscountDal : IDisposable, IDal
    {
        #region Class Level Variables
         bool _fullsyn;
        string _qbfile;
        DateTime? _fromsyn;
        DateTime? _tosyn;
        int _chunksize;
        string _constring;
        itemdiscountTableAdapter _adpater;
        #endregion

        #region Constructors / Destructors
        public ItemdiscountDal(string qbfile, int chunksize, DateTime? fromsyn, DateTime? tosyn, bool fullsyn, string constring)
        {
            _adpater = new itemdiscountTableAdapter();
            _adpater.Connection.ConnectionString = constring;
            _adpater.ClearBeforeFill = false;
            _fullsyn = fullsyn;
            _fromsyn = fromsyn;
            _tosyn = tosyn;
            _qbfile = qbfile;
            _chunksize = chunksize;
            _constring = constring;
        }
        #endregion
        
        #region Methods (Public)
        public void Synchronize()
        {
            try
            {
                using (ItemDiscountFetch fetcher = new ItemDiscountFetch(_qbfile, _chunksize, _fromsyn, _tosyn, Logger))
                {
                    while (fetcher.IsNextChunck)
                    {
                        QBdb.itemdiscountDataTable table = new Dal.QBdb.itemdiscountDataTable();
                        foreach (Itemdiscount instance in fetcher.FetchNext())
                        {
                            #region Save Master
                            if (_fullsyn)
                            {
                                DataRow dr = table.NewRow();
                                table.Rows.Add(dr);
                                Bind(dr, instance);
                            }
                            else
                            {
                                _adpater.FillBy(table, instance.ListID);

                                DataRow dr = table.Rows.Cast<DataRow>().SingleOrDefault(s => string.Compare(s.Field<string>(ItemdiscountFields.ListID), instance.ListID, true) == 0);

                                if (object.ReferenceEquals(dr, null))
                                {
                                    dr = table.NewRow();
                                    table.Rows.Add(dr);
                                }
                                Bind(dr, instance);
                            }
                            #endregion

                            #region Save Child
                            if (instance.Items.Count > 0)
                            {
                                using (ItemDal ItemDal = new ItemDal(_fullsyn, _constring, TableName.itemdiscount, instance.ListID))
                                {
                                    ItemDal.Save(instance.Items);
                                }
                            }

                            if (instance.DataExtDetails.Count > 0)
                            {
                                using (DataextdetailDal DataextdetailDal = new DataextdetailDal(_fullsyn, _constring, TableName.itemdiscount, instance.ListID))
                                {
                                    DataextdetailDal.Save(instance.DataExtDetails);
                                }
                            }
                            #endregion
                        }
                        _adpater.Update(table);
                    }
                }
            }
            catch 
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Itemdiscount itemdiscount)
        {
            try
            {
                dr[ItemdiscountFields.ListID] = itemdiscount.ListID;
                dr[ItemdiscountFields.TimeCreated] = itemdiscount.TimeCreated;
                dr[ItemdiscountFields.TimeModified] = itemdiscount.TimeModified;
                dr[ItemdiscountFields.EditSequence] = itemdiscount.EditSequence;
                dr[ItemdiscountFields.Name] = itemdiscount.Name;
                dr[ItemdiscountFields.FullName] = itemdiscount.FullName;
                dr[ItemdiscountFields.IsActive] = itemdiscount.IsActive.GetShort().GetDbType();
                dr[ItemdiscountFields.ParentRef_ListID] = itemdiscount.ParentRef_ListID;
                dr[ItemdiscountFields.ParentRef_FullName] = itemdiscount.ParentRef_FullName;
                dr[ItemdiscountFields.Sublevel] = itemdiscount.Sublevel.GetDbType();
                dr[ItemdiscountFields.ItemDesc] = itemdiscount.ItemDesc;
                dr[ItemdiscountFields.SalesTaxCodeRef_ListID] = itemdiscount.SalesTaxCodeRef_ListID;
                dr[ItemdiscountFields.SalesTaxCodeRef_FullName] = itemdiscount.SalesTaxCodeRef_FullName;
                dr[ItemdiscountFields.DiscountRate] = itemdiscount.DiscountRate;
                dr[ItemdiscountFields.DiscountRatePercent] = itemdiscount.DiscountRatePercent;
                dr[ItemdiscountFields.AccountRef_ListID] = itemdiscount.AccountRef_ListID;
                dr[ItemdiscountFields.AccountRef_FullName] = itemdiscount.AccountRef_FullName;

                dr[ItemdiscountFields.CustomField1] = itemdiscount.CustomField1;
                dr[ItemdiscountFields.CustomField2] = itemdiscount.CustomField2;
                dr[ItemdiscountFields.CustomField3] = itemdiscount.CustomField3;
                dr[ItemdiscountFields.CustomField4] = itemdiscount.CustomField4;
                dr[ItemdiscountFields.CustomField5] = itemdiscount.CustomField5;
                dr[ItemdiscountFields.CustomField6] = itemdiscount.CustomField6;
                dr[ItemdiscountFields.CustomField7] = itemdiscount.CustomField7;
                dr[ItemdiscountFields.CustomField8] = itemdiscount.CustomField8;
                dr[ItemdiscountFields.CustomField9] = itemdiscount.CustomField9;
                dr[ItemdiscountFields.CustomField10] = itemdiscount.CustomField10;
                dr[ItemdiscountFields.Status] = itemdiscount.Status;
                dr[ItemdiscountFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[ItemdiscountFields.SynDate] = DBNull.Value;
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adpater.Dispose();
                    _adpater = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion
    }
}
