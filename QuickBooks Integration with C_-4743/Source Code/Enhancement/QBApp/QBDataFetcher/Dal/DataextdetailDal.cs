//
// Class	:	EstimatelinedetailDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:48 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class DataextdetailDal : IDisposable
    {
        #region Class Level Variables
        bool _fullsyn;
        string _mastertable;
        string _idkey;
        dataextdetailTableAdapter _adapter;
        #endregion

        #region Constructors / Destructors
        public DataextdetailDal(bool fullsyn, string conString, string mastertable, string idkey)
        {
            _adapter = new dataextdetailTableAdapter();
            _adapter.Connection.ConnectionString = conString;
            _fullsyn = fullsyn;
            _idkey = idkey;
            _mastertable = mastertable;
        }
        #endregion

        #region Methods (Public)
        public void Save(List<DataExtDetail> DataExtDetails)
        {
            try
            {
                QBdb.dataextdetailDataTable table = new Dal.QBdb.dataextdetailDataTable();

                if (!_fullsyn)
                {
                    _adapter.DeleteBy(_idkey, _mastertable);
                }

                foreach (DataExtDetail instance in DataExtDetails)
                {
                    DataRow dr = table.NewRow();
                    table.Rows.Add(dr);
                    Bind(dr, instance);
                }
                _adapter.Update(table);
            }
            catch 
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, DataExtDetail dataExtDetail)
        {
            try
            {
                dr[DataExtDetailFields.DataExtName] = dataExtDetail.DataExtName;
                dr[DataExtDetailFields.DataExtType] = dataExtDetail.DataExtType;
                dr[DataExtDetailFields.DataExtValue] = dataExtDetail.DataExtValue;
                dr[DataExtDetailFields.OwnerID] = dataExtDetail.OwnerID;
                dr[DataExtDetailFields.OwnerType] = dataExtDetail.OwnerType;
                dr[DataExtDetailFields.CustomField1] = dataExtDetail.CustomField1;
                dr[DataExtDetailFields.CustomField2] = dataExtDetail.CustomField2;
                dr[DataExtDetailFields.CustomField3] = dataExtDetail.CustomField3;
                dr[DataExtDetailFields.CustomField4] = dataExtDetail.CustomField4;
                dr[DataExtDetailFields.CustomField5] = dataExtDetail.CustomField5;
                dr[DataExtDetailFields.CustomField6] = dataExtDetail.CustomField6;
                dr[DataExtDetailFields.CustomField7] = dataExtDetail.CustomField7;
                dr[DataExtDetailFields.CustomField8] = dataExtDetail.CustomField8;
                dr[DataExtDetailFields.CustomField9] = dataExtDetail.CustomField9;
                dr[DataExtDetailFields.CustomField10] = dataExtDetail.CustomField10;
                dr[DataExtDetailFields.Idkey] = dataExtDetail.Idkey;
                dr[DataExtDetailFields.GroupIDKEY] = dataExtDetail.GroupIDKEY;
                dr[DataExtDetailFields.TableName] = dataExtDetail.TableName;
                dr[DataExtDetailFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[DataExtDetailFields.SynDate] = DBNull.Value;
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adapter.Dispose();
                    _adapter = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion
    }
}
