//
// Class	:	BillingrateperitemdetailDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:43 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class BillingrateperitemdetailDal : IDisposable
    {
        #region Class Level Variables
        bool _fullsyn;
        string _mastertable;
        string _idkey;
        billingrateperitemdetailTableAdapter _adpater;
        #endregion

        #region Constructors / Destructors
        public BillingrateperitemdetailDal(bool fullsyn, string conString, string mastertable, string idkey)
        {
            _adpater = new billingrateperitemdetailTableAdapter();
            _adpater.Connection.ConnectionString = conString;
            _fullsyn = fullsyn;
            _idkey = idkey;
            _mastertable = mastertable;
        }
        #endregion

        #region Methods (Public)
        public void Save(List<Billingrateperitemdetail> Billingrateperitemdetail)
        {
            try
            {
                QBdb.billingrateperitemdetailDataTable table = new Dal.QBdb.billingrateperitemdetailDataTable();
                if (!_fullsyn)
                {
                    _adpater.DeleteBy(_idkey, _mastertable);
                }
                foreach (Billingrateperitemdetail instance in Billingrateperitemdetail)
                {                    
                    DataRow dr = table.NewRow();
                    table.Rows.Add(dr);
                    Bind(dr, instance);
                }
                _adpater.Update(table);
            }
            catch 
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Billingrateperitemdetail billingrateperitemdetail)
        {
            try
            {
                dr[BillingrateperitemdetailFields.ItemRef_ListID] = billingrateperitemdetail.ItemRef_ListID;
                dr[BillingrateperitemdetailFields.ItemRef_FullName] = billingrateperitemdetail.ItemRef_FullName;
                dr[BillingrateperitemdetailFields.CustomRate] = billingrateperitemdetail.CustomRate;
                dr[BillingrateperitemdetailFields.CustomRatePercent] = billingrateperitemdetail.CustomRatePercent;
                dr[BillingrateperitemdetailFields.CustomField1] = billingrateperitemdetail.CustomField1;
                dr[BillingrateperitemdetailFields.CustomField2] = billingrateperitemdetail.CustomField2;
                dr[BillingrateperitemdetailFields.CustomField3] = billingrateperitemdetail.CustomField3;
                dr[BillingrateperitemdetailFields.CustomField4] = billingrateperitemdetail.CustomField4;
                dr[BillingrateperitemdetailFields.CustomField5] = billingrateperitemdetail.CustomField5;
                dr[BillingrateperitemdetailFields.CustomField6] = billingrateperitemdetail.CustomField6;
                dr[BillingrateperitemdetailFields.CustomField7] = billingrateperitemdetail.CustomField7;
                dr[BillingrateperitemdetailFields.CustomField8] = billingrateperitemdetail.CustomField8;
                dr[BillingrateperitemdetailFields.CustomField9] = billingrateperitemdetail.CustomField9;
                dr[BillingrateperitemdetailFields.CustomField10] = billingrateperitemdetail.CustomField10;
                dr[BillingrateperitemdetailFields.Idkey] = billingrateperitemdetail.Idkey;
                dr[BillingrateperitemdetailFields.GroupIDKEY] = billingrateperitemdetail.GroupIDKEY;
                dr[BillingrateperitemdetailFields.TableName] = billingrateperitemdetail.TableName;
                dr[BillingrateperitemdetailFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[BillingrateperitemdetailFields.SynDate] = DBNull.Value;
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adpater.Dispose();
                    _adpater = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
