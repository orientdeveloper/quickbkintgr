//
// Class	:	ItemfixedassetDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/5/2013 10:11:50 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class ItemfixedassetDal : IDisposable, IDal
    {

        #region Class Level Variables
        bool _fullsyn;
        string _qbfile;
        DateTime? _fromsyn;
        DateTime? _tosyn;
        int _chunksize;
        itemfixedassetTableAdapter _adpater;
        #endregion

        #region Constructors / Destructors
        public ItemfixedassetDal(string qbfile, int chunksize, DateTime? fromsyn,DateTime? tosyn, bool fullsyn, string conString)
        {
            _adpater = new itemfixedassetTableAdapter();
            _adpater.Connection.ConnectionString = conString;
            _adpater.ClearBeforeFill = false;
            _fullsyn = fullsyn;
            _fromsyn = fromsyn;
            _tosyn = tosyn;
            _qbfile = qbfile;
            _chunksize = chunksize;
        }
        #endregion        
        
        #region Methods (Public)
        public void Synchronize()
        {
            try
            {
                using (ItemFixedAssetFetch fetcher = new ItemFixedAssetFetch(_qbfile, _chunksize, _fromsyn, _tosyn, Logger))
                {
                    while (fetcher.IsNextChunck)
                    {
                        QBdb.itemfixedassetDataTable table = new Dal.QBdb.itemfixedassetDataTable();
                        foreach (Itemfixedasset instance in fetcher.FetchNext())
                        {
                            if (_fullsyn)
                            {
                                DataRow dr = table.NewRow();
                                table.Rows.Add(dr);
                                Bind(dr, instance);
                            }
                            else
                            {
                                _adpater.FillBy(table, instance.ListID);

                                DataRow dr = table.Rows.Cast<DataRow>().SingleOrDefault(s => string.Compare(s.Field<string>(ItemfixedassetFields.ListID), instance.ListID, true) == 0);

                                if (object.ReferenceEquals(dr, null))
                                {
                                    dr = table.NewRow();
                                    table.Rows.Add(dr);
                                }
                                Bind(dr, instance);
                            }
                        }
                        _adpater.Update(table);
                    }
                }
            }
            catch (Exception ex)
            {
                //1264logLogger.LogError(ex.Message, ex);
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Itemfixedasset itemfixedasset)
        {
            dr[ItemfixedassetFields.ListID] = itemfixedasset.ListID;
            dr[ItemfixedassetFields.TimeCreated] = itemfixedasset.TimeCreated;
            dr[ItemfixedassetFields.TimeModified] = itemfixedasset.TimeModified;
            dr[ItemfixedassetFields.EditSequence] = itemfixedasset.EditSequence;
            dr[ItemfixedassetFields.Name] = itemfixedasset.Name;
            dr[ItemfixedassetFields.IsActive] = itemfixedasset.IsActive.GetShort().GetDbType();
            dr[ItemfixedassetFields.AcquiredAs] = itemfixedasset.AcquiredAs;
            dr[ItemfixedassetFields.PurchaseDesc] = itemfixedasset.PurchaseDesc;
            dr[ItemfixedassetFields.PurchaseDate] = itemfixedasset.PurchaseDate.GetDbType();
            dr[ItemfixedassetFields.PurchaseCost] = itemfixedasset.PurchaseCost;
            dr[ItemfixedassetFields.VendorOrPayeeName] = itemfixedasset.VendorOrPayeeName;
            dr[ItemfixedassetFields.AssetAccountRef_ListID] = itemfixedasset.AssetAccountRef_ListID;
            dr[ItemfixedassetFields.AssetAccountRef_FullName] = itemfixedasset.AssetAccountRef_FullName;
            dr[ItemfixedassetFields.AssetDesc] = itemfixedasset.AssetDesc;
            dr[ItemfixedassetFields.Location] = itemfixedasset.Location;
            dr[ItemfixedassetFields.PONumber] = itemfixedasset.PONumber;
            dr[ItemfixedassetFields.SerialNumber] = itemfixedasset.SerialNumber;
            dr[ItemfixedassetFields.WarrantyExpDate] = itemfixedasset.WarrantyExpDate.GetDbType();
            dr[ItemfixedassetFields.Notes] = itemfixedasset.Notes;
            dr[ItemfixedassetFields.AssetNumber] = itemfixedasset.AssetNumber;
            dr[ItemfixedassetFields.CostBasis] = itemfixedasset.CostBasis.GetDbType();
            dr[ItemfixedassetFields.YearEndAccumulatedDepreciation] = itemfixedasset.YearEndAccumulatedDepreciation.GetDbType();
            dr[ItemfixedassetFields.YearEndBookValue] = itemfixedasset.YearEndBookValue.GetDbType();
            dr[ItemfixedassetFields.CustomField1] = itemfixedasset.CustomField1;
            dr[ItemfixedassetFields.CustomField2] = itemfixedasset.CustomField2;
            dr[ItemfixedassetFields.CustomField3] = itemfixedasset.CustomField3;
            dr[ItemfixedassetFields.CustomField4] = itemfixedasset.CustomField4;
            dr[ItemfixedassetFields.CustomField5] = itemfixedasset.CustomField5;
            dr[ItemfixedassetFields.CustomField6] = itemfixedasset.CustomField6;
            dr[ItemfixedassetFields.CustomField7] = itemfixedasset.CustomField7;
            dr[ItemfixedassetFields.CustomField8] = itemfixedasset.CustomField8;
            dr[ItemfixedassetFields.CustomField9] = itemfixedasset.CustomField9;
            dr[ItemfixedassetFields.CustomField10] = itemfixedasset.CustomField10;
            dr[ItemfixedassetFields.Status] = itemfixedasset.Status;
            dr[ItemfixedassetFields.IsSyn] = (short)SynStatus.Synchronize;
            dr[ItemfixedassetFields.SynDate] = DBNull.Value;
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adpater.Dispose();
                    _adpater = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion

    }
}
