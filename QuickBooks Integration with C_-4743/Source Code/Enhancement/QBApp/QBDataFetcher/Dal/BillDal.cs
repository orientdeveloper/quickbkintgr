//
// Class	:	BillDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:43 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class BillDal : IDisposable, IDal
    {
        #region Class Level Variables
        bool _fullsyn;
        string _qbfile;
        DateTime? _fromsyn;
        DateTime? _tosyn;
        int _chunksize;
        string _constring;
        billTableAdapter _adpater;
        #endregion

        #region Constructors / Destructors
        public BillDal(string qbfile, int chunksize, DateTime? fromsyn, DateTime? tosyn, bool fullsyn, string constring)
        {
            _adpater = new billTableAdapter();
            _adpater.Connection.ConnectionString = constring;
            _adpater.ClearBeforeFill = false;
            _fullsyn = fullsyn;
            _fromsyn = fromsyn;
            _tosyn = tosyn;
            _qbfile = qbfile;
            _chunksize = chunksize;
            _constring = constring;
        }
        #endregion

        #region Methods (Public)
        /// <summary>
        /// This method is used for calling the fetcher class for getting the data and storing it in acccess database.
        /// </summary>
        public void Synchronize()
        {
            try
            {
                using (BillFetch fetcher = new BillFetch(_qbfile, _chunksize, _fromsyn,_tosyn, Logger))
                {
                    while (fetcher.IsNextChunck)
                    {
                        QBdb.billDataTable table = new Dal.QBdb.billDataTable();
                        foreach (Bill instance in fetcher.FetchNext())
                        {
                            #region Save Master
                            if (_fullsyn)
                            {
                                DataRow dr = table.NewRow();
                                table.Rows.Add(dr);
                                Bind(dr, instance);
                            }
                            else
                            {
                                _adpater.FillBy(table, instance.TxnID);

                                DataRow dr = table.Rows.Cast<DataRow>().SingleOrDefault(s => string.Compare(s.Field<string>(BillFields.TxnID), instance.TxnID, true) == 0);

                                if (object.ReferenceEquals(dr, null))
                                {
                                    dr = table.NewRow();
                                    table.Rows.Add(dr);
                                }
                                Bind(dr, instance);
                            }
                            #endregion

                            #region Save Child Table
                            if (instance.Txnexpenselinedetails.Count != 0)
                            {
                                using (TxnexpenselinedetailDal TxnexpenselinedetailDal = new TxnexpenselinedetailDal(_fullsyn, _constring, TableName.bill, instance.TxnID))
                                {
                                    TxnexpenselinedetailDal.Save(instance.Txnexpenselinedetails);
                                }
                            }

                            if (instance.Txnitemlinedetails.Count > 0)
                            {
                                using (TxnitemlinedetailDal txnitemlinedetailDal = new TxnitemlinedetailDal(_fullsyn, _constring, TableName.bill, instance.TxnID))
                                {
                                    txnitemlinedetailDal.Save(instance.Txnitemlinedetails);
                                }
                            }

                            if (instance.Txnitemgrouplinedetails.Count > 0)
                            {
                                using (TxnitemgrouplinedetailDal TxnitemgrouplinedetailDal = new TxnitemgrouplinedetailDal(_fullsyn, _constring, TableName.bill, instance.TxnID))
                                {
                                    TxnitemgrouplinedetailDal.Save(instance.Txnitemgrouplinedetails);
                                }
                            }

                            if (instance.Linkedtxndetails.Count > 0)
                            {
                                using (LinkedtxndetailDal LinkedtxndetailDal = new LinkedtxndetailDal(_fullsyn, _constring, TableName.bill, instance.TxnID))
                                {
                                    LinkedtxndetailDal.Save(instance.Linkedtxndetails);
                                }
                            }
                            #endregion
                        }
                        _adpater.Update(table);
                    }
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Bill bill)
        {
            try
            {
                dr[BillFields.AmountDue] = bill.AmountDue.GetDbType();
                dr[BillFields.AmountDueInHomeCurrency] = bill.AmountDueInHomeCurrency.GetDbType();
                dr[BillFields.APAccountRef_FullName] = bill.APAccountRef_FullName;
                dr[BillFields.APAccountRef_ListID] = bill.APAccountRef_ListID;
                dr[BillFields.CurrencyRef_FullName] = bill.CurrencyRef_FullName;
                dr[BillFields.CurrencyRef_ListID] = bill.CurrencyRef_ListID;
                dr[BillFields.CustomField1] = bill.CustomField1;
                dr[BillFields.CustomField2] = bill.CustomField2;
                dr[BillFields.CustomField3] = bill.CustomField3;
                dr[BillFields.CustomField4] = bill.CustomField4;
                dr[BillFields.CustomField5] = bill.CustomField5;
                dr[BillFields.CustomField6] = bill.CustomField6;
                dr[BillFields.CustomField7] = bill.CustomField7;
                dr[BillFields.CustomField8] = bill.CustomField8;
                dr[BillFields.CustomField9] = bill.CustomField9;
                dr[BillFields.CustomField10] = bill.CustomField10;
                dr[BillFields.DueDate] = bill.DueDate.GetDbType();
                dr[BillFields.EditSequence] = bill.EditSequence;
                dr[BillFields.ExchangeRate] = bill.ExchangeRate.GetDbType();
                dr[BillFields.IsPaid] = bill.IsPaid.GetShort().GetDbType();
                dr[BillFields.IsTaxIncluded] = bill.IsTaxIncluded.GetShort().GetDbType();
                dr[BillFields.Memo] = bill.Memo;
                dr[BillFields.OpenAmount] = bill.OpenAmount.GetDbType();
                dr[BillFields.RefNumber] = bill.RefNumber;
                dr[BillFields.SalesTaxCodeRef_FullName] = bill.SalesTaxCodeRef_FullName;
                dr[BillFields.SalesTaxCodeRef_ListID] = bill.SalesTaxCodeRef_ListID;
                dr[BillFields.Status] = bill.Status;
                dr[BillFields.TermsRef_FullName] = bill.TermsRef_FullName;
                dr[BillFields.TermsRef_ListID] = bill.TermsRef_ListID;
                dr[BillFields.TimeCreated] = bill.TimeCreated;
                dr[BillFields.TimeModified] = bill.TimeModified;
                dr[BillFields.TxnDate] = bill.TxnDate.GetDbType();
                dr[BillFields.TxnID] = bill.TxnID;
                dr[BillFields.TxnNumber] = bill.TxnNumber.GetDbType();
                dr[BillFields.VendorRef_FullName] = bill.VendorRef_FullName;
                dr[BillFields.VendorRef_ListID] = bill.VendorRef_ListID;
                dr[BillFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[BillFields.SynDate] = DBNull.Value;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message, ex);
                throw ex;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adpater.Dispose();
                    _adpater = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion
    }
}
