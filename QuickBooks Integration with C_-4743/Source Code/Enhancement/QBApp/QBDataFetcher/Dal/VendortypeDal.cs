//
// Class	:	VendortypeDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	27/05/2013 2:11:59 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class VendortypeDal : IDisposable, IDal
    {
        #region Class Level Variables
        bool _fullsyn;
        string _qbfile;
        DateTime? _fromsyn;
        DateTime? _tosyn;
        int _chunksize;
        vendortypeTableAdapter _adpater;
        #endregion

        #region Constructors / Destructors
        public VendortypeDal(string qbfile, int chunksize, DateTime? fromsyn, DateTime? tosyn, bool fullsyn, string conString)
        {
            _adpater = new vendortypeTableAdapter();
            _adpater.Connection.ConnectionString = conString;
            _adpater.ClearBeforeFill = false;
            _fullsyn = fullsyn;
            _fromsyn = fromsyn;
            _tosyn = tosyn;
            _qbfile = qbfile;
            _chunksize = chunksize;
        }
        #endregion

        #region Methods (Public)
        public void Synchronize()
        {
            try
            {
                using (VendorTypeFetch fetcher = new VendorTypeFetch(_qbfile, _chunksize, _fromsyn, _tosyn, Logger))
                {
                    while (fetcher.IsNextChunck)
                    {

                        QBdb.vendortypeDataTable table = new Dal.QBdb.vendortypeDataTable();

                        foreach (Vendortype instance in fetcher.FetchNext())
                        {
                            if (_fullsyn)
                            {
                                DataRow dr = table.NewRow();
                                table.Rows.Add(dr);
                                Bind(dr, instance);
                            }
                            else
                            {
                                _adpater.FillBy(table, instance.ListID);

                                DataRow dr = table.Rows.Cast<DataRow>().SingleOrDefault(s => string.Compare(s.Field<string>(VendortypeFields.ListID), instance.ListID, true) == 0);

                                if (object.ReferenceEquals(dr, null))
                                {
                                    dr = table.NewRow();
                                    table.Rows.Add(dr);
                                }
                                Bind(dr, instance);
                            }
                        }
                        _adpater.Update(table);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message, ex);
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Vendortype vendortype)
        {
            try
            {
                dr[VendortypeFields.ListID] = vendortype.ListID;
                dr[VendortypeFields.TimeCreated] = vendortype.TimeCreated;
                dr[VendortypeFields.TimeModified] = vendortype.TimeModified;
                dr[VendortypeFields.EditSequence] = vendortype.EditSequence;
                dr[VendortypeFields.Name] = vendortype.Name;
                dr[VendortypeFields.FullName] = vendortype.FullName;
                dr[VendortypeFields.IsActive] = vendortype.IsActive.GetShort().GetDbType();
                dr[VendortypeFields.ParentRef_ListID] = vendortype.ParentRef_ListID;
                dr[VendortypeFields.ParentRef_FullName] = vendortype.ParentRef_FullName;
                dr[VendortypeFields.Sublevel] = vendortype.Sublevel.GetDbType();
                dr[VendortypeFields.Status] = vendortype.Status;
                dr[VendortypeFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[VendortypeFields.SynDate] = DBNull.Value;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message, ex);
                throw;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adpater.Dispose();
                    _adpater = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion
    }
}
