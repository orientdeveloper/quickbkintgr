//
// Class	:	TxnexpenselinedetailDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/8/2013 10:11:54 AM
//


using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class TxnexpenselinedetailDal : IDisposable
    {
        #region Class Level Variables
        bool _fullsyn;
        string _mastertable;
        txnexpenselinedetailTableAdapter _adapter;
        string _idkey;
        #endregion

        #region Constructors / Destructors
        public TxnexpenselinedetailDal(bool fullsyn, string conString, string mastertable, string idkey)
        {
            _adapter = new txnexpenselinedetailTableAdapter();
            _adapter.Connection.ConnectionString = conString;
            _fullsyn = fullsyn;
            _idkey = idkey;
            _mastertable = mastertable;
        }
        #endregion

        #region Methods (Public)
        public void Save(List<Txnexpenselinedetail> Txnexpenselinedetails)
        {
            try
            {
                QBdb.txnexpenselinedetailDataTable table = new Dal.QBdb.txnexpenselinedetailDataTable();

                if (!_fullsyn)
                {
                    _adapter.DeleteBy(_idkey, _mastertable);
                }

                foreach (Txnexpenselinedetail instance in Txnexpenselinedetails)
                {                    
                    DataRow dr = table.NewRow();
                    table.Rows.Add(dr);
                    Bind(dr, instance);
                }
                _adapter.Update(table);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Txnexpenselinedetail txnexpenselinedetail)
        {
            try
            {
                dr[TxnexpenselinedetailFields.TxnLineID] = txnexpenselinedetail.TxnLineID;
                dr[TxnexpenselinedetailFields.AccountRef_ListID] = txnexpenselinedetail.AccountRef_ListID;
                dr[TxnexpenselinedetailFields.AccountRef_FullName] = txnexpenselinedetail.AccountRef_FullName;
                dr[TxnexpenselinedetailFields.Amount] = txnexpenselinedetail.Amount.GetDbType();
                dr[TxnexpenselinedetailFields.Memo] = txnexpenselinedetail.Memo;
                dr[TxnexpenselinedetailFields.CustomerRef_ListID] = txnexpenselinedetail.CustomerRef_ListID;
                dr[TxnexpenselinedetailFields.CustomerRef_FullName] = txnexpenselinedetail.CustomerRef_FullName;
                dr[TxnexpenselinedetailFields.ClassRef_ListID] = txnexpenselinedetail.ClassRef_ListID;
                dr[TxnexpenselinedetailFields.ClassRef_FullName] = txnexpenselinedetail.ClassRef_FullName;
                dr[TxnexpenselinedetailFields.SalesTaxCodeRef_ListID] = txnexpenselinedetail.SalesTaxCodeRef_ListID;
                dr[TxnexpenselinedetailFields.SalesTaxCodeRef_FullName] = txnexpenselinedetail.SalesTaxCodeRef_FullName;
                dr[TxnexpenselinedetailFields.BillableStatus] = txnexpenselinedetail.BillableStatus;
                dr[TxnexpenselinedetailFields.TxnLineID] = txnexpenselinedetail.TxnLineID;
                dr[TxnexpenselinedetailFields.CustomField1] = txnexpenselinedetail.CustomField1;
                dr[TxnexpenselinedetailFields.CustomField2] = txnexpenselinedetail.CustomField2;
                dr[TxnexpenselinedetailFields.CustomField3] = txnexpenselinedetail.CustomField3;
                dr[TxnexpenselinedetailFields.CustomField4] = txnexpenselinedetail.CustomField4;
                dr[TxnexpenselinedetailFields.CustomField5] = txnexpenselinedetail.CustomField5;
                dr[TxnexpenselinedetailFields.CustomField6] = txnexpenselinedetail.CustomField6;
                dr[TxnexpenselinedetailFields.CustomField7] = txnexpenselinedetail.CustomField7;
                dr[TxnexpenselinedetailFields.CustomField8] = txnexpenselinedetail.CustomField8;
                dr[TxnexpenselinedetailFields.CustomField9] = txnexpenselinedetail.CustomField9;
                dr[TxnexpenselinedetailFields.CustomField10] = txnexpenselinedetail.CustomField10;
                dr[TxnexpenselinedetailFields.Idkey] = txnexpenselinedetail.Idkey;
                dr[TxnexpenselinedetailFields.GroupIDKEY] = txnexpenselinedetail.GroupIDKEY;
                dr[TxnexpenselinedetailFields.TableName] = txnexpenselinedetail.TableName;
                dr[TxnexpenselinedetailFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[TxnexpenselinedetailFields.SynDate] = DBNull.Value;
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adapter.Dispose();
                    _adapter = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
