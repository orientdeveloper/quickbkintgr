//
// Class	:	CustomertypeDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class CustomertypeDal : IDisposable, IDal
    {
        #region Class Level Variables
        /// <summary>
        /// Global variables
        /// </summary>
        bool _fullsyn;
        string _qbfile;
        DateTime? _fromdate;
        DateTime? _todate;
        int _chunksize;
        customertypeTableAdapter _adpater;
        #endregion

        #region Constructors / Destructors
        /// <summary>
        /// Constructure for the class
        /// </summary>
        /// <param name="qbfile"></param>
        /// <param name="chunksize"></param>
        /// <param name="fromdate"></param>
        /// <param name="todate"></param>
        /// <param name="fullsyn"></param>
        /// <param name="conString"></param>
        public CustomertypeDal(string qbfile, int chunksize, DateTime? fromdate, DateTime? todate, bool fullsyn, string conString)
        {
            _adpater = new customertypeTableAdapter();
            _adpater.Connection.ConnectionString = conString;
            _adpater.ClearBeforeFill = false;
            _fullsyn = fullsyn;
            _fromdate = fromdate;
            _todate = todate;
            _qbfile = qbfile;
            _chunksize = chunksize;
        }
        #endregion

        #region Methods (Public)
        /// <summary>
        /// Synchronize method for the class CustomertypeDal
        /// </summary>
        public void Synchronize()
        {
            try
            {
                using (CustomerTypeFetch fetcher = new CustomerTypeFetch(_qbfile, _chunksize, _fromdate, _todate, Logger))
                {
                    while (fetcher.IsNextChunck)
                    {

                        QBdb.customertypeDataTable table = new Dal.QBdb.customertypeDataTable();

                        foreach (Customertype instance in fetcher.FetchNext())
                        {
                            if (_fullsyn)
                            {
                                DataRow dr = table.NewRow();
                                table.Rows.Add(dr);
                                Bind(dr, instance);
                            }
                            else
                            {
                                _adpater.FillBy(table, instance.ListID);

                                DataRow dr = table.Rows.Cast<DataRow>().SingleOrDefault(s => string.Compare(s.Field<string>(CustomertypeFields.ListID), instance.ListID, true) == 0);

                                if (object.ReferenceEquals(dr, null))
                                {
                                    dr = table.NewRow();
                                    table.Rows.Add(dr);
                                }
                                Bind(dr, instance);
                            }
                        }
                        _adpater.Update(table);
                    }
                }
            }
            catch (Exception ex)
            {
                //1264logLogger.LogError(ex.Message, ex);
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Customertype Customertype)
        {
            try
            {
                dr[CustomertypeFields.EditSequence] = Customertype.EditSequence;
                dr[CustomertypeFields.FullName] = Customertype.FullName;
                dr[CustomertypeFields.IsActive] = Customertype.IsActive.GetShort().GetDbType();
                dr[CustomertypeFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[CustomertypeFields.ListID] = Customertype.ListID;
                dr[CustomertypeFields.Name] = Customertype.Name;
                dr[CustomertypeFields.ParentRef_FullName] = Customertype.ParentRef_FullName;
                dr[CustomertypeFields.ParentRef_ListID] = Customertype.ParentRef_ListID;
                dr[CustomertypeFields.Status] = Customertype.Status;
                dr[CustomertypeFields.Sublevel] = Customertype.Sublevel.GetDbType();
                dr[CustomertypeFields.SynDate] = DBNull.Value;
                dr[CustomertypeFields.TimeCreated] = Customertype.TimeCreated;
                dr[CustomertypeFields.TimeModified] = Customertype.TimeModified;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message, ex);
                throw ex;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        /// <summary>
        /// Dispose method for the class
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adpater.Dispose();
                    _adpater = null;
                }
                disposed = true;
            }
        }

        /// <summary>
        /// Dispose method for the class
        /// </summary>
        /// <param name="disposing"></param>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        #region Implementation IDal
        /// <summary>
        /// Logger property for the class
        /// </summary>
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion
    }
}
