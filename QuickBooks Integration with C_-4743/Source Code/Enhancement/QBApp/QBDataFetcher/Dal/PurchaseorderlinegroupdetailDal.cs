//
// Class	:	PurchaseorderlinegroupdetailDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:54 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class PurchaseorderlinegroupdetailDal : IDisposable
    {
        #region Class Level Variables
        bool _fullsyn;
        string _mastertable;
        string _idkey;
        purchaseorderlinegroupdetailTableAdapter _adapter;
        #endregion

        #region Constructors / Destructors
        public PurchaseorderlinegroupdetailDal(bool fullsyn, string conString, string mastertable, string idkey)
        {
            _adapter = new purchaseorderlinegroupdetailTableAdapter();
            _adapter.Connection.ConnectionString = conString;
            _fullsyn = fullsyn;
            _idkey = idkey;
            _mastertable = mastertable;
        }
        #endregion

        #region Methods (Public)
        public void Save(List<Purchaseorderlinegroupdetail> Purchaseorderlinegroupdetails)
        {
            try
            {
                QBdb.purchaseorderlinegroupdetailDataTable table = new Dal.QBdb.purchaseorderlinegroupdetailDataTable();

                if (!_fullsyn)
                {
                    _adapter.DeleteBy(_idkey, _mastertable);
                }

                foreach (Purchaseorderlinegroupdetail instance in Purchaseorderlinegroupdetails)
                {                    
                    DataRow dr = table.NewRow();
                    table.Rows.Add(dr);
                    Bind(dr, instance);
                }
                _adapter.Update(table);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Purchaseorderlinegroupdetail purchaseorderlinegroupdetail)
        {
            try
            {
                dr[PurchaseorderlinegroupdetailFields.TxnLineID] = purchaseorderlinegroupdetail.TxnLineID;
                dr[PurchaseorderlinegroupdetailFields.ItemGroupRef_ListID] = purchaseorderlinegroupdetail.ItemGroupRef_ListID;
                dr[PurchaseorderlinegroupdetailFields.ItemGroupRef_FullName] = purchaseorderlinegroupdetail.ItemGroupRef_FullName;
                dr[PurchaseorderlinegroupdetailFields.Desc] = purchaseorderlinegroupdetail.Desc;
                dr[PurchaseorderlinegroupdetailFields.Quantity] = purchaseorderlinegroupdetail.Quantity;
                dr[PurchaseorderlinegroupdetailFields.IsPrintItemsInGroup] = purchaseorderlinegroupdetail.IsPrintItemsInGroup.GetShort().GetDbType();
                dr[PurchaseorderlinegroupdetailFields.TotalAmount] = purchaseorderlinegroupdetail.TotalAmount.GetDbType();
                dr[PurchaseorderlinegroupdetailFields.ServiceDate] = purchaseorderlinegroupdetail.ServiceDate.GetDbType();
                dr[PurchaseorderlinegroupdetailFields.CustomField1] = purchaseorderlinegroupdetail.CustomField1;
                dr[PurchaseorderlinegroupdetailFields.CustomField2] = purchaseorderlinegroupdetail.CustomField2;
                dr[PurchaseorderlinegroupdetailFields.CustomField3] = purchaseorderlinegroupdetail.CustomField3;
                dr[PurchaseorderlinegroupdetailFields.CustomField4] = purchaseorderlinegroupdetail.CustomField4;
                dr[PurchaseorderlinegroupdetailFields.CustomField5] = purchaseorderlinegroupdetail.CustomField5;
                dr[PurchaseorderlinegroupdetailFields.CustomField6] = purchaseorderlinegroupdetail.CustomField6;
                dr[PurchaseorderlinegroupdetailFields.CustomField7] = purchaseorderlinegroupdetail.CustomField7;
                dr[PurchaseorderlinegroupdetailFields.CustomField8] = purchaseorderlinegroupdetail.CustomField8;
                dr[PurchaseorderlinegroupdetailFields.CustomField9] = purchaseorderlinegroupdetail.CustomField9;
                dr[PurchaseorderlinegroupdetailFields.CustomField10] = purchaseorderlinegroupdetail.CustomField10;
                dr[PurchaseorderlinegroupdetailFields.Idkey] = purchaseorderlinegroupdetail.Idkey;
                dr[PurchaseorderlinegroupdetailFields.GroupIDKEY] = purchaseorderlinegroupdetail.GroupIDKEY;
                dr[PurchaseorderlinegroupdetailFields.TableName] = purchaseorderlinegroupdetail.TableName;
                dr[PurchaseorderlinegroupdetailFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[PurchaseorderlinegroupdetailFields.SynDate] = DBNull.Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adapter.Dispose();
                    _adapter = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
