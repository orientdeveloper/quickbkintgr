//
// Class	:	SalesorpurchasedetailDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/8/2013 10:11:55 AM
//
using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class SalesorpurchasedetailDal : IDisposable
    {
        #region Class Level Variables
        bool _fullsyn;
        string _mastertable;
        string _idkey;
        salesorpurchasedetailTableAdapter _adapter;
        #endregion

        #region Constructors / Destructors
        public SalesorpurchasedetailDal(bool fullsyn, string conString, string mastertable, string idkey)
        {
            _adapter = new salesorpurchasedetailTableAdapter();
            _adapter.Connection.ConnectionString = conString;
            _fullsyn = fullsyn;
            _idkey = idkey;
            _mastertable = mastertable;
        }
        #endregion

        #region Methods (Public)
        public void Save(List<Salesorpurchasedetail> Salesorpurchasedetails)
        {
            try
            {
                QBdb.salesorpurchasedetailDataTable table = new Dal.QBdb.salesorpurchasedetailDataTable();

                if (!_fullsyn)
                {
                    _adapter.DeleteBy(_idkey, _mastertable);
                }

                foreach (Salesorpurchasedetail instance in Salesorpurchasedetails)
                {                    
                    DataRow dr = table.NewRow();
                    table.Rows.Add(dr);
                    Bind(dr, instance);
                }
                _adapter.Update(table);
            }
            catch 
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Salesorpurchasedetail salesorpurchasedetail)
        {
            try
            {

                dr[SalesorpurchasedetailFields.Desc] = salesorpurchasedetail.Desc;
                dr[SalesorpurchasedetailFields.Price] = salesorpurchasedetail.Price;
                dr[SalesorpurchasedetailFields.PricePercent] = salesorpurchasedetail.PricePercent;
                dr[SalesorpurchasedetailFields.AccountRef_ListID] = salesorpurchasedetail.AccountRef_ListID;
                dr[SalesorpurchasedetailFields.AccountRef_FullName] = salesorpurchasedetail.AccountRef_FullName;

                dr[SalesorpurchasedetailFields.CustomField1] = salesorpurchasedetail.CustomField1;
                dr[SalesorpurchasedetailFields.CustomField2] = salesorpurchasedetail.CustomField2;
                dr[SalesorpurchasedetailFields.CustomField3] = salesorpurchasedetail.CustomField3;
                dr[SalesorpurchasedetailFields.CustomField4] = salesorpurchasedetail.CustomField4;
                dr[SalesorpurchasedetailFields.CustomField5] = salesorpurchasedetail.CustomField5;
                dr[SalesorpurchasedetailFields.CustomField6] = salesorpurchasedetail.CustomField6;
                dr[SalesorpurchasedetailFields.CustomField7] = salesorpurchasedetail.CustomField7;
                dr[SalesorpurchasedetailFields.CustomField8] = salesorpurchasedetail.CustomField8;
                dr[SalesorpurchasedetailFields.CustomField9] = salesorpurchasedetail.CustomField9;
                dr[SalesorpurchasedetailFields.CustomField10] = salesorpurchasedetail.CustomField10;
                dr[SalesorpurchasedetailFields.Idkey] = salesorpurchasedetail.Idkey;
                dr[SalesorpurchasedetailFields.GroupIDKEY] = salesorpurchasedetail.GroupIDKEY;

                dr[SalesorpurchasedetailFields.TableName] = salesorpurchasedetail.TableName;
                dr[SalesorpurchasedetailFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[SalesorpurchasedetailFields.SynDate] = DBNull.Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adapter.Dispose();
                    _adapter = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion
    }
}
