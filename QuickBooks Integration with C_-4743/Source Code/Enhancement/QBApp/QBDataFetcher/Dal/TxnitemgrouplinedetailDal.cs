//
// Class	:	TxnitemlinedetailDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:58 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class TxnitemgrouplinedetailDal : IDisposable
    {
        #region Class Level Variables
        bool _fullsyn;
        string _mastertable;
        txnitemgrouplinedetailTableAdapter _adapter;
        string _idkey;
        #endregion

        #region Constructors / Destructors
        public TxnitemgrouplinedetailDal(bool fullsyn, string conString, string mastertable, string idkey)
        {
            _adapter = new txnitemgrouplinedetailTableAdapter();
            _adapter.Connection.ConnectionString = conString;
            _fullsyn = fullsyn;
            _idkey = idkey;
            _mastertable = mastertable;
        }
        #endregion

        #region Methods (Public)
        public void Save(List<Txnitemgrouplinedetail> Txnitemgrouplinedetails)
        {
            try
            {
                QBdb.txnitemgrouplinedetailDataTable table = new Dal.QBdb.txnitemgrouplinedetailDataTable();

                if (!_fullsyn)
                {
                    _adapter.DeleteBy(_idkey, _mastertable);
                }

                foreach (Txnitemgrouplinedetail instance in Txnitemgrouplinedetails)
                {                   
                    DataRow dr = table.NewRow();
                    table.Rows.Add(dr);
                    Bind(dr, instance);
                }
                _adapter.Update(table);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Txnitemgrouplinedetail txnitemgrouplinedetail)
        {
            try
            {
                dr[TxnitemgrouplinedetailFields.TxnLineID] = txnitemgrouplinedetail.TxnLineID;
                dr[TxnitemgrouplinedetailFields.ItemGroupRef_ListID] = txnitemgrouplinedetail.ItemGroupRef_ListID;
                dr[TxnitemgrouplinedetailFields.ItemGroupRef_FullName] = txnitemgrouplinedetail.ItemGroupRef_FullName;
                dr[TxnitemgrouplinedetailFields.Desc] = txnitemgrouplinedetail.Desc;
                dr[TxnitemgrouplinedetailFields.Quantity] = txnitemgrouplinedetail.Quantity;
                dr[TxnitemgrouplinedetailFields.TotalAmount] = txnitemgrouplinedetail.TotalAmount.GetDbType();
                dr[TxnitemgrouplinedetailFields.CustomField1] = txnitemgrouplinedetail.CustomField1;
                dr[TxnitemgrouplinedetailFields.CustomField2] = txnitemgrouplinedetail.CustomField2;
                dr[TxnitemgrouplinedetailFields.CustomField3] = txnitemgrouplinedetail.CustomField3;
                dr[TxnitemgrouplinedetailFields.CustomField4] = txnitemgrouplinedetail.CustomField4;
                dr[TxnitemgrouplinedetailFields.CustomField5] = txnitemgrouplinedetail.CustomField5;
                dr[TxnitemgrouplinedetailFields.CustomField6] = txnitemgrouplinedetail.CustomField6;
                dr[TxnitemgrouplinedetailFields.CustomField7] = txnitemgrouplinedetail.CustomField7;
                dr[TxnitemgrouplinedetailFields.CustomField8] = txnitemgrouplinedetail.CustomField8;
                dr[TxnitemgrouplinedetailFields.CustomField9] = txnitemgrouplinedetail.CustomField9;
                dr[TxnitemgrouplinedetailFields.CustomField10] = txnitemgrouplinedetail.CustomField10;
                dr[TxnitemgrouplinedetailFields.Idkey] = txnitemgrouplinedetail.Idkey;
                dr[TxnitemgrouplinedetailFields.GroupIDKEY] = txnitemgrouplinedetail.GroupIDKEY;
                dr[TxnitemgrouplinedetailFields.TableName] = txnitemgrouplinedetail.TableName;
                dr[TxnitemgrouplinedetailFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[TxnitemgrouplinedetailFields.SynDate] = DBNull.Value;
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adapter.Dispose();
                    _adapter = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
