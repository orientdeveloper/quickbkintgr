//
// Class	:	EstimatelinedetailDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:48 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class EstimatelinedetailDal : IDisposable
    {
        #region Class Level Variables
        bool _fullsyn;
        string _mastertable;
        string _idkey;
        estimatelinedetailTableAdapter _adapter;
        #endregion

        #region Constructors / Destructors
        public EstimatelinedetailDal(bool fullsyn, string conString, string mastertable, string idkey)
        {
            _adapter = new estimatelinedetailTableAdapter();
            _adapter.Connection.ConnectionString = conString;
            _fullsyn = fullsyn;
            _idkey = idkey;
            _mastertable = mastertable;
        }
        #endregion

        #region Methods (Public)
        public void Save(List<Estimatelinedetail> Estimatelinedetails)
        {
            try
            {
                QBdb.estimatelinedetailDataTable table = new Dal.QBdb.estimatelinedetailDataTable();

                if (!_fullsyn)
                {
                    _adapter.DeleteBy(_idkey, _mastertable);
                }

                foreach (Estimatelinedetail instance in Estimatelinedetails)
                {
                    DataRow dr = table.NewRow();
                    table.Rows.Add(dr);
                    Bind(dr, instance);
                }
                _adapter.Update(table);
            }
            catch 
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Estimatelinedetail estimatelinedetail)
        {
            try
            {
                dr[EstimatelinedetailFields.TxnLineID] = estimatelinedetail.TxnLineID;
                dr[EstimatelinedetailFields.ItemRef_ListID] = estimatelinedetail.ItemRef_ListID;
                dr[EstimatelinedetailFields.ItemRef_FullName] = estimatelinedetail.ItemRef_FullName;
                dr[EstimatelinedetailFields.Desc] = estimatelinedetail.Desc;
                dr[EstimatelinedetailFields.Quantity] = estimatelinedetail.Quantity;
                dr[EstimatelinedetailFields.UnitOfMeasure] = estimatelinedetail.UnitOfMeasure;
                dr[EstimatelinedetailFields.OverrideUOMSetRef_ListID] = estimatelinedetail.OverrideUOMSetRef_ListID;
                dr[EstimatelinedetailFields.OverrideUOMSetRef_FullName] = estimatelinedetail.OverrideUOMSetRef_FullName;
                dr[EstimatelinedetailFields.Rate] = estimatelinedetail.Rate;
                dr[EstimatelinedetailFields.RatePercent] = estimatelinedetail.RatePercent;
                dr[EstimatelinedetailFields.ClassRef_ListID] = estimatelinedetail.ClassRef_ListID;
                dr[EstimatelinedetailFields.ClassRef_FullName] = estimatelinedetail.ClassRef_FullName;
                dr[EstimatelinedetailFields.Amount] = estimatelinedetail.Amount.GetDbType();
                dr[EstimatelinedetailFields.InventorySiteRef_ListID] = estimatelinedetail.InventorySiteRef_ListID;
                dr[EstimatelinedetailFields.InventorySiteRef_FullName] = estimatelinedetail.InventorySiteRef_FullName;
                dr[EstimatelinedetailFields.SalesTaxCodeRef_ListID] = estimatelinedetail.SalesTaxCodeRef_ListID;
                dr[EstimatelinedetailFields.SalesTaxCodeRef_FullName] = estimatelinedetail.SalesTaxCodeRef_FullName;
                dr[EstimatelinedetailFields.MarkupRate] = estimatelinedetail.MarkupRate;
                dr[EstimatelinedetailFields.MarkupRatePercent] = estimatelinedetail.MarkupRatePercent;
                dr[EstimatelinedetailFields.Other1] = estimatelinedetail.Other1;
                dr[EstimatelinedetailFields.Other2] = estimatelinedetail.Other2;
                dr[EstimatelinedetailFields.CustomField1] = estimatelinedetail.CustomField1;
                dr[EstimatelinedetailFields.CustomField2] = estimatelinedetail.CustomField2;
                dr[EstimatelinedetailFields.CustomField3] = estimatelinedetail.CustomField3;
                dr[EstimatelinedetailFields.CustomField4] = estimatelinedetail.CustomField4;
                dr[EstimatelinedetailFields.CustomField5] = estimatelinedetail.CustomField5;
                dr[EstimatelinedetailFields.CustomField6] = estimatelinedetail.CustomField6;
                dr[EstimatelinedetailFields.CustomField7] = estimatelinedetail.CustomField7;
                dr[EstimatelinedetailFields.CustomField8] = estimatelinedetail.CustomField8;
                dr[EstimatelinedetailFields.CustomField9] = estimatelinedetail.CustomField9;
                dr[EstimatelinedetailFields.CustomField10] = estimatelinedetail.CustomField10;
                dr[EstimatelinedetailFields.Idkey] = estimatelinedetail.Idkey;
                dr[EstimatelinedetailFields.GroupIDKEY] = estimatelinedetail.GroupIDKEY;
                dr[EstimatelinedetailFields.TableName] = estimatelinedetail.TableName;
                dr[EstimatelinedetailFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[EstimatelinedetailFields.SynDate] = DBNull.Value;
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adapter.Dispose();
                    _adapter = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion
    }
}
