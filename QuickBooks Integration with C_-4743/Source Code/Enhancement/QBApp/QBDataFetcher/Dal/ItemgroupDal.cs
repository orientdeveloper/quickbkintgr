//
// Class	:	ItemgroupDal.cs
// Author	:  	SynapseIndia @2013
// Date		:	4/2/2013 10:11:50 AM
//
using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class ItemgroupDal : IDisposable, IDal
	{
		#region Class Level Variables
        bool _fullsyn;
        string _qbfile;
        DateTime? _fromsyn;
        DateTime? _tosyn;
        int _chunksize;
        string _constring;
        itemgroupTableAdapter _adpater;
        #endregion

        #region Constructors / Destructors
        public ItemgroupDal(string qbfile, int chunksize, DateTime? fromsyn,DateTime? tosyn, bool fullsyn, string constring)
        {
            _adpater = new itemgroupTableAdapter();
            _adpater.Connection.ConnectionString = constring;
            _adpater.ClearBeforeFill = false;
            _fullsyn = fullsyn;
            _fromsyn = fromsyn;
            _tosyn = tosyn;
            _qbfile = qbfile;
            _chunksize = chunksize;
            _constring = constring;
        }
        #endregion

        #region Methods (Public)
        public void Synchronize()
        {
            try
            {
                using (ItemgroupFetch fetcher = new ItemgroupFetch(_qbfile, _chunksize, _fromsyn, _tosyn, Logger))
                {
                    while (fetcher.IsNextChunck)
                    {
                        QBdb.itemgroupDataTable table = new Dal.QBdb.itemgroupDataTable();
                        foreach (Itemgroup instance in fetcher.FetchNext())
                        {
                            #region Save Master
                            if (_fullsyn)
                            {
                                DataRow dr = table.NewRow();
                                table.Rows.Add(dr);
                                Bind(dr, instance);
                            }
                            else
                            {
                                _adpater.FillBy(table, instance.ListID);

                                DataRow dr = table.Rows.Cast<DataRow>().SingleOrDefault(s => string.Compare(s.Field<string>(ItemgroupFields.ListID), instance.ListID, true) == 0);

                                if (object.ReferenceEquals(dr, null))
                                {
                                    dr = table.NewRow();
                                    table.Rows.Add(dr);
                                }
                                Bind(dr, instance);
                            }
                            #endregion

                            #region Save Child
                            if (instance.Items.Count > 0)
                            {
                                using (ItemDal ItemDal = new ItemDal(_fullsyn, _constring, TableName.itemgroup, instance.ListID))
                                {
                                    ItemDal.Save(instance.Items);
                                }
                            }

                            if (instance.Itemgrouplinedetails.Count > 0)
                            {
                                using (ItemgrouplinedetailDal ItemgrouplinedetailDal = new ItemgrouplinedetailDal(_fullsyn, _constring, TableName.itemgroup, instance.ListID))
                                {
                                    ItemgrouplinedetailDal.Save(instance.Itemgrouplinedetails);
                                }
                            }

                            if (instance.DataExtDetails.Count > 0)
                            {
                                using (DataextdetailDal DataextdetailDal = new DataextdetailDal(_fullsyn, _constring, TableName.itemgroup, instance.ListID))
                                {
                                    DataextdetailDal.Save(instance.DataExtDetails);
                                }
                            }
                            #endregion
                        }
                        _adpater.Update(table);
                    }
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Itemgroup itemgroup)
        {
            try
            {
                dr[ItemgroupFields.ListID] = itemgroup.ListID;
                dr[ItemgroupFields.TimeCreated] = itemgroup.TimeCreated;
                dr[ItemgroupFields.TimeModified] = itemgroup.TimeModified;
                dr[ItemgroupFields.EditSequence] = itemgroup.EditSequence;
                dr[ItemgroupFields.Name] = itemgroup.Name;
                dr[ItemgroupFields.IsActive] = itemgroup.IsActive.GetShort().GetDbType();
                dr[ItemgroupFields.ItemDesc] = itemgroup.ItemDesc;
                dr[ItemgroupFields.IsPrintItemsInGroup] = itemgroup.IsPrintItemsInGroup.GetShort().GetDbType();
                dr[ItemgroupFields.SpecialItemType] = itemgroup.SpecialItemType;
                dr[ItemgroupFields.CustomField1] = itemgroup.CustomField1;
                dr[ItemgroupFields.CustomField2] = itemgroup.CustomField2;
                dr[ItemgroupFields.CustomField3] = itemgroup.CustomField3;
                dr[ItemgroupFields.CustomField4] = itemgroup.CustomField4;
                dr[ItemgroupFields.CustomField5] = itemgroup.CustomField5;
                dr[ItemgroupFields.CustomField6] = itemgroup.CustomField6;
                dr[ItemgroupFields.CustomField7] = itemgroup.CustomField7;
                dr[ItemgroupFields.CustomField8] = itemgroup.CustomField8;
                dr[ItemgroupFields.CustomField9] = itemgroup.CustomField9;
                dr[ItemgroupFields.CustomField10] = itemgroup.CustomField10;
                dr[ItemgroupFields.Status] = itemgroup.Status;
                dr[ItemgroupFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[ItemgroupFields.SynDate] = DBNull.Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adpater.Dispose();
                    _adpater = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion
	}
}
