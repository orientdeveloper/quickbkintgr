﻿//
// Class	:	PayrollitemnonwageDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:53 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;
using QBDataFetcher.DAL;

namespace QBDataFetcher.Dal
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class PayrollitemnonwageDal : IDisposable, IDal
    {
        #region Class Level Variables
        bool _fullsyn;
        string _qbfile;
        DateTime? _fromsyn;
        DateTime? _tosyn;
        int _chunksize;
        string _constring;
        payrollitemnonwageTableAdapter _adpater;
        #endregion

        #region Constructors / Destructors
        public PayrollitemnonwageDal(string qbfile, int chunksize, DateTime? fromsyn, DateTime? tosyn, bool fullsyn, string constring)
        {
            _adpater = new payrollitemnonwageTableAdapter();
            _adpater.Connection.ConnectionString = constring;
            _adpater.ClearBeforeFill = false;
            _fullsyn = fullsyn;
            _fromsyn = fromsyn;
            _tosyn = tosyn;
            _qbfile = qbfile;
            _chunksize = chunksize;
            _constring = constring;
        }
        #endregion

        #region Methods (Public)
        public void Synchronize()
        {
            try
            {
                using (PayrollitemnonwageFetch fetcher = new PayrollitemnonwageFetch(_qbfile, _chunksize, _fromsyn, _tosyn, Logger))
                {
                    while (fetcher.IsNextChunck)
                    {
                        QBdb.payrollitemnonwageDataTable table = new Dal.QBdb.payrollitemnonwageDataTable();
                        foreach (Payrollitemnonwage instance in fetcher.FetchNext())
                        {
                            #region Fetch payrollitemnonwage
                            if (_fullsyn)
                            {
                                DataRow dr = table.NewRow();
                                table.Rows.Add(dr);
                                Bind(dr, instance);
                            }
                            else
                            {
                                _adpater.FillBy(table, instance.ListID);

                                DataRow dr = table.Rows.Cast<DataRow>().SingleOrDefault(s => string.Compare(s.Field<string>(PayrollitemnonwageFields.ListID), instance.ListID, true) == 0);

                                if (object.ReferenceEquals(dr, null))
                                {
                                    dr = table.NewRow();
                                    table.Rows.Add(dr);
                                }
                                Bind(dr, instance);
                            }
                            #endregion

                            #region Save Child
                            if (instance.Items.Count > 0)
                            {
                                using (ItemDal ItemDal = new ItemDal(_fullsyn, _constring, TableName.Payrollitemnonwage, instance.ListID))
                                {
                                    ItemDal.Save(instance.Items);
                                }
                            }
                            #endregion
                        }
                        _adpater.Update(table);
                    }
                }
            }
            catch 
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Payrollitemnonwage payrollitemnonwage)
        {
            try
            {
                dr[PayrollitemnonwageFields.ListID] = payrollitemnonwage.ListID;
                dr[PayrollitemnonwageFields.TimeCreated] = payrollitemnonwage.TimeCreated;
                dr[PayrollitemnonwageFields.TimeModified] = payrollitemnonwage.TimeModified;
                dr[PayrollitemnonwageFields.EditSequence] = payrollitemnonwage.EditSequence;
                dr[PayrollitemnonwageFields.Name] = payrollitemnonwage.Name;
                dr[PayrollitemnonwageFields.IsActive] = payrollitemnonwage.IsActive.GetShort().GetDbType();
                dr[PayrollitemnonwageFields.NonWageType] = payrollitemnonwage.NonWageType;
                dr[PayrollitemnonwageFields.ExpenseAccountRef_ListID] = payrollitemnonwage.ExpenseAccountRef_ListID;
                dr[PayrollitemnonwageFields.ExpenseAccountRef_FullName] = payrollitemnonwage.ExpenseAccountRef_FullName;
                dr[PayrollitemnonwageFields.LiabilityAccountRef_ListID] = payrollitemnonwage.LiabilityAccountRef_ListID;
                dr[PayrollitemnonwageFields.LiabilityAccountRef_FullName] = payrollitemnonwage.LiabilityAccountRef_FullName;
                dr[PayrollitemnonwageFields.Status] = payrollitemnonwage.Status;
                dr[PayrollitemnonwageFields.SynDate] = DBNull.Value;
                dr[PayrollitemnonwageFields.IsSyn] = (short)SynStatus.Synchronize;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adpater.Dispose();
                    _adpater = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Implementation IDal
        public Logger.ISynLogger Logger
        {
            get;
            set;
        }
        #endregion
    }
}
