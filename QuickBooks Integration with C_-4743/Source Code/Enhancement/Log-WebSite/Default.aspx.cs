﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            DropDownList1.DataBind();
    }

    protected void Timer1_Tick(object sender, EventArgs e)
    {
        bindGrid();
    }

    void bindGrid()
    {
        //GridView1.DataSource = LinqDataSource1;
        if (DropDownList2.SelectedValue != "ALL")
            LinqDataSource1.Where = "CompanyId==Guid.Parse(\"" + new Guid(DropDownList1.SelectedValue) + "\") and ExceptionLevel==\"" + DropDownList2.SelectedValue + "\"";
        else
            LinqDataSource1.Where = "CompanyId==Guid.Parse(\"" + new Guid(DropDownList1.SelectedValue) + "\")";

        GridView1.DataSourceID = "LinqDataSource1";
        GridView1.DataBind();
    }

    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        bindGrid();
    }

    protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
    {
        bindGrid();
    }

    protected void viewLog_Click(object sender, EventArgs e)
    {
        if (password.Text == System.Configuration.ConfigurationManager.AppSettings["password"])
            Session["Password"] = "OK";
        else
            Response.Write("<font color='red'>Incorrect Password!!!</font></br></br>");
    }

    protected void btnTimer_Click(object sender, EventArgs e)
    {
        if (Timer1.Enabled)
        {
            btnTimer.Text = "Enable Timer";
            Timer1.Enabled = false;
        }
        else
        {
            btnTimer.Text = "Disable Timer";
            Timer1.Enabled = true;
        }
    }
}