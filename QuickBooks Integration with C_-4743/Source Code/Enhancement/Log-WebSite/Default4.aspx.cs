﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Default4 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Timer1_Tick(object sender, EventArgs e)
    {
        bindGrid();
    }

    void bindGrid()
    {
        //GridView1.DataSource = LinqDataSource1;
        //if (DropDownList2.SelectedValue != "ALL")
        //    LinqDataSource1.Where = "CompanyId==Guid.Parse(\"" + new Guid(DropDownList1.SelectedValue) + "\") and ExceptionLevel==\"" + DropDownList2.SelectedValue + "\"";
        //else
        //    LinqDataSource1.Where = "CompanyId==Guid.Parse(\"" + new Guid(DropDownList1.SelectedValue) + "\")";

        LinqDataSource1.WhereParameters.Clear();
        int opid = 0;
        LinqDataSource1.Where = "CompanyId = @CompanyId";
        LinqDataSource1.WhereParameters.Add("CompanyId", System.Data.DbType.Guid, DropDownList1.SelectedValue);
        if (!string.IsNullOrEmpty(txtoperationid.Text))
        {
            if (int.TryParse(txtoperationid.Text.Trim(), out opid))
            {
                LinqDataSource1.Where = "OperationId = @OperationId";
                LinqDataSource1.WhereParameters.Add("OperationId", System.Data.DbType.Int64, txtoperationid.Text.Trim());
            }
        }
        if (!string.IsNullOrEmpty(txtStatus.Text))
        {
            LinqDataSource1.Where = "Status = @Status";
            LinqDataSource1.WhereParameters.Add("Status", System.Data.DbType.Int32, txtoperationid.Text.Trim());
        }
        if (!string.IsNullOrEmpty(txtMessageTime.Text))
        {
            LinqDataSource1.Where = "MessageTime = @MessageTime";
            LinqDataSource1.WhereParameters.Add("MessageTime", System.Data.DbType.DateTime, txtoperationid.Text.Trim());
        }
        if (!string.IsNullOrEmpty(txtMessage.Text))
        {
            LinqDataSource1.Where = "Message = @Message";
            LinqDataSource1.WhereParameters.Add("Message", System.Data.DbType.String, txtoperationid.Text.Trim());
        }
        GridView1.DataSourceID = "LinqDataSource1";
        GridView1.DataBind();
    }

    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        bindGrid();
    }

    //protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    bindGrid();
    //}

    protected void viewLog_Click(object sender, EventArgs e)
    {
        if (password.Text == System.Configuration.ConfigurationManager.AppSettings["password"])
            Session["Password"] = "OK";
        else
            Response.Write("<font color='red'>Incorrect Password!!!</font></br></br>");
    }

    protected void btnTimer_Click(object sender, EventArgs e)
    {
        if (Timer1.Enabled)
        {
            btnTimer.Text = "Enable Timer";
            Timer1.Enabled = false;
        }
        else
        {
            btnTimer.Text = "Disable Timer";
            Timer1.Enabled = true;
        }
    }

    protected void btn_Click(object sender, EventArgs e)
    {

    }
}