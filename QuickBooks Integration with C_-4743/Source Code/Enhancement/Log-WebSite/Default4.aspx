﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default4.aspx.cs" Inherits="Default4" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <%   
            if (Session["Password"] != null)
            {
        %>
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <div>
                <h1 style="width:350px; float: left;">SSIS Log :- Test Company</h1>
                <div style="float: right; margin-right: 100px; width: 200px;">
                    Current Server time :- 
                <asp:UpdatePanel ID="uptimmer" runat="server">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="Timer2" EventName="Tick" />
                    </Triggers>
                    <ContentTemplate>
                        <%=System.DateTime.Now %>
                    </ContentTemplate>
                </asp:UpdatePanel>
                    <asp:Timer ID="Timer2" runat="server" Interval="1000" OnTick="Timer1_Tick"></asp:Timer>
                </div>
                <div style="clear:both;"></div>
            </div>
            <br />
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;<a href="Default.aspx">Live Company Sync log</a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="Default2.aspx">Test Company Sync log</a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="Default3.aspx">Live Company SSIS log</a>
            <br />
            <br />
            Select Company
            <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" DataSourceID="LinqDataSource2" DataTextField="CompanyName" DataValueField="CompanyId" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:LinqDataSource ID="LinqDataSource2" runat="server" ContextTypeName="Data2DataContext" EntityTypeName="" TableName="Company1s" OrderBy="CompanyName">
            </asp:LinqDataSource>
            <%--Select Error Type
            <asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged">
                <asp:ListItem>ALL</asp:ListItem>
                <asp:ListItem>FATAL</asp:ListItem>
                <asp:ListItem>ERROR</asp:ListItem>
                <asp:ListItem>INFO</asp:ListItem>
            </asp:DropDownList>--%>
            <asp:Button ID="btnTimer" runat="server" Text="Disable Timer" OnClick="btnTimer_Click" />&nbsp;&nbsp;&nbsp;&nbsp;
             <br />
            <br />
            OperationId : <asp:TextBox ID="txtoperationid" runat="server"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;
            Status : <asp:TextBox ID="txtStatus" runat="server"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;
            MessageTime : <asp:TextBox ID="txtMessageTime" runat="server"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;
            Message : <asp:TextBox ID="txtMessage" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Button id="btn" runat="server" Text="Search" OnClick="btn_Click"/>
            <br />
            <br />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
                </Triggers>
                <ContentTemplate>
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" CellPadding="4" ForeColor="#333333" PageSize="25" DataSourceID="LinqDataSource1" ViewStateMode="Disabled">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:BoundField DataField="OperationId" HeaderText="OperationId" ReadOnly="True" SortExpression="OperationId" />
                            <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="True" SortExpression="Status" />
                            <asp:BoundField DataField="MessageTime" HeaderText="MessageTime" ReadOnly="True" SortExpression="MessageTime" />
                            <asp:BoundField DataField="Message" HeaderText="Message" ReadOnly="True" SortExpression="Message" />
                        </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
                    <asp:LinqDataSource ID="LinqDataSource3" runat="server" EntityTypeName="">
                    </asp:LinqDataSource>
                    <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="Data2DataContext" EntityTypeName="" Select="new (DatabaseName, OperationId, Status, MessageTime, Message)" TableName="SSISLog1s" Where="CompanyId == @CompanyId">
                        <WhereParameters>
                            <asp:ControlParameter ControlID="DropDownList1" DbType="Guid" Name="CompanyId" PropertyName="SelectedValue" />
                        </WhereParameters>
                    </asp:LinqDataSource>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:Timer ID="Timer1" runat="server" Interval="10000" OnTick="Timer1_Tick"></asp:Timer>
        </div>
        <%
            }
            else
            {
        %>
        <div>
            Enter Password :
            <asp:TextBox ID="password" runat="server" TextMode="Password"></asp:TextBox>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button Text="View Log" runat="server" ID="viewLog" OnClick="viewLog_Click" />
        </div>
        <%
            }
        %>
    </form>
</body>
</html>
