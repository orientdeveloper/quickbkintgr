﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Default3 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Timer1_Tick(object sender, EventArgs e)
    {
        bindGrid();
    }

    void bindGrid()
    {
        //GridView1.DataSource = LinqDataSource1;
        //if (DropDownList2.SelectedValue != "ALL")
        //    LinqDataSource1.Where = "CompanyId==Guid.Parse(\"" + new Guid(DropDownList1.SelectedValue) + "\") and ExceptionLevel==\"" + DropDownList2.SelectedValue + "\"";
        //else
        //    LinqDataSource1.Where = "CompanyId==Guid.Parse(\"" + new Guid(DropDownList1.SelectedValue) + "\")";

        LinqDataSource1.WhereParameters.Clear();
        int opid = 0;
        string wherecondition = string.Empty;
        wherecondition = "CompanyId==Guid.Parse(\"" + new Guid(DropDownList1.SelectedValue) + "\")";
        if (!string.IsNullOrEmpty(txtoperationid.Text))
        {
            if (int.TryParse(txtoperationid.Text.Trim(), out opid))
            {
                wherecondition = wherecondition + "and OperationId==" + txtoperationid.Text.Trim();
                //LinqDataSource1.WhereParameters.Add("OperationId", System.Data.DbType.Int64, txtoperationid.Text.Trim());
            }
        }
        if (!string.IsNullOrEmpty(txtStatus.Text))
        {
            wherecondition = wherecondition + "and Status==" + txtStatus.Text.Trim();
            //LinqDataSource1.WhereParameters.Add("Status", System.Data.DbType.Int32, txtStatus.Text.Trim());
        }
        if (!string.IsNullOrEmpty(txtMessageTime.Text))
        {
            wherecondition = wherecondition + "and MessageTime==" + txtMessageTime.Text.Trim();
            //LinqDataSource1.WhereParameters.Add("MessageTime", System.Data.DbType.DateTime, txtMessageTime.Text.Trim());
        }
        if (!string.IsNullOrEmpty(txtMessage.Text))
        {
            wherecondition = wherecondition + "and Message==" + txtMessage.Text.Trim();
            //LinqDataSource1.WhereParameters.Add("Message", System.Data.DbType.String, txtMessage.Text.Trim());
        }

        GridView1.DataSourceID = "LinqDataSource1";
        GridView1.DataBind();
    }

    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        bindGrid();
    }

    //protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    bindGrid();
    //}

    protected void viewLog_Click(object sender, EventArgs e)
    {
        if (password.Text == System.Configuration.ConfigurationManager.AppSettings["password"])
            Session["Password"] = "OK";
        else
            Response.Write("<font color='red'>Incorrect Password!!!</font></br></br>");
    }

    protected void btnTimer_Click(object sender, EventArgs e)
    {
        if (Timer1.Enabled)
        {
            btnTimer.Text = "Enable Timer";
            Timer1.Enabled = false;
        }
        else
        {
            btnTimer.Text = "Disable Timer";
            Timer1.Enabled = true;
        }
    }

    protected void btn_Click(object sender, EventArgs e)
    {
        bindGrid();
    }
}