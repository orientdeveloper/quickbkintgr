﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <%
            if (Session["Password"] != null)
            {
        %>
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <div>
                <h1 style="width:350px; float: left;">Sync Log :- Company</h1>
                <div style="float: right; margin-right: 100px; width: 200px;">
                    Current Server time :- 
                <asp:UpdatePanel ID="uptimmer" runat="server">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="Timer2" EventName="Tick" />
                    </Triggers>
                    <ContentTemplate>
                        <%=System.DateTime.Now %>
                    </ContentTemplate>
                </asp:UpdatePanel>
                    <asp:Timer ID="Timer2" runat="server" Interval="1000" OnTick="Timer1_Tick"></asp:Timer>
                </div>
                <div style="clear:both;"></div>
            </div>
            <br />
            <br />
            &nbsp;&nbsp;&nbsp;<a href="Default2.aspx">Test Company</a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="Default3.aspx">Live Company SSIS log</a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="Default4.aspx">Test Company SSIS log</a>
            <br />
            <br />
            Select Company
            <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" DataSourceID="LinqDataSource2" DataTextField="CompanyName" DataValueField="CompanyId" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
            </asp:DropDownList>
            Select Error Type
            <asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged">
                <asp:ListItem>ALL</asp:ListItem>
                <asp:ListItem>FATAL</asp:ListItem>
                <asp:ListItem>ERROR</asp:ListItem>
                <asp:ListItem>INFO</asp:ListItem>
            </asp:DropDownList>
            <asp:LinqDataSource ID="LinqDataSource2" runat="server" ContextTypeName="DataClassesDataContext" EntityTypeName="" Select="new (CompanyId, CompanyName)" TableName="Companies" OrderBy="CompanyName">
            </asp:LinqDataSource>
            <br />
            <br />
            <asp:Button ID="btnTimer" runat="server" Text="Disable Timer" OnClick="btnTimer_Click" />
            <br />
            <br />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
                </Triggers>
                <ContentTemplate>
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" CellPadding="4" ForeColor="#333333" PageSize="25" DataSourceID="LinqDataSource1" ViewStateMode="Disabled">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="Id" />
                            <asp:BoundField DataField="ExceptionLevel" HeaderText="ExceptionLevel" ReadOnly="True" SortExpression="ExceptionLevel" />
                            <asp:BoundField DataField="ExceptionMessage" HeaderText="ExceptionMessage" ReadOnly="True" SortExpression="ExceptionMessage" />
                            <asp:BoundField DataField="Message" HeaderText="Message" ReadOnly="True" SortExpression="Message" />
                            <asp:BoundField DataField="Dated" HeaderText="Dated" ReadOnly="True" SortExpression="Dated" />
                        </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
                    <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="DataClassesDataContext" EntityTypeName="" OrderBy="Id desc" Select="new (Id, Message, Dated, ExceptionMessage, ExceptionLevel)" TableName="ClientExceptions" Where="CompanyId == @CompanyId">
                        <WhereParameters>
                            <asp:ControlParameter ControlID="DropDownList1" DbType="Guid" Name="CompanyId" PropertyName="SelectedValue" />
                        </WhereParameters>
                    </asp:LinqDataSource>

                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:Timer ID="Timer1" runat="server" Interval="10000" OnTick="Timer1_Tick"></asp:Timer>
        </div>
        <%}
            else
            {
        %>

        <div>
            Enter Password :
            <asp:TextBox ID="password" runat="server" TextMode="Password"></asp:TextBox>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button Text="View Log" runat="server" ID="viewLog" OnClick="viewLog_Click" />
        </div>

        <%} %>
    </form>
</body>
</html>
