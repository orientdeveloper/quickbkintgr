//
// Class	:	Purchaseorder.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:44 AM
//
using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization;
namespace QBSynDal
{
    /// <summary>
    /// Class for the properties of the object
    /// </summary>
    public class PurchaseorderFields
    {
        public const string TxnID = "TxnID";
        public const string TimeCreated = "TimeCreated";
        public const string TimeModified = "TimeModified";
        public const string EditSequence = "EditSequence";
        public const string TxnNumber = "TxnNumber";
        public const string VendorRef_ListID = "VendorRef_ListID";
        public const string VendorRef_FullName = "VendorRef_FullName";
        public const string ClassRef_ListID = "ClassRef_ListID";
        public const string ClassRef_FullName = "ClassRef_FullName";
        public const string InventorySiteRef_ListID = "InventorySiteRef_ListID";
        public const string InventorySiteRef_FullName = "InventorySiteRef_FullName";
        public const string ShipToEntityRef_ListID = "ShipToEntityRef_ListID";
        public const string ShipToEntityRef_FullName = "ShipToEntityRef_FullName";
        public const string TemplateRef_ListID = "TemplateRef_ListID";
        public const string TemplateRef_FullName = "TemplateRef_FullName";
        public const string TxnDate = "TxnDate";
        public const string RefNumber = "RefNumber";
        public const string VendorAddress_Addr1 = "VendorAddress_Addr1";
        public const string VendorAddress_Addr2 = "VendorAddress_Addr2";
        public const string VendorAddress_Addr3 = "VendorAddress_Addr3";
        public const string VendorAddress_Addr4 = "VendorAddress_Addr4";
        public const string VendorAddress_Addr5 = "VendorAddress_Addr5";
        public const string VendorAddress_City = "VendorAddress_City";
        public const string VendorAddress_State = "VendorAddress_State";
        public const string VendorAddress_PostalCode = "VendorAddress_PostalCode";
        public const string VendorAddress_Country = "VendorAddress_Country";
        public const string VendorAddress_Note = "VendorAddress_Note";
        public const string ShipAddress_Addr1 = "ShipAddress_Addr1";
        public const string ShipAddress_Addr2 = "ShipAddress_Addr2";
        public const string ShipAddress_Addr3 = "ShipAddress_Addr3";
        public const string ShipAddress_Addr4 = "ShipAddress_Addr4";
        public const string ShipAddress_Addr5 = "ShipAddress_Addr5";
        public const string ShipAddress_City = "ShipAddress_City";
        public const string ShipAddress_State = "ShipAddress_State";
        public const string ShipAddress_PostalCode = "ShipAddress_PostalCode";
        public const string ShipAddress_Country = "ShipAddress_Country";
        public const string ShipAddress_Note = "ShipAddress_Note";
        public const string TermsRef_ListID = "TermsRef_ListID";
        public const string TermsRef_FullName = "TermsRef_FullName";
        public const string DueDate = "DueDate";
        public const string ExpectedDate = "ExpectedDate";
        public const string ShipMethodRef_ListID = "ShipMethodRef_ListID";
        public const string ShipMethodRef_FullName = "ShipMethodRef_FullName";
        public const string Fob = "FOB";
        public const string TotalAmount = "TotalAmount";
        public const string CurrencyRef_ListID = "CurrencyRef_ListID";
        public const string CurrencyRef_FullName = "CurrencyRef_FullName";
        public const string ExchangeRate = "ExchangeRate";
        public const string TotalAmountInHomeCurrency = "TotalAmountInHomeCurrency";
        public const string Memo = "Memo";
        public const string VendorMsg = "VendorMsg";
        public const string IsToBePrinted = "IsToBePrinted";
        public const string IsToBeEmailed = "IsToBeEmailed";
        public const string IsTaxIncluded = "IsTaxIncluded";
        public const string SalesTaxCodeRef_ListID = "SalesTaxCodeRef_ListID";
        public const string SalesTaxCodeRef_FullName = "SalesTaxCodeRef_FullName";
        public const string Other1 = "Other1";
        public const string Other2 = "Other2";
        public const string IsManuallyClosed = "IsManuallyClosed";
        public const string IsFullyReceived = "IsFullyReceived";
        public const string CustomField1 = "CustomField1";
        public const string CustomField2 = "CustomField2";
        public const string CustomField3 = "CustomField3";
        public const string CustomField4 = "CustomField4";
        public const string CustomField5 = "CustomField5";
        public const string CustomField6 = "CustomField6";
        public const string CustomField7 = "CustomField7";
        public const string CustomField8 = "CustomField8";
        public const string CustomField9 = "CustomField9";
        public const string CustomField10 = "CustomField10";
        public const string Status = "Status";
        
    }
    /// <summary>
    /// Model class for the "purchaseorder" table.
    /// </summary>
    [DataContract(Name = "ServicePurchaseorder")]
    public class Purchaseorder : IModel
    {

        #region Class Level Variables

        private string _txnID = null;
        private string _timeCreated = null;
        private string _timeModified = null;
        private string _editSequence = null;
        private int? _txnNumber = null;
        private string _vendorRef_ListID = null;
        private string _vendorRef_FullName = null;
        private string _classRef_ListID = null;
        private string _classRef_FullName = null;
        private string _inventorySiteRef_ListID = null;
        private string _inventorySiteRef_FullName = null;
        private string _shipToEntityRef_ListID = null;
        private string _shipToEntityRef_FullName = null;
        private string _templateRef_ListID = null;
        private string _templateRef_FullName = null;
        private DateTime? _txnDate = null;
        private string _refNumber = null;
        private string _vendorAddress_Addr1 = null;
        private string _vendorAddress_Addr2 = null;
        private string _vendorAddress_Addr3 = null;
        private string _vendorAddress_Addr4 = null;
        private string _vendorAddress_Addr5 = null;
        private string _vendorAddress_City = null;
        private string _vendorAddress_State = null;
        private string _vendorAddress_PostalCode = null;
        private string _vendorAddress_Country = null;
        private string _vendorAddress_Note = null;
        private string _shipAddress_Addr1 = null;
        private string _shipAddress_Addr2 = null;
        private string _shipAddress_Addr3 = null;
        private string _shipAddress_Addr4 = null;
        private string _shipAddress_Addr5 = null;
        private string _shipAddress_City = null;
        private string _shipAddress_State = null;
        private string _shipAddress_PostalCode = null;
        private string _shipAddress_Country = null;
        private string _shipAddress_Note = null;
        private string _termsRef_ListID = null;
        private string _termsRef_FullName = null;
        private DateTime? _dueDate = null;
        private DateTime? _expectedDate = null;
        private string _shipMethodRef_ListID = null;
        private string _shipMethodRef_FullName = null;
        private string _fob = null;
        private double? _totalAmount = null;
        private string _currencyRef_ListID = null;
        private string _currencyRef_FullName = null;
        private double? _exchangeRate = null;
        private double? _totalAmountInHomeCurrency = null;
        private string _memo = null;
        private string _vendorMsg = null;
        private bool? _isToBePrinted = null;
        private bool? _isToBeEmailed = null;
        private bool? _isTaxIncluded = null;
        private string _salesTaxCodeRef_ListID = null;
        private string _salesTaxCodeRef_FullName = null;
        private string _other1 = null;
        private string _other2 = null;
        private string _isManuallyClosed = null;
        private string _isFullyReceived = null;
        private string _customField1 = null;
        private string _customField2 = null;
        private string _customField3 = null;
        private string _customField4 = null;
        private string _customField5 = null;
        private string _customField6 = null;
        private string _customField7 = null;
        private string _customField8 = null;
        private string _customField9 = null;
        private string _customField10 = null;
        private string _status = null;

        #endregion

        #region Properties

        /// <summary>
        /// This property is mapped to the "TxnID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TxnID
        {
            get
            {
                return _txnID;
            }
            set
            {
                _txnID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeCreated" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TimeCreated
        {
            get
            {
                return _timeCreated;
            }
            set
            {

                _timeCreated = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeModified" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TimeModified
        {
            get
            {
                return _timeModified;
            }
            set
            {
                _timeModified = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "EditSequence" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string EditSequence
        {
            get
            {
                return _editSequence;
            }
            set
            {
                _editSequence = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TxnNumber" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public int? TxnNumber
        {
            get
            {
                return _txnNumber;
            }
            set
            {
                _txnNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "VendorRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string VendorRef_ListID
        {
            get
            {
                return _vendorRef_ListID;
            }
            set
            {
                _vendorRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "VendorRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string VendorRef_FullName
        {
            get
            {
                return _vendorRef_FullName;
            }
            set
            {

                _vendorRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ClassRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ClassRef_ListID
        {
            get
            {
                return _classRef_ListID;
            }
            set
            {

                _classRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ClassRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ClassRef_FullName
        {
            get
            {
                return _classRef_FullName;
            }
            set
            {
                _classRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "InventorySiteRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string InventorySiteRef_ListID
        {
            get
            {
                return _inventorySiteRef_ListID;
            }
            set
            {
                _inventorySiteRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "InventorySiteRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string InventorySiteRef_FullName
        {
            get
            {
                return _inventorySiteRef_FullName;
            }
            set
            {
                _inventorySiteRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipToEntityRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipToEntityRef_ListID
        {
            get
            {
                return _shipToEntityRef_ListID;
            }
            set
            {
                _shipToEntityRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipToEntityRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipToEntityRef_FullName
        {
            get
            {
                return _shipToEntityRef_FullName;
            }
            set
            {

                _shipToEntityRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TemplateRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TemplateRef_ListID
        {
            get
            {
                return _templateRef_ListID;
            }
            set
            {

                _templateRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TemplateRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TemplateRef_FullName
        {
            get
            {
                return _templateRef_FullName;
            }
            set
            {
                _templateRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TxnDate" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public DateTime? TxnDate
        {
            get
            {
                return _txnDate;
            }
            set
            {
                _txnDate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "RefNumber" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string RefNumber
        {
            get
            {
                return _refNumber;
            }
            set
            {
                _refNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "VendorAddress_Addr1" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string VendorAddress_Addr1
        {
            get
            {
                return _vendorAddress_Addr1;
            }
            set
            {
                _vendorAddress_Addr1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "VendorAddress_Addr2" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string VendorAddress_Addr2
        {
            get
            {
                return _vendorAddress_Addr2;
            }
            set
            {
                _vendorAddress_Addr2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "VendorAddress_Addr3" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string VendorAddress_Addr3
        {
            get
            {
                return _vendorAddress_Addr3;
            }
            set
            {
                _vendorAddress_Addr3 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "VendorAddress_Addr4" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string VendorAddress_Addr4
        {
            get
            {
                return _vendorAddress_Addr4;
            }
            set
            {
                _vendorAddress_Addr4 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "VendorAddress_Addr5" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string VendorAddress_Addr5
        {
            get
            {
                return _vendorAddress_Addr5;
            }
            set
            {
                _vendorAddress_Addr5 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "VendorAddress_City" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string VendorAddress_City
        {
            get
            {
                return _vendorAddress_City;
            }
            set
            {
                _vendorAddress_City = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "VendorAddress_State" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string VendorAddress_State
        {
            get
            {
                return _vendorAddress_State;
            }
            set
            {
                _vendorAddress_State = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "VendorAddress_PostalCode" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string VendorAddress_PostalCode
        {
            get
            {
                return _vendorAddress_PostalCode;
            }
            set
            {
                _vendorAddress_PostalCode = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "VendorAddress_Country" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string VendorAddress_Country
        {
            get
            {
                return _vendorAddress_Country;
            }
            set
            {
                _vendorAddress_Country = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "VendorAddress_Note" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string VendorAddress_Note
        {
            get
            {
                return _vendorAddress_Note;
            }
            set
            {
                _vendorAddress_Note = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_Addr1" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipAddress_Addr1
        {
            get
            {
                return _shipAddress_Addr1;
            }
            set
            {
                _shipAddress_Addr1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_Addr2" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipAddress_Addr2
        {
            get
            {
                return _shipAddress_Addr2;
            }
            set
            {
                _shipAddress_Addr2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_Addr3" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipAddress_Addr3
        {
            get
            {
                return _shipAddress_Addr3;
            }
            set
            {
                _shipAddress_Addr3 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_Addr4" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipAddress_Addr4
        {
            get
            {
                return _shipAddress_Addr4;
            }
            set
            {
                _shipAddress_Addr4 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_Addr5" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipAddress_Addr5
        {
            get
            {
                return _shipAddress_Addr5;
            }
            set
            {
                _shipAddress_Addr5 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_City" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipAddress_City
        {
            get
            {
                return _shipAddress_City;
            }
            set
            {
                _shipAddress_City = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_State" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipAddress_State
        {
            get
            {
                return _shipAddress_State;
            }
            set
            {
                _shipAddress_State = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_PostalCode" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipAddress_PostalCode
        {
            get
            {
                return _shipAddress_PostalCode;
            }
            set
            {
                _shipAddress_PostalCode = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_Country" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipAddress_Country
        {
            get
            {
                return _shipAddress_Country;
            }
            set
            {
                _shipAddress_Country = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_Note" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipAddress_Note
        {
            get
            {
                return _shipAddress_Note;
            }
            set
            {
                _shipAddress_Note = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TermsRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TermsRef_ListID
        {
            get
            {
                return _termsRef_ListID;
            }
            set
            {
                _termsRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TermsRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TermsRef_FullName
        {
            get
            {
                return _termsRef_FullName;
            }
            set
            {
                _termsRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "DueDate" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public DateTime? DueDate
        {
            get
            {
                return _dueDate;
            }
            set
            {
                _dueDate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ExpectedDate" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public DateTime? ExpectedDate
        {
            get
            {
                return _expectedDate;
            }
            set
            {
                _expectedDate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipMethodRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipMethodRef_ListID
        {
            get
            {
                return _shipMethodRef_ListID;
            }
            set
            {
                _shipMethodRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipMethodRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipMethodRef_FullName
        {
            get
            {
                return _shipMethodRef_FullName;
            }
            set
            {
                _shipMethodRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "FOB" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Fob
        {
            get
            {
                return _fob;
            }
            set
            {

                _fob = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TotalAmount" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? TotalAmount
        {
            get
            {
                return _totalAmount;
            }
            set
            {
                _totalAmount = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CurrencyRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CurrencyRef_ListID
        {
            get
            {
                return _currencyRef_ListID;
            }
            set
            {
                _currencyRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CurrencyRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CurrencyRef_FullName
        {
            get
            {
                return _currencyRef_FullName;
            }
            set
            {
                _currencyRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ExchangeRate" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? ExchangeRate
        {
            get
            {
                return _exchangeRate;
            }
            set
            {
                _exchangeRate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TotalAmountInHomeCurrency" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? TotalAmountInHomeCurrency
        {
            get
            {
                return _totalAmountInHomeCurrency;
            }
            set
            {
                _totalAmountInHomeCurrency = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Memo" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Memo
        {
            get
            {
                return _memo;
            }
            set
            {
                    _memo = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "VendorMsg" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string VendorMsg
        {
            get
            {
                return _vendorMsg;
            }
            set
            {
                _vendorMsg = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsToBePrinted" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public bool? IsToBePrinted
        {
            get
            {
                return _isToBePrinted;
            }
            set
            {
                _isToBePrinted = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsToBeEmailed" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public bool? IsToBeEmailed
        {
            get
            {
                return _isToBeEmailed;
            }
            set
            {
                _isToBeEmailed = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsTaxIncluded" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public bool? IsTaxIncluded
        {
            get
            {
                return _isTaxIncluded;
            }
            set
            {
                _isTaxIncluded = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SalesTaxCodeRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SalesTaxCodeRef_ListID
        {
            get
            {
                return _salesTaxCodeRef_ListID;
            }
            set
            {
                _salesTaxCodeRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SalesTaxCodeRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SalesTaxCodeRef_FullName
        {
            get
            {
                return _salesTaxCodeRef_FullName;
            }
            set
            {
                _salesTaxCodeRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Other1" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Other1
        {
            get
            {
                return _other1;
            }
            set
            {
                _other1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Other2" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Other2
        {
            get
            {
                return _other2;
            }
            set
            {
                _other2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsManuallyClosed" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>

        [DataMember]
        public string IsManuallyClosed
        {
            get
            {
                return _isManuallyClosed;
            }
            set
            {
                _isManuallyClosed = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsFullyReceived" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string IsFullyReceived
        {
            get
            {
                return _isFullyReceived;
            }
            set
            {
                _isFullyReceived = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField1" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField1
        {
            get
            {
                return _customField1;
            }
            set
            {
                _customField1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField2" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField2
        {
            get
            {
                return _customField2;
            }
            set
            {
                _customField2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField3" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField3
        {
            get
            {
                return _customField3;
            }
            set
            {
                _customField3 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField4" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField4
        {
            get
            {
                return _customField4;
            }
            set
            {
                _customField4 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField5" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField5
        {
            get
            {
                return _customField5;
            }
            set
            {
                _customField5 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField6" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField6
        {
            get
            {
                return _customField6;
            }
            set
            {
                _customField6 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField7" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField7
        {
            get
            {
                return _customField7;
            }
            set
            {
                _customField7 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField8" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField8
        {
            get
            {
                return _customField8;
            }
            set
            {
                _customField8 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField9" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField9
        {
            get
            {
                return _customField9;
            }
            set
            {
                _customField9 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField10" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField10
        {
            get
            {
                return _customField10;
            }
            set
            {
                _customField10 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Status" field. Length must be between 0 and 10 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        #endregion

    }
}
