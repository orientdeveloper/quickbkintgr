//
// Class	:	Arrefundcreditcard.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:44 AM
//
using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization;

namespace QBSynDal
{
    /// <summary>
    /// Class for the properties of the object
    /// </summary>
    public class ArrefundcreditcardFields
    {
        public const string TxnID = "TxnID";
        public const string TimeCreated = "TimeCreated";
        public const string TimeModified = "TimeModified";
        public const string EditSequence = "EditSequence";
        public const string TxnNumber = "TxnNumber";
        public const string CustomerRef_ListID = "CustomerRef_ListID";
        public const string CustomerRef_FullName = "CustomerRef_FullName";
        public const string RefundFromAccountRef_ListID = "RefundFromAccountRef_ListID";
        public const string RefundFromAccountRef_FullName = "RefundFromAccountRef_FullName";
        public const string ARAccountRef_ListID = "ARAccountRef_ListID";
        public const string ARAccountRef_FullName = "ARAccountRef_FullName";
        public const string TxnDate = "TxnDate";
        public const string RefNumber = "RefNumber";
        public const string TotalAmount = "TotalAmount";
        public const string Address_Addr1 = "Address_Addr1";
        public const string Address_Addr2 = "Address_Addr2";
        public const string Address_Addr3 = "Address_Addr3";
        public const string Address_Addr4 = "Address_Addr4";
        public const string Address_Addr5 = "Address_Addr5";
        public const string Address_City = "Address_City";
        public const string Address_State = "Address_State";
        public const string Address_PostalCode = "Address_PostalCode";
        public const string Address_Country = "Address_Country";
        public const string Address_Note = "Address_Note";
        public const string AddressBlock_Addr1 = "AddressBlock_Addr1";
        public const string AddressBlock_Addr2 = "AddressBlock_Addr2";
        public const string AddressBlock_Addr3 = "AddressBlock_Addr3";
        public const string AddressBlock_Addr4 = "AddressBlock_Addr4";
        public const string AddressBlock_Addr5 = "AddressBlock_Addr5";
        public const string PaymentMethodRef_ListID = "PaymentMethodRef_ListID";
        public const string PaymentMethodRef_FullName = "PaymentMethodRef_FullName";
        public const string Memo = "Memo";
        public const string CreditCardTxnInfo = "CreditCardTxnInfo";
        public const string CustomField1 = "CustomField1";
        public const string CustomField2 = "CustomField2";
        public const string CustomField3 = "CustomField3";
        public const string CustomField4 = "CustomField4";
        public const string CustomField5 = "CustomField5";
        public const string CustomField6 = "CustomField6";
        public const string CustomField7 = "CustomField7";
        public const string CustomField8 = "CustomField8";
        public const string CustomField9 = "CustomField9";
        public const string CustomField10 = "CustomField10";
        public const string Status = "Status";

    }
    /// <summary>
    /// Model class for the "arrefundcreditcard" table.
    /// </summary>
    [DataContract(Name = "ServiceArrefundcreditcard")]
    public class Arrefundcreditcard : IModel
    {
        #region Class Level Variables

        private string _txnID = null;
        private string _timeCreated = null;
        private string _timeModified = null;
        private string _editSequence = null;
        private int? _txnNumber = null;
        private string _customerRef_ListID = null;
        private string _customerRef_FullName = null;
        private string _refundFromAccountRef_ListID = null;
        private string _refundFromAccountRef_FullName = null;
        private string _aRAccountRef_ListID = null;
        private string _aRAccountRef_FullName = null;
        private DateTime? _txnDate = null;
        private string _refNumber = null;
        private double? _totalAmount = null;
        private string _address_Addr1 = null;
        private string _address_Addr2 = null;
        private string _address_Addr3 = null;
        private string _address_Addr4 = null;
        private string _address_Addr5 = null;
        private string _address_City = null;
        private string _address_State = null;
        private string _address_PostalCode = null;
        private string _address_Country = null;
        private string _address_Note = null;
        private string _addressBlock_Addr1 = null;
        private string _addressBlock_Addr2 = null;
        private string _addressBlock_Addr3 = null;
        private string _addressBlock_Addr4 = null;
        private string _addressBlock_Addr5 = null;
        private string _paymentMethodRef_ListID = null;
        private string _paymentMethodRef_FullName = null;
        private string _memo = null;
        private string _creditCardTxnInfo = null;
        private string _customField1 = null;
        private string _customField2 = null;
        private string _customField3 = null;
        private string _customField4 = null;
        private string _customField5 = null;
        private string _customField6 = null;
        private string _customField7 = null;
        private string _customField8 = null;
        private string _customField9 = null;
        private string _customField10 = null;
        private string _status = null;

        #endregion

        #region Properties

        /// <summary>
        /// This property is mapped to the "TxnID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TxnID
        {
            get
            {
                return _txnID;
            }
            set
            {
                _txnID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeCreated" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TimeCreated
        {
            get
            {
                return _timeCreated;
            }
            set
            {
                _timeCreated = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeModified" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TimeModified
        {
            get
            {
                return _timeModified;
            }
            set
            {
                _timeModified = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "EditSequence" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string EditSequence
        {
            get
            {
                return _editSequence;
            }
            set
            {
                _editSequence = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TxnNumber" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public int? TxnNumber
        {
            get
            {
                return _txnNumber;
            }
            set
            {
                _txnNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomerRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomerRef_ListID
        {
            get
            {
                return _customerRef_ListID;
            }
            set
            {
                _customerRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomerRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomerRef_FullName
        {
            get
            {
                return _customerRef_FullName;
            }
            set
            {
                _customerRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "RefundFromAccountRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string RefundFromAccountRef_ListID
        {
            get
            {
                return _refundFromAccountRef_ListID;
            }
            set
            {
                _refundFromAccountRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "RefundFromAccountRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string RefundFromAccountRef_FullName
        {
            get
            {
                return _refundFromAccountRef_FullName;
            }
            set
            {
                _refundFromAccountRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ARAccountRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ARAccountRef_ListID
        {
            get
            {
                return _aRAccountRef_ListID;
            }
            set
            {
                _aRAccountRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ARAccountRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ARAccountRef_FullName
        {
            get
            {
                return _aRAccountRef_FullName;
            }
            set
            {
                _aRAccountRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TxnDate" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public DateTime? TxnDate
        {
            get
            {
                return _txnDate;
            }
            set
            {
                _txnDate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "RefNumber" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string RefNumber
        {
            get
            {
                return _refNumber;
            }
            set
            {
                _refNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TotalAmount" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? TotalAmount
        {
            get
            {
                return _totalAmount;
            }
            set
            {
                _totalAmount = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Address_Addr1" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Address_Addr1
        {
            get
            {
                return _address_Addr1;
            }
            set
            {
                _address_Addr1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Address_Addr2" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Address_Addr2
        {
            get
            {
                return _address_Addr2;
            }
            set
            {
                _address_Addr2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Address_Addr3" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>

        [DataMember]
        public string Address_Addr3
        {
            get
            {
                return _address_Addr3;
            }
            set
            {
                _address_Addr3 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Address_Addr4" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>

        [DataMember]
        public string Address_Addr4
        {
            get
            {
                return _address_Addr4;
            }
            set
            {
                _address_Addr4 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Address_Addr5" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Address_Addr5
        {
            get
            {
                return _address_Addr5;
            }
            set
            {
                _address_Addr5 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Address_City" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Address_City
        {
            get
            {
                return _address_City;
            }
            set
            {
                _address_City = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Address_State" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Address_State
        {
            get
            {
                return _address_State;
            }
            set
            {
                _address_State = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Address_PostalCode" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Address_PostalCode
        {
            get
            {
                return _address_PostalCode;
            }
            set
            {
                _address_PostalCode = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Address_Country" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Address_Country
        {
            get
            {
                return _address_Country;
            }
            set
            {
                _address_Country = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Address_Note" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Address_Note
        {
            get
            {
                return _address_Note;
            }
            set
            {
                _address_Note = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AddressBlock_Addr1" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string AddressBlock_Addr1
        {
            get
            {
                return _addressBlock_Addr1;
            }
            set
            {
                _addressBlock_Addr1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AddressBlock_Addr2" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string AddressBlock_Addr2
        {
            get
            {
                return _addressBlock_Addr2;
            }
            set
            {
                _addressBlock_Addr2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AddressBlock_Addr3" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string AddressBlock_Addr3
        {
            get
            {
                return _addressBlock_Addr3;
            }
            set
            {
                _addressBlock_Addr3 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AddressBlock_Addr4" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string AddressBlock_Addr4
        {
            get
            {
                return _addressBlock_Addr4;
            }
            set
            {
                _addressBlock_Addr4 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AddressBlock_Addr5" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string AddressBlock_Addr5
        {
            get
            {
                return _addressBlock_Addr5;
            }
            set
            {
                _addressBlock_Addr5 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "PaymentMethodRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string PaymentMethodRef_ListID
        {
            get
            {
                return _paymentMethodRef_ListID;
            }
            set
            {
                _paymentMethodRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "PaymentMethodRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string PaymentMethodRef_FullName
        {
            get
            {
                return _paymentMethodRef_FullName;
            }
            set
            {
                _paymentMethodRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Memo" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Memo
        {
            get
            {
                return _memo;
            }
            set
            {
                _memo = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CreditCardTxnInfo" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CreditCardTxnInfo
        {
            get
            {
                return _creditCardTxnInfo;
            }
            set
            {
                _creditCardTxnInfo = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField1" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField1
        {
            get
            {
                return _customField1;
            }
            set
            {
                _customField1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField2" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField2
        {
            get
            {
                return _customField2;
            }
            set
            {
                _customField2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField3" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField3
        {
            get
            {
                return _customField3;
            }
            set
            {
                _customField3 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField4" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField4
        {
            get
            {
                return _customField4;
            }
            set
            {
                _customField4 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField5" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField5
        {
            get
            {
                return _customField5;
            }
            set
            {
                _customField5 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField6" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField6
        {
            get
            {
                return _customField6;
            }
            set
            {
                _customField6 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField7" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField7
        {
            get
            {
                return _customField7;
            }
            set
            {
                _customField7 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField8" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField8
        {
            get
            {
                return _customField8;
            }
            set
            {
                _customField8 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField9" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField9
        {
            get
            {
                return _customField9;
            }
            set
            {
                _customField9 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField10" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField10
        {
            get
            {
                return _customField10;
            }
            set
            {
                _customField10 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Status" field. Length must be between 0 and 10 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        #endregion
    }
}
