//
// Class	:	ECFO_SALESTAX_ACCOUNT_LKUP.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization;
using QBSynDal.List;

namespace QBSynDal
{
    /// <summary>
    /// Class for the properties of the object
    /// </summary>
    public class ECFO_SALESTAX_ACCOUNT_LKUP_Fields
    {
        public const string SalesTaxRef_ListID = "SalesTaxRef_ListID";
        public const string AccountRef_ListID = "AccountRef_ListID";
        public const string AccountType = "AccountType";
    }

    /// <summary>
    /// Model class for the "account" table.
    /// </summary>
    [DataContract(Name = "ServiceECFO_SALESTAX_ACCOUNT_LKUP")]
    public class ECFO_SALESTAX_ACCOUNT_LKUP : IModel
    {
        #region Class Level Variables

        private string _SalesTaxRef_ListID = null;
        private string _AccountRef_ListID = null;
        private string _AccountType = null;

        #endregion

        #region Properties

        /// <summary>
        /// This property is mapped to the "SalesTaxRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SalesTaxRef_ListID
        {
            get
            {
                return _SalesTaxRef_ListID;
            }
            set
            {
                _SalesTaxRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AccountRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string AccountRef_ListID
        {
            get
            {
                return _AccountRef_ListID;
            }
            set
            {
                _AccountRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AccountType" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string AccountType
        {
            get
            {
                return _AccountType;
            }
            set
            {
                _AccountType = value;
            }
        }

        #endregion
    }
}
