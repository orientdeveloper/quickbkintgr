//
// Class	:	Item.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:44 AM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization;

namespace QBSynDal
{
    /// <summary>
    /// Class for the properties of the object
    /// </summary>
    public class ItemFields
    {
        public const string ListID = "ListID";
        public const string FullName = "FullName";
        public const string TableName = "TableName";
    }
    /// <summary>
    /// Datacontract class for the "items" table.
    /// </summary>
    [DataContract(Name = "ServiceItem")]
    public class Item : IModel
    {
        #region Class Level Variables
        private string _listID = null;
        private string _fullName = null;
        private string _tableName = null;
        #endregion

        #region Properties

        /// <summary>
        /// This property is mapped to the "ListID" field. Length must be between 0 and 200 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ListID
        {
            get
            {
                return _listID;
            }
            set
            {
                _listID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "FullName" field. Length must be between 0 and 200 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string FullName
        {
            get
            {
                return _fullName;
            }
            set
            {
                _fullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TableName" field. Length must be between 0 and 200 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TableName
        {
            get
            {
                return _tableName;
            }
            set
            {
                _tableName = value;
            }
        }

        #endregion
    }
}
