//
// Class	:	Customer.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:44 AM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization;

namespace QBSynDal
{

    /// <summary>
    /// Class for the properties of the object
    /// </summary>
    public class CustomerFields
    {
        public const string ListID = "ListID";
        public const string TimeCreated = "TimeCreated";
        public const string TimeModified = "TimeModified";
        public const string EditSequence = "EditSequence";
        public const string Name = "Name";
        public const string FullName = "FullName";
        public const string IsActive = "IsActive";
        public const string ParentRef_ListID = "ParentRef_ListID";
        public const string ParentRef_FullName = "ParentRef_FullName";
        public const string Sublevel = "Sublevel";
        public const string CompanyName = "CompanyName";
        public const string Salutation = "Salutation";
        public const string FirstName = "FirstName";
        public const string MiddleName = "MiddleName";
        public const string LastName = "LastName";
        public const string Suffix = "Suffix";
        public const string BillAddress_Addr1 = "BillAddress_Addr1";
        public const string BillAddress_Addr2 = "BillAddress_Addr2";
        public const string BillAddress_Addr3 = "BillAddress_Addr3";
        public const string BillAddress_Addr4 = "BillAddress_Addr4";
        public const string BillAddress_Addr5 = "BillAddress_Addr5";
        public const string BillAddress_City = "BillAddress_City";
        public const string BillAddress_State = "BillAddress_State";
        public const string BillAddress_PostalCode = "BillAddress_PostalCode";
        public const string BillAddress_Country = "BillAddress_Country";
        public const string BillAddress_Note = "BillAddress_Note";
        public const string ShipAddress_Addr1 = "ShipAddress_Addr1";
        public const string ShipAddress_Addr2 = "ShipAddress_Addr2";
        public const string ShipAddress_Addr3 = "ShipAddress_Addr3";
        public const string ShipAddress_Addr4 = "ShipAddress_Addr4";
        public const string ShipAddress_Addr5 = "ShipAddress_Addr5";
        public const string ShipAddress_City = "ShipAddress_City";
        public const string ShipAddress_State = "ShipAddress_State";
        public const string ShipAddress_PostalCode = "ShipAddress_PostalCode";
        public const string ShipAddress_Country = "ShipAddress_Country";
        public const string ShipAddress_Note = "ShipAddress_Note";
        public const string PrintAs = "PrintAs";
        public const string Phone = "Phone";
        public const string Mobile = "Mobile";
        public const string Pager = "Pager";
        public const string AltPhone = "AltPhone";
        public const string Fax = "Fax";
        public const string Email = "Email";
        public const string Contact = "Contact";
        public const string AltContact = "AltContact";
        public const string CustomerTypeRef_ListID = "CustomerTypeRef_ListID";
        public const string CustomerTypeRef_FullName = "CustomerTypeRef_FullName";
        public const string TermsRef_ListID = "TermsRef_ListID";
        public const string TermsRef_FullName = "TermsRef_FullName";
        public const string SalesRepRef_ListID = "SalesRepRef_ListID";
        public const string SalesRepRef_FullName = "SalesRepRef_FullName";
        public const string Balance = "Balance";
        public const string TotalBalance = "TotalBalance";
        public const string SalesTaxCodeRef_ListID = "SalesTaxCodeRef_ListID";
        public const string SalesTaxCodeRef_FullName = "SalesTaxCodeRef_FullName";
        public const string ItemSalesTaxRef_ListID = "ItemSalesTaxRef_ListID";
        public const string ItemSalesTaxRef_FullName = "ItemSalesTaxRef_FullName";
        public const string SalesTaxCountry = "SalesTaxCountry";
        public const string ResaleNumber = "ResaleNumber";
        public const string AccountNumber = "AccountNumber";
        public const string CreditLimit = "CreditLimit";
        public const string PreferredPaymentMethodRef_ListID = "PreferredPaymentMethodRef_ListID";
        public const string PreferredPaymentMethodRef_FullName = "PreferredPaymentMethodRef_FullName";
        public const string CreditCardNumber = "CreditCardNumber";
        public const string ExpirationMonth = "ExpirationMonth";
        public const string ExpirationYear = "ExpirationYear";
        public const string NameOnCard = "NameOnCard";
        public const string CreditCardAddress = "CreditCardAddress";
        public const string CreditCardPostalCode = "CreditCardPostalCode";
        public const string JobStatus = "JobStatus";
        public const string JobStartDate = "JobStartDate";
        public const string JobProjectedEndDate = "JobProjectedEndDate";
        public const string JobEndDate = "JobEndDate";
        public const string JobDesc = "JobDesc";
        public const string JobTypeRef_ListID = "JobTypeRef_ListID";
        public const string JobTypeRef_FullName = "JobTypeRef_FullName";
        public const string Notes = "Notes";
        public const string PriceLevelRef_ListID = "PriceLevelRef_ListID";
        public const string PriceLevelRef_FullName = "PriceLevelRef_FullName";
        public const string TaxRegistrationNumber = "TaxRegistrationNumber";
        public const string CurrencyRef_ListID = "CurrencyRef_ListID";
        public const string CurrencyRef_FullName = "CurrencyRef_FullName";
        public const string IsStatementWithParent = "IsStatementWithParent";
        public const string DeliveryMethod = "DeliveryMethod";
        public const string CustomField1 = "CustomField1";
        public const string CustomField2 = "CustomField2";
        public const string CustomField3 = "CustomField3";
        public const string CustomField4 = "CustomField4";
        public const string CustomField5 = "CustomField5";
        public const string CustomField6 = "CustomField6";
        public const string CustomField7 = "CustomField7";
        public const string CustomField8 = "CustomField8";
        public const string CustomField9 = "CustomField9";
        public const string CustomField10 = "CustomField10";
        public const string Status = "Status";
    }
    /// <summary>
    /// Model class for the "customer" table.
    /// </summary>
    [DataContract(Name = "ServiceCustomer")]
    public class Customer : IModel
    {
        #region Class Level Variables

        private string _listID = null;
        private string _timeCreated = null;
        private string _timeModified = null;
        private string _editSequence = null;
        private string _name = null;
        private string _fullName = null;
        private bool? _isActive = null;
        private string _parentRef_ListID = null;
        private string _parentRef_FullName = null;
        private int? _sublevel = null;
        private string _companyName = null;
        private string _salutation = null;
        private string _firstName = null;
        private string _middleName = null;
        private string _lastName = null;
        private string _suffix = null;
        private string _billAddress_Addr1 = null;
        private string _billAddress_Addr2 = null;
        private string _billAddress_Addr3 = null;
        private string _billAddress_Addr4 = null;
        private string _billAddress_Addr5 = null;
        private string _billAddress_City = null;
        private string _billAddress_State = null;
        private string _billAddress_PostalCode = null;
        private string _billAddress_Country = null;
        private string _billAddress_Note = null;
        private string _shipAddress_Addr1 = null;
        private string _shipAddress_Addr2 = null;
        private string _shipAddress_Addr3 = null;
        private string _shipAddress_Addr4 = null;
        private string _shipAddress_Addr5 = null;
        private string _shipAddress_City = null;
        private string _shipAddress_State = null;
        private string _shipAddress_PostalCode = null;
        private string _shipAddress_Country = null;
        private string _shipAddress_Note = null;
        private string _printAs = null;
        private string _phone = null;
        private string _mobile = null;
        private string _pager = null;
        private string _altPhone = null;
        private string _fax = null;
        private string _email = null;
        private string _contact = null;
        private string _altContact = null;
        private string _customerTypeRef_ListID = null;
        private string _customerTypeRef_FullName = null;
        private string _termsRef_ListID = null;
        private string _termsRef_FullName = null;
        private string _salesRepRef_ListID = null;
        private string _salesRepRef_FullName = null;
        private double? _balance = null;
        private double? _totalBalance = null;
        private string _salesTaxCodeRef_ListID = null;
        private string _salesTaxCodeRef_FullName = null;
        private string _itemSalesTaxRef_ListID = null;
        private string _itemSalesTaxRef_FullName = null;
        private string _salesTaxCountry = null;
        private string _resaleNumber = null;
        private string _accountNumber = null;
        private double? _creditLimit = null;
        private string _preferredPaymentMethodRef_ListID = null;
        private string _preferredPaymentMethodRef_FullName = null;
        private string _creditCardNumber = null;
        private int? _expirationMonth = null;
        private int? _expirationYear = null;
        private string _nameOnCard = null;
        private string _creditCardAddress = null;
        private string _creditCardPostalCode = null;
        private string _jobStatus = null;
        private DateTime? _jobStartDate = null;
        private DateTime? _jobProjectedEndDate = null;
        private DateTime? _jobEndDate = null;
        private string _jobDesc = null;
        private string _jobTypeRef_ListID = null;
        private string _jobTypeRef_FullName = null;
        private string _notes = null;
        private string _priceLevelRef_ListID = null;
        private string _priceLevelRef_FullName = null;
        private string _taxRegistrationNumber = null;
        private string _currencyRef_ListID = null;
        private string _currencyRef_FullName = null;
        private bool? _isStatementWithParent = null;
        private string _deliveryMethod = null;
        private string _customField1 = null;
        private string _customField2 = null;
        private string _customField3 = null;
        private string _customField4 = null;
        private string _customField5 = null;
        private string _customField6 = null;
        private string _customField7 = null;
        private string _customField8 = null;
        private string _customField9 = null;
        private string _customField10 = null;
        private string _status = null;

        #endregion

        #region Properties

        /// <summary>
        /// This property is mapped to the "ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ListID
        {
            get
            {
                return _listID;
            }
            set
            {
                _listID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeCreated" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TimeCreated
        {
            get
            {
                return _timeCreated;
            }
            set
            {
                _timeCreated = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeModified" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TimeModified
        {
            get
            {
                return _timeModified;
            }
            set
            {
                _timeModified = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "EditSequence" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string EditSequence
        {
            get
            {
                return _editSequence;
            }
            set
            {
                _editSequence = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Name" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string FullName
        {
            get
            {
                return _fullName;
            }
            set
            {
                _fullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsActive" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public bool? IsActive
        {
            get
            {
                return _isActive;
            }
            set
            {
                _isActive = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ParentRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ParentRef_ListID
        {
            get
            {
                return _parentRef_ListID;
            }
            set
            {
                _parentRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ParentRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ParentRef_FullName
        {
            get
            {
                return _parentRef_FullName;
            }
            set
            {
                _parentRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Sublevel" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public int? Sublevel
        {
            get
            {
                return _sublevel;
            }
            set
            {
                _sublevel = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CompanyName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CompanyName
        {
            get
            {
                return _companyName;
            }
            set
            {
                _companyName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Salutation" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Salutation
        {
            get
            {
                return _salutation;
            }
            set
            {
                _salutation = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "FirstName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string FirstName
        {
            get
            {
                return _firstName;
            }
            set
            {
                _firstName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "MiddleName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string MiddleName
        {
            get
            {
                return _middleName;
            }
            set
            {
                _middleName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "LastName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string LastName
        {
            get
            {
                return _lastName;
            }
            set
            {
                _lastName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Suffix" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Suffix
        {
            get
            {
                return _suffix;
            }
            set
            {
                _suffix = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BillAddress_Addr1" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string BillAddress_Addr1
        {
            get
            {
                return _billAddress_Addr1;
            }
            set
            {
                _billAddress_Addr1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BillAddress_Addr2" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string BillAddress_Addr2
        {
            get
            {
                return _billAddress_Addr2;
            }
            set
            {
                _billAddress_Addr2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BillAddress_Addr3" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string BillAddress_Addr3
        {
            get
            {
                return _billAddress_Addr3;
            }
            set
            {
                _billAddress_Addr3 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BillAddress_Addr4" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string BillAddress_Addr4
        {
            get
            {
                return _billAddress_Addr4;
            }
            set
            {
                _billAddress_Addr4 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BillAddress_Addr5" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string BillAddress_Addr5
        {
            get
            {
                return _billAddress_Addr5;
            }
            set
            {
                _billAddress_Addr5 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BillAddress_City" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string BillAddress_City
        {
            get
            {
                return _billAddress_City;
            }
            set
            {
                _billAddress_City = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BillAddress_State" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string BillAddress_State
        {
            get
            {
                return _billAddress_State;
            }
            set
            {

                _billAddress_State = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BillAddress_PostalCode" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string BillAddress_PostalCode
        {
            get
            {
                return _billAddress_PostalCode;
            }
            set
            {

                _billAddress_PostalCode = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BillAddress_Country" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string BillAddress_Country
        {
            get
            {
                return _billAddress_Country;
            }
            set
            {
                _billAddress_Country = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BillAddress_Note" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string BillAddress_Note
        {
            get
            {
                return _billAddress_Note;
            }
            set
            {
                _billAddress_Note = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_Addr1" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipAddress_Addr1
        {
            get
            {
                return _shipAddress_Addr1;
            }
            set
            {
                _shipAddress_Addr1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_Addr2" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipAddress_Addr2
        {
            get
            {
                return _shipAddress_Addr2;
            }
            set
            {
                _shipAddress_Addr2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_Addr3" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipAddress_Addr3
        {
            get
            {
                return _shipAddress_Addr3;
            }
            set
            {
                _shipAddress_Addr3 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_Addr4" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipAddress_Addr4
        {
            get
            {
                return _shipAddress_Addr4;
            }
            set
            {
                _shipAddress_Addr4 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_Addr5" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>

        [DataMember]
        public string ShipAddress_Addr5
        {
            get
            {
                return _shipAddress_Addr5;
            }
            set
            {
                _shipAddress_Addr5 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_City" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipAddress_City
        {
            get
            {
                return _shipAddress_City;
            }
            set
            {
                _shipAddress_City = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_State" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipAddress_State
        {
            get
            {
                return _shipAddress_State;
            }
            set
            {
                _shipAddress_State = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_PostalCode" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipAddress_PostalCode
        {
            get
            {
                return _shipAddress_PostalCode;
            }
            set
            {
                _shipAddress_PostalCode = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_Country" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipAddress_Country
        {
            get
            {
                return _shipAddress_Country;
            }
            set
            {
                _shipAddress_Country = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_Note" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipAddress_Note
        {
            get
            {
                return _shipAddress_Note;
            }
            set
            {
                _shipAddress_Note = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "PrintAs" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string PrintAs
        {
            get
            {
                return _printAs;
            }
            set
            {
                _printAs = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Phone" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Phone
        {
            get
            {
                return _phone;
            }
            set
            {
                _phone = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Mobile" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Mobile
        {
            get
            {
                return _mobile;
            }
            set
            {
                _mobile = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Pager" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Pager
        {
            get
            {
                return _pager;
            }
            set
            {
                _pager = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AltPhone" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string AltPhone
        {
            get
            {
                return _altPhone;
            }
            set
            {
                _altPhone = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Fax" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Fax
        {
            get
            {
                return _fax;
            }
            set
            {
                _fax = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Email" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                _email = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Contact" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Contact
        {
            get
            {
                return _contact;
            }
            set
            {
                _contact = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AltContact" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string AltContact
        {
            get
            {
                return _altContact;
            }
            set
            {
                _altContact = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomerTypeRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomerTypeRef_ListID
        {
            get
            {
                return _customerTypeRef_ListID;
            }
            set
            {
                _customerTypeRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomerTypeRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomerTypeRef_FullName
        {
            get
            {
                return _customerTypeRef_FullName;
            }
            set
            {
                _customerTypeRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TermsRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TermsRef_ListID
        {
            get
            {
                return _termsRef_ListID;
            }
            set
            {
                _termsRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TermsRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TermsRef_FullName
        {
            get
            {
                return _termsRef_FullName;
            }
            set
            {
                _termsRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SalesRepRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SalesRepRef_ListID
        {
            get
            {
                return _salesRepRef_ListID;
            }
            set
            {
                _salesRepRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SalesRepRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SalesRepRef_FullName
        {
            get
            {
                return _salesRepRef_FullName;
            }
            set
            {
                _salesRepRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Balance" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? Balance
        {
            get
            {
                return _balance;
            }
            set
            {
                _balance = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TotalBalance" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? TotalBalance
        {
            get
            {
                return _totalBalance;
            }
            set
            {
                _totalBalance = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SalesTaxCodeRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SalesTaxCodeRef_ListID
        {
            get
            {
                return _salesTaxCodeRef_ListID;
            }
            set
            {
                _salesTaxCodeRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SalesTaxCodeRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SalesTaxCodeRef_FullName
        {
            get
            {
                return _salesTaxCodeRef_FullName;
            }
            set
            {
                _salesTaxCodeRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ItemSalesTaxRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ItemSalesTaxRef_ListID
        {
            get
            {
                return _itemSalesTaxRef_ListID;
            }
            set
            {
                _itemSalesTaxRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ItemSalesTaxRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ItemSalesTaxRef_FullName
        {
            get
            {
                return _itemSalesTaxRef_FullName;
            }
            set
            {
                _itemSalesTaxRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SalesTaxCountry" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SalesTaxCountry
        {
            get
            {
                return _salesTaxCountry;
            }
            set
            {
                _salesTaxCountry = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ResaleNumber" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ResaleNumber
        {
            get
            {
                return _resaleNumber;
            }
            set
            {
                _resaleNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AccountNumber" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string AccountNumber
        {
            get
            {
                return _accountNumber;
            }
            set
            {
                _accountNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CreditLimit" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? CreditLimit
        {
            get
            {
                return _creditLimit;
            }
            set
            {
                _creditLimit = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "PreferredPaymentMethodRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string PreferredPaymentMethodRef_ListID
        {
            get
            {
                return _preferredPaymentMethodRef_ListID;
            }
            set
            {
                _preferredPaymentMethodRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "PreferredPaymentMethodRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string PreferredPaymentMethodRef_FullName
        {
            get
            {
                return _preferredPaymentMethodRef_FullName;
            }
            set
            {
                _preferredPaymentMethodRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CreditCardNumber" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CreditCardNumber
        {
            get
            {
                return _creditCardNumber;
            }
            set
            {
                _creditCardNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ExpirationMonth" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public int? ExpirationMonth
        {
            get
            {
                return _expirationMonth;
            }
            set
            {
                _expirationMonth = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ExpirationYear" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public int? ExpirationYear
        {
            get
            {
                return _expirationYear;
            }
            set
            {
                _expirationYear = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "NameOnCard" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string NameOnCard
        {
            get
            {
                return _nameOnCard;
            }
            set
            {
                _nameOnCard = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CreditCardAddress" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CreditCardAddress
        {
            get
            {
                return _creditCardAddress;
            }
            set
            {
                _creditCardAddress = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CreditCardPostalCode" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CreditCardPostalCode
        {
            get
            {
                return _creditCardPostalCode;
            }
            set
            {
                _creditCardPostalCode = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "JobStatus" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string JobStatus
        {
            get
            {
                return _jobStatus;
            }
            set
            {
                _jobStatus = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "JobStartDate" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public DateTime? JobStartDate
        {
            get
            {
                return _jobStartDate;
            }
            set
            {
                _jobStartDate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "JobProjectedEndDate" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public DateTime? JobProjectedEndDate
        {
            get
            {
                return _jobProjectedEndDate;
            }
            set
            {
                _jobProjectedEndDate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "JobEndDate" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public DateTime? JobEndDate
        {
            get
            {
                return _jobEndDate;
            }
            set
            {
                _jobEndDate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "JobDesc" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]

        public string JobDesc
        {
            get
            {
                return _jobDesc;
            }
            set
            {
                _jobDesc = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "JobTypeRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string JobTypeRef_ListID
        {
            get
            {
                return _jobTypeRef_ListID;
            }
            set
            {
                _jobTypeRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "JobTypeRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string JobTypeRef_FullName
        {
            get
            {
                return _jobTypeRef_FullName;
            }
            set
            {
                _jobTypeRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Notes" field. Length must be between 0 and 2000 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Notes
        {
            get
            {
                return _notes;
            }
            set
            {
                _notes = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "PriceLevelRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string PriceLevelRef_ListID
        {
            get
            {
                return _priceLevelRef_ListID;
            }
            set
            {
                _priceLevelRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "PriceLevelRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string PriceLevelRef_FullName
        {
            get
            {
                return _priceLevelRef_FullName;
            }
            set
            {
                _priceLevelRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TaxRegistrationNumber" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TaxRegistrationNumber
        {
            get
            {
                return _taxRegistrationNumber;
            }
            set
            {
                _taxRegistrationNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CurrencyRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CurrencyRef_ListID
        {
            get
            {
                return _currencyRef_ListID;
            }
            set
            {
                _currencyRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CurrencyRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CurrencyRef_FullName
        {
            get
            {
                return _currencyRef_FullName;
            }
            set
            {
                _currencyRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsStatementWithParent" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public bool? IsStatementWithParent
        {
            get
            {
                return _isStatementWithParent;
            }
            set
            {
                _isStatementWithParent = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "DeliveryMethod" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string DeliveryMethod
        {
            get
            {
                return _deliveryMethod;
            }
            set
            {
                _deliveryMethod = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField1" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField1
        {
            get
            {
                return _customField1;
            }
            set
            {
                _customField1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField2" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField2
        {
            get
            {
                return _customField2;
            }
            set
            {
                _customField2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField3" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField3
        {
            get
            {
                return _customField3;
            }
            set
            {
                _customField3 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField4" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField4
        {
            get
            {
                return _customField4;
            }
            set
            {
                _customField4 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField5" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField5
        {
            get
            {
                return _customField5;
            }
            set
            {
                _customField5 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField6" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField6
        {
            get
            {
                return _customField6;
            }
            set
            {
                _customField6 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField7" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField7
        {
            get
            {
                return _customField7;
            }
            set
            {
                _customField7 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField8" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField8
        {
            get
            {
                return _customField8;
            }
            set
            {
                _customField8 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField9" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField9
        {
            get
            {
                return _customField9;
            }
            set
            {
                _customField9 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField10" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField10
        {
            get
            {
                return _customField10;
            }
            set
            {
                _customField10 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Status" field. Length must be between 0 and 10 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        #endregion
    }
}
