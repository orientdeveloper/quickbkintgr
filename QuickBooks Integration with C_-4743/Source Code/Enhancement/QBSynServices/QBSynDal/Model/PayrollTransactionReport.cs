//
// Class	:	Account.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Data.SqlClient;
using System.Runtime.Serialization;
using QBSynDal;

namespace QBSynDal
{
    /// <summary>
    /// Class for the properties of the object
    /// </summary>
    public class PayrollTransactionReportFields
    {
        public const string Date = "Date";
        public const string TxnNumber = "TxnNumber";
        public const string RefNumber = "RefNumber";
        public const string TxnType = "TxnType";
        public const string Name = "Name";
        public const string SourceName = "SourceName";
        public const string PayrollItem = "PayrollItem";
        public const string WageBase = "WageBase";
        public const string Account = "Account";
        public const string Class_ListID = "Class_ListID";
        public const string Class = "Class";
        public const string BillingStatus = "BillingStatus";
        public const string SplitAccount_ListID = "SplitAccount_ListID";
        public const string SplitAccount = "SplitAccount";
        public const string Quantity = "Quantity";
        public const string Amount = "Amount";
        public const string PayPeriodBeginDate = "PayPeriodBeginDate";
        public const string PayPeriodEndDate = "PayPeriodEndDate";
        public const string Employee_ListID = "Employee_ListID";
        public const string PayrollItem_ListID = "Payrollitem_ListID";
        public const string Account_ListID = "Account_ListID";
        public const string ModifiedTime = "ModifiedTime";
        public const string AccountType = "AccountType";
    }

    /// <summary>
    /// Model class for the "PayrollTransactionReport" table.
    /// </summary>
    [DataContract(Name = "ServicePayrollTransactionReport")]
    public class PayrollTransactionReport : IModel
    {
        #region Class Level Variables
        private DateTime? _Date = null;
        private int? _txnNumber = null;
        private string _refNumber = null;
        private string _txnType = null;
        private string _Name = null;
        private string _SourceName = null;
        private string _PayrollItem = null;
        private double? _WageBase = null;
        private string _Account = null;
        private string _class_ListID = null;
        private string _class = null;
        private string _BillingStatus = null;
        private string _SplitAccount_ListID = null;
        private string _SplitAccount = null;
        private string _Qty = null;
        private double? _amount = null;
        private string _Employee_ListID = null;
        private string _PayrollItem_ListID = null;
        private string _Account_ListID = null;
        private DateTime? _modifiedTime = null;
        private string _accountType = null;
        #endregion

        #region Properties
        /// <summary>
        /// This property is mapped to the "TxnDate" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public DateTime? Date
        {
            get
            {
                return _Date;
            }
            set
            {
                _Date = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TxnDate" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public int? TxnNumber
        {
            get
            {
                return _txnNumber;
            }
            set
            {
                _txnNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "RefNumber" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string RefNumber
        {
            get
            {
                return _refNumber;
            }
            set
            {
                _refNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TxnType" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TxnType
        {
            get
            {
                return _txnType;
            }
            set
            {
                _txnType = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField2" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SourceName" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SourceName
        {
            get
            {
                return _SourceName;
            }
            set
            {
                _SourceName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "PayrollItem" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string PayrollItem
        {
            get
            {
                return _PayrollItem;
            }
            set
            {
                _PayrollItem = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "WageBase" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? WageBase
        {
            get
            {
                return _WageBase;
            }
            set
            {
                _WageBase = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Account" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Account
        {
            get
            {
                return _Account;
            }
            set
            {
                _Account = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Class_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Class_ListID
        {
            get
            {
                return _class_ListID;
            }
            set
            {
                _class_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Class" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Class
        {
            get
            {
                return _class;
            }
            set
            {
                _class = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Billing Status" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string BillingStatus
        {
            get
            {
                return _BillingStatus;
            }
            set
            {
                _BillingStatus = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Split" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SplitAccount
        {
            get
            {
                return _SplitAccount;
            }
            set
            {
                _SplitAccount = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Split" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SplitAccount_ListID
        {
            get
            {
                return _SplitAccount_ListID;
            }
            set
            {
                _SplitAccount_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Billing Status" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Quanity
        {
            get
            {
                return _Qty;
            }
            set
            {
                _Qty = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Amount" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? Amount
        {
            get
            {
                return _amount;
            }
            set
            {
                _amount = value;
            }
        }

        [DataMember]
        public string Employee_ListID
        {
            get
            {
                return _Employee_ListID;
            }
            set
            {
                _Employee_ListID = value;
            }
        }

        [DataMember]
        public string PayrollItem_ListID
        {
            get
            {
                return _PayrollItem_ListID;
            }
            set
            {
                _PayrollItem_ListID = value;
            }
        }

        [DataMember]
        public string Account_ListID
        {
            get
            {
                return _Account_ListID;
            }
            set
            {
                _Account_ListID = value;
            }
        }

        [DataMember]
        public DateTime? ModifiedTime
        {
            get
            {
                return _modifiedTime;
            }
            set
            {
                _modifiedTime = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AccounType" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string AccounType
        {
            get
            {
                return _accountType;
            }
            set
            {
                _accountType = value;
            }
        }
        #endregion
    }
}
