//
// Class	:	Txnexpenselinedetail.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:43 AM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization;

namespace QBSynDal
{
    /// <summary>
    /// Class for the properties of the object
    /// </summary>
    public class TxnexpenselinedetailFields
    {
        public const string TxnLineID = "TxnLineID";
        public const string AccountRef_ListID = "AccountRef_ListID";
        public const string AccountRef_FullName = "AccountRef_FullName";
        public const string Amount = "Amount";
        public const string Memo = "Memo";
        public const string CustomerRef_ListID = "CustomerRef_ListID";
        public const string CustomerRef_FullName = "CustomerRef_FullName";
        public const string ClassRef_ListID = "ClassRef_ListID";
        public const string ClassRef_FullName = "ClassRef_FullName";
        public const string SalesTaxCodeRef_ListID = "SalesTaxCodeRef_ListID";
        public const string SalesTaxCodeRef_FullName = "SalesTaxCodeRef_FullName";
        public const string BillableStatus = "BillableStatus";
        public const string CustomField1 = "CustomField1";
        public const string CustomField2 = "CustomField2";
        public const string CustomField3 = "CustomField3";
        public const string CustomField4 = "CustomField4";
        public const string CustomField5 = "CustomField5";
        public const string CustomField6 = "CustomField6";
        public const string CustomField7 = "CustomField7";
        public const string CustomField8 = "CustomField8";
        public const string CustomField9 = "CustomField9";
        public const string CustomField10 = "CustomField10";
        public const string Idkey = "IDKEY";
        public const string GroupIDKEY = "GroupIDKEY";

    }
    /// <summary>
    /// Model class for the "txnexpenselinedetail" table.
    /// </summary>
    [DataContract(Name = "ServiceTxnexpenselinedetail")]
    public class Txnexpenselinedetail : IModel
    {
        #region Class Level Variables

        private string _txnLineID = null;
        private string _accountRef_ListID = null;
        private string _accountRef_FullName = null;
        private double? _amount = null;
        private string _memo = null;
        private string _customerRef_ListID = null;
        private string _customerRef_FullName = null;
        private string _classRef_ListID = null;
        private string _classRef_FullName = null;
        private string _salesTaxCodeRef_ListID = null;
        private string _salesTaxCodeRef_FullName = null;
        private string _billableStatus = null;
        private string _customField1 = null;
        private string _customField2 = null;
        private string _customField3 = null;
        private string _customField4 = null;
        private string _customField5 = null;
        private string _customField6 = null;
        private string _customField7 = null;
        private string _customField8 = null;
        private string _customField9 = null;
        private string _customField10 = null;
        private string _idkey = null;
        private string _groupIDKEY = null;
        #endregion

        #region Properties

        /// <summary>
        /// This property is mapped to the "TxnLineID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TxnLineID
        {
            get
            {
                return _txnLineID;
            }
            set
            {
                _txnLineID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AccountRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string AccountRef_ListID
        {
            get
            {
                return _accountRef_ListID;
            }
            set
            {
                _accountRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AccountRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string AccountRef_FullName
        {
            get
            {
                return _accountRef_FullName;
            }
            set
            {
                _accountRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Amount" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? Amount
        {
            get
            {
                return _amount;
            }
            set
            {
                _amount = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Memo" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Memo
        {
            get
            {
                return _memo;
            }
            set
            {
                    _memo = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomerRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomerRef_ListID
        {
            get
            {
                return _customerRef_ListID;
            }
            set
            {
                _customerRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomerRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomerRef_FullName
        {
            get
            {
                return _customerRef_FullName;
            }
            set
            {
                _customerRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ClassRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ClassRef_ListID
        {
            get
            {
                return _classRef_ListID;
            }
            set
            {
                _classRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ClassRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ClassRef_FullName
        {
            get
            {
                return _classRef_FullName;
            }
            set
            {
                _classRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SalesTaxCodeRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SalesTaxCodeRef_ListID
        {
            get
            {
                return _salesTaxCodeRef_ListID;
            }
            set
            {
                _salesTaxCodeRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SalesTaxCodeRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SalesTaxCodeRef_FullName
        {
            get
            {
                return _salesTaxCodeRef_FullName;
            }
            set
            {
                _salesTaxCodeRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BillableStatus" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string BillableStatus
        {
            get
            {
                return _billableStatus;
            }
            set
            {
                _billableStatus = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField1" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField1
        {
            get
            {
                return _customField1;
            }
            set
            {
                _customField1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField2" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>

        [DataMember]
        public string CustomField2
        {
            get
            {
                return _customField2;
            }
            set
            {
                _customField2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField3" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField3
        {
            get
            {
                return _customField3;
            }
            set
            {
                _customField3 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField4" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField4
        {
            get
            {
                return _customField4;
            }
            set
            {
                _customField4 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField5" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField5
        {
            get
            {
                return _customField5;
            }
            set
            {
                _customField5 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField6" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField6
        {
            get
            {
                return _customField6;
            }
            set
            {
                _customField6 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField7" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField7
        {
            get
            {
                return _customField7;
            }
            set
            {
                _customField7 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField8" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField8
        {
            get
            {
                return _customField8;
            }
            set
            {
                _customField8 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField9" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>

        [DataMember]
        public string CustomField9
        {
            get
            {
                return _customField9;
            }
            set
            {
                _customField9 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField10" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField10
        {
            get
            {
                return _customField10;
            }
            set
            {

                _customField10 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IDKEY" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Idkey
        {
            get
            {
                return _idkey;
            }
            set
            {

                _idkey = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "GroupIDKEY" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string GroupIDKEY
        {
            get
            {
                return _groupIDKEY;
            }
            set
            {
                _groupIDKEY = value;
            }
        }

        #endregion
    }
}
