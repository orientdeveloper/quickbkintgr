﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace QBSynDal.Model
{
    public class QBPushItemReceiptItemLineFields
    {
        public const string TxnLineID = "TxnLineID";
        public const string Amount = "Amount";
        public const string BillableStatus = "BillableStatus";
        public const string ClassRef_ListID = "ClassRef_ListID";
        public const string ClassRef_FullName = "ClassRef_FullName";
        public const string Cost = "Cost";
        public const string CustomerRef_ListID = "CustomerRef_ListID";
        public const string CustomerRef_FullName = "CustomerRef_FullName";
        public const string Desc = "Desc";
        public const string ItemRef_ListID = "ItemRef_ListID";
        public const string ItemRef_FullName = "ItemRef_FullName";
        public const string InventorySiteRef_ListID = "InventorySiteRef_ListID";
        public const string InventorySiteRef_FullName = "InventorySiteRef_FullName";
        public const string OverrideItemAccountRef_ListID = "OverrideUOMSetRef_ListID";
        public const string OverrideItemAccountRef_FullName = "OverrideUOMSetRef_FullName";
        public const string Quantity = "Quantity";
        public const string SalesTaxCodeRef_ListID = "SalesTaxCodeRef_ListID";
        public const string SalesTaxCodeRef_FullName = "SalesTaxCodeRef_FullName";
        public const string UnitOfMeasure = "UnitOfMeasure";

        //Not in main sql server table
        public const string InventorySiteLocationRef_ListID = "InventorySiteLocationRef_ListID";
        public const string InventorySiteLocationRef_FullName = "InventorySiteLocationRef_FullName";
        public const string LinkedTxn_TxnLineID = "LinkedTxn";
        public const string SerialNumber = "SerialNumber";
        public const string LotNumber = "LotNumber";
        public const string TaxAmount = "TaxAmount";
    }

    [DataContract(Name = "ServiceQBPushItemReceiptItemLine")]
    public class QBPushItemReceiptItemLine
    {
        #region Class Level Variables
        private long? _txnLineID = null;
        private double? _amount = null;
        private string _billableStatus = null;
        private string _classRef_ListID = null;
        private string _classRef_FullName = null;
        private double? _cost = null;
        private string _customerRef_ListID = null;
        private string _customerRef_FullName = null;
        private string _desc = null;
        private string _inventorySiteRef_ListID = null;
        private string _inventorySiteRef_FullName = null;
        private string _itemRef_ListID = null;
        private string _itemRef_FullName = null;
        private string _overrideItemAccountRef_ListID = null;
        private string _overrideItemAccountRef_FullName = null;
        private double? _quantity = null;
        private string _salesTaxCodeRef_ListID = null;
        private string _salesTaxCodeRef_FullName = null;
        private string _unitOfMeasure = null;

        private string _inventorySiteLocationRef_ListID = null;
        private string _inventorySiteLocationRef_FullName = null;
        private string _linkedTxn_TxnID = null;
        private string _linkedTxn_TxnLineID = null;
        private string _serialNumber = null;
        private string _lotNumber = null;
        private double? _taxAmount = null;

        #endregion

        #region Properties

        /// <summary>
        /// This property is mapped to the "TxnLineID" field. 
        /// </summary>
        [DataMember]
        public long? TxnLineID
        {
            get
            {
                return _txnLineID;
            }
            set
            {
                _txnLineID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ItemRef_ListID" field.
        /// </summary>
        [DataMember]
        public string ItemRef_ListID
        {
            get
            {
                return _itemRef_ListID;
            }
            set
            {
                _itemRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ItemRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ItemRef_FullName
        {
            get
            {
                return _itemRef_FullName;
            }
            set
            {
                _itemRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "InventorySiteRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string InventorySiteRef_ListID
        {
            get
            {
                return _inventorySiteRef_ListID;
            }
            set
            {
                _inventorySiteRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "InventorySiteRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string InventorySiteRef_FullName
        {
            get
            {
                return _inventorySiteRef_FullName;
            }
            set
            {
                _inventorySiteRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Desc" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Desc
        {
            get
            {
                return _desc;
            }
            set
            {
                _desc = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Quantity" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? Quantity
        {
            get
            {
                return _quantity;
            }
            set
            {
                _quantity = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "UnitOfMeasure" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string UnitOfMeasure
        {
            get
            {
                return _unitOfMeasure;
            }
            set
            {
                _unitOfMeasure = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "OverrideUOMSetRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string OverrideItemAccountRef_ListID
        {
            get
            {
                return _overrideItemAccountRef_ListID;
            }
            set
            {
                _overrideItemAccountRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "OverrideUOMSetRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string OverrideItemAccountRef_FullName
        {
            get
            {
                return _overrideItemAccountRef_FullName;
            }
            set
            {
                _overrideItemAccountRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Cost" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? Cost
        {
            get
            {
                return _cost;
            }
            set
            {
                _cost = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Amount" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? Amount
        {
            get
            {
                return _amount;
            }
            set
            {
                _amount = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomerRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomerRef_ListID
        {
            get
            {
                return _customerRef_ListID;
            }
            set
            {
                _customerRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomerRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomerRef_FullName
        {
            get
            {
                return _customerRef_FullName;
            }
            set
            {
                _customerRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ClassRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ClassRef_ListID
        {
            get
            {
                return _classRef_ListID;
            }
            set
            {

                _classRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ClassRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ClassRef_FullName
        {
            get
            {
                return _classRef_FullName;
            }
            set
            {
                _classRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SalesTaxCodeRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SalesTaxCodeRef_ListID
        {
            get
            {
                return _salesTaxCodeRef_ListID;
            }
            set
            {
                _salesTaxCodeRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SalesTaxCodeRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SalesTaxCodeRef_FullName
        {
            get
            {
                return _salesTaxCodeRef_FullName;
            }
            set
            {
                _salesTaxCodeRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BillableStatus" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string BillableStatus
        {
            get
            {
                return _billableStatus;
            }
            set
            {
                _billableStatus = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TaxAmount" field
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? TaxAmount
        {
            get { return _taxAmount; }
            set { _taxAmount = value; }
        }

        /// <summary>
        /// This property is mapped to the "LotNumber" field
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string LotNumber
        {
            get { return _lotNumber; }
            set { _lotNumber = value; }
        }

        /// <summary>
        /// This property is mapped to the "SerialNumber" field
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SerialNumber
        {
            get { return _serialNumber; }
            set { _serialNumber = value; }
        }

        /// <summary>
        /// This property is mapped to the "LinkedTxn_TxnID" field
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string LinkedTxn_TxnID
        {
            get { return _linkedTxn_TxnID; }
            set { _linkedTxn_TxnID = value; }
        }

        /// <summary>
        /// This property is mapped to the "LinkedTxn_TxnLineID" field
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string LinkedTxn_TxnLineID
        {
            get { return _linkedTxn_TxnLineID; }
            set { _linkedTxn_TxnLineID = value; }
        }

        /// <summary>
        /// This property is mapped to the "InventorySiteLocationRef_FullName" field
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string InventorySiteLocationRef_FullName
        {
            get { return _inventorySiteLocationRef_FullName; }
            set { _inventorySiteLocationRef_FullName = value; }
        }

        /// <summary>
        /// This property is mapped to the "InventorySiteLocationRef_ListID" field
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string InventorySiteLocationRef_ListID
        {
            get { return _inventorySiteLocationRef_ListID; }
            set { _inventorySiteLocationRef_ListID = value; }
        }

        #endregion
    }
}
