//
// Class	:	Pricelevel.cs
// Author	:  	Synapse India © 2013
// Date		:	24/07/2013 10:11:53 AM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization;
using QBSynDal.List;

namespace QBSynDal
{

	/// <summary>
	/// Class for the properties of the object
	/// </summary>
	public class PricelevelFields
	{
		public const string ListID                    = "ListID";
		public const string TimeCreated               = "TimeCreated";
		public const string TimeModified              = "TimeModified";
		public const string EditSequence              = "EditSequence";
		public const string Name                      = "Name";
		public const string IsActive                  = "IsActive";
		public const string PriceLevelType            = "PriceLevelType";
		public const string PriceLevelFixedPercentage = "PriceLevelFixedPercentage";
		public const string CurrencyRef_ListID        = "CurrencyRef_ListID";
		public const string CurrencyRef_FullName      = "CurrencyRef_FullName";
		public const string Status                    = "Status";
	}
	
	/// <summary>
	/// Modal class for the "pricelevel" table.
	/// </summary>
    [DataContract(Name = "ServicePricelevel")]
    public class Pricelevel : IModel
	{
		
		#region Class Level Variables
		
		private string         	_listID                  	= null;
		private string         	_timeCreated             	= null;
		private string         	_timeModified            	= null;
		private string         	_editSequence            	= null;
		private string         	_name                    	= null;
		private bool?          	_isActive                	= null;
		private string         	_priceLevelType          	= null;
		private string         	_priceLevelFixedPercentage	= null;
		private string         	_currencyRef_ListID      	= null;
		private string         	_currencyRef_FullName    	= null;
		private string         	_status                  	= null;
		
		#endregion
		
		#region Properties

		/// <summary>
		/// This property is mapped to the "ListID" field. Length must be between 0 and 255 characters. 
		/// Comment found in the Database for this field: .
		/// </summary>
		[DataMember]
        public string ListID
		{
			get 
			{ 
				return _listID; 
			}
			set 
			{
				_listID = value;
			}
		}

		/// <summary>
		/// This property is mapped to the "TimeCreated" field. Length must be between 0 and 255 characters. 
		/// Comment found in the Database for this field: .
		/// </summary>
        [DataMember]
        public string TimeCreated
		{
			get 
			{ 
				return _timeCreated; 
			}
			set 
			{
				_timeCreated = value;
			}
		}

		/// <summary>
		/// This property is mapped to the "TimeModified" field. Length must be between 0 and 255 characters. 
		/// Comment found in the Database for this field: .
		/// </summary>
        [DataMember]
        public string TimeModified
		{
			get 
			{ 
				return _timeModified; 
			}
			set 
			{
				_timeModified = value;
			}
		}

		/// <summary>
		/// This property is mapped to the "EditSequence" field. Length must be between 0 and 255 characters. 
		/// Comment found in the Database for this field: .
		/// </summary>
        [DataMember]
        public string EditSequence
		{
			get 
			{ 
				return _editSequence; 
			}
			set 
			{
				_editSequence = value;
			}
		}

		/// <summary>
		/// This property is mapped to the "Name" field. Length must be between 0 and 255 characters. 
		/// Comment found in the Database for this field: .
		/// </summary>
        [DataMember]
        public string Name
		{
			get 
			{ 
				return _name; 
			}
			set 
			{
				_name = value;
			}
		}

		/// <summary>
		/// This property is mapped to the "IsActive" field.  
		/// Comment found in the Database for this field: .
		/// </summary>
        [DataMember]
        public bool? IsActive
		{
			get 
			{ 
				return _isActive; 
			}
			set 
			{
				_isActive = value;
			}
		}

		/// <summary>
		/// This property is mapped to the "PriceLevelType" field. Length must be between 0 and 255 characters. 
		/// Comment found in the Database for this field: .
		/// </summary>
        [DataMember]
        public string PriceLevelType
		{
			get 
			{ 
				return _priceLevelType; 
			}
			set 
			{
				_priceLevelType = value;
			}
		}

		/// <summary>
		/// This property is mapped to the "PriceLevelFixedPercentage" field. Length must be between 0 and 255 characters. 
		/// Comment found in the Database for this field: .
		/// </summary>
        [DataMember]
        public string PriceLevelFixedPercentage
		{
			get 
			{ 
				return _priceLevelFixedPercentage; 
			}
			set 
			{
				_priceLevelFixedPercentage = value;
			}
		}

		/// <summary>
		/// This property is mapped to the "CurrencyRef_ListID" field. Length must be between 0 and 255 characters. 
		/// Comment found in the Database for this field: .
		/// </summary>
        [DataMember]
        public string CurrencyRef_ListID
		{
			get 
			{ 
				return _currencyRef_ListID; 
			}
			set 
			{
				_currencyRef_ListID = value;
			}
		}

		/// <summary>
		/// This property is mapped to the "CurrencyRef_FullName" field. Length must be between 0 and 255 characters. 
		/// Comment found in the Database for this field: .
		/// </summary>
        [DataMember]
        public string CurrencyRef_FullName
		{
			get 
			{ 
				return _currencyRef_FullName; 
			}
			set 
			{
				_currencyRef_FullName = value;
			}
		}

		/// <summary>
		/// This property is mapped to the "Status" field. Length must be between 0 and 10 characters. 
		/// Comment found in the Database for this field: .
		/// </summary>
        [DataMember]
        public string Status
		{
			get 
			{ 
				return _status; 
			}
			set 
			{
				_status = value;
			}
		}

		#endregion
		
	}
}
