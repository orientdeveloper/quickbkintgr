//
// Class	:	Timetracking.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:57 AM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization;

namespace QBSynDal
{
    /// <summary>
    /// Class for the properties of the object
    /// </summary>
    public class TimetrackingFields
    {
        public const string TxnID = "TxnID";
        public const string TimeCreated = "TimeCreated";
        public const string TimeModified = "TimeModified";
        public const string EditSequence = "EditSequence";
        public const string TxnNumber = "TxnNumber";
        public const string TxnDate = "TxnDate";
        public const string EntityRef_ListID = "EntityRef_ListID";
        public const string EntityRef_FullName = "EntityRef_FullName";
        public const string CustomerRef_ListID = "CustomerRef_ListID";
        public const string CustomerRef_FullName = "CustomerRef_FullName";
        public const string ItemServiceRef_ListID = "ItemServiceRef_ListID";
        public const string ItemServiceRef_FullName = "ItemServiceRef_FullName";
        public const string Rate = "Rate";
        public const string Duration = "Duration";
        public const string ClassRef_ListID = "ClassRef_ListID";
        public const string ClassRef_FullName = "ClassRef_FullName";
        public const string PayrollItemWageRef_ListID = "PayrollItemWageRef_ListID";
        public const string PayrollItemWageRef_FullName = "PayrollItemWageRef_FullName";
        public const string Notes = "Notes";
        public const string IsBillable = "IsBillable";
        public const string IsBilled = "IsBilled";
        public const string BillableStatus = "BillableStatus";
        public const string Status = "Status";
    }

    /// <summary>
    /// Model class for the "timetracking" table.
    /// </summary>
    [DataContract(Name = "ServiceTimetracking")]
    public class Timetracking : IModel
    {
        #region Class Level Variables

        private string _txnID = null;
        private string _timeCreated = null;
        private string _timeModified = null;
        private string _editSequence = null;
        private int? _txnNumber = null;
        private DateTime? _txnDate = null;
        private string _entityRef_ListID = null;
        private string _entityRef_FullName = null;
        private string _customerRef_ListID = null;
        private string _customerRef_FullName = null;
        private string _itemServiceRef_ListID = null;
        private string _itemServiceRef_FullName = null;
        private string _rate = null;
        private string _duration = null;
        private string _classRef_ListID = null;
        private string _classRef_FullName = null;
        private string _payrollItemWageRef_ListID = null;
        private string _payrollItemWageRef_FullName = null;
        private string _notes = null;
        private bool? _isBillable = null;
        private bool? _isBilled = null;
        private string _billableStatus = null;
        private string _status = null;

        #endregion

        #region Properties

        /// <summary>
        /// This property is mapped to the "TxnID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string TxnID
        {
            get
            {
                return _txnID;
            }
            set
            {
                _txnID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeCreated" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string TimeCreated
        {
            get
            {
                return _timeCreated;
            }
            set
            {
                _timeCreated = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeModified" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string TimeModified
        {
            get
            {
                return _timeModified;
            }
            set
            {
                _timeModified = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "EditSequence" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string EditSequence
        {
            get
            {
                return _editSequence;
            }
            set
            {
                _editSequence = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TxnNumber" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public int? TxnNumber
        {
            get
            {
                return _txnNumber;
            }
            set
            {
                _txnNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TxnDate" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public DateTime? TxnDate
        {
            get
            {
                return _txnDate;
            }
            set
            {
                _txnDate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "EntityRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string EntityRef_ListID
        {
            get
            {
                return _entityRef_ListID;
            }
            set
            {

                _entityRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "EntityRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string EntityRef_FullName
        {
            get
            {
                return _entityRef_FullName;
            }
            set
            {
                _entityRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomerRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string CustomerRef_ListID
        {
            get
            {
                return _customerRef_ListID;
            }
            set
            {
                _customerRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomerRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string CustomerRef_FullName
        {
            get
            {
                return _customerRef_FullName;
            }
            set
            {
                _customerRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ItemServiceRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string ItemServiceRef_ListID
        {
            get
            {
                return _itemServiceRef_ListID;
            }
            set
            {
                _itemServiceRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ItemServiceRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string ItemServiceRef_FullName
        {
            get
            {
                return _itemServiceRef_FullName;
            }
            set
            {
                _itemServiceRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Rate" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string Rate
        {
            get
            {
                return _rate;
            }
            set
            {
                _rate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Duration" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string Duration
        {
            get
            {
                return _duration;
            }
            set
            {
                _duration = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ClassRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ClassRef_ListID
        {
            get
            {
                return _classRef_ListID;
            }
            set
            {
                _classRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ClassRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string ClassRef_FullName
        {
            get
            {
                return _classRef_FullName;
            }
            set
            {
                _classRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "PayrollItemWageRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string PayrollItemWageRef_ListID
        {
            get
            {
                return _payrollItemWageRef_ListID;
            }
            set
            {
                _payrollItemWageRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "PayrollItemWageRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string PayrollItemWageRef_FullName
        {
            get
            {
                return _payrollItemWageRef_FullName;
            }
            set
            {
                _payrollItemWageRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Notes" field. Length must be between 0 and 2000 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string Notes
        {
            get
            {
                return _notes;
            }
            set
            {
                    _notes = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsBillable" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public bool? IsBillable
        {
            get
            {
                return _isBillable;
            }
            set
            {
                _isBillable = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsBilled" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public bool? IsBilled
        {
            get
            {
                return _isBilled;
            }
            set
            {
                _isBilled = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BillableStatus" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string BillableStatus
        {
            get
            {
                return _billableStatus;
            }
            set
            {
                _billableStatus = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Status" field. Length must be between 0 and 10 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        #endregion
    }
}
