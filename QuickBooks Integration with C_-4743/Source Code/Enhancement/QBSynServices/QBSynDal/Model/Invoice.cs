//
// Class	:	Invoice.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:44 AM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization;

namespace QBSynDal
{
    /// <summary>
    /// Class for the properties of the object
    /// </summary>
    public class InvoiceFields
    {
        public const string TxnID = "TxnID";
        public const string TimeCreated = "TimeCreated";
        public const string TimeModified = "TimeModified";
        public const string EditSequence = "EditSequence";
        public const string TxnNumber = "TxnNumber";
        public const string CustomerRef_ListID = "CustomerRef_ListID";
        public const string CustomerRef_FullName = "CustomerRef_FullName";
        public const string ClassRef_ListID = "ClassRef_ListID";
        public const string ClassRef_FullName = "ClassRef_FullName";
        public const string ARAccountRef_ListID = "ARAccountRef_ListID";
        public const string ARAccountRef_FullName = "ARAccountRef_FullName";
        public const string TemplateRef_ListID = "TemplateRef_ListID";
        public const string TemplateRef_FullName = "TemplateRef_FullName";
        public const string TxnDate = "TxnDate";
        public const string RefNumber = "RefNumber";
        public const string BillAddress_Addr1 = "BillAddress_Addr1";
        public const string BillAddress_Addr2 = "BillAddress_Addr2";
        public const string BillAddress_Addr3 = "BillAddress_Addr3";
        public const string BillAddress_Addr4 = "BillAddress_Addr4";
        public const string BillAddress_Addr5 = "BillAddress_Addr5";
        public const string BillAddress_City = "BillAddress_City";
        public const string BillAddress_State = "BillAddress_State";
        public const string BillAddress_PostalCode = "BillAddress_PostalCode";
        public const string BillAddress_Country = "BillAddress_Country";
        public const string BillAddress_Note = "BillAddress_Note";
        public const string ShipAddress_Addr1 = "ShipAddress_Addr1";
        public const string ShipAddress_Addr2 = "ShipAddress_Addr2";
        public const string ShipAddress_Addr3 = "ShipAddress_Addr3";
        public const string ShipAddress_Addr4 = "ShipAddress_Addr4";
        public const string ShipAddress_Addr5 = "ShipAddress_Addr5";
        public const string ShipAddress_City = "ShipAddress_City";
        public const string ShipAddress_State = "ShipAddress_State";
        public const string ShipAddress_PostalCode = "ShipAddress_PostalCode";
        public const string ShipAddress_Country = "ShipAddress_Country";
        public const string ShipAddress_Note = "ShipAddress_Note";
        public const string IsPending = "IsPending";
        public const string IsFinanceCharge = "IsFinanceCharge";
        public const string PONumber = "PONumber";
        public const string TermsRef_ListID = "TermsRef_ListID";
        public const string TermsRef_FullName = "TermsRef_FullName";
        public const string DueDate = "DueDate";
        public const string SalesRepRef_ListID = "SalesRepRef_ListID";
        public const string SalesRepRef_FullName = "SalesRepRef_FullName";
        public const string Fob = "FOB";
        public const string ShipDate = "ShipDate";
        public const string ShipMethodRef_ListID = "ShipMethodRef_ListID";
        public const string ShipMethodRef_FullName = "ShipMethodRef_FullName";
        public const string Subtotal = "Subtotal";
        public const string ItemSalesTaxRef_ListID = "ItemSalesTaxRef_ListID";
        public const string ItemSalesTaxRef_FullName = "ItemSalesTaxRef_FullName";
        public const string SalesTaxPercentage = "SalesTaxPercentage";
        public const string SalesTaxTotal = "SalesTaxTotal";
        public const string AppliedAmount = "AppliedAmount";
        public const string BalanceRemaining = "BalanceRemaining";
        public const string CurrencyRef_ListID = "CurrencyRef_ListID";
        public const string CurrencyRef_FullName = "CurrencyRef_FullName";
        public const string ExchangeRate = "ExchangeRate";
        public const string BalanceRemainingInHomeCurrency = "BalanceRemainingInHomeCurrency";
        public const string Memo = "Memo";
        public const string IsPaid = "IsPaid";
        public const string CustomerMsgRef_ListID = "CustomerMsgRef_ListID";
        public const string CustomerMsgRef_FullName = "CustomerMsgRef_FullName";
        public const string IsToBePrinted = "IsToBePrinted";
        public const string IsToBeEmailed = "IsToBeEmailed";
        public const string IsTaxIncluded = "IsTaxIncluded";
        public const string CustomerSalesTaxCodeRef_ListID = "CustomerSalesTaxCodeRef_ListID";
        public const string CustomerSalesTaxCodeRef_FullName = "CustomerSalesTaxCodeRef_FullName";
        public const string SuggestedDiscountAmount = "SuggestedDiscountAmount";
        public const string SuggestedDiscountDate = "SuggestedDiscountDate";
        public const string Other = "Other";
        public const string CustomField1 = "CustomField1";
        public const string CustomField2 = "CustomField2";
        public const string CustomField3 = "CustomField3";
        public const string CustomField4 = "CustomField4";
        public const string CustomField5 = "CustomField5";
        public const string CustomField6 = "CustomField6";
        public const string CustomField7 = "CustomField7";
        public const string CustomField8 = "CustomField8";
        public const string CustomField9 = "CustomField9";
        public const string CustomField10 = "CustomField10";
        public const string Status = "Status";
    }
    /// <summary>
    /// Model class for the "invoice" table.
    /// </summary>
    [DataContract(Name = "ServiceInvoice")]
    public class Invoice : IModel
    {
        #region Class Level Variables

        private string _txnID = null;
        private string _timeCreated = null;
        private string _timeModified = null;
        private string _editSequence = null;
        private int? _txnNumber = null;
        private string _customerRef_ListID = null;
        private string _customerRef_FullName = null;
        private string _classRef_ListID = null;
        private string _classRef_FullName = null;
        private string _aRAccountRef_ListID = null;
        private string _aRAccountRef_FullName = null;
        private string _templateRef_ListID = null;
        private string _templateRef_FullName = null;
        private DateTime? _txnDate = null;
        private string _refNumber = null;
        private string _billAddress_Addr1 = null;
        private string _billAddress_Addr2 = null;
        private string _billAddress_Addr3 = null;
        private string _billAddress_Addr4 = null;
        private string _billAddress_Addr5 = null;
        private string _billAddress_City = null;
        private string _billAddress_State = null;
        private string _billAddress_PostalCode = null;
        private string _billAddress_Country = null;
        private string _billAddress_Note = null;
        private string _shipAddress_Addr1 = null;
        private string _shipAddress_Addr2 = null;
        private string _shipAddress_Addr3 = null;
        private string _shipAddress_Addr4 = null;
        private string _shipAddress_Addr5 = null;
        private string _shipAddress_City = null;
        private string _shipAddress_State = null;
        private string _shipAddress_PostalCode = null;
        private string _shipAddress_Country = null;
        private string _shipAddress_Note = null;
        private bool? _isPending = null;
        private bool? _isFinanceCharge = null;
        private string _pONumber = null;
        private string _termsRef_ListID = null;
        private string _termsRef_FullName = null;
        private DateTime? _dueDate = null;
        private string _salesRepRef_ListID = null;
        private string _salesRepRef_FullName = null;
        private string _fob = null;
        private DateTime? _shipDate = null;
        private string _shipMethodRef_ListID = null;
        private string _shipMethodRef_FullName = null;
        private double? _subtotal = null;
        private string _itemSalesTaxRef_ListID = null;
        private string _itemSalesTaxRef_FullName = null;
        private string _salesTaxPercentage = null;
        private double? _salesTaxTotal = null;
        private double? _appliedAmount = null;
        private double? _balanceRemaining = null;
        private string _currencyRef_ListID = null;
        private string _currencyRef_FullName = null;
        private double? _exchangeRate = null;
        private double? _balanceRemainingInHomeCurrency = null;
        private string _memo = null;
        private bool? _isPaid = null;
        private string _customerMsgRef_ListID = null;
        private string _customerMsgRef_FullName = null;
        private bool? _isToBePrinted = null;
        private bool? _isToBeEmailed = null;
        private bool? _isTaxIncluded = null;
        private string _customerSalesTaxCodeRef_ListID = null;
        private string _customerSalesTaxCodeRef_FullName = null;
        private double? _suggestedDiscountAmount = null;
        private DateTime? _suggestedDiscountDate = null;
        private string _other = null;
        private string _customField1 = null;
        private string _customField2 = null;
        private string _customField3 = null;
        private string _customField4 = null;
        private string _customField5 = null;
        private string _customField6 = null;
        private string _customField7 = null;
        private string _customField8 = null;
        private string _customField9 = null;
        private string _customField10 = null;
        private string _status = null;

        #endregion

        #region Properties

        /// <summary>
        /// This property is mapped to the "TxnID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TxnID
        {
            get
            {
                return _txnID;
            }
            set
            {
                _txnID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeCreated" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TimeCreated
        {
            get
            {
                return _timeCreated;
            }
            set
            {
                _timeCreated = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeModified" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TimeModified
        {
            get
            {
                return _timeModified;
            }
            set
            {
                _timeModified = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "EditSequence" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string EditSequence
        {
            get
            {
                return _editSequence;
            }
            set
            {
                _editSequence = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TxnNumber" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public int? TxnNumber
        {
            get
            {
                return _txnNumber;
            }
            set
            {
                _txnNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomerRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomerRef_ListID
        {
            get
            {
                return _customerRef_ListID;
            }
            set
            {
                _customerRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomerRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomerRef_FullName
        {
            get
            {
                return _customerRef_FullName;
            }
            set
            {
                _customerRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ClassRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ClassRef_ListID
        {
            get
            {
                return _classRef_ListID;
            }
            set
            {
                _classRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ClassRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ClassRef_FullName
        {
            get
            {
                return _classRef_FullName;
            }
            set
            {
                _classRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ARAccountRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ARAccountRef_ListID
        {
            get
            {
                return _aRAccountRef_ListID;
            }
            set
            {
                _aRAccountRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ARAccountRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ARAccountRef_FullName
        {
            get
            {
                return _aRAccountRef_FullName;
            }
            set
            {
                _aRAccountRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TemplateRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TemplateRef_ListID
        {
            get
            {
                return _templateRef_ListID;
            }
            set
            {
                _templateRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TemplateRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TemplateRef_FullName
        {
            get
            {
                return _templateRef_FullName;
            }
            set
            {
                _templateRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TxnDate" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public DateTime? TxnDate
        {
            get
            {
                return _txnDate;
            }
            set
            {
                _txnDate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "RefNumber" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string RefNumber
        {
            get
            {
                return _refNumber;
            }
            set
            {
                _refNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BillAddress_Addr1" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string BillAddress_Addr1
        {
            get
            {
                return _billAddress_Addr1;
            }
            set
            {
                _billAddress_Addr1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BillAddress_Addr2" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string BillAddress_Addr2
        {
            get
            {
                return _billAddress_Addr2;
            }
            set
            {
                _billAddress_Addr2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BillAddress_Addr3" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string BillAddress_Addr3
        {
            get
            {
                return _billAddress_Addr3;
            }
            set
            {
                _billAddress_Addr3 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BillAddress_Addr4" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string BillAddress_Addr4
        {
            get
            {
                return _billAddress_Addr4;
            }
            set
            {
                _billAddress_Addr4 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BillAddress_Addr5" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string BillAddress_Addr5
        {
            get
            {
                return _billAddress_Addr5;
            }
            set
            {
                _billAddress_Addr5 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BillAddress_City" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string BillAddress_City
        {
            get
            {
                return _billAddress_City;
            }
            set
            {
                _billAddress_City = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BillAddress_State" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string BillAddress_State
        {
            get
            {
                return _billAddress_State;
            }
            set
            {
                _billAddress_State = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BillAddress_PostalCode" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string BillAddress_PostalCode
        {
            get
            {
                return _billAddress_PostalCode;
            }
            set
            {
                _billAddress_PostalCode = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BillAddress_Country" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string BillAddress_Country
        {
            get
            {
                return _billAddress_Country;
            }
            set
            {
                _billAddress_Country = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BillAddress_Note" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string BillAddress_Note
        {
            get
            {
                return _billAddress_Note;
            }
            set
            {
                _billAddress_Note = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_Addr1" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipAddress_Addr1
        {
            get
            {
                return _shipAddress_Addr1;
            }
            set
            {
                _shipAddress_Addr1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_Addr2" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipAddress_Addr2
        {
            get
            {
                return _shipAddress_Addr2;
            }
            set
            {
                _shipAddress_Addr2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_Addr3" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipAddress_Addr3
        {
            get
            {
                return _shipAddress_Addr3;
            }
            set
            {
                _shipAddress_Addr3 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_Addr4" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipAddress_Addr4
        {
            get
            {
                return _shipAddress_Addr4;
            }
            set
            {
                _shipAddress_Addr4 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_Addr5" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipAddress_Addr5
        {
            get
            {
                return _shipAddress_Addr5;
            }
            set
            {
                _shipAddress_Addr5 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_City" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipAddress_City
        {
            get
            {
                return _shipAddress_City;
            }
            set
            {
                _shipAddress_City = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_State" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipAddress_State
        {
            get
            {
                return _shipAddress_State;
            }
            set
            {
                _shipAddress_State = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_PostalCode" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipAddress_PostalCode
        {
            get
            {
                return _shipAddress_PostalCode;
            }
            set
            {
                _shipAddress_PostalCode = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_Country" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipAddress_Country
        {
            get
            {
                return _shipAddress_Country;
            }
            set
            {
                _shipAddress_Country = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipAddress_Note" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipAddress_Note
        {
            get
            {
                return _shipAddress_Note;
            }
            set
            {
                _shipAddress_Note = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsPending" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public bool? IsPending
        {
            get
            {
                return _isPending;
            }
            set
            {
                _isPending = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsFinanceCharge" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public bool? IsFinanceCharge
        {
            get
            {
                return _isFinanceCharge;
            }
            set
            {
                _isFinanceCharge = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "PONumber" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string PONumber
        {
            get
            {
                return _pONumber;
            }
            set
            {
                _pONumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TermsRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TermsRef_ListID
        {
            get
            {
                return _termsRef_ListID;
            }
            set
            {
                _termsRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TermsRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TermsRef_FullName
        {
            get
            {
                return _termsRef_FullName;
            }
            set
            {
                _termsRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "DueDate" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public DateTime? DueDate
        {
            get
            {
                return _dueDate;
            }
            set
            {
                _dueDate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SalesRepRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>

        [DataMember]
        public string SalesRepRef_ListID
        {
            get
            {
                return _salesRepRef_ListID;
            }
            set
            {
                _salesRepRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SalesRepRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SalesRepRef_FullName
        {
            get
            {
                return _salesRepRef_FullName;
            }
            set
            {
                _salesRepRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "FOB" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Fob
        {
            get
            {
                return _fob;
            }
            set
            {
                _fob = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipDate" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public DateTime? ShipDate
        {
            get
            {
                return _shipDate;
            }
            set
            {
                _shipDate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipMethodRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipMethodRef_ListID
        {
            get
            {
                return _shipMethodRef_ListID;
            }
            set
            {
                _shipMethodRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipMethodRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ShipMethodRef_FullName
        {
            get
            {
                return _shipMethodRef_FullName;
            }
            set
            {
                _shipMethodRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Subtotal" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? Subtotal
        {
            get
            {
                return _subtotal;
            }
            set
            {
                _subtotal = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ItemSalesTaxRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ItemSalesTaxRef_ListID
        {
            get
            {
                return _itemSalesTaxRef_ListID;
            }
            set
            {
                _itemSalesTaxRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ItemSalesTaxRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ItemSalesTaxRef_FullName
        {
            get
            {
                return _itemSalesTaxRef_FullName;
            }
            set
            {
                _itemSalesTaxRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SalesTaxPercentage" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SalesTaxPercentage
        {
            get
            {
                return _salesTaxPercentage;
            }
            set
            {
                _salesTaxPercentage = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SalesTaxTotal" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? SalesTaxTotal
        {
            get
            {
                return _salesTaxTotal;
            }
            set
            {
                _salesTaxTotal = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AppliedAmount" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? AppliedAmount
        {
            get
            {
                return _appliedAmount;
            }
            set
            {
                _appliedAmount = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BalanceRemaining" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? BalanceRemaining
        {
            get
            {
                return _balanceRemaining;
            }
            set
            {
                _balanceRemaining = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CurrencyRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CurrencyRef_ListID
        {
            get
            {
                return _currencyRef_ListID;
            }
            set
            {
                _currencyRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CurrencyRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CurrencyRef_FullName
        {
            get
            {
                return _currencyRef_FullName;
            }
            set
            {
                _currencyRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ExchangeRate" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? ExchangeRate
        {
            get
            {
                return _exchangeRate;
            }
            set
            {
                _exchangeRate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BalanceRemainingInHomeCurrency" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? BalanceRemainingInHomeCurrency
        {
            get
            {
                return _balanceRemainingInHomeCurrency;
            }
            set
            {
                _balanceRemainingInHomeCurrency = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Memo" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Memo
        {
            get
            {
                return _memo;
            }
            set
            {
                _memo = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsPaid" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public bool? IsPaid
        {
            get
            {
                return _isPaid;
            }
            set
            {
                _isPaid = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomerMsgRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomerMsgRef_ListID
        {
            get
            {
                return _customerMsgRef_ListID;
            }
            set
            {
                _customerMsgRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomerMsgRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomerMsgRef_FullName
        {
            get
            {
                return _customerMsgRef_FullName;
            }
            set
            {
                _customerMsgRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsToBePrinted" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public bool? IsToBePrinted
        {
            get
            {
                return _isToBePrinted;
            }
            set
            {
                _isToBePrinted = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsToBeEmailed" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public bool? IsToBeEmailed
        {
            get
            {
                return _isToBeEmailed;
            }
            set
            {
                _isToBeEmailed = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsTaxIncluded" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public bool? IsTaxIncluded
        {
            get
            {
                return _isTaxIncluded;
            }
            set
            {
                _isTaxIncluded = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomerSalesTaxCodeRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomerSalesTaxCodeRef_ListID
        {
            get
            {
                return _customerSalesTaxCodeRef_ListID;
            }
            set
            {
                _customerSalesTaxCodeRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomerSalesTaxCodeRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomerSalesTaxCodeRef_FullName
        {
            get
            {
                return _customerSalesTaxCodeRef_FullName;
            }
            set
            {
                _customerSalesTaxCodeRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SuggestedDiscountAmount" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? SuggestedDiscountAmount
        {
            get
            {
                return _suggestedDiscountAmount;
            }
            set
            {
                _suggestedDiscountAmount = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SuggestedDiscountDate" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public DateTime? SuggestedDiscountDate
        {
            get
            {
                return _suggestedDiscountDate;
            }
            set
            {
                _suggestedDiscountDate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Other" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Other
        {
            get
            {
                return _other;
            }
            set
            {
                _other = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField1" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField1
        {
            get
            {
                return _customField1;
            }
            set
            {
                _customField1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField2" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField2
        {
            get
            {
                return _customField2;
            }
            set
            {
                _customField2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField3" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField3
        {
            get
            {
                return _customField3;
            }
            set
            {
                _customField3 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField4" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField4
        {
            get
            {
                return _customField4;
            }
            set
            {
                _customField4 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField5" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField5
        {
            get
            {
                return _customField5;
            }
            set
            {
                _customField5 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField6" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField6
        {
            get
            {
                return _customField6;
            }
            set
            {
                _customField6 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField7" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField7
        {
            get
            {
                return _customField7;
            }
            set
            {
                _customField7 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField8" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField8
        {
            get
            {
                return _customField8;
            }
            set
            {
                _customField8 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField9" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField9
        {
            get
            {
                return _customField9;
            }
            set
            {
                _customField9 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField10" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField10
        {
            get
            {
                return _customField10;
            }
            set
            {
                _customField10 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Status" field. Length must be between 0 and 10 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        #endregion
    }
}
