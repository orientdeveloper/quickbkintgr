//
// Class	:	Billingrate.cs
// Author	:  	Synapse © 2013
// Date		:	4/2/2013 10:11:43 AM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization;

namespace QBSynDal
{
    /// <summary>
    /// Class for the properties of the object
    /// </summary>
    public class BillingrateFields
    {
        public const string ListID = "ListID";
        public const string TimeCreated = "TimeCreated";
        public const string TimeModified = "TimeModified";
        public const string EditSequence = "EditSequence";
        public const string Name = "Name";
        public const string BillingRateType = "BillingRateType";
        public const string FixedBillingRate = "FixedBillingRate";
        public const string Status = "Status";

    }
    /// <summary>
    /// DataContract class for the "billingrate" table.
    /// </summary>
    [DataContract(Name = "ServiceBillingrate")]
    public class Billingrate : IModel
    {

        #region Class Level Variables

        private string _listID = null;
        private string _timeCreated = null;
        private string _timeModified = null;
        private string _editSequence = null;
        private string _name = null;
        private string _billingRateType = null;
        private string _fixedBillingRate = null;
        private string _status = null;

        #endregion

        #region Properties

        /// <summary>
        /// This property is mapped to the "ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ListID
        {
            get
            {
                return _listID;
            }
            set
            {
                _listID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeCreated" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TimeCreated
        {
            get
            {
                return _timeCreated;
            }
            set
            {
                _timeCreated = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeModified" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TimeModified
        {
            get
            {
                return _timeModified;
            }
            set
            {
                _timeModified = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "EditSequence" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string EditSequence
        {
            get
            {
                return _editSequence;
            }
            set
            {
                _editSequence = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Name" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BillingRateType" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string BillingRateType
        {
            get
            {
                return _billingRateType;
            }
            set
            {
                _billingRateType = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "FixedBillingRate" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string FixedBillingRate
        {
            get
            {
                return _fixedBillingRate;
            }
            set
            {
                _fixedBillingRate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Status" field. Length must be between 0 and 10 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        #endregion

    }
}
