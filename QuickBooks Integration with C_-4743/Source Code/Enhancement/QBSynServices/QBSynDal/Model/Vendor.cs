//
// Class	:	Vendor.cs
// Author	:  	SynapseIndia © 2013
// Date		:	27/05/2013 11:30:59 AM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization;

namespace QBSynDal
{

    /// <summary>
    /// Class for the properties of the object
    /// </summary>
    public class VendorFields
    {
        public const string ListID = "ListID";
        public const string TimeCreated = "TimeCreated";
        public const string TimeModified = "TimeModified";
        public const string EditSequence = "EditSequence";
        public const string Name = "Name";
        public const string IsActive = "IsActive";
        public const string CompanyName = "CompanyName";
        public const string Salutation = "Salutation";
        public const string FirstName = "FirstName";
        public const string MiddleName = "MiddleName";
        public const string LastName = "LastName";
        public const string Suffix = "Suffix";
        public const string VendorAddress_Addr1 = "VendorAddress_Addr1";
        public const string VendorAddress_Addr2 = "VendorAddress_Addr2";
        public const string VendorAddress_Addr3 = "VendorAddress_Addr3";
        public const string VendorAddress_Addr4 = "VendorAddress_Addr4";
        public const string VendorAddress_Addr5 = "VendorAddress_Addr5";
        public const string VendorAddress_City = "VendorAddress_City";
        public const string VendorAddress_State = "VendorAddress_State";
        public const string VendorAddress_PostalCode = "VendorAddress_PostalCode";
        public const string VendorAddress_Country = "VendorAddress_Country";
        public const string VendorAddress_Note = "VendorAddress_Note";
        public const string Phone = "Phone";
        public const string Mobile = "Mobile";
        public const string Pager = "Pager";
        public const string AltPhone = "AltPhone";
        public const string Fax = "Fax";
        public const string Email = "Email";
        public const string Contact = "Contact";
        public const string AltContact = "AltContact";
        public const string NameOnCheck = "NameOnCheck";
        public const string Notes = "Notes";
        public const string AccountNumber = "AccountNumber";
        public const string VendorTypeRef_ListID = "VendorTypeRef_ListID";
        public const string VendorTypeRef_FullName = "VendorTypeRef_FullName";
        public const string TermsRef_ListID = "TermsRef_ListID";
        public const string TermsRef_FullName = "TermsRef_FullName";
        public const string CreditLimit = "CreditLimit";
        public const string VendorTaxIdent = "VendorTaxIdent";
        public const string IsVendorEligibleFor1099 = "IsVendorEligibleFor1099";
        public const string Balance = "Balance";
        public const string IsSalesTaxAgency = "IsSalesTaxAgency";
        public const string CurrencyRef_ListID = "CurrencyRef_ListID";
        public const string CurrencyRef_FullName = "CurrencyRef_FullName";
        public const string BillingRateRef_ListID = "BillingRateRef_ListID";
        public const string BillingRateRef_FullName = "BillingRateRef_FullName";
        public const string CustomField1 = "CustomField1";
        public const string CustomField2 = "CustomField2";
        public const string CustomField3 = "CustomField3";
        public const string CustomField4 = "CustomField4";
        public const string CustomField5 = "CustomField5";
        public const string CustomField6 = "CustomField6";
        public const string CustomField7 = "CustomField7";
        public const string CustomField8 = "CustomField8";
        public const string CustomField9 = "CustomField9";
        public const string CustomField10 = "CustomField10";
        public const string Status = "Status";

    }

    /// <summary>
    /// Model class for the "vendor" table.
    /// </summary>
    [DataContract(Name = "ServiceVendor")]
    public class Vendor : IModel
    {

        #region Class Level Variables


        private string _listID = null;
        private string _timeCreated = null;
        private string _timeModified = null;
        private string _editSequence = null;
        private string _name = null;
        private bool? _isActive = null;
        private string _companyName = null;
        private string _salutation = null;
        private string _firstName = null;
        private string _middleName = null;
        private string _lastName = null;
        private string _suffix = null;
        private string _vendorAddress_Addr1 = null;
        private string _vendorAddress_Addr2 = null;
        private string _vendorAddress_Addr3 = null;
        private string _vendorAddress_Addr4 = null;
        private string _vendorAddress_Addr5 = null;
        private string _vendorAddress_City = null;
        private string _vendorAddress_State = null;
        private string _vendorAddress_PostalCode = null;
        private string _vendorAddress_Country = null;
        private string _vendorAddress_Note = null;
        private string _phone = null;
        private string _mobile = null;
        private string _pager = null;
        private string _altPhone = null;
        private string _fax = null;
        private string _email = null;
        private string _contact = null;
        private string _altContact = null;
        private string _nameOnCheck = null;
        private string _notes = null;
        private string _accountNumber = null;
        private string _vendorTypeRef_ListID = null;
        private string _vendorTypeRef_FullName = null;
        private string _termsRef_ListID = null;
        private string _termsRef_FullName = null;
        private double? _creditLimit = null;
        private string _vendorTaxIdent = null;
        private bool? _isVendorEligibleFor1099 = null;
        private double? _balance = null;
        private bool? _isSalesTaxAgency = null;
        private string _currencyRef_ListID = null;
        private string _currencyRef_FullName = null;
        private string _billingRateRef_ListID = null;
        private string _billingRateRef_FullName = null;
        private string _customField1 = null;
        private string _customField2 = null;
        private string _customField3 = null;
        private string _customField4 = null;
        private string _customField5 = null;
        private string _customField6 = null;
        private string _customField7 = null;
        private string _customField8 = null;
        private string _customField9 = null;
        private string _customField10 = null;
        private string _status = null;

        #endregion

        #region Properties

        /// <summary>
        /// This property is mapped to the "ListID" field.
        /// </summary>
        [DataMember]
        public string ListID
        {
            get
            {
                return _listID;
            }
            set
            {
                _listID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeCreated" field. 
        /// </summary>
        [DataMember]
        public string TimeCreated
        {
            get
            {
                return _timeCreated;
            }
            set
            {
                _timeCreated = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeModified" field.
        /// </summary>
        [DataMember]
        public string TimeModified
        {
            get
            {
                return _timeModified;
            }
            set
            {
                _timeModified = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "EditSequence" field.
        /// </summary>
        [DataMember]
        public string EditSequence
        {
            get
            {
                return _editSequence;
            }
            set
            {
                _editSequence = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Name" field. 
        /// </summary>
        [DataMember]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsActive" field.  
        /// </summary>
        [DataMember]
        public bool? IsActive
        {
            get
            {
                return _isActive;
            }
            set
            {
                _isActive = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CompanyName" field.
        /// </summary>
        [DataMember]
        public string CompanyName
        {
            get
            {
                return _companyName;
            }
            set
            {
                _companyName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Salutation" field. 
        /// </summary>
        [DataMember]
        public string Salutation
        {
            get
            {
                return _salutation;
            }
            set
            {
                _salutation = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "FirstName" field.
        /// </summary>
        [DataMember]
        public string FirstName
        {
            get
            {
                return _firstName;
            }
            set
            {
                _firstName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "MiddleName" field. 
        /// </summary>
        [DataMember]
        public string MiddleName
        {
            get
            {
                return _middleName;
            }
            set
            {
                _middleName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "LastName" field. 
        /// </summary>
        [DataMember]
        public string LastName
        {
            get
            {
                return _lastName;
            }
            set
            {
                _lastName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Suffix" field. 
        /// </summary>
        [DataMember]
        public string Suffix
        {
            get
            {
                return _suffix;
            }
            set
            {
                _suffix = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "VendorAddress_Addr1" field. 
        /// </summary>
        [DataMember]
        public string VendorAddress_Addr1
        {
            get
            {
                return _vendorAddress_Addr1;
            }
            set
            {
                _vendorAddress_Addr1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "VendorAddress_Addr2" field. 
        /// </summary>
        [DataMember]
        public string VendorAddress_Addr2
        {
            get
            {
                return _vendorAddress_Addr2;
            }
            set
            {
                _vendorAddress_Addr2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "VendorAddress_Addr3" field. 
        /// </summary>
        [DataMember]
        public string VendorAddress_Addr3
        {
            get
            {
                return _vendorAddress_Addr3;
            }
            set
            {
                _vendorAddress_Addr3 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "VendorAddress_Addr4" field. 
        /// </summary>
        [DataMember]
        public string VendorAddress_Addr4
        {
            get
            {
                return _vendorAddress_Addr4;
            }
            set
            {
                _vendorAddress_Addr4 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "VendorAddress_Addr5" field.
        /// </summary>
        [DataMember]
        public string VendorAddress_Addr5
        {
            get
            {
                return _vendorAddress_Addr5;
            }
            set
            {
                _vendorAddress_Addr5 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "VendorAddress_City" field. 
        /// </summary>
        [DataMember]
        public string VendorAddress_City
        {
            get
            {
                return _vendorAddress_City;
            }
            set
            {
                _vendorAddress_City = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "VendorAddress_State" field. 
        /// </summary>
        [DataMember]
        public string VendorAddress_State
        {
            get
            {
                return _vendorAddress_State;
            }
            set
            {
                _vendorAddress_State = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "VendorAddress_PostalCode" field.
        /// </summary>
        [DataMember]
        public string VendorAddress_PostalCode
        {
            get
            {
                return _vendorAddress_PostalCode;
            }
            set
            {
                _vendorAddress_PostalCode = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "VendorAddress_Country" field. 
        /// </summary>
        [DataMember]
        public string VendorAddress_Country
        {
            get
            {
                return _vendorAddress_Country;
            }
            set
            {
                _vendorAddress_Country = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "VendorAddress_Note" field. 
        /// </summary>
        [DataMember]
        public string VendorAddress_Note
        {
            get
            {
                return _vendorAddress_Note;
            }
            set
            {
                _vendorAddress_Note = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Phone" field. 
        /// </summary>
        [DataMember]
        public string Phone
        {
            get
            {
                return _phone;
            }
            set
            {
                _phone = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Mobile" field. 
        /// </summary>
        [DataMember]
        public string Mobile
        {
            get
            {
                return _mobile;
            }
            set
            {
                _mobile = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Pager" field. 
        /// </summary>
        [DataMember]
        public string Pager
        {
            get
            {
                return _pager;
            }
            set
            {
                _pager = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AltPhone" field.
        /// </summary>
        [DataMember]
        public string AltPhone
        {
            get
            {
                return _altPhone;
            }
            set
            {
                _altPhone = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Fax" field. 
        /// </summary>
        [DataMember]
        public string Fax
        {
            get
            {
                return _fax;
            }
            set
            {
                _fax = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Email" field. 
        /// </summary>
        [DataMember]
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                _email = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Contact" field.
        /// </summary>
        [DataMember]
        public string Contact
        {
            get
            {
                return _contact;
            }
            set
            {
                _contact = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AltContact" field. 
        /// </summary>
        [DataMember]
        public string AltContact
        {
            get
            {
                return _altContact;
            }
            set
            {
                _altContact = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "NameOnCheck" field. 
        /// </summary>
        [DataMember]
        public string NameOnCheck
        {
            get
            {
                return _nameOnCheck;
            }
            set
            {
                _nameOnCheck = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Notes" field.
        /// </summary>
        [DataMember]
        public string Notes
        {
            get
            {
                return _notes;
            }
            set
            {
                _notes = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AccountNumber" field.
        /// </summary>
        [DataMember]
        public string AccountNumber
        {
            get
            {
                return _accountNumber;
            }
            set
            {
                _accountNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "VendorTypeRef_ListID" field.
        /// </summary>
        [DataMember]
        public string VendorTypeRef_ListID
        {
            get
            {
                return _vendorTypeRef_ListID;
            }
            set
            {
                _vendorTypeRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "VendorTypeRef_FullName" field. 
        /// </summary>
        [DataMember]
        public string VendorTypeRef_FullName
        {
            get
            {
                return _vendorTypeRef_FullName;
            }
            set
            {
                _vendorTypeRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TermsRef_ListID" field. 
        /// </summary>
        [DataMember]
        public string TermsRef_ListID
        {
            get
            {
                return _termsRef_ListID;
            }
            set
            {
                _termsRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TermsRef_FullName" field.
        /// </summary>
        [DataMember]
        public string TermsRef_FullName
        {
            get
            {
                return _termsRef_FullName;
            }
            set
            {
                _termsRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CreditLimit" field.  
        /// </summary>
        [DataMember]
        public double? CreditLimit
        {
            get
            {
                return _creditLimit;
            }
            set
            {
                _creditLimit = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "VendorTaxIdent" field.
        /// </summary>
        [DataMember]
        public string VendorTaxIdent
        {
            get
            {
                return _vendorTaxIdent;
            }
            set
            {
                _vendorTaxIdent = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsVendorEligibleFor1099" field.  
        /// </summary>
        [DataMember]
        public bool? IsVendorEligibleFor1099
        {
            get
            {
                return _isVendorEligibleFor1099;
            }
            set
            {
                _isVendorEligibleFor1099 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Balance" field.  
        /// </summary>
        [DataMember]
        public double? Balance
        {
            get
            {
                return _balance;
            }
            set
            {
                _balance = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsSalesTaxAgency" field.  
        /// </summary>
        [DataMember]
        public bool? IsSalesTaxAgency
        {
            get
            {
                return _isSalesTaxAgency;
            }
            set
            {
                _isSalesTaxAgency = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CurrencyRef_ListID" field. 
        /// </summary>
        [DataMember]
        public string CurrencyRef_ListID
        {
            get
            {
                return _currencyRef_ListID;
            }
            set
            {
                _currencyRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CurrencyRef_FullName" field. 
        /// </summary>
        [DataMember]
        public string CurrencyRef_FullName
        {
            get
            {
                return _currencyRef_FullName;
            }
            set
            {
                _currencyRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BillingRateRef_ListID" field.
        /// </summary>
        [DataMember]
        public string BillingRateRef_ListID
        {
            get
            {
                return _billingRateRef_ListID;
            }
            set
            {
                _billingRateRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BillingRateRef_FullName" field. 
        /// </summary>
        [DataMember]
        public string BillingRateRef_FullName
        {
            get
            {
                return _billingRateRef_FullName;
            }
            set
            {
                _billingRateRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField1" field.
        /// </summary>
        [DataMember]
        public string CustomField1
        {
            get
            {
                return _customField1;
            }
            set
            {
                _customField1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField2" field. 
        /// </summary>
        [DataMember]
        public string CustomField2
        {
            get
            {
                return _customField2;
            }
            set
            {
                _customField2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField3" field. 
        /// </summary>
        [DataMember]
        public string CustomField3
        {
            get
            {
                return _customField3;
            }
            set
            {
                _customField3 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField4" field. 
        /// </summary>
        [DataMember]
        public string CustomField4
        {
            get
            {
                return _customField4;
            }
            set
            {
                _customField4 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField5" field. 
        /// </summary>
        [DataMember]
        public string CustomField5
        {
            get
            {
                return _customField5;
            }
            set
            {
                _customField5 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField6" field. 
        /// </summary>
        [DataMember]
        public string CustomField6
        {
            get
            {
                return _customField6;
            }
            set
            {
                _customField6 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField7" field. 
        /// </summary>
        [DataMember]
        public string CustomField7
        {
            get
            {
                return _customField7;
            }
            set
            {
                _customField7 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField8" field.
        /// </summary>
        [DataMember]
        public string CustomField8
        {
            get
            {
                return _customField8;
            }
            set
            {
                _customField8 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField9" field.
        /// </summary>
        [DataMember]
        public string CustomField9
        {
            get
            {
                return _customField9;
            }
            set
            {
                _customField9 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField10" field.
        /// </summary>
        [DataMember]
        public string CustomField10
        {
            get
            {
                return _customField10;
            }
            set
            {
                _customField10 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Status" field. 
        /// </summary>
        [DataMember]
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        #endregion
    }
}
