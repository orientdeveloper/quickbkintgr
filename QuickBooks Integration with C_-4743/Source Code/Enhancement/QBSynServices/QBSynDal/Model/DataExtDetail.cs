//
// Class	:	Account.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Data.SqlClient;
using System.Runtime.Serialization;

namespace QBSynDal
{
    /// <summary>
    /// Class for the properties of the object
    /// </summary>
    public class DataExtDetailFields
    {
        public const string OwnerID = "OwnerID";
        public const string DataExtName = "DataExtName";
        public const string DataExtType = "DataExtType";
        public const string DataExtValue = "DataExtValue";
        public const string OwnerType = "OwnerType";
        public const string CustomField1 = "CustomField1";
        public const string CustomField2 = "CustomField2";
        public const string CustomField3 = "CustomField3";
        public const string CustomField4 = "CustomField4";
        public const string CustomField5 = "CustomField5";
        public const string CustomField6 = "CustomField6";
        public const string CustomField7 = "CustomField7";
        public const string CustomField8 = "CustomField8";
        public const string CustomField9 = "CustomField9";
        public const string CustomField10 = "CustomField10";
        public const string Idkey = "IDKEY";
        public const string GroupIDKEY = "GroupIDKEY";
        public const string TableName = "tablename";
    }

    /// <summary>
    /// Model class for the "dataextdetail" table.
    /// </summary>
    [DataContract(Name = "ServiceDataExtDetail")]
    public class DataExtDetail : IModel
    {
        #region Class Level Variables

        private string _ownerId = null;
        private string _dataExtName = null;
        private string _dataExtType = null;
        private string _dataExtValue = null;
        private string _ownerType = null;
        private string _customField1 = null;
        private string _customField2 = null;
        private string _customField3 = null;
        private string _customField4 = null;
        private string _customField5 = null;
        private string _customField6 = null;
        private string _customField7 = null;
        private string _customField8 = null;
        private string _customField9 = null;
        private string _customField10 = null;
        private string _idkey = null;
        private string _groupIDKEY = null;
        private string _tablename = null;

        #endregion

        #region Properties

        /// <summary>
        /// This property is mapped to the "OwnerID" field. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string OwnerID
        {
            get
            {
                return _ownerId;
            }
            set
            {
                _ownerId = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "DataExtName" field.
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string DataExtName
        {
            get
            {
                return _dataExtName;
            }
            set
            {
                _dataExtName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "DataExtType" field. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string DataExtType
        {
            get
            {
                return _dataExtType;
            }
            set
            {
                _dataExtType = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "DataExtValue" field. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string DataExtValue
        {
            get
            {
                return _dataExtValue;
            }
            set
            {
                _dataExtValue = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "OwnerType" field. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string OwnerType
        {
            get
            {
                return _ownerType;
            }
            set
            {
                _ownerType = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField1" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField1
        {
            get
            {
                return _customField1;
            }
            set
            {
                _customField1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField2" field. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField2
        {
            get
            {
                return _customField2;
            }
            set
            {
                _customField2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField3" field. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField3
        {
            get
            {
                return _customField3;
            }
            set
            {
                _customField3 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField4" field. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField4
        {
            get
            {
                return _customField4;
            }
            set
            {
                _customField4 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField5" field. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField5
        {
            get
            {
                return _customField5;
            }
            set
            {
                _customField5 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField6" field. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField6
        {
            get
            {
                return _customField6;
            }
            set
            {
                _customField6 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField7" field. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField7
        {
            get
            {
                return _customField7;
            }
            set
            {
                _customField7 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField8" field. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField8
        {
            get
            {
                return _customField8;
            }
            set
            {
                _customField8 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField9" field. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField9
        {
            get
            {
                return _customField9;
            }
            set
            {
                _customField9 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField10" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField10
        {
            get
            {
                return _customField10;
            }
            set
            {
                _customField10 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IDKEY" field. 
        /// </summary>
        [DataMember]
        public string Idkey
        {
            get
            {
                return _idkey;
            }
            set
            {
                _idkey = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "GroupIDKEY" field.
        /// </summary>
        [DataMember]
        public string GroupIDKEY
        {
            get
            {
                return _groupIDKEY;
            }
            set
            {
                _groupIDKEY = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "tablename" field. 
        /// </summary>
        [DataMember]
        public string TableName
        {
            get
            {
                return _tablename;
            }
            set
            {
                _tablename = value;
            }
        }

        #endregion
    }
}
