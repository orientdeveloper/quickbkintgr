//
// Class	:	Salestaxcode.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:44 AM
//
using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization;

namespace QBSynDal
{
    /// <summary>
    /// Class for the properties of the object
    /// </summary>
    public class SalestaxcodeFields
    {
        public const string ListID = "ListID";
        public const string TimeCreated = "TimeCreated";
        public const string TimeModified = "TimeModified";
        public const string EditSequence = "EditSequence";
        public const string Name = "Name";
        public const string IsActive = "IsActive";
        public const string IsTaxable = "IsTaxable";
        public const string Desc = "Desc";
        public const string ItemPurchaseTaxRef_ListID = "ItemPurchaseTaxRef_ListID";
        public const string ItemPurchaseTaxRef_FullName = "ItemPurchaseTaxRef_FullName";
        public const string ItemSalesTaxRef_ListID = "ItemSalesTaxRef_ListID";
        public const string ItemSalesTaxRef_FullName = "ItemSalesTaxRef_FullName";
        public const string Status = "Status";

    }
    /// <summary>
    /// Model class for the "salestaxcode" table.
    /// </summary>
    [DataContract(Name = "ServiceSalestaxcode")]
    public class Salestaxcode : IModel
    {
        #region Class Level Variables

        private string _listID = null;
        private string _timeCreated = null;
        private string _timeModified = null;
        private string _editSequence = null;
        private string _name = null;
        private bool? _isActive = null;
        private bool? _isTaxable = null;
        private string _desc = null;
        private string _itemPurchaseTaxRef_ListID = null;
        private string _itemPurchaseTaxRef_FullName = null;
        private string _itemSalesTaxRef_ListID = null;
        private string _itemSalesTaxRef_FullName = null;
        private string _status = null;

        #endregion

        #region Properties

        /// <summary>
        /// This property is mapped to the "ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ListID
        {
            get
            {
                return _listID;
            }
            set
            {

                _listID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeCreated" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TimeCreated
        {
            get
            {
                return _timeCreated;
            }
            set
            {

                _timeCreated = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeModified" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TimeModified
        {
            get
            {
                return _timeModified;
            }
            set
            {

                _timeModified = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "EditSequence" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string EditSequence
        {
            get
            {
                return _editSequence;
            }
            set
            {

                _editSequence = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Name" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {

                _name = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsActive" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public bool? IsActive
        {
            get
            {
                return _isActive;
            }
            set
            {
                _isActive = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsTaxable" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public bool? IsTaxable
        {
            get
            {
                return _isTaxable;
            }
            set
            {
                _isTaxable = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Desc" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Desc
        {
            get
            {
                return _desc;
            }
            set
            {
                    _desc = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ItemPurchaseTaxRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ItemPurchaseTaxRef_ListID
        {
            get
            {
                return _itemPurchaseTaxRef_ListID;
            }
            set
            {

                _itemPurchaseTaxRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ItemPurchaseTaxRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ItemPurchaseTaxRef_FullName
        {
            get
            {
                return _itemPurchaseTaxRef_FullName;
            }
            set
            {

                _itemPurchaseTaxRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ItemSalesTaxRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ItemSalesTaxRef_ListID
        {
            get
            {
                return _itemSalesTaxRef_ListID;
            }
            set
            {

                _itemSalesTaxRef_ListID = value;


            }
        }

        /// <summary>
        /// This property is mapped to the "ItemSalesTaxRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ItemSalesTaxRef_FullName
        {
            get
            {
                return _itemSalesTaxRef_FullName;
            }
            set
            {
                _itemSalesTaxRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Status" field. Length must be between 0 and 10 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {

                _status = value;
            }
        }

        #endregion
    }
}
