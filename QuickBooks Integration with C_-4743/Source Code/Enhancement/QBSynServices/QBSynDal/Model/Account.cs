//
// Class	:	Account.cs
// Author	:  	Synapse © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization;
using QBSynDal.List;

namespace QBSynDal
{
    /// <summary>
    /// Class for the properties of the object
    /// </summary>
    public class AccountFields
    {
        public const string ListID = "ListID";
        public const string TimeCreated = "TimeCreated";
        public const string TimeModified = "TimeModified";
        public const string EditSequence = "EditSequence";
        public const string Name = "Name";
        public const string FullName = "FullName";
        public const string IsActive = "IsActive";
        public const string ParentRef_ListID = "ParentRef_ListID";
        public const string ParentRef_FullName = "ParentRef_FullName";
        public const string Sublevel = "Sublevel";
        public const string AccountType = "AccountType";
        public const string DetailAccountType = "DetailAccountType";
        public const string AccountNumber = "AccountNumber";
        public const string BankNumber = "BankNumber";
        public const string LastCheckNumber = "LastCheckNumber";
        public const string Desc = "Desc";
        public const string Balance = "Balance";
        public const string TotalBalance = "TotalBalance";
        public const string CashFlowClassification = "CashFlowClassification";
        public const string SpecialAccountType = "SpecialAccountType";
        public const string SalesTaxCodeRef_ListID = "SalesTaxCodeRef_ListID";
        public const string SalesTaxCodeRef_FullName = "SalesTaxCodeRef_FullName";
        public const string IsTaxAccount = "IsTaxAccount";
        public const string TaxLineID = "TaxLineID";
        public const string TaxLineInfo = "TaxLineInfo";
        public const string CurrencyRef_ListID = "CurrencyRef_ListID";
        public const string CurrencyRef_FullName = "CurrencyRef_FullName";
        public const string CustomField1 = "CustomField1";
        public const string CustomField2 = "CustomField2";
        public const string CustomField3 = "CustomField3";
        public const string CustomField4 = "CustomField4";
        public const string CustomField5 = "CustomField5";
        public const string CustomField6 = "CustomField6";
        public const string CustomField7 = "CustomField7";
        public const string CustomField8 = "CustomField8";
        public const string CustomField9 = "CustomField9";
        public const string CustomField10 = "CustomField10";
        public const string Status = "Status";
    }

    /// <summary>
    /// Data Contract class for the "account" table.
    /// </summary>
    [DataContract(Name = "ServiceAccount")]
    public class Account : IModel
    {
        #region Class Level Variables
        private string _listID = null;
        private string _timeCreated = null;
        private string _timeModified = null;
        private string _editSequence = null;
        private string _name = null;
        private string _fullName = null;
        private bool? _isActive = null;
        private string _parentRef_ListID = null;
        private string _parentRef_FullName = null;
        private int? _sublevel = null;
        private string _accountType = null;
        private string _detailAccountType = null;
        private string _accountNumber = null;
        private string _bankNumber = null;
        private string _lastCheckNumber = null;
        private string _desc = null;
        private double? _balance = null;
        private double? _totalBalance = null;
        private string _cashFlowClassification = null;
        private string _specialAccountType = null;
        private string _salesTaxCodeRef_ListID = null;
        private string _salesTaxCodeRef_FullName = null;
        private bool? _isTaxAccount = null;
        private int? _taxLineID = null;
        private string _taxLineInfo = null;
        private string _currencyRef_ListID = null;
        private string _currencyRef_FullName = null;
        private string _customField1 = null;
        private string _customField2 = null;
        private string _customField3 = null;
        private string _customField4 = null;
        private string _customField5 = null;
        private string _customField6 = null;
        private string _customField7 = null;
        private string _customField8 = null;
        private string _customField9 = null;
        private string _customField10 = null;
        private string _status = null;

        #endregion

        #region Properties
        /// <summary>
        /// This property is mapped to the "ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ListID
        {
            get
            {
                return _listID;
            }
            set
            {
                _listID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeCreated" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public string TimeCreated
        {
            get
            {
                return _timeCreated;
            }
            set
            {
                _timeCreated = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeModified" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public string TimeModified
        {
            get
            {
                return _timeModified;
            }
            set
            {
                _timeModified = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "EditSequence" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public string EditSequence
        {
            get
            {
                return _editSequence;
            }
            set
            {
                _editSequence = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Name" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string FullName
        {
            get
            {
                return _fullName;
            }
            set
            {
                _fullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsActive" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public bool? IsActive
        {
            get
            {
                return _isActive;
            }
            set
            {
                _isActive = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ParentRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public string ParentRef_ListID
        {
            get
            {
                return _parentRef_ListID;
            }
            set
            {
                _parentRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ParentRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public string ParentRef_FullName
        {
            get
            {
                return _parentRef_FullName;
            }
            set
            {
                _parentRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Sublevel" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public int? Sublevel
        {
            get
            {
                return _sublevel;
            }
            set
            {
                _sublevel = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AccountType" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public string AccountType
        {
            get
            {
                return _accountType;
            }
            set
            {
                _accountType = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "DetailAccountType" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public string DetailAccountType
        {
            get
            {
                return _detailAccountType;
            }
            set
            {
                _detailAccountType = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AccountNumber" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public string AccountNumber
        {
            get
            {
                return _accountNumber;
            }
            set
            {
                _accountNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BankNumber" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string BankNumber
        {
            get
            {
                return _bankNumber;
            }
            set
            {
                _bankNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "LastCheckNumber" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public string LastCheckNumber
        {
            get
            {
                return _lastCheckNumber;
            }
            set
            {
                _lastCheckNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Desc" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public string Desc
        {
            get
            {
                return _desc;
            }
            set
            {
                _desc = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Balance" field.  
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public double? Balance
        {
            get
            {
                return _balance;
            }
            set
            {
                _balance = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TotalBalance" field.  
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public double? TotalBalance
        {
            get
            {
                return _totalBalance;
            }
            set
            {
                _totalBalance = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CashFlowClassification" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public string CashFlowClassification
        {
            get
            {
                return _cashFlowClassification;
            }
            set
            {
                _cashFlowClassification = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SpecialAccountType" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public string SpecialAccountType
        {
            get
            {
                return _specialAccountType;
            }
            set
            {
                _specialAccountType = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SalesTaxCodeRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public string SalesTaxCodeRef_ListID
        {
            get
            {
                return _salesTaxCodeRef_ListID;
            }
            set
            {
                _salesTaxCodeRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SalesTaxCodeRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public string SalesTaxCodeRef_FullName
        {
            get
            {
                return _salesTaxCodeRef_FullName;
            }
            set
            {
                _salesTaxCodeRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsTaxAccount" field.  
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public bool? IsTaxAccount
        {
            get
            {
                return _isTaxAccount;
            }
            set
            {
                _isTaxAccount = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TaxLineID" field.  
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public int? TaxLineID
        {
            get
            {
                return _taxLineID;
            }
            set
            {
                _taxLineID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TaxLineInfo" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public string TaxLineInfo
        {
            get
            {
                return _taxLineInfo;
            }
            set
            {
                _taxLineInfo = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CurrencyRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public string CurrencyRef_ListID
        {
            get
            {
                return _currencyRef_ListID;
            }
            set
            {
                _currencyRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CurrencyRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public string CurrencyRef_FullName
        {
            get
            {
                return _currencyRef_FullName;
            }
            set
            {
                _currencyRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField1" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public string CustomField1
        {
            get
            {
                return _customField1;
            }
            set
            {
                _customField1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField2" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public string CustomField2
        {
            get
            {
                return _customField2;
            }
            set
            {
                _customField2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField3" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public string CustomField3
        {
            get
            {
                return _customField3;
            }
            set
            {
                _customField3 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField4" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public string CustomField4
        {
            get
            {
                return _customField4;
            }
            set
            {
                _customField4 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField5" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public string CustomField5
        {
            get
            {
                return _customField5;
            }
            set
            {
                _customField5 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField6" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public string CustomField6
        {
            get
            {
                return _customField6;
            }
            set
            {
                _customField6 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField7" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public string CustomField7
        {
            get
            {
                return _customField7;
            }
            set
            {
                _customField7 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField8" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public string CustomField8
        {
            get
            {
                return _customField8;
            }
            set
            {
                _customField8 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField9" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public string CustomField9
        {
            get
            {
                return _customField9;
            }
            set
            {
                _customField9 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField10" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public string CustomField10
        {
            get
            {
                return _customField10;
            }
            set
            {
                _customField10 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Status" field. Length must be between 0 and 10 characters. 
        /// Comment found in the Database for this field: .
        /// </summary> 
        [DataMember]
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        #endregion
    }
}
