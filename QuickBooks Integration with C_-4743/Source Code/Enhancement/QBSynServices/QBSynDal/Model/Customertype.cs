//
// Class	:	CustomertypeBase.cs
// Author	:  	Ignyte Software © 2006
// Date		:	4/2/2013 10:11:45 AM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization;
using QBSynDal;

namespace QBSynDal
{

    /// <summary>
    /// Class for the properties of the object
    /// </summary>
    public class CustomertypeFields
    {
        public const string ListID = "ListID";
        public const string TimeCreated = "TimeCreated";
        public const string TimeModified = "TimeModified";
        public const string EditSequence = "EditSequence";
        public const string Name = "Name";
        public const string FullName = "FullName";
        public const string IsActive = "IsActive";
        public const string ParentRef_ListID = "ParentRef_ListID";
        public const string ParentRef_FullName = "ParentRef_FullName";
        public const string Sublevel = "Sublevel";
        public const string Status = "Status";
        public const string IsSyn = "IsSyn";
        public const string SynDate = "SynDate";
    }

    /// <summary>
    /// Data access class for the "customertype" table.
    /// </summary>
    [DataContract(Name = "ServiceCustomertype")]
    public class Customertype : IModel
    {
        #region Class Level Variables

        private string _listID = null;
        private string _timeCreated = null;
        private string _timeModified = null;
        private string _editSequence = null;
        private string _name = null;
        private string _fullName = null;
        private bool? _isActive = null;
        private string _parentRef_ListID = null;
        private string _parentRef_FullName = null;
        private int? _sublevel = null;
        private string _status = null;

        #endregion

        #region Properties

        /// <summary>
        /// This property is mapped to the "ListID" field. 
        /// </summary>
        [DataMember]
        public string ListID
        {
            get
            {
                return _listID;
            }
            set
            {
                _listID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeCreated" field. 
        /// </summary>
        [DataMember]
        public string TimeCreated
        {
            get
            {
                return _timeCreated;
            }
            set
            {
                _timeCreated = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeModified" field. 
        /// </summary>
        [DataMember]
        public string TimeModified
        {
            get
            {
                return _timeModified;
            }
            set
            {
                _timeModified = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "EditSequence" field. 
        /// </summary>
        [DataMember]
        public string EditSequence
        {
            get
            {
                return _editSequence;
            }
            set
            {
                _editSequence = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Name" field.
        /// </summary>
        [DataMember]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string FullName
        {
            get
            {
                return _fullName;
            }
            set
            {
                _fullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsActive" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public bool? IsActive
        {
            get
            {
                return _isActive;
            }
            set
            {
                _isActive = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ParentRef_ListID" field.
        /// </summary>
        [DataMember]
        public string ParentRef_ListID
        {
            get
            {
                return _parentRef_ListID;
            }
            set
            {
                _parentRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ParentRef_FullName" field. 
        /// </summary>
        [DataMember]
        public string ParentRef_FullName
        {
            get
            {
                return _parentRef_FullName;
            }
            set
            {
                _parentRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Sublevel" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public int? Sublevel
        {
            get
            {
                return _sublevel;
            }
            set
            {
                _sublevel = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Status" field. 
        /// </summary>
        [DataMember]
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        #endregion
    }
}
