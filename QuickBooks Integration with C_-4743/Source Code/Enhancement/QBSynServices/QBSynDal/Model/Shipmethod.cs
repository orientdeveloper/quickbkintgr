//
// Class	:	ShipmethodBase.cs
// Author	:  	Synapse India © 2013
// Date		:	22/07/2013 10:55:56 AM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization;
using QBSynDal.List;

namespace QBSynDal
{

	/// <summary>
	/// Class for the properties of the object
	/// </summary>
	public class ShipmethodFields
	{
		public const string ListID                    = "ListID";
		public const string TimeCreated               = "TimeCreated";
		public const string TimeModified              = "TimeModified";
		public const string EditSequence              = "EditSequence";
		public const string Name                      = "Name";
		public const string IsActive                  = "IsActive";
		public const string Status                    = "Status";
	}
	
	/// <summary>
	/// Modal class for the "shipmethod" table.
	/// </summary>
    [DataContract(Name = "ServiceShipmethod")]
    public class Shipmethod : IModel
	{
		
		#region Class Level Variables
		
		private string         	_listID                  	= null;
		private string         	_timeCreated             	= null;
		private string         	_timeModified            	= null;
		private string         	_editSequence            	= null;
		private string         	_name                    	= null;
		private bool?          	_isActive                	= null;
		private string         	_status                  	= null;
		
		#endregion
		
		#region Properties

		/// <summary>
		/// This property is mapped to the "ListID" field. Length must be between 0 and 255 characters. 
		/// Comment found in the Database for this field: .
		/// </summary>
        [DataMember]
        public string ListID
		{
			get 
			{ 
				return _listID; 
			}
			set 
			{
				_listID = value;
			}
		}

		/// <summary>
		/// This property is mapped to the "TimeCreated" field. Length must be between 0 and 255 characters. 
		/// Comment found in the Database for this field: .
		/// </summary>
        [DataMember]
        public string TimeCreated
		{
			get 
			{ 
				return _timeCreated; 
			}
			set 
			{
				_timeCreated = value;
			}
		}

		/// <summary>
		/// This property is mapped to the "TimeModified" field. Length must be between 0 and 255 characters. 
		/// Comment found in the Database for this field: .
		/// </summary>
        [DataMember]
        public string TimeModified
		{
			get 
			{ 
				return _timeModified; 
			}
			set 
			{
				_timeModified = value;
			}
		}

		/// <summary>
		/// This property is mapped to the "EditSequence" field. Length must be between 0 and 255 characters. 
		/// Comment found in the Database for this field: .
		/// </summary>
        [DataMember]
        public string EditSequence
		{
			get 
			{ 
				return _editSequence; 
			}
			set 
			{
				_editSequence = value;
			}
		}

		/// <summary>
		/// This property is mapped to the "Name" field. Length must be between 0 and 255 characters. 
		/// Comment found in the Database for this field: .
		/// </summary>
        [DataMember]
        public string Name
		{
			get 
			{ 
				return _name; 
			}
			set 
			{
				_name = value;            
			}
		}

		/// <summary>
		/// This property is mapped to the "IsActive" field.  
		/// Comment found in the Database for this field: .
		/// </summary>
        [DataMember]
        public bool? IsActive
		{
			get 
			{ 
				return _isActive; 
			}
			set 
			{
				_isActive = value;
			}
		}

		/// <summary>
		/// This property is mapped to the "Status" field. Length must be between 0 and 10 characters. 
		/// Comment found in the Database for this field: .
		/// </summary>
        [DataMember]
        public string Status
		{
			get 
			{ 
				return _status; 
			}
			set 
			{
				_status = value;
			}
		}

		#endregion
		

	}
}
