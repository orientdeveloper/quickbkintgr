//
// Class	:	Transaction.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:58 AM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization;

namespace QBSynDal
{

    /// <summary>
    /// Class for the properties of the object
    /// </summary>
    public class TransactionFields
    {
        public const string TxnID = "TxnID";
        public const string TxnType = "TxnType";
        public const string TxnLineID = "TxnLineID";
        public const string TimeCreated = "TimeCreated";
        public const string TimeModified = "TimeModified";
        public const string EntityRef_ListID = "EntityRef_ListID";
        public const string EntityRef_FullName = "EntityRef_FullName";
        public const string AccountRef_ListID = "AccountRef_ListID";
        public const string AccountRef_FullName = "AccountRef_FullName";
        public const string TxnDate = "TxnDate";
        public const string RefNumber = "RefNumber";
        public const string Amount = "Amount";
        public const string CurrencyRef_ListID = "CurrencyRef_ListID";
        public const string CurrencyRef_FullName = "CurrencyRef_FullName";
        public const string ExchangeRate = "ExchangeRate";
        public const string AmountInHomeCurrency = "AmountInHomeCurrency";
        public const string Memo = "Memo";
        public const string Status = "Status";

    }

    /// <summary>
    /// Model class for the "transaction" table.
    /// </summary>
    [DataContract(Name = "ServiceTransaction")]
    public class Transaction : IModel
    {

        #region Class Level Variables

        private string _txnID = null;
        private string _txnType = null;
        private string _txnLineID = null;
        private string _timeCreated = null;
        private string _timeModified = null;
        private string _entityRef_ListID = null;
        private string _entityRef_FullName = null;
        private string _accountRef_ListID = null;
        private string _accountRef_FullName = null;
        private DateTime? _txnDate = null;
        private string _refNumber = null;
        private double? _amount = null;
        private string _currencyRef_ListID = null;
        private string _currencyRef_FullName = null;
        private double? _exchangeRate = null;
        private double? _amountInHomeCurrency = null;
        private string _memo = null;
        private string _status = null;

        #endregion

        #region Properties

        /// <summary>
        /// This property is mapped to the "TxnID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TxnID
        {
            get
            {
                return _txnID;
            }
            set
            {
                _txnID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TxnType" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TxnType
        {
            get
            {
                return _txnType;
            }
            set
            {
                _txnType = value;
            }
        }
        
        /// <summary>
        /// This property is mapped to the "TxnLineID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TxnLineID
        {
            get
            {
                return _txnLineID;
            }
            set
            {
                _txnLineID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeCreated" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TimeCreated
        {
            get
            {
                return _timeCreated;
            }
            set
            {
                _timeCreated = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeModified" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TimeModified
        {
            get
            {
                return _timeModified;
            }
            set
            {
                _timeModified = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "EntityRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string EntityRef_ListID
        {
            get
            {
                return _entityRef_ListID;
            }
            set
            {
                _entityRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "EntityRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string EntityRef_FullName
        {
            get
            {
                return _entityRef_FullName;
            }
            set
            {
                _entityRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AccountRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string AccountRef_ListID
        {
            get
            {
                return _accountRef_ListID;
            }
            set
            {
                _accountRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AccountRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string AccountRef_FullName
        {
            get
            {
                return _accountRef_FullName;
            }
            set
            {
                _accountRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TxnDate" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public DateTime? TxnDate
        {
            get
            {
                return _txnDate;
            }
            set
            {
                _txnDate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "RefNumber" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string RefNumber
        {
            get
            {
                return _refNumber;
            }
            set
            {
                _refNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Amount" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? Amount
        {
            get
            {
                return _amount;
            }
            set
            {
                _amount = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CurrencyRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CurrencyRef_ListID
        {
            get
            {
                return _currencyRef_ListID;
            }
            set
            {
                _currencyRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CurrencyRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CurrencyRef_FullName
        {
            get
            {
                return _currencyRef_FullName;
            }
            set
            {
                _currencyRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ExchangeRate" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? ExchangeRate
        {
            get
            {
                return _exchangeRate;
            }
            set
            {
                _exchangeRate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AmountInHomeCurrency" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? AmountInHomeCurrency
        {
            get
            {
                return _amountInHomeCurrency;
            }
            set
            {
                _amountInHomeCurrency = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Memo" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Memo
        {
            get
            {
                return _memo;
            }
            set
            {
                    _memo = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Status" field. Length must be between 0 and 10 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        #endregion

    }
}
