//
// Class	:	QBPushPurchaseorderlinegroupdetail.cs
// Author	:  	SynapseIndia © 2013
// Date		:	8/19/2013 11:56:54 AM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization;

namespace QBSynDal
{

    /// <summary>
    /// Class for the properties of the object
    /// </summary>
    public class QBPushPurchaseorderlinegroupdetailFields
    {
        public const string ItemGroupRef_ListID = "ItemGroupRef_ListID";
        public const string ItemGroupRef_FullName = "ItemGroupRef_FullName";
        public const string Quantity = "Quantity";
        public const string UnitOfMeasure = "UnitOfMeasure";
        public const string InventorySiteLocationRef_ListID = "InventorySiteLocationRef_ListID";
        public const string InventorySiteLocationRef_FullName = "InventorySiteLocationRef_FullName";
        public const string Idkey = "IDKEY";
        public const string DataExt_OwnerID = "DataExt_OwnerID";
        public const string DataExt_DataExtName = "DataExt_DataExtName";
        public const string DataExt_DataExtValue = "DataExt_DataExtValue";
    }

    /// <summary>
    /// Model class for the "QBPushPurchaseorderlinegroupdetail" table.
    /// </summary>
    [DataContract(Name = "ServiceQBPushPurchaseorderlinegroupdetail")]
    public class QBPushPurchaseorderlinegroupdetail
    {
        #region Class Level Variables

        private string _itemGroupRef_ListID = null;
        private string _itemGroupRef_FullName = null;
        private double? _quantity = null;
        private string _unitofmeasure = null;
        private string _inventorysitelocationref_ListID = null;
        private string _inventorysitelocationref_FullName = null;
        private long? _idkey = null;
        private Guid? _dataext_OwnerID = null;
        private string _dataext_DataExtName = null;
        private string _dataext_DataExtValue = null;

        #endregion

        #region Properties

        /// <summary>
        /// This property is mapped to the "ItemGroupRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ItemGroupRef_ListID
        {
            get
            {
                return _itemGroupRef_ListID;
            }
            set
            {
                _itemGroupRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ItemGroupRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ItemGroupRef_FullName
        {
            get
            {
                return _itemGroupRef_FullName;
            }
            set
            {
                _itemGroupRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsPrintItemsInGroup" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? Quantity
        {
            get
            {
                return _quantity;
            }
            set
            {
                _quantity = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "UnitOfMeasure" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string UnitOfMeasure
        {
            get
            {
                return _unitofmeasure;
            }
            set
            {
                _unitofmeasure = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField2" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string InventorySiteLocationRef_ListID
        {
            get
            {
                return _inventorysitelocationref_ListID;
            }
            set
            {
                _inventorysitelocationref_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField3" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string InventorySiteLocationRef_FullName
        {
            get
            {
                return _inventorysitelocationref_ListID;
            }
            set
            {
                _inventorysitelocationref_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IDKEY" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public long? Idkey
        {
            get
            {
                return _idkey;
            }
            set
            {
                _idkey = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "DataExt_OwnerID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public Guid? DataExt_OwnerID
        {
            get
            {
                return _dataext_OwnerID;
            }
            set
            {
                _dataext_OwnerID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "DataExt_DataExtName" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string DataExt_DataExtName
        {
            get
            {
                return _dataext_DataExtName;
            }
            set
            {
                _dataext_DataExtName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "DataExt_DataExtValue" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string DataExt_DataExtValue
        {
            get
            {
                return _dataext_DataExtValue;
            }
            set
            {
                _dataext_DataExtValue = value;
            }
        }

        #endregion

    }
}
