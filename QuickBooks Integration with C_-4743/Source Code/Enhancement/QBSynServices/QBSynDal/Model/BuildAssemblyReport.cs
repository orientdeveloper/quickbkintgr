﻿
//
// Class	:	COGSReport.cs
// Author	:  	SynapseIndia © 2013
// Date		:	5/3/2013 10:11:42 AM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Data.SqlClient;
using System.Runtime.Serialization;

namespace QBSynDal
{
    /// <summary>
    /// Class for the properties of the object
    /// </summary>
    public class BuildAssemblyReportFields
    {
        public const string TxnNumber = "TxnNumber";
        public const string TxnType = "Type";
        public const string Date = "Date";
        public const string RefNumber = "RefNumber";
        public const string Customer_ListID = "CustomerRef_ListID";
        public const string Name = "Name";
        public const string Memo = "Memo";
        public const string ShipDate = "ShipDate";
        public const string DeliveryDate = "DeliveryDate";
        public const string FOB = "FOB";
        public const string ShipMethod = "ShipMethod";
        public const string Item_ListID = "ItemRef_ListID";
        public const string Item = "Item";
        public const string Account_ListID = "AccountRef_ListID";
        public const string Account = "Account";
        public const string Class_ListID = "ClassRef_ListID";
        public const string Class = "Class";
        public const string SalesRep = "SalesRep";
        public const string SalesTaxCode = "SalesTaxCode";
        public const string ClearedStatus = "ClearedStatus";
        public const string SplitAccount = "SplitAccount";
        public const string SplitAccount_ListID = "SplitAccount_ListID";
        public const string Quantity = "Quantity";
        public const string Debit = "Debit";
        public const string Credit = "Credit";
        public const string ExchangeRate = "ExchangeRate";
        public const string ModifiedTime = "ModifiedTime";
        public const string Balance = "Balance";
        public const string AccountType = "AccountType";
    }

    [DataContract(Name = "ServiceBuildAssemblyReport")]
    public class BuildAssemblyReport : IModel
    {
        #region Class Level Variables
        private int? _txnNumber = null;
        private string _txnType = null;
        private DateTime? _Date = null;
        private string _RefNumber = null;
        private string _Customer_ListID = null;
        private string _Name = null;
        private string _Memo = null;
        private DateTime? _ShipDate = null;
        private DateTime? _DeliveryDate = null;
        private string _FOB = null;
        private string _ShipMethod = null;
        private string _Item_ListID = null;
        private string _Item = null;
        private string _Account_ListID = null;
        private string _Account = null;
        private string _class_ListID = null;
        private string _class = null;
        private string _salesRep = null;
        private string _SalesTaxCode = null;
        private string _ClearedStatus = null;
        private string _SplitAccount = null;
        private string _SplitAccount_ListID = null;
        private double? _Quantity = null;
        private double? _Debit = null;
        private double? _Credit = null;
        private double? _ExchangeRate = null;
        private double? _balance = null;
        private DateTime? _modifiedTime = null;
        private string _accountType = null;

        #endregion

        #region Properties
        /// <summary>
        /// This property is mapped to the "TxnNumber" field. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public int? TxnNumber
        {
            get
            {
                return _txnNumber;
            }
            set
            {
                _txnNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TxnType" field. 
        /// </summary>
        [DataMember]
        public string TxnType
        {
            get
            {
                return _txnType;
            }
            set
            {
                _txnType = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Date" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public DateTime? Date
        {
            get
            {
                return _Date;
            }
            set
            {
                _Date = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "RefNumber" field. 
        /// </summary>
        [DataMember]
        public string RefNumber
        {
            get
            {
                return _RefNumber;
            }
            set
            {
                _RefNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Customer_ListID" field. 
        /// </summary>
        [DataMember]
        public string Customer_ListID
        {
            get
            {
                return _Customer_ListID;
            }
            set
            {
                _Customer_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Name" field. 
        /// </summary>
        [DataMember]
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Memo" field. 
        /// </summary>
        [DataMember]
        public string Memo
        {
            get
            {
                return _Memo;
            }
            set
            {
                _Memo = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipDate" field. 
        /// </summary>
        [DataMember]
        public DateTime? ShipDate
        {
            get
            {
                return _ShipDate;
            }
            set
            {
                _ShipDate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "DeliveryDate" field. 
        /// </summary>
        [DataMember]
        public DateTime? DeliveryDate
        {
            get
            {
                return _DeliveryDate;
            }
            set
            {
                _DeliveryDate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "FOB" field.
        /// </summary>
        [DataMember]
        public string FOB
        {
            get
            {
                return _FOB;
            }
            set
            {
                _FOB = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ShipMethod" field. 
        /// </summary>
        [DataMember]
        public string ShipMethod
        {
            get
            {
                return _ShipMethod;
            }
            set
            {
                _ShipMethod = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Item_ListID" field. 
        /// </summary>
        [DataMember]
        public string Item_ListID
        {
            get
            {
                return _Item_ListID;
            }
            set
            {
                _Item_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Item" field. 
        /// </summary>
        [DataMember]
        public string Item
        {
            get
            {
                return _Item;
            }
            set
            {
                _Item = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Account_ListID" field.
        /// </summary>
        [DataMember]
        public string Account_ListID
        {
            get
            {
                return _Account_ListID;
            }
            set
            {
                _Account_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Account" field. 
        /// </summary>
        [DataMember]
        public string Account
        {
            get
            {
                return _Account;
            }
            set
            {
                _Account = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Class_ListID" field.
        /// </summary>
        [DataMember]
        public string Class_ListID
        {
            get
            {
                return _class_ListID;
            }
            set
            {
                _class_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Class" field.
        /// </summary>
        [DataMember]
        public string Class
        {
            get
            {
                return _class;
            }
            set
            {
                _class = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SalesTaxCode" field. 
        /// </summary>
        [DataMember]
        public string SalesTaxCode
        {
            get
            {
                return _SalesTaxCode;
            }
            set
            {
                _SalesTaxCode = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ClearedStatus" field.
        /// </summary>
        [DataMember]
        public string ClearedStatus
        {
            get
            {
                return _ClearedStatus;
            }
            set
            {
                _ClearedStatus = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SplitAccount" field. 
        /// </summary>
        [DataMember]
        public string SplitAccount
        {
            get
            {
                return _SplitAccount;
            }
            set
            {
                _SplitAccount = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SplitAccount_ListID" field. 
        /// </summary>
        [DataMember]
        public string SplitAccount_ListID
        {
            get
            {
                return _SplitAccount_ListID;
            }
            set
            {
                _SplitAccount_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Quanity" field. 
        /// </summary>
        [DataMember]
        public double? Quanity
        {
            get
            {
                return _Quantity;
            }
            set
            {
                _Quantity = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Debit" field. 
        /// </summary>
        [DataMember]
        public double? Debit
        {
            get
            {
                return _Debit;
            }
            set
            {
                _Debit = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Credit" field. 
        /// </summary>
        [DataMember]
        public double? Credit
        {
            get
            {
                return _Credit;
            }
            set
            {
                _Credit = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Balance" field.
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? ExchangeRate
        {
            get
            {
                return _ExchangeRate;
            }
            set
            {
                _ExchangeRate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ModifiedTime" field.
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public DateTime? ModifiedTime
        {
            get
            {
                return _modifiedTime;
            }
            set
            {
                _modifiedTime = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AccountType" field.
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string AccountType
        {
            get
            {
                return _accountType;
            }
            set
            {
                _accountType = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Balance" field.
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? Balance
        {
            get
            {
                return _balance;
            }
            set
            {
                _balance = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SalesRep" field.
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SalesRep
        {
            get
            {
                return _salesRep;
            }
            set
            {
                _salesRep = value;
            }
        }
        #endregion
    }
}
