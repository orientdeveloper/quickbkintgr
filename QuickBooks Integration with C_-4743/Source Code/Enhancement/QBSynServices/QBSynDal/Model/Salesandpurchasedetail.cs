//
// Class	:	Salesandpurchasedetail.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:43 AM
//
using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization;

namespace QBSynDal
{
    /// <summary>
    /// Class for the properties of the object
    /// </summary>
    public class SalesandpurchasedetailFields
    {
        public const string SalesDesc = "SalesDesc";
        public const string SalesPrice = "SalesPrice";
        public const string IncomeAccountRef_ListID = "IncomeAccountRef_ListID";
        public const string IncomeAccountRef_FullName = "IncomeAccountRef_FullName";
        public const string PurchaseDesc = "PurchaseDesc";
        public const string PurchaseCost = "PurchaseCost";
        public const string PurchaseTaxCodeRef_ListID = "PurchaseTaxCodeRef_ListID";
        public const string PurchaseTaxCodeRef_FullName = "PurchaseTaxCodeRef_FullName";
        public const string ExpenseAccountRef_ListID = "ExpenseAccountRef_ListID";
        public const string ExpenseAccountRef_FullName = "ExpenseAccountRef_FullName";
        public const string PrefVendorRef_ListID = "PrefVendorRef_ListID";
        public const string PrefVendorRef_FullName = "PrefVendorRef_FullName";
        public const string CustomField1 = "CustomField1";
        public const string CustomField2 = "CustomField2";
        public const string CustomField3 = "CustomField3";
        public const string CustomField4 = "CustomField4";
        public const string CustomField5 = "CustomField5";
        public const string CustomField6 = "CustomField6";
        public const string CustomField7 = "CustomField7";
        public const string CustomField8 = "CustomField8";
        public const string CustomField9 = "CustomField9";
        public const string CustomField10 = "CustomField10";
        public const string Idkey = "IDKEY";
        public const string GroupIDKEY = "GroupIDKEY";

    }
    /// <summary>
    /// Model class for the "salesandpurchasedetail" table.
    /// </summary>
    [DataContract(Name = "ServiceSalesandpurchasedetail")]
    public class Salesandpurchasedetail : IModel
    {
        #region Class Level Variables

        private string _salesDesc = null;
        private string _salesPrice = null;
        private string _incomeAccountRef_ListID = null;
        private string _incomeAccountRef_FullName = null;
        private string _purchaseDesc = null;
        private string _purchaseCost = null;
        private string _purchaseTaxCodeRef_ListID = null;
        private string _purchaseTaxCodeRef_FullName = null;
        private string _expenseAccountRef_ListID = null;
        private string _expenseAccountRef_FullName = null;
        private string _prefVendorRef_ListID = null;
        private string _prefVendorRef_FullName = null;
        private string _customField1 = null;
        private string _customField2 = null;
        private string _customField3 = null;
        private string _customField4 = null;
        private string _customField5 = null;
        private string _customField6 = null;
        private string _customField7 = null;
        private string _customField8 = null;
        private string _customField9 = null;
        private string _customField10 = null;
        private string _idkey = null;
        private string _groupIDKEY = null;
        #endregion

        #region Properties

        /// <summary>
        /// This property is mapped to the "SalesDesc" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SalesDesc
        {
            get
            {
                return _salesDesc;
            }
            set
            {
                    _salesDesc = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SalesPrice" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SalesPrice
        {
            get
            {
                return _salesPrice;
            }
            set
            {
                _salesPrice = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IncomeAccountRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string IncomeAccountRef_ListID
        {
            get
            {
                return _incomeAccountRef_ListID;
            }
            set
            {
                _incomeAccountRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IncomeAccountRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string IncomeAccountRef_FullName
        {
            get
            {
                return _incomeAccountRef_FullName;
            }
            set
            {
                _incomeAccountRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "PurchaseDesc" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string PurchaseDesc
        {
            get
            {
                return _purchaseDesc;
            }
            set
            {
                    _purchaseDesc = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "PurchaseCost" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string PurchaseCost
        {
            get
            {
                return _purchaseCost;
            }
            set
            {
                _purchaseCost = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "PurchaseTaxCodeRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string PurchaseTaxCodeRef_ListID
        {
            get
            {
                return _purchaseTaxCodeRef_ListID;
            }
            set
            {
                _purchaseTaxCodeRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "PurchaseTaxCodeRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string PurchaseTaxCodeRef_FullName
        {
            get
            {
                return _purchaseTaxCodeRef_FullName;
            }
            set
            {
                _purchaseTaxCodeRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ExpenseAccountRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ExpenseAccountRef_ListID
        {
            get
            {
                return _expenseAccountRef_ListID;
            }
            set
            {
                _expenseAccountRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ExpenseAccountRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ExpenseAccountRef_FullName
        {
            get
            {
                return _expenseAccountRef_FullName;
            }
            set
            {

                _expenseAccountRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "PrefVendorRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string PrefVendorRef_ListID
        {
            get
            {
                return _prefVendorRef_ListID;
            }
            set
            {
                _prefVendorRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "PrefVendorRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string PrefVendorRef_FullName
        {
            get
            {
                return _prefVendorRef_FullName;
            }
            set
            {
                _prefVendorRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField1" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField1
        {
            get
            {
                return _customField1;
            }
            set
            {
                _customField1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField2" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField2
        {
            get
            {
                return _customField2;
            }
            set
            {

                _customField2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField3" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField3
        {
            get
            {
                return _customField3;
            }
            set
            {

                _customField3 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField4" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField4
        {
            get
            {
                return _customField4;
            }
            set
            {

                _customField4 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField5" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField5
        {
            get
            {
                return _customField5;
            }
            set
            {
                _customField5 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField6" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField6
        {
            get
            {
                return _customField6;
            }
            set
            {
                _customField6 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField7" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField7
        {
            get
            {
                return _customField7;
            }
            set
            {
                _customField7 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField8" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField8
        {
            get
            {
                return _customField8;
            }
            set
            {
                _customField8 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField9" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField9
        {
            get
            {
                return _customField9;
            }
            set
            {
                _customField9 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField10" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField10
        {
            get
            {
                return _customField10;
            }
            set
            {
                _customField10 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IDKEY" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Idkey
        {
            get
            {
                return _idkey;
            }
            set
            {
                _idkey = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "GroupIDKEY" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string GroupIDKEY
        {
            get
            {
                return _groupIDKEY;
            }
            set
            {
                _groupIDKEY = value;
            }
        }

        #endregion
    }
}
