//
// Class	:	Estimatelinedetail.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:43 AM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization;

namespace QBSynDal
{
    /// <summary>
    /// Class for the properties of the object
    /// </summary>
    public class EstimatelinedetailFields
    {
        public const string TxnLineID = "TxnLineID";
        public const string ItemRef_ListID = "ItemRef_ListID";
        public const string ItemRef_FullName = "ItemRef_FullName";
        public const string Desc = "Desc";
        public const string Quantity = "Quantity";
        public const string UnitOfMeasure = "UnitOfMeasure";
        public const string OverrideUOMSetRef_ListID = "OverrideUOMSetRef_ListID";
        public const string OverrideUOMSetRef_FullName = "OverrideUOMSetRef_FullName";
        public const string Rate = "Rate";
        public const string RatePercent = "RatePercent";
        public const string ClassRef_ListID = "ClassRef_ListID";
        public const string ClassRef_FullName = "ClassRef_FullName";
        public const string Amount = "Amount";
        public const string InventorySiteRef_ListID = "InventorySiteRef_ListID";
        public const string InventorySiteRef_FullName = "InventorySiteRef_FullName";
        public const string SalesTaxCodeRef_ListID = "SalesTaxCodeRef_ListID";
        public const string SalesTaxCodeRef_FullName = "SalesTaxCodeRef_FullName";
        public const string MarkupRate = "MarkupRate";
        public const string MarkupRatePercent = "MarkupRatePercent";
        public const string Other1 = "Other1";
        public const string Other2 = "Other2";
        public const string CustomField1 = "CustomField1";
        public const string CustomField2 = "CustomField2";
        public const string CustomField3 = "CustomField3";
        public const string CustomField4 = "CustomField4";
        public const string CustomField5 = "CustomField5";
        public const string CustomField6 = "CustomField6";
        public const string CustomField7 = "CustomField7";
        public const string CustomField8 = "CustomField8";
        public const string CustomField9 = "CustomField9";
        public const string CustomField10 = "CustomField10";
        public const string Idkey = "IDKEY";
        public const string GroupIDKEY = "GroupIDKEY";
    }
    /// <summary>
    /// Datacontract class for the "estimatelinedetail" table.
    /// </summary>
    [DataContract(Name = "ServiceEstimatelinedetail")]
    public class Estimatelinedetail : IModel
    {
        #region Class Level Variables
        private string _txnLineID = null;
        private string _itemRef_ListID = null;
        private string _itemRef_FullName = null;
        private string _desc = null;
        private string _quantity = null;
        private string _unitOfMeasure = null;
        private string _overrideUOMSetRef_ListID = null;
        private string _overrideUOMSetRef_FullName = null;
        private string _rate = null;
        private string _ratePercent = null;
        private string _classRef_ListID = null;
        private string _classRef_FullName = null;
        private double? _amount = null;
        private string _inventorySiteRef_ListID = null;
        private string _inventorySiteRef_FullName = null;
        private string _salesTaxCodeRef_ListID = null;
        private string _salesTaxCodeRef_FullName = null;
        private string _markupRate = null;
        private string _markupRatePercent = null;
        private string _other1 = null;
        private string _other2 = null;
        private string _customField1 = null;
        private string _customField2 = null;
        private string _customField3 = null;
        private string _customField4 = null;
        private string _customField5 = null;
        private string _customField6 = null;
        private string _customField7 = null;
        private string _customField8 = null;
        private string _customField9 = null;
        private string _customField10 = null;
        private string _idkey = null;
        private string _groupIDKEY = null;
        #endregion

        #region Properties

        /// <summary>
        /// This property is mapped to the "TxnLineID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TxnLineID
        {
            get
            {
                return _txnLineID;
            }
            set
            {
                _txnLineID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ItemRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ItemRef_ListID
        {
            get
            {
                return _itemRef_ListID;
            }
            set
            {
                _itemRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ItemRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ItemRef_FullName
        {
            get
            {
                return _itemRef_FullName;
            }
            set
            {
                _itemRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Desc" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Desc
        {
            get
            {
                return _desc;
            }
            set
            {
                _desc = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Quantity" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Quantity
        {
            get
            {
                return _quantity;
            }
            set
            {
                _quantity = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "UnitOfMeasure" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string UnitOfMeasure
        {
            get
            {
                return _unitOfMeasure;
            }
            set
            {
                _unitOfMeasure = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "OverrideUOMSetRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string OverrideUOMSetRef_ListID
        {
            get
            {
                return _overrideUOMSetRef_ListID;
            }
            set
            {
                _overrideUOMSetRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "OverrideUOMSetRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string OverrideUOMSetRef_FullName
        {
            get
            {
                return _overrideUOMSetRef_FullName;
            }
            set
            {
                _overrideUOMSetRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Rate" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Rate
        {
            get
            {
                return _rate;
            }
            set
            {
                _rate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "RatePercent" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string RatePercent
        {
            get
            {
                return _ratePercent;
            }
            set
            {
                _ratePercent = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ClassRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ClassRef_ListID
        {
            get
            {
                return _classRef_ListID;
            }
            set
            {
                _classRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ClassRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ClassRef_FullName
        {
            get
            {
                return _classRef_FullName;
            }
            set
            {
                _classRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Amount" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? Amount
        {
            get
            {
                return _amount;
            }
            set
            {
                _amount = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "InventorySiteRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>

        [DataMember]
        public string InventorySiteRef_ListID
        {
            get
            {
                return _inventorySiteRef_ListID;
            }
            set
            {
                _inventorySiteRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "InventorySiteRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string InventorySiteRef_FullName
        {
            get
            {
                return _inventorySiteRef_FullName;
            }
            set
            {
                _inventorySiteRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SalesTaxCodeRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SalesTaxCodeRef_ListID
        {
            get
            {
                return _salesTaxCodeRef_ListID;
            }
            set
            {

                _salesTaxCodeRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SalesTaxCodeRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SalesTaxCodeRef_FullName
        {
            get
            {
                return _salesTaxCodeRef_FullName;
            }
            set
            {
                _salesTaxCodeRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "MarkupRate" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string MarkupRate
        {
            get
            {
                return _markupRate;
            }
            set
            {

                _markupRate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "MarkupRatePercent" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>

        [DataMember]
        public string MarkupRatePercent
        {
            get
            {
                return _markupRatePercent;
            }
            set
            {
                _markupRatePercent = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Other1" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Other1
        {
            get
            {
                return _other1;
            }
            set
            {

                _other1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Other2" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Other2
        {
            get
            {
                return _other2;
            }
            set
            {
                _other2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField1" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField1
        {
            get
            {
                return _customField1;
            }
            set
            {
                _customField1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField2" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField2
        {
            get
            {
                return _customField2;
            }
            set
            {
                _customField2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField3" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField3
        {
            get
            {
                return _customField3;
            }
            set
            {
                _customField3 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField4" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField4
        {
            get
            {
                return _customField4;
            }
            set
            {
                _customField4 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField5" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField5
        {
            get
            {
                return _customField5;
            }
            set
            {
                _customField5 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField6" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField6
        {
            get
            {
                return _customField6;
            }
            set
            {
                _customField6 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField7" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField7
        {
            get
            {
                return _customField7;
            }
            set
            {
                _customField7 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField8" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField8
        {
            get
            {
                return _customField8;
            }
            set
            {
                _customField8 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField9" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField9
        {
            get
            {
                return _customField9;
            }
            set
            {
                _customField9 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField10" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField10
        {
            get
            {
                return _customField10;
            }
            set
            {
                _customField10 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IDKEY" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Idkey
        {
            get
            {
                return _idkey;
            }
            set
            {
                _idkey = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "GroupIDKEY" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string GroupIDKEY
        {
            get
            {
                return _groupIDKEY;
            }
            set
            {
                _groupIDKEY = value;
            }
        }
        #endregion
    }
}
