//
// Class	:	Transfer.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/19/2013 11:13:42 AM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization;


namespace QBSynDal
{
    /// <summary>
    /// Class for the properties of the object
    /// </summary>
    public class TransferFields
    {
        public const string TxnID = "TxnID";
        public const string TimeCreated = "TimeCreated";
        public const string TimeModified = "TimeModified";
        public const string EditSequence = "EditSequence";
        public const string Amount = "Amount";
        public const string ClassRef_ListID = "ClassRef_ListID";
        public const string ClassRef_FullName = "ClassRef_FullName";
        public const string FromAccountBalance = "FromAccountBalance";
        public const string Memo = "Memo";
        public const string ToAccountBalance = "ToAccountBalance";
        public const string TransferFromAccountRef_ListID = "TransferFromAccountRef_ListID";
        public const string TransferFromAccountRef_FullName = "TransferFromAccountRef_FullName";
        public const string TransferToAccountRef_ListID = "TransferToAccountRef_ListID";
        public const string TransferToAccountRef_FullName = "TransferToAccountRef_FullName";
        public const string TxnDate = "TxnDate";
        public const string TxnNumber = "TxnNumber";
        public const string CustomField1 = "CustomField1";
        public const string CustomField2 = "CustomField2";
        public const string CustomField3 = "CustomField3";
        public const string CustomField4 = "CustomField4";
        public const string CustomField5 = "CustomField5";
        public const string CustomField6 = "CustomField6";
        public const string CustomField7 = "CustomField7";
        public const string CustomField8 = "CustomField8";
        public const string CustomField9 = "CustomField9";
        public const string CustomField10 = "CustomField10";
        public const string Status = "Status";

    }

    /// <summary>
    /// Model class for the "Transfer" table.
    /// </summary>
    [DataContract(Name = "ServiceTransfer")]
    public class Transfer : IModel
    {
        #region Class Level Variables
        private string _txnID = null;
        private string _timeCreated = null;
        private string _timeModified = null;
        private string _editSequence = null;
        private double? _amount = null;
        private string _classRef_ListID = null;
        private string _classRef_FullName = null;
        private double? _fromAccountBalance = null;
        private string _memo = null;
        private double? _toAccountBalance = null;
        private string _transferFromAccountRef_ListID = null;
        private string _transferFromAccountRef_FullName = null;
        private string _transferToAccountRef_ListID = null;
        private string _transferToAccountRef_FullName = null;
        private DateTime? _txnDate = null;
        private int? _txnNumber = null;
        private string _customField1 = null;
        private string _customField2 = null;
        private string _customField3 = null;
        private string _customField4 = null;
        private string _customField5 = null;
        private string _customField6 = null;
        private string _customField7 = null;
        private string _customField8 = null;
        private string _customField9 = null;
        private string _customField10 = null;
        private string _status = null;

        #endregion

        #region Properties

        /// <summary>
        /// This property is mapped to the "TxnID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TxnID
        {
            get
            {
                return _txnID;
            }
            set
            {
                _txnID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeCreated" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TimeCreated
        {
            get
            {
                return _timeCreated;
            }
            set
            {
                _timeCreated = value;
            }
        }
        
        /// <summary>
        /// This property is mapped to the "TimeModified" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TimeModified
        {
            get
            {
                return _timeModified;
            }
            set
            {
                _timeModified = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Memo" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Memo
        {
            get
            {
                return _memo;
            }
            set
            {
                    _memo = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "EditSequence" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string EditSequence
        {
            get
            {
                return _editSequence;
            }
            set
            {
                _editSequence = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Amount" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? Amount
        {
            get
            {
                return _amount;
            }
            set
            {
                _amount = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ClassRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ClassRef_ListID
        {
            get
            {
                return _classRef_ListID;
            }
            set
            {

                _classRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ClassRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ClassRef_FullName
        {
            get
            {
                return _classRef_FullName;
            }
            set
            {
                _classRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "FromAccountBalance" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? FromAccountBalance
        {
            get
            {
                return _fromAccountBalance;
            }
            set
            {
                _fromAccountBalance = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ToAccountBalance" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? ToAccountBalance
        {
            get
            {
                return _toAccountBalance;
            }
            set
            {
                _toAccountBalance = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TransferFromAccountRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TransferFromAccountRef_ListID
        {
            get
            {
                return _transferFromAccountRef_ListID;
            }
            set
            {
                _transferFromAccountRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TransferFromAccountRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TransferFromAccountRef_FullName
        {
            get
            {
                return _transferFromAccountRef_FullName;
            }
            set
            {
                _transferFromAccountRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TransferToAccountRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TransferToAccountRef_ListID
        {
            get
            {
                return _transferToAccountRef_ListID;
            }
            set
            {
                _transferToAccountRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TransferToAccountRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TransferToAccountRef_FullName
        {
            get
            {
                return _transferToAccountRef_FullName;
            }
            set
            {
                _transferToAccountRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TxnDate" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public DateTime? TxnDate
        {
            get
            {
                return _txnDate;
            }
            set
            {
                _txnDate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TxnDate" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public int? TxnNumber
        {
            get
            {
                return _txnNumber;
            }
            set
            {
                _txnNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField1" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField1
        {
            get
            {
                return _customField1;
            }
            set
            {
                _customField1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField2" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField2
        {
            get
            {
                return _customField2;
            }
            set
            {
                _customField2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField3" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField3
        {
            get
            {
                return _customField3;
            }
            set
            {

                _customField3 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField4" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField4
        {
            get
            {
                return _customField4;
            }
            set
            {
                _customField4 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField5" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField5
        {
            get
            {
                return _customField5;
            }
            set
            {
                _customField5 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField6" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField6
        {
            get
            {
                return _customField6;
            }
            set
            {
                _customField6 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField7" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField7
        {
            get
            {
                return _customField7;
            }
            set
            {
                _customField7 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField8" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField8
        {
            get
            {
                return _customField8;
            }
            set
            {
                _customField8 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField9" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField9
        {
            get
            {
                return _customField9;
            }
            set
            {
                _customField9 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField10" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField10
        {
            get
            {
                return _customField10;
            }
            set
            {
                _customField10 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Status" field. Length must be between 0 and 10 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        #endregion
    }
}
