//
// Class	:	TransfRerport.cs
// Author	:  	SynapseIndia © 2013
// Date		:	05/08/2013 11:13:42 AM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Data.SqlClient;
using System.Runtime.Serialization;
using QBSynDal;

namespace QBSynDal
{
    /// <summary>
    /// Class for the properties of the object
    /// </summary>

    public class TransferReportFields
    {
        public const string TxnNumber = "TxnNumber";
        public const string TxnType = "TxnType";
        public const string Date = "Date";
        public const string RefNumber = "RefNumber";
        public const string Customer_ListID = "Customer_ListID";
        public const string Customer_Name = "Customer_Name";
        public const string SourceName = "SourceName";
        public const string Account_ListID = "Account_ListID";
        public const string Account = "Account";
        public const string Class_ListID = "Class_ListID";
        public const string Class = "Class";
        public const string ClearedStatus = "ClearedStatus";
        public const string SplitAccount = "SplitAccount";
        public const string SplitAccount_ListID = "SplitAccount_ListID";
        public const string Quantity = "Quantity";
        public const string Debit = "Debit";
        public const string Credit = "Credit";
        public const string ModifiedTime = "ModifiedTime";
        public const string AccountType = "AccountType";
    }

    /// <summary>
    /// Model class for the "TransferReport" table.
    /// </summary>
    [DataContract(Name = "ServiceTransferReport")]
    public class TransferReport : IModel
    {
        #region Class Level Variables
        private int? _txnNumber = null;
        private string _Type = null;
        private DateTime? _Date = null;
        private string _RefNumber = null;
        private string _Customer_ListID = null;
        private string _Customer_Name = null;
        private string _SourceName = null;
        private string _Account_ListID = null;
        private string _Account = null;
        private string _class_ListID = null;
        private string _class = null;
        private string _ClearedStatus = null;
        private string _SplitAccount_ListID = null;
        private string _SplitAccount = null;
        private double? _Quantity = null;
        private double? _Debit = null;
        private double? _Credit = null;
        private DateTime? _modifiedTime = null;
        private string _accountType = null;
        #endregion

        #region Properties
        /// <summary>
        /// This property is mapped to the "TxnDate" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public int? TxnNumber
        {
            get
            {
                return _txnNumber;
            }
            set
            {
                _txnNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TxnType" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TxnType
        {
            get
            {
                return _Type;
            }
            set
            {
                _Type = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TxnDate" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public DateTime? Date
        {
            get
            {
                return _Date;
            }
            set
            {
                _Date = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "RefNumber" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string RefNumber
        {
            get
            {
                return _RefNumber;
            }
            set
            {

                _RefNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Employee_ListID" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Customer_ListID
        {
            get
            {
                return _Customer_ListID;
            }
            set
            {
                _Customer_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Name" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Customer_Name
        {
            get
            {
                return _Customer_Name;
            }
            set
            {
                _Customer_Name = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SourceName" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SourceName
        {
            get
            {
                return _SourceName;
            }
            set
            {
                _SourceName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Account_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Account_ListID
        {
            get
            {
                return _Account_ListID;
            }
            set
            {
                _Account_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Account" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Account
        {
            get
            {
                return _Account;
            }
            set
            {
                _Account = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Class_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Class_ListID
        {
            get
            {
                return _class_ListID;
            }
            set
            {
                _class_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Class" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Class
        {
            get
            {
                return _class;
            }
            set
            {
                _class = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ClearedStatus" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ClearedStatus
        {
            get
            {
                return _ClearedStatus;
            }
            set
            {

                _ClearedStatus = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SplitAccount" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SplitAccount
        {
            get
            {
                return _SplitAccount;
            }
            set
            {
                _SplitAccount = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SplitAccount_ListId" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SplitAccount_ListId
        {
            get
            {
                return _SplitAccount_ListID;
            }
            set
            {
                _SplitAccount_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Quanity" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? Quantity
        {
            get
            {
                return _Quantity;
            }
            set
            {
                _Quantity = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Debit" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? Debit
        {
            get
            {
                return _Debit;
            }
            set
            {
                _Debit = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Credit" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? Credit
        {
            get
            {
                return _Credit;
            }
            set
            {
                _Credit = value;
            }
        }

        [DataMember]
        public DateTime? ModifiedTime
        {
            get
            {
                return _modifiedTime;
            }
            set
            {
                _modifiedTime = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AccountType" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string AccountType
        {
            get
            {
                return _accountType;
            }
            set
            {

                _accountType = value;
            }
        }

        #endregion
    }
}
