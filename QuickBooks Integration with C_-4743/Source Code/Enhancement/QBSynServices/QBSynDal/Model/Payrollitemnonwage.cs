//
// Class	:	Payrollitemnonwage.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:53 AM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization;
using QBSynDal.List;

namespace QBSynDal
{

	/// <summary>
	/// Class for the properties of the object
	/// </summary>
	public class PayrollitemnonwageFields
	{
		public const string ListID                    = "ListID";
		public const string TimeCreated               = "TimeCreated";
		public const string TimeModified              = "TimeModified";
		public const string EditSequence              = "EditSequence";
		public const string Name                      = "Name";
		public const string IsActive                  = "IsActive";
		public const string NonWageType               = "NonWageType";
		public const string ExpenseAccountRef_ListID  = "ExpenseAccountRef_ListID";
		public const string ExpenseAccountRef_FullName = "ExpenseAccountRef_FullName";
		public const string LiabilityAccountRef_ListID = "LiabilityAccountRef_ListID";
		public const string LiabilityAccountRef_FullName = "LiabilityAccountRef_FullName";
		public const string Status                    = "Status";

	}
	
	/// <summary>
	/// Data access class for the "payrollitemnonwage" table.
	/// </summary>
    [DataContract(Name = "ServicePayrollitemnonwage")]
    public class Payrollitemnonwage : IModel
	{
		
		#region Class Level Variables
		
		

		private string         	_listID                  	= null;
		private string         	_timeCreated             	= null;
		private string         	_timeModified            	= null;
		private string         	_editSequence            	= null;
		private string         	_name                    	= null;
		private bool?          	_isActive                	= null;
		private string         	_nonWageType             	= null;
		private string         	_expenseAccountRef_ListID	= null;
		private string         	_expenseAccountRef_FullName	= null;
		private string         	_liabilityAccountRef_ListID	= null;
		private string         	_liabilityAccountRef_FullName	= null;
		private string         	_status                  	= null;
		
		#endregion
		
		#region Properties

		/// <summary>
		/// This property is mapped to the "ListID" field. Length must be between 0 and 255 characters. 
		/// Comment found in the Database for this field: .
		/// </summary>
        [DataMember]
        public string ListID
		{
			get 
			{ 
				return _listID; 
			}
			set 
			{
				_listID = value;
			}
		}

		/// <summary>
		/// This property is mapped to the "TimeCreated" field. Length must be between 0 and 255 characters. 
		/// Comment found in the Database for this field: .
		/// </summary>
         [DataMember]
        public string TimeCreated
		{
			get 
			{ 
				return _timeCreated; 
			}
			set 
			{
				_timeCreated = value;
			}
		}

		/// <summary>
		/// This property is mapped to the "TimeModified" field. Length must be between 0 and 255 characters. 
		/// Comment found in the Database for this field: .
		/// </summary>
         [DataMember]
        public string TimeModified
		{
			get 
			{ 
				return _timeModified; 
			}
			set 
			{
				_timeModified = value;
			}
		}

		/// <summary>
		/// This property is mapped to the "EditSequence" field. Length must be between 0 and 255 characters. 
		/// Comment found in the Database for this field: .
		/// </summary>
         [DataMember]
        public string EditSequence
		{
			get 
			{ 
				return _editSequence; 
			}
			set 
			{

				_editSequence = value;
			}
		}

		/// <summary>
		/// This property is mapped to the "Name" field. Length must be between 0 and 255 characters. 
		/// Comment found in the Database for this field: .
		/// </summary>
         [DataMember]
        public string Name
		{
			get 
			{ 
				return _name; 
			}
			set 
			{
				_name = value;
			}
		}

		/// <summary>
		/// This property is mapped to the "IsActive" field.  
		/// Comment found in the Database for this field: .
		/// </summary>
         [DataMember]
        public bool? IsActive
		{
			get 
			{ 
				return _isActive; 
			}
			set 
			{
				_isActive = value;
			}
		}

		/// <summary>
		/// This property is mapped to the "NonWageType" field. Length must be between 0 and 255 characters. 
		/// Comment found in the Database for this field: .
		/// </summary>
         [DataMember]
        public string NonWageType
		{
			get 
			{ 
				return _nonWageType; 
			}
			set 
			{
				_nonWageType = value;
			}
		}

		/// <summary>
		/// This property is mapped to the "ExpenseAccountRef_ListID" field. Length must be between 0 and 255 characters. 
		/// Comment found in the Database for this field: .
		/// </summary>
         [DataMember]
        public string ExpenseAccountRef_ListID
		{
			get 
			{ 
				return _expenseAccountRef_ListID; 
			}
			set 
			{
				_expenseAccountRef_ListID = value;
			}
		}

		/// <summary>
		/// This property is mapped to the "ExpenseAccountRef_FullName" field. Length must be between 0 and 255 characters. 
		/// Comment found in the Database for this field: .
		/// </summary>
         [DataMember]
        public string ExpenseAccountRef_FullName
		{
			get 
			{ 
				return _expenseAccountRef_FullName; 
			}
			set 
			{
				_expenseAccountRef_FullName = value;
			}
		}

		/// <summary>
		/// This property is mapped to the "LiabilityAccountRef_ListID" field. Length must be between 0 and 255 characters. 
		/// Comment found in the Database for this field: .
		/// </summary>
         [DataMember]
        public string LiabilityAccountRef_ListID
		{
			get 
			{ 
				return _liabilityAccountRef_ListID; 
			}
			set 
			{
				_liabilityAccountRef_ListID = value;
			}
		}

		/// <summary>
		/// This property is mapped to the "LiabilityAccountRef_FullName" field. Length must be between 0 and 255 characters. 
		/// Comment found in the Database for this field: .
		/// </summary>
         [DataMember]
        public string LiabilityAccountRef_FullName
		{
			get 
			{ 
				return _liabilityAccountRef_FullName; 
			}
			set 
			{
				_liabilityAccountRef_FullName = value;
			}
		}

		/// <summary>
		/// This property is mapped to the "Status" field. Length must be between 0 and 10 characters. 
		/// Comment found in the Database for this field: .
		/// </summary>
         [DataMember]
        public string Status
		{
			get 
			{ 
				return _status; 
			}
			set 
			{
				_status = value;            
			}
		}

		#endregion

	}
}
