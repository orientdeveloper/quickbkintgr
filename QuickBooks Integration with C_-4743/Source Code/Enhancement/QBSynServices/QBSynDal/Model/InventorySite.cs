//
// Class	:	Inventorysite.cs
// Author	:  	SynapseIndia © 2013
// Date		:	25/05//2013 10:11:49 AM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization;

namespace QBSynDal
{

    /// <summary>
    /// Class for the properties of the object
    /// </summary>
    public class InventorysiteFields
    {
        public const string ListID = "ListID";
        public const string TimeCreated = "TimeCreated";
        public const string TimeModified = "TimeModified";
        public const string EditSequence = "EditSequence";
        public const string Name = "Name";
        public const string IsActive = "IsActive";
        public const string IsDefaultSite = "IsDefaultSite";
        public const string SiteDesc = "SiteDesc";
        public const string Contact = "Contact";
        public const string Phone = "Phone";
        public const string Fax = "Fax";
        public const string Email = "Email";
        public const string SiteAddress_Addr1 = "SiteAddress_Addr1";
        public const string SiteAddress_Addr2 = "SiteAddress_Addr2";
        public const string SiteAddress_Addr3 = "SiteAddress_Addr3";
        public const string SiteAddress_Addr4 = "SiteAddress_Addr4";
        public const string SiteAddress_Addr5 = "SiteAddress_Addr5";
        public const string SiteAddress_City = "SiteAddress_City";
        public const string SiteAddress_State = "SiteAddress_State";
        public const string SiteAddress_PostalCode = "SiteAddress_PostalCode";
        public const string SiteAddress_Country = "SiteAddress_Country";
        public const string SiteAddressBlock_Addr1 = "SiteAddressBlock_Addr1";
        public const string SiteAddressBlock_Addr2 = "SiteAddressBlock_Addr2";
        public const string SiteAddressBlock_Addr3 = "SiteAddressBlock_Addr3";
        public const string SiteAddressBlock_Addr4 = "SiteAddressBlock_Addr4";
        public const string SiteAddressBlock_Addr5 = "SiteAddressBlock_Addr5";
        public const string Status = "Status";
    }

    /// <summary>
    /// Model class for the "inventorysite" table.
    /// </summary>

    [DataContract(Name = "ServiceInventorySite")]
    public class InventorySite : IModel
    {

        #region Class Level Variables

        private string _listID = null;
        private string _timeCreated = null;
        private string _timeModified = null;
        private string _editSequence = null;
        private string _name = null;
        private bool? _isActive = null;
        private bool? _isDefaultSite = null;
        private string _siteDesc = null;
        private string _contact = null;
        private string _phone = null;
        private string _fax = null;
        private string _email = null;
        private string _siteAddress_Addr1 = null;
        private string _siteAddress_Addr2 = null;
        private string _siteAddress_Addr3 = null;
        private string _siteAddress_Addr4 = null;
        private string _siteAddress_Addr5 = null;
        private string _siteAddress_City = null;
        private string _siteAddress_State = null;
        private string _siteAddress_PostalCode = null;
        private string _siteAddress_Country = null;
        private string _siteAddressBlock_Addr1 = null;
        private string _siteAddressBlock_Addr2 = null;
        private string _siteAddressBlock_Addr3 = null;
        private string _siteAddressBlock_Addr4 = null;
        private string _siteAddressBlock_Addr5 = null;
        private string _status = null;

        #endregion

        #region Properties

        /// <summary>
        /// This property is mapped to the "ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ListID
        {
            get
            {
                return _listID;
            }
            set
            {
                _listID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeCreated" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TimeCreated
        {
            get
            {
                return _timeCreated;
            }
            set
            {
                _timeCreated = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeModified" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TimeModified
        {
            get
            {
                return _timeModified;
            }
            set
            {
                _timeModified = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "EditSequence" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>

        [DataMember]
        public string EditSequence
        {
            get
            {
                return _editSequence;
            }
            set
            {
                _editSequence = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Name" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsActive" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public bool? IsActive
        {
            get
            {
                return _isActive;
            }
            set
            {
                _isActive = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsDefaultSite" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public bool? IsDefaultSite
        {
            get
            {
                return _isDefaultSite;
            }
            set
            {
                _isDefaultSite = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SiteDesc" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SiteDesc
        {
            get
            {
                return _siteDesc;
            }
            set
            {
                _siteDesc = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Contact" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Contact
        {
            get
            {
                return _contact;
            }
            set
            {
                _contact = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Phone" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Phone
        {
            get
            {
                return _phone;
            }
            set
            {
                _phone = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Fax" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Fax
        {
            get
            {
                return _fax;
            }
            set
            {
                _fax = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Email" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                _email = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SiteAddress_Addr1" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SiteAddress_Addr1
        {
            get
            {
                return _siteAddress_Addr1;
            }
            set
            {
                _siteAddress_Addr1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SiteAddress_Addr2" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SiteAddress_Addr2
        {
            get
            {
                return _siteAddress_Addr2;
            }
            set
            {
                _siteAddress_Addr2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SiteAddress_Addr3" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SiteAddress_Addr3
        {
            get
            {
                return _siteAddress_Addr3;
            }
            set
            {
                _siteAddress_Addr3 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SiteAddress_Addr4" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SiteAddress_Addr4
        {
            get
            {
                return _siteAddress_Addr4;
            }
            set
            {
                _siteAddress_Addr4 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SiteAddress_Addr5" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SiteAddress_Addr5
        {
            get
            {
                return _siteAddress_Addr5;
            }
            set
            {
                _siteAddress_Addr5 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SiteAddress_City" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SiteAddress_City
        {
            get
            {
                return _siteAddress_City;
            }
            set
            {
                _siteAddress_City = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SiteAddress_State" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SiteAddress_State
        {
            get
            {
                return _siteAddress_State;
            }
            set
            {
                _siteAddress_State = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SiteAddress_PostalCode" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SiteAddress_PostalCode
        {
            get
            {
                return _siteAddress_PostalCode;
            }
            set
            {
                _siteAddress_PostalCode = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SiteAddress_Country" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SiteAddress_Country
        {
            get
            {
                return _siteAddress_Country;
            }
            set
            {
                _siteAddress_Country = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SiteAddressBlock_Addr1" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SiteAddressBlock_Addr1
        {
            get
            {
                return _siteAddressBlock_Addr1;
            }
            set
            {
                _siteAddressBlock_Addr1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SiteAddressBlock_Addr2" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SiteAddressBlock_Addr2
        {
            get
            {
                return _siteAddressBlock_Addr2;
            }
            set
            {
                _siteAddressBlock_Addr2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SiteAddressBlock_Addr3" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SiteAddressBlock_Addr3
        {
            get
            {
                return _siteAddressBlock_Addr3;
            }
            set
            {
                _siteAddressBlock_Addr3 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SiteAddressBlock_Addr4" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SiteAddressBlock_Addr4
        {
            get
            {
                return _siteAddressBlock_Addr4;
            }
            set
            {
                _siteAddressBlock_Addr4 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SiteAddressBlock_Addr5" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SiteAddressBlock_Addr5
        {
            get
            {
                return _siteAddressBlock_Addr5;
            }
            set
            {
                _siteAddressBlock_Addr5 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Status" field. Length must be between 0 and 10 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        #endregion

    }
}
