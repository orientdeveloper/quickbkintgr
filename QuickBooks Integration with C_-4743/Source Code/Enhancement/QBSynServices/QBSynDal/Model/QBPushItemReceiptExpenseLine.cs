﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace QBSynDal.Model
{
    public class QBPushItemReceiptExpenseLineFields
    {
        public const string TxnLineID = "TxnLineID";
        public const string APAccountRef_ListID = "APAccountRef_ListID";
        public const string APAccountRef_FullName = "APAccountRef_FullName";
        public const string Amount = "Amount";
        public const string BillableStatus = "BillableStatus";
        public const string ClassRef_ListID = "ClassRef_ListID";
        public const string ClassRef_FullName = "ClassRef_FullName";
        public const string CustomerRef_ListID = "CustomerRef_ListID";
        public const string CustomerRef_FullName = "CustomerRef_FullName";
        public const string Memo = "Memo";
        public const string SalesTaxCodeRef_ListID = "SalesTaxCodeRef_ListID";
        public const string SalesTaxCodeRef_FullName = "SalesTaxCodeRef_FullName";
        public const string TaxAmount = "TaxAmount";
    }

    [DataContract(Name = "ServiceQBPushItemReceiptExpenseLine")]
    public class QBPushItemReceiptExpenseLine
    {
        #region Class Level Variables

        private string _defMacro = null;
        private string _txnLineID = null;
        private string _accountRef_ListID = null;
        private string _accountRef_FullName = null;
        private double? _amount = null;
        private string _memo = null;
        private string _customerRef_ListID = null;
        private string _customerRef_FullName = null;
        private string _classRef_ListID = null;
        private string _classRef_FullName = null;
        private string _salesTaxCodeRef_ListID = null;
        private string _salesTaxCodeRef_FullName = null;
        private string _billableStatus = null;
        private double? _taxAmount = null;

        #endregion

        #region Properties

        /// <summary>
        /// Custom field used in Quickbook
        /// </summary>
        [DataMember]
        public string DefMacro
        {
            get { return _defMacro; }
            set { _defMacro = value; }
        }

        /// <summary>
        /// This property is mapped to the "TxnLineID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TxnLineID
        {
            get
            {
                return _txnLineID;
            }
            set
            {
                _txnLineID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AccountRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string AccountRef_ListID
        {
            get
            {
                return _accountRef_ListID;
            }
            set
            {
                _accountRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AccountRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string AccountRef_FullName
        {
            get
            {
                return _accountRef_FullName;
            }
            set
            {
                _accountRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Amount" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? Amount
        {
            get
            {
                return _amount;
            }
            set
            {
                _amount = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Memo" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Memo
        {
            get
            {
                return _memo;
            }
            set
            {
                _memo = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomerRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomerRef_ListID
        {
            get
            {
                return _customerRef_ListID;
            }
            set
            {
                _customerRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomerRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomerRef_FullName
        {
            get
            {
                return _customerRef_FullName;
            }
            set
            {
                _customerRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ClassRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ClassRef_ListID
        {
            get
            {
                return _classRef_ListID;
            }
            set
            {
                _classRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ClassRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ClassRef_FullName
        {
            get
            {
                return _classRef_FullName;
            }
            set
            {
                _classRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SalesTaxCodeRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SalesTaxCodeRef_ListID
        {
            get
            {
                return _salesTaxCodeRef_ListID;
            }
            set
            {
                _salesTaxCodeRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SalesTaxCodeRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SalesTaxCodeRef_FullName
        {
            get
            {
                return _salesTaxCodeRef_FullName;
            }
            set
            {
                _salesTaxCodeRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BillableStatus" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string BillableStatus
        {
            get
            {
                return _billableStatus;
            }
            set
            {
                _billableStatus = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TaxAmount" field
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? TaxAmount
        {
            get { return _taxAmount; }
            set { _taxAmount = value; }
        }
        #endregion
    }
}
