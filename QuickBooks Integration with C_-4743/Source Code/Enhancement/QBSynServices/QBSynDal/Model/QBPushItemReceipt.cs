//
// Class	:	QBPushPurchaseorder.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:44 AM
//
using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization;

namespace QBSynDal.Model
{
    /// <summary>
    /// Class for the properties of the object
    /// </summary>
    public class QBPushItemReceiptFields
    {
        public const string TxnID = "TxnID";
        public const string APAccountRef_ListID = "APAccountRef_ListID";
        public const string APAccountRef_FullName = "APAccountRef_FullName";
        public const string ExchangeRate = "ExchangeRate";
        public const string IsTaxIncluded = "IsTaxIncluded";
        public const string LinkedTxn = "LinkedTxn";
        public const string Memo = "Memo";
        public const string RefNumber = "RefNumber";
        public const string SalesTaxCodeRef_ListID = "SalesTaxCodeRef_ListID";
        public const string SalesTaxCodeRef_FullName = "SalesTaxCodeRef_FullName";
        public const string TxnDate = "TxnDate";
        public const string VendorRef_ListID = "VendorRef_ListID";
        public const string VendorRef_FullName = "VendorRef_FullName";

        public const string PushStatus = "PushStatus";
        public const string PushDate = "PushDate";
        public const string ErrorMessage = "ErrorMessage";
    }

    /// <summary>
    /// Model class for the "QBPushItemReceipt" table.
    /// </summary>
    [DataContract(Name = "ServiceQBItemReceipt")]
    public class QBPushItemReceipt
    {
        #region Class Level Variables

        private string _defMacro = null;
        private Guid? _externalGUID = null;
        private long? _txnID = null;
        private string _aPAccountRef_ListID = null;
        private string _aPAccountRef_FullName = null;
        private double? _exchangeRate = null;
        private bool? _isTaxIncluded = null;
        private string _linkedTxn = null;
        private string _memo = null;
        private string _refNumber = null;
        private string _salesTaxCodeRef_ListID = null;
        private string _salesTaxCodeRef_FullName = null;
        private DateTime? _txnDate = null;
        private string _vendorRef_ListID = null;
        private string _vendorRef_FullName = null;
        private byte? _pushstatus = null;
        private DateTime? _pushdate = null;
        private string _errormessage = null;

        private List<QBPushItemReceiptExpenseLine> _qBPushItemReceiptExpenseLine;
        private List<QBPushItemReceiptItemGroupLine> _qBPushItemReceiptItemGroupLine;
        private List<QBPushItemReceiptItemLine> _qBPushItemReceiptItemLine;
        #endregion

        #region Properties

        /// <summary>
        /// Custom field used in Quickbook
        /// </summary>
        [DataMember]
        public string DefMacro
        {
            get { return _defMacro; }
            set { _defMacro = value; }
        }

        /// <summary>
        /// Custom field used in Quickbook
        /// </summary>
        [DataMember]
        public Guid? ExternalGUID
        {
            get { return _externalGUID; }
            set { _externalGUID = value; }
        }

        /// <summary>
        /// This property is mapped to the "APAccountRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public long? TxnID
        {
            get
            {
                return _txnID;
            }
            set
            {
                _txnID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "APAccountRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string APAccountRef_ListID
        {
            get
            {
                return _aPAccountRef_ListID;
            }
            set
            {
                _aPAccountRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "APAccountRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string APAccountRef_FullName
        {
            get
            {
                return _aPAccountRef_FullName;
            }
            set
            {
                _aPAccountRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ExchangeRate" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? ExchangeRate
        {
            get
            {
                return _exchangeRate;
            }
            set
            {
                _exchangeRate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsTaxIncluded" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public bool? IsTaxIncluded
        {
            get
            {
                return _isTaxIncluded;
            }
            set
            {
                _isTaxIncluded = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "LinkedTxn" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string LinkedTxn
        {
            get
            {
                return _linkedTxn;
            }
            set
            {
                _linkedTxn = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Memo" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Memo
        {
            get
            {
                return _memo;
            }
            set
            {
                _memo = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "RefNumber" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string RefNumber
        {
            get
            {
                return _refNumber;
            }
            set
            {
                _refNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SalesTaxCodeRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SalesTaxCodeRef_ListID
        {
            get
            {
                return _salesTaxCodeRef_ListID;
            }
            set
            {
                _salesTaxCodeRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SalesTaxCodeRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string SalesTaxCodeRef_FullName
        {
            get
            {
                return _salesTaxCodeRef_FullName;
            }
            set
            {
                _salesTaxCodeRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TxnDate" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public DateTime? TxnDate
        {
            get
            {
                return _txnDate;
            }
            set
            {
                _txnDate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "VendorRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string VendorRef_ListID
        {
            get
            {
                return _vendorRef_ListID;
            }
            set
            {
                _vendorRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "VendorRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string VendorRef_FullName
        {
            get
            {
                return _vendorRef_FullName;
            }
            set
            {
                _vendorRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "PushStatus" field. Length must be between 0 and 10 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public byte? PushStatus
        {
            get
            {
                return _pushstatus;
            }
            set
            {
                _pushstatus = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "PushDate" field. Length must be between 0 and 10 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public DateTime? PushDate
        {
            get
            {
                return _pushdate;
            }
            set
            {
                _pushdate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ErrorMessage" field
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                return _errormessage;
            }
            set
            {
                _errormessage = value;
            }
        }

        [DataMember]
        public List<QBPushItemReceiptExpenseLine> QBPushItemReceiptExpenseLine
        {
            get { return _qBPushItemReceiptExpenseLine; }
            set { _qBPushItemReceiptExpenseLine = value; }
        }

        [DataMember]
        public List<QBPushItemReceiptItemGroupLine> QBPushItemReceiptItemGroupLine
        {
            get { return _qBPushItemReceiptItemGroupLine; }
            set { _qBPushItemReceiptItemGroupLine = value; }
        }

        [DataMember]
        public List<QBPushItemReceiptItemLine> QBPushItemReceiptItemLine
        {
            get { return _qBPushItemReceiptItemLine; }
            set { _qBPushItemReceiptItemLine = value; }
        }
        #endregion
    }
}
