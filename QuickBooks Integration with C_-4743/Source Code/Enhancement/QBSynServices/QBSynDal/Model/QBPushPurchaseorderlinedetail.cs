//
// Class	:	QBPushPurchaseorderlinedetail.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:43 AM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization;

namespace QBSynDal
{
    /// <summary>
    /// Class for the properties of the object
    /// </summary>
    public class QBPushPurchaseorderlinedetailFields
    {
        public const string defMacro = "defMacro";
        public const string ItemRef_ListID = "ItemRef_ListID";
        public const string ItemRef_FullName = "ItemRef_FullName";
        public const string ManufacturerPartNumber = "ManufacturerPartNumber";
        public const string Desc = "Desc";
        public const string Quantity = "Quantity";
        public const string UnitOfMeasure = "UnitOfMeasure";
        public const string Rate = "Rate";
        public const string ClassRef_ListID = "ClassRef_ListID";
        public const string ClassRef_FullName = "ClassRef_FullName";
        public const string Amount = "Amount";
        public const string InventorySiteLocationRef_ListID = "InventorySiteLocationRef_ListID";
        public const string InventorySiteLocationRef_FullName = "InventorySiteLocationRef_FullName";
        public const string CustomerRef_ListID = "CustomerRef_ListID";
        public const string CustomerRef_FullName = "CustomerRef_FullName";
        public const string ServiceDate = "ServiceDate";
        public const string OverrideItemAccountRef_ListID = "OverrideItemAccountRef_ListID";
        public const string OverrideItemAccountRef_FullName = "OverrideItemAccountRef_FullName";
        public const string Other1 = "Other1";
        public const string Other2 = "Other2";
        public const string Idkey = "IDKEY";
        public const string DataExt_OwnerID = "DataExt_OwnerID";
        public const string DataExt_DataExtName = "DataExt_DataExtName";
        public const string DataExt_DataExtValue = "DataExt_DataExtValue";
    }

    /// <summary>
    /// Model class for the "QBPushPurchaseorderlinedetail" table.
    /// </summary>
    [DataContract(Name = "ServiceQBPushPurchaseorderlinedetail")]
    public class QBPushPurchaseorderlinedetail
    {

        #region Class Level Variables

        private string _defMacro = null;
        private string _itemRef_ListID = null;
        private string _itemRef_FullName = null;
        private string _manufacturerPartNumber = null;
        private string _desc = null;
        private double? _quantity = null;
        private string _unitOfMeasure = null;
        private double? _rate = null;
        private string _classRef_ListID = null;
        private string _classRef_FullName = null;
        private double? _amount = null;
        private string _inventorysitelocationref_ListID = null;
        private string _inventorysitelocationref_FullName = null;
        private string _customerref_ListID = null;
        private string _customerref_FullName = null;
        private DateTime? _serviceDate = null;
        private string _overrideitemaccountref_ListID = null;
        private string _overrideitemaccountref_FullName = null;
        private string _other1 = null;
        private string _other2 = null;
        private long? _idkey = null;
        private Guid? _dataext_OwnerID = null;
        private string _dataext_DataExtName = null;
        private string _dataext_DataExtValue = null;

        #endregion

        #region Properties

        /// <summary>
        /// This property is mapped to the "TxnLineID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string defMacro
        {
            get
            {
                return _defMacro;
            }
            set
            {
                _defMacro = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ItemRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ItemRef_ListID
        {
            get
            {
                return _itemRef_ListID;
            }
            set
            {

                _itemRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ItemRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ItemRef_FullName
        {
            get
            {
                return _itemRef_FullName;
            }
            set
            {
                _itemRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ManufacturerPartNumber" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ManufacturerPartNumber
        {
            get
            {
                return _manufacturerPartNumber;
            }
            set
            {
                _manufacturerPartNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Desc" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Desc
        {
            get
            {
                return _desc;
            }
            set
            {
                    _desc = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Quantity" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? Quantity
        {
            get
            {
                return _quantity;
            }
            set
            {
                _quantity = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "UnitOfMeasure" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string UnitOfMeasure
        {
            get
            {
                return _unitOfMeasure;
            }
            set
            {
                _unitOfMeasure = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Rate" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? Rate
        {
            get
            {
                return _rate;
            }
            set
            {
                _rate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ClassRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ClassRef_ListID
        {
            get
            {
                return _classRef_ListID;
            }
            set
            {
                _classRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ClassRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ClassRef_FullName
        {
            get
            {
                return _classRef_FullName;
            }
            set
            {
                _classRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Amount" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? Amount
        {
            get
            {
                return _amount;
            }
            set
            {
                _amount = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomerRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string InventorySiteLocationRef_ListID
        {
            get
            {
                return _inventorysitelocationref_ListID;
            }
            set
            {
                _inventorysitelocationref_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomerRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string InventorySiteLocationRef_FullName
        {
            get
            {
                return _inventorysitelocationref_FullName;
            }
            set
            {
                _inventorysitelocationref_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomerRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomerRef_ListID
        {
            get
            {
                return _customerref_ListID;
            }
            set
            {
                _customerref_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomerRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomerRef_FullName
        {
            get
            {
                return _customerref_FullName;
            }
            set
            {
                _customerref_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ServiceDate" field.  
        /// Comment found in the Database for this field: .
        /// </summary>

        [DataMember]
        public DateTime? ServiceDate
        {
            get
            {
                return _serviceDate;
            }
            set
            {
                _serviceDate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SalesTaxCodeRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string OverrideItemAccountRef_ListID
        {
            get
            {
                return _overrideitemaccountref_ListID;
            }
            set
            {
                _overrideitemaccountref_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SalesTaxCodeRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string OverrideItemAccountRef_FullName
        {
            get
            {
                return _overrideitemaccountref_FullName;
            }
            set
            {
                _overrideitemaccountref_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Other1" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Other1
        {
            get
            {
                return _other1;
            }
            set
            {

                _other1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Other2" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Other2
        {
            get
            {
                return _other2;
            }
            set
            {

                _other2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IDKEY" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public long? Idkey
        {
            get
            {
                return _idkey;
            }
            set
            {
                _idkey = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "DataExt_OwnerID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public Guid? DataExt_OwnerID
        {
            get
            {
                return _dataext_OwnerID;
            }
            set
            {
                _dataext_OwnerID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "DataExt_DataExtName" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string DataExt_DataExtName
        {
            get
            {
                return _dataext_DataExtName;
            }
            set
            {
                _dataext_DataExtName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "DataExt_DataExtValue" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string DataExt_DataExtValue
        {
            get
            {
                return _dataext_DataExtValue;
            }
            set
            {
                _dataext_DataExtValue = value;
            }
        }

        #endregion

    }
}
