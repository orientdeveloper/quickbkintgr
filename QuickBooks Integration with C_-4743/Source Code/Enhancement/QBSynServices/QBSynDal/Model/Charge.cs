//
// Class	:	Charge.cs
// Author	:  	SynapseIndia © 2013
// Date		:	8/4/2013 10:11:44 AM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization;

namespace QBSynDal
{
    /// <summary>
    /// Class for the properties of the object
    /// </summary>
    /// 

    #region ChargeFields
    public class ChargeFields
    {
        public const string TxnID = "TxnID";
        public const string TimeCreated = "TimeCreated";
        public const string TimeModified = "TimeModified";
        public const string EditSequence = "EditSequence";
        public const string TxnNumber = "TxnNumber";
        public const string CustomerRef_ListID = "CustomerRef_ListID";
        public const string CustomerRef_FullName = "CustomerRef_FullName";
        public const string TxnDate = "TxnDate";
        public const string RefNumber = "RefNumber";
        public const string ItemRef_ListID = "ItemRef_ListID";
        public const string ItemRef_FullName = "ItemRef_FullName";
        public const string InventorySiteRef_ListID = "InventorySiteRef_ListID";
        public const string InventorySiteRef_FullName = "InventorySiteRef_FullName";
        public const string Quantity = "Quantity";
        public const string UnitOfMeasure = "UnitOfMeasure";
        public const string OverrideUOMSetRef_ListID = "OverrideUOMSetRef_ListID";
        public const string OverrideUOMSetRef_FullName = "OverrideUOMSetRef_FullName";
        public const string Rate = "Rate";
        public const string Amount = "Amount";
        public const string BalanceRemaining = "BalanceRemaining";
        public const string Desc = "Desc";
        public const string ARAccountRef_ListID = "ARAccountRef_ListID";
        public const string ARAccountRef_FullName = "ARAccountRef_FullName";
        public const string ClassRef_ListID = "ClassRef_ListID";
        public const string ClassRef_FullName = "ClassRef_FullName";
        public const string BilledDate = "BilledDate";
        public const string DueDate = "DueDate";
        public const string IsPaid = "IsPaid";
        public const string CustomField1 = "CustomField1";
        public const string CustomField2 = "CustomField2";
        public const string CustomField3 = "CustomField3";
        public const string CustomField4 = "CustomField4";
        public const string CustomField5 = "CustomField5";
        public const string CustomField6 = "CustomField6";
        public const string CustomField7 = "CustomField7";
        public const string CustomField8 = "CustomField8";
        public const string CustomField9 = "CustomField9";
        public const string CustomField10 = "CustomField10";
        public const string Status = "Status";
    }
    #endregion

    /// <summary>
    /// Model class for the "charge" table.
    /// </summary>
    /// 
    [DataContract(Name = "ServiceCharge")]
    public class Charge : IModel
    {
        #region Class Level Variables

        private string _txnID = null;
        private string _timeCreated = null;
        private string _timeModified = null;
        private string _editSequence = null;
        private int? _txnNumber = null;
        private string _customerRef_ListID = null;
        private string _customerRef_FullName = null;
        private DateTime? _txnDate = null;
        private string _refNumber = null;
        private string _itemRef_ListID = null;
        private string _itemRef_FullName = null;
        private string _inventorySiteRef_ListID = null;
        private string _inventorySiteRef_FullName = null;
        private string _quantity = null;
        private string _unitOfMeasure = null;
        private string _overrideUOMSetRef_ListID = null;
        private string _overrideUOMSetRef_FullName = null;
        private string _rate = null;
        private double? _amount = null;
        private double? _balanceRemaining = null;
        private string _desc = null;
        private string _aRAccountRef_ListID = null;
        private string _aRAccountRef_FullName = null;
        private string _classRef_ListID = null;
        private string _classRef_FullName = null;
        private DateTime? _billedDate = null;
        private DateTime? _dueDate = null;
        private bool? _isPaid = null;
        private string _customField1 = null;
        private string _customField2 = null;
        private string _customField3 = null;
        private string _customField4 = null;
        private string _customField5 = null;
        private string _customField6 = null;
        private string _customField7 = null;
        private string _customField8 = null;
        private string _customField9 = null;
        private string _customField10 = null;
        private string _status = null;

        #endregion

        #region Properties

        /// <summary>
        /// This property is mapped to the "TxnID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TxnID
        {
            get
            {
                return _txnID;
            }
            set
            {
                _txnID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeCreated" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TimeCreated
        {
            get
            {
                return _timeCreated;
            }
            set
            {
                _timeCreated = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeModified" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TimeModified
        {
            get
            {
                return _timeModified;
            }
            set
            {
                _timeModified = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "EditSequence" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string EditSequence
        {
            get
            {
                return _editSequence;
            }
            set
            {
                _editSequence = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TxnNumber" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public int? TxnNumber
        {
            get
            {
                return _txnNumber;
            }
            set
            {
                _txnNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomerRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomerRef_ListID
        {
            get
            {
                return _customerRef_ListID;
            }
            set
            {
                _customerRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomerRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomerRef_FullName
        {
            get
            {
                return _customerRef_FullName;
            }
            set
            {
                _customerRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TxnDate" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public DateTime? TxnDate
        {
            get
            {
                return _txnDate;
            }
            set
            {
                _txnDate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "RefNumber" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string RefNumber
        {
            get
            {
                return _refNumber;
            }
            set
            {
                _refNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ItemRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ItemRef_ListID
        {
            get
            {
                return _itemRef_ListID;
            }
            set
            {
                _itemRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ItemRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ItemRef_FullName
        {
            get
            {
                return _itemRef_FullName;
            }
            set
            {
                _itemRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "InventorySiteRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string InventorySiteRef_ListID
        {
            get
            {
                return _inventorySiteRef_ListID;
            }
            set
            {
                _inventorySiteRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "InventorySiteRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string InventorySiteRef_FullName
        {
            get
            {
                return _inventorySiteRef_FullName;
            }
            set
            {
                _inventorySiteRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Quantity" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Quantity
        {
            get
            {
                return _quantity;
            }
            set
            {
                _quantity = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "UnitOfMeasure" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string UnitOfMeasure
        {
            get
            {
                return _unitOfMeasure;
            }
            set
            {
                _unitOfMeasure = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "OverrideUOMSetRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string OverrideUOMSetRef_ListID
        {
            get
            {
                return _overrideUOMSetRef_ListID;
            }
            set
            {
                _overrideUOMSetRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "OverrideUOMSetRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string OverrideUOMSetRef_FullName
        {
            get
            {
                return _overrideUOMSetRef_FullName;
            }
            set
            {
                _overrideUOMSetRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Rate" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Rate
        {
            get
            {
                return _rate;
            }
            set
            {
                _rate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Amount" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? Amount
        {
            get
            {
                return _amount;
            }
            set
            {
                _amount = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BalanceRemaining" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? BalanceRemaining
        {
            get
            {
                return _balanceRemaining;
            }
            set
            {
                _balanceRemaining = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Desc" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Desc
        {
            get
            {
                return _desc;
            }
            set
            {
                _desc = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ARAccountRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ARAccountRef_ListID
        {
            get
            {
                return _aRAccountRef_ListID;
            }
            set
            {
                _aRAccountRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ARAccountRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ARAccountRef_FullName
        {
            get
            {
                return _aRAccountRef_FullName;
            }
            set
            {
                _aRAccountRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ClassRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ClassRef_ListID
        {
            get
            {
                return _classRef_ListID;
            }
            set
            {
                _classRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ClassRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ClassRef_FullName
        {
            get
            {
                return _classRef_FullName;
            }
            set
            {
                _classRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "BilledDate" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public DateTime? BilledDate
        {
            get
            {
                return _billedDate;
            }
            set
            {
                _billedDate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "DueDate" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public DateTime? DueDate
        {
            get
            {
                return _dueDate;
            }
            set
            {
                _dueDate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsPaid" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public bool? IsPaid
        {
            get
            {
                return _isPaid;
            }
            set
            {
                _isPaid = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField1" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField1
        {
            get
            {
                return _customField1;
            }
            set
            {
                _customField1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField2" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField2
        {
            get
            {
                return _customField2;
            }
            set
            {
                _customField2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField3" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField3
        {
            get
            {
                return _customField3;
            }
            set
            {
                _customField3 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField4" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField4
        {
            get
            {
                return _customField4;
            }
            set
            {
                _customField4 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField5" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField5
        {
            get
            {
                return _customField5;
            }
            set
            {
                _customField5 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField6" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField6
        {
            get
            {
                return _customField6;
            }
            set
            {
                _customField6 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField7" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField7
        {
            get
            {
                return _customField7;
            }
            set
            {
                _customField7 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField8" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField8
        {
            get
            {
                return _customField8;
            }
            set
            {
                _customField8 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField9" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField9
        {
            get
            {
                return _customField9;
            }
            set
            {
                _customField9 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField10" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField10
        {
            get
            {
                return _customField10;
            }
            set
            {
                _customField10 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Status" field. Length must be between 0 and 10 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        #endregion
    }
}
