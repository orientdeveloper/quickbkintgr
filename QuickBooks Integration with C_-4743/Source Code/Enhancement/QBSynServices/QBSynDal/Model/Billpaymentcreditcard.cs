//
// Class	:	Billpaymentcreditcard.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/5/2013 10:11:43 AM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization;

namespace QBSynDal
{
    /// <summary>
    /// Class for the properties of the object
    /// </summary>
    public class BillpaymentcreditcardFields
    {
        public const string TxnID = "TxnID";
        public const string TimeCreated = "TimeCreated";
        public const string TimeModified = "TimeModified";
        public const string EditSequence = "EditSequence";
        public const string TxnNumber = "TxnNumber";
        public const string PayeeEntityRef_ListID = "PayeeEntityRef_ListID";
        public const string PayeeEntityRef_FullName = "PayeeEntityRef_FullName";
        public const string APAccountRef_ListID = "APAccountRef_ListID";
        public const string APAccountRef_FullName = "APAccountRef_FullName";
        public const string TxnDate = "TxnDate";
        public const string CreditCardAccountRef_ListID = "CreditCardAccountRef_ListID";
        public const string CreditCardAccountRef_FullName = "CreditCardAccountRef_FullName";
        public const string Amount = "Amount";
        public const string CurrencyRef_ListID = "CurrencyRef_ListID";
        public const string CurrencyRef_FullName = "CurrencyRef_FullName";
        public const string ExchangeRate = "ExchangeRate";
        public const string AmountInHomeCurrency = "AmountInHomeCurrency";
        public const string RefNumber = "RefNumber";
        public const string Memo = "Memo";
        public const string CustomField1 = "CustomField1";
        public const string CustomField2 = "CustomField2";
        public const string CustomField3 = "CustomField3";
        public const string CustomField4 = "CustomField4";
        public const string CustomField5 = "CustomField5";
        public const string CustomField6 = "CustomField6";
        public const string CustomField7 = "CustomField7";
        public const string CustomField8 = "CustomField8";
        public const string CustomField9 = "CustomField9";
        public const string CustomField10 = "CustomField10";
        public const string Status = "Status";
    }

    /// <summary>
    /// Data Contract for the "billpaymentcreditcard" table.
    /// </summary>
    [DataContract(Name = "ServiceBillpaymentcreditcard")]
    public class Billpaymentcreditcard : IModel
    {

        #region Class Level Variables

        private string _txnID = null;
        private string _timeCreated = null;
        private string _timeModified = null;
        private string _editSequence = null;
        private int? _txnNumber = null;
        private string _payeeEntityRef_ListID = null;
        private string _payeeEntityRef_FullName = null;
        private string _aPAccountRef_ListID = null;
        private string _aPAccountRef_FullName = null;
        private DateTime? _txnDate = null;
        private string _creditCardAccountRef_ListID = null;
        private string _creditCardAccountRef_FullName = null;
        private double? _amount = null;
        private string _currencyRef_ListID = null;
        private string _currencyRef_FullName = null;
        private double? _exchangeRate = null;
        private double? _amountInHomeCurrency = null;
        private string _refNumber = null;
        private string _memo = null;
        private string _customField1 = null;
        private string _customField2 = null;
        private string _customField3 = null;
        private string _customField4 = null;
        private string _customField5 = null;
        private string _customField6 = null;
        private string _customField7 = null;
        private string _customField8 = null;
        private string _customField9 = null;
        private string _customField10 = null;
        private string _status = null;

        #endregion

        #region Properties
        /// <summary>
        /// This property is mapped to the "TxnID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TxnID
        {
            get
            {
                return _txnID;
            }
            set
            {
                _txnID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeCreated" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TimeCreated
        {
            get
            {
                return _timeCreated;
            }
            set
            {
                _timeCreated = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeModified" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TimeModified
        {
            get
            {
                return _timeModified;
            }
            set
            {
                _timeModified = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "EditSequence" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string EditSequence
        {
            get
            {
                return _editSequence;
            }
            set
            {
                _editSequence = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TxnNumber" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public int? TxnNumber
        {
            get
            {
                return _txnNumber;
            }
            set
            {
                _txnNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "PayeeEntityRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string PayeeEntityRef_ListID
        {
            get
            {
                return _payeeEntityRef_ListID;
            }
            set
            {
                _payeeEntityRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "PayeeEntityRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string PayeeEntityRef_FullName
        {
            get
            {
                return _payeeEntityRef_FullName;
            }
            set
            {
                _payeeEntityRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "APAccountRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string APAccountRef_ListID
        {
            get
            {
                return _aPAccountRef_ListID;
            }
            set
            {
                _aPAccountRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "APAccountRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string APAccountRef_FullName
        {
            get
            {
                return _aPAccountRef_FullName;
            }
            set
            {
                _aPAccountRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TxnDate" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public DateTime? TxnDate
        {
            get
            {
                return _txnDate;
            }
            set
            {
                _txnDate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CreditCardAccountRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CreditCardAccountRef_ListID
        {
            get
            {
                return _creditCardAccountRef_ListID;
            }
            set
            {
                _creditCardAccountRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CreditCardAccountRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CreditCardAccountRef_FullName
        {
            get
            {
                return _creditCardAccountRef_FullName;
            }
            set
            {
                _creditCardAccountRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Amount" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? Amount
        {
            get
            {
                return _amount;
            }
            set
            {
                _amount = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CurrencyRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CurrencyRef_ListID
        {
            get
            {
                return _currencyRef_ListID;
            }
            set
            {
                _currencyRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CurrencyRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CurrencyRef_FullName
        {
            get
            {
                return _currencyRef_FullName;
            }
            set
            {
                _currencyRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ExchangeRate" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? ExchangeRate
        {
            get
            {
                return _exchangeRate;
            }
            set
            {
                _exchangeRate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AmountInHomeCurrency" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? AmountInHomeCurrency
        {
            get
            {
                return _amountInHomeCurrency;
            }
            set
            {
                _amountInHomeCurrency = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "RefNumber" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string RefNumber
        {
            get
            {
                return _refNumber;
            }
            set
            {
                _refNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Memo" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Memo
        {
            get
            {
                return _memo;
            }
            set
            {
                _memo = value;

            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField1" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField1
        {
            get
            {
                return _customField1;
            }
            set
            {
                _customField1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField2" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField2
        {
            get
            {
                return _customField2;
            }
            set
            {
                _customField2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField3" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField3
        {
            get
            {
                return _customField3;
            }
            set
            {
                _customField3 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField4" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField4
        {
            get
            {
                return _customField4;
            }
            set
            {
                _customField4 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField5" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField5
        {
            get
            {
                return _customField5;
            }
            set
            {
                _customField5 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField6" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField6
        {
            get
            {
                return _customField6;
            }
            set
            {
                _customField6 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField7" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField7
        {
            get
            {
                return _customField7;
            }
            set
            {
                _customField7 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField8" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField8
        {
            get
            {
                return _customField8;
            }
            set
            {
                _customField8 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField9" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField9
        {
            get
            {
                return _customField9;
            }
            set
            {
                _customField9 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField10" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField10
        {
            get
            {
                return _customField10;
            }
            set
            {
                _customField10 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Status" field. Length must be between 0 and 10 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        #endregion

    }
}
