//
// Class	:	Othername.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:44 AM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization;

namespace QBSynDal
{
    /// <summary>
    /// Class for the properties of the object
    /// </summary>
    public class OthernameFields
    {
        public const string ListID = "ListID";
        public const string TimeCreated = "TimeCreated";
        public const string TimeModified = "TimeModified";
        public const string EditSequence = "EditSequence";
        public const string Name = "Name";
        public const string IsActive = "IsActive";
        public const string CompanyName = "CompanyName";
        public const string Salutation = "Salutation";
        public const string FirstName = "FirstName";
        public const string MiddleName = "MiddleName";
        public const string LastName = "LastName";
        public const string Suffix = "Suffix";
        public const string OtherNameAddress_Addr1 = "OtherNameAddress_Addr1";
        public const string OtherNameAddress_Addr2 = "OtherNameAddress_Addr2";
        public const string OtherNameAddress_Addr3 = "OtherNameAddress_Addr3";
        public const string OtherNameAddress_Addr4 = "OtherNameAddress_Addr4";
        public const string OtherNameAddress_Addr5 = "OtherNameAddress_Addr5";
        public const string OtherNameAddress_City = "OtherNameAddress_City";
        public const string OtherNameAddress_State = "OtherNameAddress_State";
        public const string OtherNameAddress_PostalCode = "OtherNameAddress_PostalCode";
        public const string OtherNameAddress_Country = "OtherNameAddress_Country";
        public const string OtherNameAddress_Note = "OtherNameAddress_Note";
        public const string Phone = "Phone";
        public const string Mobile = "Mobile";
        public const string Pager = "Pager";
        public const string AltPhone = "AltPhone";
        public const string Fax = "Fax";
        public const string Email = "Email";
        public const string Contact = "Contact";
        public const string AltContact = "AltContact";
        public const string AccountNumber = "AccountNumber";
        public const string CustomField1 = "CustomField1";
        public const string CustomField2 = "CustomField2";
        public const string CustomField3 = "CustomField3";
        public const string CustomField4 = "CustomField4";
        public const string CustomField5 = "CustomField5";
        public const string CustomField6 = "CustomField6";
        public const string CustomField7 = "CustomField7";
        public const string CustomField8 = "CustomField8";
        public const string CustomField9 = "CustomField9";
        public const string CustomField10 = "CustomField10";
        public const string Status = "Status";
    }
    /// <summary>
    /// Model class for the "othername" table.
    /// </summary>
    [DataContract(Name = "ServiceOthername")]
    public class Othername : IModel
    {

        #region Class Level Variables

        private string _listID = null;
        private string _timeCreated = null;
        private string _timeModified = null;
        private string _editSequence = null;
        private string _name = null;
        private bool? _isActive = null;
        private string _companyName = null;
        private string _salutation = null;
        private string _firstName = null;
        private string _middleName = null;
        private string _lastName = null;
        private string _suffix = null;
        private string _otherNameAddress_Addr1 = null;
        private string _otherNameAddress_Addr2 = null;
        private string _otherNameAddress_Addr3 = null;
        private string _otherNameAddress_Addr4 = null;
        private string _otherNameAddress_Addr5 = null;
        private string _otherNameAddress_City = null;
        private string _otherNameAddress_State = null;
        private string _otherNameAddress_PostalCode = null;
        private string _otherNameAddress_Country = null;
        private string _otherNameAddress_Note = null;
        private string _phone = null;
        private string _mobile = null;
        private string _pager = null;
        private string _altPhone = null;
        private string _fax = null;
        private string _email = null;
        private string _contact = null;
        private string _altContact = null;
        private string _accountNumber = null;
        private string _customField1 = null;
        private string _customField2 = null;
        private string _customField3 = null;
        private string _customField4 = null;
        private string _customField5 = null;
        private string _customField6 = null;
        private string _customField7 = null;
        private string _customField8 = null;
        private string _customField9 = null;
        private string _customField10 = null;
        private string _status = null;

        #endregion

        #region Properties

        /// <summary>
        /// This property is mapped to the "ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ListID
        {
            get
            {
                return _listID;
            }
            set
            {
                _listID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeCreated" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TimeCreated
        {
            get
            {
                return _timeCreated;
            }
            set
            {
                _timeCreated = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeModified" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TimeModified
        {
            get
            {
                return _timeModified;
            }
            set
            {
                _timeModified = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "EditSequence" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string EditSequence
        {
            get
            {
                return _editSequence;
            }
            set
            {
                _editSequence = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Name" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsActive" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public bool? IsActive
        {
            get
            {
                return _isActive;
            }
            set
            {
                _isActive = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CompanyName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CompanyName
        {
            get
            {
                return _companyName;
            }
            set
            {
                _companyName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Salutation" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Salutation
        {
            get
            {
                return _salutation;
            }
            set
            {
                _salutation = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "FirstName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string FirstName
        {
            get
            {
                return _firstName;
            }
            set
            {
                _firstName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "MiddleName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string MiddleName
        {
            get
            {
                return _middleName;
            }
            set
            {
                _middleName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "LastName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string LastName
        {
            get
            {
                return _lastName;
            }
            set
            {
                _lastName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Suffix" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Suffix
        {
            get
            {
                return _suffix;
            }
            set
            {
                _suffix = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "OtherNameAddress_Addr1" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string OtherNameAddress_Addr1
        {
            get
            {
                return _otherNameAddress_Addr1;
            }
            set
            {
                _otherNameAddress_Addr1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "OtherNameAddress_Addr2" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string OtherNameAddress_Addr2
        {
            get
            {
                return _otherNameAddress_Addr2;
            }
            set
            {
                _otherNameAddress_Addr2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "OtherNameAddress_Addr3" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string OtherNameAddress_Addr3
        {
            get
            {
                return _otherNameAddress_Addr3;
            }
            set
            {
                _otherNameAddress_Addr3 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "OtherNameAddress_Addr4" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string OtherNameAddress_Addr4
        {
            get
            {
                return _otherNameAddress_Addr4;
            }
            set
            {
                _otherNameAddress_Addr4 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "OtherNameAddress_Addr5" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string OtherNameAddress_Addr5
        {
            get
            {
                return _otherNameAddress_Addr5;
            }
            set
            {
                _otherNameAddress_Addr5 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "OtherNameAddress_City" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string OtherNameAddress_City
        {
            get
            {
                return _otherNameAddress_City;
            }
            set
            {
                _otherNameAddress_City = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "OtherNameAddress_State" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string OtherNameAddress_State
        {
            get
            {
                return _otherNameAddress_State;
            }
            set
            {
                _otherNameAddress_State = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "OtherNameAddress_PostalCode" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string OtherNameAddress_PostalCode
        {
            get
            {
                return _otherNameAddress_PostalCode;
            }
            set
            {
                _otherNameAddress_PostalCode = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "OtherNameAddress_Country" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string OtherNameAddress_Country
        {
            get
            {
                return _otherNameAddress_Country;
            }
            set
            {
                _otherNameAddress_Country = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "OtherNameAddress_Note" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string OtherNameAddress_Note
        {
            get
            {
                return _otherNameAddress_Note;
            }
            set
            {
                _otherNameAddress_Note = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Phone" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Phone
        {
            get
            {
                return _phone;
            }
            set
            {
                _phone = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Mobile" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Mobile
        {
            get
            {
                return _mobile;
            }
            set
            {
                _mobile = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Pager" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Pager
        {
            get
            {
                return _pager;
            }
            set
            {
                _pager = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AltPhone" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string AltPhone
        {
            get
            {
                return _altPhone;
            }
            set
            {
                _altPhone = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Fax" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Fax
        {
            get
            {
                return _fax;
            }
            set
            {
                _fax = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Email" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                _email = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Contact" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Contact
        {
            get
            {
                return _contact;
            }
            set
            {
                _contact = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AltContact" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string AltContact
        {
            get
            {
                return _altContact;
            }
            set
            {
                _altContact = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AccountNumber" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string AccountNumber
        {
            get
            {
                return _accountNumber;
            }
            set
            {
                _accountNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField1" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField1
        {
            get
            {
                return _customField1;
            }
            set
            {
                _customField1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField2" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>

        [DataMember]
        public string CustomField2
        {
            get
            {
                return _customField2;
            }
            set
            {
                _customField2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField3" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField3
        {
            get
            {
                return _customField3;
            }
            set
            {
                _customField3 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField4" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField4
        {
            get
            {
                return _customField4;
            }
            set
            {
                _customField4 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField5" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField5
        {
            get
            {
                return _customField5;
            }
            set
            {
                _customField5 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField6" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField6
        {
            get
            {
                return _customField6;
            }
            set
            {
                _customField6 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField7" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField7
        {
            get
            {
                return _customField7;
            }
            set
            {
                _customField7 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField8" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField8
        {
            get
            {
                return _customField8;
            }
            set
            {
                _customField8 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField9" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField9
        {
            get
            {
                return _customField9;
            }
            set
            {
                _customField9 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField10" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CustomField10
        {
            get
            {
                return _customField10;
            }
            set
            {
                _customField10 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Status" field. Length must be between 0 and 10 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        #endregion

    }
}
