﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace QBSynDal.Model
{
    public class QBPushItemReceiptItemGroupLineFields
    {
        public const string TxnLineID = "TxnLineID";
        public const string Desc = "Desc";
        public const string ItemGroupRef_ListID = "ItemGroupRef_ListID";
        public const string ItemGroupRef_FullName = "ItemGroupRef_FullName";
        public const string Quantity = "Quantity";

        //Not in main sql server table
        public const string UnitOfMeasure = "UnitOfMeasure";
        public const string InventorySiteLocationRef_ListID = "InventorySiteLocationRef_ListID";
        public const string InventorySiteLocationRef_FullName = "InventorySiteLocationRef_FullName";
        public const string InventorySiteRef_ListID = "InventorySiteRef_ListID";
        public const string InventorySiteRef_FullName = "InventorySiteRef_FullName";
    }

    [DataContract(Name = "ServiceQBPushItemReceiptItemGroupLine")]
    public class QBPushItemReceiptItemGroupLine
    {
        #region Class Level Variables

        private long? _txnLineID = null;
        private string _itemGroupRef_ListID = null;
        private string _itemGroupRef_FullName = null;
        private string _desc = null;
        private double? _quantity = null;

        private string _unitOfMeasure = null;
        private string _inventorySiteLocationRef_ListID = null;
        private string _inventorySiteLocationRef_FullName = null;
        private string _inventorySiteRef_ListID = null;
        private string _inventorySiteRef_FullName = null;

        #endregion

        /// <summary>
        /// This property is mapped to the "TxnLineID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public long? TxnLineID
        {
            get
            {
                return _txnLineID;
            }
            set
            {
                _txnLineID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ItemGroupRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ItemGroupRef_ListID
        {
            get
            {
                return _itemGroupRef_ListID;
            }
            set
            {
                _itemGroupRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ItemGroupRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ItemGroupRef_FullName
        {
            get
            {
                return _itemGroupRef_FullName;
            }
            set
            {
                _itemGroupRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Desc" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Desc
        {
            get
            {
                return _desc;
            }
            set
            {
                _desc = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Quantity" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? Quantity
        {
            get
            {
                return _quantity;
            }
            set
            {
                _quantity = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "InventorySiteRef_FullName" field
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string InventorySiteRef_FullName
        {
            get { return _inventorySiteRef_FullName; }
            set { _inventorySiteRef_FullName = value; }
        }

        /// <summary>
        /// This property is mapped to the "InventorySiteRef_ListID" field
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string InventorySiteRef_ListID
        {
            get { return _inventorySiteRef_ListID; }
            set { _inventorySiteRef_ListID = value; }
        }

        /// <summary>
        /// This property is mapped to the "InventorySiteLocationRef_FullName" field
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string InventorySiteLocationRef_FullName
        {
            get { return _inventorySiteLocationRef_FullName; }
            set { _inventorySiteLocationRef_FullName = value; }
        }

        /// <summary>
        /// This property is mapped to the "InventorySiteLocationRef_ListID" field
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string InventorySiteLocationRef_ListID
        {
            get { return _inventorySiteLocationRef_ListID; }
            set { _inventorySiteLocationRef_ListID = value; }
        }

        /// <summary>
        /// This property is mapped to the "UnitOfMeasure" field
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string UnitOfMeasure
        {
            get { return _unitOfMeasure; }
            set { _unitOfMeasure = value; }
        }
    }
}
