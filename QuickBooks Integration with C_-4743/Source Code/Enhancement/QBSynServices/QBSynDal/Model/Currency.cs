//
// Class	:	Currency.cs
// Author	:  	Synapse India© 2013
// Date		:	7/18/2013 10:11:45 AM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization;

namespace QBSynDal
{

    /// <summary>
    /// Class for the properties of the object
    /// </summary>
    public class CurrencyFields
    {
        public const string ListID = "ListID";
        public const string TimeCreated = "TimeCreated";
        public const string TimeModified = "TimeModified";
        public const string EditSequence = "EditSequence";
        public const string Name = "Name";
        public const string IsActive = "IsActive";
        public const string CurrencyCode = "CurrencyCode";
        public const string ThousandSeparator = "ThousandSeparator";
        public const string ThousandSeparatorGrouping = "ThousandSeparatorGrouping";
        public const string DecimalPlaces = "DecimalPlaces";
        public const string DecimalSeparator = "DecimalSeparator";
        public const string IsUserDefinedCurrency = "IsUserDefinedCurrency";
        public const string ExchangeRate = "ExchangeRate";
        public const string AsOfDate = "AsOfDate";
        public const string Status = "Status";
    }

    /// <summary>
    /// Data access class for the "currency" table.
    /// </summary>
    [DataContract(Name = "ServiceCurrency")]
    public class Currency : IModel
    {

        #region Class Level Variables

        private string _listID = null;
        private string _timeCreated = null;
        private string _timeModified = null;
        private string _editSequence = null;
        private string _name = null;
        private bool? _isActive = null;
        private string _currencyCode = null;
        private string _thousandSeparator = null;
        private string _thousandSeparatorGrouping = null;
        private string _decimalPlaces = null;
        private string _decimalSeparator = null;
        private bool? _isUserDefinedCurrency = null;
        private double? _exchangeRate = null;
        private DateTime? _asOfDate = null;
        private string _status = null;

        #endregion

        #region Properties

        /// <summary>
        /// This property is mapped to the "ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ListID
        {
            get
            {
                return _listID;
            }
            set
            {
                _listID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeCreated" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TimeCreated
        {
            get
            {
                return _timeCreated;
            }
            set
            {
                _timeCreated = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeModified" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string TimeModified
        {
            get
            {
                return _timeModified;
            }
            set
            {
                _timeModified = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "EditSequence" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string EditSequence
        {
            get
            {
                return _editSequence;
            }
            set
            {
                _editSequence = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Name" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsActive" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public bool? IsActive
        {
            get
            {
                return _isActive;
            }
            set
            {
                _isActive = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CurrencyCode" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string CurrencyCode
        {
            get
            {
                return _currencyCode;
            }
            set
            {
                _currencyCode = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ThousandSeparator" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ThousandSeparator
        {
            get
            {
                return _thousandSeparator;
            }
            set
            {
                _thousandSeparator = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ThousandSeparatorGrouping" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string ThousandSeparatorGrouping
        {
            get
            {
                return _thousandSeparatorGrouping;
            }
            set
            {
                _thousandSeparatorGrouping = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "DecimalPlaces" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string DecimalPlaces
        {
            get
            {
                return _decimalPlaces;
            }
            set
            {
                _decimalPlaces = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "DecimalSeparator" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string DecimalSeparator
        {
            get
            {
                return _decimalSeparator;
            }
            set
            {
                _decimalSeparator = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsUserDefinedCurrency" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public bool? IsUserDefinedCurrency
        {
            get
            {
                return _isUserDefinedCurrency;
            }
            set
            {
                _isUserDefinedCurrency = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "ExchangeRate" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public double? ExchangeRate
        {
            get
            {
                return _exchangeRate;
            }
            set
            {
                _exchangeRate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AsOfDate" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public DateTime? AsOfDate
        {
            get
            {
                return _asOfDate;
            }
            set
            {
                _asOfDate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Status" field. Length must be between 0 and 10 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        [DataMember]
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        #endregion

    }
}
