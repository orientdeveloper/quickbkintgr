//
// Class	:	Inventoryadjustments.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:49 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class Inventoryadjustments :AbstractDalBase
    {
        #region Constructors / Destructors
        public Inventoryadjustments(string constr)
            :base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                inventoryadjustmentTableAdapter _inventoryadjustmentTableAdapter = new inventoryadjustmentTableAdapter();
                _inventoryadjustmentTableAdapter.Connection = Connection;
                _inventoryadjustmentTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Inventoryadjustment inventoryadjustment in this.Items)
                {
                    QBSynDal.List.QBSynDal.inventoryadjustmentDataTable inventoryadjustmentTable = _inventoryadjustmentTableAdapter.GetDataByTxnID(inventoryadjustment.TxnID);
                    DataRow dr = null;

                    if (inventoryadjustmentTable.Rows.Count == 0)
                    {
                        dr = inventoryadjustmentTable.NewRow();
                        inventoryadjustmentTable.Rows.Add(dr);
                        Bind(dr, inventoryadjustment);
                    }
                    else
                    {
                        dr = inventoryadjustmentTable.Rows[0];
                        Bind(dr, inventoryadjustment);
                    }
                    _inventoryadjustmentTableAdapter.Update(inventoryadjustmentTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Method (Private)
        private void Bind(DataRow dr, Inventoryadjustment inventoryadjustment)
        {
            dr[InventoryadjustmentFields.TxnID] = inventoryadjustment.TxnID;
            dr[InventoryadjustmentFields.TimeCreated] = inventoryadjustment.TimeCreated;
            dr[InventoryadjustmentFields.TimeModified] = inventoryadjustment.TimeModified;
            dr[InventoryadjustmentFields.EditSequence] = inventoryadjustment.EditSequence;
            dr[InventoryadjustmentFields.TxnNumber] = inventoryadjustment.TxnNumber.GetDbType();
            dr[InventoryadjustmentFields.AccountRef_ListID] = inventoryadjustment.AccountRef_ListID;
            dr[InventoryadjustmentFields.AccountRef_FullName] = inventoryadjustment.AccountRef_FullName;
            dr[InventoryadjustmentFields.InventorySiteRef_ListID] = inventoryadjustment.InventorySiteRef_ListID;
            dr[InventoryadjustmentFields.InventorySiteRef_FullName] = inventoryadjustment.InventorySiteRef_FullName;
            dr[InventoryadjustmentFields.TxnDate] = inventoryadjustment.TxnDate.GetDbType();
            dr[InventoryadjustmentFields.RefNumber] = inventoryadjustment.RefNumber;
            dr[InventoryadjustmentFields.CustomerRef_ListID] = inventoryadjustment.CustomerRef_ListID;
            dr[InventoryadjustmentFields.CustomerRef_FullName] = inventoryadjustment.CustomerRef_FullName;
            dr[InventoryadjustmentFields.ClassRef_ListID] = inventoryadjustment.ClassRef_ListID;
            dr[InventoryadjustmentFields.ClassRef_FullName] = inventoryadjustment.ClassRef_FullName;
            dr[InventoryadjustmentFields.Memo] = inventoryadjustment.Memo;
            dr[InventoryadjustmentFields.CustomField1] = inventoryadjustment.CustomField1;
            dr[InventoryadjustmentFields.CustomField2] = inventoryadjustment.CustomField2;
            dr[InventoryadjustmentFields.CustomField3] = inventoryadjustment.CustomField3;
            dr[InventoryadjustmentFields.CustomField4] = inventoryadjustment.CustomField4;
            dr[InventoryadjustmentFields.CustomField5] = inventoryadjustment.CustomField5;
            dr[InventoryadjustmentFields.CustomField6] = inventoryadjustment.CustomField6;
            dr[InventoryadjustmentFields.CustomField7] = inventoryadjustment.CustomField7;
            dr[InventoryadjustmentFields.CustomField8] = inventoryadjustment.CustomField8;
            dr[InventoryadjustmentFields.CustomField9] = inventoryadjustment.CustomField9;
            dr[InventoryadjustmentFields.CustomField10] = inventoryadjustment.CustomField10;
            dr[InventoryadjustmentFields.Status] = inventoryadjustment.Status;

        }
        #endregion
    }
}
