//
// Class	:	Arrefundcreditcards.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List;
using QBSynDal.List.QBSynDalTableAdapters;
using QBSynDal.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace QBSynDal
{
    public class Invoicelinedetails : AbstractDalBase
    {
        #region Constructors / Destructors
        public Invoicelinedetails(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                invoicelinedetailTableAdapter _invoicelinedetailTableAdapter = new invoicelinedetailTableAdapter();
                _invoicelinedetailTableAdapter.Connection = Connection;
                _invoicelinedetailTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                QBSynDal.List.QBSynDal.invoicelinedetailDataTable invoicelinedetailDataTable = new List.QBSynDal.invoicelinedetailDataTable();
                foreach (Invoicelinedetail invoicelinedetail in this.Items)
                {
                    DataRow dr = invoicelinedetailDataTable.NewRow();
                    invoicelinedetailDataTable.Rows.Add(dr);
                    Bind(dr, invoicelinedetail);
                }
                _invoicelinedetailTableAdapter.Update(invoicelinedetailDataTable);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Method (Private)
        private void Bind(DataRow dr, Invoicelinedetail invoicelinedetail)
        {
            dr[InvoicelinedetailFields.TxnLineID] = invoicelinedetail.TxnLineID;
            dr[InvoicelinedetailFields.ItemRef_ListID] = invoicelinedetail.ItemRef_ListID;
            dr[InvoicelinedetailFields.ItemRef_FullName] = invoicelinedetail.ItemRef_FullName;
            dr[InvoicelinedetailFields.Desc] = invoicelinedetail.Desc;
            dr[InvoicelinedetailFields.Quantity] = invoicelinedetail.Quantity;
            dr[InvoicelinedetailFields.UnitOfMeasure] = invoicelinedetail.UnitOfMeasure;
            dr[InvoicelinedetailFields.OverrideUOMSetRef_ListID] = invoicelinedetail.OverrideUOMSetRef_ListID;
            dr[InvoicelinedetailFields.OverrideUOMSetRef_FullName] = invoicelinedetail.OverrideUOMSetRef_FullName;
            dr[InvoicelinedetailFields.Rate] = invoicelinedetail.Rate;
            dr[InvoicelinedetailFields.RatePercent] = invoicelinedetail.RatePercent;
            dr[InvoicelinedetailFields.ClassRef_ListID] = invoicelinedetail.ClassRef_ListID;
            dr[InvoicelinedetailFields.ClassRef_FullName] = invoicelinedetail.ClassRef_FullName;
            dr[InvoicelinedetailFields.Amount] = invoicelinedetail.Amount.GetDbType();
            dr[InvoicelinedetailFields.InventorySiteRef_ListID] = invoicelinedetail.InventorySiteRef_ListID;
            dr[InvoicelinedetailFields.InventorySiteRef_FullName] = invoicelinedetail.InventorySiteRef_FullName;
            dr[InvoicelinedetailFields.ServiceDate] = invoicelinedetail.ServiceDate.GetDbType();
            dr[InvoicelinedetailFields.SalesTaxCodeRef_ListID] = invoicelinedetail.SalesTaxCodeRef_ListID;
            dr[InvoicelinedetailFields.SalesTaxCodeRef_FullName] = invoicelinedetail.SalesTaxCodeRef_FullName;
            dr[InvoicelinedetailFields.Other1] = invoicelinedetail.Other1;
            dr[InvoicelinedetailFields.Other2] = invoicelinedetail.Other2;
            dr[InvoicelinedetailFields.LinkedTxnID] = invoicelinedetail.LinkedTxnID;
            dr[InvoicelinedetailFields.LinkedTxnLineID] = invoicelinedetail.LinkedTxnLineID;
            dr[InvoicelinedetailFields.CustomField1] = invoicelinedetail.CustomField1;
            dr[InvoicelinedetailFields.CustomField2] = invoicelinedetail.CustomField2;
            dr[InvoicelinedetailFields.CustomField3] = invoicelinedetail.CustomField3;
            dr[InvoicelinedetailFields.CustomField4] = invoicelinedetail.CustomField4;
            dr[InvoicelinedetailFields.CustomField5] = invoicelinedetail.CustomField5;
            dr[InvoicelinedetailFields.CustomField6] = invoicelinedetail.CustomField6;
            dr[InvoicelinedetailFields.CustomField7] = invoicelinedetail.CustomField7;
            dr[InvoicelinedetailFields.CustomField8] = invoicelinedetail.CustomField8;
            dr[InvoicelinedetailFields.CustomField9] = invoicelinedetail.CustomField9;
            dr[InvoicelinedetailFields.CustomField10] = invoicelinedetail.CustomField10;
            dr[InvoicelinedetailFields.Idkey] = invoicelinedetail.Idkey;
            dr[InvoicelinedetailFields.GroupIDKEY] = invoicelinedetail.GroupIDKEY;
        }
        #endregion

    }
}
