﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using QBSynDal.List;
using QBSynDal.List.QBSynDalTableAdapters;
using QBSynDal.Helper;

namespace QBSynDal
{
    public class QBPushPurchaseOrders : AbstractPushDalBase
    {
        #region constructor

        public QBPushPurchaseOrders(string constr)
            : base(constr)
        {

        }

        #endregion

        #region public method



        public List<long> PurchaseOrderTxnID()
        {
            List<long> PurchaseOrderTxnIds = new List<long>();
            try
            {
                QBPushPurchaseorderTableAdapter qbpushpurchaseordertableadapter = new QBPushPurchaseorderTableAdapter();
                qbpushpurchaseordertableadapter.Connection = Connection;
                QBSynDal.List.QBSynDal.QBPushPurchaseorderDataTable qbpushpurchaseorderdatatable = qbpushpurchaseordertableadapter.GetDataByPushStatus();
                if (qbpushpurchaseorderdatatable != null)
                {
                    foreach (DataRow dr in qbpushpurchaseorderdatatable)
                    {
                        PurchaseOrderTxnIds.Add((long)dr[QBPushPurchaseorderFields.TxnID]);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return PurchaseOrderTxnIds;
        }

        public QBPushPurchaseorder QBPushPurchaseOrder(long TxnId)
        {
            QBPushPurchaseorder QBPushpurchaseorders = new QBPushPurchaseorder();
            try
            {
                QBPushPurchaseorderTableAdapter qbpushpurchaseordertableadapter = new QBPushPurchaseorderTableAdapter();
                qbpushpurchaseordertableadapter.Connection = Connection;
                QBSynDal.List.QBSynDal.QBPushPurchaseorderDataTable qbpushpurchaseorderdatatable = qbpushpurchaseordertableadapter.GetDataByTxnID(TxnId);
                if (qbpushpurchaseorderdatatable != null && qbpushpurchaseorderdatatable.Count == 1)
                {
                    DataRow dr = qbpushpurchaseorderdatatable.Rows[0];
                    QBPushPurchaseorder _qbpushpurchaseorder = new QBPushPurchaseorder();
                    _qbpushpurchaseorder.TxnID = dr[QBPushPurchaseorderFields.TxnID].IsDBNull() ? null : (long?)dr[QBPushPurchaseorderFields.TxnID];
                    _qbpushpurchaseorder.defMacro = dr[QBPushPurchaseorderFields.defMacro].IsDBNull() ? null : dr[QBPushPurchaseorderFields.defMacro].ToString();
                    _qbpushpurchaseorder.VendorRef_ListID = dr[QBPushPurchaseorderFields.VendorRef_ListID].IsDBNull() ? null : dr[QBPushPurchaseorderFields.VendorRef_ListID].ToString();
                    _qbpushpurchaseorder.VendorRef_FullName = dr[QBPushPurchaseorderFields.VendorRef_FullName].IsDBNull() ? null : dr[QBPushPurchaseorderFields.VendorRef_FullName].ToString();
                    _qbpushpurchaseorder.ClassRef_ListID = dr[QBPushPurchaseorderFields.ClassRef_ListID].IsDBNull() ? null : dr[QBPushPurchaseorderFields.ClassRef_ListID].ToString();
                    _qbpushpurchaseorder.ClassRef_FullName = dr[QBPushPurchaseorderFields.ClassRef_FullName].IsDBNull() ? null : dr[QBPushPurchaseorderFields.ClassRef_FullName].ToString();
                    _qbpushpurchaseorder.InventorySiteRef_ListID = dr[QBPushPurchaseorderFields.InventorySiteRef_ListID].IsDBNull() ? null : dr[QBPushPurchaseorderFields.InventorySiteRef_ListID].ToString();
                    _qbpushpurchaseorder.InventorySiteRef_FullName = dr[QBPushPurchaseorderFields.InventorySiteRef_FullName].IsDBNull() ? null : dr[QBPushPurchaseorderFields.InventorySiteRef_FullName].ToString();
                    _qbpushpurchaseorder.ShipToEntityRef_ListID = dr[QBPushPurchaseorderFields.ShipToEntityRef_ListID].IsDBNull() ? null : dr[QBPushPurchaseorderFields.ShipToEntityRef_ListID].ToString();
                    _qbpushpurchaseorder.ShipToEntityRef_FullName = dr[QBPushPurchaseorderFields.ShipToEntityRef_FullName].IsDBNull() ? null : dr[QBPushPurchaseorderFields.ShipToEntityRef_FullName].ToString();
                    _qbpushpurchaseorder.TemplateRef_ListID = dr[QBPushPurchaseorderFields.TemplateRef_ListID].IsDBNull() ? null : dr[QBPushPurchaseorderFields.TemplateRef_ListID].ToString();
                    _qbpushpurchaseorder.TemplateRef_FullName = dr[QBPushPurchaseorderFields.TemplateRef_FullName].IsDBNull() ? null : dr[QBPushPurchaseorderFields.TemplateRef_FullName].ToString();
                    _qbpushpurchaseorder.TxnDate = dr[QBPushPurchaseorderFields.TxnDate].IsDBNull() ? null : (DateTime?)dr[QBPushPurchaseorderFields.TxnDate];
                    _qbpushpurchaseorder.RefNumber = dr[QBPushPurchaseorderFields.RefNumber].IsDBNull() ? null : dr[QBPushPurchaseorderFields.RefNumber].ToString();
                    _qbpushpurchaseorder.VendorAddress_Addr1 = dr[QBPushPurchaseorderFields.VendorAddress_Addr1].IsDBNull() ? null : dr[QBPushPurchaseorderFields.VendorAddress_Addr1].ToString();
                    _qbpushpurchaseorder.VendorAddress_Addr2 = dr[QBPushPurchaseorderFields.VendorAddress_Addr2].IsDBNull() ? null : dr[QBPushPurchaseorderFields.VendorAddress_Addr2].ToString();
                    _qbpushpurchaseorder.VendorAddress_Addr3 = dr[QBPushPurchaseorderFields.VendorAddress_Addr3].IsDBNull() ? null : dr[QBPushPurchaseorderFields.VendorAddress_Addr3].ToString();
                    _qbpushpurchaseorder.VendorAddress_Addr4 = dr[QBPushPurchaseorderFields.VendorAddress_Addr4].IsDBNull() ? null : dr[QBPushPurchaseorderFields.VendorAddress_Addr4].ToString();
                    _qbpushpurchaseorder.VendorAddress_Addr5 = dr[QBPushPurchaseorderFields.VendorAddress_Addr5].IsDBNull() ? null : dr[QBPushPurchaseorderFields.VendorAddress_Addr5].ToString();
                    _qbpushpurchaseorder.VendorAddress_City = dr[QBPushPurchaseorderFields.VendorAddress_City].IsDBNull() ? null : dr[QBPushPurchaseorderFields.VendorAddress_City].ToString();
                    _qbpushpurchaseorder.VendorAddress_State = dr[QBPushPurchaseorderFields.VendorAddress_State].IsDBNull() ? null : dr[QBPushPurchaseorderFields.VendorAddress_State].ToString();
                    _qbpushpurchaseorder.VendorAddress_PostalCode = dr[QBPushPurchaseorderFields.VendorAddress_PostalCode].IsDBNull() ? null : dr[QBPushPurchaseorderFields.VendorAddress_PostalCode].ToString();
                    _qbpushpurchaseorder.VendorAddress_Country = dr[QBPushPurchaseorderFields.VendorAddress_Country].IsDBNull() ? null : dr[QBPushPurchaseorderFields.VendorAddress_Country].ToString();
                    _qbpushpurchaseorder.VendorAddress_Note = dr[QBPushPurchaseorderFields.VendorAddress_Note].IsDBNull() ? null : dr[QBPushPurchaseorderFields.VendorAddress_Note].ToString();
                    _qbpushpurchaseorder.ShipAddress_Addr1 = dr[QBPushPurchaseorderFields.ShipAddress_Addr1].IsDBNull() ? null : dr[QBPushPurchaseorderFields.ShipAddress_Addr1].ToString();
                    _qbpushpurchaseorder.ShipAddress_Addr2 = dr[QBPushPurchaseorderFields.ShipAddress_Addr2].IsDBNull() ? null : dr[QBPushPurchaseorderFields.ShipAddress_Addr2].ToString();
                    _qbpushpurchaseorder.ShipAddress_Addr3 = dr[QBPushPurchaseorderFields.ShipAddress_Addr3].IsDBNull() ? null : dr[QBPushPurchaseorderFields.ShipAddress_Addr3].ToString();
                    _qbpushpurchaseorder.ShipAddress_Addr4 = dr[QBPushPurchaseorderFields.ShipAddress_Addr4].IsDBNull() ? null : dr[QBPushPurchaseorderFields.ShipAddress_Addr4].ToString();
                    _qbpushpurchaseorder.ShipAddress_Addr5 = dr[QBPushPurchaseorderFields.ShipAddress_Addr5].IsDBNull() ? null : dr[QBPushPurchaseorderFields.ShipAddress_Addr5].ToString();
                    _qbpushpurchaseorder.ShipAddress_City = dr[QBPushPurchaseorderFields.ShipAddress_City].IsDBNull() ? null : dr[QBPushPurchaseorderFields.ShipAddress_City].ToString();
                    _qbpushpurchaseorder.ShipAddress_State = dr[QBPushPurchaseorderFields.ShipAddress_State].IsDBNull() ? null : dr[QBPushPurchaseorderFields.ShipAddress_State].ToString();
                    _qbpushpurchaseorder.ShipAddress_PostalCode = dr[QBPushPurchaseorderFields.ShipAddress_PostalCode].IsDBNull() ? null : dr[QBPushPurchaseorderFields.ShipAddress_PostalCode].ToString();
                    _qbpushpurchaseorder.ShipAddress_Country = dr[QBPushPurchaseorderFields.ShipAddress_Country].IsDBNull() ? null : dr[QBPushPurchaseorderFields.ShipAddress_Country].ToString();
                    _qbpushpurchaseorder.ShipAddress_Note = dr[QBPushPurchaseorderFields.ShipAddress_Note].IsDBNull() ? null : dr[QBPushPurchaseorderFields.ShipAddress_Note].ToString();
                    _qbpushpurchaseorder.TermsRef_ListID = dr[QBPushPurchaseorderFields.TermsRef_ListID].IsDBNull() ? null : dr[QBPushPurchaseorderFields.TermsRef_ListID].ToString();
                    _qbpushpurchaseorder.TermsRef_FullName = dr[QBPushPurchaseorderFields.TermsRef_FullName].IsDBNull() ? null : dr[QBPushPurchaseorderFields.TermsRef_FullName].ToString();
                    _qbpushpurchaseorder.DueDate = dr[QBPushPurchaseorderFields.DueDate].IsDBNull() ? null : (DateTime?)dr[QBPushPurchaseorderFields.DueDate];
                    _qbpushpurchaseorder.ExpectedDate = dr[QBPushPurchaseorderFields.ExpectedDate].IsDBNull() ? null : (DateTime?)dr[QBPushPurchaseorderFields.ExpectedDate];
                    _qbpushpurchaseorder.ShipMethodRef_ListID = dr[QBPushPurchaseorderFields.ShipMethodRef_ListID].IsDBNull() ? null : dr[QBPushPurchaseorderFields.ShipMethodRef_ListID].ToString();
                    _qbpushpurchaseorder.ShipMethodRef_FullName = dr[QBPushPurchaseorderFields.ShipMethodRef_FullName].IsDBNull() ? null : dr[QBPushPurchaseorderFields.ShipMethodRef_FullName].ToString();
                    _qbpushpurchaseorder.Fob = dr[QBPushPurchaseorderFields.Fob].IsDBNull() ? null : dr[QBPushPurchaseorderFields.Fob].ToString();
                    _qbpushpurchaseorder.ExchangeRate = dr[QBPushPurchaseorderFields.ExchangeRate].IsDBNull() ? null : (double?)dr[QBPushPurchaseorderFields.ExchangeRate];
                    _qbpushpurchaseorder.Memo = dr[QBPushPurchaseorderFields.Memo].IsDBNull() ? null : dr[QBPushPurchaseorderFields.Memo].ToString();
                    _qbpushpurchaseorder.VendorMsg = dr[QBPushPurchaseorderFields.VendorMsg].IsDBNull() ? null : dr[QBPushPurchaseorderFields.VendorMsg].ToString();
                    _qbpushpurchaseorder.IsToBePrinted = dr[QBPushPurchaseorderFields.IsToBePrinted].IsDBNull() ? null : (bool?)dr[QBPushPurchaseorderFields.IsToBePrinted];
                    _qbpushpurchaseorder.IsToBeEmailed = dr[QBPushPurchaseorderFields.IsToBeEmailed].IsDBNull() ? null : (bool?)dr[QBPushPurchaseorderFields.IsToBeEmailed];
                    _qbpushpurchaseorder.Other1 = dr[QBPushPurchaseorderFields.Other1].IsDBNull() ? null : dr[QBPushPurchaseorderFields.Other1].ToString();
                    _qbpushpurchaseorder.Other2 = dr[QBPushPurchaseorderFields.Other2].IsDBNull() ? null : dr[QBPushPurchaseorderFields.Other2].ToString();
                    _qbpushpurchaseorder.ExternalGUID = dr[QBPushPurchaseorderFields.ExternalGUID].IsDBNull() ? null : (Guid?)dr[QBPushPurchaseorderFields.ExternalGUID];
                    _qbpushpurchaseorder.PushStatus = dr[QBPushPurchaseorderFields.PushStatus].IsDBNull() ? null : (byte?)dr[QBPushPurchaseorderFields.PushStatus];
                    _qbpushpurchaseorder.PushDate = dr[QBPushPurchaseorderFields.PushDate].IsDBNull() ? null : (DateTime?)dr[QBPushPurchaseorderFields.PushDate];
                    _qbpushpurchaseorder.ErrorMessage = dr[QBPushPurchaseorderFields.ErrorMessage].IsDBNull() ? null : dr[QBPushPurchaseorderFields.ErrorMessage].ToString();
                    _qbpushpurchaseorder.ISSharePointApproved = dr[QBPushPurchaseorderFields.ISSharePointApproved].IsDBNull() ? null : (bool?)dr[QBPushPurchaseorderFields.ISSharePointApproved];
                    _qbpushpurchaseorder.ISSharePointApprovedDate = dr[QBPushPurchaseorderFields.ISSharePointApprovedDate].IsDBNull() ? null : (DateTime?)dr[QBPushPurchaseorderFields.ISSharePointApprovedDate];
                    // set the property for QBPushPurchaseorderlinedetail
                    _qbpushpurchaseorder.QBPushPurchaseorderlinedetail = GetQBPushPurchaseOrderlinedetail(TxnId);
                    // set the property for QBPushPurchaseorderlinegroupdetail
                    _qbpushpurchaseorder.QBPushPurchaseorderlinegroupdetail = GetQBPushPurchaseOrderlinegroupdetail(TxnId);

                    return _qbpushpurchaseorder;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return QBPushpurchaseorders;
        }

        public bool PushStatusUpdate(long TxnID, string ErrorMessage, PushStatus Status, DateTime PushDate)
        {
            try
            {
                QBPushPurchaseorderTableAdapter qbpushpurchaseordertableadapter = new QBPushPurchaseorderTableAdapter();
                qbpushpurchaseordertableadapter.Connection = Connection;
                qbpushpurchaseordertableadapter.Transaction = Transaction;

                int updatedRows = qbpushpurchaseordertableadapter.Update((byte?)Status, PushDate, ErrorMessage, TxnID);
                if (updatedRows > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Private Methods

        private List<QBPushPurchaseorderlinedetail> GetQBPushPurchaseOrderlinedetail(long TxnId)
        {
            List<QBPushPurchaseorderlinedetail> QBPushpurchaseorderlinedetails = new List<QBPushPurchaseorderlinedetail>();
            QBPushPurchaseorderlinedetailTableAdapter qbpushpurchaseorderlinedetailtableadapter = new QBPushPurchaseorderlinedetailTableAdapter();
            qbpushpurchaseorderlinedetailtableadapter.Connection = Connection;

            QBSynDal.List.QBSynDal.QBPushPurchaseorderlinedetailDataTable qbpushpurchaseorderlinedetaildatatable = qbpushpurchaseorderlinedetailtableadapter.GetDataByIDKEY(TxnId);
            if (qbpushpurchaseorderlinedetaildatatable != null )
            {
                foreach (DataRow dr in qbpushpurchaseorderlinedetaildatatable)
                {
                    QBPushPurchaseorderlinedetail _qbpushpurchaseorderlinedetail = new QBPushPurchaseorderlinedetail();

                    _qbpushpurchaseorderlinedetail.defMacro = dr[QBPushPurchaseorderlinedetailFields.defMacro].IsDBNull() ? null : dr[QBPushPurchaseorderlinedetailFields.defMacro].ToString();
                    _qbpushpurchaseorderlinedetail.ItemRef_ListID = dr[QBPushPurchaseorderlinedetailFields.ItemRef_ListID].IsDBNull() ? null : dr[QBPushPurchaseorderlinedetailFields.ItemRef_ListID].ToString();
                    _qbpushpurchaseorderlinedetail.ItemRef_FullName = dr[QBPushPurchaseorderlinedetailFields.ItemRef_FullName].IsDBNull() ? null : dr[QBPushPurchaseorderlinedetailFields.ItemRef_FullName].ToString();
                    _qbpushpurchaseorderlinedetail.ManufacturerPartNumber = dr[QBPushPurchaseorderlinedetailFields.ManufacturerPartNumber].IsDBNull() ? null : dr[QBPushPurchaseorderlinedetailFields.ManufacturerPartNumber].ToString();
                    _qbpushpurchaseorderlinedetail.Desc = dr[QBPushPurchaseorderlinedetailFields.Desc].IsDBNull() ? null : dr[QBPushPurchaseorderlinedetailFields.Desc].ToString();
                    _qbpushpurchaseorderlinedetail.Quantity = dr[QBPushPurchaseorderlinedetailFields.Quantity].IsDBNull() ? null : (double?)dr[QBPushPurchaseorderlinedetailFields.Quantity];
                    _qbpushpurchaseorderlinedetail.UnitOfMeasure = dr[QBPushPurchaseorderlinedetailFields.UnitOfMeasure].IsDBNull() ? null : dr[QBPushPurchaseorderlinedetailFields.UnitOfMeasure].ToString();
                    _qbpushpurchaseorderlinedetail.Rate = dr[QBPushPurchaseorderlinedetailFields.Rate].IsDBNull() ? null : (double?)dr[QBPushPurchaseorderlinedetailFields.Rate];
                    _qbpushpurchaseorderlinedetail.ClassRef_ListID = dr[QBPushPurchaseorderlinedetailFields.ClassRef_ListID].IsDBNull() ? null : dr[QBPushPurchaseorderlinedetailFields.ClassRef_ListID].ToString();
                    _qbpushpurchaseorderlinedetail.ClassRef_FullName = dr[QBPushPurchaseorderlinedetailFields.ClassRef_FullName].IsDBNull() ? null : dr[QBPushPurchaseorderlinedetailFields.ClassRef_FullName].ToString();
                    _qbpushpurchaseorderlinedetail.Amount = dr[QBPushPurchaseorderlinedetailFields.Amount].IsDBNull() ? null : (double?)dr[QBPushPurchaseorderlinedetailFields.Amount];
                    _qbpushpurchaseorderlinedetail.InventorySiteLocationRef_ListID = dr[QBPushPurchaseorderlinedetailFields.InventorySiteLocationRef_ListID].IsDBNull() ? null : dr[QBPushPurchaseorderlinedetailFields.InventorySiteLocationRef_ListID].ToString();
                    _qbpushpurchaseorderlinedetail.InventorySiteLocationRef_FullName = dr[QBPushPurchaseorderlinedetailFields.InventorySiteLocationRef_FullName].IsDBNull() ? null : dr[QBPushPurchaseorderlinedetailFields.InventorySiteLocationRef_FullName].ToString();
                    _qbpushpurchaseorderlinedetail.CustomerRef_ListID = dr[QBPushPurchaseorderlinedetailFields.CustomerRef_ListID].IsDBNull() ? null : dr[QBPushPurchaseorderlinedetailFields.CustomerRef_ListID].ToString();
                    _qbpushpurchaseorderlinedetail.CustomerRef_FullName = dr[QBPushPurchaseorderlinedetailFields.CustomerRef_FullName].IsDBNull() ? null : dr[QBPushPurchaseorderlinedetailFields.CustomerRef_FullName].ToString();
                    _qbpushpurchaseorderlinedetail.ServiceDate = dr[QBPushPurchaseorderlinedetailFields.ServiceDate].IsDBNull() ? null : (DateTime?)dr[QBPushPurchaseorderlinedetailFields.ServiceDate];
                    _qbpushpurchaseorderlinedetail.OverrideItemAccountRef_ListID = dr[QBPushPurchaseorderlinedetailFields.OverrideItemAccountRef_ListID].IsDBNull() ? null : dr[QBPushPurchaseorderlinedetailFields.OverrideItemAccountRef_ListID].ToString();
                    _qbpushpurchaseorderlinedetail.OverrideItemAccountRef_FullName = dr[QBPushPurchaseorderlinedetailFields.OverrideItemAccountRef_FullName].IsDBNull() ? null : dr[QBPushPurchaseorderlinedetailFields.OverrideItemAccountRef_FullName].ToString();
                    _qbpushpurchaseorderlinedetail.Other1 = dr[QBPushPurchaseorderlinedetailFields.Other1].IsDBNull() ? null : dr[QBPushPurchaseorderlinedetailFields.Other1].ToString();
                    _qbpushpurchaseorderlinedetail.Other2 = dr[QBPushPurchaseorderlinedetailFields.Other2].IsDBNull() ? null : dr[QBPushPurchaseorderlinedetailFields.Other2].ToString();
                    _qbpushpurchaseorderlinedetail.Idkey = dr[QBPushPurchaseorderlinedetailFields.Idkey].IsDBNull() ? null : (long?)dr[QBPushPurchaseorderlinedetailFields.Idkey];
                    _qbpushpurchaseorderlinedetail.DataExt_OwnerID = dr[QBPushPurchaseorderlinedetailFields.DataExt_OwnerID].IsDBNull() ? null : (Guid?)dr[QBPushPurchaseorderlinedetailFields.DataExt_OwnerID];
                    _qbpushpurchaseorderlinedetail.DataExt_DataExtName = dr[QBPushPurchaseorderlinedetailFields.DataExt_DataExtName].IsDBNull() ? null : dr[QBPushPurchaseorderlinedetailFields.DataExt_DataExtName].ToString();
                    _qbpushpurchaseorderlinedetail.DataExt_DataExtValue = dr[QBPushPurchaseorderlinedetailFields.DataExt_DataExtValue].IsDBNull() ? null : dr[QBPushPurchaseorderlinedetailFields.DataExt_DataExtValue].ToString();
                    QBPushpurchaseorderlinedetails.Add(_qbpushpurchaseorderlinedetail);
                }
            }
            return QBPushpurchaseorderlinedetails;
        }

        private List<QBPushPurchaseorderlinegroupdetail> GetQBPushPurchaseOrderlinegroupdetail(long TxnId)
        {
            List<QBPushPurchaseorderlinegroupdetail> QBPushPurchaseorderlinegroupdetails = new List<QBPushPurchaseorderlinegroupdetail>();
            QBPushPurchaseorderlinegroupdetailTableAdapter qbpushpurchaseorderlinegroupdetailtableadapter = new QBPushPurchaseorderlinegroupdetailTableAdapter();
            qbpushpurchaseorderlinegroupdetailtableadapter.Connection = Connection;

            QBSynDal.List.QBSynDal.QBPushPurchaseorderlinegroupdetailDataTable qbpushpurchaseorderlinegroupdetaildatatable = qbpushpurchaseorderlinegroupdetailtableadapter.GetDataByIDKEY(TxnId);
            if (qbpushpurchaseorderlinegroupdetaildatatable != null )
            {
                foreach (DataRow dr in qbpushpurchaseorderlinegroupdetaildatatable)
                {
                    QBPushPurchaseorderlinegroupdetail _qbpushpurchaseorderlinegroupdetail = new QBPushPurchaseorderlinegroupdetail();

                    _qbpushpurchaseorderlinegroupdetail.ItemGroupRef_ListID = dr[QBPushPurchaseorderlinegroupdetailFields.ItemGroupRef_ListID].IsDBNull() ? null : dr[QBPushPurchaseorderlinegroupdetailFields.ItemGroupRef_ListID].ToString();
                    _qbpushpurchaseorderlinegroupdetail.ItemGroupRef_FullName = dr[QBPushPurchaseorderlinegroupdetailFields.ItemGroupRef_FullName].IsDBNull() ? null : dr[QBPushPurchaseorderlinegroupdetailFields.ItemGroupRef_FullName].ToString();
                    _qbpushpurchaseorderlinegroupdetail.Quantity = dr[QBPushPurchaseorderlinegroupdetailFields.Quantity].IsDBNull() ? null : (double?)dr[QBPushPurchaseorderlinegroupdetailFields.Quantity];
                    _qbpushpurchaseorderlinegroupdetail.UnitOfMeasure = dr[QBPushPurchaseorderlinegroupdetailFields.UnitOfMeasure].IsDBNull() ? null : dr[QBPushPurchaseorderlinegroupdetailFields.UnitOfMeasure].ToString();
                    _qbpushpurchaseorderlinegroupdetail.InventorySiteLocationRef_ListID = dr[QBPushPurchaseorderlinegroupdetailFields.InventorySiteLocationRef_ListID].IsDBNull() ? null : dr[QBPushPurchaseorderlinegroupdetailFields.InventorySiteLocationRef_ListID].ToString();
                    _qbpushpurchaseorderlinegroupdetail.InventorySiteLocationRef_FullName = dr[QBPushPurchaseorderlinegroupdetailFields.InventorySiteLocationRef_FullName].IsDBNull() ? null : dr[QBPushPurchaseorderlinegroupdetailFields.InventorySiteLocationRef_FullName].ToString();
                    _qbpushpurchaseorderlinegroupdetail.Idkey = dr[QBPushPurchaseorderlinegroupdetailFields.Idkey].IsDBNull() ? null : (long?)dr[QBPushPurchaseorderlinegroupdetailFields.Idkey];
                    _qbpushpurchaseorderlinegroupdetail.DataExt_OwnerID = dr[QBPushPurchaseorderlinegroupdetailFields.DataExt_OwnerID].IsDBNull() ? null : (Guid?)dr[QBPushPurchaseorderlinegroupdetailFields.DataExt_OwnerID];
                    _qbpushpurchaseorderlinegroupdetail.DataExt_DataExtName = dr[QBPushPurchaseorderlinegroupdetailFields.DataExt_DataExtName].IsDBNull() ? null : dr[QBPushPurchaseorderlinegroupdetailFields.DataExt_DataExtName].ToString();
                    _qbpushpurchaseorderlinegroupdetail.DataExt_DataExtValue = dr[QBPushPurchaseorderlinegroupdetailFields.DataExt_DataExtValue].IsDBNull() ? null : dr[QBPushPurchaseorderlinegroupdetailFields.DataExt_DataExtValue].ToString();
                    QBPushPurchaseorderlinegroupdetails.Add(_qbpushpurchaseorderlinegroupdetail);
                }
            }

            return QBPushPurchaseorderlinegroupdetails;
        }

        #endregion
    }
}
