//
// Class	:	Items.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;
namespace QBSynDal
{
    public class Iteminventories : AbstractDalBase
    {
        #region Constructors / Destructors
        public Iteminventories(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                iteminventoryTableAdapter _iteminventoryTableAdapter = new iteminventoryTableAdapter();
                _iteminventoryTableAdapter.Connection = Connection;
                _iteminventoryTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                   
                }

                foreach (Iteminventory iteminventory in this.Items)
                {
                    QBSynDal.List.QBSynDal.iteminventoryDataTable iteminventoryTable = _iteminventoryTableAdapter.GetDataByListID(iteminventory.ListID);
                    DataRow dr = null;

                    if (iteminventoryTable.Rows.Count == 0)
                    {
                        dr = iteminventoryTable.NewRow();
                        iteminventoryTable.Rows.Add(dr);
                        Bind(dr, iteminventory);
                    }
                    else
                    {
                        dr = iteminventoryTable.Rows[0];
                        Bind(dr, iteminventory);
                    }
                    _iteminventoryTableAdapter.Update(iteminventoryTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Method (Private)
        private void Bind(DataRow dr, Iteminventory iteminventory)
        {
            try
            {
                dr[IteminventoryFields.ListID] = iteminventory.ListID;
                dr[IteminventoryFields.TimeCreated] = iteminventory.TimeCreated;
                dr[IteminventoryFields.TimeModified] = iteminventory.TimeModified;
                dr[IteminventoryFields.EditSequence] = iteminventory.EditSequence;
                dr[IteminventoryFields.Name] = iteminventory.Name;
                dr[IteminventoryFields.FullName] = iteminventory.FullName;
                dr[IteminventoryFields.IsActive] = iteminventory.IsActive.GetShort().GetDbType();
                dr[IteminventoryFields.ParentRef_FullName] = iteminventory.ParentRef_FullName;
                dr[IteminventoryFields.ParentRef_ListID] = iteminventory.ParentRef_ListID;
                dr[IteminventoryFields.Sublevel] = iteminventory.Sublevel.GetDbType();
                dr[IteminventoryFields.ManufacturerPartNumber] = iteminventory.ManufacturerPartNumber;
                dr[IteminventoryFields.UnitOfMeasureSetRef_ListID] = iteminventory.UnitOfMeasureSetRef_ListID;
                dr[IteminventoryFields.UnitOfMeasureSetRef_FullName] = iteminventory.UnitOfMeasureSetRef_FullName;
                dr[IteminventoryFields.IsTaxIncluded] = iteminventory.IsTaxIncluded.GetShort().GetDbType();
                dr[IteminventoryFields.SalesTaxCodeRef_FullName] = iteminventory.SalesTaxCodeRef_FullName;
                dr[IteminventoryFields.SalesTaxCodeRef_ListID] = iteminventory.SalesTaxCodeRef_ListID;
                dr[IteminventoryFields.SalesDesc] = iteminventory.SalesDesc;
                dr[IteminventoryFields.SalesPrice] = iteminventory.SalesPrice;
                dr[IteminventoryFields.IncomeAccountRef_ListID] = iteminventory.IncomeAccountRef_ListID;
                dr[IteminventoryFields.IncomeAccountRef_FullName] = iteminventory.IncomeAccountRef_FullName;
                dr[IteminventoryFields.PurchaseDesc] = iteminventory.PurchaseDesc;
                dr[IteminventoryFields.PurchaseCost] = iteminventory.PurchaseCost;
                dr[IteminventoryFields.COGSAccountRef_ListID] = iteminventory.COGSAccountRef_ListID;
                dr[IteminventoryFields.COGSAccountRef_FullName] = iteminventory.COGSAccountRef_FullName;
                dr[IteminventoryFields.PrefVendorRef_ListID] = iteminventory.PrefVendorRef_ListID;
                dr[IteminventoryFields.PrefVendorRef_FullName] = iteminventory.PrefVendorRef_FullName;
                dr[IteminventoryFields.AssetAccountRef_ListID] = iteminventory.AssetAccountRef_ListID;
                dr[IteminventoryFields.AssetAccountRef_FullName] = iteminventory.AssetAccountRef_FullName;
                dr[IteminventoryFields.ReorderPoint] = iteminventory.ReorderPoint;
                dr[IteminventoryFields.QuantityOnHand] = iteminventory.QuantityOnHand;
                dr[IteminventoryFields.AverageCost] = iteminventory.AverageCost;
                dr[IteminventoryFields.QuantityOnOrder] = iteminventory.QuantityOnOrder;
                dr[IteminventoryFields.QuantityOnSalesOrder] = iteminventory.QuantityOnSalesOrder;
                dr[IteminventoryFields.CustomField1] = iteminventory.CustomField1;
                dr[IteminventoryFields.CustomField10] = iteminventory.CustomField10;
                dr[IteminventoryFields.CustomField2] = iteminventory.CustomField2;
                dr[IteminventoryFields.CustomField2] = iteminventory.CustomField3;
                dr[IteminventoryFields.CustomField2] = iteminventory.CustomField4;
                dr[IteminventoryFields.CustomField2] = iteminventory.CustomField5;
                dr[IteminventoryFields.CustomField2] = iteminventory.CustomField6;
                dr[IteminventoryFields.CustomField2] = iteminventory.CustomField7;
                dr[IteminventoryFields.CustomField2] = iteminventory.CustomField8;
                dr[IteminventoryFields.CustomField2] = iteminventory.CustomField9;
                dr[IteminventoryFields.Status] = iteminventory.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
