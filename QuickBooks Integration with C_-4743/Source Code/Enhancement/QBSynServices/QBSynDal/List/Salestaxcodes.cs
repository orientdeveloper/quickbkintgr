//
// Class	:	Salestaxcodes.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;
namespace QBSynDal
{
    public class Salestaxcodes : AbstractDalBase
    {
        #region Constructors / Destructors
        public Salestaxcodes(string constr)
            :base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                salestaxcodeTableAdapter _salestaxcodeTableAdapter = new salestaxcodeTableAdapter();
                _salestaxcodeTableAdapter.Connection = Connection;
                _salestaxcodeTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Salestaxcode salestaxcode in this.Items)
                {
                    QBSynDal.List.QBSynDal.salestaxcodeDataTable salestaxcodeTable = _salestaxcodeTableAdapter.GetDataByListID(salestaxcode.ListID);
                    DataRow dr = null;

                    if (salestaxcodeTable.Rows.Count == 0)
                    {
                        dr = salestaxcodeTable.NewRow();
                        salestaxcodeTable.Rows.Add(dr);
                        Bind(dr, salestaxcode);
                    }
                    else
                    {
                        dr = salestaxcodeTable.Rows[0];
                        Bind(dr, salestaxcode);
                    }
                    _salestaxcodeTableAdapter.Update(salestaxcodeTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Method (Private)
        private void Bind(DataRow dr, Salestaxcode salestaxcode)
        {
            try
            {
                dr[SalestaxcodeFields.ListID] = salestaxcode.ListID;
                dr[SalestaxcodeFields.TimeCreated] = salestaxcode.TimeCreated;
                dr[SalestaxcodeFields.TimeModified] = salestaxcode.TimeModified;
                dr[SalestaxcodeFields.EditSequence] = salestaxcode.EditSequence;
                dr[SalestaxcodeFields.Name] = salestaxcode.Name;
                dr[SalestaxcodeFields.IsActive] = salestaxcode.IsActive.GetShort().GetDbType();
                dr[SalestaxcodeFields.IsTaxable] = salestaxcode.IsTaxable.GetShort().GetDbType();
                dr[SalestaxcodeFields.Desc] = salestaxcode.Desc;
                dr[SalestaxcodeFields.ItemPurchaseTaxRef_ListID] = salestaxcode.ItemPurchaseTaxRef_ListID;
                dr[SalestaxcodeFields.ItemPurchaseTaxRef_FullName] = salestaxcode.ItemPurchaseTaxRef_FullName;
                dr[SalestaxcodeFields.ItemSalesTaxRef_ListID] = salestaxcode.ItemSalesTaxRef_ListID;
                dr[SalestaxcodeFields.ItemSalesTaxRef_FullName] = salestaxcode.ItemSalesTaxRef_FullName;
                dr[SalestaxcodeFields.Status] = salestaxcode.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
