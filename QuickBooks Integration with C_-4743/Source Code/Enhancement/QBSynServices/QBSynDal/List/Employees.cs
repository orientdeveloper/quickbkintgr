//
// Class	:	Bills.cs
// Author	:  	SynapseIndia © 2013
// Date		:	8/2/2013 10:11:43 AM
//


using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.List.QBSynDalTableAdapters;
using QBSynDal.Helper;

namespace QBSynDal
{
    public class Employees : AbstractDalBase
    {
        #region Constructors / Destructors
        public Employees(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                
                BeginTransaction();
                employeeTableAdapter _employeeTableAdapter = new employeeTableAdapter();
                _employeeTableAdapter.Connection = Connection;
                _employeeTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {

                }

                foreach (Employee employee in this.Items)
                {
                    QBSynDal.List.QBSynDal.employeeDataTable employeeTable = _employeeTableAdapter.GetDataByListID(employee.ListID);
                    DataRow dr = null;
                    //changed for elmah
                    if (employeeTable.Rows.Count == 0)
                    { 
                         dr = employeeTable.NewRow();
                         employeeTable.Rows.Add(dr);
                        Bind(dr, employee);
                    }
                    else
                    {
                        dr = employeeTable.Rows[0];
                        Bind(dr, employee);
                    }
                    _employeeTableAdapter.Update(employeeTable);
                }

                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
                
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Employee employee)
        {
            try
            {
                dr[EmployeeFields.ListID] = employee.ListID;
                dr[EmployeeFields.TimeCreated] = employee.TimeCreated;
                dr[EmployeeFields.TimeModified] = employee.TimeModified;
                dr[EmployeeFields.EditSequence] = employee.EditSequence;
                dr[EmployeeFields.Name] = employee.Name;
                dr[EmployeeFields.IsActive] = employee.IsActive.GetShort().GetDbType();
                dr[EmployeeFields.Salutation] = employee.Salutation;
                dr[EmployeeFields.FirstName] = employee.FirstName;
                dr[EmployeeFields.MiddleName] = employee.MiddleName;
                dr[EmployeeFields.LastName] = employee.LastName;
                dr[EmployeeFields.Suffix] = employee.Suffix;
                dr[EmployeeFields.EmployeeAddress_Addr1] = employee.EmployeeAddress_Addr1;
                dr[EmployeeFields.EmployeeAddress_Addr2] = employee.EmployeeAddress_Addr2;
                dr[EmployeeFields.EmployeeAddress_Addr3] = employee.EmployeeAddress_Addr3;
                dr[EmployeeFields.EmployeeAddress_Addr4] = employee.EmployeeAddress_Addr4;
                dr[EmployeeFields.EmployeeAddress_City] = employee.EmployeeAddress_City;
                dr[EmployeeFields.EmployeeAddress_State] = employee.EmployeeAddress_State;
                dr[EmployeeFields.EmployeeAddress_PostalCode] = employee.EmployeeAddress_PostalCode;
                dr[EmployeeFields.EmployeeAddress_Country] = employee.EmployeeAddress_Country;
                dr[EmployeeFields.PrintAs] = employee.PrintAs;
                dr[EmployeeFields.Phone] = employee.Phone;
                dr[EmployeeFields.Mobile] = employee.Mobile;
                dr[EmployeeFields.Pager] = employee.Pager;
                dr[EmployeeFields.PagerPIN] = employee.PagerPIN;
                dr[EmployeeFields.AltPhone] = employee.AltPhone;
                dr[EmployeeFields.Fax] = employee.Fax;
                dr[EmployeeFields.Ssn] = employee.Ssn;
                dr[EmployeeFields.Email] = employee.Email;
                dr[EmployeeFields.EmployeeType] = employee.EmployeeType;
                dr[EmployeeFields.Gender] = employee.Gender;
                dr[EmployeeFields.HiredDate] = employee.HiredDate.GetDbType();
                dr[EmployeeFields.ReleasedDate] = employee.ReleasedDate.GetDbType();
                dr[EmployeeFields.BirthDate] = employee.BirthDate.GetDbType();
                dr[EmployeeFields.AccountNumber] = employee.AccountNumber;
                dr[EmployeeFields.Notes] = employee.Notes;
                dr[EmployeeFields.BillingRateRef_ListID] = employee.BillingRateRef_ListID;
                dr[EmployeeFields.BillingRateRef_FullName] = employee.BillingRateRef_FullName;
                dr[EmployeeFields.CustomField1] = employee.CustomField1;
                dr[EmployeeFields.CustomField2] = employee.CustomField2;
                dr[EmployeeFields.CustomField3] = employee.CustomField3;
                dr[EmployeeFields.CustomField4] = employee.CustomField4;
                dr[EmployeeFields.CustomField5] = employee.CustomField5;
                dr[EmployeeFields.CustomField6] = employee.CustomField6;
                dr[EmployeeFields.CustomField7] = employee.CustomField7;
                dr[EmployeeFields.CustomField8] = employee.CustomField8;
                dr[EmployeeFields.CustomField9] = employee.CustomField9;
                dr[EmployeeFields.CustomField10] = employee.CustomField10;
                dr[EmployeeFields.Status] = employee.Status;
                
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
