//
// Class	:	Purchaseorderlinedetails.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List;
using QBSynDal.List.QBSynDalTableAdapters;
using QBSynDal.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Linq;
using System.Text;

namespace QBSynDal
{
    public class Purchaseorderlinedetails : AbstractDalBase
    {
        #region Constructors / Destructors
        public Purchaseorderlinedetails(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                purchaseorderlinedetailTableAdapter _purchaseorderlinedetailTableAdapter = new purchaseorderlinedetailTableAdapter();
                _purchaseorderlinedetailTableAdapter.Connection = Connection;
                _purchaseorderlinedetailTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                QBSynDal.List.QBSynDal.purchaseorderlinedetailDataTable purchaseorderlinedetailDataTable = new List.QBSynDal.purchaseorderlinedetailDataTable();
                foreach (Purchaseorderlinedetail purchaseorderlinedetail in this.Items)
                {
                    DataRow dr = purchaseorderlinedetailDataTable.NewRow();
                    purchaseorderlinedetailDataTable.Rows.Add(dr);
                    Bind(dr, purchaseorderlinedetail);
                }
                _purchaseorderlinedetailTableAdapter.Update(purchaseorderlinedetailDataTable);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Purchaseorderlinedetail purchaseorderlinedetail)
        {
            try
            {
                dr[PurchaseorderlinedetailFields.TxnLineID] = purchaseorderlinedetail.TxnLineID;
                dr[PurchaseorderlinedetailFields.ItemRef_ListID] = purchaseorderlinedetail.ItemRef_ListID;
                dr[PurchaseorderlinedetailFields.ItemRef_FullName] = purchaseorderlinedetail.ItemRef_FullName;
                dr[PurchaseorderlinedetailFields.ManufacturerPartNumber] = purchaseorderlinedetail.ManufacturerPartNumber;
                dr[PurchaseorderlinedetailFields.Desc] = purchaseorderlinedetail.Desc;
                dr[PurchaseorderlinedetailFields.Quantity] = purchaseorderlinedetail.Quantity;
                dr[PurchaseorderlinedetailFields.UnitOfMeasure] = purchaseorderlinedetail.UnitOfMeasure;
                dr[PurchaseorderlinedetailFields.OverrideUOMSetRef_ListID] = purchaseorderlinedetail.OverrideUOMSetRef_ListID;
                dr[PurchaseorderlinedetailFields.OverrideUOMSetRef_FullName] = purchaseorderlinedetail.OverrideUOMSetRef_FullName;
                dr[PurchaseorderlinedetailFields.Rate] = purchaseorderlinedetail.Rate;
                dr[PurchaseorderlinedetailFields.ClassRef_ListID] = purchaseorderlinedetail.ClassRef_ListID;
                dr[PurchaseorderlinedetailFields.ClassRef_FullName] = purchaseorderlinedetail.ClassRef_FullName;
                dr[PurchaseorderlinedetailFields.Amount] = purchaseorderlinedetail.Amount.GetDbType();
                dr[PurchaseorderlinedetailFields.CustomerRef_ListID] = purchaseorderlinedetail.CustomerRef_ListID;
                dr[PurchaseorderlinedetailFields.CustomerRef_FullName] = purchaseorderlinedetail.CustomerRef_FullName;
                dr[PurchaseorderlinedetailFields.ServiceDate] = purchaseorderlinedetail.ServiceDate.GetDbType();
                dr[PurchaseorderlinedetailFields.SalesTaxCodeRef_ListID] = purchaseorderlinedetail.SalesTaxCodeRef_ListID;
                dr[PurchaseorderlinedetailFields.SalesTaxCodeRef_FullName] = purchaseorderlinedetail.SalesTaxCodeRef_FullName;
                dr[PurchaseorderlinedetailFields.ReceivedQuantity] = purchaseorderlinedetail.ReceivedQuantity;
                dr[PurchaseorderlinedetailFields.Other1] = purchaseorderlinedetail.Other1;
                dr[PurchaseorderlinedetailFields.Other2] = purchaseorderlinedetail.Other2;
                dr[PurchaseorderlinedetailFields.IsManuallyClosed] = purchaseorderlinedetail.IsManuallyClosed;
                dr[PurchaseorderlinedetailFields.CustomField1] = purchaseorderlinedetail.CustomField1;
                dr[PurchaseorderlinedetailFields.CustomField2] = purchaseorderlinedetail.CustomField2;
                dr[PurchaseorderlinedetailFields.CustomField3] = purchaseorderlinedetail.CustomField3;
                dr[PurchaseorderlinedetailFields.CustomField4] = purchaseorderlinedetail.CustomField4;
                dr[PurchaseorderlinedetailFields.CustomField5] = purchaseorderlinedetail.CustomField5;
                dr[PurchaseorderlinedetailFields.CustomField6] = purchaseorderlinedetail.CustomField6;
                dr[PurchaseorderlinedetailFields.CustomField7] = purchaseorderlinedetail.CustomField7;
                dr[PurchaseorderlinedetailFields.CustomField8] = purchaseorderlinedetail.CustomField8;
                dr[PurchaseorderlinedetailFields.CustomField9] = purchaseorderlinedetail.CustomField9;
                dr[PurchaseorderlinedetailFields.CustomField10] = purchaseorderlinedetail.CustomField10;
                dr[PurchaseorderlinedetailFields.Idkey] = purchaseorderlinedetail.Idkey;
                dr[PurchaseorderlinedetailFields.GroupIDKEY] = purchaseorderlinedetail.GroupIDKEY;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
