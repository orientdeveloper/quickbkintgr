//
// Class	:	Estimates.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;
namespace QBSynDal
{
    public class Estimates :AbstractDalBase
    {

        #region Constructors / Destructors
        public Estimates(string constr)
            :base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                estimateTableAdapter _estimateTableAdapter = new estimateTableAdapter();
                _estimateTableAdapter.Connection = Connection;
                _estimateTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Estimate estimate in this.Items)
                {
                    QBSynDal.List.QBSynDal.estimateDataTable estimateTable = _estimateTableAdapter.GetDataByTxnID(estimate.TxnID);
                    DataRow dr = null;

                    if (estimateTable.Rows.Count == 0)
                    {
                        dr = estimateTable.NewRow();
                        estimateTable.Rows.Add(dr);
                        Bind(dr, estimate);
                    }
                    else
                    {
                        dr = estimateTable.Rows[0];
                        Bind(dr, estimate);
                    }
                    _estimateTableAdapter.Update(estimateTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Method (Private)
        private void Bind(DataRow dr, Estimate estimate)
        {
            try
            {
                dr[EstimateFields.TxnID] = estimate.TxnID;
                dr[EstimateFields.TimeCreated] = estimate.TimeCreated;
                dr[EstimateFields.TimeModified] = estimate.TimeModified;
                dr[EstimateFields.EditSequence] = estimate.EditSequence;
                dr[EstimateFields.TxnNumber] = estimate.TxnNumber.GetDbType();
                dr[EstimateFields.CustomerRef_ListID] = estimate.CustomerRef_ListID;
                dr[EstimateFields.CustomerRef_FullName] = estimate.CustomerRef_FullName;
                dr[EstimateFields.ClassRef_ListID] = estimate.ClassRef_ListID;
                dr[EstimateFields.ClassRef_FullName] = estimate.ClassRef_FullName;
                dr[EstimateFields.TemplateRef_ListID] = estimate.TemplateRef_ListID;
                dr[EstimateFields.TemplateRef_FullName] = estimate.TemplateRef_FullName;
                dr[EstimateFields.TxnDate] = estimate.TxnDate.GetDbType();
                dr[EstimateFields.RefNumber] = estimate.RefNumber;
                dr[EstimateFields.BillAddress_Addr1] = estimate.BillAddress_Addr1;
                dr[EstimateFields.BillAddress_Addr2] = estimate.BillAddress_Addr2;
                dr[EstimateFields.BillAddress_Addr3] = estimate.BillAddress_Addr3;
                dr[EstimateFields.BillAddress_Addr4] = estimate.BillAddress_Addr4;
                dr[EstimateFields.BillAddress_Addr5] = estimate.BillAddress_Addr5;
                dr[EstimateFields.BillAddress_City] = estimate.BillAddress_City;
                dr[EstimateFields.BillAddress_State] = estimate.BillAddress_State;
                dr[EstimateFields.BillAddress_PostalCode] = estimate.BillAddress_PostalCode;
                dr[EstimateFields.BillAddress_Country] = estimate.BillAddress_Country;
                dr[EstimateFields.BillAddress_Note] = estimate.BillAddress_Note;
                dr[EstimateFields.ShipAddress_Addr1] = estimate.ShipAddress_Addr1;
                dr[EstimateFields.ShipAddress_Addr2] = estimate.ShipAddress_Addr2;
                dr[EstimateFields.ShipAddress_Addr3] = estimate.ShipAddress_Addr3;
                dr[EstimateFields.ShipAddress_Addr4] = estimate.ShipAddress_Addr4;
                dr[EstimateFields.ShipAddress_Addr5] = estimate.ShipAddress_Addr5;
                dr[EstimateFields.ShipAddress_City] = estimate.ShipAddress_City;
                dr[EstimateFields.ShipAddress_State] = estimate.ShipAddress_State;
                dr[EstimateFields.ShipAddress_PostalCode] = estimate.ShipAddress_PostalCode;
                dr[EstimateFields.ShipAddress_Country] = estimate.ShipAddress_Country;
                dr[EstimateFields.ShipAddress_Note] = estimate.ShipAddress_Note;
                dr[EstimateFields.IsActive] = estimate.IsActive.GetShort().GetDbType();
                dr[EstimateFields.PONumber] = estimate.PONumber;
                dr[EstimateFields.TermsRef_ListID] = estimate.TermsRef_ListID;
                dr[EstimateFields.TermsRef_FullName] = estimate.TermsRef_FullName;
                dr[EstimateFields.DueDate] = estimate.DueDate.GetDbType();
                dr[EstimateFields.SalesRepRef_ListID] = estimate.SalesRepRef_ListID;
                dr[EstimateFields.SalesRepRef_FullName] = estimate.SalesRepRef_FullName;
                dr[EstimateFields.Fob] = estimate.Fob;
                dr[EstimateFields.Subtotal] = estimate.Subtotal.GetDbType();
                dr[EstimateFields.ItemSalesTaxRef_ListID] = estimate.ItemSalesTaxRef_ListID;
                dr[EstimateFields.ItemSalesTaxRef_FullName] = estimate.ItemSalesTaxRef_FullName;
                dr[EstimateFields.SalesTaxPercentage] = estimate.SalesTaxPercentage;
                dr[EstimateFields.SalesTaxTotal] = estimate.SalesTaxTotal.GetDbType();
                dr[EstimateFields.TotalAmount] = estimate.TotalAmount.GetDbType();
                dr[EstimateFields.CurrencyRef_ListID] = estimate.CurrencyRef_ListID;
                dr[EstimateFields.CurrencyRef_FullName] = estimate.CurrencyRef_FullName;
                dr[EstimateFields.ExchangeRate] = estimate.ExchangeRate.GetDbType();
                dr[EstimateFields.TotalAmountInHomeCurrency] = estimate.TotalAmountInHomeCurrency.GetDbType();
                dr[EstimateFields.Memo] = estimate.Memo;
                dr[EstimateFields.CustomerMsgRef_ListID] = estimate.CustomerMsgRef_ListID;
                dr[EstimateFields.CustomerMsgRef_FullName] = estimate.CustomerMsgRef_FullName;
                dr[EstimateFields.IsToBeEmailed] = estimate.IsToBeEmailed.GetDbType();
                dr[EstimateFields.CustomerSalesTaxCodeRef_ListID] = estimate.CustomerSalesTaxCodeRef_ListID;
                dr[EstimateFields.CustomerSalesTaxCodeRef_FullName] = estimate.CustomerSalesTaxCodeRef_FullName;
                dr[EstimateFields.Other] = estimate.Other;
                dr[EstimateFields.CustomField1] = estimate.CustomField1;
                dr[EstimateFields.CustomField2] = estimate.CustomField2;
                dr[EstimateFields.CustomField3] = estimate.CustomField3;
                dr[EstimateFields.CustomField4] = estimate.CustomField4;
                dr[EstimateFields.CustomField5] = estimate.CustomField5;
                dr[EstimateFields.CustomField6] = estimate.CustomField6;
                dr[EstimateFields.CustomField7] = estimate.CustomField7;
                dr[EstimateFields.CustomField8] = estimate.CustomField8;
                dr[EstimateFields.CustomField9] = estimate.CustomField9;
                dr[EstimateFields.CustomField10] = estimate.CustomField10;
                dr[EstimateFields.Status] = estimate.Status;
          
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        

    }
}
