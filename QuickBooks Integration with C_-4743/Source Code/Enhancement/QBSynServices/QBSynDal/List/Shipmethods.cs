//
// Class	:	Shipmethods.cs
// Author	:  	Synapse Software © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class Shipmethods : AbstractDalBase
    {
        #region Constructors / Destructors
        public Shipmethods(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                shipmethodTableAdapter _shipmethodTableAdapter = new shipmethodTableAdapter();
                _shipmethodTableAdapter.Connection = Connection;
                _shipmethodTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Shipmethod shipmethod in this.Items)
                {
                    QBSynDal.List.QBSynDal.shipmethodDataTable shipmethodTable = _shipmethodTableAdapter.GetDataByListID(shipmethod.ListID);
                    DataRow dr = null;

                    if (shipmethodTable.Rows.Count == 0)
                    {
                        dr = shipmethodTable.NewRow();
                        Bind(dr, shipmethod);
                        shipmethodTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = shipmethodTable.Rows[0];
                        Bind(dr, shipmethod);
                    }
                    _shipmethodTableAdapter.Update(shipmethodTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Shipmethod shipmethod)
        {
            try
            {
                dr[ShipmethodFields.ListID] = shipmethod.ListID;
                dr[ShipmethodFields.TimeCreated] = shipmethod.TimeCreated;
                dr[ShipmethodFields.TimeModified] = shipmethod.TimeModified;
                dr[ShipmethodFields.EditSequence] = shipmethod.EditSequence;
                dr[ShipmethodFields.Name] = shipmethod.Name;
                dr[ShipmethodFields.IsActive] = shipmethod.IsActive.GetShort().GetDbType();
                dr[ShipmethodFields.Status] = shipmethod.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
