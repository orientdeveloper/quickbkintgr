//
// Class	:	Billpaymentcheckpaymentchecks.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:43 AM
//
using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class Billpaymentchecks : AbstractDalBase
    {
        #region Constructors / Destructors
        public Billpaymentchecks(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                billpaymentcheckTableAdapter _billpaymentcheckTableAdapter = new billpaymentcheckTableAdapter();
                _billpaymentcheckTableAdapter.Connection = Connection;
                _billpaymentcheckTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Billpaymentcheck billpaymentcheck in this.Items)
                {
                    QBSynDal.List.QBSynDal.billpaymentcheckDataTable billpaymentcheckTable = _billpaymentcheckTableAdapter.GetDataByTxnID(billpaymentcheck.TxnID);
                    DataRow dr = null;

                    if (billpaymentcheckTable.Rows.Count == 0)
                    {
                        dr = billpaymentcheckTable.NewRow();
                        Bind(dr, billpaymentcheck);
                        billpaymentcheckTable.Rows.Add(dr);

                    }
                    else
                    {
                        dr = billpaymentcheckTable.Rows[0];
                        Bind(dr, billpaymentcheck);
                    }
                    _billpaymentcheckTableAdapter.Update(billpaymentcheckTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Billpaymentcheck billpaymentcheck)
        {
            try
            {
                dr[BillpaymentcheckFields.Address_Addr1] = billpaymentcheck.Address_Addr1;
                dr[BillpaymentcheckFields.Address_Addr2] = billpaymentcheck.Address_Addr2;
                dr[BillpaymentcheckFields.Address_Addr3] = billpaymentcheck.Address_Addr3;
                dr[BillpaymentcheckFields.Address_Addr4] = billpaymentcheck.Address_Addr4;
                dr[BillpaymentcheckFields.Address_Addr5] = billpaymentcheck.Address_Addr5;
                dr[BillpaymentcheckFields.Address_City] = billpaymentcheck.Address_City;
                dr[BillpaymentcheckFields.Address_Country] = billpaymentcheck.Address_Country;
                dr[BillpaymentcheckFields.Address_Note] = billpaymentcheck.Address_Note;
                dr[BillpaymentcheckFields.Address_PostalCode] = billpaymentcheck.Address_PostalCode;
                dr[BillpaymentcheckFields.Address_State] = billpaymentcheck.Address_State;
                dr[BillpaymentcheckFields.Amount] = billpaymentcheck.Amount.GetDbType();
                dr[BillpaymentcheckFields.AmountInHomeCurrency] = billpaymentcheck.AmountInHomeCurrency.GetDbType();
                dr[BillpaymentcheckFields.APAccountRef_ListID] = billpaymentcheck.APAccountRef_ListID;
                dr[BillpaymentcheckFields.APAccountRef_FullName] = billpaymentcheck.APAccountRef_FullName;
                dr[BillpaymentcheckFields.BankAccountRef_FullName] = billpaymentcheck.BankAccountRef_FullName;
                dr[BillpaymentcheckFields.BankAccountRef_ListID] = billpaymentcheck.BankAccountRef_ListID;
                dr[BillpaymentcheckFields.CurrencyRef_FullName] = billpaymentcheck.CurrencyRef_FullName;
                dr[BillpaymentcheckFields.CurrencyRef_ListID] = billpaymentcheck.CurrencyRef_ListID;
                dr[BillpaymentcheckFields.CustomField1] = billpaymentcheck.CustomField1;
                dr[BillpaymentcheckFields.CustomField2] = billpaymentcheck.CustomField2;
                dr[BillpaymentcheckFields.CustomField3] = billpaymentcheck.CustomField3;
                dr[BillpaymentcheckFields.CustomField4] = billpaymentcheck.CustomField4;
                dr[BillpaymentcheckFields.CustomField5] = billpaymentcheck.CustomField5;
                dr[BillpaymentcheckFields.CustomField6] = billpaymentcheck.CustomField6;
                dr[BillpaymentcheckFields.CustomField7] = billpaymentcheck.CustomField7;
                dr[BillpaymentcheckFields.CustomField8] = billpaymentcheck.CustomField8;
                dr[BillpaymentcheckFields.CustomField9] = billpaymentcheck.CustomField9;
                dr[BillpaymentcheckFields.CustomField10] = billpaymentcheck.CustomField10;
                dr[BillpaymentcheckFields.IsToBePrinted] = billpaymentcheck.IsToBePrinted.GetShort().GetDbType();
                dr[BillpaymentcheckFields.EditSequence] = billpaymentcheck.EditSequence;
                dr[BillpaymentcheckFields.ExchangeRate] = billpaymentcheck.ExchangeRate.GetDbType();
                dr[BillpaymentcheckFields.Memo] = billpaymentcheck.Memo;
                dr[BillpaymentcheckFields.RefNumber] = billpaymentcheck.RefNumber;
                dr[BillpaymentcheckFields.Status] = billpaymentcheck.Status;
                dr[BillpaymentcheckFields.TimeCreated] = billpaymentcheck.TimeCreated;
                dr[BillpaymentcheckFields.TimeModified] = billpaymentcheck.TimeModified;
                dr[BillpaymentcheckFields.TxnDate] = billpaymentcheck.TxnDate.GetDbType();
                dr[BillpaymentcheckFields.TxnID] = billpaymentcheck.TxnID;
                dr[BillpaymentcheckFields.TxnNumber] = billpaymentcheck.TxnNumber.GetDbType();
                dr[BillpaymentcheckFields.PayeeEntityRef_FullName] = billpaymentcheck.PayeeEntityRef_FullName;
                dr[BillpaymentcheckFields.PayeeEntityRef_ListID] = billpaymentcheck.PayeeEntityRef_ListID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
