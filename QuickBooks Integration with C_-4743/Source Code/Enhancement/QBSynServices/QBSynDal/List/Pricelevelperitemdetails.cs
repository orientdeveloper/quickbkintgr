//
// Class	:	Pricelevelperitemdetails.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List;
using QBSynDal.List.QBSynDalTableAdapters;
using QBSynDal.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace QBSynDal
{
    public class Pricelevelperitemdetails : AbstractDalBase
    {
        #region Constructors / Destructors
        public Pricelevelperitemdetails(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                pricelevelperitemdetailTableAdapter _pricelevelperitemdetailTableAdapter = new pricelevelperitemdetailTableAdapter();
                _pricelevelperitemdetailTableAdapter.Connection = Connection;
                _pricelevelperitemdetailTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                   
                }

                QBSynDal.List.QBSynDal.pricelevelperitemdetailDataTable pricelevelperitemdetailDataTable = new List.QBSynDal.pricelevelperitemdetailDataTable();
                foreach (Pricelevelperitemdetail pricelevelperitemdetail in this.Items)
                {
                    DataRow dr = pricelevelperitemdetailDataTable.NewRow();
                    pricelevelperitemdetailDataTable.Rows.Add(dr);
                    Bind(dr, pricelevelperitemdetail);
                }
                _pricelevelperitemdetailTableAdapter.Update(pricelevelperitemdetailDataTable);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Method (Private)
        private void Bind(DataRow dr, Pricelevelperitemdetail pricelevelperitemdetail)
        {
            dr[PricelevelperitemdetailFields.ItemRef_ListID] = pricelevelperitemdetail.ItemRef_ListID;
            dr[PricelevelperitemdetailFields.ItemRef_FullName] = pricelevelperitemdetail.ItemRef_FullName;
            dr[PricelevelperitemdetailFields.CustomPrice] = pricelevelperitemdetail.CustomPrice;
            dr[PricelevelperitemdetailFields.CustomPricePercent] = pricelevelperitemdetail.CustomPricePercent;
            dr[PricelevelperitemdetailFields.CustomField1] = pricelevelperitemdetail.CustomField1;
            dr[PricelevelperitemdetailFields.CustomField2] = pricelevelperitemdetail.CustomField2;
            dr[PricelevelperitemdetailFields.CustomField3] = pricelevelperitemdetail.CustomField3;
            dr[PricelevelperitemdetailFields.CustomField4] = pricelevelperitemdetail.CustomField4;
            dr[PricelevelperitemdetailFields.CustomField5] = pricelevelperitemdetail.CustomField5;
            dr[PricelevelperitemdetailFields.CustomField6] = pricelevelperitemdetail.CustomField6;
            dr[PricelevelperitemdetailFields.CustomField7] = pricelevelperitemdetail.CustomField7;
            dr[PricelevelperitemdetailFields.CustomField8] = pricelevelperitemdetail.CustomField8;
            dr[PricelevelperitemdetailFields.CustomField9] = pricelevelperitemdetail.CustomField9;
            dr[PricelevelperitemdetailFields.CustomField10] = pricelevelperitemdetail.CustomField10;
            dr[PricelevelperitemdetailFields.Idkey] = pricelevelperitemdetail.Idkey;
            dr[PricelevelperitemdetailFields.GroupIDKEY] = pricelevelperitemdetail.GroupIDKEY;
        }
        #endregion
    }
}
