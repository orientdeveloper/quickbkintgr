//
// Class	:	Itemsalestaxgrouplinedetails.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List;
using QBSynDal.List.QBSynDalTableAdapters;
using QBSynDal.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Linq;
using System.Text;

namespace QBSynDal
{
    public class Itemsalestaxgrouplinedetails : AbstractDalBase
    {
        #region Constructors / Destructors
        public Itemsalestaxgrouplinedetails(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                itemsalestaxgrouplinedetailTableAdapter _itemsalestaxgrouplinedetailTableAdapter = new itemsalestaxgrouplinedetailTableAdapter();
                _itemsalestaxgrouplinedetailTableAdapter.Connection = Connection;
                _itemsalestaxgrouplinedetailTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                QBSynDal.List.QBSynDal.itemsalestaxgrouplinedetailDataTable itemsalestaxgrouplinedetailDataTable = new List.QBSynDal.itemsalestaxgrouplinedetailDataTable();
                foreach (Itemsalestaxgrouplinedetail itemsalestaxgrouplinedetail in this.Items)
                {
                    DataRow dr = itemsalestaxgrouplinedetailDataTable.NewRow();
                    itemsalestaxgrouplinedetailDataTable.Rows.Add(dr);
                    Bind(dr, itemsalestaxgrouplinedetail);
                }
                _itemsalestaxgrouplinedetailTableAdapter.Update(itemsalestaxgrouplinedetailDataTable);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Itemsalestaxgrouplinedetail itemsalestaxgrouplinedetail)
        {
            try
            {
                dr[ItemsalestaxgrouplinedetailFields.ItemSalesTaxRef_ListID] = itemsalestaxgrouplinedetail.ItemSalesTaxRef_ListID;
                dr[ItemsalestaxgrouplinedetailFields.ItemSalesTaxRef_FullName] = itemsalestaxgrouplinedetail.ItemSalesTaxRef_FullName;
                dr[ItemsalestaxgrouplinedetailFields.CustomField1] = itemsalestaxgrouplinedetail.CustomField1;
                dr[ItemsalestaxgrouplinedetailFields.CustomField2] = itemsalestaxgrouplinedetail.CustomField2;
                dr[ItemsalestaxgrouplinedetailFields.CustomField3] = itemsalestaxgrouplinedetail.CustomField3;
                dr[ItemsalestaxgrouplinedetailFields.CustomField4] = itemsalestaxgrouplinedetail.CustomField4;
                dr[ItemsalestaxgrouplinedetailFields.CustomField5] = itemsalestaxgrouplinedetail.CustomField5;
                dr[ItemsalestaxgrouplinedetailFields.CustomField6] = itemsalestaxgrouplinedetail.CustomField6;
                dr[ItemsalestaxgrouplinedetailFields.CustomField7] = itemsalestaxgrouplinedetail.CustomField7;
                dr[ItemsalestaxgrouplinedetailFields.CustomField8] = itemsalestaxgrouplinedetail.CustomField8;
                dr[ItemsalestaxgrouplinedetailFields.CustomField9] = itemsalestaxgrouplinedetail.CustomField9;
                dr[ItemsalestaxgrouplinedetailFields.CustomField10] = itemsalestaxgrouplinedetail.CustomField10;
                dr[ItemsalestaxgrouplinedetailFields.Idkey] = itemsalestaxgrouplinedetail.Idkey;
                dr[ItemsalestaxgrouplinedetailFields.GroupIDKEY] = itemsalestaxgrouplinedetail.GroupIDKEY;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
