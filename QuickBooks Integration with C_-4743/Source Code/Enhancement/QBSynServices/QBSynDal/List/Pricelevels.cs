//
// Class	:	Accounts.cs
// Author	:  	Synapse Software © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class Pricelevels : AbstractDalBase
    {
        #region Constructors / Destructors
        public Pricelevels(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                pricelevelTableAdapter _pricelevelTableAdapter = new pricelevelTableAdapter();
                _pricelevelTableAdapter.Connection = Connection;
                _pricelevelTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Pricelevel pricelevel in this.Items)
                {
                    QBSynDal.List.QBSynDal.pricelevelDataTable pricelevelTable = _pricelevelTableAdapter.GetDataByListID(pricelevel.ListID);
                    DataRow dr = null;

                    if (pricelevelTable.Rows.Count == 0)
                    {
                        dr = pricelevelTable.NewRow();
                        Bind(dr, pricelevel);
                        pricelevelTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = pricelevelTable.Rows[0];
                        Bind(dr, pricelevel);
                    }
                    _pricelevelTableAdapter.Update(pricelevelTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Pricelevel pricelevel)
        {
            try
            {
                dr[PricelevelFields.ListID] = pricelevel.ListID;
                dr[PricelevelFields.TimeCreated] = pricelevel.TimeCreated;
                dr[PricelevelFields.TimeModified] = pricelevel.TimeModified;
                dr[PricelevelFields.EditSequence] = pricelevel.EditSequence;
                dr[PricelevelFields.Name] = pricelevel.Name;
                dr[PricelevelFields.IsActive] = pricelevel.IsActive.GetShort().GetDbType();
                dr[PricelevelFields.PriceLevelType] = pricelevel.PriceLevelType;
                dr[PricelevelFields.PriceLevelFixedPercentage] = pricelevel.PriceLevelFixedPercentage;
                dr[PricelevelFields.CurrencyRef_ListID] = pricelevel.CurrencyRef_ListID;
                dr[PricelevelFields.CurrencyRef_FullName] = pricelevel.CurrencyRef_FullName;
                dr[PricelevelFields.Status] = pricelevel.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
