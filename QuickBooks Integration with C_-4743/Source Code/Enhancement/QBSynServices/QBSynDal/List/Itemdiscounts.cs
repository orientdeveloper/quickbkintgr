//
// Class	:	Itemdiscounts.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:50 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class Itemdiscounts :AbstractDalBase
    {

       #region Constructors / Destructors
        public Itemdiscounts(string constr)
            :base(constr)
        {

        }
        #endregion

       #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                itemdiscountTableAdapter _itemdiscountTableAdapter = new itemdiscountTableAdapter();
                _itemdiscountTableAdapter.Connection = Connection;
                _itemdiscountTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Itemdiscount itemdiscount in this.Items)
                {
                    QBSynDal.List.QBSynDal.itemdiscountDataTable itemdiscountTable = _itemdiscountTableAdapter.GetDataByListID(itemdiscount.ListID);
                    DataRow dr = null;

                    if (itemdiscountTable.Rows.Count == 0)
                    {
                        dr = itemdiscountTable.NewRow();
                        itemdiscountTable.Rows.Add(dr);
                        Bind(dr, itemdiscount);
                    }
                    else
                    {
                        dr = itemdiscountTable.Rows[0];
                        Bind(dr, itemdiscount);
                    }
                    _itemdiscountTableAdapter.Update(itemdiscountTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

       #region Method (Private)
        private void Bind(DataRow dr, Itemdiscount itemdiscount)
        {
            dr[ItemdiscountFields.ListID] = itemdiscount.ListID;
            dr[ItemdiscountFields.TimeCreated] = itemdiscount.TimeCreated;
            dr[ItemdiscountFields.TimeModified] = itemdiscount.TimeModified;
            dr[ItemdiscountFields.EditSequence] = itemdiscount.EditSequence;
            dr[ItemdiscountFields.Name] = itemdiscount.Name;
            dr[ItemdiscountFields.FullName] = itemdiscount.FullName;
            dr[ItemdiscountFields.IsActive] = itemdiscount.IsActive.GetShort().GetDbType();
            dr[ItemdiscountFields.ParentRef_ListID] = itemdiscount.ParentRef_ListID;
            dr[ItemdiscountFields.ParentRef_FullName] = itemdiscount.ParentRef_FullName;
            dr[ItemdiscountFields.Sublevel] = itemdiscount.Sublevel.GetDbType();
            dr[ItemdiscountFields.ItemDesc] = itemdiscount.ItemDesc;
            dr[ItemdiscountFields.SalesTaxCodeRef_ListID] = itemdiscount.SalesTaxCodeRef_ListID;
            dr[ItemdiscountFields.SalesTaxCodeRef_FullName] = itemdiscount.SalesTaxCodeRef_FullName;
            dr[ItemdiscountFields.DiscountRate] = itemdiscount.DiscountRate;
            dr[ItemdiscountFields.DiscountRatePercent] = itemdiscount.DiscountRatePercent;
            dr[ItemdiscountFields.AccountRef_ListID] = itemdiscount.AccountRef_ListID;
            dr[ItemdiscountFields.AccountRef_FullName] = itemdiscount.AccountRef_FullName;
            dr[ItemdiscountFields.CustomField1] = itemdiscount.CustomField1;
            dr[ItemdiscountFields.CustomField2] = itemdiscount.CustomField2;
            dr[ItemdiscountFields.CustomField3] = itemdiscount.CustomField3;
            dr[ItemdiscountFields.CustomField4] = itemdiscount.CustomField4;
            dr[ItemdiscountFields.CustomField5] = itemdiscount.CustomField5;
            dr[ItemdiscountFields.CustomField6] = itemdiscount.CustomField6;
            dr[ItemdiscountFields.CustomField7] = itemdiscount.CustomField7;
            dr[ItemdiscountFields.CustomField8] = itemdiscount.CustomField8;
            dr[ItemdiscountFields.CustomField9] = itemdiscount.CustomField9;
            dr[ItemdiscountFields.CustomField10] = itemdiscount.CustomField10;
            dr[ItemdiscountFields.Status] = itemdiscount.Status;
        }
        #endregion
    }
}
