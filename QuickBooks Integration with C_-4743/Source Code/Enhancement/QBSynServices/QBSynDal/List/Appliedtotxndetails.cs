//
// Class	:	Arrefundcreditcards.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List;
using QBSynDal.List.QBSynDalTableAdapters;
using QBSynDal.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Linq;
using System.Text;

namespace QBSynDal
{
    public class Appliedtotxndetails : AbstractDalBase
    {
        #region Constructors / Destructors
        public Appliedtotxndetails(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                appliedtotxndetailTableAdapter _appliedtotxndetailTableAdapter = new appliedtotxndetailTableAdapter();
                _appliedtotxndetailTableAdapter.Connection = Connection;
                _appliedtotxndetailTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                QBSynDal.List.QBSynDal.appliedtotxndetailDataTable appliedtotxndetailDataTable = new List.QBSynDal.appliedtotxndetailDataTable();
                foreach (Appliedtotxndetail appliedtotxndetail in this.Items)
                {
                    DataRow dr = appliedtotxndetailDataTable.NewRow();
                    appliedtotxndetailDataTable.Rows.Add(dr);
                    Bind(dr, appliedtotxndetail);
                }
                _appliedtotxndetailTableAdapter.Update(appliedtotxndetailDataTable);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Appliedtotxndetail appliedtotxndetail)
        {
            try
            {
                dr[AppliedtotxndetailFields.TxnID] = appliedtotxndetail.TxnID;
                dr[AppliedtotxndetailFields.TxnType] = appliedtotxndetail.TxnType;
                dr[AppliedtotxndetailFields.TxnDate] = appliedtotxndetail.TxnDate.GetDbType();
                dr[AppliedtotxndetailFields.RefNumber] = appliedtotxndetail.RefNumber;
                dr[AppliedtotxndetailFields.BalanceRemaining] = appliedtotxndetail.BalanceRemaining.GetDbType();
                dr[AppliedtotxndetailFields.Amount] = appliedtotxndetail.Amount.GetDbType();
                dr[AppliedtotxndetailFields.DiscountAmount] = appliedtotxndetail.DiscountAmount.GetDbType();
                dr[AppliedtotxndetailFields.DiscountAccountRef_ListID] = appliedtotxndetail.DiscountAccountRef_ListID;
                dr[AppliedtotxndetailFields.DiscountAccountRef_FullName] = appliedtotxndetail.DiscountAccountRef_FullName;
                dr[AppliedtotxndetailFields.CustomField1] = appliedtotxndetail.CustomField1;
                dr[AppliedtotxndetailFields.CustomField2] = appliedtotxndetail.CustomField2;
                dr[AppliedtotxndetailFields.CustomField3] = appliedtotxndetail.CustomField3;
                dr[AppliedtotxndetailFields.CustomField4] = appliedtotxndetail.CustomField4;
                dr[AppliedtotxndetailFields.CustomField5] = appliedtotxndetail.CustomField5;
                dr[AppliedtotxndetailFields.CustomField6] = appliedtotxndetail.CustomField6;
                dr[AppliedtotxndetailFields.CustomField7] = appliedtotxndetail.CustomField7;
                dr[AppliedtotxndetailFields.CustomField8] = appliedtotxndetail.CustomField8;
                dr[AppliedtotxndetailFields.CustomField9] = appliedtotxndetail.CustomField9;
                dr[AppliedtotxndetailFields.CustomField10] = appliedtotxndetail.CustomField10;
                dr[AppliedtotxndetailFields.Idkey] = appliedtotxndetail.Idkey;
                dr[AppliedtotxndetailFields.GroupIDKEY] = appliedtotxndetail.GroupIDKEY;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
