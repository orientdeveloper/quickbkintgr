//
// Class	:	Arrefundcreditcards.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List;
using QBSynDal.List.QBSynDalTableAdapters;
using QBSynDal.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace QBSynDal
{
    public class Billingrateperitemdetails : AbstractDalBase
    {
      #region Constructors / Destructors
        public Billingrateperitemdetails(string constr)
            : base(constr)
        {

        }
        #endregion

      #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                billingrateperitemdetailTableAdapter _billingrateperitemdetailTableAdapter = new billingrateperitemdetailTableAdapter();
                _billingrateperitemdetailTableAdapter.Connection = Connection;
                _billingrateperitemdetailTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                QBSynDal.List.QBSynDal.billingrateperitemdetailDataTable billingrateperitemdetailDataTable = new List.QBSynDal.billingrateperitemdetailDataTable();
                foreach (Billingrateperitemdetail billingrateperitemdetail in this.Items)
                {
                    DataRow dr = billingrateperitemdetailDataTable.NewRow();
                    billingrateperitemdetailDataTable.Rows.Add(dr);
                    Bind(dr, billingrateperitemdetail);
                }
                _billingrateperitemdetailTableAdapter.Update(billingrateperitemdetailDataTable);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

      #region Method (Private)
        private void Bind(DataRow dr, Billingrateperitemdetail billingrateperitemdetail)
        {
            try
            {
                dr[BillingrateperitemdetailFields.ItemRef_ListID] = billingrateperitemdetail.ItemRef_ListID;
                dr[BillingrateperitemdetailFields.ItemRef_FullName] = billingrateperitemdetail.ItemRef_FullName;
                dr[BillingrateperitemdetailFields.CustomRate] = billingrateperitemdetail.CustomRate;
                dr[BillingrateperitemdetailFields.CustomRatePercent] = billingrateperitemdetail.CustomRatePercent;
                dr[BillingrateperitemdetailFields.CustomField1] = billingrateperitemdetail.CustomField1;
                dr[BillingrateperitemdetailFields.CustomField2] = billingrateperitemdetail.CustomField2;
                dr[BillingrateperitemdetailFields.CustomField3] = billingrateperitemdetail.CustomField3;
                dr[BillingrateperitemdetailFields.CustomField4] = billingrateperitemdetail.CustomField4;
                dr[BillingrateperitemdetailFields.CustomField5] = billingrateperitemdetail.CustomField5;
                dr[BillingrateperitemdetailFields.CustomField6] = billingrateperitemdetail.CustomField6;
                dr[BillingrateperitemdetailFields.CustomField7] = billingrateperitemdetail.CustomField7;
                dr[BillingrateperitemdetailFields.CustomField8] = billingrateperitemdetail.CustomField8;
                dr[BillingrateperitemdetailFields.CustomField9] = billingrateperitemdetail.CustomField9;
                dr[BillingrateperitemdetailFields.CustomField10] = billingrateperitemdetail.CustomField10;
                dr[BillingrateperitemdetailFields.Idkey] = billingrateperitemdetail.Idkey;
                dr[BillingrateperitemdetailFields.GroupIDKEY] = billingrateperitemdetail.GroupIDKEY;
      
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
