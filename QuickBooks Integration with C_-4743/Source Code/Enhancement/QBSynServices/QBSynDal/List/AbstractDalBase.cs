﻿using QBSynDal.Helper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;

namespace QBSynDal.List
{
    public abstract class AbstractDalBase : IDisposable
    {
        #region Class Level Variables
        SqlConnection _connection;
        SqlTransaction _transation;
        List<IModel> _items;
        #endregion

        #region Contructor
        public AbstractDalBase(string Constr)
        {
            _connection = new SqlConnection(Constr);
            _items = new List<IModel>();
        }
        #endregion

        #region Properties
        protected SqlTransaction Transaction
        {
            get
            {
                return _transation;
            }
        }

        protected SqlConnection Connection
        {
            get
            {
                return _connection;
            }
        }

        public List<IModel> Items
        {
            get
            {
                return _items;
            }
            set
            {
                _items = value;
            }
        }

        #endregion

        #region Public Method
        protected void BeginTransaction()
        {
            if (_connection.State == System.Data.ConnectionState.Closed)
            {
                _connection.Open();
            }
            _transation = _connection.BeginTransaction();
        }

        protected void CommitTransaction()
        {
            _transation.Commit();
            _connection.Close();
        }

        protected void RollbackTransaction()
        {
            _transation.Rollback();
            _connection.Close();
        }
        #endregion

        #region Abstract Method

        public abstract void SaveAll(SynAction synAction);
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (_connection.State == ConnectionState.Open)
                        _connection.Close();
                    _connection.Dispose();
                    _connection = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
