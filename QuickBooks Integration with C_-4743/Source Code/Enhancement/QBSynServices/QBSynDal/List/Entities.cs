//
// Class	:	Accounts.cs
// Author	:  	Synapse Software © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class Entities : AbstractDalBase
    {
        #region Constructors / Destructors
        public Entities(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                entityTableAdapter _entityTableAdapter = new entityTableAdapter();
                _entityTableAdapter.Connection = Connection;
                _entityTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Entity entity in this.Items)
                {
                    QBSynDal.List.QBSynDal.entityDataTable entityTable = _entityTableAdapter.GetDataByListID(entity.ListID);
                    DataRow dr = null;

                    if (entityTable.Rows.Count == 0)
                    {
                        dr = entityTable.NewRow();
                        Bind(dr, entity);
                        entityTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = entityTable.Rows[0];
                        Bind(dr, entity);
                    }
                    _entityTableAdapter.Update(entityTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Entity entity)
        {
            try
            {
                dr[EntityFields.ListID] = entity.ListID;
                dr[EntityFields.FullName] = entity.FullName;
                dr[EntityFields.TableName] = entity.TableName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
