//
// Class	:	Deposits.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:46 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class Deposits : AbstractDalBase
    {
        #region Constructors / Destructors
        public Deposits(string constr)
            :base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                depositTableAdapter _depositTableAdapter = new depositTableAdapter();
                _depositTableAdapter.Connection = Connection;
                _depositTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                   
                }

                foreach (Deposit deposit in this.Items)
                {
                    QBSynDal.List.QBSynDal.depositDataTable depositTable = _depositTableAdapter.GetDataByTxnID(deposit.TxnID);
                    DataRow dr = null;

                    if (depositTable.Rows.Count == 0)
                    {
                        dr = depositTable.NewRow();
                        depositTable.Rows.Add(dr);
                        Bind(dr, deposit);
                    }
                    else
                    {
                        dr = depositTable.Rows[0];
                        Bind(dr, deposit);
                    }
                    _depositTableAdapter.Update(depositTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Method (Private)
        private void Bind(DataRow dr, Deposit deposit)
        {
            dr[DepositFields.TxnID] = deposit.TxnID;
            dr[DepositFields.EditSequence] = deposit.EditSequence;
            dr[DepositFields.TimeCreated] = deposit.TimeCreated;
            dr[DepositFields.TimeModified] = deposit.TimeModified;
            dr[DepositFields.TxnNumber] = deposit.TxnNumber.GetDbType();
            dr[DepositFields.TxnDate] = deposit.TxnDate.GetDbType();
            dr[DepositFields.DepositToAccountRef_ListID] = deposit.DepositToAccountRef_ListID;
            dr[DepositFields.DepositToAccountRef_FullName] = deposit.DepositToAccountRef_FullName;
            dr[DepositFields.Memo] = deposit.Memo;
            dr[DepositFields.DepositTotal] = deposit.DepositTotal.GetDbType();
            dr[DepositFields.CurrencyRef_FullName] = deposit.CurrencyRef_FullName;
            dr[DepositFields.CurrencyRef_ListID] = deposit.CurrencyRef_ListID;
            dr[DepositFields.ExchangeRate] = deposit.ExchangeRate.GetDbType();
            dr[DepositFields.DepositTotalInHomeCurrency] = deposit.DepositTotalInHomeCurrency.GetDbType();
            dr[DepositFields.CustomField1] = deposit.CustomField1;
            dr[DepositFields.CustomField2] = deposit.CustomField2;
            dr[DepositFields.CustomField3] = deposit.CustomField3;
            dr[DepositFields.CustomField4] = deposit.CustomField4;
            dr[DepositFields.CustomField5] = deposit.CustomField5;
            dr[DepositFields.CustomField6] = deposit.CustomField6;
            dr[DepositFields.CustomField7] = deposit.CustomField7;
            dr[DepositFields.CustomField8] = deposit.CustomField8;
            dr[DepositFields.CustomField9] = deposit.CustomField9;
            dr[DepositFields.CustomField10] = deposit.CustomField10;
            dr[DepositFields.Status] = deposit.Status;
        }
        #endregion
        

        
    }
}
