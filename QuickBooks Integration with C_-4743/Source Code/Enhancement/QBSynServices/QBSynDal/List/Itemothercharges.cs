//
// Class	:	Itemothercharges.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:51 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class Itemothercharges : AbstractDalBase
    {
        #region Constructors / Destructors
        public Itemothercharges(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                itemotherchargeTableAdapter _itemotherchargeTableAdapter = new itemotherchargeTableAdapter();
                _itemotherchargeTableAdapter.Connection = Connection;
                _itemotherchargeTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Itemothercharge itemothercharge in this.Items)
                {
                    QBSynDal.List.QBSynDal.itemotherchargeDataTable itemotherchargeTable = _itemotherchargeTableAdapter.GetDataByListID(itemothercharge.ListID);
                    DataRow dr = null;

                    if (itemotherchargeTable.Rows.Count == 0)
                    {
                        dr = itemotherchargeTable.NewRow();
                        Bind(dr, itemothercharge);
                        itemotherchargeTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = itemotherchargeTable.Rows[0];
                        Bind(dr, itemothercharge);
                    }
                    _itemotherchargeTableAdapter.Update(itemotherchargeTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Itemothercharge itemothercharge)
        {
            try
            {
                dr[ItemotherchargeFields.ListID] = itemothercharge.ListID;
                dr[ItemotherchargeFields.TimeCreated] = itemothercharge.TimeCreated;
                dr[ItemotherchargeFields.TimeModified] = itemothercharge.TimeModified;
                dr[ItemotherchargeFields.EditSequence] = itemothercharge.EditSequence;
                dr[ItemotherchargeFields.Name] = itemothercharge.Name;
                dr[ItemotherchargeFields.FullName] = itemothercharge.FullName;
                dr[ItemotherchargeFields.IsActive] = itemothercharge.IsActive.GetShort().GetDbType();
                dr[ItemotherchargeFields.ParentRef_FullName] = itemothercharge.ParentRef_FullName;
                dr[ItemotherchargeFields.ParentRef_ListID] = itemothercharge.ParentRef_ListID;
                dr[ItemotherchargeFields.Sublevel] = itemothercharge.Sublevel.GetDbType();
                dr[ItemotherchargeFields.IsTaxIncluded] = itemothercharge.IsTaxIncluded.GetShort().GetDbType();
                dr[ItemotherchargeFields.SalesTaxCodeRef_FullName] = itemothercharge.SalesTaxCodeRef_FullName;
                dr[ItemotherchargeFields.SalesTaxCodeRef_ListID] = itemothercharge.SalesTaxCodeRef_ListID;
                dr[ItemotherchargeFields.SpecialItemType] = itemothercharge.SpecialItemType;
                dr[ItemotherchargeFields.CustomField1] = itemothercharge.CustomField1;
                dr[ItemotherchargeFields.CustomField10] = itemothercharge.CustomField10;
                dr[ItemotherchargeFields.CustomField2] = itemothercharge.CustomField2;
                dr[ItemotherchargeFields.CustomField2] = itemothercharge.CustomField3;
                dr[ItemotherchargeFields.CustomField2] = itemothercharge.CustomField4;
                dr[ItemotherchargeFields.CustomField2] = itemothercharge.CustomField5;
                dr[ItemotherchargeFields.CustomField2] = itemothercharge.CustomField6;
                dr[ItemotherchargeFields.CustomField2] = itemothercharge.CustomField7;
                dr[ItemotherchargeFields.CustomField2] = itemothercharge.CustomField8;
                dr[ItemotherchargeFields.CustomField2] = itemothercharge.CustomField9;
                dr[ItemotherchargeFields.Status] = itemothercharge.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
