//
// Class	:	Accounts.cs
// Author	:  	Synapse Software © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class ECFO_SALESTAX_ACCOUNT_LKUPs : AbstractDalBase
    {
        #region Constructors / Destructors
        public ECFO_SALESTAX_ACCOUNT_LKUPs(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                ECFO_SALESTAX_ACCOUNT_LKUPTableAdapter _ECFO_SALESTAX_ACCOUNT_LKUPTableAdapter = new ECFO_SALESTAX_ACCOUNT_LKUPTableAdapter();
                _ECFO_SALESTAX_ACCOUNT_LKUPTableAdapter.Connection = Connection;
                _ECFO_SALESTAX_ACCOUNT_LKUPTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (ECFO_SALESTAX_ACCOUNT_LKUP ecfo in this.Items)
                {
                    QBSynDal.List.QBSynDal.ECFO_SALESTAX_ACCOUNT_LKUPDataTable ECFO_SALESTAX_ACCOUNT_LKUPTable = _ECFO_SALESTAX_ACCOUNT_LKUPTableAdapter.GetDataByListID(ecfo.SalesTaxRef_ListID);
                    DataRow dr = null;

                    if (ECFO_SALESTAX_ACCOUNT_LKUPTable.Rows.Count == 0)
                    {
                        dr = ECFO_SALESTAX_ACCOUNT_LKUPTable.NewRow();
                        Bind(dr, ecfo);
                        ECFO_SALESTAX_ACCOUNT_LKUPTable.Rows.Add(dr);
                        _ECFO_SALESTAX_ACCOUNT_LKUPTableAdapter.Update(ECFO_SALESTAX_ACCOUNT_LKUPTable);
                    }
                    
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, ECFO_SALESTAX_ACCOUNT_LKUP ecfo)
        {
            try
            {
                dr[ECFO_SALESTAX_ACCOUNT_LKUP_Fields.AccountRef_ListID] = ecfo.AccountRef_ListID;
                dr[ECFO_SALESTAX_ACCOUNT_LKUP_Fields.SalesTaxRef_ListID] = ecfo.SalesTaxRef_ListID;
                dr[ECFO_SALESTAX_ACCOUNT_LKUP_Fields.AccountType] = ecfo.AccountType;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
