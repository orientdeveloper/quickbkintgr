//
// Class	:	Itemservices.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;
namespace QBSynDal
{
    public class Itemservices : AbstractDalBase
    {

        #region Constructors / Destructors
        public Itemservices(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                itemserviceTableAdapter _itemserviceTableAdapter = new itemserviceTableAdapter();
                _itemserviceTableAdapter.Connection = Connection;
                _itemserviceTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Itemservice itemservice in this.Items)
                {
                    QBSynDal.List.QBSynDal.itemserviceDataTable itemserviceTable = _itemserviceTableAdapter.GetDataByListID(itemservice.ListID);
                    DataRow dr = null;

                    if (itemserviceTable.Rows.Count == 0)
                    {
                        dr = itemserviceTable.NewRow();
                        Bind(dr, itemservice);
                        itemserviceTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = itemserviceTable.Rows[0];
                        Bind(dr, itemservice);
                    }
                    _itemserviceTableAdapter.Update(itemserviceTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Itemservice itemservice)
        {
            try
            {
                dr[ItemserviceFields.ListID] = itemservice.ListID;
                dr[ItemserviceFields.TimeCreated] = itemservice.TimeCreated;
                dr[ItemserviceFields.TimeModified] = itemservice.TimeModified;
                dr[ItemserviceFields.EditSequence] = itemservice.EditSequence;
                dr[ItemserviceFields.Name] = itemservice.Name;
                dr[ItemserviceFields.IsActive] = itemservice.IsActive.GetShort().GetDbType();
                dr[ItemserviceFields.FullName] = itemservice.FullName;
                dr[ItemserviceFields.ParentRef_ListID] = itemservice.ParentRef_ListID;
                dr[ItemserviceFields.ParentRef_FullName] = itemservice.ParentRef_FullName;
                dr[ItemserviceFields.Sublevel] = itemservice.Sublevel.GetDbType();
                dr[ItemserviceFields.UnitOfMeasureSetRef_ListID] = itemservice.UnitOfMeasureSetRef_ListID;
                dr[ItemserviceFields.UnitOfMeasureSetRef_FullName] = itemservice.UnitOfMeasureSetRef_FullName;
                dr[ItemserviceFields.IsTaxIncluded] = itemservice.IsTaxIncluded.GetShort().GetDbType();
                dr[ItemserviceFields.SalesTaxCodeRef_ListID] = itemservice.SalesTaxCodeRef_ListID;
                dr[ItemserviceFields.SalesTaxCodeRef_FullName] = itemservice.SalesTaxCodeRef_FullName;
                dr[ItemserviceFields.CustomField1] = itemservice.CustomField1;
                dr[ItemserviceFields.CustomField2] = itemservice.CustomField2;
                dr[ItemserviceFields.CustomField3] = itemservice.CustomField3;
                dr[ItemserviceFields.CustomField4] = itemservice.CustomField4;
                dr[ItemserviceFields.CustomField5] = itemservice.CustomField5;
                dr[ItemserviceFields.CustomField6] = itemservice.CustomField6;
                dr[ItemserviceFields.CustomField7] = itemservice.CustomField7;
                dr[ItemserviceFields.CustomField8] = itemservice.CustomField8;
                dr[ItemserviceFields.CustomField9] = itemservice.CustomField9;
                dr[ItemserviceFields.CustomField10] = itemservice.CustomField10;
                dr[ItemserviceFields.Status] = itemservice.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
