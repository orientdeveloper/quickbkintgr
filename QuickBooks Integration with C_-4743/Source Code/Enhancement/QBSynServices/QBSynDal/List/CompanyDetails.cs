﻿using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class CompanyDetails : AbstractDalBase
    {
         #region Constructors / Destructors
        public CompanyDetails(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public void Insert(Companydetail company)
        {
            try
            {
                BeginTransaction();
                tblCompanyDetailTableAdapter _tblCompanyDetailTableAdapter = new tblCompanyDetailTableAdapter();
                _tblCompanyDetailTableAdapter.Connection = Connection;
                _tblCompanyDetailTableAdapter.Transaction = Transaction;
                _tblCompanyDetailTableAdapter.Insert(company.UserName, company.LicenseKey,company.ConnectionString, company.CPUID,company.MACADDRESS);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion
    }
}
