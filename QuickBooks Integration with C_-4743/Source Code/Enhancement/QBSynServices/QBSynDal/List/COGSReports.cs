//
// Class	:	PayrollTransactionReport.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class COGSReports : AbstractDalBase
    {
        #region Variable
        DateTime? _lastmodifieddate;
        List<string> _transactiontype;
        #endregion

        #region Constructors / Destructors
        public COGSReports(string constr, DateTime? lastmodifieddate, List<string> transactiontype)
            : base(constr)
        {
            _lastmodifieddate = lastmodifieddate;
            _transactiontype = transactiontype;
        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                COGSReportTableAdapter _COGSReportTableAdapter = new COGSReportTableAdapter();
                _COGSReportTableAdapter.Connection = Connection;
                _COGSReportTableAdapter.Transaction = Transaction;

                if (synAction == SynAction.InitiateDelete || synAction == SynAction.DoneDelete)
                {
                    if (_transactiontype.Count == 2)
                    {
                        if (_lastmodifieddate.HasValue)
                        {

                            _COGSReportTableAdapter.DeleteByDateCogs(_lastmodifieddate.Value.Date.Year, _lastmodifieddate.Value.Date.Month, _lastmodifieddate.Value.Date.Day, _transactiontype[0], _transactiontype[1]);

                        }
                        else
                        {
                            _COGSReportTableAdapter.DeleteAllCogs(_transactiontype[0], _transactiontype[1]);
                        }
                    }
                    else if (_transactiontype.Count == 1)
                    {
                        if (_lastmodifieddate.HasValue)
                        {
                            _COGSReportTableAdapter.DeleteByDate(_lastmodifieddate.Value.Date.Year, _lastmodifieddate.Value.Date.Month, _lastmodifieddate.Value.Date.Day, _transactiontype[0]);
                        }
                        else
                        {
                            _COGSReportTableAdapter.DeleteAll(_transactiontype[0]);
                        }
                    }
                }

                QBSynDal.List.QBSynDal.COGSReportDataTable _COGSReportDataTable = new List.QBSynDal.COGSReportDataTable();
                foreach (COGSReport _COGSReport in this.Items)
                {
                    DataRow dr = _COGSReportDataTable.NewRow();
                    _COGSReportDataTable.Rows.Add(dr);
                    Bind(dr, _COGSReport);
                }
                _COGSReportTableAdapter.Update(_COGSReportDataTable);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Method (Private)
        private void Bind(DataRow dr, COGSReport _COGSReport)
        {
            try
            {
                dr[COGSReportFields.TxnNumber] = _COGSReport.TxnNumber.GetDbType();
                dr[COGSReportFields.TxnType] = _COGSReport.TxnType;
                dr[COGSReportFields.Date] = _COGSReport.Date.GetDbType();
                dr[COGSReportFields.RefNumber] = _COGSReport.RefNumber;
                dr[COGSReportFields.PONumber] = _COGSReport.PONumber;
                dr[COGSReportFields.Customer_ListID] = _COGSReport.Customer_ListID;
                dr[COGSReportFields.Name] = _COGSReport.Name;
                dr[COGSReportFields.SourceName] = _COGSReport.SourceName;
                dr[COGSReportFields.Memo] = _COGSReport.Memo;
                dr[COGSReportFields.ShipDate] = _COGSReport.ShipDate.GetDbType();
                dr[COGSReportFields.DeliveryDate] = _COGSReport.DeliveryDate.GetDbType();
                dr[COGSReportFields.FOB] = _COGSReport.FOB;
                dr[COGSReportFields.ShipMethod] = _COGSReport.ShipMethod;
                dr[COGSReportFields.Item_ListID] = _COGSReport.Item_ListID;
                dr[COGSReportFields.Item] = _COGSReport.Item;
                dr[COGSReportFields.Account_ListID] = _COGSReport.Account_ListID;
                dr[COGSReportFields.Account] = _COGSReport.Account;
                dr[COGSReportFields.Class_ListID] = _COGSReport.Class_ListID;
                dr[COGSReportFields.Class] = _COGSReport.Class;
                dr[COGSReportFields.SalesTaxCode] = _COGSReport.SalesTaxCode;
                dr[COGSReportFields.ClearedStatus] = _COGSReport.ClearedStatus;
                dr[COGSReportFields.SplitAccount_ListID] = _COGSReport.SplitAccount_ListID;
                dr[COGSReportFields.SplitAccount] = _COGSReport.SplitAccount;
                dr[COGSReportFields.Quantity] = _COGSReport.Quanity.GetDbType();
                dr[COGSReportFields.Debit] = _COGSReport.Debit.GetDbType();
                dr[COGSReportFields.Credit] = _COGSReport.Credit.GetDbType();
                dr[COGSReportFields.ModifiedTime] = _COGSReport.ModifiedTime.GetDbType();
                dr[COGSReportFields.AccountType] = _COGSReport.AccountType;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
