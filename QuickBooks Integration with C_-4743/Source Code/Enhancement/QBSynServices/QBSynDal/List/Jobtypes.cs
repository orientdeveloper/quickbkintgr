//
// Class	:	Jobtypes.cs
// Author	:  	Synapse Software © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class Jobtypes : AbstractDalBase
    {
        #region Constructors / Destructors
        public Jobtypes(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                jobtypeTableAdapter _jobtypeTableAdapter = new jobtypeTableAdapter();
                _jobtypeTableAdapter.Connection = Connection;
                _jobtypeTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Jobtype jobtype in this.Items)
                {
                    QBSynDal.List.QBSynDal.jobtypeDataTable jobtypeTable = _jobtypeTableAdapter.GetDataByListID(jobtype.ListID);
                    DataRow dr = null;

                    if (jobtypeTable.Rows.Count == 0)
                    {
                        dr = jobtypeTable.NewRow();
                        Bind(dr, jobtype);
                        jobtypeTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = jobtypeTable.Rows[0];
                        Bind(dr, jobtype);
                    }
                    _jobtypeTableAdapter.Update(jobtypeTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Jobtype jobtype)
        {
            try
            {
                dr[JobtypeFields.EditSequence] = jobtype.EditSequence;
                dr[JobtypeFields.FullName] = jobtype.FullName;
                dr[JobtypeFields.IsActive] = jobtype.IsActive.GetShort().GetDbType();
                dr[JobtypeFields.ListID] = jobtype.ListID;
                dr[JobtypeFields.Name] = jobtype.Name;
                dr[JobtypeFields.ParentRef_FullName] = jobtype.ParentRef_FullName;
                dr[JobtypeFields.ParentRef_ListID] = jobtype.ParentRef_ListID;
                dr[JobtypeFields.Status] = jobtype.Status;
                dr[JobtypeFields.Sublevel] = jobtype.Sublevel.GetDbType();
                dr[JobtypeFields.TimeCreated] = jobtype.TimeCreated;
                dr[JobtypeFields.TimeModified] = jobtype.TimeModified;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
