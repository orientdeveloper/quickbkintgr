//
// Class	:	Creditmemolinedetails.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List;
using QBSynDal.List.QBSynDalTableAdapters;
using QBSynDal.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace QBSynDal
{
    public class Creditmemolinedetails : AbstractDalBase
    {
        #region Constructors / Destructors
        public Creditmemolinedetails(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                creditmemolinedetailTableAdapter _creditmemolinedetailTableAdapter = new creditmemolinedetailTableAdapter();
                _creditmemolinedetailTableAdapter.Connection = Connection;
                _creditmemolinedetailTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                QBSynDal.List.QBSynDal.creditmemolinedetailDataTable creditmemolinedetailDataTable = new List.QBSynDal.creditmemolinedetailDataTable();
                foreach (Creditmemolinedetail creditmemolinedetail in this.Items)
                {
                    DataRow dr = creditmemolinedetailDataTable.NewRow();
                    creditmemolinedetailDataTable.Rows.Add(dr);
                    Bind(dr, creditmemolinedetail);
                }
                _creditmemolinedetailTableAdapter.Update(creditmemolinedetailDataTable);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Method (Private)
        private void Bind(DataRow dr, Creditmemolinedetail creditmemolinedetail)
        {
            dr[CreditmemolinedetailFields.TxnLineID] = creditmemolinedetail.TxnLineID;
            dr[CreditmemolinedetailFields.ItemRef_ListID] = creditmemolinedetail.ItemRef_ListID;
            dr[CreditmemolinedetailFields.ItemRef_FullName] = creditmemolinedetail.ItemRef_FullName;
            dr[CreditmemolinedetailFields.Desc] = creditmemolinedetail.Desc;
            dr[CreditmemolinedetailFields.Quantity] = creditmemolinedetail.Quantity;
            dr[CreditmemolinedetailFields.UnitOfMeasure] = creditmemolinedetail.UnitOfMeasure;
            dr[CreditmemolinedetailFields.OverrideUOMSetRef_ListID] = creditmemolinedetail.OverrideUOMSetRef_ListID;
            dr[CreditmemolinedetailFields.OverrideUOMSetRef_FullName] = creditmemolinedetail.OverrideUOMSetRef_FullName;
            dr[CreditmemolinedetailFields.Rate] = creditmemolinedetail.Rate;
            dr[CreditmemolinedetailFields.RatePercent] = creditmemolinedetail.RatePercent;
            dr[CreditmemolinedetailFields.ClassRef_ListID] = creditmemolinedetail.ClassRef_ListID;
            dr[CreditmemolinedetailFields.ClassRef_FullName] = creditmemolinedetail.ClassRef_FullName;
            dr[CreditmemolinedetailFields.Amount] = creditmemolinedetail.Amount.GetDbType();
            dr[CreditmemolinedetailFields.InventorySiteRef_ListID] = creditmemolinedetail.InventorySiteRef_ListID;
            dr[CreditmemolinedetailFields.InventorySiteRef_FullName] = creditmemolinedetail.InventorySiteRef_FullName;
            dr[CreditmemolinedetailFields.ServiceDate] = creditmemolinedetail.ServiceDate.GetDbType();
            dr[CreditmemolinedetailFields.SalesTaxCodeRef_ListID] = creditmemolinedetail.SalesTaxCodeRef_ListID;
            dr[CreditmemolinedetailFields.SalesTaxCodeRef_FullName] = creditmemolinedetail.SalesTaxCodeRef_FullName;
            dr[CreditmemolinedetailFields.Other1] = creditmemolinedetail.Other1;
            dr[CreditmemolinedetailFields.Other2] = creditmemolinedetail.Other2;
            dr[CreditmemolinedetailFields.CustomField1] = creditmemolinedetail.CustomField1;
            dr[CreditmemolinedetailFields.CustomField2] = creditmemolinedetail.CustomField2;
            dr[CreditmemolinedetailFields.CustomField3] = creditmemolinedetail.CustomField3;
            dr[CreditmemolinedetailFields.CustomField4] = creditmemolinedetail.CustomField4;
            dr[CreditmemolinedetailFields.CustomField5] = creditmemolinedetail.CustomField5;
            dr[CreditmemolinedetailFields.CustomField6] = creditmemolinedetail.CustomField6;
            dr[CreditmemolinedetailFields.CustomField7] = creditmemolinedetail.CustomField7;
            dr[CreditmemolinedetailFields.CustomField8] = creditmemolinedetail.CustomField8;
            dr[CreditmemolinedetailFields.CustomField9] = creditmemolinedetail.CustomField9;
            dr[CreditmemolinedetailFields.CustomField10] = creditmemolinedetail.CustomField10;
            dr[CreditmemolinedetailFields.Idkey] = creditmemolinedetail.Idkey;
            dr[CreditmemolinedetailFields.GroupIDKEY] = creditmemolinedetail.GroupIDKEY;
        }
        #endregion

    }
}
