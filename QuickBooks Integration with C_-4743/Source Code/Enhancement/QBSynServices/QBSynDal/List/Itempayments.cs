//
// Class	:	Itempayments.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:51 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class Itempayments : AbstractDalBase
    {

        #region Constructors / Destructors
        public Itempayments(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                itempaymentTableAdapter _itempaymentTableAdapter = new itempaymentTableAdapter();
                _itempaymentTableAdapter.Connection = Connection;
                _itempaymentTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Itempayment itempayment in this.Items)
                {
                    QBSynDal.List.QBSynDal.itempaymentDataTable itempaymentTable = _itempaymentTableAdapter.GetDataByListID(itempayment.ListID);
                    DataRow dr = null;

                    if (itempaymentTable.Rows.Count == 0)
                    {
                        dr = itempaymentTable.NewRow();
                        Bind(dr, itempayment);
                        itempaymentTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = itempaymentTable.Rows[0];
                        Bind(dr, itempayment);
                    }
                    _itempaymentTableAdapter.Update(itempaymentTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Itempayment itempayment)
        {
            try
            {
                dr[ItempaymentFields.ListID] = itempayment.ListID;
                dr[ItempaymentFields.TimeCreated] = itempayment.TimeCreated;
                dr[ItempaymentFields.TimeModified] = itempayment.TimeModified;
                dr[ItempaymentFields.EditSequence] = itempayment.EditSequence;
                dr[ItempaymentFields.Name] = itempayment.Name;
                dr[ItempaymentFields.IsActive] = itempayment.IsActive.GetShort().GetDbType();
                dr[ItempaymentFields.DepositToAccountRef_ListID] = itempayment.DepositToAccountRef_ListID;
                dr[ItempaymentFields.DepositToAccountRef_FullName] = itempayment.DepositToAccountRef_FullName;
                dr[ItempaymentFields.PaymentMethodRef_ListID] = itempayment.PaymentMethodRef_ListID;
                dr[ItempaymentFields.PaymentMethodRef_FullName] = itempayment.PaymentMethodRef_FullName;
                dr[ItempaymentFields.CustomField1] = itempayment.CustomField1;
                dr[ItempaymentFields.CustomField2] = itempayment.CustomField2;
                dr[ItempaymentFields.CustomField3] = itempayment.CustomField3;
                dr[ItempaymentFields.CustomField4] = itempayment.CustomField4;
                dr[ItempaymentFields.CustomField5] = itempayment.CustomField5;
                dr[ItempaymentFields.CustomField6] = itempayment.CustomField6;
                dr[ItempaymentFields.CustomField7] = itempayment.CustomField7;
                dr[ItempaymentFields.CustomField8] = itempayment.CustomField8;
                dr[ItempaymentFields.CustomField9] = itempayment.CustomField9;
                dr[ItempaymentFields.CustomField10] = itempayment.CustomField10;
                dr[ItempaymentFields.Status] = itempayment.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
