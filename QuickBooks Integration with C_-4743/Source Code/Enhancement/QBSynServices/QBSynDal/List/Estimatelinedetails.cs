//
// Class	:	Estimatelinedetails.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List;
using QBSynDal.List.QBSynDalTableAdapters;
using QBSynDal.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace QBSynDal
{
    public class Estimatelinedetails : AbstractDalBase
    {
        #region Constructors / Destructors
        public Estimatelinedetails(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                estimatelinedetailTableAdapter _estimatelinedetailTableAdapter = new estimatelinedetailTableAdapter();
                _estimatelinedetailTableAdapter.Connection = Connection;
                _estimatelinedetailTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                   
                }

                QBSynDal.List.QBSynDal.estimatelinedetailDataTable estimatelinedetailDataTable = new List.QBSynDal.estimatelinedetailDataTable();
                foreach (Estimatelinedetail estimatelinedetail in this.Items)
                {
                    DataRow dr = estimatelinedetailDataTable.NewRow();
                    estimatelinedetailDataTable.Rows.Add(dr);
                    Bind(dr, estimatelinedetail);
                }
                _estimatelinedetailTableAdapter.Update(estimatelinedetailDataTable);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Method (Private)
        private void Bind(DataRow dr, Estimatelinedetail estimatelinedetail)
        {
            dr[EstimatelinedetailFields.TxnLineID] = estimatelinedetail.TxnLineID;
            dr[EstimatelinedetailFields.ItemRef_ListID] = estimatelinedetail.ItemRef_ListID;
            dr[EstimatelinedetailFields.ItemRef_FullName] = estimatelinedetail.ItemRef_FullName;
            dr[EstimatelinedetailFields.Desc] = estimatelinedetail.Desc;
            dr[EstimatelinedetailFields.Quantity] = estimatelinedetail.Quantity;
            dr[EstimatelinedetailFields.UnitOfMeasure] = estimatelinedetail.UnitOfMeasure;
            dr[EstimatelinedetailFields.OverrideUOMSetRef_ListID] = estimatelinedetail.OverrideUOMSetRef_ListID;
            dr[EstimatelinedetailFields.OverrideUOMSetRef_FullName] = estimatelinedetail.OverrideUOMSetRef_FullName;
            dr[EstimatelinedetailFields.Rate] = estimatelinedetail.Rate;
            dr[EstimatelinedetailFields.RatePercent] = estimatelinedetail.RatePercent;
            dr[EstimatelinedetailFields.ClassRef_ListID] = estimatelinedetail.ClassRef_ListID;
            dr[EstimatelinedetailFields.ClassRef_FullName] = estimatelinedetail.ClassRef_FullName;
            dr[EstimatelinedetailFields.Amount] = estimatelinedetail.Amount.GetDbType();
            dr[EstimatelinedetailFields.InventorySiteRef_ListID] = estimatelinedetail.InventorySiteRef_ListID;
            dr[EstimatelinedetailFields.InventorySiteRef_FullName] = estimatelinedetail.InventorySiteRef_FullName;
            dr[EstimatelinedetailFields.SalesTaxCodeRef_ListID] = estimatelinedetail.SalesTaxCodeRef_ListID;
            dr[EstimatelinedetailFields.SalesTaxCodeRef_FullName] = estimatelinedetail.SalesTaxCodeRef_FullName;
            dr[EstimatelinedetailFields.MarkupRate] = estimatelinedetail.MarkupRate;
            dr[EstimatelinedetailFields.MarkupRatePercent] = estimatelinedetail.MarkupRatePercent;
            dr[EstimatelinedetailFields.Other1] = estimatelinedetail.Other1;
            dr[EstimatelinedetailFields.Other2] = estimatelinedetail.Other2;
            dr[EstimatelinedetailFields.CustomField1] = estimatelinedetail.CustomField1;
            dr[EstimatelinedetailFields.CustomField2] = estimatelinedetail.CustomField2;
            dr[EstimatelinedetailFields.CustomField3] = estimatelinedetail.CustomField3;
            dr[EstimatelinedetailFields.CustomField4] = estimatelinedetail.CustomField4;
            dr[EstimatelinedetailFields.CustomField5] = estimatelinedetail.CustomField5;
            dr[EstimatelinedetailFields.CustomField6] = estimatelinedetail.CustomField6;
            dr[EstimatelinedetailFields.CustomField7] = estimatelinedetail.CustomField7;
            dr[EstimatelinedetailFields.CustomField8] = estimatelinedetail.CustomField8;
            dr[EstimatelinedetailFields.CustomField9] = estimatelinedetail.CustomField9;
            dr[EstimatelinedetailFields.CustomField10] = estimatelinedetail.CustomField10;
            dr[EstimatelinedetailFields.Idkey] = estimatelinedetail.Idkey;
            dr[EstimatelinedetailFields.GroupIDKEY] = estimatelinedetail.GroupIDKEY;
        }
        #endregion
    }
}
