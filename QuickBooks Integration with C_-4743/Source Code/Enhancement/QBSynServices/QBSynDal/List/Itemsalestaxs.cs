//
// Class	:	Itemsalestaxs.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;
namespace QBSynDal
{
    public class Itemsalestaxs : AbstractDalBase
    {

        #region Constructors / Destructors
        public Itemsalestaxs(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                itemsalestaxTableAdapter _itemsalestaxTableAdapter = new itemsalestaxTableAdapter();
                _itemsalestaxTableAdapter.Connection = Connection;
                _itemsalestaxTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Itemsalestax itemsalestax in this.Items)
                {
                    QBSynDal.List.QBSynDal.itemsalestaxDataTable itemsalestaxTable = _itemsalestaxTableAdapter.GetDataByListID(itemsalestax.ListID);
                    DataRow dr = null;

                    if (itemsalestaxTable.Rows.Count == 0)
                    {
                        dr = itemsalestaxTable.NewRow();
                        Bind(dr, itemsalestax);
                        itemsalestaxTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = itemsalestaxTable.Rows[0];
                        Bind(dr, itemsalestax);
                    }
                    _itemsalestaxTableAdapter.Update(itemsalestaxTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Itemsalestax itemsalestax)
        {
            try
            {
                dr[ItemsalestaxFields.ListID] = itemsalestax.ListID;
                dr[ItemsalestaxFields.TimeCreated] = itemsalestax.TimeCreated;
                dr[ItemsalestaxFields.TimeModified] = itemsalestax.TimeModified;
                dr[ItemsalestaxFields.EditSequence] = itemsalestax.EditSequence;
                dr[ItemsalestaxFields.Name] = itemsalestax.Name;
                dr[ItemsalestaxFields.IsActive] = itemsalestax.IsActive.GetShort().GetDbType();
                dr[ItemsalestaxFields.ItemDesc] = itemsalestax.ItemDesc;
                dr[ItemsalestaxFields.IsUsedOnPurchaseTransaction] = itemsalestax.IsUsedOnPurchaseTransaction.GetShort().GetDbType();
                dr[ItemsalestaxFields.TaxRate] = itemsalestax.TaxRate;
                dr[ItemsalestaxFields.TaxVendorRef_ListID] = itemsalestax.TaxVendorRef_ListID;
                dr[ItemsalestaxFields.TaxVendorRef_FullName] = itemsalestax.TaxVendorRef_FullName;
                dr[ItemsalestaxFields.CustomField1] = itemsalestax.CustomField1;
                dr[ItemsalestaxFields.CustomField2] = itemsalestax.CustomField2;
                dr[ItemsalestaxFields.CustomField3] = itemsalestax.CustomField3;
                dr[ItemsalestaxFields.CustomField4] = itemsalestax.CustomField4;
                dr[ItemsalestaxFields.CustomField5] = itemsalestax.CustomField5;
                dr[ItemsalestaxFields.CustomField6] = itemsalestax.CustomField6;
                dr[ItemsalestaxFields.CustomField7] = itemsalestax.CustomField7;
                dr[ItemsalestaxFields.CustomField8] = itemsalestax.CustomField8;
                dr[ItemsalestaxFields.CustomField9] = itemsalestax.CustomField9;
                dr[ItemsalestaxFields.CustomField10] = itemsalestax.CustomField10;
                dr[ItemsalestaxFields.Status] = itemsalestax.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
