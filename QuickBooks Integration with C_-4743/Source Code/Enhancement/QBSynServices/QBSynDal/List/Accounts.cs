//
// Class	:	Accounts.cs
// Author	:  	Synapse Software © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class Accounts : AbstractDalBase
    {
        #region Constructors / Destructors
        public Accounts(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                accountTableAdapter _accountTableAdapter = new accountTableAdapter();
                _accountTableAdapter.Connection = Connection;
                _accountTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Account account in this.Items)
                {
                    QBSynDal.List.QBSynDal.accountDataTable accountTable = _accountTableAdapter.GetDataByListID(account.ListID);
                    DataRow dr = null;

                    if (accountTable.Rows.Count == 0)
                    {
                        dr = accountTable.NewRow();
                        Bind(dr, account);
                        accountTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = accountTable.Rows[0];
                        Bind(dr, account);
                    }
                    _accountTableAdapter.Update(accountTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Account account)
        {
            try
            {
                dr[AccountFields.AccountNumber] = account.AccountNumber;
                dr[AccountFields.AccountType] = account.AccountType;
                dr[AccountFields.Balance] = account.Balance.GetDbType();
                dr[AccountFields.BankNumber] = account.BankNumber;
                dr[AccountFields.CashFlowClassification] = account.CashFlowClassification;
                dr[AccountFields.CurrencyRef_FullName] = account.CurrencyRef_FullName;
                dr[AccountFields.CurrencyRef_ListID] = account.CurrencyRef_ListID;
                dr[AccountFields.CustomField1] = account.CustomField1;
                dr[AccountFields.CustomField10] = account.CustomField10;
                dr[AccountFields.CustomField2] = account.CustomField2;
                dr[AccountFields.CustomField3] = account.CustomField3;
                dr[AccountFields.CustomField4] = account.CustomField4;
                dr[AccountFields.CustomField5] = account.CustomField5;
                dr[AccountFields.CustomField6] = account.CustomField6;
                dr[AccountFields.CustomField7] = account.CustomField7;
                dr[AccountFields.CustomField8] = account.CustomField8;
                dr[AccountFields.CustomField9] = account.CustomField9;
                dr[AccountFields.Desc] = account.Desc;
                dr[AccountFields.DetailAccountType] = account.DetailAccountType;
                dr[AccountFields.EditSequence] = account.EditSequence;
                dr[AccountFields.FullName] = account.FullName;
                dr[AccountFields.IsActive] = account.IsActive.GetShort().GetDbType();
                dr[AccountFields.IsTaxAccount] = account.IsTaxAccount.GetShort().GetDbType();
                dr[AccountFields.LastCheckNumber] = account.LastCheckNumber;
                dr[AccountFields.ListID] = account.ListID;
                dr[AccountFields.Name] = account.Name;
                dr[AccountFields.ParentRef_FullName] = account.ParentRef_FullName;
                dr[AccountFields.ParentRef_ListID] = account.ParentRef_ListID;
                dr[AccountFields.SalesTaxCodeRef_FullName] = account.SalesTaxCodeRef_FullName;
                dr[AccountFields.SalesTaxCodeRef_ListID] = account.SalesTaxCodeRef_ListID;
                dr[AccountFields.SpecialAccountType] = account.SpecialAccountType;
                dr[AccountFields.Status] = account.Status;
                dr[AccountFields.Sublevel] = account.Sublevel.GetDbType();
                dr[AccountFields.TaxLineID] = account.TaxLineID.GetDbType();
                dr[AccountFields.TaxLineInfo] = account.TaxLineInfo;
                dr[AccountFields.TimeCreated] = account.TimeCreated;
                dr[AccountFields.TimeModified] = account.TimeModified;
                dr[AccountFields.TotalBalance] = account.TotalBalance.GetDbType();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
