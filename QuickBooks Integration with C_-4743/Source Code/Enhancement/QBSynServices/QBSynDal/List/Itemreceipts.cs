//
// Class	:	Itemreceipts.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:44 AM
//
using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;
namespace QBSynDal
{
    public class Itemreceipts : AbstractDalBase
    {

        #region Constructors / Destructors
        public Itemreceipts(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                itemreceiptTableAdapter _itemreceiptTableAdapter = new itemreceiptTableAdapter();
                _itemreceiptTableAdapter.Connection = Connection;
                _itemreceiptTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Itemreceipt itemreceipt in this.Items)
                {
                    QBSynDal.List.QBSynDal.itemreceiptDataTable itemreceiptTable = _itemreceiptTableAdapter.GetDataByTxnID(itemreceipt.TxnID);
                    DataRow dr = null;

                    if (itemreceiptTable.Rows.Count == 0)
                    {
                        dr = itemreceiptTable.NewRow();
                        Bind(dr, itemreceipt);
                        itemreceiptTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = itemreceiptTable.Rows[0];
                        Bind(dr, itemreceipt);
                    }
                    _itemreceiptTableAdapter.Update(itemreceiptTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Itemreceipt itemreceipt)
        {
            try
            {
                dr[ItemreceiptFields.TxnID] = itemreceipt.TxnID;
                dr[ItemreceiptFields.TimeCreated] = itemreceipt.TimeCreated;
                dr[ItemreceiptFields.TimeModified] = itemreceipt.TimeModified;
                dr[ItemreceiptFields.EditSequence] = itemreceipt.EditSequence;
                dr[ItemreceiptFields.TxnNumber] = itemreceipt.TxnNumber.GetDbType();
                dr[ItemreceiptFields.VendorRef_ListID] = itemreceipt.VendorRef_ListID;
                dr[ItemreceiptFields.VendorRef_FullName] = itemreceipt.VendorRef_FullName;
                dr[ItemreceiptFields.APAccountRef_ListID] = itemreceipt.APAccountRef_ListID;
                dr[ItemreceiptFields.APAccountRef_FullName] = itemreceipt.APAccountRef_FullName;
                dr[ItemreceiptFields.TxnDate] = itemreceipt.TxnDate.GetDbType();
                dr[ItemreceiptFields.TotalAmount] = itemreceipt.TotalAmount.GetDbType();
                dr[ItemreceiptFields.CurrencyRef_ListID] = itemreceipt.CurrencyRef_ListID;
                dr[ItemreceiptFields.CurrencyRef_FullName] = itemreceipt.CurrencyRef_FullName;
                dr[ItemreceiptFields.ExchangeRate] = itemreceipt.ExchangeRate.GetDbType();
                dr[ItemreceiptFields.TotalAmountInHomeCurrency] = itemreceipt.TotalAmountInHomeCurrency.GetDbType();
                dr[ItemreceiptFields.RefNumber] = itemreceipt.RefNumber;
                dr[ItemreceiptFields.Memo] = itemreceipt.Memo;
                dr[ItemreceiptFields.LinkedTxn] = itemreceipt.LinkedTxn;
                dr[ItemreceiptFields.CustomField1] = itemreceipt.CustomField1;
                dr[ItemreceiptFields.CustomField2] = itemreceipt.CustomField2;
                dr[ItemreceiptFields.CustomField3] = itemreceipt.CustomField3;
                dr[ItemreceiptFields.CustomField4] = itemreceipt.CustomField4;
                dr[ItemreceiptFields.CustomField5] = itemreceipt.CustomField5;
                dr[ItemreceiptFields.CustomField6] = itemreceipt.CustomField6;
                dr[ItemreceiptFields.CustomField7] = itemreceipt.CustomField7;
                dr[ItemreceiptFields.CustomField8] = itemreceipt.CustomField8;
                dr[ItemreceiptFields.CustomField9] = itemreceipt.CustomField9;
                dr[ItemreceiptFields.CustomField10] = itemreceipt.CustomField10;
                dr[ItemreceiptFields.Status] = itemreceipt.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
