//
// Class	:	Accounts.cs
// Author	:  	Synapse Software © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class Terms : AbstractDalBase
    {
        #region Constructors / Destructors
        public Terms(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                termsTableAdapter _termsTableAdapter = new termsTableAdapter();
                _termsTableAdapter.Connection = Connection;
                _termsTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {

                }

                foreach (Term term in this.Items)
                {
                    QBSynDal.List.QBSynDal.termsDataTable termTable = _termsTableAdapter.GetDataByListID(term.ListID);
                    DataRow dr = null;

                    if (termTable.Rows.Count == 0)
                    {
                        dr = termTable.NewRow();
                        Bind(dr, term);
                        termTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = termTable.Rows[0];
                        Bind(dr, term);
                    }
                    _termsTableAdapter.Update(termTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Term term)
        {
            try
            {
                dr[TermFields.ListID] = term.ListID;
                dr[TermFields.FullName] = term.FullName;
                dr[TermFields.TableName] = term.TableName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
