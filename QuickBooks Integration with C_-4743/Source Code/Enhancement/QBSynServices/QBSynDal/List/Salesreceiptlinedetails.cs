//
// Class	:	Salesreceiptlinedetails.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List;
using QBSynDal.List.QBSynDalTableAdapters;
using QBSynDal.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Linq;
using System.Text;

namespace QBSynDal
{
    public class Salesreceiptlinedetails : AbstractDalBase
    {
        #region Constructors / Destructors
        public Salesreceiptlinedetails(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                salesreceiptlinedetailTableAdapter _salesreceiptlinedetailTableAdapter = new salesreceiptlinedetailTableAdapter();
                _salesreceiptlinedetailTableAdapter.Connection = Connection;
                _salesreceiptlinedetailTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                QBSynDal.List.QBSynDal.salesreceiptlinedetailDataTable salesreceiptlinedetailDataTable = new List.QBSynDal.salesreceiptlinedetailDataTable();
                foreach (Salesreceiptlinedetail salesreceiptlinedetail in this.Items)
                {
                    DataRow dr = salesreceiptlinedetailDataTable.NewRow();
                    salesreceiptlinedetailDataTable.Rows.Add(dr);
                    Bind(dr, salesreceiptlinedetail);
                }
                _salesreceiptlinedetailTableAdapter.Update(salesreceiptlinedetailDataTable);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Salesreceiptlinedetail salesreceiptlinedetail)
        {
            try
            {
                dr[SalesreceiptlinedetailFields.TxnLineID] = salesreceiptlinedetail.TxnLineID;
                dr[SalesreceiptlinedetailFields.ItemRef_ListID] = salesreceiptlinedetail.ItemRef_ListID;
                dr[SalesreceiptlinedetailFields.ItemRef_FullName] = salesreceiptlinedetail.ItemRef_FullName;
                dr[SalesreceiptlinedetailFields.Desc] = salesreceiptlinedetail.Desc;
                dr[SalesreceiptlinedetailFields.Quantity] = salesreceiptlinedetail.Quantity;
                dr[SalesreceiptlinedetailFields.UnitOfMeasure] = salesreceiptlinedetail.UnitOfMeasure;
                dr[SalesreceiptlinedetailFields.OverrideUOMSetRef_ListID] = salesreceiptlinedetail.OverrideUOMSetRef_ListID;
                dr[SalesreceiptlinedetailFields.OverrideUOMSetRef_FullName] = salesreceiptlinedetail.OverrideUOMSetRef_FullName;
                dr[SalesreceiptlinedetailFields.Rate] = salesreceiptlinedetail.Rate;
                dr[SalesreceiptlinedetailFields.RatePercent] = salesreceiptlinedetail.RatePercent;
                dr[SalesreceiptlinedetailFields.ClassRef_ListID] = salesreceiptlinedetail.ClassRef_ListID;
                dr[SalesreceiptlinedetailFields.ClassRef_FullName] = salesreceiptlinedetail.ClassRef_FullName;
                dr[SalesreceiptlinedetailFields.Amount] = salesreceiptlinedetail.Amount.GetDbType();
                dr[SalesreceiptlinedetailFields.InventorySiteRef_ListID] = salesreceiptlinedetail.InventorySiteRef_ListID;
                dr[SalesreceiptlinedetailFields.InventorySiteRef_FullName] = salesreceiptlinedetail.InventorySiteRef_FullName;
                dr[SalesreceiptlinedetailFields.ServiceDate] = salesreceiptlinedetail.ServiceDate.GetDbType();
                dr[SalesreceiptlinedetailFields.SalesTaxCodeRef_ListID] = salesreceiptlinedetail.SalesTaxCodeRef_ListID;
                dr[SalesreceiptlinedetailFields.SalesTaxCodeRef_FullName] = salesreceiptlinedetail.SalesTaxCodeRef_FullName;
                dr[SalesreceiptlinedetailFields.Other1] = salesreceiptlinedetail.Other1;
                dr[SalesreceiptlinedetailFields.Other2] = salesreceiptlinedetail.Other2;
                dr[SalesreceiptlinedetailFields.CustomField1] = salesreceiptlinedetail.CustomField1;
                dr[SalesreceiptlinedetailFields.CustomField2] = salesreceiptlinedetail.CustomField2;
                dr[SalesreceiptlinedetailFields.CustomField3] = salesreceiptlinedetail.CustomField3;
                dr[SalesreceiptlinedetailFields.CustomField4] = salesreceiptlinedetail.CustomField4;
                dr[SalesreceiptlinedetailFields.CustomField5] = salesreceiptlinedetail.CustomField5;
                dr[SalesreceiptlinedetailFields.CustomField6] = salesreceiptlinedetail.CustomField6;
                dr[SalesreceiptlinedetailFields.CustomField7] = salesreceiptlinedetail.CustomField7;
                dr[SalesreceiptlinedetailFields.CustomField8] = salesreceiptlinedetail.CustomField8;
                dr[SalesreceiptlinedetailFields.CustomField9] = salesreceiptlinedetail.CustomField9;
                dr[SalesreceiptlinedetailFields.CustomField10] = salesreceiptlinedetail.CustomField10;
                dr[SalesreceiptlinedetailFields.Idkey] = salesreceiptlinedetail.Idkey;
                dr[SalesreceiptlinedetailFields.GroupIDKEY] = salesreceiptlinedetail.GroupIDKEY;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
