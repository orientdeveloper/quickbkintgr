//
// Class	:	Txnexpenselinedetails.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List;
using QBSynDal.List.QBSynDalTableAdapters;
using QBSynDal.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Linq;
using System.Text;

namespace QBSynDal
{
    public class Txnexpenselinedetails : AbstractDalBase
    {
        #region Constructors / Destructors
        public Txnexpenselinedetails(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                txnexpenselinedetailTableAdapter _txnexpenselinedetailTableAdapter = new txnexpenselinedetailTableAdapter();
                _txnexpenselinedetailTableAdapter.Connection = Connection;
                _txnexpenselinedetailTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                QBSynDal.List.QBSynDal.txnexpenselinedetailDataTable txnexpenselinedetailDataTable = new List.QBSynDal.txnexpenselinedetailDataTable();
                foreach (Txnexpenselinedetail txnexpenselinedetail in this.Items)
                {
                    DataRow dr = txnexpenselinedetailDataTable.NewRow();
                    txnexpenselinedetailDataTable.Rows.Add(dr);
                    Bind(dr, txnexpenselinedetail);
                }
                _txnexpenselinedetailTableAdapter.Update(txnexpenselinedetailDataTable);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Txnexpenselinedetail txnexpenselinedetail)
        {
            try
            {
                dr[TxnexpenselinedetailFields.TxnLineID] = txnexpenselinedetail.TxnLineID;
                dr[TxnexpenselinedetailFields.AccountRef_ListID] = txnexpenselinedetail.AccountRef_ListID;
                dr[TxnexpenselinedetailFields.AccountRef_FullName] = txnexpenselinedetail.AccountRef_FullName;
                dr[TxnexpenselinedetailFields.Amount] = txnexpenselinedetail.Amount.GetDbType();
                dr[TxnexpenselinedetailFields.Memo] = txnexpenselinedetail.Memo;
                dr[TxnexpenselinedetailFields.CustomerRef_ListID] = txnexpenselinedetail.CustomerRef_ListID;
                dr[TxnexpenselinedetailFields.CustomerRef_FullName] = txnexpenselinedetail.CustomerRef_FullName;
                dr[TxnexpenselinedetailFields.ClassRef_ListID] = txnexpenselinedetail.ClassRef_ListID;
                dr[TxnexpenselinedetailFields.ClassRef_FullName] = txnexpenselinedetail.ClassRef_FullName;
                dr[TxnexpenselinedetailFields.SalesTaxCodeRef_ListID] = txnexpenselinedetail.SalesTaxCodeRef_ListID;
                dr[TxnexpenselinedetailFields.SalesTaxCodeRef_FullName] = txnexpenselinedetail.SalesTaxCodeRef_FullName;
                dr[TxnexpenselinedetailFields.BillableStatus] = txnexpenselinedetail.BillableStatus;
                dr[TxnexpenselinedetailFields.TxnLineID] = txnexpenselinedetail.TxnLineID;
                dr[TxnexpenselinedetailFields.CustomField1] = txnexpenselinedetail.CustomField1;
                dr[TxnexpenselinedetailFields.CustomField2] = txnexpenselinedetail.CustomField2;
                dr[TxnexpenselinedetailFields.CustomField3] = txnexpenselinedetail.CustomField3;
                dr[TxnexpenselinedetailFields.CustomField4] = txnexpenselinedetail.CustomField4;
                dr[TxnexpenselinedetailFields.CustomField5] = txnexpenselinedetail.CustomField5;
                dr[TxnexpenselinedetailFields.CustomField6] = txnexpenselinedetail.CustomField6;
                dr[TxnexpenselinedetailFields.CustomField7] = txnexpenselinedetail.CustomField7;
                dr[TxnexpenselinedetailFields.CustomField8] = txnexpenselinedetail.CustomField8;
                dr[TxnexpenselinedetailFields.CustomField9] = txnexpenselinedetail.CustomField9;
                dr[TxnexpenselinedetailFields.CustomField10] = txnexpenselinedetail.CustomField10;
                dr[TxnexpenselinedetailFields.Idkey] = txnexpenselinedetail.Idkey;
                dr[TxnexpenselinedetailFields.GroupIDKEY] = txnexpenselinedetail.GroupIDKEY;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
