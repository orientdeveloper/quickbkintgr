//
// Class	:	Billpaymentcreditcards.cs
// Author	:  	Synapse Software © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class Billpaymentcreditcards : AbstractDalBase
    {
        #region Constructors / Destructors
        public Billpaymentcreditcards(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                billpaymentcreditcardTableAdapter _billpaymentcreditcardTableAdapter = new billpaymentcreditcardTableAdapter();
                _billpaymentcreditcardTableAdapter.Connection = Connection;
                _billpaymentcreditcardTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Billpaymentcreditcard billpaymentcreditcard in this.Items)
                {
                    QBSynDal.List.QBSynDal.billpaymentcreditcardDataTable billpaymentcreditcardTable = _billpaymentcreditcardTableAdapter.GetDataByTxnID(billpaymentcreditcard.TxnID);
                    DataRow dr = null;

                    if (billpaymentcreditcardTable.Rows.Count == 0)
                    {
                        dr = billpaymentcreditcardTable.NewRow();
                        Bind(dr, billpaymentcreditcard);
                        billpaymentcreditcardTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = billpaymentcreditcardTable.Rows[0];
                        Bind(dr, billpaymentcreditcard);
                    }
                    _billpaymentcreditcardTableAdapter.Update(billpaymentcreditcardTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Billpaymentcreditcard billpaymentcreditcard)
        {
            try
            {
                dr[BillpaymentcreditcardFields.TxnID] = billpaymentcreditcard.TxnID;
                dr[BillpaymentcreditcardFields.TimeCreated] = billpaymentcreditcard.TimeCreated;
                dr[BillpaymentcreditcardFields.TimeModified] = billpaymentcreditcard.TimeModified;
                dr[BillpaymentcreditcardFields.EditSequence] = billpaymentcreditcard.EditSequence;
                dr[BillpaymentcreditcardFields.TxnNumber] = billpaymentcreditcard.TxnNumber.GetDbType();
                dr[BillpaymentcreditcardFields.PayeeEntityRef_ListID] = billpaymentcreditcard.PayeeEntityRef_ListID;
                dr[BillpaymentcreditcardFields.PayeeEntityRef_FullName] = billpaymentcreditcard.PayeeEntityRef_FullName;
                dr[BillpaymentcreditcardFields.APAccountRef_ListID] = billpaymentcreditcard.APAccountRef_ListID;
                dr[BillpaymentcreditcardFields.APAccountRef_FullName] = billpaymentcreditcard.APAccountRef_FullName;
                dr[BillpaymentcreditcardFields.CreditCardAccountRef_ListID] = billpaymentcreditcard.CreditCardAccountRef_ListID;
                dr[BillpaymentcreditcardFields.CreditCardAccountRef_FullName] = billpaymentcreditcard.CreditCardAccountRef_FullName;
                dr[BillpaymentcreditcardFields.Amount] = billpaymentcreditcard.Amount.GetDbType();
                dr[BillpaymentcreditcardFields.CurrencyRef_ListID] = billpaymentcreditcard.CurrencyRef_ListID;
                dr[BillpaymentcreditcardFields.CurrencyRef_FullName] = billpaymentcreditcard.CurrencyRef_FullName;
                dr[BillpaymentcreditcardFields.ExchangeRate] = billpaymentcreditcard.ExchangeRate.GetDbType();
                dr[BillpaymentcreditcardFields.AmountInHomeCurrency] = billpaymentcreditcard.AmountInHomeCurrency.GetDbType();
                dr[BillpaymentcreditcardFields.RefNumber] = billpaymentcreditcard.RefNumber;
                dr[BillpaymentcreditcardFields.Memo] = billpaymentcreditcard.Memo;
                dr[BillpaymentcreditcardFields.CustomField1] = billpaymentcreditcard.CustomField1;
                dr[BillpaymentcreditcardFields.CustomField2] = billpaymentcreditcard.CustomField2;
                dr[BillpaymentcreditcardFields.CustomField3] = billpaymentcreditcard.CustomField3;
                dr[BillpaymentcreditcardFields.CustomField4] = billpaymentcreditcard.CustomField4;
                dr[BillpaymentcreditcardFields.CustomField5] = billpaymentcreditcard.CustomField5;
                dr[BillpaymentcreditcardFields.CustomField6] = billpaymentcreditcard.CustomField6;
                dr[BillpaymentcreditcardFields.CustomField7] = billpaymentcreditcard.CustomField7;
                dr[BillpaymentcreditcardFields.CustomField8] = billpaymentcreditcard.CustomField8;
                dr[BillpaymentcreditcardFields.CustomField9] = billpaymentcreditcard.CustomField9;
                dr[BillpaymentcreditcardFields.CustomField10] = billpaymentcreditcard.CustomField10;
                dr[BillpaymentcreditcardFields.Status] = billpaymentcreditcard.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
