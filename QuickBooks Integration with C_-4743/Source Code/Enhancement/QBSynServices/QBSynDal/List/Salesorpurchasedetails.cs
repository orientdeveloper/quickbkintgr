//
// Class	:	Salesorpurchasedetails.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List;
using QBSynDal.List.QBSynDalTableAdapters;
using QBSynDal.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Linq;
using System.Text;

namespace QBSynDal
{
    public class Salesorpurchasedetails : AbstractDalBase
    {
        #region Constructors / Destructors
        public Salesorpurchasedetails(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                salesorpurchasedetailTableAdapter _salesorpurchasedetailTableAdapter = new salesorpurchasedetailTableAdapter();
                _salesorpurchasedetailTableAdapter.Connection = Connection;
                _salesorpurchasedetailTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                   
                }

                QBSynDal.List.QBSynDal.salesorpurchasedetailDataTable salesorpurchasedetailDataTable = new List.QBSynDal.salesorpurchasedetailDataTable();
                foreach (Salesorpurchasedetail salesorpurchasedetail in this.Items)
                {
                    DataRow dr = salesorpurchasedetailDataTable.NewRow();
                    salesorpurchasedetailDataTable.Rows.Add(dr);
                    Bind(dr, salesorpurchasedetail);
                }
                _salesorpurchasedetailTableAdapter.Update(salesorpurchasedetailDataTable);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Salesorpurchasedetail salesorpurchasedetail)
        {
            try
            {
                dr[SalesorpurchasedetailFields.Desc] = salesorpurchasedetail.Desc;
                dr[SalesorpurchasedetailFields.Price] = salesorpurchasedetail.Price;
                dr[SalesorpurchasedetailFields.PricePercent] = salesorpurchasedetail.PricePercent;
                dr[SalesorpurchasedetailFields.AccountRef_ListID] = salesorpurchasedetail.AccountRef_ListID;
                dr[SalesorpurchasedetailFields.AccountRef_FullName] = salesorpurchasedetail.AccountRef_FullName;

                dr[SalesorpurchasedetailFields.CustomField1] = salesorpurchasedetail.CustomField1;
                dr[SalesorpurchasedetailFields.CustomField2] = salesorpurchasedetail.CustomField2;
                dr[SalesorpurchasedetailFields.CustomField3] = salesorpurchasedetail.CustomField3;
                dr[SalesorpurchasedetailFields.CustomField4] = salesorpurchasedetail.CustomField4;
                dr[SalesorpurchasedetailFields.CustomField5] = salesorpurchasedetail.CustomField5;
                dr[SalesorpurchasedetailFields.CustomField6] = salesorpurchasedetail.CustomField6;
                dr[SalesorpurchasedetailFields.CustomField7] = salesorpurchasedetail.CustomField7;
                dr[SalesorpurchasedetailFields.CustomField8] = salesorpurchasedetail.CustomField8;
                dr[SalesorpurchasedetailFields.CustomField9] = salesorpurchasedetail.CustomField9;
                dr[SalesorpurchasedetailFields.CustomField10] = salesorpurchasedetail.CustomField10;
                dr[SalesorpurchasedetailFields.Idkey] = salesorpurchasedetail.Idkey;
                dr[SalesorpurchasedetailFields.GroupIDKEY] = salesorpurchasedetail.GroupIDKEY;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
