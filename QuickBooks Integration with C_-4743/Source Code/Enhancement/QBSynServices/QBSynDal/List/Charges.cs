//
// Class	:	Charges.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class Charges : AbstractDalBase
    {
        #region Constructors / Destructors
        public Charges(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                chargeTableAdapter _chargeTableAdapter = new chargeTableAdapter();
                _chargeTableAdapter.Connection = Connection;
                _chargeTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                   
                }

                foreach (Charge charge in this.Items)
                {
                    QBSynDal.List.QBSynDal.chargeDataTable chargeTable = _chargeTableAdapter.GetDataByTxnID(charge.TxnID);
                    DataRow dr = null;

                    if (chargeTable.Rows.Count == 0)
                    {
                        dr = chargeTable.NewRow();
                        Bind(dr, charge);
                        chargeTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = chargeTable.Rows[0];
                        Bind(dr, charge);
                    }
                    _chargeTableAdapter.Update(chargeTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Charge charge)
        {
            try
            {
                dr[ChargeFields.Amount] = charge.Amount.GetDbType();
                dr[ChargeFields.ARAccountRef_FullName] = charge.ARAccountRef_FullName;
                dr[ChargeFields.ARAccountRef_ListID] = charge.ARAccountRef_ListID;
                dr[ChargeFields.BalanceRemaining] = charge.BalanceRemaining.GetDbType();
                dr[ChargeFields.BilledDate] = charge.BilledDate.GetDbType();
                dr[ChargeFields.ClassRef_FullName] = charge.ClassRef_FullName;
                dr[ChargeFields.ClassRef_ListID] = charge.ClassRef_ListID;
                dr[ChargeFields.CustomerRef_FullName] = charge.CustomerRef_FullName;
                dr[ChargeFields.CustomerRef_ListID] = charge.CustomerRef_ListID;
                dr[ChargeFields.CustomField1] = charge.CustomField1;
                dr[ChargeFields.CustomField2] = charge.CustomField2;
                dr[ChargeFields.CustomField3] = charge.CustomField3;
                dr[ChargeFields.CustomField4] = charge.CustomField4;
                dr[ChargeFields.CustomField5] = charge.CustomField5;
                dr[ChargeFields.CustomField6] = charge.CustomField6;
                dr[ChargeFields.CustomField7] = charge.CustomField7;
                dr[ChargeFields.CustomField8] = charge.CustomField8;
                dr[ChargeFields.CustomField9] = charge.CustomField9;
                dr[ChargeFields.CustomField10] = charge.CustomField10;
                dr[ChargeFields.Desc] = charge.Desc;
                dr[ChargeFields.DueDate] = charge.DueDate.GetDbType();
                dr[ChargeFields.EditSequence] = charge.EditSequence;
                dr[ChargeFields.InventorySiteRef_FullName] = charge.InventorySiteRef_FullName;
                dr[ChargeFields.InventorySiteRef_ListID] = charge.InventorySiteRef_ListID;
                dr[ChargeFields.IsPaid] = charge.IsPaid.GetShort().GetDbType();
                dr[ChargeFields.ItemRef_FullName] = charge.ItemRef_FullName;
                dr[ChargeFields.ItemRef_ListID] = charge.ItemRef_ListID;
                dr[ChargeFields.OverrideUOMSetRef_FullName] = charge.OverrideUOMSetRef_FullName;
                dr[ChargeFields.OverrideUOMSetRef_ListID] = charge.OverrideUOMSetRef_ListID;
                dr[ChargeFields.Quantity] = charge.Quantity;
                dr[ChargeFields.Rate] = charge.Rate;
                dr[ChargeFields.RefNumber] = charge.RefNumber;
                dr[ChargeFields.Status] = charge.Status;
                dr[ChargeFields.TimeCreated] = charge.TimeCreated;
                dr[ChargeFields.TimeModified] = charge.TimeModified;
                dr[ChargeFields.TxnDate] = charge.TxnDate.GetDbType();
                dr[ChargeFields.TxnID] = charge.TxnID;
                dr[ChargeFields.TxnNumber] = charge.TxnNumber.GetDbType();
                dr[ChargeFields.UnitOfMeasure] = charge.UnitOfMeasure;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
