//
// Class	:	Arrefundcreditcards.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List;
using QBSynDal.List.QBSynDalTableAdapters;
using QBSynDal.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Linq;
using System.Text;

namespace QBSynDal
{
    public class Salesorderlinedetails : AbstractDalBase
    {
        #region Constructors / Destructors
        public Salesorderlinedetails(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                salesorderlinedetailTableAdapter _salesorderlinedetailTableAdapter = new salesorderlinedetailTableAdapter();
                _salesorderlinedetailTableAdapter.Connection = Connection;
                _salesorderlinedetailTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                QBSynDal.List.QBSynDal.salesorderlinedetailDataTable salesorderlinedetailDataTable = new List.QBSynDal.salesorderlinedetailDataTable();
                foreach (Salesorderlinedetail salesorderlinedetail in this.Items)
                {
                    DataRow dr = salesorderlinedetailDataTable.NewRow();
                    salesorderlinedetailDataTable.Rows.Add(dr);
                    Bind(dr, salesorderlinedetail);
                }
                _salesorderlinedetailTableAdapter.Update(salesorderlinedetailDataTable);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Salesorderlinedetail salesorderlinedetail)
        {
            try
            {
                dr[SalesorderlinedetailFields.TxnLineID] = salesorderlinedetail.TxnLineID;
                dr[SalesorderlinedetailFields.ItemRef_ListID] = salesorderlinedetail.ItemRef_ListID;
                dr[SalesorderlinedetailFields.ItemRef_FullName] = salesorderlinedetail.ItemRef_FullName;
                dr[SalesorderlinedetailFields.Desc] = salesorderlinedetail.Desc;
                dr[SalesorderlinedetailFields.Quantity] = salesorderlinedetail.Quantity;
                dr[SalesorderlinedetailFields.UnitOfMeasure] = salesorderlinedetail.UnitOfMeasure;
                dr[SalesorderlinedetailFields.OverrideUOMSetRef_ListID] = salesorderlinedetail.OverrideUOMSetRef_ListID;
                dr[SalesorderlinedetailFields.OverrideUOMSetRef_FullName] = salesorderlinedetail.OverrideUOMSetRef_FullName;
                dr[SalesorderlinedetailFields.Rate] = salesorderlinedetail.Rate;
                dr[SalesorderlinedetailFields.RatePercent] = salesorderlinedetail.RatePercent;
                dr[SalesorderlinedetailFields.ClassRef_ListID] = salesorderlinedetail.ClassRef_ListID;
                dr[SalesorderlinedetailFields.ClassRef_FullName] = salesorderlinedetail.ClassRef_FullName;
                dr[SalesorderlinedetailFields.Amount] = salesorderlinedetail.Amount.GetDbType();
                dr[SalesorderlinedetailFields.InventorySiteRef_ListID] = salesorderlinedetail.InventorySiteRef_ListID;
                dr[SalesorderlinedetailFields.InventorySiteRef_FullName] = salesorderlinedetail.InventorySiteRef_FullName;
                dr[SalesorderlinedetailFields.SalesTaxCodeRef_ListID] = salesorderlinedetail.SalesTaxCodeRef_ListID;
                dr[SalesorderlinedetailFields.SalesTaxCodeRef_FullName] = salesorderlinedetail.SalesTaxCodeRef_FullName;
                dr[SalesorderlinedetailFields.Invoiced] = salesorderlinedetail.Invoiced;
                dr[SalesorderlinedetailFields.IsManuallyClosed] = salesorderlinedetail.IsManuallyClosed.GetShort().GetDbType();
                dr[SalesorderlinedetailFields.Other1] = salesorderlinedetail.Other1;
                dr[SalesorderlinedetailFields.Other2] = salesorderlinedetail.Other2;
                dr[SalesorderlinedetailFields.CustomField1] = salesorderlinedetail.CustomField1;
                dr[SalesorderlinedetailFields.CustomField2] = salesorderlinedetail.CustomField2;
                dr[SalesorderlinedetailFields.CustomField3] = salesorderlinedetail.CustomField3;
                dr[SalesorderlinedetailFields.CustomField4] = salesorderlinedetail.CustomField4;
                dr[SalesorderlinedetailFields.CustomField5] = salesorderlinedetail.CustomField5;
                dr[SalesorderlinedetailFields.CustomField6] = salesorderlinedetail.CustomField6;
                dr[SalesorderlinedetailFields.CustomField7] = salesorderlinedetail.CustomField7;
                dr[SalesorderlinedetailFields.CustomField8] = salesorderlinedetail.CustomField8;
                dr[SalesorderlinedetailFields.CustomField9] = salesorderlinedetail.CustomField9;
                dr[SalesorderlinedetailFields.CustomField10] = salesorderlinedetail.CustomField10;
                dr[SalesorderlinedetailFields.Idkey] = salesorderlinedetail.Idkey;
                dr[SalesorderlinedetailFields.GroupIDKEY] = salesorderlinedetail.GroupIDKEY;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
