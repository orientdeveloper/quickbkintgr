//
// Class	:	Itemnoninventories.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:51 AM
//
using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;
namespace QBSynDal
{
    public class Itemnoninventories : AbstractDalBase
    {

        #region Constructors / Destructors
        public Itemnoninventories(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                itemnoninventoryTableAdapter _itemnoninventoryTableAdapter = new itemnoninventoryTableAdapter();
                _itemnoninventoryTableAdapter.Connection = Connection;
                _itemnoninventoryTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Itemnoninventory itemnoninventory in this.Items)
                {
                    QBSynDal.List.QBSynDal.itemnoninventoryDataTable itemnoninventoryTable = _itemnoninventoryTableAdapter.GetDataByListID(itemnoninventory.ListID);
                    DataRow dr = null;

                    if (itemnoninventoryTable.Rows.Count == 0)
                    {
                        dr = itemnoninventoryTable.NewRow();
                        Bind(dr, itemnoninventory);
                        itemnoninventoryTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = itemnoninventoryTable.Rows[0];
                        Bind(dr, itemnoninventory);
                    }
                    _itemnoninventoryTableAdapter.Update(itemnoninventoryTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Itemnoninventory itemnoninventory)
        {
            try
            {
                dr[ItemnoninventoryFields.ListID] = itemnoninventory.ListID;
                dr[ItemnoninventoryFields.TimeCreated] = itemnoninventory.TimeCreated;
                dr[ItemnoninventoryFields.TimeModified] = itemnoninventory.TimeModified;
                dr[ItemnoninventoryFields.EditSequence] = itemnoninventory.EditSequence;
                dr[ItemnoninventoryFields.Name] = itemnoninventory.Name;
                dr[ItemnoninventoryFields.FullName] = itemnoninventory.FullName;
                dr[ItemnoninventoryFields.IsActive] = itemnoninventory.IsActive.GetShort().GetDbType();
                dr[ItemnoninventoryFields.ParentRef_FullName] = itemnoninventory.ParentRef_FullName;
                dr[ItemnoninventoryFields.ParentRef_ListID] = itemnoninventory.ParentRef_ListID;
                dr[ItemnoninventoryFields.Sublevel] = itemnoninventory.Sublevel.GetDbType();
                dr[ItemnoninventoryFields.ManufacturerPartNumber] = itemnoninventory.ManufacturerPartNumber;
                dr[ItemnoninventoryFields.UnitOfMeasureSetRef_ListID] = itemnoninventory.UnitOfMeasureSetRef_ListID;
                dr[ItemnoninventoryFields.UnitOfMeasureSetRef_FullName] = itemnoninventory.UnitOfMeasureSetRef_FullName;
                dr[ItemnoninventoryFields.IsTaxIncluded] = itemnoninventory.IsTaxIncluded.GetShort().GetDbType();
                dr[ItemnoninventoryFields.SalesTaxCodeRef_FullName] = itemnoninventory.SalesTaxCodeRef_FullName;
                dr[ItemnoninventoryFields.SalesTaxCodeRef_ListID] = itemnoninventory.SalesTaxCodeRef_ListID;
                dr[ItemnoninventoryFields.CustomField1] = itemnoninventory.CustomField1;
                dr[ItemnoninventoryFields.CustomField10] = itemnoninventory.CustomField10;
                dr[ItemnoninventoryFields.CustomField2] = itemnoninventory.CustomField2;
                dr[ItemnoninventoryFields.CustomField2] = itemnoninventory.CustomField3;
                dr[ItemnoninventoryFields.CustomField2] = itemnoninventory.CustomField4;
                dr[ItemnoninventoryFields.CustomField2] = itemnoninventory.CustomField5;
                dr[ItemnoninventoryFields.CustomField2] = itemnoninventory.CustomField6;
                dr[ItemnoninventoryFields.CustomField2] = itemnoninventory.CustomField7;
                dr[ItemnoninventoryFields.CustomField2] = itemnoninventory.CustomField8;
                dr[ItemnoninventoryFields.CustomField2] = itemnoninventory.CustomField9;
                dr[ItemnoninventoryFields.Status] = itemnoninventory.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
