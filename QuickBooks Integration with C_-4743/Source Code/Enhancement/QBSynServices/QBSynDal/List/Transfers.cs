//
// Class	:	Transfer.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/19/2013 11:13:42 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;
using QBSynDal;

namespace QBSynDal
{
    public class Transfers : AbstractDalBase
    {
        #region Constructors / Destructors
        public Transfers(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                TransferTableAdapter _transferTableAdapter = new TransferTableAdapter();
                _transferTableAdapter.Connection = Connection;
                _transferTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Transfer transfer in this.Items)
                {
                    QBSynDal.List.QBSynDal.TransferDataTable transferTable = _transferTableAdapter.GetDataByTxnID(transfer.TxnID);
                    DataRow dr = null;

                    if (transferTable.Rows.Count == 0)
                    {
                        dr = transferTable.NewRow();
                        Bind(dr, transfer);
                        transferTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = transferTable.Rows[0];
                        Bind(dr, transfer);
                    }
                    _transferTableAdapter.Update(transferTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Methods (Private)
        private void Bind(DataRow dr, Transfer transfer)
        {
            try
            {
                dr[TransferFields.TxnID] = transfer.TxnID;
                dr[TransferFields.TimeModified] = transfer.TimeModified;
                dr[TransferFields.TimeCreated] = transfer.TimeCreated;
                dr[TransferFields.EditSequence] = transfer.EditSequence;
                dr[TransferFields.Amount] = transfer.Amount.GetDbType();
                dr[TransferFields.ClassRef_ListID] = transfer.ClassRef_ListID;
                dr[TransferFields.ClassRef_FullName] = transfer.ClassRef_FullName;
                dr[TransferFields.FromAccountBalance] = transfer.FromAccountBalance.GetDbType();
                dr[TransferFields.Memo] = transfer.Memo;
                dr[TransferFields.ToAccountBalance] = transfer.ToAccountBalance.GetDbType();
                dr[TransferFields.TransferFromAccountRef_ListID] = transfer.TransferFromAccountRef_ListID;
                dr[TransferFields.TransferFromAccountRef_FullName] = transfer.TransferFromAccountRef_FullName;
                dr[TransferFields.TransferToAccountRef_ListID] = transfer.TransferToAccountRef_ListID;
                dr[TransferFields.TransferToAccountRef_FullName] = transfer.TransferToAccountRef_FullName;
                dr[TransferFields.TxnDate] = transfer.TxnDate.GetDbType();
                dr[TransferFields.TxnNumber] = transfer.TxnNumber.GetDbType();
                dr[TransferFields.CustomField1] = transfer.CustomField1;
                dr[TransferFields.CustomField2] = transfer.CustomField2;
                dr[TransferFields.CustomField3] = transfer.CustomField3;
                dr[TransferFields.CustomField4] = transfer.CustomField4;
                dr[TransferFields.CustomField5] = transfer.CustomField5;
                dr[TransferFields.CustomField6] = transfer.CustomField6;
                dr[TransferFields.CustomField7] = transfer.CustomField7;
                dr[TransferFields.CustomField8] = transfer.CustomField8;
                dr[TransferFields.CustomField9] = transfer.CustomField9;
                dr[TransferFields.CustomField10] = transfer.CustomField10;
                dr[TransferFields.Status] = transfer.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
