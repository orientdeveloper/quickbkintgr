//
// Class	:	Billingrates.cs
// Author	:  	Synapse © 2006
// Date		:	4/2/2013 10:11:43 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class Billingrates : AbstractDalBase
    {
        #region Constructors / Destructors
        public Billingrates(string constr)
            :base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                billingrateTableAdapter _billingrateTableAdapter = new billingrateTableAdapter();
                _billingrateTableAdapter.Connection = Connection;
                _billingrateTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Billingrate billingrate in this.Items)
                {
                    QBSynDal.List.QBSynDal.billingrateDataTable billingrateTable = _billingrateTableAdapter.GetDataByListID(billingrate.ListID);
                    DataRow dr = null;

                    if (billingrateTable.Rows.Count == 0)
                    {
                        dr = billingrateTable.NewRow();
                        billingrateTable.Rows.Add(dr);
                        Bind(dr, billingrate);
                    }
                    else
                    {
                        dr = billingrateTable.Rows[0];
                        Bind(dr, billingrate);
                    }
                    _billingrateTableAdapter.Update(billingrateTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Method (Private)
        private void Bind(DataRow dr, Billingrate billingrate)
        {
            try
            {
                dr[BillingrateFields.ListID] = billingrate.ListID;
                dr[BillingrateFields.TimeCreated] = billingrate.TimeCreated;
                dr[BillingrateFields.TimeModified] = billingrate.TimeModified;
                dr[BillingrateFields.EditSequence] = billingrate.EditSequence;
                dr[BillingrateFields.Name] = billingrate.Name;
                dr[BillingrateFields.BillingRateType] = billingrate.BillingRateType;
                dr[BillingrateFields.FixedBillingRate] = billingrate.FixedBillingRate;
                dr[BillingrateFields.Status] = billingrate.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
