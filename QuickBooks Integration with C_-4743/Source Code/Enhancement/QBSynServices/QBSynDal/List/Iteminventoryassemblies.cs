////
// Class	:	Iteminventoryassemblies.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:50 AM
//


using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class Iteminventoryassemblyassemblies : AbstractDalBase
    {
        #region Constructors / Destructors
        public Iteminventoryassemblyassemblies(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                iteminventoryassemblyTableAdapter _iteminventoryassemblyTableAdapter = new iteminventoryassemblyTableAdapter();
                _iteminventoryassemblyTableAdapter.Connection = Connection;
                _iteminventoryassemblyTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Iteminventoryassembly iteminventoryassembly in this.Items)
                {
                    QBSynDal.List.QBSynDal.iteminventoryassemblyDataTable iteminventoryassemblyTable = _iteminventoryassemblyTableAdapter.GetDataByListID(iteminventoryassembly.ListID);
                    DataRow dr = null;

                    if (iteminventoryassemblyTable.Rows.Count == 0)
                    {
                        dr = iteminventoryassemblyTable.NewRow();
                        iteminventoryassemblyTable.Rows.Add(dr);
                        Bind(dr, iteminventoryassembly);
                    }
                    else
                    {
                        dr = iteminventoryassemblyTable.Rows[0];
                        Bind(dr, iteminventoryassembly);
                    }
                    _iteminventoryassemblyTableAdapter.Update(iteminventoryassemblyTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Method (Private)
        private void Bind(DataRow dr, Iteminventoryassembly iteminventoryassembly)
        {
            try
            {
                dr[IteminventoryassemblyFields.ListID] = iteminventoryassembly.ListID;
                dr[IteminventoryassemblyFields.TimeCreated] = iteminventoryassembly.TimeCreated;
                dr[IteminventoryassemblyFields.TimeModified] = iteminventoryassembly.TimeModified;
                dr[IteminventoryassemblyFields.EditSequence] = iteminventoryassembly.EditSequence;
                dr[IteminventoryassemblyFields.Name] = iteminventoryassembly.Name;
                dr[IteminventoryassemblyFields.FullName] = iteminventoryassembly.FullName;
                dr[IteminventoryassemblyFields.IsActive] = iteminventoryassembly.IsActive.GetShort().GetDbType();
                dr[IteminventoryassemblyFields.ParentRef_FullName] = iteminventoryassembly.ParentRef_FullName;
                dr[IteminventoryassemblyFields.ParentRef_ListID] = iteminventoryassembly.ParentRef_ListID;
                dr[IteminventoryassemblyFields.Sublevel] = iteminventoryassembly.Sublevel.GetDbType();
                dr[IteminventoryassemblyFields.SalesTaxCodeRef_FullName] = iteminventoryassembly.SalesTaxCodeRef_FullName;
                dr[IteminventoryassemblyFields.SalesTaxCodeRef_ListID] = iteminventoryassembly.SalesTaxCodeRef_ListID;
                dr[IteminventoryassemblyFields.IsTaxIncluded] = iteminventoryassembly.IsTaxIncluded.GetShort().GetDbType();
                dr[IteminventoryassemblyFields.SalesDesc] = iteminventoryassembly.SalesDesc;
                dr[IteminventoryassemblyFields.SalesPrice] = iteminventoryassembly.SalesPrice;
                dr[IteminventoryassemblyFields.IncomeAccountRef_ListID] = iteminventoryassembly.IncomeAccountRef_ListID;
                dr[IteminventoryassemblyFields.IncomeAccountRef_FullName] = iteminventoryassembly.IncomeAccountRef_FullName;
                dr[IteminventoryassemblyFields.PurchaseDesc] = iteminventoryassembly.PurchaseDesc;
                dr[IteminventoryassemblyFields.PurchaseCost] = iteminventoryassembly.PurchaseCost;
                dr[IteminventoryassemblyFields.PurchaseTaxCodeRef_ListID] = iteminventoryassembly.PurchaseTaxCodeRef_ListID;
                dr[IteminventoryassemblyFields.PurchaseTaxCodeRef_FullName] = iteminventoryassembly.PurchaseTaxCodeRef_FullName;
                dr[IteminventoryassemblyFields.COGSAccountRef_ListID] = iteminventoryassembly.COGSAccountRef_ListID;
                dr[IteminventoryassemblyFields.COGSAccountRef_FullName] = iteminventoryassembly.COGSAccountRef_FullName;
                dr[IteminventoryassemblyFields.PrefVendorRef_ListID] = iteminventoryassembly.PrefVendorRef_ListID;
                dr[IteminventoryassemblyFields.PrefVendorRef_FullName] = iteminventoryassembly.PrefVendorRef_FullName;
                dr[IteminventoryassemblyFields.AssetAccountRef_ListID] = iteminventoryassembly.AssetAccountRef_ListID;
                dr[IteminventoryassemblyFields.AssetAccountRef_FullName] = iteminventoryassembly.AssetAccountRef_FullName;
                dr[IteminventoryassemblyFields.BuildPoint] = iteminventoryassembly.BuildPoint;
                dr[IteminventoryassemblyFields.QuantityOnHand] = iteminventoryassembly.QuantityOnHand;
                dr[IteminventoryassemblyFields.AverageCost] = iteminventoryassembly.AverageCost;
                dr[IteminventoryassemblyFields.QuantityOnOrder] = iteminventoryassembly.QuantityOnOrder;
                dr[IteminventoryassemblyFields.QuantityOnSalesOrder] = iteminventoryassembly.QuantityOnSalesOrder;
                dr[IteminventoryassemblyFields.CustomField1] = iteminventoryassembly.CustomField1;
                dr[IteminventoryassemblyFields.CustomField10] = iteminventoryassembly.CustomField10;
                dr[IteminventoryassemblyFields.CustomField2] = iteminventoryassembly.CustomField2;
                dr[IteminventoryassemblyFields.CustomField2] = iteminventoryassembly.CustomField3;
                dr[IteminventoryassemblyFields.CustomField2] = iteminventoryassembly.CustomField4;
                dr[IteminventoryassemblyFields.CustomField2] = iteminventoryassembly.CustomField5;
                dr[IteminventoryassemblyFields.CustomField2] = iteminventoryassembly.CustomField6;
                dr[IteminventoryassemblyFields.CustomField2] = iteminventoryassembly.CustomField7;
                dr[IteminventoryassemblyFields.CustomField2] = iteminventoryassembly.CustomField8;
                dr[IteminventoryassemblyFields.CustomField2] = iteminventoryassembly.CustomField9;
                dr[IteminventoryassemblyFields.Status] = iteminventoryassembly.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
