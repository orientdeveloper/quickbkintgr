//
// Class	:	Items.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List;
using QBSynDal.List.QBSynDalTableAdapters;
using QBSynDal.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Linq;
using System.Text;

namespace QBSynDal
{
    public class Linkedtxndetails : AbstractDalBase
    {

        #region Constructors / Destructors
        public Linkedtxndetails(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                linkedtxndetailTableAdapter _linkedtxndetailTableAdapter = new linkedtxndetailTableAdapter();
                _linkedtxndetailTableAdapter.Connection = Connection;
                _linkedtxndetailTableAdapter.Transaction = Transaction;

                QBSynDal.List.QBSynDal.linkedtxndetailDataTable linkedtxndetailDataTable = new List.QBSynDal.linkedtxndetailDataTable();
                foreach (Linkedtxndetail linkedtxndetail in this.Items)
                {
                    DataRow dr = linkedtxndetailDataTable.NewRow();
                    linkedtxndetailDataTable.Rows.Add(dr);
                    Bind(dr, linkedtxndetail);
                }
                _linkedtxndetailTableAdapter.Update(linkedtxndetailDataTable);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Linkedtxndetail linkedtxndetail)
        {
            try
            {
                dr[LinkedtxndetailFields.TxnID] = linkedtxndetail.TxnID;
                dr[LinkedtxndetailFields.TxnType] = linkedtxndetail.TxnType;
                dr[LinkedtxndetailFields.TxnDate] = linkedtxndetail.TxnDate.GetDbType();
                dr[LinkedtxndetailFields.RefNumber] = linkedtxndetail.RefNumber;
                dr[LinkedtxndetailFields.LinkType] = linkedtxndetail.LinkType;
                dr[LinkedtxndetailFields.Amount] = linkedtxndetail.Amount.GetDbType();
                dr[LinkedtxndetailFields.CustomField1] = linkedtxndetail.CustomField1;
                dr[LinkedtxndetailFields.CustomField2] = linkedtxndetail.CustomField2;
                dr[LinkedtxndetailFields.CustomField3] = linkedtxndetail.CustomField3;
                dr[LinkedtxndetailFields.CustomField4] = linkedtxndetail.CustomField4;
                dr[LinkedtxndetailFields.CustomField5] = linkedtxndetail.CustomField5;
                dr[LinkedtxndetailFields.CustomField6] = linkedtxndetail.CustomField6;
                dr[LinkedtxndetailFields.CustomField7] = linkedtxndetail.CustomField7;
                dr[LinkedtxndetailFields.CustomField8] = linkedtxndetail.CustomField8;
                dr[LinkedtxndetailFields.CustomField9] = linkedtxndetail.CustomField9;
                dr[LinkedtxndetailFields.CustomField10] = linkedtxndetail.CustomField10;
                dr[LinkedtxndetailFields.Idkey] = linkedtxndetail.Idkey;
                dr[LinkedtxndetailFields.GroupIDKEY] = linkedtxndetail.GroupIDKEY;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
