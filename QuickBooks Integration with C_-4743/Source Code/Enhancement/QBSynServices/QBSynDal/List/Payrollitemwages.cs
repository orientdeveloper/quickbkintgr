﻿
using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
   public class Payrollitemwages: AbstractDalBase
    {
        #region Constructors / Destructors
       public Payrollitemwages(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                payrollitemwageTableAdapter _payrollitemwageTableAdapter = new payrollitemwageTableAdapter();
                _payrollitemwageTableAdapter.Connection = Connection;
                _payrollitemwageTableAdapter.Transaction = Transaction;
                if (synAction != SynAction.None)
                {
                    
                }
                foreach (Payrollitemwage payrollitemwage in this.Items)
                {
                    QBSynDal.List.QBSynDal.payrollitemwageDataTable payrollitemwageTable = _payrollitemwageTableAdapter.GetDataByListID(payrollitemwage.ListID);
                    DataRow dr = null;

                    if (payrollitemwageTable.Rows.Count == 0)
                    {
                        dr = payrollitemwageTable.NewRow();
                        Bind(dr, payrollitemwage);
                        payrollitemwageTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = payrollitemwageTable.Rows[0];
                        Bind(dr, payrollitemwage);
                    }
                    _payrollitemwageTableAdapter.Update(payrollitemwageTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Payrollitemwage payrollitemwage)
        {
            try
            {
                dr[PayrollitemwageFields.ListID] = payrollitemwage.ListID;
                dr[PayrollitemwageFields.TimeCreated] = payrollitemwage.TimeCreated;
                dr[PayrollitemwageFields.TimeModified] = payrollitemwage.TimeModified;
                dr[PayrollitemwageFields.EditSequence] = payrollitemwage.EditSequence;
                dr[PayrollitemwageFields.Name] = payrollitemwage.Name;
                dr[PayrollitemwageFields.IsActive] = payrollitemwage.IsActive;
                dr[PayrollitemwageFields.WageType] = payrollitemwage.WageType;
                dr[PayrollitemwageFields.ExpenseAccountRef_ListID] = payrollitemwage.ExpenseAccountRef_ListID;
                dr[PayrollitemwageFields.ExpenseAccountRef_FullName] = payrollitemwage.ExpenseAccountRef_FullName;
                dr[PayrollitemwageFields.Status] = payrollitemwage.Status;
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
