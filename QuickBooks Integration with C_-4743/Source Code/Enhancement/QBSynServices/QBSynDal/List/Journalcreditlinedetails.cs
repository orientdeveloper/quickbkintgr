//
// Class	:	Journalcreditlinedetails.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List;
using QBSynDal.List.QBSynDalTableAdapters;
using QBSynDal.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Linq;
using System.Text;

namespace QBSynDal
{
    public class Journalcreditlinedetails : AbstractDalBase
    {
        #region Constructors / Destructors
        public Journalcreditlinedetails(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                journalcreditlinedetailTableAdapter _journalcreditlinedetailTableAdapter = new journalcreditlinedetailTableAdapter();
                _journalcreditlinedetailTableAdapter.Connection = Connection;
                _journalcreditlinedetailTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                QBSynDal.List.QBSynDal.journalcreditlinedetailDataTable journalcreditlinedetailDataTable = new List.QBSynDal.journalcreditlinedetailDataTable();
                foreach (Journalcreditlinedetail journalcreditlinedetail in this.Items)
                {
                    DataRow dr = journalcreditlinedetailDataTable.NewRow();
                    journalcreditlinedetailDataTable.Rows.Add(dr);
                    Bind(dr, journalcreditlinedetail);
                }
                _journalcreditlinedetailTableAdapter.Update(journalcreditlinedetailDataTable);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Journalcreditlinedetail journalcreditlinedetail)
        {
            try
            {
                dr[JournalcreditlinedetailFields.TxnLineID] = journalcreditlinedetail.TxnLineID;
                dr[JournalcreditlinedetailFields.AccountRef_ListID] = journalcreditlinedetail.AccountRef_ListID;
                dr[JournalcreditlinedetailFields.AccountRef_FullName] = journalcreditlinedetail.AccountRef_FullName;
                dr[JournalcreditlinedetailFields.Amount] = journalcreditlinedetail.Amount.GetDbType();
                dr[JournalcreditlinedetailFields.Memo] = journalcreditlinedetail.Memo;
                dr[JournalcreditlinedetailFields.IsAdjustment] = journalcreditlinedetail.IsAdjustment.GetShort().GetDbType();
                dr[JournalcreditlinedetailFields.EntityRef_ListID] = journalcreditlinedetail.EntityRef_ListID;
                dr[JournalcreditlinedetailFields.EntityRef_FullName] = journalcreditlinedetail.EntityRef_FullName;
                dr[JournalcreditlinedetailFields.ClassRef_ListID] = journalcreditlinedetail.ClassRef_ListID;
                dr[JournalcreditlinedetailFields.ClassRef_FullName] = journalcreditlinedetail.ClassRef_FullName;
                dr[JournalcreditlinedetailFields.ItemSalesTaxRef_ListID] = journalcreditlinedetail.ItemSalesTaxRef_ListID;
                dr[JournalcreditlinedetailFields.ItemSalesTaxRef_FullName] = journalcreditlinedetail.ItemSalesTaxRef_FullName;
                dr[JournalcreditlinedetailFields.BillableStatus] = journalcreditlinedetail.BillableStatus;
                dr[JournalcreditlinedetailFields.CustomField1] = journalcreditlinedetail.CustomField1;
                dr[JournalcreditlinedetailFields.CustomField2] = journalcreditlinedetail.CustomField2;
                dr[JournalcreditlinedetailFields.CustomField3] = journalcreditlinedetail.CustomField3;
                dr[JournalcreditlinedetailFields.CustomField4] = journalcreditlinedetail.CustomField4;
                dr[JournalcreditlinedetailFields.CustomField5] = journalcreditlinedetail.CustomField5;
                dr[JournalcreditlinedetailFields.CustomField6] = journalcreditlinedetail.CustomField6;
                dr[JournalcreditlinedetailFields.CustomField7] = journalcreditlinedetail.CustomField7;
                dr[JournalcreditlinedetailFields.CustomField8] = journalcreditlinedetail.CustomField8;
                dr[JournalcreditlinedetailFields.CustomField9] = journalcreditlinedetail.CustomField9;
                dr[JournalcreditlinedetailFields.CustomField10] = journalcreditlinedetail.CustomField10;
                dr[JournalcreditlinedetailFields.Idkey] = journalcreditlinedetail.Idkey;
                dr[JournalcreditlinedetailFields.GroupIDKEY] = journalcreditlinedetail.GroupIDKEY;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
