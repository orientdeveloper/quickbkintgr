//
// Class	:	Checks.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;
namespace QBSynDal
{
    public class Checks : AbstractDalBase
    {
        #region Constructors / Destructors
        public Checks(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                checkTableAdapter _checkTableAdapter = new checkTableAdapter();
                _checkTableAdapter.Connection = Connection;
                _checkTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Check check in this.Items)
                {
                    QBSynDal.List.QBSynDal.checkDataTable checkTable = _checkTableAdapter.GetDataByTxnID(check.TxnID);
                    DataRow dr = null;

                    if (checkTable.Rows.Count == 0)
                    {
                        dr = checkTable.NewRow();
                        Bind(dr, check);
                        checkTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = checkTable.Rows[0];
                        Bind(dr, check);
                    }
                    _checkTableAdapter.Update(checkTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Check check)
        {
            try
            {
                dr[CheckFields.AccountRef_FullName] = check.AccountRef_FullName;
                dr[CheckFields.AccountRef_ListID] = check.AccountRef_ListID;
                dr[CheckFields.Address_Addr1] = check.Address_Addr1;
                dr[CheckFields.Address_Addr2] = check.Address_Addr2;
                dr[CheckFields.Address_Addr3] = check.Address_Addr3;
                dr[CheckFields.Address_Addr4] = check.Address_Addr4;
                dr[CheckFields.Address_Addr5] = check.Address_Addr5;
                dr[CheckFields.Address_City] = check.Address_City;
                dr[CheckFields.Address_Country] = check.Address_Country;
                dr[CheckFields.Address_Note] = check.Address_Note;
                dr[CheckFields.Address_PostalCode] = check.Address_PostalCode;
                dr[CheckFields.Address_State] = check.Address_State;
                dr[CheckFields.Amount] = check.Amount.GetDbType();
                dr[CheckFields.AmountInHomeCurrency] = check.AmountInHomeCurrency.GetDbType();
                dr[CheckFields.CurrencyRef_FullName] = check.CurrencyRef_FullName;
                dr[CheckFields.CurrencyRef_ListID] = check.CurrencyRef_ListID;
                dr[CheckFields.CustomField1] = check.CustomField1;
                dr[CheckFields.CustomField2] = check.CustomField2;
                dr[CheckFields.CustomField3] = check.CustomField3;
                dr[CheckFields.CustomField4] = check.CustomField4;
                dr[CheckFields.CustomField5] = check.CustomField5;
                dr[CheckFields.CustomField6] = check.CustomField6;
                dr[CheckFields.CustomField7] = check.CustomField7;
                dr[CheckFields.CustomField8] = check.CustomField8;
                dr[CheckFields.CustomField9] = check.CustomField9;
                dr[CheckFields.CustomField10] = check.CustomField10;
                dr[CheckFields.EditSequence] = check.EditSequence;
                dr[CheckFields.ExchangeRate] = check.ExchangeRate.GetDbType();
                dr[CheckFields.IsTaxIncluded] = check.IsTaxIncluded.GetShort().GetDbType();
                dr[CheckFields.IsToBePrinted] = check.IsToBePrinted.GetShort().GetDbType();
                dr[CheckFields.Memo] = check.Memo;
                dr[CheckFields.PayeeEntityRef_FullName] = check.PayeeEntityRef_FullName;
                dr[CheckFields.PayeeEntityRef_ListID] = check.PayeeEntityRef_ListID;
                dr[CheckFields.RefNumber] = check.RefNumber;
                dr[CheckFields.SalesTaxCodeRef_FullName] = check.SalesTaxCodeRef_FullName;
                dr[CheckFields.SalesTaxCodeRef_ListID] = check.SalesTaxCodeRef_ListID;
                dr[CheckFields.Status] = check.Status;
                dr[CheckFields.TimeCreated] = check.TimeCreated;
                dr[CheckFields.TimeModified] = check.TimeModified;
                dr[CheckFields.TxnDate] = check.TxnDate.GetDbType();
                dr[CheckFields.TxnID] = check.TxnID;
                dr[CheckFields.TxnNumber] = check.TxnNumber.GetDbType();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
