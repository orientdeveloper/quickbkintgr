﻿using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class Payrollitemnonwages : AbstractDalBase
    {
       #region Constructors / Destructors
       public Payrollitemnonwages(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                payrollitemnonwageTableAdapter _payrollitemnonwageTableAdapter = new payrollitemnonwageTableAdapter();
                _payrollitemnonwageTableAdapter.Connection = Connection;
                _payrollitemnonwageTableAdapter.Transaction = Transaction;
                if (synAction != SynAction.None)
                {
                    
                }
                foreach (Payrollitemnonwage payrollitemnonwage in this.Items)
                {
                    QBSynDal.List.QBSynDal.payrollitemnonwageDataTable payrollitemnonwageTable = _payrollitemnonwageTableAdapter.GetDataByListID(payrollitemnonwage.ListID);
                    DataRow dr = null;

                    if (payrollitemnonwageTable.Rows.Count == 0)
                    {
                        dr = payrollitemnonwageTable.NewRow();
                        Bind(dr, payrollitemnonwage);
                        payrollitemnonwageTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = payrollitemnonwageTable.Rows[0];
                        Bind(dr, payrollitemnonwage);
                    }
                    _payrollitemnonwageTableAdapter.Update(payrollitemnonwageTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Payrollitemnonwage payrollitemnonwage)
        {
            try
            {
                dr[PayrollitemnonwageFields.ListID] = payrollitemnonwage.ListID;
                dr[PayrollitemnonwageFields.TimeCreated] = payrollitemnonwage.TimeCreated;
                dr[PayrollitemnonwageFields.TimeModified] = payrollitemnonwage.TimeModified;
                dr[PayrollitemnonwageFields.EditSequence] = payrollitemnonwage.EditSequence;
                dr[PayrollitemnonwageFields.Name] = payrollitemnonwage.Name;
                dr[PayrollitemnonwageFields.IsActive] = payrollitemnonwage.IsActive;
                dr[PayrollitemnonwageFields.NonWageType] = payrollitemnonwage.NonWageType;
                dr[PayrollitemnonwageFields.ExpenseAccountRef_ListID] = payrollitemnonwage.ExpenseAccountRef_ListID;
                dr[PayrollitemnonwageFields.ExpenseAccountRef_FullName] = payrollitemnonwage.ExpenseAccountRef_FullName;
                dr[PayrollitemnonwageFields.LiabilityAccountRef_ListID] = payrollitemnonwage.LiabilityAccountRef_ListID;
                dr[PayrollitemnonwageFields.LiabilityAccountRef_FullName] = payrollitemnonwage.LiabilityAccountRef_FullName;
                dr[PayrollitemnonwageFields.Status] = payrollitemnonwage.Status;
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
