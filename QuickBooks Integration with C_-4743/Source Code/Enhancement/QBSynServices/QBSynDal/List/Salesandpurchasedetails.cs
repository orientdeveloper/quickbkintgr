//
// Class	:	Salesandpurchasedetails.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List;
using QBSynDal.List.QBSynDalTableAdapters;
using QBSynDal.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Linq;
using System.Text;

namespace QBSynDal
{
    public class Salesandpurchasedetails : AbstractDalBase
    {
        #region Constructors / Destructors
        public Salesandpurchasedetails(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                salesandpurchasedetailTableAdapter _salesandpurchasedetailTableAdapter = new salesandpurchasedetailTableAdapter();
                _salesandpurchasedetailTableAdapter.Connection = Connection;
                _salesandpurchasedetailTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                QBSynDal.List.QBSynDal.salesandpurchasedetailDataTable salesandpurchasedetailDataTable = new List.QBSynDal.salesandpurchasedetailDataTable();
                foreach (Salesandpurchasedetail salesandpurchasedetail in this.Items)
                {
                    DataRow dr = salesandpurchasedetailDataTable.NewRow();
                    salesandpurchasedetailDataTable.Rows.Add(dr);
                    Bind(dr, salesandpurchasedetail);
                }
                _salesandpurchasedetailTableAdapter.Update(salesandpurchasedetailDataTable);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Salesandpurchasedetail salesandpurchasedetail)
        {
            try
            {
                dr[SalesandpurchasedetailFields.SalesDesc] = salesandpurchasedetail.SalesDesc;
                dr[SalesandpurchasedetailFields.SalesPrice] = salesandpurchasedetail.SalesPrice;
                dr[SalesandpurchasedetailFields.IncomeAccountRef_ListID] = salesandpurchasedetail.IncomeAccountRef_ListID;
                dr[SalesandpurchasedetailFields.IncomeAccountRef_FullName] = salesandpurchasedetail.IncomeAccountRef_FullName;
                dr[SalesandpurchasedetailFields.PurchaseDesc] = salesandpurchasedetail.PurchaseDesc;
                dr[SalesandpurchasedetailFields.PurchaseCost] = salesandpurchasedetail.PurchaseCost;
                dr[SalesandpurchasedetailFields.PurchaseTaxCodeRef_ListID] = salesandpurchasedetail.PurchaseTaxCodeRef_ListID;
                dr[SalesandpurchasedetailFields.PurchaseTaxCodeRef_FullName] = salesandpurchasedetail.PurchaseTaxCodeRef_FullName;
                dr[SalesandpurchasedetailFields.ExpenseAccountRef_ListID] = salesandpurchasedetail.ExpenseAccountRef_ListID;
                dr[SalesandpurchasedetailFields.ExpenseAccountRef_FullName] = salesandpurchasedetail.ExpenseAccountRef_FullName;
                dr[SalesandpurchasedetailFields.PrefVendorRef_ListID] = salesandpurchasedetail.PrefVendorRef_ListID;
                dr[SalesandpurchasedetailFields.PrefVendorRef_FullName] = salesandpurchasedetail.PrefVendorRef_FullName;
                dr[SalesandpurchasedetailFields.CustomField1] = salesandpurchasedetail.CustomField1;
                dr[SalesandpurchasedetailFields.CustomField2] = salesandpurchasedetail.CustomField2;
                dr[SalesandpurchasedetailFields.CustomField3] = salesandpurchasedetail.CustomField3;
                dr[SalesandpurchasedetailFields.CustomField4] = salesandpurchasedetail.CustomField4;
                dr[SalesandpurchasedetailFields.CustomField5] = salesandpurchasedetail.CustomField5;
                dr[SalesandpurchasedetailFields.CustomField6] = salesandpurchasedetail.CustomField6;
                dr[SalesandpurchasedetailFields.CustomField7] = salesandpurchasedetail.CustomField7;
                dr[SalesandpurchasedetailFields.CustomField8] = salesandpurchasedetail.CustomField8;
                dr[SalesandpurchasedetailFields.CustomField9] = salesandpurchasedetail.CustomField9;
                dr[SalesandpurchasedetailFields.CustomField10] = salesandpurchasedetail.CustomField10;
                dr[SalesandpurchasedetailFields.Idkey] = salesandpurchasedetail.Idkey;
                dr[SalesandpurchasedetailFields.GroupIDKEY] = salesandpurchasedetail.GroupIDKEY;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
