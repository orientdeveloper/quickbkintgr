//
// Class	:	Purchaseorderlinegroupdetails.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List;
using QBSynDal.List.QBSynDalTableAdapters;
using QBSynDal.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Linq;
using System.Text;

namespace QBSynDal
{
    public class Purchaseorderlinegroupdetails : AbstractDalBase
    {
        #region Constructors / Destructors
        public Purchaseorderlinegroupdetails(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                purchaseorderlinegroupdetailTableAdapter _purchaseorderlinegroupdetailTableAdapter = new purchaseorderlinegroupdetailTableAdapter();
                _purchaseorderlinegroupdetailTableAdapter.Connection = Connection;
                _purchaseorderlinegroupdetailTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                QBSynDal.List.QBSynDal.purchaseorderlinegroupdetailDataTable purchaseorderlinegroupdetailDataTable = new List.QBSynDal.purchaseorderlinegroupdetailDataTable();
                foreach (Purchaseorderlinegroupdetail purchaseorderlinegroupdetail in this.Items)
                {
                    DataRow dr = purchaseorderlinegroupdetailDataTable.NewRow();
                    purchaseorderlinegroupdetailDataTable.Rows.Add(dr);
                    Bind(dr, purchaseorderlinegroupdetail);
                }
                _purchaseorderlinegroupdetailTableAdapter.Update(purchaseorderlinegroupdetailDataTable);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Purchaseorderlinegroupdetail purchaseorderlinegroupdetail)
        {
            try
            {
                dr[PurchaseorderlinegroupdetailFields.TxnLineID] = purchaseorderlinegroupdetail.TxnLineID;
                dr[PurchaseorderlinegroupdetailFields.ItemGroupRef_ListID] = purchaseorderlinegroupdetail.ItemGroupRef_ListID;
                dr[PurchaseorderlinegroupdetailFields.ItemGroupRef_FullName] = purchaseorderlinegroupdetail.ItemGroupRef_FullName;
                dr[PurchaseorderlinegroupdetailFields.Desc] = purchaseorderlinegroupdetail.Desc;
                dr[PurchaseorderlinegroupdetailFields.Quantity] = purchaseorderlinegroupdetail.Quantity;
                dr[PurchaseorderlinegroupdetailFields.IsPrintItemsInGroup] = purchaseorderlinegroupdetail.IsPrintItemsInGroup.GetShort().GetDbType();
                dr[PurchaseorderlinegroupdetailFields.TotalAmount] = purchaseorderlinegroupdetail.TotalAmount.GetDbType();
                dr[PurchaseorderlinegroupdetailFields.ServiceDate] = purchaseorderlinegroupdetail.ServiceDate.GetDbType();
                dr[PurchaseorderlinegroupdetailFields.CustomField1] = purchaseorderlinegroupdetail.CustomField1;
                dr[PurchaseorderlinegroupdetailFields.CustomField2] = purchaseorderlinegroupdetail.CustomField2;
                dr[PurchaseorderlinegroupdetailFields.CustomField3] = purchaseorderlinegroupdetail.CustomField3;
                dr[PurchaseorderlinegroupdetailFields.CustomField4] = purchaseorderlinegroupdetail.CustomField4;
                dr[PurchaseorderlinegroupdetailFields.CustomField5] = purchaseorderlinegroupdetail.CustomField5;
                dr[PurchaseorderlinegroupdetailFields.CustomField6] = purchaseorderlinegroupdetail.CustomField6;
                dr[PurchaseorderlinegroupdetailFields.CustomField7] = purchaseorderlinegroupdetail.CustomField7;
                dr[PurchaseorderlinegroupdetailFields.CustomField8] = purchaseorderlinegroupdetail.CustomField8;
                dr[PurchaseorderlinegroupdetailFields.CustomField9] = purchaseorderlinegroupdetail.CustomField9;
                dr[PurchaseorderlinegroupdetailFields.CustomField10] = purchaseorderlinegroupdetail.CustomField10;
                dr[PurchaseorderlinegroupdetailFields.Idkey] = purchaseorderlinegroupdetail.Idkey;
                dr[PurchaseorderlinegroupdetailFields.GroupIDKEY] = purchaseorderlinegroupdetail.GroupIDKEY;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
