//
// Class	:	Itemsubtotals.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;
namespace QBSynDal
{
    public class Itemsubtotals :AbstractDalBase
    {
         #region Constructors / Destructors
        public Itemsubtotals(string constr)
            :base(constr)
        {

        }
        #endregion

         #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                itemsubtotalTableAdapter _itemsubtotalTableAdapter = new itemsubtotalTableAdapter();
                _itemsubtotalTableAdapter.Connection = Connection;
                _itemsubtotalTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                   
                }

                foreach (Itemsubtotal itemsubtotal in this.Items)
                {
                    QBSynDal.List.QBSynDal.itemsubtotalDataTable itemsubtotalTable = _itemsubtotalTableAdapter.GetDataByListID(itemsubtotal.ListID);
                    DataRow dr = null;

                    if (itemsubtotalTable.Rows.Count == 0)
                    {
                        dr = itemsubtotalTable.NewRow();
                        itemsubtotalTable.Rows.Add(dr);
                        Bind(dr, itemsubtotal);
                    }
                    else
                    {
                        dr = itemsubtotalTable.Rows[0];
                        Bind(dr, itemsubtotal);
                    }
                    _itemsubtotalTableAdapter.Update(itemsubtotalTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

         #region Method (Private)
        private void Bind(DataRow dr, Itemsubtotal itemsubtotal)
        {
            try
            {
                dr[ItemsubtotalFields.ListID] = itemsubtotal.ListID;
                dr[ItemsubtotalFields.TimeCreated] = itemsubtotal.TimeCreated;
                dr[ItemsubtotalFields.TimeModified] = itemsubtotal.TimeModified;
                dr[ItemsubtotalFields.EditSequence] = itemsubtotal.EditSequence;
                dr[ItemsubtotalFields.Name] = itemsubtotal.Name;
                dr[ItemsubtotalFields.IsActive] = itemsubtotal.IsActive.GetShort().GetDbType();
                dr[ItemsubtotalFields.ItemDesc] = itemsubtotal.ItemDesc;
                dr[ItemsubtotalFields.SpecialItemType] = itemsubtotal.SpecialItemType;
                dr[ItemsubtotalFields.CustomField1] = itemsubtotal.CustomField1;
                dr[ItemsubtotalFields.CustomField2] = itemsubtotal.CustomField2;
                dr[ItemsubtotalFields.CustomField3] = itemsubtotal.CustomField3;
                dr[ItemsubtotalFields.CustomField4] = itemsubtotal.CustomField4;
                dr[ItemsubtotalFields.CustomField5] = itemsubtotal.CustomField5;
                dr[ItemsubtotalFields.CustomField6] = itemsubtotal.CustomField6;
                dr[ItemsubtotalFields.CustomField7] = itemsubtotal.CustomField7;
                dr[ItemsubtotalFields.CustomField8] = itemsubtotal.CustomField8;
                dr[ItemsubtotalFields.CustomField9] = itemsubtotal.CustomField9;
                dr[ItemsubtotalFields.CustomField10] = itemsubtotal.CustomField10;
                dr[ItemsubtotalFields.Status] = itemsubtotal.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
