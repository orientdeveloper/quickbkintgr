//
// Class	:	Othernames.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;
namespace QBSynDal
{
    public class Othernames : AbstractDalBase
    {
        #region Constructors / Destructors
        public Othernames(string constr)
            :base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                othernameTableAdapter _othernameTableAdapter = new othernameTableAdapter();
                _othernameTableAdapter.Connection = Connection;
                _othernameTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Othername othername in this.Items)
                {
                    QBSynDal.List.QBSynDal.othernameDataTable othernameTable = _othernameTableAdapter.GetDataByListID(othername.ListID);
                    DataRow dr = null;

                    if (othernameTable.Rows.Count == 0)
                    {
                        dr = othernameTable.NewRow();
                        othernameTable.Rows.Add(dr);
                        Bind(dr, othername);
                    }
                    else
                    {
                        dr = othernameTable.Rows[0];
                        Bind(dr, othername);
                    }
                    _othernameTableAdapter.Update(othernameTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Method (Private)
        private void Bind(DataRow dr, Othername othername)
        {
            try
            {
                dr[OthernameFields.ListID] = othername.ListID;
                dr[OthernameFields.TimeCreated] = othername.TimeCreated;
                dr[OthernameFields.TimeModified] = othername.TimeModified;
                dr[OthernameFields.EditSequence] = othername.EditSequence;
                dr[OthernameFields.Name] = othername.Name;
                dr[OthernameFields.IsActive] = othername.IsActive.GetShort().GetDbType();
                dr[OthernameFields.CompanyName] = othername.CompanyName;
                dr[OthernameFields.Salutation] = othername.Salutation;
                dr[OthernameFields.FirstName] = othername.FirstName;
                dr[OthernameFields.MiddleName] = othername.MiddleName;
                dr[OthernameFields.LastName] = othername.LastName;
                dr[OthernameFields.Suffix] = othername.Suffix;
                dr[OthernameFields.OtherNameAddress_Addr1] = othername.OtherNameAddress_Addr1;
                dr[OthernameFields.OtherNameAddress_Addr2] = othername.OtherNameAddress_Addr2;
                dr[OthernameFields.OtherNameAddress_Addr3] = othername.OtherNameAddress_Addr3;
                dr[OthernameFields.OtherNameAddress_Addr4] = othername.OtherNameAddress_Addr4;
                dr[OthernameFields.OtherNameAddress_Addr5] = othername.OtherNameAddress_Addr5;
                dr[OthernameFields.OtherNameAddress_City] = othername.OtherNameAddress_City;
                dr[OthernameFields.OtherNameAddress_State] = othername.OtherNameAddress_State;
                dr[OthernameFields.OtherNameAddress_PostalCode] = othername.OtherNameAddress_PostalCode;
                dr[OthernameFields.OtherNameAddress_Country] = othername.OtherNameAddress_Country;
                dr[OthernameFields.OtherNameAddress_Note] = othername.OtherNameAddress_Note;
                dr[OthernameFields.Phone] = othername.Phone;
                dr[OthernameFields.Mobile] = othername.Mobile;
                dr[OthernameFields.Pager] = othername.Pager;
                dr[OthernameFields.AltPhone] = othername.AltPhone;
                dr[OthernameFields.Fax] = othername.Fax;
                dr[OthernameFields.Email] = othername.Email;
                dr[OthernameFields.Contact] = othername.Contact;
                dr[OthernameFields.AltContact] = othername.AltContact;
                dr[OthernameFields.AccountNumber] = othername.AccountNumber;
                dr[OthernameFields.CustomField1] = othername.CustomField1;
                dr[OthernameFields.CustomField2] = othername.CustomField2;
                dr[OthernameFields.CustomField3] = othername.CustomField3;
                dr[OthernameFields.CustomField4] = othername.CustomField4;
                dr[OthernameFields.CustomField5] = othername.CustomField5;
                dr[OthernameFields.CustomField6] = othername.CustomField6;
                dr[OthernameFields.CustomField7] = othername.CustomField7;
                dr[OthernameFields.CustomField8] = othername.CustomField8;
                dr[OthernameFields.CustomField9] = othername.CustomField9;
                dr[OthernameFields.CustomField10] = othername.CustomField10;
                dr[OthernameFields.Status] = othername.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
