//
// Class	:	Bills.cs
// Author	:  	SynapseIndia © 2013
// Date		:	8/2/2013 10:11:43 AM
//


using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.List.QBSynDalTableAdapters;
using QBSynDal.Helper;

namespace QBSynDal
{
    public class Bills : AbstractDalBase
    {
        #region Constructors / Destructors
        public Bills(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                
                BeginTransaction();
                billTableAdapter _billTableAdapter = new billTableAdapter();
                _billTableAdapter.Connection = Connection;
                _billTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {

                }

                foreach (Bill bill in this.Items)
                {
                    QBSynDal.List.QBSynDal.billDataTable billTable = _billTableAdapter.GetDataByTxnID(bill.TxnID);
                    DataRow dr = null;
                    //changed for elmah
                    if (billTable.Rows.Count == 0)
                    {
                        dr = billTable.NewRow();
                        billTable.Rows.Add(dr);
                        Bind(dr, bill);
                    }
                    else
                    {
                        dr = billTable.Rows[0];
                        Bind(dr, bill);
                    }
                    _billTableAdapter.Update(billTable);
                }

                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
                
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Bill bill)
        {
            try
            {
                dr[BillFields.AmountDue] = bill.AmountDue.GetDbType();
                dr[BillFields.AmountDueInHomeCurrency] = bill.AmountDueInHomeCurrency.GetDbType();
                dr[BillFields.APAccountRef_FullName] = bill.APAccountRef_FullName;
                dr[BillFields.APAccountRef_ListID] = bill.APAccountRef_ListID;
                dr[BillFields.CurrencyRef_FullName] = bill.CurrencyRef_FullName;
                dr[BillFields.CurrencyRef_ListID] = bill.CurrencyRef_ListID;
                dr[BillFields.CustomField1] = bill.CustomField1;
                dr[BillFields.CustomField2] = bill.CustomField2;
                dr[BillFields.CustomField3] = bill.CustomField3;
                dr[BillFields.CustomField4] = bill.CustomField4;
                dr[BillFields.CustomField5] = bill.CustomField5;
                dr[BillFields.CustomField6] = bill.CustomField6;
                dr[BillFields.CustomField7] = bill.CustomField7;
                dr[BillFields.CustomField8] = bill.CustomField8;
                dr[BillFields.CustomField9] = bill.CustomField9;
                dr[BillFields.CustomField10] = bill.CustomField10;
                dr[BillFields.DueDate] = bill.DueDate.GetDbType();
                dr[BillFields.EditSequence] = bill.EditSequence;
                dr[BillFields.ExchangeRate] = bill.ExchangeRate.GetDbType();
                dr[BillFields.IsPaid] = bill.IsPaid.GetShort().GetDbType();
                dr[BillFields.IsTaxIncluded] = bill.IsTaxIncluded.GetShort().GetDbType();
                dr[BillFields.Memo] = bill.Memo;
                dr[BillFields.OpenAmount] = bill.OpenAmount.GetDbType();
                dr[BillFields.RefNumber] = bill.RefNumber;
                dr[BillFields.SalesTaxCodeRef_FullName] = bill.SalesTaxCodeRef_FullName;
                dr[BillFields.SalesTaxCodeRef_ListID] = bill.SalesTaxCodeRef_ListID;
                dr[BillFields.Status] = bill.Status;
                dr[BillFields.TermsRef_FullName] = bill.TermsRef_FullName;
                dr[BillFields.TermsRef_ListID] = bill.TermsRef_ListID;
                dr[BillFields.TimeCreated] = bill.TimeCreated;
                dr[BillFields.TimeModified] = bill.TimeModified;
                dr[BillFields.TxnDate] = bill.TxnDate.GetDbType();
                dr[BillFields.TxnID] = bill.TxnID;
                dr[BillFields.TxnNumber] = bill.TxnNumber.GetDbType();
                dr[BillFields.VendorRef_FullName] = bill.VendorRef_FullName;
                dr[BillFields.VendorRef_ListID] = bill.VendorRef_ListID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
