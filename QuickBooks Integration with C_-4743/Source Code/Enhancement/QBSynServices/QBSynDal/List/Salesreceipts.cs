//
// Class	:	Salesreceipts.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;
namespace QBSynDal
{
    public class Salesreceipts : AbstractDalBase
    {
        #region Constructors / Destructors
        public Salesreceipts(string constr)
            :base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                salesreceiptTableAdapter _salesreceiptTableAdapter = new salesreceiptTableAdapter();
                _salesreceiptTableAdapter.Connection = Connection;
                _salesreceiptTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Salesreceipt salesreceipt in this.Items)
                {
                    QBSynDal.List.QBSynDal.salesreceiptDataTable salesreceiptTable = _salesreceiptTableAdapter.GetDataByTxnID(salesreceipt.TxnID);
                    DataRow dr = null;

                    if (salesreceiptTable.Rows.Count == 0)
                    {
                        dr = salesreceiptTable.NewRow();
                        salesreceiptTable.Rows.Add(dr);
                        Bind(dr, salesreceipt);
                    }
                    else
                    {
                        dr = salesreceiptTable.Rows[0];
                        Bind(dr, salesreceipt);
                    }
                    _salesreceiptTableAdapter.Update(salesreceiptTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Method (Private)
        private void Bind(DataRow dr, Salesreceipt salesreceipt)
        {
            try
            {

                dr[SalesreceiptFields.TxnID] = salesreceipt.TxnID;
                dr[SalesreceiptFields.TimeCreated] = salesreceipt.TimeCreated;
                dr[SalesreceiptFields.TimeModified] = salesreceipt.TimeModified;
                dr[SalesreceiptFields.EditSequence] = salesreceipt.EditSequence;
                dr[SalesreceiptFields.TxnNumber] = salesreceipt.TxnNumber.GetDbType();
                dr[SalesreceiptFields.CustomerRef_ListID] = salesreceipt.CustomerRef_ListID;
                dr[SalesreceiptFields.CustomerRef_FullName] = salesreceipt.CustomerRef_FullName;
                dr[SalesreceiptFields.ClassRef_ListID] = salesreceipt.ClassRef_ListID;
                dr[SalesreceiptFields.ClassRef_FullName] = salesreceipt.ClassRef_FullName;
                dr[SalesreceiptFields.TemplateRef_ListID] = salesreceipt.TemplateRef_ListID;
                dr[SalesreceiptFields.TemplateRef_FullName] = salesreceipt.TemplateRef_FullName;
                dr[SalesreceiptFields.TxnDate] = salesreceipt.TxnDate.GetDbType();
                dr[SalesreceiptFields.RefNumber] = salesreceipt.RefNumber;
                dr[SalesreceiptFields.BillAddress_Addr1] = salesreceipt.BillAddress_Addr1;
                dr[SalesreceiptFields.BillAddress_Addr2] = salesreceipt.BillAddress_Addr2;
                dr[SalesreceiptFields.BillAddress_Addr3] = salesreceipt.BillAddress_Addr3;
                dr[SalesreceiptFields.BillAddress_Addr4] = salesreceipt.BillAddress_Addr4;
                dr[SalesreceiptFields.BillAddress_Addr5] = salesreceipt.BillAddress_Addr5;
                dr[SalesreceiptFields.BillAddress_City] = salesreceipt.BillAddress_City;
                dr[SalesreceiptFields.BillAddress_State] = salesreceipt.BillAddress_State;
                dr[SalesreceiptFields.BillAddress_PostalCode] = salesreceipt.BillAddress_PostalCode;
                dr[SalesreceiptFields.BillAddress_Country] = salesreceipt.BillAddress_Country;
                dr[SalesreceiptFields.BillAddress_Note] = salesreceipt.BillAddress_Note;
                dr[SalesreceiptFields.ShipAddress_Addr1] = salesreceipt.ShipAddress_Addr1;
                dr[SalesreceiptFields.ShipAddress_Addr2] = salesreceipt.ShipAddress_Addr2;
                dr[SalesreceiptFields.ShipAddress_Addr3] = salesreceipt.ShipAddress_Addr3;
                dr[SalesreceiptFields.ShipAddress_Addr4] = salesreceipt.ShipAddress_Addr4;
                dr[SalesreceiptFields.ShipAddress_Addr5] = salesreceipt.ShipAddress_Addr5;
                dr[SalesreceiptFields.ShipAddress_City] = salesreceipt.ShipAddress_City;
                dr[SalesreceiptFields.ShipAddress_State] = salesreceipt.ShipAddress_State;
                dr[SalesreceiptFields.ShipAddress_PostalCode] = salesreceipt.ShipAddress_PostalCode;
                dr[SalesreceiptFields.ShipAddress_Country] = salesreceipt.ShipAddress_Country;
                dr[SalesreceiptFields.ShipAddress_Note] = salesreceipt.ShipAddress_Note;
                dr[SalesreceiptFields.IsPending] = salesreceipt.IsPending.GetShort().GetDbType();
                dr[SalesreceiptFields.CheckNumber] = salesreceipt.CheckNumber;
                dr[SalesreceiptFields.PaymentMethodRef_ListID] = salesreceipt.PaymentMethodRef_ListID;
                dr[SalesreceiptFields.PaymentMethodRef_FullName] = salesreceipt.PaymentMethodRef_FullName;
                dr[SalesreceiptFields.DueDate] = salesreceipt.DueDate.GetDbType();
                dr[SalesreceiptFields.SalesRepRef_ListID] = salesreceipt.SalesRepRef_ListID;
                dr[SalesreceiptFields.SalesRepRef_FullName] = salesreceipt.SalesRepRef_FullName;
                dr[SalesreceiptFields.ShipDate] = salesreceipt.ShipDate.GetDbType();
                dr[SalesreceiptFields.ShipMethodRef_ListID] = salesreceipt.ShipMethodRef_ListID;
                dr[SalesreceiptFields.ShipMethodRef_FullName] = salesreceipt.ShipMethodRef_FullName;
                dr[SalesreceiptFields.Fob] = salesreceipt.Fob;
                dr[SalesreceiptFields.Subtotal] = salesreceipt.Subtotal.GetDbType();
                dr[SalesreceiptFields.ItemSalesTaxRef_ListID] = salesreceipt.ItemSalesTaxRef_ListID;
                dr[SalesreceiptFields.ItemSalesTaxRef_FullName] = salesreceipt.ItemSalesTaxRef_FullName;
                dr[SalesreceiptFields.SalesTaxPercentage] = salesreceipt.SalesTaxPercentage;
                dr[SalesreceiptFields.SalesTaxTotal] = salesreceipt.SalesTaxTotal.GetDbType();
                dr[SalesreceiptFields.TotalAmount] = salesreceipt.TotalAmount.GetDbType();
                dr[SalesreceiptFields.CurrencyRef_ListID] = salesreceipt.CurrencyRef_ListID;
                dr[SalesreceiptFields.CurrencyRef_FullName] = salesreceipt.CurrencyRef_FullName;
                dr[SalesreceiptFields.ExchangeRate] = salesreceipt.ExchangeRate.GetDbType();
                dr[SalesreceiptFields.TotalAmountInHomeCurrency] = salesreceipt.TotalAmountInHomeCurrency.GetDbType();
                dr[SalesreceiptFields.Memo] = salesreceipt.Memo;
                dr[SalesreceiptFields.CustomerMsgRef_ListID] = salesreceipt.CustomerMsgRef_ListID;
                dr[SalesreceiptFields.CustomerMsgRef_FullName] = salesreceipt.CustomerMsgRef_FullName;
                dr[SalesreceiptFields.IsToBePrinted] = salesreceipt.IsToBePrinted.GetShort().GetDbType();
                dr[SalesreceiptFields.IsToBeEmailed] = salesreceipt.IsToBeEmailed.GetShort().GetDbType();
                dr[SalesreceiptFields.IsTaxIncluded] = salesreceipt.IsTaxIncluded.GetShort().GetDbType();
                dr[SalesreceiptFields.CustomerSalesTaxCodeRef_ListID] = salesreceipt.CustomerSalesTaxCodeRef_ListID;
                dr[SalesreceiptFields.CustomerSalesTaxCodeRef_FullName] = salesreceipt.CustomerSalesTaxCodeRef_FullName;
                dr[SalesreceiptFields.DepositToAccountRef_ListID] = salesreceipt.DepositToAccountRef_ListID;
                dr[SalesreceiptFields.DepositToAccountRef_FullName] = salesreceipt.DepositToAccountRef_FullName;
                dr[SalesreceiptFields.Other] = salesreceipt.Other;
                dr[SalesreceiptFields.CustomField1] = salesreceipt.CustomField1;
                dr[SalesreceiptFields.CustomField2] = salesreceipt.CustomField2;
                dr[SalesreceiptFields.CustomField3] = salesreceipt.CustomField3;
                dr[SalesreceiptFields.CustomField5] = salesreceipt.CustomField5;
                dr[SalesreceiptFields.CustomField6] = salesreceipt.CustomField6;
                dr[SalesreceiptFields.CustomField7] = salesreceipt.CustomField7;
                dr[SalesreceiptFields.CustomField8] = salesreceipt.CustomField8;
                dr[SalesreceiptFields.CustomField9] = salesreceipt.CustomField9;
                dr[SalesreceiptFields.CustomField10] = salesreceipt.CustomField10;
                dr[SalesreceiptFields.Status] = salesreceipt.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
   

    }
}
