//
// Class	:	PayrollTransactionReport.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class PayrollTransactionReports : AbstractDalBase
    {
        #region Variable
        DateTime? _lastmodifieddate;
        #endregion

        #region Constructors / Destructors
        public PayrollTransactionReports(string constr, DateTime? lastmodifieddate)
            : base(constr)
        {
            _lastmodifieddate = lastmodifieddate;
        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                PayrollTransactionReportTableAdapter _PayrollTransactionReportTableAdapter = new PayrollTransactionReportTableAdapter();
                _PayrollTransactionReportTableAdapter.Connection = Connection;
                _PayrollTransactionReportTableAdapter.Transaction = Transaction;

                if (synAction == SynAction.InitiateDelete || synAction == SynAction.DoneDelete)
                {
                    if (_lastmodifieddate.HasValue)
                    {
                        _PayrollTransactionReportTableAdapter.DeleteByDate(_lastmodifieddate.Value.Date.Year, _lastmodifieddate.Value.Date.Month, _lastmodifieddate.Value.Date.Day);
                    }
                    else
                    {
                        _PayrollTransactionReportTableAdapter.DeleteAll();
                    }
                }

                QBSynDal.List.QBSynDal.PayrollTransactionReportDataTable payrolltransactionreportDataTable = new List.QBSynDal.PayrollTransactionReportDataTable();
                foreach (PayrollTransactionReport payrolltransactionreport in this.Items)
                {
                    DataRow dr = payrolltransactionreportDataTable.NewRow();
                    payrolltransactionreportDataTable.Rows.Add(dr);
                    Bind(dr, payrolltransactionreport);
                }
                _PayrollTransactionReportTableAdapter.Update(payrolltransactionreportDataTable);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Method (Private)
        private void Bind(DataRow dr, PayrollTransactionReport _PayrollTransactionReport)
        {
            try
            {
                dr[PayrollTransactionReportFields.Date] = _PayrollTransactionReport.Date.GetDbType();
                dr[PayrollTransactionReportFields.TxnNumber] = _PayrollTransactionReport.TxnNumber.GetDbType();
                dr[PayrollTransactionReportFields.RefNumber] = _PayrollTransactionReport.RefNumber;
                dr[PayrollTransactionReportFields.TxnType] = _PayrollTransactionReport.TxnType;
                dr[PayrollTransactionReportFields.Name] = _PayrollTransactionReport.Name;
                dr[PayrollTransactionReportFields.SourceName] = _PayrollTransactionReport.SourceName;
                dr[PayrollTransactionReportFields.PayrollItem] = _PayrollTransactionReport.PayrollItem;
                dr[PayrollTransactionReportFields.WageBase] = _PayrollTransactionReport.WageBase.GetDbType();
                dr[PayrollTransactionReportFields.Account] = _PayrollTransactionReport.Account;
                dr[PayrollTransactionReportFields.Class_ListID] = _PayrollTransactionReport.Class_ListID;
                dr[PayrollTransactionReportFields.Class] = _PayrollTransactionReport.Class;
                dr[PayrollTransactionReportFields.BillingStatus] = _PayrollTransactionReport.BillingStatus;
                dr[PayrollTransactionReportFields.SplitAccount_ListID] = _PayrollTransactionReport.SplitAccount_ListID;
                dr[PayrollTransactionReportFields.SplitAccount] = _PayrollTransactionReport.SplitAccount;
                dr[PayrollTransactionReportFields.Quantity] = _PayrollTransactionReport.Quanity;
                dr[PayrollTransactionReportFields.Amount] = _PayrollTransactionReport.Amount.GetDbType();
                dr[PayrollTransactionReportFields.Employee_ListID] = _PayrollTransactionReport.Employee_ListID;
                dr[PayrollTransactionReportFields.PayrollItem_ListID] = _PayrollTransactionReport.PayrollItem_ListID;
                dr[PayrollTransactionReportFields.Account_ListID] = _PayrollTransactionReport.Account_ListID;
                dr[PayrollTransactionReportFields.ModifiedTime] = _PayrollTransactionReport.ModifiedTime.GetDbType();
                dr[PayrollTransactionReportFields.AccountType] = _PayrollTransactionReport.AccounType;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
