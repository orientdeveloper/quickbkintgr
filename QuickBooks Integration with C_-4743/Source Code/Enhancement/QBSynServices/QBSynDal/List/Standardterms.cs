//
// Class	:	Standardterms.cs
// Author	:  	Synapse Software © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class Standardterms : AbstractDalBase
    {
        #region Constructors / Destructors
        public Standardterms(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                standardtermsTableAdapter _standardtermsTableAdapter = new standardtermsTableAdapter();
                _standardtermsTableAdapter.Connection = Connection;
                _standardtermsTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {

                }

                foreach (Standardterm standardterm in this.Items)
                {
                    QBSynDal.List.QBSynDal.standardtermsDataTable standardtermsTable = _standardtermsTableAdapter.GetDataByListID(standardterm.ListID);
                    DataRow dr = null;

                    if (standardtermsTable.Rows.Count == 0)
                    {
                        dr = standardtermsTable.NewRow();
                        Bind(dr, standardterm);
                        standardtermsTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = standardtermsTable.Rows[0];
                        Bind(dr, standardterm);
                    }
                    _standardtermsTableAdapter.Update(standardtermsTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Standardterm standardterm)
        {
            try
            {
                dr[StandardtermFields.ListID] = standardterm.ListID;
                dr[StandardtermFields.TimeCreated] = standardterm.TimeCreated;
                dr[StandardtermFields.TimeModified] = standardterm.TimeModified;
                dr[StandardtermFields.EditSequence] = standardterm.EditSequence;
                dr[StandardtermFields.Name] = standardterm.Name;
                dr[StandardtermFields.IsActive] = standardterm.IsActive.GetShort().GetDbType();
                dr[StandardtermFields.StdDueDays] = standardterm.StdDueDays.GetDbType();
                dr[StandardtermFields.StdDiscountDays] = standardterm.StdDiscountDays.GetDbType();
                dr[StandardtermFields.DiscountPct] = standardterm.DiscountPct;
                dr[StandardtermFields.Status] = standardterm.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
