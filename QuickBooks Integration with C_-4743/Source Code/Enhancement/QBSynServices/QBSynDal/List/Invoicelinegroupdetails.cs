//
// Class	:	Invoicelinegroupdetails.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List;
using QBSynDal.List.QBSynDalTableAdapters;
using QBSynDal.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace QBSynDal
{
    public class Invoicelinegroupdetails : AbstractDalBase
    {
        #region Constructors / Destructors
        public Invoicelinegroupdetails(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                invoicelinegroupdetailTableAdapter _invoicelinegroupdetailTableAdapter = new invoicelinegroupdetailTableAdapter();
                _invoicelinegroupdetailTableAdapter.Connection = Connection;
                _invoicelinegroupdetailTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {

                }

                QBSynDal.List.QBSynDal.invoicelinegroupdetailDataTable invoicelinegroupdetailDataTable = new List.QBSynDal.invoicelinegroupdetailDataTable();
                foreach (Invoicelinegroupdetail invoicelinegroupdetail in this.Items)
                {
                    DataRow dr = invoicelinegroupdetailDataTable.NewRow();
                    invoicelinegroupdetailDataTable.Rows.Add(dr);
                    Bind(dr, invoicelinegroupdetail);
                }
                _invoicelinegroupdetailTableAdapter.Update(invoicelinegroupdetailDataTable);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Method (Private)
        private void Bind(DataRow dr, Invoicelinegroupdetail invoicelinegroupdetail)
        {
            dr[InvoicelinegroupdetailFields.TxnLineID] = invoicelinegroupdetail.TxnLineID;
            dr[InvoicelinegroupdetailFields.ItemGroupRef_ListID] = invoicelinegroupdetail.ItemGroupRef_ListID;
            dr[InvoicelinegroupdetailFields.ItemGroupRef_FullName] = invoicelinegroupdetail.ItemGroupRef_FullName;
            dr[InvoicelinegroupdetailFields.Desc] = invoicelinegroupdetail.Desc;
            dr[InvoicelinegroupdetailFields.Quantity] = invoicelinegroupdetail.Quantity;
            dr[InvoicelinegroupdetailFields.IsPrintItemsInGroup] = invoicelinegroupdetail.IsPrintItemsInGroup.GetShort().GetDbType();
            dr[InvoicelinegroupdetailFields.TotalAmount] = invoicelinegroupdetail.TotalAmount.GetDbType();
            dr[InvoicelinegroupdetailFields.ServiceDate] = invoicelinegroupdetail.ServiceDate.GetDbType();
            dr[InvoicelinegroupdetailFields.CustomField1] = invoicelinegroupdetail.CustomField1;
            dr[InvoicelinegroupdetailFields.CustomField2] = invoicelinegroupdetail.CustomField2;
            dr[InvoicelinegroupdetailFields.CustomField3] = invoicelinegroupdetail.CustomField3;
            dr[InvoicelinegroupdetailFields.CustomField4] = invoicelinegroupdetail.CustomField4;
            dr[InvoicelinegroupdetailFields.CustomField5] = invoicelinegroupdetail.CustomField5;
            dr[InvoicelinegroupdetailFields.CustomField6] = invoicelinegroupdetail.CustomField6;
            dr[InvoicelinegroupdetailFields.CustomField7] = invoicelinegroupdetail.CustomField7;
            dr[InvoicelinegroupdetailFields.CustomField8] = invoicelinegroupdetail.CustomField8;
            dr[InvoicelinegroupdetailFields.CustomField9] = invoicelinegroupdetail.CustomField9;
            dr[InvoicelinegroupdetailFields.CustomField10] = invoicelinegroupdetail.CustomField10;
            dr[InvoicelinegroupdetailFields.Idkey] = invoicelinegroupdetail.Idkey;
            dr[InvoicelinegroupdetailFields.GroupIDKEY] = invoicelinegroupdetail.GroupIDKEY;
        }
        #endregion

    }
}
