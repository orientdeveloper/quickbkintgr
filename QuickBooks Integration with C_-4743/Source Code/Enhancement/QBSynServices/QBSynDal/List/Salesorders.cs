﻿//
// Class	:	Salesorders.cs
// Author	:  	Synapse Software © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class Salesorders : AbstractDalBase
    {
        #region Constructors / Destructors
        public Salesorders(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                salesorderTableAdapter _salesorderTableAdapter = new salesorderTableAdapter();
                _salesorderTableAdapter.Connection = Connection;
                _salesorderTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                   
                }

                foreach (Salesorder salesorder in this.Items)
                {
                    QBSynDal.List.QBSynDal.salesorderDataTable salesorderTable = _salesorderTableAdapter.GetDataByTxnID(salesorder.TxnID);
                    DataRow dr = null;

                    if (salesorderTable.Rows.Count == 0)
                    {
                        dr = salesorderTable.NewRow();
                        Bind(dr, salesorder);
                        salesorderTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = salesorderTable.Rows[0];
                        Bind(dr, salesorder);
                    }
                    _salesorderTableAdapter.Update(salesorderTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Salesorder salesorder)
        {
            try
            {
                dr[SalesorderFields.TxnID] = salesorder.TxnID;
                dr[SalesorderFields.TimeCreated] = salesorder.TimeCreated;
                dr[SalesorderFields.TimeModified] = salesorder.TimeModified;
                dr[SalesorderFields.EditSequence] = salesorder.EditSequence;
                dr[SalesorderFields.TxnNumber] = salesorder.TxnNumber.GetDbType();
                dr[SalesorderFields.CustomerRef_ListID] = salesorder.CustomerRef_ListID;
                dr[SalesorderFields.CustomerRef_FullName] = salesorder.CustomerRef_FullName;
                dr[SalesorderFields.ClassRef_ListID] = salesorder.ClassRef_ListID;
                dr[SalesorderFields.ClassRef_FullName] = salesorder.ClassRef_FullName;
                dr[SalesorderFields.TemplateRef_ListID] = salesorder.TemplateRef_ListID;
                dr[SalesorderFields.TemplateRef_FullName] = salesorder.TemplateRef_FullName;
                dr[SalesorderFields.TxnDate] = salesorder.TxnDate.GetDbType();
                dr[SalesorderFields.RefNumber] = salesorder.RefNumber;
                dr[SalesorderFields.BillAddress_Addr1] = salesorder.BillAddress_Addr1;
                dr[SalesorderFields.BillAddress_Addr2] = salesorder.BillAddress_Addr2;
                dr[SalesorderFields.BillAddress_Addr3] = salesorder.BillAddress_Addr3;
                dr[SalesorderFields.BillAddress_Addr4] = salesorder.BillAddress_Addr4;
                dr[SalesorderFields.BillAddress_Addr5] = salesorder.BillAddress_Addr5;
                dr[SalesorderFields.BillAddress_City] = salesorder.BillAddress_City;
                dr[SalesorderFields.BillAddress_State] = salesorder.BillAddress_State;
                dr[SalesorderFields.BillAddress_PostalCode] = salesorder.BillAddress_PostalCode;
                dr[SalesorderFields.BillAddress_Country] = salesorder.BillAddress_Country;
                dr[SalesorderFields.BillAddress_Note] = salesorder.BillAddress_Note;
                dr[SalesorderFields.ShipAddress_Addr1] = salesorder.ShipAddress_Addr1;
                dr[SalesorderFields.ShipAddress_Addr2] = salesorder.ShipAddress_Addr2;
                dr[SalesorderFields.ShipAddress_Addr3] = salesorder.ShipAddress_Addr3;
                dr[SalesorderFields.ShipAddress_Addr4] = salesorder.ShipAddress_Addr4;
                dr[SalesorderFields.ShipAddress_Addr5] = salesorder.ShipAddress_Addr5;
                dr[SalesorderFields.ShipAddress_City] = salesorder.ShipAddress_City;
                dr[SalesorderFields.ShipAddress_State] = salesorder.ShipAddress_State;
                dr[SalesorderFields.ShipAddress_PostalCode] = salesorder.ShipAddress_PostalCode;
                dr[SalesorderFields.ShipAddress_Country] = salesorder.ShipAddress_Country;
                dr[SalesorderFields.ShipAddress_Note] = salesorder.ShipAddress_Note;
                dr[SalesorderFields.PONumber] = salesorder.PONumber;
                dr[SalesorderFields.TermsRef_ListID] = salesorder.TermsRef_ListID;
                dr[SalesorderFields.TermsRef_FullName] = salesorder.TermsRef_FullName;
                dr[SalesorderFields.DueDate] = salesorder.DueDate.GetDbType();
                dr[SalesorderFields.SalesRepRef_ListID] = salesorder.SalesRepRef_ListID;
                dr[SalesorderFields.SalesRepRef_FullName] = salesorder.SalesRepRef_FullName;
                dr[SalesorderFields.Fob] = salesorder.Fob;
                dr[SalesorderFields.ShipDate] = salesorder.ShipDate.GetDbType();
                dr[SalesorderFields.ShipMethodRef_ListID] = salesorder.ShipMethodRef_ListID;
                dr[SalesorderFields.ShipMethodRef_FullName] = salesorder.ShipMethodRef_FullName;
                dr[SalesorderFields.Subtotal] = salesorder.Subtotal.GetDbType();
                dr[SalesorderFields.ItemSalesTaxRef_ListID] = salesorder.ItemSalesTaxRef_ListID;
                dr[SalesorderFields.ItemSalesTaxRef_FullName] = salesorder.ItemSalesTaxRef_FullName;
                dr[SalesorderFields.SalesTaxPercentage] = salesorder.SalesTaxPercentage;
                dr[SalesorderFields.SalesTaxTotal] = salesorder.SalesTaxTotal.GetDbType();
                dr[SalesorderFields.TotalAmount] = salesorder.TotalAmount.GetDbType();
                dr[SalesorderFields.CurrencyRef_ListID] = salesorder.CurrencyRef_ListID;
                dr[SalesorderFields.CurrencyRef_FullName] = salesorder.CurrencyRef_FullName;
                dr[SalesorderFields.ExchangeRate] = salesorder.ExchangeRate.GetDbType();
                dr[SalesorderFields.TotalAmountInHomeCurrency] = salesorder.TotalAmountInHomeCurrency.GetDbType();
                dr[SalesorderFields.IsManuallyClosed] = salesorder.IsManuallyClosed.GetShort().GetDbType();
                dr[SalesorderFields.IsFullyInvoiced] = salesorder.IsFullyInvoiced.GetShort().GetDbType();
                dr[SalesorderFields.Memo] = salesorder.Memo;
                dr[SalesorderFields.CustomerMsgRef_ListID] = salesorder.CustomerMsgRef_ListID;
                dr[SalesorderFields.CustomerMsgRef_FullName] = salesorder.CustomerMsgRef_FullName;
                dr[SalesorderFields.IsToBePrinted] = salesorder.IsToBePrinted.GetShort().GetDbType();
                dr[SalesorderFields.IsToBeEmailed] = salesorder.IsToBeEmailed.GetShort().GetDbType();
                dr[SalesorderFields.IsTaxIncluded] = salesorder.IsTaxIncluded.GetShort().GetDbType();
                dr[SalesorderFields.CustomerSalesTaxCodeRef_ListID] = salesorder.CustomerSalesTaxCodeRef_ListID;
                dr[SalesorderFields.CustomerSalesTaxCodeRef_FullName] = salesorder.CustomerSalesTaxCodeRef_FullName;
                dr[SalesorderFields.Other] = salesorder.Other;
                dr[SalesorderFields.LinkedTxn] = salesorder.LinkedTxn;
                dr[SalesorderFields.CustomField1] = salesorder.CustomField1;
                dr[SalesorderFields.CustomField2] = salesorder.CustomField2;
                dr[SalesorderFields.CustomField3] = salesorder.CustomField3;
                dr[SalesorderFields.CustomField4] = salesorder.CustomField4;
                dr[SalesorderFields.CustomField5] = salesorder.CustomField5;
                dr[SalesorderFields.CustomField6] = salesorder.CustomField6;
                dr[SalesorderFields.CustomField7] = salesorder.CustomField7;
                dr[SalesorderFields.CustomField8] = salesorder.CustomField8;
                dr[SalesorderFields.CustomField9] = salesorder.CustomField9;
                dr[SalesorderFields.CustomField10] = salesorder.CustomField10;
                dr[SalesorderFields.Status] = salesorder.Status;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}

