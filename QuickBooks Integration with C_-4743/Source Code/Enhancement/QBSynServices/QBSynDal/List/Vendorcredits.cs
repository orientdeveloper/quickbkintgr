//
// Class	:	Vendorcredits.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;
namespace QBSynDal
{
    public class Vendorcredits : AbstractDalBase
    {

        #region Constructors / Destructors
        public Vendorcredits(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                vendorcreditTableAdapter _vendorcreditTableAdapter = new vendorcreditTableAdapter();
                _vendorcreditTableAdapter.Connection = Connection;
                _vendorcreditTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                   
                }

                foreach (Vendorcredit vendorcredit in this.Items)
                {
                    QBSynDal.List.QBSynDal.vendorcreditDataTable vendorcreditTable = _vendorcreditTableAdapter.GetDataByTxnID(vendorcredit.TxnID);
                    DataRow dr = null;

                    if (vendorcreditTable.Rows.Count == 0)
                    {
                        dr = vendorcreditTable.NewRow();
                        Bind(dr, vendorcredit);
                        vendorcreditTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = vendorcreditTable.Rows[0];
                        Bind(dr, vendorcredit);
                    }
                    _vendorcreditTableAdapter.Update(vendorcreditTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Vendorcredit vendorcredit)
        {
            try
            {
                dr[VendorcreditFields.TxnID] = vendorcredit.TxnID;
                dr[VendorcreditFields.TimeCreated] = vendorcredit.TimeCreated;
                dr[VendorcreditFields.TimeModified] = vendorcredit.TimeModified;
                dr[VendorcreditFields.EditSequence] = vendorcredit.EditSequence;
                dr[VendorcreditFields.TxnNumber] = vendorcredit.TxnNumber.GetDbType();
                dr[VendorcreditFields.TxnDate] = vendorcredit.TxnDate.GetDbType();
                dr[VendorcreditFields.VendorRef_ListID] = vendorcredit.VendorRef_ListID;
                dr[VendorcreditFields.VendorRef_FullName] = vendorcredit.VendorRef_FullName;
                dr[VendorcreditFields.APAccountRef_ListID] = vendorcredit.APAccountRef_ListID;
                dr[VendorcreditFields.APAccountRef_FullName] = vendorcredit.APAccountRef_FullName;
                dr[VendorcreditFields.CreditAmount] = vendorcredit.CreditAmount.GetDbType();
                dr[VendorcreditFields.CurrencyRef_ListID] = vendorcredit.CurrencyRef_ListID;
                dr[VendorcreditFields.CurrencyRef_FullName] = vendorcredit.CurrencyRef_FullName;
                dr[VendorcreditFields.ExchangeRate] = vendorcredit.ExchangeRate.GetDbType();
                dr[VendorcreditFields.CreditAmountInHomeCurrency] = vendorcredit.CreditAmountInHomeCurrency.GetDbType();
                dr[VendorcreditFields.RefNumber] = vendorcredit.RefNumber;
                dr[VendorcreditFields.Memo] = vendorcredit.Memo;
                dr[VendorcreditFields.OpenAmount] = vendorcredit.OpenAmount.GetDbType();
                dr[VendorcreditFields.CustomField1] = vendorcredit.CustomField1;
                dr[VendorcreditFields.CustomField2] = vendorcredit.CustomField2;
                dr[VendorcreditFields.CustomField3] = vendorcredit.CustomField3;
                dr[VendorcreditFields.CustomField4] = vendorcredit.CustomField4;
                dr[VendorcreditFields.CustomField5] = vendorcredit.CustomField5;
                dr[VendorcreditFields.CustomField6] = vendorcredit.CustomField6;
                dr[VendorcreditFields.CustomField7] = vendorcredit.CustomField7;
                dr[VendorcreditFields.CustomField8] = vendorcredit.CustomField8;
                dr[VendorcreditFields.CustomField9] = vendorcredit.CustomField9;
                dr[VendorcreditFields.CustomField10] = vendorcredit.CustomField10;
                dr[VendorcreditFields.Status] = vendorcredit.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
