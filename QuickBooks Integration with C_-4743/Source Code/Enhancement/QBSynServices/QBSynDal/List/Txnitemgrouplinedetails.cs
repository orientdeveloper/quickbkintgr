//
// Class	:	Txnitemlinedetails.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List;
using QBSynDal.List.QBSynDalTableAdapters;
using QBSynDal.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Linq;
using System.Text;

namespace QBSynDal
{
    public class Txnitemgrouplinedetails : AbstractDalBase
    {
        #region Constructors / Destructors
        public Txnitemgrouplinedetails(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                txnitemgrouplinedetailTableAdapter _txnitemgrouplinedetailTableAdapter = new txnitemgrouplinedetailTableAdapter();
                _txnitemgrouplinedetailTableAdapter.Connection = Connection;
                _txnitemgrouplinedetailTableAdapter.Transaction = Transaction;

                QBSynDal.List.QBSynDal.txnitemgrouplinedetailDataTable txnitemgrouplinedetailDataTable = new List.QBSynDal.txnitemgrouplinedetailDataTable();
                foreach (Txnitemgrouplinedetail txnitemgrouplinedetail in this.Items)
                {
                    DataRow dr = txnitemgrouplinedetailDataTable.NewRow();
                    txnitemgrouplinedetailDataTable.Rows.Add(dr);
                    Bind(dr, txnitemgrouplinedetail);
                }
                _txnitemgrouplinedetailTableAdapter.Update(txnitemgrouplinedetailDataTable);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Txnitemgrouplinedetail txnitemgrouplinedetail)
        {
            try
            {
                dr[TxnitemgrouplinedetailFields.TxnLineID] = txnitemgrouplinedetail.TxnLineID;
                dr[TxnitemgrouplinedetailFields.ItemGroupRef_ListID] = txnitemgrouplinedetail.ItemGroupRef_ListID;
                dr[TxnitemgrouplinedetailFields.ItemGroupRef_FullName] = txnitemgrouplinedetail.ItemGroupRef_FullName;
                dr[TxnitemgrouplinedetailFields.Desc] = txnitemgrouplinedetail.Desc;
                dr[TxnitemgrouplinedetailFields.Quantity] = txnitemgrouplinedetail.Quantity;
                dr[TxnitemgrouplinedetailFields.TotalAmount] = txnitemgrouplinedetail.TotalAmount.GetDbType();
                dr[TxnitemgrouplinedetailFields.CustomField1] = txnitemgrouplinedetail.CustomField1;
                dr[TxnitemgrouplinedetailFields.CustomField2] = txnitemgrouplinedetail.CustomField2;
                dr[TxnitemgrouplinedetailFields.CustomField3] = txnitemgrouplinedetail.CustomField3;
                dr[TxnitemgrouplinedetailFields.CustomField4] = txnitemgrouplinedetail.CustomField4;
                dr[TxnitemgrouplinedetailFields.CustomField5] = txnitemgrouplinedetail.CustomField5;
                dr[TxnitemgrouplinedetailFields.CustomField6] = txnitemgrouplinedetail.CustomField6;
                dr[TxnitemgrouplinedetailFields.CustomField7] = txnitemgrouplinedetail.CustomField7;
                dr[TxnitemgrouplinedetailFields.CustomField8] = txnitemgrouplinedetail.CustomField8;
                dr[TxnitemgrouplinedetailFields.CustomField9] = txnitemgrouplinedetail.CustomField9;
                dr[TxnitemgrouplinedetailFields.CustomField10] = txnitemgrouplinedetail.CustomField10;
                dr[TxnitemgrouplinedetailFields.Idkey] = txnitemgrouplinedetail.Idkey;
                dr[TxnitemgrouplinedetailFields.GroupIDKEY] = txnitemgrouplinedetail.GroupIDKEY;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
