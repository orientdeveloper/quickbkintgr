﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using QBSynDal.List;
using QBSynDal.List.QBSynDalTableAdapters;
using QBSynDal.Helper;
using QBSynDal.Model;

namespace QBSynDal
{

    /// <summary>
    /// Note: The LinkToTxn aggregate allows you to bring in a specific line item from a transaction. 
    /// If you want to link an entire transaction and bring in all its lines, use the LinkToTxnID aggregate. 
    /// Notice that if you use LinkToTxn in the item line, you cannot use ItemRef in that line: you'll get a runtime error. 
    /// LinkToTxn brings in all the item information you need. 
    /// (You can, however, specify whatever quantity or rate that you want, or any other item line element other than ItemRef
    /// </summary>
    public class QBPushItemReceipts : AbstractPushDalBase
    {
        #region constructor

        public QBPushItemReceipts(string constr)
            : base(constr)
        {
        }

        #endregion

        #region public method
        public List<long> ItemReceiptTxnID()
        {
            try
            {
                List<long> ItemReceiptTxnIds = new List<long>();
                QBPushitemreceiptTableAdapter qbpushItemReceipttableadapter = new QBPushitemreceiptTableAdapter();
                qbpushItemReceipttableadapter.Connection = Connection;
                QBSynDal.List.QBSynDal.QBPushitemreceiptDataTable qbpushItemReceiptdatatable = qbpushItemReceipttableadapter.GetDataByPushStatus();
                if (qbpushItemReceiptdatatable != null)
                {
                    foreach (DataRow dr in qbpushItemReceiptdatatable)
                    {
                        ItemReceiptTxnIds.Add((long)dr[QBPushItemReceiptFields.TxnID]);
                    }
                }
                return ItemReceiptTxnIds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public QBPushItemReceipt QBPushItemReceipt(long TxnId)
        {
            try
            {
                string LinkedTxn = null;
                QBPushItemReceipt QBPushItemReceipts = new QBPushItemReceipt();
                QBPushitemreceiptTableAdapter qbpushItemReceipttableadapter = new QBPushitemreceiptTableAdapter();
                qbpushItemReceipttableadapter.Connection = Connection;
                QBSynDal.List.QBSynDal.QBPushitemreceiptDataTable qbpushItemReceiptdatatable = qbpushItemReceipttableadapter.GetDataByTxnID(TxnId);
                if (qbpushItemReceiptdatatable != null && qbpushItemReceiptdatatable.Count == 1)
                {
                    DataRow dr = qbpushItemReceiptdatatable.Rows[0];
                    QBPushItemReceipt _qbpushItemReceipt = new QBPushItemReceipt();
                    _qbpushItemReceipt.APAccountRef_FullName = dr[QBPushItemReceiptFields.APAccountRef_FullName].IsDBNull() ? null : dr[QBPushItemReceiptFields.APAccountRef_FullName].ToString();
                    _qbpushItemReceipt.APAccountRef_ListID = dr[QBPushItemReceiptFields.APAccountRef_ListID].IsDBNull() ? null : dr[QBPushItemReceiptFields.APAccountRef_ListID].ToString();
                    _qbpushItemReceipt.ErrorMessage = dr[QBPushItemReceiptFields.ErrorMessage].IsDBNull() ? null : dr[QBPushItemReceiptFields.ErrorMessage].ToString();
                    _qbpushItemReceipt.ExchangeRate = dr[QBPushItemReceiptFields.ExchangeRate].IsDBNull() ? null : (double?)dr[QBPushItemReceiptFields.ExchangeRate];
                    _qbpushItemReceipt.IsTaxIncluded = dr[QBPushItemReceiptFields.IsTaxIncluded].IsDBNull() ? null : (bool?)dr[QBPushItemReceiptFields.IsTaxIncluded];

                    LinkedTxn = dr[QBPushItemReceiptFields.LinkedTxn].IsDBNull() ? null : dr[QBPushItemReceiptFields.LinkedTxn].ToString();

                    _qbpushItemReceipt.Memo = dr[QBPushItemReceiptFields.Memo].IsDBNull() ? null : dr[QBPushItemReceiptFields.Memo].ToString();
                    _qbpushItemReceipt.PushDate = dr[QBPushItemReceiptFields.PushDate].IsDBNull() ? null : (DateTime?)dr[QBPushItemReceiptFields.PushDate];
                    _qbpushItemReceipt.PushStatus = dr[QBPushItemReceiptFields.PushStatus].IsDBNull() ? null : (byte?)dr[QBPushItemReceiptFields.PushStatus];
                    _qbpushItemReceipt.RefNumber = dr[QBPushItemReceiptFields.RefNumber].IsDBNull() ? null : dr[QBPushItemReceiptFields.RefNumber].ToString();
                    _qbpushItemReceipt.SalesTaxCodeRef_FullName = dr[QBPushItemReceiptFields.SalesTaxCodeRef_FullName].IsDBNull() ? null : dr[QBPushItemReceiptFields.SalesTaxCodeRef_FullName].ToString();
                    _qbpushItemReceipt.SalesTaxCodeRef_ListID = dr[QBPushItemReceiptFields.SalesTaxCodeRef_ListID].IsDBNull() ? null : dr[QBPushItemReceiptFields.SalesTaxCodeRef_ListID].ToString();
                    _qbpushItemReceipt.TxnDate = dr[QBPushItemReceiptFields.TxnDate].IsDBNull() ? null : (DateTime?)dr[QBPushItemReceiptFields.TxnDate];
                    _qbpushItemReceipt.TxnID = dr[QBPushItemReceiptFields.TxnID].IsDBNull() ? null : (long?)dr[QBPushItemReceiptFields.TxnID];
                    _qbpushItemReceipt.VendorRef_FullName = dr[QBPushItemReceiptFields.VendorRef_FullName].IsDBNull() ? null : dr[QBPushItemReceiptFields.VendorRef_FullName].ToString();
                    _qbpushItemReceipt.VendorRef_ListID = dr[QBPushItemReceiptFields.VendorRef_ListID].IsDBNull() ? null : dr[QBPushItemReceiptFields.VendorRef_ListID].ToString();
                    _qbpushItemReceipt.QBPushItemReceiptExpenseLine = null;
                    _qbpushItemReceipt.QBPushItemReceiptItemLine = GetQBPushItemReceiptlinedetail(TxnId, LinkedTxn);
                    //_qbpushItemReceipt.QBPushItemReceiptItemGroupLine = GetQBPushItemReceiptlinegroupdetail(TxnId);

                    if (_qbpushItemReceipt.QBPushItemReceiptItemLine.Count == 0)
                    {
                        _qbpushItemReceipt.LinkedTxn = LinkedTxn;
                    }
                    return _qbpushItemReceipt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool PushStatusUpdate(long TxnID, string ErrorMessage, PushStatus Status, DateTime PushDate)
        {
            try
            {
                QBPushitemreceiptTableAdapter qbpushItemReceipttableadapter = new QBPushitemreceiptTableAdapter();
                qbpushItemReceipttableadapter.Connection = Connection;
                qbpushItemReceipttableadapter.Transaction = Transaction;

                int updatedRows = qbpushItemReceipttableadapter.Update((byte?)Status, PushDate, ErrorMessage, TxnID);
                if (updatedRows > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Private Methods

        private List<QBPushItemReceiptItemLine> GetQBPushItemReceiptlinedetail(long TxnId, string LinkedId)
        {
            List<QBPushItemReceiptItemLine> QBPushItemReceiptlinedetails = new List<QBPushItemReceiptItemLine>();
            QBPushtxnitemlinedetailTableAdapter qbpushItemReceiptlinedetailtableadapter = new QBPushtxnitemlinedetailTableAdapter();
            qbpushItemReceiptlinedetailtableadapter.Connection = Connection;

            QBSynDal.List.QBSynDal.QBPushtxnitemlinedetailDataTable qbpushItemReceiptlinedetaildatatable = qbpushItemReceiptlinedetailtableadapter.GetDataByIDKEY(TxnId.ToString());
            if (qbpushItemReceiptlinedetaildatatable != null)
            {
                foreach (DataRow dr in qbpushItemReceiptlinedetaildatatable)
                {
                    QBPushItemReceiptItemLine _qbpushItemReceiptlinedetail = new QBPushItemReceiptItemLine();
                    _qbpushItemReceiptlinedetail.Amount = dr[QBPushItemReceiptItemLineFields.Amount].IsDBNull() ? null : (double?)dr[QBPushItemReceiptItemLineFields.Amount];
                    _qbpushItemReceiptlinedetail.BillableStatus = dr[QBPushItemReceiptItemLineFields.BillableStatus].IsDBNull() ? null : dr[QBPushItemReceiptItemLineFields.BillableStatus].ToString();
                    _qbpushItemReceiptlinedetail.ClassRef_FullName = dr[QBPushItemReceiptItemLineFields.ClassRef_FullName].IsDBNull() ? null : dr[QBPushItemReceiptItemLineFields.ClassRef_FullName].ToString();
                    _qbpushItemReceiptlinedetail.ClassRef_ListID = dr[QBPushItemReceiptItemLineFields.ClassRef_ListID].IsDBNull() ? null : dr[QBPushItemReceiptItemLineFields.ClassRef_ListID].ToString();
                    _qbpushItemReceiptlinedetail.Cost = dr[QBPushItemReceiptItemLineFields.Cost].IsDBNull() ? null : (double?)double.Parse(dr[QBPushItemReceiptItemLineFields.Cost].ToString());
                    _qbpushItemReceiptlinedetail.CustomerRef_FullName = dr[QBPushItemReceiptItemLineFields.CustomerRef_FullName].IsDBNull() ? null : dr[QBPushItemReceiptItemLineFields.CustomerRef_FullName].ToString();
                    _qbpushItemReceiptlinedetail.CustomerRef_ListID = dr[QBPushItemReceiptItemLineFields.CustomerRef_ListID].IsDBNull() ? null : dr[QBPushItemReceiptItemLineFields.CustomerRef_ListID].ToString();
                    _qbpushItemReceiptlinedetail.Desc = dr[QBPushItemReceiptItemLineFields.Desc].IsDBNull() ? null : dr[QBPushItemReceiptItemLineFields.Desc].ToString();
                    _qbpushItemReceiptlinedetail.InventorySiteLocationRef_FullName = dr[QBPushItemReceiptItemLineFields.InventorySiteLocationRef_FullName].IsDBNull() ? null : dr[QBPushItemReceiptItemLineFields.InventorySiteLocationRef_FullName].ToString();
                    _qbpushItemReceiptlinedetail.InventorySiteLocationRef_ListID = dr[QBPushItemReceiptItemLineFields.InventorySiteLocationRef_ListID].IsDBNull() ? null : dr[QBPushItemReceiptItemLineFields.InventorySiteLocationRef_ListID].ToString();
                    _qbpushItemReceiptlinedetail.InventorySiteRef_FullName = dr[QBPushItemReceiptItemLineFields.InventorySiteRef_FullName].IsDBNull() ? null : dr[QBPushItemReceiptItemLineFields.InventorySiteRef_FullName].ToString();
                    _qbpushItemReceiptlinedetail.InventorySiteRef_ListID = dr[QBPushItemReceiptItemLineFields.InventorySiteRef_ListID].IsDBNull() ? null : dr[QBPushItemReceiptItemLineFields.InventorySiteRef_ListID].ToString();
                    //_qbpushItemReceiptlinedetail.ItemRef_FullName = dr[QBPushItemReceiptItemLineFields.ItemRef_FullName].IsDBNull() ? null : dr[QBPushItemReceiptItemLineFields.ItemRef_FullName].ToString();
                    //_qbpushItemReceiptlinedetail.ItemRef_ListID = dr[QBPushItemReceiptItemLineFields.ItemRef_ListID].IsDBNull() ? null : dr[QBPushItemReceiptItemLineFields.ItemRef_ListID].ToString();
                    if (!string.IsNullOrEmpty(LinkedId))
                        _qbpushItemReceiptlinedetail.LinkedTxn_TxnID = LinkedId;
                    _qbpushItemReceiptlinedetail.LinkedTxn_TxnLineID = dr[QBPushItemReceiptItemLineFields.LinkedTxn_TxnLineID].IsDBNull() ? null : dr[QBPushItemReceiptItemLineFields.LinkedTxn_TxnLineID].ToString();
                    _qbpushItemReceiptlinedetail.LotNumber = dr[QBPushItemReceiptItemLineFields.LotNumber].IsDBNull() ? null : dr[QBPushItemReceiptItemLineFields.LotNumber].ToString();
                    _qbpushItemReceiptlinedetail.OverrideItemAccountRef_FullName = dr[QBPushItemReceiptItemLineFields.OverrideItemAccountRef_FullName].IsDBNull() ? null : dr[QBPushItemReceiptItemLineFields.OverrideItemAccountRef_FullName].ToString();
                    _qbpushItemReceiptlinedetail.OverrideItemAccountRef_ListID = dr[QBPushItemReceiptItemLineFields.OverrideItemAccountRef_ListID].IsDBNull() ? null : dr[QBPushItemReceiptItemLineFields.OverrideItemAccountRef_ListID].ToString();
                    _qbpushItemReceiptlinedetail.Quantity = dr[QBPushItemReceiptItemLineFields.Quantity].IsDBNull() ? null : (double?)double.Parse(dr[QBPushItemReceiptItemLineFields.Quantity].ToString());
                    _qbpushItemReceiptlinedetail.SalesTaxCodeRef_FullName = dr[QBPushItemReceiptItemLineFields.SalesTaxCodeRef_FullName].IsDBNull() ? null : dr[QBPushItemReceiptItemLineFields.SalesTaxCodeRef_FullName].ToString();
                    _qbpushItemReceiptlinedetail.SalesTaxCodeRef_ListID = dr[QBPushItemReceiptItemLineFields.SalesTaxCodeRef_ListID].IsDBNull() ? null : dr[QBPushItemReceiptItemLineFields.SalesTaxCodeRef_ListID].ToString();
                    _qbpushItemReceiptlinedetail.SerialNumber = dr[QBPushItemReceiptItemLineFields.SerialNumber].IsDBNull() ? null : dr[QBPushItemReceiptItemLineFields.SerialNumber].ToString();
                    _qbpushItemReceiptlinedetail.TaxAmount = dr[QBPushItemReceiptItemLineFields.TaxAmount].IsDBNull() ? null : (double?)double.Parse(dr[QBPushItemReceiptItemLineFields.TaxAmount].ToString());
                    _qbpushItemReceiptlinedetail.TxnLineID = dr[QBPushItemReceiptItemLineFields.TxnLineID].IsDBNull() ? null : (long?)long.Parse(dr[QBPushItemReceiptItemLineFields.TxnLineID].ToString());
                    _qbpushItemReceiptlinedetail.UnitOfMeasure = dr[QBPushItemReceiptItemLineFields.UnitOfMeasure].IsDBNull() ? null : dr[QBPushItemReceiptItemLineFields.UnitOfMeasure].ToString();
                    if (_qbpushItemReceiptlinedetail.Quantity > 0)
                        QBPushItemReceiptlinedetails.Add(_qbpushItemReceiptlinedetail);
                }
            }
            return QBPushItemReceiptlinedetails;
        }

        private List<QBPushItemReceiptItemGroupLine> GetQBPushItemReceiptlinegroupdetail(long TxnId)
        {
            List<QBPushItemReceiptItemGroupLine> QBPushItemReceiptlinegroupdetails = new List<QBPushItemReceiptItemGroupLine>();
            QBPushtxnitemgrouplinedetailTableAdapter qbpushItemReceiptlinegroupdetailtableadapter = new QBPushtxnitemgrouplinedetailTableAdapter();
            qbpushItemReceiptlinegroupdetailtableadapter.Connection = Connection;

            QBSynDal.List.QBSynDal.QBPushtxnitemgrouplinedetailDataTable qbpushItemReceiptlinegroupdetaildatatable = qbpushItemReceiptlinegroupdetailtableadapter.GetDataByIDKEY(TxnId.ToString());
            if (qbpushItemReceiptlinegroupdetaildatatable != null)
            {
                foreach (DataRow dr in qbpushItemReceiptlinegroupdetaildatatable)
                {
                    QBPushItemReceiptItemGroupLine _qbpushItemReceiptlinegroupdetail = new QBPushItemReceiptItemGroupLine();
                    _qbpushItemReceiptlinegroupdetail.Desc = dr[QBPushItemReceiptItemGroupLineFields.Desc].IsDBNull() ? null : dr[QBPushItemReceiptItemGroupLineFields.Desc].ToString();
                    _qbpushItemReceiptlinegroupdetail.InventorySiteLocationRef_FullName = dr[QBPushItemReceiptItemGroupLineFields.InventorySiteLocationRef_FullName].IsDBNull() ? null : dr[QBPushItemReceiptItemGroupLineFields.InventorySiteLocationRef_FullName].ToString();
                    _qbpushItemReceiptlinegroupdetail.InventorySiteLocationRef_ListID = dr[QBPushItemReceiptItemGroupLineFields.InventorySiteLocationRef_ListID].IsDBNull() ? null : dr[QBPushItemReceiptItemGroupLineFields.InventorySiteLocationRef_ListID].ToString();
                    _qbpushItemReceiptlinegroupdetail.InventorySiteRef_FullName = dr[QBPushItemReceiptItemGroupLineFields.InventorySiteRef_FullName].IsDBNull() ? null : dr[QBPushItemReceiptItemGroupLineFields.InventorySiteRef_FullName].ToString();
                    _qbpushItemReceiptlinegroupdetail.InventorySiteRef_ListID = dr[QBPushItemReceiptItemGroupLineFields.InventorySiteRef_ListID].IsDBNull() ? null : dr[QBPushItemReceiptItemGroupLineFields.InventorySiteRef_ListID].ToString();
                    _qbpushItemReceiptlinegroupdetail.ItemGroupRef_FullName = dr[QBPushItemReceiptItemGroupLineFields.ItemGroupRef_FullName].IsDBNull() ? null : dr[QBPushItemReceiptItemGroupLineFields.ItemGroupRef_FullName].ToString();
                    _qbpushItemReceiptlinegroupdetail.ItemGroupRef_ListID = dr[QBPushItemReceiptItemGroupLineFields.ItemGroupRef_ListID].IsDBNull() ? null : dr[QBPushItemReceiptItemGroupLineFields.ItemGroupRef_ListID].ToString();
                    _qbpushItemReceiptlinegroupdetail.Quantity = dr[QBPushItemReceiptItemGroupLineFields.Quantity].IsDBNull() ? null : (double?)double.Parse(dr[QBPushItemReceiptItemGroupLineFields.Quantity].ToString());
                    _qbpushItemReceiptlinegroupdetail.TxnLineID = dr[QBPushItemReceiptItemGroupLineFields.TxnLineID].IsDBNull() ? null : (long?)long.Parse(dr[QBPushItemReceiptItemGroupLineFields.TxnLineID].ToString());
                    _qbpushItemReceiptlinegroupdetail.UnitOfMeasure = dr[QBPushItemReceiptItemGroupLineFields.UnitOfMeasure].IsDBNull() ? null : dr[QBPushItemReceiptItemGroupLineFields.UnitOfMeasure].ToString();
                    QBPushItemReceiptlinegroupdetails.Add(_qbpushItemReceiptlinegroupdetail);
                }
            }
            return QBPushItemReceiptlinegroupdetails;
        }

        #endregion
    }
}
