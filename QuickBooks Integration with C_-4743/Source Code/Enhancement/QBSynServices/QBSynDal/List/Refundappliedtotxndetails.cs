//
// Class	:	Refundappliedtotxndetails.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List;
using QBSynDal.List.QBSynDalTableAdapters;
using QBSynDal.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Linq;
using System.Text;

namespace QBSynDal
{
    public class Refundappliedtotxndetails : AbstractDalBase
    {
        #region Constructors / Destructors
        public Refundappliedtotxndetails(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                refundappliedtotxndetailTableAdapter _refundappliedtotxndetailTableAdapter = new refundappliedtotxndetailTableAdapter();
                _refundappliedtotxndetailTableAdapter.Connection = Connection;
                _refundappliedtotxndetailTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                QBSynDal.List.QBSynDal.refundappliedtotxndetailDataTable refundappliedtotxndetailDataTable = new List.QBSynDal.refundappliedtotxndetailDataTable();
                foreach (Refundappliedtotxndetail refundappliedtotxndetail in this.Items)
                {
                    DataRow dr = refundappliedtotxndetailDataTable.NewRow();
                    refundappliedtotxndetailDataTable.Rows.Add(dr);
                    Bind(dr, refundappliedtotxndetail);
                }
                _refundappliedtotxndetailTableAdapter.Update(refundappliedtotxndetailDataTable);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Refundappliedtotxndetail refundappliedtotxndetail)
        {
            try
            {
                dr[RefundappliedtotxndetailFields.TxnID] = refundappliedtotxndetail.TxnID;
                dr[RefundappliedtotxndetailFields.TxnType] = refundappliedtotxndetail.TxnType;
                dr[RefundappliedtotxndetailFields.TxnDate] = refundappliedtotxndetail.TxnDate.GetDbType();
                dr[RefundappliedtotxndetailFields.RefNumber] = refundappliedtotxndetail.RefNumber;
                dr[RefundappliedtotxndetailFields.CreditRemaining] = refundappliedtotxndetail.CreditRemaining.GetDbType();
                dr[RefundappliedtotxndetailFields.RefundAmount] = refundappliedtotxndetail.RefundAmount.GetDbType();
                dr[RefundappliedtotxndetailFields.CustomField1] = refundappliedtotxndetail.CustomField1;
                dr[RefundappliedtotxndetailFields.CustomField2] = refundappliedtotxndetail.CustomField2;
                dr[RefundappliedtotxndetailFields.CustomField3] = refundappliedtotxndetail.CustomField3;
                dr[RefundappliedtotxndetailFields.CustomField4] = refundappliedtotxndetail.CustomField4;
                dr[RefundappliedtotxndetailFields.CustomField5] = refundappliedtotxndetail.CustomField5;
                dr[RefundappliedtotxndetailFields.CustomField6] = refundappliedtotxndetail.CustomField6;
                dr[RefundappliedtotxndetailFields.CustomField7] = refundappliedtotxndetail.CustomField7;
                dr[RefundappliedtotxndetailFields.CustomField8] = refundappliedtotxndetail.CustomField8;
                dr[RefundappliedtotxndetailFields.CustomField9] = refundappliedtotxndetail.CustomField9;
                dr[RefundappliedtotxndetailFields.CustomField10] = refundappliedtotxndetail.CustomField10;
                dr[RefundappliedtotxndetailFields.Idkey] = refundappliedtotxndetail.Idkey;
                dr[RefundappliedtotxndetailFields.GroupIDKEY] = refundappliedtotxndetail.GroupIDKEY;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
