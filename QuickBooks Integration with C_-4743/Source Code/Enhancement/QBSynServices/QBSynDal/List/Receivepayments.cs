//
// Class	:	Receivepayments.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:44 AM
//
using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;
namespace QBSynDal
{
    public class Receivepayments :AbstractDalBase
    {
        #region Constructors / Destructors
        public Receivepayments(string constr)
            :base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                receivepaymentTableAdapter _receivepaymentTableAdapter = new receivepaymentTableAdapter();
                _receivepaymentTableAdapter.Connection = Connection;
                _receivepaymentTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Receivepayment receivepayment in this.Items)
                {
                    QBSynDal.List.QBSynDal.receivepaymentDataTable receivepaymentTable = _receivepaymentTableAdapter.GetDataByTxnID(receivepayment.TxnID);
                    DataRow dr = null;

                    if (receivepaymentTable.Rows.Count == 0)
                    {
                        dr = receivepaymentTable.NewRow();
                        receivepaymentTable.Rows.Add(dr);
                        Bind(dr, receivepayment);
                    }
                    else
                    {
                        dr = receivepaymentTable.Rows[0];
                        Bind(dr, receivepayment);
                    }
                    _receivepaymentTableAdapter.Update(receivepaymentTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Method (Private)
        private void Bind(DataRow dr, Receivepayment receivepayment)
        {
            try
            {
                dr[ReceivepaymentFields.TxnID] = receivepayment.TxnID;
                dr[ReceivepaymentFields.TimeCreated] = receivepayment.TimeCreated;
                dr[ReceivepaymentFields.TimeModified] = receivepayment.TimeModified;
                dr[ReceivepaymentFields.EditSequence] = receivepayment.EditSequence;
                dr[ReceivepaymentFields.TxnNumber] = receivepayment.TxnNumber.GetDbType();
                dr[ReceivepaymentFields.CustomerRef_ListID] = receivepayment.CustomerRef_ListID;
                dr[ReceivepaymentFields.CustomerRef_FullName] = receivepayment.CustomerRef_FullName;
                dr[ReceivepaymentFields.ARAccountRef_ListID] = receivepayment.ARAccountRef_ListID;
                dr[ReceivepaymentFields.ARAccountRef_FullName] = receivepayment.ARAccountRef_FullName;
                dr[ReceivepaymentFields.TxnDate] = receivepayment.TxnDate.GetDbType();
                dr[ReceivepaymentFields.RefNumber] = receivepayment.RefNumber;
                dr[ReceivepaymentFields.TotalAmount] = receivepayment.TotalAmount.GetDbType();
                dr[ReceivepaymentFields.CurrencyRef_ListID] = receivepayment.CurrencyRef_ListID;
                dr[ReceivepaymentFields.CurrencyRef_FullName] = receivepayment.CurrencyRef_FullName;
                dr[ReceivepaymentFields.ExchangeRate] = receivepayment.ExchangeRate.GetDbType();
                dr[ReceivepaymentFields.TotalAmountInHomeCurrency] = receivepayment.TotalAmountInHomeCurrency.GetDbType();
                dr[ReceivepaymentFields.PaymentMethodRef_ListID] = receivepayment.PaymentMethodRef_ListID;
                dr[ReceivepaymentFields.PaymentMethodRef_FullName] = receivepayment.PaymentMethodRef_FullName;
                dr[ReceivepaymentFields.Memo] = receivepayment.Memo;
                dr[ReceivepaymentFields.DepositToAccountRef_ListID] = receivepayment.DepositToAccountRef_ListID;
                dr[ReceivepaymentFields.DepositToAccountRef_FullName] = receivepayment.DepositToAccountRef_FullName;
                dr[ReceivepaymentFields.UnusedPayment] = receivepayment.UnusedPayment.GetDbType();
                dr[ReceivepaymentFields.UnusedCredits] = receivepayment.UnusedCredits.GetDbType();
                dr[ReceivepaymentFields.CustomField1] = receivepayment.CustomField1;
                dr[ReceivepaymentFields.CustomField2] = receivepayment.CustomField2;
                dr[ReceivepaymentFields.CustomField3] = receivepayment.CustomField3;
                dr[ReceivepaymentFields.CustomField4] = receivepayment.CustomField4;
                dr[ReceivepaymentFields.CustomField5] = receivepayment.CustomField5;
                dr[ReceivepaymentFields.CustomField6] = receivepayment.CustomField6;
                dr[ReceivepaymentFields.CustomField7] = receivepayment.CustomField7;
                dr[ReceivepaymentFields.CustomField8] = receivepayment.CustomField8;
                dr[ReceivepaymentFields.CustomField9] = receivepayment.CustomField9;
                dr[ReceivepaymentFields.CustomField10] = receivepayment.CustomField10;
                dr[ReceivepaymentFields.Status] = receivepayment.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
