//
// Class	:	Creditcardcredits.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:45 AM
//
using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class Creditcardcredits : AbstractDalBase
    {
        #region Constructors / Destructors
        public Creditcardcredits(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                creditcardcreditTableAdapter _creditcardcreditTableAdapter = new creditcardcreditTableAdapter();
                _creditcardcreditTableAdapter.Connection = Connection;
                _creditcardcreditTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Creditcardcredit creditcardcredit in this.Items)
                {
                    QBSynDal.List.QBSynDal.creditcardcreditDataTable creditcardcreditTable = _creditcardcreditTableAdapter.GetDataByTxnID(creditcardcredit.TxnID);
                    DataRow dr = null;

                    if (creditcardcreditTable.Rows.Count == 0)
                    {
                        dr = creditcardcreditTable.NewRow();
                        Bind(dr, creditcardcredit);
                        creditcardcreditTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = creditcardcreditTable.Rows[0];
                        Bind(dr, creditcardcredit);
                    }
                    _creditcardcreditTableAdapter.Update(creditcardcreditTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Creditcardcredit creditcardcredit)
        {
            try
            {
                dr[CreditcardcreditFields.TxnID] = creditcardcredit.TxnID;
                dr[CreditcardcreditFields.TimeCreated] = creditcardcredit.TimeCreated;
                dr[CreditcardcreditFields.TimeModified] = creditcardcredit.TimeModified;
                dr[CreditcardcreditFields.EditSequence] = creditcardcredit.EditSequence;
                dr[CreditcardcreditFields.TxnNumber] = creditcardcredit.TxnNumber.GetDbType();
                dr[CreditcardcreditFields.AccountRef_ListID] = creditcardcredit.AccountRef_ListID;
                dr[CreditcardcreditFields.AccountRef_FullName] = creditcardcredit.AccountRef_FullName;
                dr[CreditcardcreditFields.PayeeEntityRef_ListID] = creditcardcredit.PayeeEntityRef_ListID;
                dr[CreditcardcreditFields.PayeeEntityRef_FullName] = creditcardcredit.PayeeEntityRef_FullName;
                dr[CreditcardcreditFields.TxnDate] = creditcardcredit.TxnDate.GetDbType();
                dr[CreditcardcreditFields.Amount] = creditcardcredit.Amount.GetDbType();
                dr[CreditcardcreditFields.CurrencyRef_FullName] = creditcardcredit.CurrencyRef_FullName;
                dr[CreditcardcreditFields.CurrencyRef_ListID] = creditcardcredit.CurrencyRef_ListID;
                dr[CreditcardcreditFields.ExchangeRate] = creditcardcredit.ExchangeRate.GetDbType();
                dr[CreditcardcreditFields.AmountInHomeCurrency] = creditcardcredit.AmountInHomeCurrency.GetDbType();
                dr[CreditcardcreditFields.RefNumber] = creditcardcredit.RefNumber;
                dr[CreditcardcreditFields.Memo] = creditcardcredit.Memo;
                dr[CreditcardcreditFields.CustomField1] = creditcardcredit.CustomField1;
                dr[CreditcardcreditFields.CustomField2] = creditcardcredit.CustomField2;
                dr[CreditcardcreditFields.CustomField3] = creditcardcredit.CustomField3;
                dr[CreditcardcreditFields.CustomField4] = creditcardcredit.CustomField4;
                dr[CreditcardcreditFields.CustomField5] = creditcardcredit.CustomField5;
                dr[CreditcardcreditFields.CustomField6] = creditcardcredit.CustomField6;
                dr[CreditcardcreditFields.CustomField7] = creditcardcredit.CustomField7;
                dr[CreditcardcreditFields.CustomField8] = creditcardcredit.CustomField8;
                dr[CreditcardcreditFields.CustomField9] = creditcardcredit.CustomField9;
                dr[CreditcardcreditFields.CustomField10] = creditcardcredit.CustomField10;
                dr[CreditcardcreditFields.Status] = creditcardcredit.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
