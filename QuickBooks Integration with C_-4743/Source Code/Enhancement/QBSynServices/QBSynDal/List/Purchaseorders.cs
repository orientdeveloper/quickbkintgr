//
// Class	:	Purchaseorders.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;
namespace QBSynDal
{
    public class Purchaseorders : AbstractDalBase
    {
        #region Constructors / Destructors
        public Purchaseorders(string constr)
            :base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                purchaseorderTableAdapter _purchaseorderTableAdapter = new purchaseorderTableAdapter();
                _purchaseorderTableAdapter.Connection = Connection;
                _purchaseorderTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Purchaseorder purchaseorder in this.Items)
                {
                    QBSynDal.List.QBSynDal.purchaseorderDataTable purchaseorderTable = _purchaseorderTableAdapter.GetDataByTxnID(purchaseorder.TxnID);
                    DataRow dr = null;

                    if (purchaseorderTable.Rows.Count == 0)
                    {
                        dr = purchaseorderTable.NewRow();
                        purchaseorderTable.Rows.Add(dr);
                        Bind(dr, purchaseorder);
                    }
                    else
                    {
                        dr = purchaseorderTable.Rows[0];
                        Bind(dr, purchaseorder);
                    }
                    _purchaseorderTableAdapter.Update(purchaseorderTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Method (Private)
        private void Bind(DataRow dr, Purchaseorder purchaseorder)
        {
            try
            {
                dr[PurchaseorderFields.TxnID] = purchaseorder.TxnID;
                dr[PurchaseorderFields.TimeCreated] = purchaseorder.TimeCreated;
                dr[PurchaseorderFields.TimeModified] = purchaseorder.TimeModified;
                dr[PurchaseorderFields.EditSequence] = purchaseorder.EditSequence;
                dr[PurchaseorderFields.TxnNumber] = purchaseorder.TxnNumber.GetDbType();
                dr[PurchaseorderFields.VendorRef_ListID] = purchaseorder.VendorRef_ListID;
                dr[PurchaseorderFields.VendorRef_FullName] = purchaseorder.VendorRef_FullName;
                dr[PurchaseorderFields.ClassRef_ListID] = purchaseorder.ClassRef_ListID;
                dr[PurchaseorderFields.ClassRef_FullName] = purchaseorder.ClassRef_FullName;
                dr[PurchaseorderFields.InventorySiteRef_ListID] = purchaseorder.InventorySiteRef_ListID;
                dr[PurchaseorderFields.InventorySiteRef_FullName] = purchaseorder.InventorySiteRef_FullName;
                dr[PurchaseorderFields.ShipToEntityRef_ListID] = purchaseorder.ShipToEntityRef_ListID;
                dr[PurchaseorderFields.ShipToEntityRef_FullName] = purchaseorder.ShipToEntityRef_FullName;
                dr[PurchaseorderFields.TemplateRef_ListID] = purchaseorder.TemplateRef_ListID;
                dr[PurchaseorderFields.TemplateRef_FullName] = purchaseorder.TemplateRef_FullName;
                dr[PurchaseorderFields.TxnDate] = purchaseorder.TxnDate.GetDbType();
                dr[PurchaseorderFields.RefNumber] = purchaseorder.RefNumber;
                dr[PurchaseorderFields.VendorAddress_Addr1] = purchaseorder.VendorAddress_Addr1;
                dr[PurchaseorderFields.VendorAddress_Addr2] = purchaseorder.VendorAddress_Addr2;
                dr[PurchaseorderFields.VendorAddress_Addr3] = purchaseorder.VendorAddress_Addr3;
                dr[PurchaseorderFields.VendorAddress_Addr4] = purchaseorder.VendorAddress_Addr4;
                dr[PurchaseorderFields.VendorAddress_Addr5] = purchaseorder.VendorAddress_Addr5;
                dr[PurchaseorderFields.VendorAddress_City] = purchaseorder.VendorAddress_City;
                dr[PurchaseorderFields.VendorAddress_State] = purchaseorder.VendorAddress_State;
                dr[PurchaseorderFields.VendorAddress_PostalCode] = purchaseorder.VendorAddress_PostalCode;
                dr[PurchaseorderFields.VendorAddress_Country] = purchaseorder.VendorAddress_Country;
                dr[PurchaseorderFields.VendorAddress_Note] = purchaseorder.VendorAddress_Note;
                dr[PurchaseorderFields.ShipAddress_Addr1] = purchaseorder.ShipAddress_Addr1;
                dr[PurchaseorderFields.ShipAddress_Addr2] = purchaseorder.ShipAddress_Addr2;
                dr[PurchaseorderFields.ShipAddress_Addr3] = purchaseorder.ShipAddress_Addr3;
                dr[PurchaseorderFields.ShipAddress_Addr4] = purchaseorder.ShipAddress_Addr4;
                dr[PurchaseorderFields.ShipAddress_Addr5] = purchaseorder.ShipAddress_Addr5;
                dr[PurchaseorderFields.ShipAddress_City] = purchaseorder.ShipAddress_City;
                dr[PurchaseorderFields.ShipAddress_State] = purchaseorder.ShipAddress_State;
                dr[PurchaseorderFields.ShipAddress_PostalCode] = purchaseorder.ShipAddress_PostalCode;
                dr[PurchaseorderFields.ShipAddress_Country] = purchaseorder.ShipAddress_Country;
                dr[PurchaseorderFields.ShipAddress_Note] = purchaseorder.ShipAddress_Note;
                dr[PurchaseorderFields.TermsRef_ListID] = purchaseorder.TermsRef_ListID;
                dr[PurchaseorderFields.TermsRef_FullName] = purchaseorder.TermsRef_FullName;
                dr[PurchaseorderFields.DueDate] = purchaseorder.DueDate.GetDbType();
                dr[PurchaseorderFields.ExpectedDate] = purchaseorder.ExpectedDate.GetDbType();
                dr[PurchaseorderFields.ShipMethodRef_ListID] = purchaseorder.ShipMethodRef_ListID;
                dr[PurchaseorderFields.ShipMethodRef_FullName] = purchaseorder.ShipMethodRef_FullName;
                dr[PurchaseorderFields.Fob] = purchaseorder.Fob;
                dr[PurchaseorderFields.TotalAmount] = purchaseorder.TotalAmount.GetDbType();
                dr[PurchaseorderFields.CurrencyRef_ListID] = purchaseorder.CurrencyRef_ListID;
                dr[PurchaseorderFields.CurrencyRef_FullName] = purchaseorder.CurrencyRef_FullName;
                dr[PurchaseorderFields.ExchangeRate] = purchaseorder.ExchangeRate.GetDbType();
                dr[PurchaseorderFields.TotalAmountInHomeCurrency] = purchaseorder.TotalAmountInHomeCurrency.GetDbType();
                dr[PurchaseorderFields.Memo] = purchaseorder.Memo;
                dr[PurchaseorderFields.VendorMsg] = purchaseorder.VendorMsg;
                dr[PurchaseorderFields.IsToBePrinted] = purchaseorder.IsToBePrinted.GetShort().GetDbType();
                dr[PurchaseorderFields.IsToBeEmailed] = purchaseorder.IsToBeEmailed.GetShort().GetDbType();
                dr[PurchaseorderFields.IsTaxIncluded] = purchaseorder.IsTaxIncluded.GetShort().GetDbType();
                dr[PurchaseorderFields.SalesTaxCodeRef_ListID] = purchaseorder.SalesTaxCodeRef_ListID;
                dr[PurchaseorderFields.SalesTaxCodeRef_FullName] = purchaseorder.SalesTaxCodeRef_FullName;
                dr[PurchaseorderFields.Other1] = purchaseorder.Other1;
                dr[PurchaseorderFields.Other2] = purchaseorder.Other2;
                dr[PurchaseorderFields.IsManuallyClosed] = purchaseorder.IsManuallyClosed;
                dr[PurchaseorderFields.IsFullyReceived] = purchaseorder.IsFullyReceived;
                dr[PurchaseorderFields.CustomField1] = purchaseorder.CustomField1;
                dr[PurchaseorderFields.CustomField2] = purchaseorder.CustomField2;
                dr[PurchaseorderFields.CustomField3] = purchaseorder.CustomField3;
                dr[PurchaseorderFields.CustomField4] = purchaseorder.CustomField4;
                dr[PurchaseorderFields.CustomField5] = purchaseorder.CustomField5;
                dr[PurchaseorderFields.CustomField6] = purchaseorder.CustomField6;
                dr[PurchaseorderFields.CustomField7] = purchaseorder.CustomField7;
                dr[PurchaseorderFields.CustomField8] = purchaseorder.CustomField8;
                dr[PurchaseorderFields.CustomField9] = purchaseorder.CustomField9;
                dr[PurchaseorderFields.CustomField10] = purchaseorder.CustomField10;
                dr[PurchaseorderFields.Status] = purchaseorder.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
