//
// Class	:	Accounts.cs
// Author	:  	Synapse Software © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class Currencies : AbstractDalBase
    {
        #region Constructors / Destructors
        public Currencies(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                currencyTableAdapter _currencyTableAdapter = new currencyTableAdapter();
                _currencyTableAdapter.Connection = Connection;
                _currencyTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Currency currency in this.Items)
                {
                    QBSynDal.List.QBSynDal.currencyDataTable currencyTable = _currencyTableAdapter.GetDataByListID(currency.ListID);
                    DataRow dr = null;

                    if (currencyTable.Rows.Count == 0)
                    {
                        dr = currencyTable.NewRow();
                        Bind(dr, currency);
                        currencyTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = currencyTable.Rows[0];
                        Bind(dr, currency);
                    }
                    _currencyTableAdapter.Update(currencyTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Currency currency)
        {
            try
            {
                dr[CurrencyFields.ListID] = currency.ListID;
                dr[CurrencyFields.TimeCreated] = currency.TimeCreated;
                dr[CurrencyFields.TimeModified] = currency.TimeModified;
                dr[CurrencyFields.ThousandSeparator] = currency.ThousandSeparator;
                dr[CurrencyFields.ThousandSeparatorGrouping] = currency.ThousandSeparatorGrouping;
                dr[CurrencyFields.Name] = currency.Name;
                dr[CurrencyFields.IsUserDefinedCurrency] = currency.IsUserDefinedCurrency.GetShort().GetDbType();
                dr[CurrencyFields.ExchangeRate] = currency.ExchangeRate.GetDbType();
                dr[CurrencyFields.IsActive] = currency.IsActive.GetShort().GetDbType();
                dr[CurrencyFields.DecimalSeparator] = currency.DecimalSeparator;
                dr[CurrencyFields.AsOfDate] = currency.AsOfDate.GetDbType();
                dr[CurrencyFields.CurrencyCode] = currency.CurrencyCode;
                dr[CurrencyFields.DecimalPlaces] = currency.DecimalPlaces;
                dr[CurrencyFields.EditSequence] = currency.EditSequence;
                dr[CurrencyFields.Status] = currency.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
