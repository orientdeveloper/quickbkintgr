//
// Class	:	Journaldebitlinedetails.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List;
using QBSynDal.List.QBSynDalTableAdapters;
using QBSynDal.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Linq;
using System.Text;

namespace QBSynDal
{
    public class Journaldebitlinedetails : AbstractDalBase
    {
        #region Constructors / Destructors
        public Journaldebitlinedetails(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                journaldebitlinedetailTableAdapter _journaldebitlinedetailTableAdapter = new journaldebitlinedetailTableAdapter();
                _journaldebitlinedetailTableAdapter.Connection = Connection;
                _journaldebitlinedetailTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                QBSynDal.List.QBSynDal.journaldebitlinedetailDataTable journaldebitlinedetailDataTable = new List.QBSynDal.journaldebitlinedetailDataTable();
                foreach (Journaldebitlinedetail journaldebitlinedetail in this.Items)
                {
                    DataRow dr = journaldebitlinedetailDataTable.NewRow();
                    journaldebitlinedetailDataTable.Rows.Add(dr);
                    Bind(dr, journaldebitlinedetail);
                }
                _journaldebitlinedetailTableAdapter.Update(journaldebitlinedetailDataTable);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Journaldebitlinedetail journaldebitlinedetail)
        {
            try
            {
                dr[JournaldebitlinedetailFields.TxnLineID] = journaldebitlinedetail.TxnLineID;
                dr[JournaldebitlinedetailFields.AccountRef_ListID] = journaldebitlinedetail.AccountRef_ListID;
                dr[JournaldebitlinedetailFields.AccountRef_FullName] = journaldebitlinedetail.AccountRef_FullName;
                dr[JournaldebitlinedetailFields.Amount] = journaldebitlinedetail.Amount.GetDbType();
                dr[JournaldebitlinedetailFields.Memo] = journaldebitlinedetail.Memo;
                dr[JournaldebitlinedetailFields.EntityRef_ListID] = journaldebitlinedetail.EntityRef_ListID;
                dr[JournaldebitlinedetailFields.EntityRef_FullName] = journaldebitlinedetail.EntityRef_FullName;
                dr[JournaldebitlinedetailFields.ClassRef_ListID] = journaldebitlinedetail.ClassRef_ListID;
                dr[JournaldebitlinedetailFields.ClassRef_FullName] = journaldebitlinedetail.ClassRef_FullName;
                dr[JournaldebitlinedetailFields.ItemSalesTaxRef_ListID] = journaldebitlinedetail.ItemSalesTaxRef_ListID;
                dr[JournaldebitlinedetailFields.ItemSalesTaxRef_FullName] = journaldebitlinedetail.ItemSalesTaxRef_FullName;
                dr[JournaldebitlinedetailFields.BillableStatus] = journaldebitlinedetail.BillableStatus;
                dr[JournaldebitlinedetailFields.CustomField1] = journaldebitlinedetail.CustomField1;
                dr[JournaldebitlinedetailFields.CustomField2] = journaldebitlinedetail.CustomField2;
                dr[JournaldebitlinedetailFields.CustomField3] = journaldebitlinedetail.CustomField3;
                dr[JournaldebitlinedetailFields.CustomField4] = journaldebitlinedetail.CustomField4;
                dr[JournaldebitlinedetailFields.CustomField5] = journaldebitlinedetail.CustomField5;
                dr[JournaldebitlinedetailFields.CustomField6] = journaldebitlinedetail.CustomField6;
                dr[JournaldebitlinedetailFields.CustomField7] = journaldebitlinedetail.CustomField7;
                dr[JournaldebitlinedetailFields.CustomField8] = journaldebitlinedetail.CustomField8;
                dr[JournaldebitlinedetailFields.CustomField9] = journaldebitlinedetail.CustomField9;
                dr[JournaldebitlinedetailFields.CustomField10] = journaldebitlinedetail.CustomField10;
                dr[JournaldebitlinedetailFields.Idkey] = journaldebitlinedetail.Idkey;
                dr[JournaldebitlinedetailFields.GroupIDKEY] = journaldebitlinedetail.GroupIDKEY;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
