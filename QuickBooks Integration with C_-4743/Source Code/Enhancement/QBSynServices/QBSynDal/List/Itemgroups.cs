//
// Class	:	Itemgroups.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:50 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;
using QBSynDal;
namespace QBSynDal
{
    public class Itemgroups : AbstractDalBase
	{
		
        #region Constructors / Destructors 
        public Itemgroups(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                itemgroupTableAdapter _itemgroupTableAdapter = new itemgroupTableAdapter();
                _itemgroupTableAdapter.Connection = Connection;
                _itemgroupTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                   
                }

                foreach (Itemgroup itemgroup in this.Items)
                {
                    QBSynDal.List.QBSynDal.itemgroupDataTable itemgroupTable = _itemgroupTableAdapter.GetDataByListID(itemgroup.ListID);
                    DataRow dr = null;

                    if (itemgroupTable.Rows.Count == 0)
                    {
                        dr = itemgroupTable.NewRow();
                        Bind(dr, itemgroup);
                        itemgroupTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = itemgroupTable.Rows[0];
                        Bind(dr, itemgroup);
                    }
                    _itemgroupTableAdapter.Update(itemgroupTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion
		
		#region Methods (Private)
        private void Bind(DataRow dr, Itemgroup itemgroup)
        {
            try
            {
                dr[ItemgroupFields.ListID] = itemgroup.ListID;
                dr[ItemgroupFields.TimeCreated] = itemgroup.TimeCreated;
                dr[ItemgroupFields.TimeModified] = itemgroup.TimeModified;
                dr[ItemgroupFields.EditSequence] = itemgroup.EditSequence;
                dr[ItemgroupFields.Name] = itemgroup.Name;
                dr[ItemgroupFields.IsActive] = itemgroup.IsActive.GetShort().GetDbType();
                dr[ItemgroupFields.ItemDesc] = itemgroup.ItemDesc;
                dr[ItemgroupFields.IsPrintItemsInGroup] = itemgroup.IsPrintItemsInGroup.GetShort().GetDbType();
                dr[ItemgroupFields.SpecialItemType] = itemgroup.SpecialItemType;
                dr[ItemgroupFields.CustomField1] = itemgroup.CustomField1;
                dr[ItemgroupFields.CustomField2] = itemgroup.CustomField2;
                dr[ItemgroupFields.CustomField3] = itemgroup.CustomField3;
                dr[ItemgroupFields.CustomField4] = itemgroup.CustomField4;
                dr[ItemgroupFields.CustomField5] = itemgroup.CustomField5;
                dr[ItemgroupFields.CustomField6] = itemgroup.CustomField6;
                dr[ItemgroupFields.CustomField7] = itemgroup.CustomField7;
                dr[ItemgroupFields.CustomField8] = itemgroup.CustomField8;
                dr[ItemgroupFields.CustomField9] = itemgroup.CustomField9;
                dr[ItemgroupFields.CustomField10] = itemgroup.CustomField10;
                dr[ItemgroupFields.Status] = itemgroup.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
		
	}
}
