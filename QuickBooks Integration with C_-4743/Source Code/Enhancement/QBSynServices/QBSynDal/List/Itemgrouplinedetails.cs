//
// Class	:	Arrefundcreditcards.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List;
using QBSynDal.List.QBSynDalTableAdapters;
using QBSynDal.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace QBSynDal
{
    public class Itemgrouplinedetails : AbstractDalBase
    {
        #region Constructors / Destructors
        public Itemgrouplinedetails(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                itemgrouplinedetailTableAdapter _itemgrouplinedetailTableAdapter = new itemgrouplinedetailTableAdapter();
                _itemgrouplinedetailTableAdapter.Connection = Connection;
                _itemgrouplinedetailTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {

                }

                QBSynDal.List.QBSynDal.itemgrouplinedetailDataTable itemgrouplinedetaildatatable = new List.QBSynDal.itemgrouplinedetailDataTable();
                foreach (Itemgrouplinedetail itemgrouplinedetail in this.Items)
                {
                    DataRow dr = itemgrouplinedetaildatatable.NewRow();
                    itemgrouplinedetaildatatable.Rows.Add(dr);
                    Bind(dr, itemgrouplinedetail);
                }
                _itemgrouplinedetailTableAdapter.Update(itemgrouplinedetaildatatable);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Method (Private)
        private void Bind(DataRow dr, Itemgrouplinedetail itemgrouplinedetail)
        {
            try
            {
                dr[ItemgrouplinedetailFields.ItemRef_ListID] = itemgrouplinedetail.ItemRef_ListID;
                dr[ItemgrouplinedetailFields.ItemRef_FullName] = itemgrouplinedetail.ItemRef_FullName;
                dr[ItemgrouplinedetailFields.Quantity] = itemgrouplinedetail.Quantity;
                dr[ItemgrouplinedetailFields.UnitOfMeasure] = itemgrouplinedetail.UnitOfMeasure;
                dr[ItemgrouplinedetailFields.CustomField1] = itemgrouplinedetail.CustomField1;
                dr[ItemgrouplinedetailFields.CustomField2] = itemgrouplinedetail.CustomField2;
                dr[ItemgrouplinedetailFields.CustomField3] = itemgrouplinedetail.CustomField3;
                dr[ItemgrouplinedetailFields.CustomField4] = itemgrouplinedetail.CustomField4;
                dr[ItemgrouplinedetailFields.CustomField5] = itemgrouplinedetail.CustomField5;
                dr[ItemgrouplinedetailFields.CustomField6] = itemgrouplinedetail.CustomField6;
                dr[ItemgrouplinedetailFields.CustomField7] = itemgrouplinedetail.CustomField7;
                dr[ItemgrouplinedetailFields.CustomField8] = itemgrouplinedetail.CustomField8;
                dr[ItemgrouplinedetailFields.CustomField9] = itemgrouplinedetail.CustomField9;
                dr[ItemgrouplinedetailFields.CustomField10] = itemgrouplinedetail.CustomField10;
                dr[ItemgrouplinedetailFields.Idkey] = itemgrouplinedetail.Idkey;
                dr[ItemgrouplinedetailFields.GroupIDKEY] = itemgrouplinedetail.GroupIDKEY;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
