//
// Class	:	Transactions.cs
// Author	:  	Ignyte Software © 2006
// Date		:	4/2/2013 10:11:58 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;
using QBSynDal;

namespace QBSynDal
{
    public class Transactions : AbstractDalBase
    {


        #region Constructors / Destructors
        public Transactions(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                transactionTableAdapter _transactionTableAdapter = new transactionTableAdapter();
                _transactionTableAdapter.Connection = Connection;
                _transactionTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Transaction transaction in this.Items)
                {
                    QBSynDal.List.QBSynDal.transactionDataTable transactionTable = _transactionTableAdapter.GetDataByTxnID(transaction.TxnID);
                    DataRow dr = null;

                    if (transactionTable.Rows.Count == 0)
                    {
                        dr = transactionTable.NewRow();
                        Bind(dr, transaction);
                        transactionTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = transactionTable.Rows[0];
                        Bind(dr, transaction);
                    }
                    _transactionTableAdapter.Update(transactionTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Methods (Private)
        private void Bind(DataRow dr, Transaction transaction)
        {
            try
            {
                dr[TransactionFields.TxnID] = transaction.TxnID;
                dr[TransactionFields.TxnType] = transaction.TxnType;
                dr[TransactionFields.TxnLineID] = transaction.TxnLineID;
                dr[TransactionFields.TimeCreated] = transaction.TimeCreated;
                dr[TransactionFields.TimeModified] = transaction.TimeModified;
                dr[TransactionFields.EntityRef_ListID] = transaction.EntityRef_ListID;
                dr[TransactionFields.EntityRef_FullName] = transaction.EntityRef_FullName;
                dr[TransactionFields.AccountRef_ListID] = transaction.AccountRef_ListID;
                dr[TransactionFields.AccountRef_FullName] = transaction.AccountRef_FullName;
                dr[TransactionFields.TxnDate] = transaction.TxnDate.GetDbType();
                dr[TransactionFields.RefNumber] = transaction.RefNumber;
                dr[TransactionFields.Amount] = transaction.Amount.GetDbType();
                dr[TransactionFields.CurrencyRef_ListID] = transaction.CurrencyRef_ListID;
                dr[TransactionFields.CurrencyRef_FullName] = transaction.CurrencyRef_FullName;
                dr[TransactionFields.ExchangeRate] = transaction.ExchangeRate.GetDbType();
                dr[TransactionFields.AmountInHomeCurrency] = transaction.AmountInHomeCurrency.GetDbType();
                dr[TransactionFields.Memo] = transaction.TimeCreated;
                dr[TransactionFields.Status] = transaction.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
