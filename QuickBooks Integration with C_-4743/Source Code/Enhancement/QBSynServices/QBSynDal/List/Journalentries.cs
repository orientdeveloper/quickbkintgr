//
// Class	:	Journalentries.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;
namespace QBSynDal
{
    public class Journalentries : AbstractDalBase
    {
        #region Constructors / Destructors
        public Journalentries(string constr)
            :base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                journalentryTableAdapter _journalentryTableAdapter = new journalentryTableAdapter();
                _journalentryTableAdapter.Connection = Connection;
                _journalentryTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Journalentry journalentry in this.Items)
                {
                    QBSynDal.List.QBSynDal.journalentryDataTable journalentryTable = _journalentryTableAdapter.GetDataByTxnID(journalentry.TxnID);
                    DataRow dr = null;

                    if (journalentryTable.Rows.Count == 0)
                    {
                        dr = journalentryTable.NewRow();
                        journalentryTable.Rows.Add(dr);
                        Bind(dr, journalentry);
                    }
                    else
                    {
                        dr = journalentryTable.Rows[0];
                        Bind(dr, journalentry);
                    }
                    _journalentryTableAdapter.Update(journalentryTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Method (Private)
        private void Bind(DataRow dr, Journalentry journalentry)
        {
            try
            {
                dr[JournalentryFields.TxnID] = journalentry.TxnID;
                dr[JournalentryFields.TimeCreated] = journalentry.TimeCreated;
                dr[JournalentryFields.TimeModified] = journalentry.TimeModified;
                dr[JournalentryFields.EditSequence] = journalentry.EditSequence;
                dr[JournalentryFields.TxnNumber] = journalentry.TxnNumber.GetDbType();
                dr[JournalentryFields.TxnDate] = journalentry.TxnDate.GetDbType();
                dr[JournalentryFields.RefNumber] = journalentry.RefNumber;
                dr[JournalentryFields.Memo] = journalentry.Memo;
                dr[JournalentryFields.IsAdjustment] = journalentry.IsAdjustment.GetShort().GetDbType();
                dr[JournalentryFields.IsHomeCurrencyAdjustment] = journalentry.IsHomeCurrencyAdjustment.GetShort().GetDbType();
                dr[JournalentryFields.IsAmountsEnteredInHomeCurrency] = journalentry.IsAmountsEnteredInHomeCurrency.GetShort().GetDbType();
                dr[JournalentryFields.CurrencyRef_ListID] = journalentry.CurrencyRef_ListID;
                dr[JournalentryFields.CurrencyRef_FullName] = journalentry.CurrencyRef_FullName;
                dr[JournalentryFields.ExchangeRate] = journalentry.ExchangeRate.GetDbType();
                dr[JournalentryFields.Name] = journalentry.Name;
                dr[JournalentryFields.CustomField1] = journalentry.CustomField1;
                dr[JournalentryFields.CustomField2] = journalentry.CustomField2;
                dr[JournalentryFields.CustomField3] = journalentry.CustomField3;
                dr[JournalentryFields.CustomField4] = journalentry.CustomField4;
                dr[JournalentryFields.CustomField5] = journalentry.CustomField5;
                dr[JournalentryFields.CustomField6] = journalentry.CustomField6;
                dr[JournalentryFields.CustomField7] = journalentry.CustomField7;
                dr[JournalentryFields.CustomField8] = journalentry.CustomField8;
                dr[JournalentryFields.CustomField9] = journalentry.CustomField9;
                dr[JournalentryFields.CustomField10] = journalentry.CustomField10;
                dr[JournalentryFields.Status] = journalentry.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
