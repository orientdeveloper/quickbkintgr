//
// Class	:	Salestaxpaymentchecks.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:56 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class Salestaxpaymentchecks : AbstractDalBase
    {
        #region Constructors / Destructors
        public Salestaxpaymentchecks(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                salestaxpaymentcheckTableAdapter _salestaxpaymentcheckTableAdapter = new salestaxpaymentcheckTableAdapter();
                _salestaxpaymentcheckTableAdapter.Connection = Connection;
                _salestaxpaymentcheckTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                   
                }

                foreach (Salestaxpaymentcheck salestaxpaymentcheck in this.Items)
                {
                    QBSynDal.List.QBSynDal.salestaxpaymentcheckDataTable salestaxpaymentcheckTable = _salestaxpaymentcheckTableAdapter.GetDataByTxnID(salestaxpaymentcheck.TxnID);
                    DataRow dr = null;

                    if (salestaxpaymentcheckTable.Rows.Count == 0)
                    {
                        dr = salestaxpaymentcheckTable.NewRow();
                        salestaxpaymentcheckTable.Rows.Add(dr);
                        Bind(dr, salestaxpaymentcheck);
                    }
                    else
                    {
                        dr = salestaxpaymentcheckTable.Rows[0];
                        Bind(dr, salestaxpaymentcheck);
                    }
                    _salestaxpaymentcheckTableAdapter.Update(salestaxpaymentcheckTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Salestaxpaymentcheck salestaxpaymentcheck)
        {
            try
            {
                dr[SalestaxpaymentcheckFields.TxnID] = salestaxpaymentcheck.TxnID;
                dr[SalestaxpaymentcheckFields.TimeCreated] = salestaxpaymentcheck.TimeCreated;
                dr[SalestaxpaymentcheckFields.TimeModified] = salestaxpaymentcheck.TimeModified;
                dr[SalestaxpaymentcheckFields.EditSequence] = salestaxpaymentcheck.EditSequence;
                dr[SalestaxpaymentcheckFields.TxnNumber] = salestaxpaymentcheck.TxnNumber.GetDbType();
                dr[SalestaxpaymentcheckFields.TxnDate] = salestaxpaymentcheck.TxnDate.GetDbType();
                dr[SalestaxpaymentcheckFields.PayeeEntityRef_ListID] = salestaxpaymentcheck.PayeeEntityRef_ListID;
                dr[SalestaxpaymentcheckFields.PayeeEntityRef_FullName] = salestaxpaymentcheck.PayeeEntityRef_FullName;
                dr[SalestaxpaymentcheckFields.BankAccountRef_ListID] = salestaxpaymentcheck.BankAccountRef_ListID;
                dr[SalestaxpaymentcheckFields.BankAccountRef_FullName] = salestaxpaymentcheck.BankAccountRef_FullName;
                dr[SalestaxpaymentcheckFields.Amount] = salestaxpaymentcheck.Amount.GetDbType();
                dr[SalestaxpaymentcheckFields.RefNumber] = salestaxpaymentcheck.RefNumber;
                dr[SalestaxpaymentcheckFields.Memo] = salestaxpaymentcheck.Memo;
                dr[SalestaxpaymentcheckFields.Address_Addr1] = salestaxpaymentcheck.Address_Addr1;
                dr[SalestaxpaymentcheckFields.Address_Addr2] = salestaxpaymentcheck.Address_Addr2;
                dr[SalestaxpaymentcheckFields.Address_Addr3] = salestaxpaymentcheck.Address_Addr3;
                dr[SalestaxpaymentcheckFields.Address_Addr4] = salestaxpaymentcheck.Address_Addr4;
                dr[SalestaxpaymentcheckFields.Address_Addr5] = salestaxpaymentcheck.Address_Addr5;
                dr[SalestaxpaymentcheckFields.Address_City] = salestaxpaymentcheck.Address_City;
                dr[SalestaxpaymentcheckFields.Address_State] = salestaxpaymentcheck.Address_State;
                dr[SalestaxpaymentcheckFields.Address_PostalCode] = salestaxpaymentcheck.Address_PostalCode;
                dr[SalestaxpaymentcheckFields.Address_Country] = salestaxpaymentcheck.Address_Country;
                dr[SalestaxpaymentcheckFields.Address_Note] = salestaxpaymentcheck.Address_Note;
                dr[SalestaxpaymentcheckFields.IsToBePrinted] = salestaxpaymentcheck.IsToBePrinted.GetShort().GetDbType();
                dr[SalestaxpaymentcheckFields.CustomField1] = salestaxpaymentcheck.CustomField1;
                dr[SalestaxpaymentcheckFields.CustomField2] = salestaxpaymentcheck.CustomField2;
                dr[SalestaxpaymentcheckFields.CustomField3] = salestaxpaymentcheck.CustomField3;
                dr[SalestaxpaymentcheckFields.CustomField4] = salestaxpaymentcheck.CustomField4;
                dr[SalestaxpaymentcheckFields.CustomField5] = salestaxpaymentcheck.CustomField5;
                dr[SalestaxpaymentcheckFields.CustomField6] = salestaxpaymentcheck.CustomField6;
                dr[SalestaxpaymentcheckFields.CustomField7] = salestaxpaymentcheck.CustomField7;
                dr[SalestaxpaymentcheckFields.CustomField8] = salestaxpaymentcheck.CustomField8;
                dr[SalestaxpaymentcheckFields.CustomField9] = salestaxpaymentcheck.CustomField9;
                dr[SalestaxpaymentcheckFields.CustomField10] = salestaxpaymentcheck.CustomField10;
                dr[SalestaxpaymentcheckFields.Status] = salestaxpaymentcheck.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
