//
// Class	:	Arrefundcreditcards.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List;
using QBSynDal.List.QBSynDalTableAdapters;
using QBSynDal.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace QBSynDal
{
    public class Inventoryadjustmentlinedetails : AbstractDalBase
    {
        #region Constructors / Destructors
        public Inventoryadjustmentlinedetails(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                inventoryadjustmentlinedetailTableAdapter _inventoryadjustmentlinedetailTableAdapter = new inventoryadjustmentlinedetailTableAdapter();
                _inventoryadjustmentlinedetailTableAdapter.Connection = Connection;
                _inventoryadjustmentlinedetailTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                QBSynDal.List.QBSynDal.inventoryadjustmentlinedetailDataTable inventoryadjustmentlinedetailDataTable = new List.QBSynDal.inventoryadjustmentlinedetailDataTable();
                foreach (Inventoryadjustmentlinedetail inventoryadjustmentlinedetail in this.Items)
                {
                    DataRow dr = inventoryadjustmentlinedetailDataTable.NewRow();
                    inventoryadjustmentlinedetailDataTable.Rows.Add(dr);
                    Bind(dr, inventoryadjustmentlinedetail);
                }
                _inventoryadjustmentlinedetailTableAdapter.Update(inventoryadjustmentlinedetailDataTable);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Method (Private)
        private void Bind(DataRow dr, Inventoryadjustmentlinedetail inventoryadjustmentlinedetail)
        {
            dr[InventoryadjustmentlinedetailFields.TxnLineID] = inventoryadjustmentlinedetail.TxnLineID;
            dr[InventoryadjustmentlinedetailFields.ItemRef_ListID] = inventoryadjustmentlinedetail.ItemRef_ListID;
            dr[InventoryadjustmentlinedetailFields.ItemRef_FullName] = inventoryadjustmentlinedetail.ItemRef_FullName;
            dr[InventoryadjustmentlinedetailFields.QuantityDifference] = inventoryadjustmentlinedetail.QuantityDifference;
            dr[InventoryadjustmentlinedetailFields.ValueDifference] = inventoryadjustmentlinedetail.ValueDifference.GetDbType();
            dr[InventoryadjustmentlinedetailFields.CustomField1] = inventoryadjustmentlinedetail.CustomField1;
            dr[InventoryadjustmentlinedetailFields.CustomField2] = inventoryadjustmentlinedetail.CustomField2;
            dr[InventoryadjustmentlinedetailFields.CustomField3] = inventoryadjustmentlinedetail.CustomField3;
            dr[InventoryadjustmentlinedetailFields.CustomField4] = inventoryadjustmentlinedetail.CustomField4;
            dr[InventoryadjustmentlinedetailFields.CustomField5] = inventoryadjustmentlinedetail.CustomField5;
            dr[InventoryadjustmentlinedetailFields.CustomField6] = inventoryadjustmentlinedetail.CustomField6;
            dr[InventoryadjustmentlinedetailFields.CustomField7] = inventoryadjustmentlinedetail.CustomField7;
            dr[InventoryadjustmentlinedetailFields.CustomField8] = inventoryadjustmentlinedetail.CustomField8;
            dr[InventoryadjustmentlinedetailFields.CustomField9] = inventoryadjustmentlinedetail.CustomField9;
            dr[InventoryadjustmentlinedetailFields.CustomField10] = inventoryadjustmentlinedetail.CustomField10;
            dr[InventoryadjustmentlinedetailFields.Idkey] = inventoryadjustmentlinedetail.Idkey;
            dr[InventoryadjustmentlinedetailFields.GroupIDKEY] = inventoryadjustmentlinedetail.GroupIDKEY;
        }
        #endregion

    }
}
