//
// Class	:	Vendors.cs
// Author	:  	Synapse Software © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class Vendors : AbstractDalBase
    {
        #region Constructors / Destructors
        public Vendors(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                vendorTableAdapter _vendorTableAdapter = new vendorTableAdapter();
                _vendorTableAdapter.Connection = Connection;
                _vendorTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Vendor vendor in this.Items)
                {
                    QBSynDal.List.QBSynDal.vendorDataTable vendorTable = _vendorTableAdapter.GetDataByListID(vendor.ListID);
                    DataRow dr = null;

                    if (vendorTable.Rows.Count == 0)
                    {
                        dr = vendorTable.NewRow();
                        Bind(dr, vendor);
                        vendorTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = vendorTable.Rows[0];
                        Bind(dr, vendor);
                    }
                    _vendorTableAdapter.Update(vendorTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Vendor vendor)
        {
            try
            {
                dr[VendorFields.ListID] = vendor.ListID;
                dr[VendorFields.TimeCreated] = vendor.TimeCreated;
                dr[VendorFields.TimeModified] = vendor.TimeModified;
                dr[VendorFields.EditSequence] = vendor.EditSequence;
                dr[VendorFields.Name] = vendor.Name;
                dr[VendorFields.IsActive] = vendor.IsActive.GetShort().GetDbType();
                dr[VendorFields.CompanyName] = vendor.CompanyName;
                dr[VendorFields.Salutation] = vendor.Salutation;
                dr[VendorFields.FirstName] = vendor.FirstName;
                dr[VendorFields.MiddleName] = vendor.MiddleName;
                dr[VendorFields.LastName] = vendor.LastName;
                dr[VendorFields.Suffix] = vendor.Suffix;
                dr[VendorFields.VendorAddress_Addr1] = vendor.VendorAddress_Addr1;
                dr[VendorFields.VendorAddress_Addr2] = vendor.VendorAddress_Addr2;
                dr[VendorFields.VendorAddress_Addr3] = vendor.VendorAddress_Addr3;
                dr[VendorFields.VendorAddress_Addr4] = vendor.VendorAddress_Addr4;
                dr[VendorFields.VendorAddress_Addr5] = vendor.VendorAddress_Addr5;
                dr[VendorFields.VendorAddress_City] = vendor.VendorAddress_City;
                dr[VendorFields.VendorAddress_State] = vendor.VendorAddress_State;
                dr[VendorFields.VendorAddress_PostalCode] = vendor.VendorAddress_PostalCode;
                dr[VendorFields.VendorAddress_Country] = vendor.VendorAddress_Country;
                dr[VendorFields.VendorAddress_Note] = vendor.VendorAddress_Note;
                dr[VendorFields.Phone] = vendor.Phone;
                dr[VendorFields.Mobile] = vendor.Mobile;
                dr[VendorFields.Pager] = vendor.Pager;
                dr[VendorFields.AltPhone] = vendor.AltPhone;
                dr[VendorFields.Fax] = vendor.Fax;
                dr[VendorFields.Email] = vendor.Email;
                dr[VendorFields.Contact] = vendor.Contact;
                dr[VendorFields.AltContact] = vendor.AltContact;
                dr[VendorFields.NameOnCheck] = vendor.NameOnCheck;
                dr[VendorFields.Notes] = vendor.Notes;
                dr[VendorFields.AccountNumber] = vendor.AccountNumber;
                dr[VendorFields.VendorTypeRef_ListID] = vendor.VendorTypeRef_ListID;
                dr[VendorFields.VendorTypeRef_FullName] = vendor.VendorTypeRef_FullName;
                dr[VendorFields.TermsRef_ListID] = vendor.TermsRef_ListID;
                dr[VendorFields.TermsRef_FullName] = vendor.TermsRef_FullName;
                dr[VendorFields.CreditLimit] = vendor.CreditLimit.GetDbType();
                dr[VendorFields.VendorTaxIdent] = vendor.VendorTaxIdent;
                dr[VendorFields.IsVendorEligibleFor1099] = vendor.IsVendorEligibleFor1099.GetShort().GetDbType();
                dr[VendorFields.Balance] = vendor.Balance.GetDbType();
                dr[VendorFields.IsSalesTaxAgency] = vendor.IsSalesTaxAgency.GetShort().GetDbType();
                dr[VendorFields.CurrencyRef_ListID] = vendor.CurrencyRef_ListID;
                dr[VendorFields.CurrencyRef_FullName] = vendor.CurrencyRef_FullName;
                dr[VendorFields.BillingRateRef_ListID] = vendor.BillingRateRef_ListID;
                dr[VendorFields.BillingRateRef_FullName] = vendor.BillingRateRef_FullName;
                dr[VendorFields.CustomField1] = vendor.CustomField1;
                dr[VendorFields.CustomField2] = vendor.CustomField2;
                dr[VendorFields.CustomField3] = vendor.CustomField3;
                dr[VendorFields.CustomField4] = vendor.CustomField4;
                dr[VendorFields.CustomField5] = vendor.CustomField5;
                dr[VendorFields.CustomField6] = vendor.CustomField6;
                dr[VendorFields.CustomField7] = vendor.CustomField7;
                dr[VendorFields.CustomField8] = vendor.CustomField8;
                dr[VendorFields.CustomField9] = vendor.CustomField9;
                dr[VendorFields.CustomField10] = vendor.CustomField10;
                dr[VendorFields.Status] = vendor.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
