//
// Class	:	Customertypes.cs
// Author	:  	Synapse Software © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class Customertypes : AbstractDalBase
    {
        #region Constructors / Destructors
        public Customertypes(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                customertypeTableAdapter _customertypeTableAdapter = new customertypeTableAdapter();
                _customertypeTableAdapter.Connection = Connection;
                _customertypeTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Customertype customertype in this.Items)
                {
                    QBSynDal.List.QBSynDal.customertypeDataTable customertypeTable = _customertypeTableAdapter.GetDataByListID(customertype.ListID);
                    DataRow dr = null;

                    if (customertypeTable.Rows.Count == 0)
                    {
                        dr = customertypeTable.NewRow();
                        Bind(dr, customertype);
                        customertypeTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = customertypeTable.Rows[0];
                        Bind(dr, customertype);
                    }
                    _customertypeTableAdapter.Update(customertypeTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Customertype customertype)
        {
            try
            {
                dr[CustomertypeFields.EditSequence] = customertype.EditSequence;
                dr[CustomertypeFields.FullName] = customertype.FullName;
                dr[CustomertypeFields.IsActive] = customertype.IsActive.GetShort().GetDbType();
                dr[CustomertypeFields.ListID] = customertype.ListID;
                dr[CustomertypeFields.Name] = customertype.Name;
                dr[CustomertypeFields.ParentRef_FullName] = customertype.ParentRef_FullName;
                dr[CustomertypeFields.ParentRef_ListID] = customertype.ParentRef_ListID;
                dr[CustomertypeFields.Status] = customertype.Status;
                dr[CustomertypeFields.Sublevel] = customertype.Sublevel.GetDbType();
                dr[CustomertypeFields.TimeCreated] = customertype.TimeCreated;
                dr[CustomertypeFields.TimeModified] = customertype.TimeModified;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
