//
// Class	:	Accounts.cs
// Author	:  	Synapse Software © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class Salesreps : AbstractDalBase
    {
        #region Constructors / Destructors
        public Salesreps(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                salesrepTableAdapter _salesrepTableAdapter = new salesrepTableAdapter();
                _salesrepTableAdapter.Connection = Connection;
                _salesrepTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Salesrep salesrep in this.Items)
                {
                    QBSynDal.List.QBSynDal.salesrepDataTable salesrepTable = _salesrepTableAdapter.GetDataByListID(salesrep.ListID);
                    DataRow dr = null;

                    if (salesrepTable.Rows.Count == 0)
                    {
                        dr = salesrepTable.NewRow();
                        Bind(dr, salesrep);
                        salesrepTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = salesrepTable.Rows[0];
                        Bind(dr, salesrep);
                    }
                    _salesrepTableAdapter.Update(salesrepTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Salesrep salesrep)
        {
            try
            {
                dr[SalesrepFields.ListID] = salesrep.ListID;
                dr[SalesrepFields.TimeCreated] = salesrep.TimeCreated;
                dr[SalesrepFields.TimeModified] = salesrep.TimeModified;
                dr[SalesrepFields.EditSequence] = salesrep.EditSequence;
                dr[SalesrepFields.Initial] = salesrep.Initial;
                dr[SalesrepFields.IsActive] = salesrep.IsActive.GetShort().GetDbType();
                dr[SalesrepFields.SalesRepEntityRef_ListID] = salesrep.SalesRepEntityRef_ListID;
                dr[SalesrepFields.SalesRepEntityRef_FullName] = salesrep.SalesRepEntityRef_FullName;
                dr[SalesrepFields.Status] = salesrep.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
