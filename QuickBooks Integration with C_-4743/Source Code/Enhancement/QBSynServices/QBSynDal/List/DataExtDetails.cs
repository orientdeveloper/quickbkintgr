//
// Class	:	Items.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List;
using QBSynDal.List.QBSynDalTableAdapters;
using QBSynDal.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Linq;
using System.Text;

namespace QBSynDal
{
    public class DataExtDetails : AbstractDalBase
    {
        #region Constructors / Destructors
        public DataExtDetails(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                dataextdetailTableAdapter _dataextdetailTableAdapter = new dataextdetailTableAdapter();
                _dataextdetailTableAdapter.Connection = Connection;
                _dataextdetailTableAdapter.Transaction = Transaction;

                QBSynDal.List.QBSynDal.dataextdetailDataTable dataextdetailDataTable = new List.QBSynDal.dataextdetailDataTable();
                foreach (DataExtDetail dataExtDetail in this.Items)
                {
                    DataRow dr = dataextdetailDataTable.NewRow();
                    dataextdetailDataTable.Rows.Add(dr);
                    Bind(dr, dataExtDetail);
                }
                _dataextdetailTableAdapter.Update(dataextdetailDataTable);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, DataExtDetail dataExtDetail)
        {
            try
            {
                dr[DataExtDetailFields.DataExtName] = dataExtDetail.DataExtName;
                dr[DataExtDetailFields.DataExtType] = dataExtDetail.DataExtType;
                dr[DataExtDetailFields.DataExtValue] = dataExtDetail.DataExtValue;
                dr[DataExtDetailFields.OwnerID] = dataExtDetail.OwnerID;
                dr[DataExtDetailFields.OwnerType] = dataExtDetail.OwnerType;
                dr[DataExtDetailFields.CustomField1] = dataExtDetail.CustomField1;
                dr[DataExtDetailFields.CustomField2] = dataExtDetail.CustomField2;
                dr[DataExtDetailFields.CustomField3] = dataExtDetail.CustomField3;
                dr[DataExtDetailFields.CustomField4] = dataExtDetail.CustomField4;
                dr[DataExtDetailFields.CustomField5] = dataExtDetail.CustomField5;
                dr[DataExtDetailFields.CustomField6] = dataExtDetail.CustomField6;
                dr[DataExtDetailFields.CustomField7] = dataExtDetail.CustomField7;
                dr[DataExtDetailFields.CustomField8] = dataExtDetail.CustomField8;
                dr[DataExtDetailFields.CustomField9] = dataExtDetail.CustomField9;
                dr[DataExtDetailFields.CustomField10] = dataExtDetail.CustomField10;
                dr[DataExtDetailFields.Idkey] = dataExtDetail.Idkey;
                dr[DataExtDetailFields.GroupIDKEY] = dataExtDetail.GroupIDKEY;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
