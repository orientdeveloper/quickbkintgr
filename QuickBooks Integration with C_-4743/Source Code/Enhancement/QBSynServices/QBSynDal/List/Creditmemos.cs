//
// Class	:	Creditmemos.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class Creditmemos : AbstractDalBase
    {
        #region Constructors / Destructors
        public Creditmemos(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                creditmemoTableAdapter _creditmemoTableAdapter = new creditmemoTableAdapter();
                _creditmemoTableAdapter.Connection = Connection;
                _creditmemoTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Creditmemo creditmemo in this.Items)
                {
                    QBSynDal.List.QBSynDal.creditmemoDataTable creditmemoTable = _creditmemoTableAdapter.GetDataByTxnID(creditmemo.TxnID);
                    DataRow dr = null;

                    if (creditmemoTable.Rows.Count == 0)
                    {
                        dr = creditmemoTable.NewRow();
                        creditmemoTable.Rows.Add(dr);
                        Bind(dr, creditmemo);
                    }
                    else
                    {
                        dr = creditmemoTable.Rows[0];
                        Bind(dr, creditmemo);
                    }
                    _creditmemoTableAdapter.Update(creditmemoTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Method (Private)
        private void Bind(DataRow dr, Creditmemo creditmemo)
        {
            dr[CreditmemoFields.TxnID] = creditmemo.TxnID;
            dr[CreditmemoFields.EditSequence] = creditmemo.EditSequence;
            dr[CreditmemoFields.TimeCreated] = creditmemo.TimeCreated;
            dr[CreditmemoFields.TimeModified] = creditmemo.TimeModified;
            dr[CreditmemoFields.TxnNumber] = creditmemo.TxnNumber.GetDbType();
            dr[CreditmemoFields.CustomerRef_ListID] = creditmemo.CustomerRef_ListID;
            dr[CreditmemoFields.CustomerRef_FullName] = creditmemo.CustomerRef_FullName;
            dr[CreditmemoFields.ClassRef_ListID] = creditmemo.ClassRef_ListID;
            dr[CreditmemoFields.ClassRef_FullName] = creditmemo.ClassRef_FullName;
            dr[CreditmemoFields.ARAccountRef_ListID] = creditmemo.ARAccountRef_ListID;
            dr[CreditmemoFields.ARAccountRef_FullName] = creditmemo.ARAccountRef_FullName;
            dr[CreditmemoFields.TemplateRef_ListID] = creditmemo.TemplateRef_ListID;
            dr[CreditmemoFields.TemplateRef_FullName] = creditmemo.TemplateRef_FullName;
            dr[CreditmemoFields.TxnDate] = creditmemo.TxnDate.GetDbType();
            dr[CreditmemoFields.RefNumber] = creditmemo.RefNumber;
            dr[CreditmemoFields.BillAddress_Addr1] = creditmemo.BillAddress_Addr1;
            dr[CreditmemoFields.BillAddress_Addr2] = creditmemo.BillAddress_Addr2;
            dr[CreditmemoFields.BillAddress_Addr3] = creditmemo.BillAddress_Addr3;
            dr[CreditmemoFields.BillAddress_Addr4] = creditmemo.BillAddress_Addr4;
            dr[CreditmemoFields.BillAddress_Addr5] = creditmemo.BillAddress_Addr5;
            dr[CreditmemoFields.BillAddress_City] = creditmemo.BillAddress_City;
            dr[CreditmemoFields.BillAddress_State] = creditmemo.BillAddress_State;
            dr[CreditmemoFields.BillAddress_PostalCode] = creditmemo.BillAddress_PostalCode;
            dr[CreditmemoFields.BillAddress_Country] = creditmemo.BillAddress_Country;
            dr[CreditmemoFields.BillAddress_Note] = creditmemo.BillAddress_Note;
            dr[CreditmemoFields.ShipAddress_Addr1] = creditmemo.ShipAddress_Addr1;
            dr[CreditmemoFields.ShipAddress_Addr2] = creditmemo.ShipAddress_Addr2;
            dr[CreditmemoFields.ShipAddress_Addr3] = creditmemo.ShipAddress_Addr3;
            dr[CreditmemoFields.ShipAddress_Addr4] = creditmemo.ShipAddress_Addr4;
            dr[CreditmemoFields.ShipAddress_Addr5] = creditmemo.ShipAddress_Addr5;
            dr[CreditmemoFields.ShipAddress_Addr5] = creditmemo.ShipAddress_Addr5;
            dr[CreditmemoFields.ShipAddress_City] = creditmemo.ShipAddress_City;
            dr[CreditmemoFields.ShipAddress_State] = creditmemo.ShipAddress_State;
            dr[CreditmemoFields.ShipAddress_PostalCode] = creditmemo.ShipAddress_PostalCode;
            dr[CreditmemoFields.ShipAddress_Country] = creditmemo.ShipAddress_Country;
            dr[CreditmemoFields.ShipAddress_Note] = creditmemo.ShipAddress_Note;
            dr[CreditmemoFields.IsPending] = creditmemo.IsPending.GetShort().GetDbType(); ;
            dr[CreditmemoFields.PONumber] = creditmemo.PONumber;
            dr[CreditmemoFields.TermsRef_ListID] = creditmemo.TermsRef_ListID;
            dr[CreditmemoFields.TermsRef_FullName] = creditmemo.TermsRef_FullName;
            dr[CreditmemoFields.DueDate] = creditmemo.DueDate.GetDbType();
            dr[CreditmemoFields.SalesRepRef_ListID] = creditmemo.SalesRepRef_ListID;
            dr[CreditmemoFields.SalesRepRef_FullName] = creditmemo.SalesRepRef_FullName;
            dr[CreditmemoFields.Fob] = creditmemo.Fob;
            dr[CreditmemoFields.ShipDate] = creditmemo.ShipDate.GetDbType();
            dr[CreditmemoFields.ShipMethodRef_ListID] = creditmemo.ShipMethodRef_ListID;
            dr[CreditmemoFields.ShipMethodRef_FullName] = creditmemo.ShipMethodRef_FullName;
            dr[CreditmemoFields.Subtotal] = creditmemo.Subtotal.GetDbType();
            dr[CreditmemoFields.ItemSalesTaxRef_ListID] = creditmemo.ItemSalesTaxRef_ListID;
            dr[CreditmemoFields.ItemSalesTaxRef_FullName] = creditmemo.ItemSalesTaxRef_FullName;
            dr[CreditmemoFields.SalesTaxPercentage] = creditmemo.SalesTaxPercentage;
            dr[CreditmemoFields.SalesTaxTotal] = creditmemo.SalesTaxTotal.GetDbType();
            dr[CreditmemoFields.TotalAmount] = creditmemo.TotalAmount.GetDbType();
            dr[CreditmemoFields.CreditRemaining] = creditmemo.CreditRemaining.GetDbType();
            dr[CreditmemoFields.CurrencyRef_ListID] = creditmemo.CurrencyRef_ListID;
            dr[CreditmemoFields.CurrencyRef_FullName] = creditmemo.CurrencyRef_FullName;
            dr[CreditmemoFields.ExchangeRate] = creditmemo.ExchangeRate.GetDbType();
            dr[CreditmemoFields.CreditRemainingInHomeCurrency] = creditmemo.CreditRemainingInHomeCurrency.GetDbType();
            dr[CreditmemoFields.Memo] = creditmemo.Memo;
            dr[CreditmemoFields.CustomerMsgRef_ListID] = creditmemo.CustomerMsgRef_ListID;
            dr[CreditmemoFields.CustomerMsgRef_FullName] = creditmemo.CustomerMsgRef_FullName;
            dr[CreditmemoFields.IsToBePrinted] = creditmemo.IsToBePrinted.GetShort().GetDbType();
            dr[CreditmemoFields.IsToBeEmailed] = creditmemo.IsToBeEmailed.GetShort().GetDbType();
            dr[CreditmemoFields.IsTaxIncluded] = creditmemo.IsTaxIncluded.GetShort().GetDbType();
            dr[CreditmemoFields.CustomerSalesTaxCodeRef_ListID] = creditmemo.CustomerSalesTaxCodeRef_ListID;
            dr[CreditmemoFields.CustomerSalesTaxCodeRef_FullName] = creditmemo.CustomerSalesTaxCodeRef_FullName;
            dr[CreditmemoFields.Other] = creditmemo.Other;
            dr[CreditmemoFields.CustomField1] = creditmemo.CustomField1;
            dr[CreditmemoFields.CustomField2] = creditmemo.CustomField2;
            dr[CreditmemoFields.CustomField3] = creditmemo.CustomField3;
            dr[CreditmemoFields.CustomField4] = creditmemo.CustomField4;
            dr[CreditmemoFields.CustomField5] = creditmemo.CustomField5;
            dr[CreditmemoFields.CustomField6] = creditmemo.CustomField6;
            dr[CreditmemoFields.CustomField7] = creditmemo.CustomField7;
            dr[CreditmemoFields.CustomField8] = creditmemo.CustomField8;
            dr[CreditmemoFields.CustomField9] = creditmemo.CustomField9;
            dr[CreditmemoFields.CustomField10] = creditmemo.CustomField10;

        }
        #endregion

    }
}
