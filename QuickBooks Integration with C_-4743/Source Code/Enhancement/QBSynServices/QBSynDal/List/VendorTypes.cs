//
// Class	:	Accounts.cs
// Author	:  	Synapse Software © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class VendorTypes : AbstractDalBase
    {
        #region Constructors / Destructors
        public VendorTypes(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                vendortypeTableAdapter _vendortypeTableAdapter = new vendortypeTableAdapter();
                _vendortypeTableAdapter.Connection = Connection;
                _vendortypeTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Vendortype vendortype in this.Items)
                {
                    QBSynDal.List.QBSynDal.vendortypeDataTable vendortypeTable = _vendortypeTableAdapter.GetDataByListID(vendortype.ListID);
                    DataRow dr = null;

                    if (vendortypeTable.Rows.Count == 0)
                    {
                        dr = vendortypeTable.NewRow();
                        Bind(dr, vendortype);
                        vendortypeTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = vendortypeTable.Rows[0];
                        Bind(dr, vendortype);
                    }
                    _vendortypeTableAdapter.Update(vendortypeTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Vendortype vendortype)
        {
            try
            {
                dr[VendortypeFields.ListID] = vendortype.ListID;
                dr[VendortypeFields.TimeCreated] = vendortype.TimeCreated;
                dr[VendortypeFields.TimeModified] = vendortype.TimeModified;
                dr[VendortypeFields.EditSequence] = vendortype.EditSequence;
                dr[VendortypeFields.Name] = vendortype.Name;
                dr[VendortypeFields.FullName] = vendortype.FullName;
                dr[VendortypeFields.IsActive] = vendortype.IsActive.GetShort().GetDbType();
                dr[VendortypeFields.ParentRef_ListID] = vendortype.ParentRef_ListID;
                dr[VendortypeFields.ParentRef_FullName] = vendortype.ParentRef_FullName;
                dr[VendortypeFields.Sublevel] = vendortype.Sublevel.GetDbType();
                dr[VendortypeFields.Status] = vendortype.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
