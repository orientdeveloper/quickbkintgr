//
// Class	:	PayrollTransactionReport.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class BuildAssemblyReports : AbstractDalBase
    {
        #region Variable
        DateTime? _lastmodifieddate;
        #endregion

        #region Constructors / Destructors
        public BuildAssemblyReports(string constr, DateTime? lastmodifieddate)
            : base(constr)
        {
            _lastmodifieddate = lastmodifieddate;
        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                BuildAssemblyReportTableAdapter _BuildAssemblyReportTableAdapter = new BuildAssemblyReportTableAdapter();
                _BuildAssemblyReportTableAdapter.Connection = Connection;
                _BuildAssemblyReportTableAdapter.Transaction = Transaction;

                if (synAction == SynAction.InitiateDelete || synAction == SynAction.DoneDelete)
                {
                    if (_lastmodifieddate.HasValue)
                    {
                        _BuildAssemblyReportTableAdapter.DeleteByDate(_lastmodifieddate.Value.Date.Year, _lastmodifieddate.Value.Date.Month, _lastmodifieddate.Value.Date.Day);
                    }
                    else
                    {
                        _BuildAssemblyReportTableAdapter.DeleteAll();
                    }
                }

                QBSynDal.List.QBSynDal.BuildAssemblyReportDataTable buildassemblyreportDataTable = new List.QBSynDal.BuildAssemblyReportDataTable();
                foreach (BuildAssemblyReport buildassemblyreport in this.Items)
                {
                    DataRow dr = buildassemblyreportDataTable.NewRow();
                    buildassemblyreportDataTable.Rows.Add(dr);
                    Bind(dr, buildassemblyreport);
                }
                _BuildAssemblyReportTableAdapter.Update(buildassemblyreportDataTable);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Method (Private)
        private void Bind(DataRow dr, BuildAssemblyReport _BuildAssemblyReport)
        {
            try
            {
                dr[BuildAssemblyReportFields.Account] = _BuildAssemblyReport.Account;
                dr[BuildAssemblyReportFields.Account_ListID] = _BuildAssemblyReport.Account_ListID;
                dr[BuildAssemblyReportFields.AccountType] = _BuildAssemblyReport.AccountType;
                dr[BuildAssemblyReportFields.Balance] = _BuildAssemblyReport.Balance.GetDbType();
                dr[BuildAssemblyReportFields.Class_ListID] = _BuildAssemblyReport.Class_ListID;
                dr[BuildAssemblyReportFields.Class] = _BuildAssemblyReport.Class;
                dr[BuildAssemblyReportFields.ClearedStatus] = _BuildAssemblyReport.ClearedStatus;
                dr[BuildAssemblyReportFields.Credit] = _BuildAssemblyReport.Credit.GetDbType();
                dr[BuildAssemblyReportFields.Customer_ListID] = _BuildAssemblyReport.Customer_ListID;
                dr[BuildAssemblyReportFields.Date] = _BuildAssemblyReport.Date.GetDbType();
                dr[BuildAssemblyReportFields.Debit] = _BuildAssemblyReport.Debit.GetDbType();
                dr[BuildAssemblyReportFields.DeliveryDate] = _BuildAssemblyReport.DeliveryDate.GetDbType();
                dr[BuildAssemblyReportFields.ExchangeRate] = _BuildAssemblyReport.ExchangeRate.GetDbType();
                dr[BuildAssemblyReportFields.FOB] = _BuildAssemblyReport.FOB;
                dr[BuildAssemblyReportFields.Item] = _BuildAssemblyReport.Item;
                dr[BuildAssemblyReportFields.Item_ListID] = _BuildAssemblyReport.Item_ListID;
                dr[BuildAssemblyReportFields.Memo] = _BuildAssemblyReport.Memo;
                dr[BuildAssemblyReportFields.ModifiedTime] = _BuildAssemblyReport.ModifiedTime.GetDbType();
                dr[BuildAssemblyReportFields.Name] = _BuildAssemblyReport.Name;
                dr[BuildAssemblyReportFields.Quantity] = _BuildAssemblyReport.Quanity.GetDbType();
                dr[BuildAssemblyReportFields.RefNumber] = _BuildAssemblyReport.RefNumber;
                dr[BuildAssemblyReportFields.SalesRep] = _BuildAssemblyReport.SalesRep;
                dr[BuildAssemblyReportFields.SalesTaxCode] = _BuildAssemblyReport.SalesTaxCode;
                dr[BuildAssemblyReportFields.ShipDate] = _BuildAssemblyReport.ShipDate.GetDbType();
                dr[BuildAssemblyReportFields.ShipMethod] = _BuildAssemblyReport.ShipMethod;
                dr[BuildAssemblyReportFields.SplitAccount_ListID] = _BuildAssemblyReport.SplitAccount_ListID;
                dr[BuildAssemblyReportFields.SplitAccount] = _BuildAssemblyReport.SplitAccount;
                dr[BuildAssemblyReportFields.TxnNumber] = _BuildAssemblyReport.TxnNumber.GetDbType();
                dr[BuildAssemblyReportFields.TxnType] = _BuildAssemblyReport.TxnType;
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
