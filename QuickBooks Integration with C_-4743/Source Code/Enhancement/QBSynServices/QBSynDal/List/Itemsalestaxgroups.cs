using QBSynDal.Helper;
//
// Class	:	Itemsalestaxgroups.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:44 AM
//
using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;
namespace QBSynDal
{
    public class Itemsalestaxgroups : AbstractDalBase
    {
        #region Constructors / Destructors
        public Itemsalestaxgroups(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                itemsalestaxgroupTableAdapter _itemsalestaxgroupTableAdapter = new itemsalestaxgroupTableAdapter();
                _itemsalestaxgroupTableAdapter.Connection = Connection;
                _itemsalestaxgroupTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Itemsalestaxgroup itemsalestaxgroup in this.Items)
                {
                    QBSynDal.List.QBSynDal.itemsalestaxgroupDataTable itemsalestaxgroupTable = _itemsalestaxgroupTableAdapter.GetDataByListID(itemsalestaxgroup.ListID);
                    DataRow dr = null;

                    if (itemsalestaxgroupTable.Rows.Count == 0)
                    {
                        dr = itemsalestaxgroupTable.NewRow();
                        Bind(dr, itemsalestaxgroup);
                        itemsalestaxgroupTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = itemsalestaxgroupTable.Rows[0];
                        Bind(dr, itemsalestaxgroup);
                    }
                    _itemsalestaxgroupTableAdapter.Update(itemsalestaxgroupTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Itemsalestaxgroup itemsalestaxgroup)
        {
            try
            {
                dr[ItemsalestaxFields.ListID] = itemsalestaxgroup.ListID;
                dr[ItemsalestaxFields.TimeCreated] = itemsalestaxgroup.TimeCreated;
                dr[ItemsalestaxFields.TimeModified] = itemsalestaxgroup.TimeModified;
                dr[ItemsalestaxFields.EditSequence] = itemsalestaxgroup.EditSequence;
                dr[ItemsalestaxFields.Name] = itemsalestaxgroup.Name;
                dr[ItemsalestaxFields.IsActive] = itemsalestaxgroup.IsActive.GetShort().GetDbType();
                dr[ItemsalestaxFields.ItemDesc] = itemsalestaxgroup.ItemDesc;
                dr[ItemsalestaxFields.CustomField1] = itemsalestaxgroup.CustomField1;
                dr[ItemsalestaxFields.CustomField2] = itemsalestaxgroup.CustomField2;
                dr[ItemsalestaxFields.CustomField3] = itemsalestaxgroup.CustomField3;
                dr[ItemsalestaxFields.CustomField4] = itemsalestaxgroup.CustomField4;
                dr[ItemsalestaxFields.CustomField5] = itemsalestaxgroup.CustomField5;
                dr[ItemsalestaxFields.CustomField6] = itemsalestaxgroup.CustomField6;
                dr[ItemsalestaxFields.CustomField7] = itemsalestaxgroup.CustomField7;
                dr[ItemsalestaxFields.CustomField8] = itemsalestaxgroup.CustomField8;
                dr[ItemsalestaxFields.CustomField9] = itemsalestaxgroup.CustomField9;
                dr[ItemsalestaxFields.CustomField10] = itemsalestaxgroup.CustomField10;
                dr[ItemsalestaxFields.Status] = itemsalestaxgroup.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
