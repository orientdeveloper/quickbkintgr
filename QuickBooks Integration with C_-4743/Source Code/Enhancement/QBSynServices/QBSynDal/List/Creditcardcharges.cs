//
// Class	:	Creditcardcreditcardcharges.cs
// Author	:  	SynapseIndia © 2013
// Date		:	10/4/2013 10:11:44 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;
namespace QBSynDal
{
	public class Creditcardcharges : AbstractDalBase
	{
		
      #region Constructors / Destructors
        public Creditcardcharges(string constr)
            : base(constr)
        {

        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                creditcardchargeTableAdapter _creditcardchargeTableAdapter = new creditcardchargeTableAdapter();
                _creditcardchargeTableAdapter.Connection = Connection;
                _creditcardchargeTableAdapter.Transaction = Transaction;

                if (synAction != SynAction.None)
                {
                    
                }

                foreach (Creditcardcharge creditcardcharge in this.Items)
                {
                    QBSynDal.List.QBSynDal.creditcardchargeDataTable creditcardchargeTable = _creditcardchargeTableAdapter.GetDataByTxnID(creditcardcharge.TxnID);
                    DataRow dr = null;

                    if (creditcardchargeTable.Rows.Count == 0)
                    {
                        dr = creditcardchargeTable.NewRow();
                        Bind(dr, creditcardcharge);
                        creditcardchargeTable.Rows.Add(dr);
                    }
                    else
                    {
                        dr = creditcardchargeTable.Rows[0];
                        Bind(dr, creditcardcharge);
                    }
                    _creditcardchargeTableAdapter.Update(creditcardchargeTable);
                }
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Private Method
        private void Bind(DataRow dr, Creditcardcharge creditcardcharge)
        {
            try
            {
                dr[CreditcardchargeFields.TxnID] = creditcardcharge.TxnID;
                dr[CreditcardchargeFields.EditSequence] = creditcardcharge.EditSequence;
                dr[CreditcardchargeFields.TimeCreated] = creditcardcharge.TimeCreated;
                dr[CreditcardchargeFields.TimeModified] = creditcardcharge.TimeModified;
                dr[CreditcardchargeFields.TxnNumber] = creditcardcharge.TxnNumber.GetDbType();
                dr[CreditcardchargeFields.AccountRef_ListID] = creditcardcharge.AccountRef_ListID;
                dr[CreditcardchargeFields.AccountRef_FullName] = creditcardcharge.AccountRef_FullName;
                dr[CreditcardchargeFields.PayeeEntityRef_ListID] = creditcardcharge.PayeeEntityRef_ListID;
                dr[CreditcardchargeFields.PayeeEntityRef_FullName] = creditcardcharge.PayeeEntityRef_FullName;
                dr[CreditcardchargeFields.TxnDate] = creditcardcharge.TxnDate.GetDbType();
                dr[CreditcardchargeFields.Amount] = creditcardcharge.Amount.GetDbType();
                dr[CreditcardchargeFields.Memo] = creditcardcharge.Memo;
                dr[CreditcardchargeFields.CurrencyRef_FullName] = creditcardcharge.CurrencyRef_FullName;
                dr[CreditcardchargeFields.CurrencyRef_ListID] = creditcardcharge.CurrencyRef_ListID;
                dr[CreditcardchargeFields.ExchangeRate] = creditcardcharge.ExchangeRate.GetDbType();
                dr[CreditcardchargeFields.AmountInHomeCurrency] = creditcardcharge.AmountInHomeCurrency.GetDbType();
                dr[CreditcardchargeFields.RefNumber] = creditcardcharge.RefNumber;
                dr[CreditcardchargeFields.IsTaxIncluded] = creditcardcharge.IsTaxIncluded.GetShort().GetDbType();
                dr[CreditcardchargeFields.SalesTaxCodeRef_ListID] = creditcardcharge.SalesTaxCodeRef_ListID;
                dr[CreditcardchargeFields.SalesTaxCodeRef_FullName] = creditcardcharge.SalesTaxCodeRef_FullName;
                dr[CreditcardchargeFields.CustomField1] = creditcardcharge.CustomField1;
                dr[CreditcardchargeFields.CustomField2] = creditcardcharge.CustomField2;
                dr[CreditcardchargeFields.CustomField3] = creditcardcharge.CustomField3;
                dr[CreditcardchargeFields.CustomField4] = creditcardcharge.CustomField4;
                dr[CreditcardchargeFields.CustomField5] = creditcardcharge.CustomField5;
                dr[CreditcardchargeFields.CustomField6] = creditcardcharge.CustomField6;
                dr[CreditcardchargeFields.CustomField7] = creditcardcharge.CustomField7;
                dr[CreditcardchargeFields.CustomField8] = creditcardcharge.CustomField8;
                dr[CreditcardchargeFields.CustomField9] = creditcardcharge.CustomField9;
                dr[CreditcardchargeFields.CustomField10] = creditcardcharge.CustomField10;
                dr[CreditcardchargeFields.Status] = creditcardcharge.Status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
		
	}
}
