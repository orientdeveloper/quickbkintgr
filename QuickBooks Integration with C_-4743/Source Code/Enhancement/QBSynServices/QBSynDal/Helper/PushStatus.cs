﻿public enum PushStatus
{
    NotPushed = 0,
    Pushed = 1,
    Error = 2
}