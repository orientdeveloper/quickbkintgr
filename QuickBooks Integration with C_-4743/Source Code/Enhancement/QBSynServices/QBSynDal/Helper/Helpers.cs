﻿using QBSynDal.List.QBSynDalTableAdapters;
using QBSynDal.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace QBSynDal.Helper
{
    public static class ExtensionMethods
    {
        public static bool IsNull(this object value)
        {
            return object.ReferenceEquals(value, null);
        }

        public static bool IsDBNull(this object value)
        {
            return object.ReferenceEquals(value, DBNull.Value);
        }

        public static object GetDbType(this object value)
        {
            if (object.ReferenceEquals(value, null))
            {
                return DBNull.Value;
            }
            else
            {
                return value;
            }
        }

        public static string ToStringCustom(this DateTime value)
        {
            return value.ToString();
        }

        public static short? GetShort(this bool? value)
        {
            if (value.HasValue)
            {
                return value.Value ? (short)1 : (short)0;
            }
            else
            {
                return null;
            }
        }
    }

    public class DeleteRecord : IDisposable
    {
        string tableName;
        string primaryKey;
        List<string> primarykeys;
        SynAction synAction;
        string Constr;
        SqlConnection connection;
        SqlTransaction Transaction;

        public DeleteRecord(string _tableName, string _primaryKey, List<string> _primarykeys, SynAction _synAction, string _Constr)
        {
            tableName = _tableName;
            primaryKey = _primaryKey;
            primarykeys = _primarykeys;
            synAction = _synAction;
            Constr = _Constr;
        }

        public bool Execute()
        {

            try
            {
                connection = new SqlConnection(Constr);
                connection.Open();
                Transaction = connection.BeginTransaction();
                SqlCommand command = new SqlCommand();
                command.Transaction = Transaction;
                command.Connection = connection;
                command.CommandType = System.Data.CommandType.Text;

                foreach (var keyvalue in primarykeys)
                {
                    command.CommandText = string.Format("DELETE FROM [{0}] WHERE [{1}] = '{2}'", tableName, primaryKey, keyvalue);
                    command.ExecuteNonQuery();
                }

                Transaction.Commit();
                return true;
            }
            catch (Exception ex)
            {
                Transaction.Rollback();
                throw ex;
            }
            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        public void Dispose()
        {
            connection = null;
            Transaction = null;
        }
    }

    public class MasterTable
    {
        public string TableName { get; set; }
        public string PrimaryKey { get; set; }
        public string Type { get; set; }
        public string DelType { get; set; }
        public List<ChildTable> Child { get; set; }
    }

    public class ChildTable
    {
        public string TableName { get; set; }
        public string ForeignKey { get; set; }
    }

    [DataContract]
    public class CredentialToken
    {
        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public Guid CompanyId{ get; set; }

        public string GetConnectionString()
        {
            try
            {
                string username = ConfigurationManager.AppSettings["UserName"];
                string pwd = ConfigurationManager.AppSettings["Password"];
                if (!(string.Compare(username, Username, true) == 0 && string.Compare(pwd, Password, false) == 0))
                {
                    throw new Exception("Unauthorised Access");
                }
                else
                {
                    using (dbCompanyEntities daobj = new dbCompanyEntities())
                    {
                        Company company = daobj.Companies.SingleOrDefault(s => s.CompanyId == CompanyId);
                        if (!object.ReferenceEquals(company, null))
                        {
                            return company.ConnectionString;
                        }
                        else
                        {
                            throw new Exception("Invalid company");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetCompanyName()
        {
            try
            {
                string username = ConfigurationManager.AppSettings["UserName"];
                string pwd = ConfigurationManager.AppSettings["Password"];
                if (!(string.Compare(username, Username, true) == 0 && string.Compare(pwd, Password, false) == 0))
                {
                    throw new Exception("Unauthorised Access");
                }
                else
                {
                    using (dbCompanyEntities daobj = new dbCompanyEntities())
                    {
                        Company company = daobj.Companies.SingleOrDefault(s => s.CompanyId == CompanyId);
                        if (!object.ReferenceEquals(company, null))
                        {
                            return company.CompanyName;
                        }
                        else
                        {
                            throw new Exception("Invalid company");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsValid()
        {
            try
            {
                return (string.Compare(ConfigurationManager.AppSettings["UserName"], Username, true) == 0 && string.Compare(ConfigurationManager.AppSettings["Password"], Password, false) == 0);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
