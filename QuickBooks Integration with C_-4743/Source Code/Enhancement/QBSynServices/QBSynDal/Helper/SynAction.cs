﻿using System.Runtime.Serialization;
namespace QBSynDal.Helper
{
    [DataContract]
    public enum SynAction
    {
        [EnumMember]
        Done = 0,
        [EnumMember]
        Initiate = 1,
        [EnumMember]
        None = 2,
        [EnumMember]
        DoneDelete = 3,
        [EnumMember]
        InitiateDelete = 4,
    }
}