﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace QBSynServices
{
    public class CredentialsExtractorBehaviorAttribute : Attribute, IContractBehavior, IDispatchMessageInspector
    {
        #region IContractBehavior
        public void AddBindingParameters(ContractDescription contractDescription, ServiceEndpoint endpoint, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
        {
            return;
        }

        public void ApplyClientBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            return;
        }

        public void ApplyDispatchBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, DispatchRuntime dispatchRuntime)
        {
            dispatchRuntime.MessageInspectors.Add(this);
        }

        public void Validate(ContractDescription contractDescription, ServiceEndpoint endpoint)
        {
            return;
        }
        #endregion

        #region IDispatchMessageInspector
        public object AfterReceiveRequest(ref System.ServiceModel.Channels.Message request, System.ServiceModel.IClientChannel channel, System.ServiceModel.InstanceContext instanceContext)
        {
            if (request.Headers.FindHeader("LiscenceKey", "SectionLiscenceKey") != -1 && request.Headers.FindHeader("CompanyId", "SectionCompanyId") != -1)
            {
                string liscenceKey = request.Headers.GetHeader<string>("LiscenceKey", "SectionLiscenceKey");
                string companyId = request.Headers.GetHeader<string>("CompanyId", "SectionCompanyId");
            }
            else
            {
                throw new Exception("Unauthorised Access");
            }
            return null;
        }

        public void BeforeSendReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
            return;
        }
        #endregion
    }
}