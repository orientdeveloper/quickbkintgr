﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace QBSynServices
{
    [DataContract]
    public class LogInfo
    {
        #region Public properties
        [DataMember]
        public string UserName
        {
            get
            {
                return userName;
            }
            set
            {
                userName = value;
            }
        }
        [DataMember]
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
            }
        }
        [DataMember]
        public string Level
        {
            get
            {
                return level;
            }
            set
            {
                level = value;
            }
        }
        [DataMember]
        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
            }
        }
        [DataMember]
        public string ExceptionMessage
        {
            get
            {
                return exceptionMessage;
            }
            set
            {
                exceptionMessage = value;
            }
        }
        [DataMember]
        public Guid Company
        {
            get
            {
                return company;
            }
            set
            {
                company = value;
            }
        }
        [DataMember]
        public DateTime TimeStamp
        {
            get
            {
                return timeStamp;
            }
            set
            {
                timeStamp = value;
            }
        }
        #endregion

        #region Private variables.
        private string userName;
        private string password;
        private string level;
        private string message;
        private string exceptionMessage;
        private Guid company;
        private DateTime timeStamp;
        #endregion
    }
}
