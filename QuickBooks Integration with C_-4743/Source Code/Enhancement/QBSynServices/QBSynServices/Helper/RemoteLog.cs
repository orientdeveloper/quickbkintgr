﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QBSynServices.Helper
{
    public class RemoteLog
    {
        public Guid GroupId { get; set; }
        public Guid Company { get; set; }
        public string Level { get; set; }
        public string Message { get; set; }
        public string Details { get; set; }
        public string StackTrace { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}