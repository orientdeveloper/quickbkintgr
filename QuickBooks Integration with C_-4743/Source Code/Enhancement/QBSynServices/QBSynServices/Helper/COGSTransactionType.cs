﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QBSynServices.Helper
{
    public class COGSTransactionType
    {
        public const string Invoice = "Invoice";
        public const string CreditMemo = "Credit Memo";
        public const string Bill = "Bill";
    }
}
