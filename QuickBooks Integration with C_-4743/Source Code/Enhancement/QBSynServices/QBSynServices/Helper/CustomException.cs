﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QBSynServices.Helper
{
    public class CustomException : Exception
    {
        public CustomException()
            : base()
        {

        }
        public CustomException(string Message)
            : base(Message)
        {
        }
    }
}