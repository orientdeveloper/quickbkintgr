﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using QBSynDal;
using QBSynServices.Helper;
using QBSynDal.Helper;
using System.Xml;

namespace QBSynServices
{
    [ServiceContract]
    public interface IQBService
    {
        [OperationContract(Name = "Validate")]
        LiscenseValidationResponse Validate(CredentialToken Token, string UserName, string Password, string LicenseKey, string FingerPrintData);

        [OperationContract(Name = "SetSynStatus")]
        void SetSynStatus(CredentialToken Token, bool Status);

        /// <summary>
        /// used for get the MasterchildRelationXml from the database. Credential Token must be true.
        /// </summary>
        /// <param name="Token"></param>
        /// <returns>string MasterChildRelationXML</returns>
        [OperationContract(Name = "GetMasterChildRelationXML")]
        string GetMasterChildRelationXML(CredentialToken Token);

        /// <summary>
        /// Set the Active status for the table through which we can handle the synchronization status of a table.
        /// </summary>
        /// <param name="Token"></param>
        /// <param name="IsActive"></param>
        /// <param name="TableId"></param>
        [OperationContract(Name = "SetActiveTableToSync")]
        void SetActiveTableToSync(CredentialToken Token, bool IsActive, int TableId);

        /// <summary>
        /// This method is used for delete the company from the Company table based on CompanyID, Credential Token must be true. 
        /// </summary>
        /// <param name="Token"></param>
        /// <param name="CompanyID"></param>
        [OperationContract(Name = "DeleteCompany")]
        int DeleteCompany(CredentialToken Token, Guid CompanyID);

        /// <summary>
        /// This method is used for counting the rows of all tables 
        /// </summary>
        /// <param name="companyid"></param>
        /// <returns></returns>
        [OperationContract(Name = "CountRemoteDBTablesRows")]
        List<RemoteDB> CountRemoteDBTablesRows(CredentialToken Token, Guid companyid);

        /// <summary>
        /// Resetting tables
        /// </summary>
        /// <param name="Token"></param>
        /// <param name="CompanyID"></param>
        /// <param name="ResetTableList"></param>
        [OperationContract(Name = "Resettable")]
        void ResetTable(CredentialToken Token, Guid CompanyID, List<string> ResetTableList);

        /// <summary>
        /// Use for store general log entry
        /// </summary>
        /// <param name="Token"></param>
        /// <param name="generalLogs"></param>
        /// <returns></returns>
        [OperationContract(Name = "SaveGeneralLog")]
        void SaveGeneralLog(CredentialToken Token, List<RemoteLog> generalLogs);

        /// <summary>
        /// Get all active plugins puchased by company
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        [OperationContract(Name = "GetActivePlugins")]
        List<Guid> GetActivePlugins(CredentialToken credential);

        [OperationContract(Name = "UploadLog")]
        void UploadLog(CredentialToken token, List<string> logs, string groupid, bool toclear);

        [OperationContract(Name = "UploadLocalTableCountLog")]
        void UploadLocalTableCountLog(CredentialToken token, List<RemoteDB> logs, string groupid);

        [OperationContract(Name = "WriteRemoteTableCountLog")]
        void WriteRemoteTableCountLog(CredentialToken token, List<RemoteDB> logs, string groupid);

        [OperationContract(Name = "GetReportSlotBackYear")]
        int GetReportSlotBackYear(CredentialToken Token);
    }
}
