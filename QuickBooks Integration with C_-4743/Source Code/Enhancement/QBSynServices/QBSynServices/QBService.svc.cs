﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using QBSynServices.ECFOWebService;
using QBSynDal;
using System.ServiceModel.Activation;
using System.Data;
using System.Data.Objects;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;
using QBSynServices.Helper;
using System.Configuration;
using QBSynDal.Helper;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Web;
using System.Text.RegularExpressions;
namespace QBSynServices
{
    [ServiceErrorBehavior(typeof(ErrorHandler))]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class QBService : IQBService
    {
        #region public methods

        public LiscenseValidationResponse Validate(CredentialToken Token, string UserName, string Password, string LicenseKey, string FingerPrintData)
        {
            try
            {
                if (!Token.IsValid())
                {
                    throw new CustomException("Unauthorised Access");
                }
                else
                {
                    using (dbCompanyEntities daobj = new dbCompanyEntities())
                    {
                        Company company = daobj.Companies.SingleOrDefault(s => string.Compare(s.LiscenceKey, LicenseKey, false) == 0);
                        if (!object.ReferenceEquals(company, null))
                        {
                            if (string.Compare(company.MachineFingerPrint, FingerPrintData, false) == 0)
                            {
                                return new LiscenseValidationResponse { CompanyId = company.CompanyId, CompanyName = company.CompanyName, IsValid = true, Message = null };
                            }
                            else
                            {
                                return new LiscenseValidationResponse { IsValid = false, CompanyId = Guid.Empty, CompanyName = null, Message = "Used license key." };
                            }
                        }
                        else
                        {
                            using (ECFOWebServiceTestSoapClient ecfoclient = new ECFOWebServiceTestSoapClient())
                            {
                                ECFOCustomer customer = ecfoclient.OrganizationDetails(LicenseKey, UserName, Password);

                                if (!customer.isSuccess)
                                {
                                    throw new CustomException(customer.Message);
                                }
                                else
                                {
                                    string ecfousername = ConfigurationManager.AppSettings["EcfoUserName"];
                                    string ecfopassword = ConfigurationManager.AppSettings["EcfoPassword"];

                                    string connectionstring = ecfoclient.GetConnectionString(LicenseKey, ecfousername, ecfopassword);
                                    Company instance = new Company
                                    {
                                        CompanyId = Guid.NewGuid(),
                                        CompanyName = customer.OrganizationName,
                                        ConnectionString = connectionstring,
                                        Date = DateTime.Now,
                                        LiscenceKey = LicenseKey,
                                        MachineFingerPrint = FingerPrintData,
                                        UserName = UserName
                                    };
                                    daobj.Companies.Add(instance);
                                    daobj.SaveChanges();

                                    return new LiscenseValidationResponse { CompanyId = instance.CompanyId, CompanyName = instance.CompanyName, IsValid = true, Message = null };
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CustomException exx = ex as CustomException;
                if (exx != null)
                {
                    throw exx;
                }
                else
                {
                    CommunicationException communicationex = ex as CommunicationException;
                    if (communicationex != null)
                    {
                        throw new CustomException("Server is busy, please try again.");
                    }
                    else
                    {
                        throw new CustomException("Some error has occured please try again.");
                    }
                }
            }
        }

        public void SetSynStatus(CredentialToken Token, bool status)
        {
            try
            {
                if (!Token.IsValid())
                {
                    throw new CustomException("Unauthorised Access");
                }
                else
                {
                    using (dbCompanyEntities daobj = new dbCompanyEntities())
                    {
                        Company company = daobj.Companies.SingleOrDefault(s => s.CompanyId == Token.CompanyId);
                        if (!object.ReferenceEquals(company, null))
                        {
                            using (ECFOWebServiceTestSoapClient client = new ECFOWebServiceTestSoapClient())
                            {
                                string ecfousername = ConfigurationManager.AppSettings["EcfoUserName"];
                                string ecfopassword = ConfigurationManager.AppSettings["EcfoPassword"];
                                client.UpdateSyncStatus(company.LiscenceKey, status, ecfousername, ecfopassword);
                                if (!status)
                                {
                                    ExecuteSSIS(company);
                                }
                                else
                                {
                                    ExecuteScript(Token.GetConnectionString());
                                }
                            }
                        }
                        else
                        {
                            throw new CustomException("Invalid company");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CustomException customex = ex as CustomException;
                if (customex != null)
                {
                    throw customex;
                }
                else
                {
                    CommunicationException communicationex = ex as CommunicationException;
                    if (communicationex != null)
                    {
                        throw new CustomException("Server is busy, please try again.");
                    }
                    else
                    {
                        throw new CustomException("Some error has occured please try again." + ex.Message);
                    }
                }
            }
        }

        public string GetMasterChildRelationXML(CredentialToken Token)
        {
            string xml = string.Empty;
            try
            {
                if (!Token.IsValid())
                {
                    throw new CustomException("Unauthorised Access");
                }
                else
                {
#if(!DEBUG)
                    XmlDocument xmldoc = new XmlDocument();
                    List<Parent> ParentList = new List<Parent>();
                    using (dbCompanyEntities daobj = new dbCompanyEntities())
                    {
                        var ParentQuery = daobj.Parents.Where(p => p.IsActive == true).Select(p => p).ToList();
                        foreach (var q in ParentQuery)
                        {
                            Parent p = new Parent();
                            p.ParentId = q.ParentID;
                            p.ParentTableName = q.ParentTableName;
                            p.PrimaryKey = q.PrimaryKey;
                            p.Type = q.Type;
                            p.DelType = q.DelType;
                            p.IsActive = q.IsActive;
                            ParentList.Add(p);
                        }
                        if (ParentList != null)
                        {
                            XmlDocument doc = new XmlDocument();
                            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
                            doc.AppendChild(docNode);

                            XmlNode MastersNode = doc.CreateElement("Masters");
                            doc.AppendChild(MastersNode);
                            foreach (Parent p in ParentList)
                            {
                                XmlNode MasterNode = doc.CreateElement("Master");

                                XmlAttribute TableNameAttribute = doc.CreateAttribute("TableName");
                                XmlAttribute PrimaryKeyAttribute = doc.CreateAttribute("PrimaryKey");
                                XmlAttribute TypeAttribute = doc.CreateAttribute("Type");
                                XmlAttribute DelTypeAttribute = doc.CreateAttribute("DelType");

                                TableNameAttribute.Value = p.ParentTableName;
                                PrimaryKeyAttribute.Value = p.PrimaryKey;
                                TypeAttribute.Value = p.Type;
                                DelTypeAttribute.Value = p.DelType;

                                MasterNode.Attributes.Append(TableNameAttribute);
                                MasterNode.Attributes.Append(PrimaryKeyAttribute);
                                MasterNode.Attributes.Append(TypeAttribute);
                                MasterNode.Attributes.Append(DelTypeAttribute);

                                MastersNode.AppendChild(MasterNode);

                                //Get Child tables for this parent

                                var ChildQuery = daobj.Children.Where(c => c.ParentID == p.ParentId).Select(c => c).ToList();

                                List<Child> ChildList = new List<Child>();
                                foreach (var c in ChildQuery)
                                {
                                    Child _child = new Child();
                                    _child.ChildTableName = c.ChildTableName;
                                    _child.ForeignKey = c.ForeignKey;
                                    ChildList.Add(_child);
                                }
                                if (ChildList.Count != 0)
                                {
                                    foreach (Child ch in ChildList)
                                    {
                                        XmlNode childNode = doc.CreateElement("Child");
                                        XmlAttribute CTableNameAttribute = doc.CreateAttribute("TableName");
                                        XmlAttribute ForeignKeyAttribute = doc.CreateAttribute("ForeignKey");
                                        CTableNameAttribute.Value = ch.ChildTableName;
                                        ForeignKeyAttribute.Value = ch.ForeignKey;

                                        childNode.Attributes.Append(CTableNameAttribute);
                                        childNode.Attributes.Append(ForeignKeyAttribute);
                                        MasterNode.AppendChild(childNode);
                                    }
                                }

                            }
                            xml = doc.InnerXml.ToString();
                        }
                    }
#endif

#if(DEBUG)
                    XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("MasterChildRelation.xml"));
                    xml = xdoc.ToString();
#endif
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public int GetReportSlotBackYear(CredentialToken Token)
        {
            try
            {
                if (!Token.IsValid())
                {
                    throw new CustomException("Unauthorised Access");
                }
                else
                {
                    string setting = GetSettings(string.Format("{0}-ReportSlotBackYear", Token.CompanyId));
                    if (string.IsNullOrEmpty(setting))
                    {
                        return int.Parse(ConfigurationManager.AppSettings["ReportSlotBackYear"]);
                    }
                    else
                    {
                        return int.Parse(setting);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SetActiveTableToSync(CredentialToken Token, bool IsActive, int TableId)
        {
            try
            {
                if (!Token.IsValid())
                {
                    throw new CustomException("Unauthorised Access");
                }
                else
                {
                    dbCompanyEntities context = new dbCompanyEntities();
                    var _parent = context.Parents.First(i => i.ParentID == TableId);
                    _parent.IsActive = IsActive;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GenerateFromXml()
        {
            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("MasterChildRelation.xml"));
            var relation = from s in xdoc.Descendants("Master")
                           select
                               new Parent { ParentId = 0, IsActive = true, ParentTableName = s.Attribute("TableName").Value, PrimaryKey = s.Attribute("PrimaryKey").Value, Type = s.Attribute("Type").Value, DelType = s.Attribute("DelType").Value, Child = (from p in s.Descendants("Child") select new Child { ChildTableName = p.Attribute("TableName").Value, ForeignKey = p.Attribute("ForeignKey").Value }).ToList() };

            using (dbCompanyEntities context = new dbCompanyEntities())
            {
                context.Database.ExecuteSqlCommand("delete from child");
                context.Database.ExecuteSqlCommand("delete from parent");
                foreach (var v in relation)
                {
                    QBSynDal.Helper.Parent instance = new QBSynDal.Helper.Parent();
                    instance.DelType = v.DelType;
                    instance.IsActive = v.IsActive;
                    instance.ParentTableName = v.ParentTableName;
                    instance.PrimaryKey = v.PrimaryKey;
                    instance.Type = v.Type;

                    foreach (var vv in v.Child)
                    {
                        instance.Children.Add(new QBSynDal.Helper.Child
                        {
                            ChildTableName = vv.ChildTableName,
                            ForeignKey = vv.ForeignKey,
                            IsActive = true,
                        });
                    }
                }

                context.SaveChanges();
            }
        }

        public int DeleteCompany(CredentialToken Token, Guid CompanyID)
        {
            try
            {
                int affectedRows = 0;
                if (!Token.IsValid())
                {
                    throw new CustomException("Unauthorised Access");
                }
                else
                {
                    dbCompanyEntities context = new dbCompanyEntities();

                    var deletedClientsExceptions = (from companyExceptions in context.ClientExceptions
                                                    where companyExceptions.CompanyId == CompanyID
                                                    select companyExceptions).ToList();
                    if (deletedClientsExceptions != null)
                    {
                        foreach (var dce in deletedClientsExceptions)
                        {
                            context.ClientExceptions.Remove(dce);
                        }
                    }
                    // Get the company to delete, by product ID.
                    var deletedComapany = (from company in context.Companies
                                           where company.CompanyId == CompanyID
                                           select company).SingleOrDefault();


                    if (deletedComapany != null)
                    {
                        // Mark the company for deletion.    
                        //Change the company name
                        //Enter timestamp in LiscenceKey field
                        deletedComapany.CompanyName = string.Format("{0} - {1}({2})", deletedComapany.CompanyName, "Deleted", Guid.NewGuid());
                        deletedComapany.LiscenceKey = DateTime.Now.ToString();
                        // Send the delete to the data service.
                        affectedRows = context.SaveChanges();
                    }

                    return affectedRows;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ResetTable(CredentialToken Token, Guid CompanyID, List<string> ResetTableList)
        {
            try
            {
                if (!Token.IsValid())
                {
                    throw new CustomException("Unauthorised Access");
                }
                else
                {
                    SqlConnection connection = new SqlConnection(Token.GetConnectionString());
                    connection.Open();
                    if (ResetTableList != null)
                    {
                        foreach (string table in ResetTableList)
                        {
                            string command = string.Format(@"DELETE from [{0}]", table.ToString());
                            SqlCommand sqlcommand = new SqlCommand(command, connection);
                            sqlcommand.ExecuteNonQuery();
                        }
                    }
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<RemoteDB> CountRemoteDBTablesRows(CredentialToken Token, Guid companyid)
        {
            try
            {
                if (!Token.IsValid())
                {
                    throw new CustomException("Unauthorised Access");
                }
                else
                {
                    List<RemoteDB> remotedbList = new List<RemoteDB>();
                    List<string> tables = new List<string>();

                    XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("MasterChildRelation.xml"));
                    tables = GetTables(xdoc.ToString());

                    SqlConnection connection = new SqlConnection(Token.GetConnectionString());
                    connection.Open();
                    string command = "SELECT ta.name TableName ,SUM(pa.rows) RowCnt FROM sys.tables ta INNER JOIN sys.partitions pa ON pa.OBJECT_ID = ta.OBJECT_ID INNER JOIN sys.schemas sc ON ta.schema_id = sc.schema_id WHERE ta.is_ms_shipped = 0 AND pa.index_id IN (1,0) GROUP BY sc.name,ta.name ORDER BY SUM(pa.rows) DESC";
                    DataSet _dstables = new DataSet();
                    SqlDataAdapter _sda = new SqlDataAdapter(command, connection);
                    _sda.Fill(_dstables);
                    if (_dstables.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < _dstables.Tables[0].Rows.Count; i++)
                        {

                            if (tables.Contains(_dstables.Tables[0].Rows[i]["TableName"].ToString()) == true)
                            {
                                if (_dstables.Tables[0].Rows[i]["TableName"].ToString().ToLower() == TableName.COGSReport.ToLower())
                                {
                                    {
                                        RemoteDB remote = new RemoteDB();
                                        command = string.Format("select count(*) from {0} where [type] = '{1}' OR [type] = '{2}'", TableName.COGSReport, COGSTransactionType.CreditMemo, COGSTransactionType.Invoice);
                                        SqlCommand sqlcommand = new SqlCommand(command, connection);
                                        object count = sqlcommand.ExecuteScalar();
                                        remote.TableName = TableName.COGSReport;
                                        remote.rowCount = Int32.Parse(count.ToString());
                                        remotedbList.Add(remote);
                                    }

                                    {
                                        RemoteDB remote = new RemoteDB();
                                        command = string.Format("select count(*) from {0} where [type] = '{1}'", TableName.COGSReport, COGSTransactionType.Bill);
                                        SqlCommand sqlcommand = new SqlCommand(command, connection);
                                        object count = sqlcommand.ExecuteScalar();
                                        remote.TableName = TableName.InventoryOversoldReport;
                                        remote.rowCount = Int32.Parse(count.ToString());
                                        remotedbList.Add(remote);
                                    }
                                }
                                else
                                {
                                    RemoteDB remote = new RemoteDB();
                                    remote.TableName = _dstables.Tables[0].Rows[i]["TableName"].ToString();
                                    remote.rowCount = Int32.Parse(_dstables.Tables[0].Rows[i]["RowCnt"].ToString());
                                    remotedbList.Add(remote);
                                }
                            }
                        }
                    }

                    connection.Close();
                    return remotedbList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveGeneralLog(CredentialToken Token, List<RemoteLog> generalLogs)
        {
            try
            {
                if (!Token.IsValid())
                {
                    throw new CustomException("Unauthorised Access");
                }
                else
                {
                    using (dbCompanyEntities context = new dbCompanyEntities())
                    {
                        foreach (RemoteLog remoteLog in generalLogs)
                        {
                            GeneralLog genralLog = new GeneralLog();
                            genralLog.GroupId = remoteLog.GroupId;
                            genralLog.Company = remoteLog.Company;
                            genralLog.Type = remoteLog.Level;
                            genralLog.Message = remoteLog.Message;
                            genralLog.Details = remoteLog.Details;
                            genralLog.StackTrace = remoteLog.StackTrace;
                            genralLog.TimeStamp = remoteLog.TimeStamp;
                            genralLog.EntryDate = System.DateTime.Now;
                            context.GeneralLogs.Add(genralLog);
                            context.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Guid> GetActivePlugins(CredentialToken credential)
        {
            List<Guid> activeplugins = new List<Guid>();
            activeplugins = ActivePlugins(credential);
            return activeplugins;
        }

        public void UploadLog(CredentialToken token, List<string> logs, string groupid, bool toclear)
        {
            try
            {
                if (token.IsValid())
                {
                    string basedir = Path.Combine(ConfigurationManager.AppSettings["ClientLogLocation"], string.Format("{1}-{0}", token.CompanyId.ToString(), RemoveSpecialCharacters(GetCompanyName(token))));
                    if (!Directory.Exists(basedir))
                    {
                        Directory.CreateDirectory(basedir);
                    }

                    string path = Path.Combine(basedir, string.Format("{0}.xml", groupid));
                    if (!File.Exists(path))
                    {
                        FileStream stream = File.Create(path);
                        stream.Close();
                    }
                    else
                    {
                        if (toclear)
                        {
                            File.Delete(path);
                            FileStream stream = File.Create(path);
                            stream.Close();
                        }
                    }

                    File.AppendAllLines(path, logs);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UploadLocalTableCountLog(CredentialToken token, List<RemoteDB> logs, string groupid)
        {
            try
            {
                if (token.IsValid())
                {
                    string basedir = Path.Combine(ConfigurationManager.AppSettings["ClientLogLocation"], string.Format("{1}-{0}", token.CompanyId.ToString(), RemoveSpecialCharacters(GetCompanyName(token))));
                    if (!Directory.Exists(basedir))
                    {
                        Directory.CreateDirectory(basedir);
                    }

                    string path = Path.Combine(basedir, string.Format("{0}{1}.csv", "LocalDBCount-", groupid));
                    if (!File.Exists(path))
                    {
                        FileStream stream = File.Create(path);
                        stream.Close();
                    }

                    var lines = new List<string>();

                    string[] columnNames = { "\"Table Name\"", "\"Local Database\"" };

                    var header = string.Join(",", columnNames);
                    lines.Add(header);

                    var valueLines = logs.Select(row => string.Join(",", new string[] { string.Format("\"{0}\"", row.TableName), string.Format("\"{0}\"", row.rowCount.ToString()) }));
                    lines.AddRange(valueLines);

                    File.WriteAllLines(path, lines);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void WriteRemoteTableCountLog(CredentialToken token, List<RemoteDB> logs, string groupid)
        {
            try
            {
                if (token.IsValid())
                {
                    string basedir = Path.Combine(ConfigurationManager.AppSettings["ClientLogLocation"], string.Format("{1}-{0}", token.CompanyId.ToString(), RemoveSpecialCharacters(GetCompanyName(token))));
                    if (!Directory.Exists(basedir))
                    {
                        Directory.CreateDirectory(basedir);
                    }

                    string path = Path.Combine(basedir, string.Format("{0}{1}.csv", "LocalRemoteDBCount-", groupid));
                    if (!File.Exists(path))
                    {
                        FileStream stream = File.Create(path);
                        stream.Close();
                    }

                    List<RemoteDB> countremotedbtablesrows = CountRemoteDBTablesRows(token, token.CompanyId);

                    var lines = new List<string>();

                    string[] columnNames = { "\"Table Name\"", "\"Remote Database\"", "\"Local Database\"" };

                    var header = string.Join(",", columnNames);
                    lines.Add(header);

                    var valueLines = logs
                        .Select(row => string.Join(",", new string[] { string.Format("\"{0}\"", row.TableName), string.Format("\"{0}\"", countremotedbtablesrows.Count(s => s.TableName.ToLower() == row.TableName.ToLower()) == 0 ? -1 : countremotedbtablesrows.First(s => s.TableName.ToLower() == row.TableName.ToLower()).rowCount), string.Format("\"{0}\"", row.rowCount.ToString()) }));
                    lines.AddRange(valueLines);

                    File.WriteAllLines(path, lines);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region private methods

        private string GetCompanyName(CredentialToken token)
        {
            try
            {
                string username = ConfigurationManager.AppSettings["UserName"];
                string pwd = ConfigurationManager.AppSettings["Password"];
                if (!(string.Compare(username, token.Username, true) == 0 && string.Compare(pwd, token.Password, false) == 0))
                {
                    throw new Exception("Unauthorised Access");
                }
                else
                {
                    using (dbCompanyEntities daobj = new dbCompanyEntities())
                    {
                        Company company = daobj.Companies.SingleOrDefault(s => s.CompanyId == token.CompanyId);
                        if (!object.ReferenceEquals(company, null))
                        {
                            return company.CompanyName;
                        }
                        else
                        {
                            throw new Exception("Invalid company");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string RemoveSpecialCharacters(string input)
        {
            Regex r = new Regex("(?:[^a-z0-9 ]|(?<=['\"])s)", RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled);
            return r.Replace(input, String.Empty);
        }

        private void ExecuteScript(string constr)
        {
            string script1 = @"alter table PayrollTransactionreport add AccountType varchar(max) NULL";
            string script2 = @"alter table COGSReport add AccountType varchar(max) NULL";
            string script3 = @"alter table TransferReport add AccountType varchar(max) NULL";
            string script4 = @"alter table TransferReport add modifiedtime datetime NULL";
            string script5 = @"alter table dataextdetail alter column DataExtValue varchar(max)";
            string script6 = @"CREATE TABLE BuildAssemblyReport(
	                            [TxnNumber] [int] NULL,
	                            [Type] [varchar](255) NULL,
	                            [Date] [datetime] NULL,
	                            [RefNumber] [varchar](250) NULL,
	                            [CustomerRef_ListID] [varchar](255) NULL,
	                            [Name] [varchar](1096) NULL,
	                            [Memo] [varchar](max) NULL,
	                            [ShipDate] [datetime] NULL,
	                            [DeliveryDate] [datetime] NULL,
	                            [FOB] [varchar](250) NULL,
	                            [ShipMethod] [varchar](250) NULL,
	                            [ItemRef_ListID] [varchar](255) NULL,
	                            [Item] [varchar](250) NULL,
	                            [AccountRef_ListID] [varchar](255) NULL,
	                            [Account] [varchar](255) NULL,
	                            [ClassRef_ListID] [varchar](255) NULL,
	                            [Class] [varchar](255) NULL,
	                            [SalesRep] [varchar](max) NULL,
	                            [SalesTaxCode] [varchar](250) NULL,
	                            [ClearedStatus] [varchar](250) NULL,
	                            [SplitAccount_ListID] [varchar](250) NULL,
	                            [SplitAccount] [varchar](250) NULL,
	                            [Quantity] [float] NULL,
	                            [Debit] [float] NULL,
	                            [Credit] [float] NULL,
	                            [ExchangeRate] [float] NULL,
	                            [ModifiedTime] [datetime] NULL,
	                            [Balance] [float] NULL,
	                            [AccountType] [varchar](50) NULL
                            ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]";

            try
            {
                SqlConnection connection = new SqlConnection(constr);
                connection.Open();
                SqlCommand command = new SqlCommand("select count(*) from BuildAssemblyReport", connection);
                command.ExecuteScalar();
            }
            catch
            {
                try
                {
                    SqlConnection connection = new SqlConnection(constr);
                    connection.Open();
                    {
                        try
                        {
                            SqlCommand command = new SqlCommand(script1, connection);
                            command.ExecuteScalar();
                        }
                        catch { }
                    }

                    {
                        try
                        {
                            SqlCommand command = new SqlCommand(script2, connection);
                            command.ExecuteScalar();
                        }
                        catch { }
                    }

                    {
                        try
                        {
                            SqlCommand command = new SqlCommand(script3, connection);
                            command.ExecuteScalar();
                        }
                        catch { }
                    }

                    {
                        try
                        {
                            SqlCommand command = new SqlCommand(script4, connection);
                            command.ExecuteScalar();
                        }
                        catch { }
                    }

                    {
                        try
                        {
                            SqlCommand command = new SqlCommand(script5, connection);
                            command.ExecuteScalar();
                        }
                        catch { }
                    }

                    {
                        try
                        {
                            SqlCommand command = new SqlCommand(script6, connection);
                            command.ExecuteScalar();
                        }
                        catch { }
                    }
                }
                catch
                {

                }
            }
        }

        private List<string> GetTables(string xml)
        {
            List<string> TableList = new List<string>();
            string _xml = xml;
            StringReader reader = new StringReader(_xml);
            XDocument xdoc = XDocument.Load(reader);
            var relation = (from s in xdoc.Descendants("Master")
                            select
                                new MasterTable { TableName = s.Attribute("TableName").Value, PrimaryKey = s.Attribute("PrimaryKey").Value, Type = s.Attribute("Type").Value, DelType = s.Attribute("DelType").Value, Child = (from p in s.Descendants("Child") select new ChildTable { TableName = p.Attribute("TableName").Value, ForeignKey = p.Attribute("ForeignKey").Value }).ToList() }).ToList();

            relation.ToList().ForEach(s =>
            {
                foreach (var child in s.Child)
                {
                    if (!TableList.Contains(child.TableName))
                    {
                        TableList.Add(child.TableName);
                    }
                }
                foreach (var master in relation)
                {
                    if (!TableList.Contains(master.TableName))
                    {
                        TableList.Add(master.TableName);
                    }
                }
            });
            return TableList;
        }

        private void ExecuteSSIS(Company company)
        {
            try
            {
                SqlConnection connection = null;
                using (dbCompanyEntities daobj = new dbCompanyEntities())
                {
                    string[] stringarray = company.ConnectionString.Split(';');
                    string[] stringarraysplit = stringarray[1].Split('=');
                    string databasename = stringarraysplit[1];

                    using (connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["QBSSISConStr"].ConnectionString))
                    {
                        using (SqlCommand command = new SqlCommand("sp_executessis", connection))
                        {
                            command.Parameters.AddWithValue("@databasename", databasename);
                            command.Parameters.AddWithValue("@companyid", company.CompanyId);
                            command.CommandType = CommandType.StoredProcedure;
                            connection.Open();
                            IAsyncResult result = command.BeginExecuteNonQuery();
                            while (!result.IsCompleted)
                            {

                            }
                            if (result.IsCompleted)
                            {
                                connection.Close();
                                using (ECFOWebServiceTestSoapClient client = new ECFOWebServiceTestSoapClient())
                                {
                                    string ecfousername = ConfigurationManager.AppSettings["EcfoUserName"];
                                    string ecfopassword = ConfigurationManager.AppSettings["EcfoPassword"];
                                    string key = company.LiscenceKey;
                                    string servername = connection.DataSource;

                                    client.processAllCubes(key, ecfousername, ecfopassword, servername, databasename);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private List<Guid> ActivePlugins(CredentialToken credential)
        {
            List<Guid> _activePlugins = new List<Guid>();
            try
            {
                string _licenseKey = string.Empty;
                string username = ConfigurationManager.AppSettings["UserName"];
                string pwd = ConfigurationManager.AppSettings["Password"];
                if (!(string.Compare(username, credential.Username, true) == 0 && string.Compare(pwd, credential.Password, false) == 0))
                {
                    throw new Exception("Unauthorised Access");
                }
                else
                {
                    using (dbCompanyEntities daobj = new dbCompanyEntities())
                    {
                        Company company = daobj.Companies.SingleOrDefault(s => s.CompanyId == credential.CompanyId);
                        if (!object.ReferenceEquals(company, null))
                        {
                            _licenseKey = company.LiscenceKey;

                            //Code for get plugins from the service based on licenseKey
                            using (ECFOWebServiceTestSoapClient client = new ECFOWebServiceTestSoapClient())
                            {
                                string ecfousername = ConfigurationManager.AppSettings["EcfoUserName"];
                                string ecfopassword = ConfigurationManager.AppSettings["EcfoPassword"];
                                DataTable dt = client.GetPluginProduct(_licenseKey, ecfousername, ecfopassword);
                                if (dt != null && dt.Rows.Count > 0 && dt.Columns.Cast<DataColumn>().Count(s => s.ColumnName == "PlugInGUID") == 1)
                                {
                                    foreach (DataRow dr in dt.Rows.Cast<DataRow>())
                                    {
                                        Guid pluginid = Guid.Empty;
                                        if (Guid.TryParse(dr["PlugInGUID"].ToString(), out pluginid))
                                        {
                                            _activePlugins.Add(pluginid);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            throw new Exception("Invalid company");
                        }
                    }
                }
                return _activePlugins;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private string GetSettings(string key)
        {
            try
            {
                string setting = string.Empty;
                dbCompanyEntities context = new dbCompanyEntities();
                setting = context.Settings.Where(x => x.Key == key).Select(x => x.Value).FirstOrDefault();
                return setting;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }

    #region classes

    public class Parent
    {
        public int ParentId { get; set; }
        public string ParentTableName { get; set; }
        public string PrimaryKey { get; set; }
        public string Type { get; set; }
        public string DelType { get; set; }
        public bool IsActive { get; set; }
        public List<Child> Child { get; set; }
    }

    public class Child
    {
        public int ChildId { get; set; }
        public int ParentId { get; set; }
        public string ChildTableName { get; set; }
        public string ForeignKey { get; set; }
    }

    [DataContract]
    public class RemoteDB
    {
        [DataMember]
        public string TableName { get; set; }
        [DataMember]
        public int rowCount { get; set; }
    }


    #endregion
}
