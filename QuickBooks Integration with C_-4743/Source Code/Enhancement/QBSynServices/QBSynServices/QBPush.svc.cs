﻿using QBSynDal.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using QBSynDal;
using System.ServiceModel.Activation;
using QBSynDal.Model;

namespace QBSynServices
{
    [ServiceErrorBehavior(typeof(ErrorHandler))]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class QBPush : IQBPush
    {
        public List<long> QBPushPurchaseOrderTxnID(CredentialToken credential)
        {
            try
            {
                QBPushPurchaseOrders _qbpushpurchaseorders = new QBPushPurchaseOrders(credential.GetConnectionString());
                return _qbpushpurchaseorders.PurchaseOrderTxnID();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public QBPushPurchaseorder GetQBPushPurchaseOrder(CredentialToken credential, long TxnID)
        {
            try
            {
                QBPushPurchaseOrders _qbpushpurchaseorders = new QBPushPurchaseOrders(credential.GetConnectionString());
                return _qbpushpurchaseorders.QBPushPurchaseOrder(TxnID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateQBPushPurchaseOrderStatus(CredentialToken credential, long TxnID, string ErrorMessage, PushStatus Status, DateTime PushDate)
        {
            try
            {
                QBPushPurchaseOrders _qbpushpurchaseorders = new QBPushPurchaseOrders(credential.GetConnectionString());
                return _qbpushpurchaseorders.PushStatusUpdate(TxnID, ErrorMessage, Status, PushDate);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<long> QBPushItemReceiptTxnID(CredentialToken credential)
        {

            try
            {
                QBPushItemReceipts _qbpushpurchaseorders = new QBPushItemReceipts(credential.GetConnectionString());
                return _qbpushpurchaseorders.ItemReceiptTxnID();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public QBSynDal.Model.QBPushItemReceipt GetQBPushItemReceipt(CredentialToken credential, long TxnID)
        {
            try
            {
                QBPushItemReceipts _qbitemreceipts = new QBPushItemReceipts(credential.GetConnectionString());
                return _qbitemreceipts.QBPushItemReceipt(TxnID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateQBPushItemReceiptStatus(CredentialToken credential, long TxnID, string ErrorMessage, PushStatus Status, DateTime PushDate)
        {
            try
            {
                QBPushItemReceipts _qbpushitemreceipts = new QBPushItemReceipts(credential.GetConnectionString());
                return _qbpushitemreceipts.PushStatusUpdate(TxnID, ErrorMessage, Status, PushDate);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
