﻿using QBSynDal.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;

namespace QBSynServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class LoggerService : ILoggerService
    {
        public void Log(LogInfo info)
        {
            if (string.Compare(ConfigurationManager.AppSettings["UserName"], info.UserName, true) == 0 && string.Compare(ConfigurationManager.AppSettings["Password"], info.Password, true) == 0)
            {
                using (dbCompanyEntities daobj = new dbCompanyEntities())
                {
                    Company company = daobj.Companies.SingleOrDefault(s => s.CompanyId == info.Company);
                    if (!object.ReferenceEquals(company, null))
                    {
                        company.ClientExceptions.Add(new ClientException { Dated = info.TimeStamp, ExceptionLevel = info.Level, ExceptionMessage = info.ExceptionMessage, Message = info.Message });
                        daobj.SaveChanges();
                        string IsSendInfoToMail = GetSettings("IsSendInfoToMail");
                        if (IsSendInfoToMail == "true" || info.Level.ToUpper() == "ERROR")
                        {
                            Sendmail(company, info);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Method : GetSettings
        /// Description : Used for get settings value.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string GetSettings(string key)
        {
            try
            {
                string setting = string.Empty;
                dbCompanyEntities context = new dbCompanyEntities();
                setting = context.Settings.Where(x => x.Key == key).Select(x => x.Value).FirstOrDefault();
                return setting;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void Sendmail(Company company, LogInfo log)
        {
            try
            {
                string emails = GetSettings("errorloggingemails");
                string[] errorloggingemails;
                List<string> toAddress = new List<string>();
                if (emails != null)
                {
                    errorloggingemails = emails.Split(',');
                    foreach (string e in errorloggingemails)
                    {
                        toAddress.Add(e);
                    }
                }
                else
                {
                    toAddress.Add("bugs@essentialcfo.com");
                }
                EmailSender sender = new EmailSender();

                string subject = string.Format("Error Log({0}) Dated:{1}", company.CompanyName, DateTime.Now.ToString());
                StringBuilder message = new StringBuilder();
                message.Append(string.Format("<h2>Company : {0}</h2><br/><br/>", company.CompanyName));
                message.Append(string.Format("<h2>{0}</h2><br/><br/>", log.Message));
                message.Append(string.Format("<p>Error:{0}</p>", log.ExceptionMessage));

                sender.SendEmail(subject, message.ToString(), toAddress);
            }
            catch
            {
            }
        }
    }
}
