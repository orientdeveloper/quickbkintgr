﻿using QBSynDal.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Xml.Linq;

namespace QBSynServices
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            GenerateFromXml();
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }

        public void GenerateFromXml()
        {
            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("MasterChildRelation.xml"));
            var relation = from s in xdoc.Descendants("Master")
                           select
                               new Parent { ParentId = 0, IsActive = true, ParentTableName = s.Attribute("TableName").Value, PrimaryKey = s.Attribute("PrimaryKey").Value, Type = s.Attribute("Type").Value, DelType = s.Attribute("DelType").Value, Child = (from p in s.Descendants("Child") select new Child { ChildTableName = p.Attribute("TableName").Value, ForeignKey = p.Attribute("ForeignKey").Value }).ToList() };

            using (dbCompanyEntities context = new dbCompanyEntities())
            {
                context.Database.ExecuteSqlCommand("delete from child");
                context.Database.ExecuteSqlCommand("delete from parent");
                foreach (var v in relation)
                {
                    QBSynDal.Helper.Parent instance = new QBSynDal.Helper.Parent();
                    instance.DelType = v.DelType;
                    instance.IsActive = v.IsActive;
                    instance.ParentTableName = v.ParentTableName;
                    instance.PrimaryKey = v.PrimaryKey;
                    instance.Type = v.Type;

                    foreach (var vv in v.Child)
                    {
                        instance.Children.Add(new QBSynDal.Helper.Child
                        {
                            ChildTableName = vv.ChildTableName,
                            ForeignKey = vv.ForeignKey,
                            IsActive = true,
                        });
                    }
                    context.Parents.Add(instance);
                }
                context.SaveChanges();
            }
        }
    }
}