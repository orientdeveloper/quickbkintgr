﻿using QBSynDal;
using QBSynDal.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace QBSynServices
{
    [ServiceContract]
    public interface IQBSyn
    {

        [OperationContract(Name = "DeleteSyn")]
        bool SynDelete(CredentialToken credential, string tableName, string primaryKey, List<string> primarykeys, SynAction synAction);

        [OperationContract(Name = "AccountSyn")]
        bool SynAccount(CredentialToken credential, List<Account> account, SynAction synAction);

        [OperationContract(Name = "AppliedtotxndetailsSyn")]
        bool SynAppliedtotxndetail(CredentialToken credential, List<Appliedtotxndetail> appliedtotxndetails, SynAction synAction);

        [OperationContract(Name = "ArrefundcreditcardSyn")]
        bool SynArrefundcreditcard(CredentialToken credential, List<Arrefundcreditcard> arrefundcreditcard, SynAction synAction);

        [OperationContract(Name = "BillSyn")]
        bool SynBill(CredentialToken credential, List<Bill> bill, SynAction synAction);

        [OperationContract(Name = "BillingrateSyn")]
        bool SynBillingrate(CredentialToken credential, List<Billingrate> billingrate, SynAction synAction);

        [OperationContract(Name = "BillingrateperitemdetailSyn")]
        bool SynBillingrateperitemdetail(CredentialToken credential, List<Billingrateperitemdetail> billingrateperitemdetail, SynAction synAction);

        [OperationContract(Name = "BillpaymentcheckSyn")]
        bool SynBillpaymentcheck(CredentialToken credential, List<Billpaymentcheck> billpaymentcheck, SynAction synAction);

        [OperationContract(Name = "BillpaymentcreditcardSyn")]
        bool SynBillpaymentcreditcard(CredentialToken credential, List<Billpaymentcreditcard> billpaymentcreditcard, SynAction synAction);

        [OperationContract(Name = "ChargeSyn")]
        bool SynCharge(CredentialToken credential, List<Charge> charge, SynAction synAction);

        [OperationContract(Name = "CheckSyn")]
        bool SynCheck(CredentialToken credential, List<Check> check, SynAction synAction);

        [OperationContract(Name = "ClassSyn")]
        bool SynClass(CredentialToken credential, List<Class> clas, SynAction synAction);

        [OperationContract(Name = "CurrencySyn")]
        bool SynCurrency(CredentialToken credential, List<Currency> currencies, SynAction synAction);

        [OperationContract(Name = "CreditcardchargeSyn")]
        bool SynCreditcardcharge(CredentialToken credential, List<Creditcardcharge> creditcardcharge, SynAction synAction);

        [OperationContract(Name = "CreditcardcreditSyn")]
        bool SynCreditcardcredit(CredentialToken credential, List<Creditcardcredit> creditcardcredit, SynAction synAction);

        [OperationContract(Name = "CreditmemoSyn")]
        bool SynCreditmemo(CredentialToken credential, List<Creditmemo> creditmemo, SynAction synAction);

        [OperationContract(Name = "CreditmemolinedetailSyn")]
        bool SynCreditmemolinedetail(CredentialToken credential, List<Creditmemolinedetail> creditmemolinedetail, SynAction synAction);

        [OperationContract(Name = "CustomerSyn")]
        bool SynCustomer(CredentialToken credential, List<Customer> customer, SynAction synAction);

        [OperationContract(Name = "CustomertypeSyn")]
        bool SynCustomerType(CredentialToken credential, List<Customertype> customer, SynAction synAction);

        [OperationContract(Name = "JobtypeSyn")]
        bool SynJobtype(CredentialToken credential, List<Jobtype> customer, SynAction synAction);

        [OperationContract(Name = "DepositSyn")]
        bool SynDeposit(CredentialToken credential, List<Deposit> deposit, SynAction synAction);

        [OperationContract(Name = "DepositlinedetailSyn")]
        bool SynDepositlinedetail(CredentialToken credential, List<Depositlinedetail> depositlinedetail, SynAction synAction);

        [OperationContract(Name = "EstimatelinedetailSyn")]
        bool SynEstimatelinedetail(CredentialToken credential, List<Estimatelinedetail> estimatelinedetail, SynAction synAction);

        [OperationContract(Name = "EstimateSyn")]
        bool SynEstimate(CredentialToken credential, List<Estimate> estimate, SynAction synAction);

        [OperationContract(Name = "InventoryadjustmentlinedetailSyn")]
        bool SynInventoryadjustmentlinedetail(CredentialToken credential, List<Inventoryadjustmentlinedetail> inventoryadjustmentlinedetail, SynAction synAction);

        [OperationContract(Name = "InventoryadjustmentSyn")]
        bool SynInventoryadjustment(CredentialToken credential, List<Inventoryadjustment> inventoryadjustment, SynAction synAction);

        [OperationContract(Name = "InvoicelinedetailSyn")]
        bool SynInvoicelinedetail(CredentialToken credential, List<Invoicelinedetail> invoicelinedetail, SynAction synAction);

        [OperationContract(Name = "ItemSyn")]
        bool SynInvoice(CredentialToken credential, List<Item> item, SynAction synAction);

        [OperationContract(Name = "ItemgroupSyn")]
        bool SynItemgroup(CredentialToken credential, List<Itemgroup> itemgroup, SynAction synAction);

        [OperationContract(Name = "ItemgroulinedetailSyn")]
        bool SynItemgroulinedetail(CredentialToken credential, List<Itemgrouplinedetail> itemgrouplinedetail, SynAction synAction);

        [OperationContract(Name = "InvoiceSyn")]
        bool SynInvoice(CredentialToken credential, List<Invoice> invoice, SynAction synAction);

        [OperationContract(Name = "ItemdiscountSyn")]
        bool SynItemdiscount(CredentialToken credential, List<Itemdiscount> itemdiscount, SynAction synAction);

        [OperationContract(Name = "ItemfixedassetSyn")]
        bool SynItemfixedasset(CredentialToken credential, List<Itemfixedasset> itemfixedasset, SynAction synAction);

        [OperationContract(Name = "IteminventorySyn")]
        bool SynIteminventory(CredentialToken credential, List<Iteminventory> iteminventory, SynAction synAction);

        [OperationContract(Name = "IteminventoryassemblySyn")]
        bool SynIteminventoryassembly(CredentialToken credential, List<Iteminventoryassembly> iteminventoryassembly, SynAction synAction);

        [OperationContract(Name = "ItemotherchargeSyn")]
        bool SynItemothercharge(CredentialToken credential, List<Itemothercharge> Itemothercharge, SynAction synAction);

        [OperationContract(Name = "ItempaymentSyn")]
        bool SynItempayment(CredentialToken credential, List<Itempayment> itempayment, SynAction synAction);

        [OperationContract(Name = "ItemreceiptSyn")]
        bool SynItemreceipt(CredentialToken credential, List<Itemreceipt> itemreceipt, SynAction synAction);

        [OperationContract(Name = "IteminventoriesSyn")]
        bool SynIteminventories(CredentialToken credential, List<Iteminventory> iteminventory, SynAction synAction);

        [OperationContract(Name = "ItemnoninventorySyn")]
        bool SynItemnoninventory(CredentialToken credential, List<Itemnoninventory> itemnoninventory, SynAction synAction);

        [OperationContract(Name = "ItemsalestaxSyn")]
        bool SynItemsalestax(CredentialToken credential, List<Itemsalestax> itemsalestax, SynAction synAction);

        [OperationContract(Name = "ItemsalestaxgroupSyn")]
        bool SynItemsalestaxgroup(CredentialToken credential, List<Itemsalestaxgroup> itemsalestaxgroup, SynAction synAction);

        [OperationContract(Name = "ItemsalestaxgrouplinedetailSyn")]
        bool SynItemsalestaxgrouplinedetail(CredentialToken credential, List<Itemsalestaxgrouplinedetail> itemsalestaxgrouplinedetail, SynAction synAction);

        [OperationContract(Name = "ItemserviceSyn")]
        bool SynItemservice(CredentialToken credential, List<Itemservice> itemservice, SynAction synAction);

        [OperationContract(Name = "ItemsubtotalSyn")]
        bool SynItemsubtotal(CredentialToken credential, List<Itemsubtotal> itemsubtotal, SynAction synAction);

        [OperationContract(Name = "JournalcreditlinedetailSyn")]
        bool SynJournalcreditlinedetail(CredentialToken credential, List<Journalcreditlinedetail> journalcreditlinedetail, SynAction synAction);

        [OperationContract(Name = "JournaldebitlinedetailSyn")]
        bool SynJournaldebitlinedetail(CredentialToken credential, List<Journaldebitlinedetail> journaldebitlinedetail, SynAction synAction);

        [OperationContract(Name = "JournalentrySyn")]
        bool SynJournalentry(CredentialToken credential, List<Journalentry> journalentry, SynAction synAction);

        [OperationContract(Name = "OthernameSyn")]
        bool SynOthername(CredentialToken credential, List<Othername> othername, SynAction synAction);

        [OperationContract(Name = "PurchaseorderSyn")]
        bool SynPurchaseorder(CredentialToken credential, List<Purchaseorder> purchaseorder, SynAction synAction);

        [OperationContract(Name = "PurchaseorderlinedetailSyn")]
        bool SynPurchaseorderlinedetail(CredentialToken credential, List<Purchaseorderlinedetail> purchaseorderlinedetail, SynAction synAction);

        [OperationContract(Name = "PurchaseorderlinegroupdetailSyn")]
        bool SynPurchaseorderlinegroupdetail(CredentialToken credential, List<Purchaseorderlinegroupdetail> purchaseorderlinegroupdetailList, SynAction synAction);

        [OperationContract(Name = "ReceivepaymentSyn")]
        bool SynReceivepayment(CredentialToken credential, List<Receivepayment> receivepayment, SynAction synAction);

        [OperationContract(Name = "RefundappliedtotxndetailSyn")]
        bool SynRefundappliedtotxndetail(CredentialToken credential, List<Refundappliedtotxndetail> refundappliedtotxndetail, SynAction synAction);

        [OperationContract(Name = "SalesandpurchasedetailSyn")]
        bool SynSalesandpurchasedetail(CredentialToken credential, List<Salesandpurchasedetail> salesandpurchasedetail, SynAction synAction);

        [OperationContract(Name = "SalesorpurchasedetailSyn")]
        bool SynSalesorpurchasedetail(CredentialToken credential, List<Salesorpurchasedetail> salesorpurchasedetail, SynAction synAction);

        [OperationContract(Name = "SalestaxcodeSyn")]
        bool SynSalestaxcode(CredentialToken credential, List<Salestaxcode> salestaxcode, SynAction synAction);

        [OperationContract(Name = "SalesreceiptSyn")]
        bool SynSalesreceipt(CredentialToken credential, List<Salesreceipt> salesreceipt, SynAction synAction);

        [OperationContract(Name = "SalesreceiptlinedetailSyn")]
        bool SynSalesreceiptlinedetail(CredentialToken credential, List<Salesreceiptlinedetail> salesreceiptlinedetail, SynAction synAction);

        [OperationContract(Name = "SalestaxpaymentcheckSyn")]
        bool SynSalestaxpaymentcheck(CredentialToken credential, List<Salestaxpaymentcheck> salestaxpaymentcheck, SynAction synAction);

        [OperationContract(Name = "SalestaxpaymentchecklinedetailSyn")]
        bool SynSalestaxpaymentchecklinedetail(CredentialToken credential, List<Salestaxpaymentchecklinedetail> salestaxpaymentchecklinedetail, SynAction synAction);

        [OperationContract(Name = "TimetrackingSyn")]
        bool SynTimetracking(CredentialToken credential, List<Timetracking> timetracking, SynAction synAction);

        [OperationContract(Name = "TransferSyn")]
        bool SynTransfer(CredentialToken credential, List<Transfer> transfer, SynAction synAction);

        [OperationContract(Name = "TransactionSyn")]
        bool SynTransaction(CredentialToken credential, List<Transaction> transaction, SynAction synAction);

        [OperationContract(Name = "TxnexpenselinedetailSyn")]
        bool SynTxnexpenselinedetail(CredentialToken credential, List<Txnexpenselinedetail> txnexpenselinedetail, SynAction synAction);

        [OperationContract(Name = "TxnitemlinedetailSyn")]
        bool SynTxnitemlinedetail(CredentialToken credential, List<Txnitemlinedetail> txnitemlinedetail, SynAction synAction);

        [OperationContract(Name = "TxnitemgrouplinedetailSyn")]
        bool SynTxnitemgrouplinedetail(CredentialToken credential, List<Txnitemgrouplinedetail> txnitemlinedetail, SynAction synAction);

        [OperationContract(Name = "VendorSyn")]
        bool SynVendor(CredentialToken credential, List<Vendor> vendor, SynAction synAction);

        [OperationContract(Name = "VendortypeSyn")]
        bool SynVendortype(CredentialToken credential, List<Vendortype> vendortype, SynAction synAction);

        [OperationContract(Name = "SalesrepSyn")]
        bool SynSalesrep(CredentialToken credential, List<Salesrep> salesrep, SynAction synAction);

        [OperationContract(Name = "InventorysiteSyn")]
        bool SynInventorysite(CredentialToken credential, List<InventorySite> inventorysite, SynAction synAction);

        [OperationContract(Name = "TermSyn")]
        bool SynTerms(CredentialToken credential, List<Term> term, SynAction synAction);

        [OperationContract(Name = "StandardTermSyn")]
        bool SynStandardTerms(CredentialToken credential, List<Standardterm> standardterm, SynAction synAction);

        [OperationContract(Name = "VendorcreditSyn")]
        bool SynVendorcredit(CredentialToken credential, List<Vendorcredit> vendorcredit, SynAction synAction);

        [OperationContract(Name = "PayrollTransactionReportSyn")]
        bool SynPayrollTransactionReport(CredentialToken credential, List<PayrollTransactionReport> payrolltransactionreport, DateTime? lastmodified, SynAction synAction);

        [OperationContract(Name = "BuildAssemblyReportSyn")]
        bool SynBuildAssemblyReport(CredentialToken credential, List<BuildAssemblyReport> buildassemblyreport, DateTime? lastmodified, SynAction synAction);

        [OperationContract(Name = "COGSReportSyn")]
        bool SynCOGSReport(CredentialToken credential, List<COGSReport> cogsreport, DateTime? lastmodified, SynAction synAction,List<string> transactiontype);

        [OperationContract(Name = "EmployeeSyn")]
        bool SynEmployee(CredentialToken credential, List<Employee> employee, SynAction synAction);

        [OperationContract(Name = "PayrollitemnonwageSyn")]
        bool SynPayrollitemnonwage(CredentialToken credential, List<Payrollitemnonwage> payrollitemnonwage, SynAction synAction);

        [OperationContract(Name = "PayrollitemwageSyn")]
        bool SynPayrollitemwage(CredentialToken credential, List<Payrollitemwage> payrollitemwage, SynAction synAction);

        [OperationContract(Name = "SalesorderSyn")]
        bool SynSalesorder(CredentialToken credential, List<Salesorder> salesorder, SynAction synAction);

        [OperationContract(Name = "SalesorderlinedetailSyn")]
        bool SynSalesorderlinedetail(CredentialToken credential, List<Salesorderlinedetail> salesorderlinedetail, SynAction synAction);

        [OperationContract(Name = "TransferReportSyn")]
        bool SynTransferReport(CredentialToken credential, List<TransferReport> transferreport, DateTime? lastmodified, SynAction synAction);

        [OperationContract(Name = "ECFO_SALESTAX_ACCOUNT_LKUPSyn")]
        bool SynECFO_SALESTAX_ACCOUNT_LKUP(CredentialToken credential, List<ECFO_SALESTAX_ACCOUNT_LKUP> ecfo, SynAction synAction);

        [OperationContract(Name = "LinkedtxndetailSyn")]
        bool SynLinkedtxndetail(CredentialToken credential, List<Linkedtxndetail> linkedtxndetail, SynAction synAction);

        [OperationContract(Name = "InvoicelinegroupdetailSyn")]
        bool SynInvoicelinegroupdetail(CredentialToken credential, List<Invoicelinegroupdetail> invoicelinegroupdetail, SynAction synAction);

        [OperationContract(Name = "ShipmethodSyn")]
        bool SynShipmethod(CredentialToken credential, List<Shipmethod> shipmethod, SynAction synAction);

        [OperationContract(Name = "EntitySyn")]
        bool SynEntity(CredentialToken credential, List<Entity> EntityList, SynAction synAction);

        [OperationContract(Name = "PricelevelSyn")]
        bool SynPricelevel(CredentialToken credential, List<Pricelevel> PricelevelList, SynAction synAction);

        [OperationContract(Name = "PricelevelperitemdetailSyn")]
        bool SynPricelevelperitemdetailSyn(CredentialToken credential, List<Pricelevelperitemdetail> PricelevelperitemdetailList, SynAction synAction);

        [OperationContract(Name = "DataExtDetailSyn")]
        bool SynDataExtDetail(CredentialToken credential, List<DataExtDetail> dataextdetailList, SynAction synAction);

        [OperationContract(Name = "FullSyn")]
        bool SynFull(CredentialToken credential);

        [OperationContract(Name = "TableFullSyn")]
        bool SynFullTable(CredentialToken credential,string tablename);
    }
}
