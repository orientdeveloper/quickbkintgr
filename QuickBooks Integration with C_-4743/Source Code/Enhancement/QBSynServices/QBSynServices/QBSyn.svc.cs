﻿using QBSynDal;
using QBSynDal.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using QBSynDal.Model;
using System.Data.SqlClient;
using QBSynDal.List.QBSynDalTableAdapters;
using System.Configuration;
using System.ServiceModel.Activation;
using System.Web;
using System.Xml.Linq;
using QBSynDal.List;
using System.Xml;
using System.IO;

namespace QBSynServices
{
    [ServiceErrorBehavior(typeof(ErrorHandler))]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class QBSyn : IQBSyn
    {
        #region Master Table
        public bool SynAccount(CredentialToken credential, List<Account> account, SynAction synAction)
        {
            try
            {
                using (Accounts accounts = new Accounts(credential.GetConnectionString()))
                {
                    accounts.Items.AddRange(account);
                    accounts.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynArrefundcreditcard(CredentialToken credential, List<Arrefundcreditcard> arrefundcreditcard, SynAction synAction)
        {
            try
            {
                using (Arrefundcreditcards arrefundcreditcards = new Arrefundcreditcards(credential.GetConnectionString()))
                {
                    arrefundcreditcards.Items.AddRange(arrefundcreditcard);
                    arrefundcreditcards.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynBill(CredentialToken credential, List<Bill> bill, SynAction synAction)
        {
            try
            {
                using (Bills bills = new Bills(credential.GetConnectionString()))
                {
                    bills.Items.AddRange(bill);
                    bills.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynBillingrate(CredentialToken credential, List<Billingrate> billingrate, SynAction synAction)
        {
            try
            {
                using (Billingrates billingrates = new Billingrates(credential.GetConnectionString()))
                {
                    billingrates.Items.AddRange(billingrate);
                    billingrates.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynBillpaymentcheck(CredentialToken credential, List<Billpaymentcheck> billpaymentcheck, SynAction synAction)
        {
            try
            {
                using (Billpaymentchecks billpaymentchecks = new Billpaymentchecks(credential.GetConnectionString()))
                {
                    billpaymentchecks.Items.AddRange(billpaymentcheck);
                    billpaymentchecks.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynBillpaymentcreditcard(CredentialToken credential, List<Billpaymentcreditcard> billpaymentcreditcard, SynAction synAction)
        {
            try
            {
                using (Billpaymentcreditcards billpaymentcreditcards = new Billpaymentcreditcards(credential.GetConnectionString()))
                {
                    billpaymentcreditcards.Items.AddRange(billpaymentcreditcard);
                    billpaymentcreditcards.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynCharge(CredentialToken credential, List<Charge> charge, SynAction synAction)
        {
            try
            {
                using (Charges charges = new Charges(credential.GetConnectionString()))
                {
                    charges.Items.AddRange(charge);
                    charges.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynCheck(CredentialToken credential, List<Check> check, SynAction synAction)
        {
            try
            {
                using (Checks checks = new Checks(credential.GetConnectionString()))
                {
                    checks.Items.AddRange(check);
                    checks.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool SynCurrency(CredentialToken credential, List<Currency> currencies, SynAction synAction)
        {
            try
            {
                using (Currencies currency = new Currencies(credential.GetConnectionString()))
                {
                    currency.Items.AddRange(currencies);
                    currency.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool SynClass(CredentialToken credential, List<Class> clas, SynAction synAction)
        {
            try
            {
                using (Classes classes = new Classes(credential.GetConnectionString()))
                {
                    classes.Items.AddRange(clas);
                    classes.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynCreditcardcharge(CredentialToken credential, List<Creditcardcharge> creditcardcharge, SynAction synAction)
        {
            try
            {
                using (Creditcardcharges creditcardcharges = new Creditcardcharges(credential.GetConnectionString()))
                {
                    creditcardcharges.Items.AddRange(creditcardcharge);
                    creditcardcharges.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynCreditcardcredit(CredentialToken credential, List<Creditcardcredit> creditcardcredit, SynAction synAction)
        {
            try
            {
                using (Creditcardcredits creditcardcredits = new Creditcardcredits(credential.GetConnectionString()))
                {
                    creditcardcredits.Items.AddRange(creditcardcredit);
                    creditcardcredits.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynCreditmemo(CredentialToken credential, List<Creditmemo> creditmemo, SynAction synAction)
        {
            try
            {
                using (Creditmemos creditmemos = new Creditmemos(credential.GetConnectionString()))
                {
                    creditmemos.Items.AddRange(creditmemo);
                    creditmemos.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynCustomer(CredentialToken credential, List<Customer> customer, SynAction synAction)
        {
            try
            {
                using (Customers customers = new Customers(credential.GetConnectionString()))
                {
                    customers.Items.AddRange(customer);
                    customers.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynCustomerType(CredentialToken credential, List<Customertype> customer, SynAction synAction)
        {
            try
            {
                using (Customertypes customers = new Customertypes(credential.GetConnectionString()))
                {
                    customers.Items.AddRange(customer);
                    customers.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynJobtype(CredentialToken credential, List<Jobtype> customer, SynAction synAction)
        {
            try
            {
                using (Jobtypes customers = new Jobtypes(credential.GetConnectionString()))
                {
                    customers.Items.AddRange(customer);
                    customers.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynDeposit(CredentialToken credential, List<Deposit> deposit, SynAction synAction)
        {
            try
            {
                using (Deposits deposits = new Deposits(credential.GetConnectionString()))
                {
                    deposits.Items.AddRange(deposit);
                    deposits.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynEstimate(CredentialToken credential, List<Estimate> estimate, SynAction synAction)
        {
            try
            {
                using (Estimates estimates = new Estimates(credential.GetConnectionString()))
                {
                    estimates.Items.AddRange(estimate);
                    estimates.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynInventoryadjustmentlinedetail(CredentialToken credential, List<Inventoryadjustmentlinedetail> inventoryadjustmentlinedetail, SynAction synAction)
        {
            try
            {
                using (Inventoryadjustmentlinedetails inventoryadjustmentlinedetails = new Inventoryadjustmentlinedetails(credential.GetConnectionString()))
                {
                    inventoryadjustmentlinedetails.Items.AddRange(inventoryadjustmentlinedetail);
                    inventoryadjustmentlinedetails.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynItemgroup(CredentialToken credential, List<Itemgroup> itemgroup, SynAction synAction)
        {
            try
            {
                using (Itemgroups itemgroups = new Itemgroups(credential.GetConnectionString()))
                {
                    itemgroups.Items.AddRange(itemgroup);
                    itemgroups.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynInventoryadjustment(CredentialToken credential, List<Inventoryadjustment> inventoryadjustment, SynAction synAction)
        {
            try
            {
                using (Inventoryadjustments inventoryadjustments = new Inventoryadjustments(credential.GetConnectionString()))
                {
                    inventoryadjustments.Items.AddRange(inventoryadjustment);
                    inventoryadjustments.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynInvoice(CredentialToken credential, List<Invoice> invoice, SynAction synAction)
        {
            try
            {
                using (Invoices invoices = new Invoices(credential.GetConnectionString()))
                {
                    invoices.Items.AddRange(invoice);
                    invoices.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynInvoice(CredentialToken credential, List<Item> item, SynAction synAction)
        {
            try
            {
                using (Items items = new Items(credential.GetConnectionString()))
                {
                    items.Items.AddRange(item);
                    items.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynItemdiscount(CredentialToken credential, List<Itemdiscount> itemdiscount, SynAction synAction)
        {
            try
            {
                using (Itemdiscounts itemdiscounts = new Itemdiscounts(credential.GetConnectionString()))
                {
                    itemdiscounts.Items.AddRange(itemdiscount);
                    itemdiscounts.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynItemfixedasset(CredentialToken credential, List<Itemfixedasset> itemfixedasset, SynAction synAction)
        {
            try
            {
                using (Itemfixedassets itemfixedassets = new Itemfixedassets(credential.GetConnectionString()))
                {
                    itemfixedassets.Items.AddRange(itemfixedasset);
                    itemfixedassets.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynIteminventory(CredentialToken credential, List<Iteminventory> iteminventory, SynAction synAction)
        {
            try
            {
                using (Iteminventories iteminventories = new Iteminventories(credential.GetConnectionString()))
                {
                    iteminventories.Items.AddRange(iteminventory);
                    iteminventories.SaveAll(synAction);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynItemnoninventory(CredentialToken credential, List<Itemnoninventory> itemnoninventory, SynAction synAction)
        {
            try
            {
                using (Itemnoninventories itemnoninventories = new Itemnoninventories(credential.GetConnectionString()))
                {
                    itemnoninventories.Items.AddRange(itemnoninventory);
                    itemnoninventories.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynIteminventoryassembly(CredentialToken credential, List<Iteminventoryassembly> iteminventoryassembly, SynAction synAction)
        {
            try
            {
                using (Iteminventoryassemblyassemblies iteminventoryassemblyassemblies = new Iteminventoryassemblyassemblies(credential.GetConnectionString()))
                {
                    iteminventoryassemblyassemblies.Items.AddRange(iteminventoryassembly);
                    iteminventoryassemblyassemblies.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynItemothercharge(CredentialToken credential, List<Itemothercharge> Itemothercharge, SynAction synAction)
        {
            try
            {
                using (Itemothercharges itemothercharges = new Itemothercharges(credential.GetConnectionString()))
                {
                    itemothercharges.Items.AddRange(Itemothercharge);
                    itemothercharges.SaveAll(synAction);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynItempayment(CredentialToken credential, List<Itempayment> itempayment, SynAction synAction)
        {
            try
            {
                using (Itempayments itempayments = new Itempayments(credential.GetConnectionString()))
                {
                    itempayments.Items.AddRange(itempayment);
                    itempayments.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynItemreceipt(CredentialToken credential, List<Itemreceipt> itemreceipt, SynAction synAction)
        {
            try
            {
                using (Itemreceipts itemreceipts = new Itemreceipts(credential.GetConnectionString()))
                {
                    itemreceipts.Items.AddRange(itemreceipt);
                    itemreceipts.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynItemsalestax(CredentialToken credential, List<Itemsalestax> itemsalestax, SynAction synAction)
        {
            try
            {
                using (Itemsalestaxs itemsalestaxs = new Itemsalestaxs(credential.GetConnectionString()))
                {
                    itemsalestaxs.Items.AddRange(itemsalestax);
                    itemsalestaxs.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynItemsalestaxgroup(CredentialToken credential, List<Itemsalestaxgroup> itemsalestaxgroup, SynAction synAction)
        {
            try
            {
                using (Itemsalestaxgroups itemsalestaxgroups = new Itemsalestaxgroups(credential.GetConnectionString()))
                {
                    itemsalestaxgroups.Items.AddRange(itemsalestaxgroup);
                    itemsalestaxgroups.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynItemservice(CredentialToken credential, List<Itemservice> itemservice, SynAction synAction)
        {
            try
            {
                using (Itemservices itemservices = new Itemservices(credential.GetConnectionString()))
                {
                    itemservices.Items.AddRange(itemservice);
                    itemservices.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynItemsubtotal(CredentialToken credential, List<Itemsubtotal> itemsubtotal, SynAction synAction)
        {
            try
            {
                using (Itemsubtotals itemsubtotals = new Itemsubtotals(credential.GetConnectionString()))
                {
                    itemsubtotals.Items.AddRange(itemsubtotal);
                    itemsubtotals.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynJournalentry(CredentialToken credential, List<Journalentry> journalentry, SynAction synAction)
        {
            try
            {
                using (Journalentries journalentries = new Journalentries(credential.GetConnectionString()))
                {
                    journalentries.Items.AddRange(journalentry);
                    journalentries.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynOthername(CredentialToken credential, List<Othername> othername, SynAction synAction)
        {
            try
            {
                using (Othernames othernames = new Othernames(credential.GetConnectionString()))
                {
                    othernames.Items.AddRange(othername);
                    othernames.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynPurchaseorder(CredentialToken credential, List<Purchaseorder> purchaseorder, SynAction synAction)
        {
            try
            {
                using (Purchaseorders purchaseorders = new Purchaseorders(credential.GetConnectionString()))
                {
                    purchaseorders.Items.AddRange(purchaseorder);
                    purchaseorders.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynReceivepayment(CredentialToken credential, List<Receivepayment> receivepayment, SynAction synAction)
        {
            try
            {
                using (Receivepayments receivepayments = new Receivepayments(credential.GetConnectionString()))
                {
                    receivepayments.Items.AddRange(receivepayment);
                    receivepayments.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynSalestaxcode(CredentialToken credential, List<Salestaxcode> salestaxcode, SynAction synAction)
        {
            try
            {
                using (Salestaxcodes salestaxcodes = new Salestaxcodes(credential.GetConnectionString()))
                {
                    salestaxcodes.Items.AddRange(salestaxcode);
                    salestaxcodes.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynSalesreceipt(CredentialToken credential, List<Salesreceipt> salesreceipt, SynAction synAction)
        {
            try
            {
                using (Salesreceipts salesreceipts = new Salesreceipts(credential.GetConnectionString()))
                {
                    salesreceipts.Items.AddRange(salesreceipt);
                    salesreceipts.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynSalestaxpaymentcheck(CredentialToken credential, List<Salestaxpaymentcheck> salestaxpaymentcheck, SynAction synAction)
        {
            try
            {
                using (Salestaxpaymentchecks salestaxpaymentchecks = new Salestaxpaymentchecks(credential.GetConnectionString()))
                {
                    salestaxpaymentchecks.Items.AddRange(salestaxpaymentcheck);
                    salestaxpaymentchecks.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynTimetracking(CredentialToken credential, List<Timetracking> timetracking, SynAction synAction)
        {
            try
            {
                using (Timetrackings timetrackings = new Timetrackings(credential.GetConnectionString()))
                {
                    timetrackings.Items.AddRange(timetracking);
                    timetrackings.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynVendorcredit(CredentialToken credential, List<Vendorcredit> vendorcredit, SynAction synAction)
        {
            try
            {
                using (Vendorcredits vendorcredits = new Vendorcredits(credential.GetConnectionString()))
                {
                    vendorcredits.Items.AddRange(vendorcredit);
                    vendorcredits.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynTransfer(CredentialToken credential, List<Transfer> transfer, SynAction synAction)
        {
            try
            {
                using (Transfers transafers = new Transfers(credential.GetConnectionString()))
                {
                    transafers.Items.AddRange(transfer);
                    transafers.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynTransaction(CredentialToken credential, List<Transaction> transaction, SynAction synAction)
        {
            try
            {
                using (Transactions transactions = new Transactions(credential.GetConnectionString()))
                {
                    transactions.Items.AddRange(transaction);
                    transactions.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynIteminventories(CredentialToken credential, List<Iteminventory> iteminventories, SynAction synAction)
        {
            try
            {
                using (Iteminventories iteminventory = new Iteminventories(credential.GetConnectionString()))
                {
                    iteminventory.Items.AddRange(iteminventories);
                    iteminventory.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynPayrollitemwage(CredentialToken credential, List<Payrollitemwage> payrollitemwage, SynAction synAction)
        {
            try
            {
                using (Payrollitemwages payrollitemwages = new Payrollitemwages(credential.GetConnectionString()))
                {
                    payrollitemwages.Items.AddRange(payrollitemwage);
                    payrollitemwages.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynEmployee(CredentialToken credential, List<Employee> employee, SynAction synAction)
        {
            try
            {
                using (Employees employees = new Employees(credential.GetConnectionString()))
                {
                    employees.Items.AddRange(employee);
                    employees.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynPayrollitemnonwage(CredentialToken credential, List<Payrollitemnonwage> payrollitemnonwage, SynAction synAction)
        {
            try
            {
                using (Payrollitemnonwages payrollitemnonwages = new Payrollitemnonwages(credential.GetConnectionString()))
                {
                    payrollitemnonwages.Items.AddRange(payrollitemnonwage);
                    payrollitemnonwages.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynSalesorder(CredentialToken credential, List<Salesorder> salesorder, SynAction synAction)
        {
            using (Salesorders salesorders = new Salesorders(credential.GetConnectionString()))
            {
                salesorders.Items.AddRange(salesorder);
                salesorders.SaveAll(synAction);
                return true;
            }
        }

        public bool SynVendor(CredentialToken credential, List<Vendor> vendor, SynAction synAction)
        {
            using (Vendors vendors = new Vendors(credential.GetConnectionString()))
            {
                vendors.Items.AddRange(vendor);
                vendors.SaveAll(synAction);
                return true;
            }
        }

        public bool SynVendortype(CredentialToken credential, List<Vendortype> vendortype, SynAction synAction)
        {
            using (VendorTypes vendortypes = new VendorTypes(credential.GetConnectionString()))
            {
                vendortypes.Items.AddRange(vendortype);
                vendortypes.SaveAll(synAction);
                return true;
            }
        }

        public bool SynSalesrep(CredentialToken credential, List<Salesrep> salesrep, SynAction synAction)
        {
            using (Salesreps salesreps = new Salesreps(credential.GetConnectionString()))
            {
                salesreps.Items.AddRange(salesrep);
                salesreps.SaveAll(synAction);
                return true;
            }
        }

        public bool SynInventorysite(CredentialToken credential, List<InventorySite> inventorysite, SynAction synAction)
        {
            using (InventorySites inventorysites = new InventorySites(credential.GetConnectionString()))
            {
                inventorysites.Items.AddRange(inventorysite);
                inventorysites.SaveAll(synAction);
                return true;
            }
        }

        public bool SynTerms(CredentialToken credential, List<Term> term, SynAction synAction)
        {
            using (Terms terms = new Terms(credential.GetConnectionString()))
            {
                terms.Items.AddRange(term);
                terms.SaveAll(synAction);
                return true;
            }
        }

        public bool SynStandardTerms(CredentialToken credential, List<Standardterm> standardterm, SynAction synAction)
        {
            using (Standardterms standardterms = new Standardterms(credential.GetConnectionString()))
            {
                standardterms.Items.AddRange(standardterm);
                standardterms.SaveAll(synAction);
                return true;
            }
        }

        public bool SynLinkedtxndetail(CredentialToken credential, List<Linkedtxndetail> linkedtxndetail, SynAction synAction)
        {
            using (Linkedtxndetails linkedtxndetails = new Linkedtxndetails(credential.GetConnectionString()))
            {
                linkedtxndetails.Items.AddRange(linkedtxndetail);
                linkedtxndetails.SaveAll(synAction);
                return true;
            }
        }

        public bool SynShipmethod(CredentialToken credential, List<Shipmethod> shipmethod, SynAction synAction)
        {
            using (Shipmethods shipmethods = new Shipmethods(credential.GetConnectionString()))
            {
                shipmethods.Items.AddRange(shipmethod);
                shipmethods.SaveAll(synAction);
                return true;
            }
        }

        public bool SynEntity(CredentialToken credential, List<Entity> EntityList, SynAction synAction)
        {
            using (Entities entities = new Entities(credential.GetConnectionString()))
            {
                entities.Items.AddRange(EntityList);
                entities.SaveAll(synAction);
                return true;
            }
        }

        public bool SynPricelevel(CredentialToken credential, List<Pricelevel> PricelevelList, SynAction synAction)
        {
            using (Pricelevels pricelevels = new Pricelevels(credential.GetConnectionString()))
            {
                pricelevels.Items.AddRange(PricelevelList);
                pricelevels.SaveAll(synAction);
                return true;
            }
        }
        #endregion

        #region Child Table
        public bool SynAppliedtotxndetail(CredentialToken credential, List<Appliedtotxndetail> appliedtotxndetail, SynAction synAction)
        {
            try
            {
                using (Appliedtotxndetails appliedtotxndetails = new Appliedtotxndetails(credential.GetConnectionString()))
                {
                    appliedtotxndetails.Items.AddRange(appliedtotxndetail);
                    appliedtotxndetails.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynBillingrateperitemdetail(CredentialToken credential, List<Billingrateperitemdetail> billingrateperitemdetail, SynAction synAction)
        {
            try
            {
                using (Billingrateperitemdetails billingrateperitemdetails = new Billingrateperitemdetails(credential.GetConnectionString()))
                {
                    billingrateperitemdetails.Items.AddRange(billingrateperitemdetail);
                    billingrateperitemdetails.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynCreditmemolinedetail(CredentialToken credential, List<Creditmemolinedetail> creditmemolinedetail, SynAction synAction)
        {
            try
            {
                using (Creditmemolinedetails creditmemolinedetails = new Creditmemolinedetails(credential.GetConnectionString()))
                {
                    creditmemolinedetails.Items.AddRange(creditmemolinedetail);
                    creditmemolinedetails.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynDepositlinedetail(CredentialToken credential, List<Depositlinedetail> depositlinedetail, SynAction synAction)
        {
            try
            {
                using (Depositlinedetails depositlinedetails = new Depositlinedetails(credential.GetConnectionString()))
                {
                    depositlinedetails.Items.AddRange(depositlinedetail);
                    depositlinedetails.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynEstimatelinedetail(CredentialToken credential, List<Estimatelinedetail> estimatelinedetail, SynAction synAction)
        {
            try
            {
                using (Estimatelinedetails estimatelinedetails = new Estimatelinedetails(credential.GetConnectionString()))
                {
                    estimatelinedetails.Items.AddRange(estimatelinedetail);
                    estimatelinedetails.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynInvoicelinedetail(CredentialToken credential, List<Invoicelinedetail> invoicelinedetail, SynAction synAction)
        {
            try
            {
                using (Invoicelinedetails invoicelinedetails = new Invoicelinedetails(credential.GetConnectionString()))
                {
                    invoicelinedetails.Items.AddRange(invoicelinedetail);
                    invoicelinedetails.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynItemsalestaxgrouplinedetail(CredentialToken credential, List<Itemsalestaxgrouplinedetail> itemsalestaxgrouplinedetail, SynAction synAction)
        {
            try
            {
                using (Itemsalestaxgrouplinedetails itemsalestaxgrouplinedetails = new Itemsalestaxgrouplinedetails(credential.GetConnectionString()))
                {
                    itemsalestaxgrouplinedetails.Items.AddRange(itemsalestaxgrouplinedetail);
                    itemsalestaxgrouplinedetails.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynJournalcreditlinedetail(CredentialToken credential, List<Journalcreditlinedetail> journalcreditlinedetail, SynAction synAction)
        {
            try
            {
                using (Journalcreditlinedetails journalcreditlinedetails = new Journalcreditlinedetails(credential.GetConnectionString()))
                {
                    journalcreditlinedetails.Items.AddRange(journalcreditlinedetail);
                    journalcreditlinedetails.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynJournaldebitlinedetail(CredentialToken credential, List<Journaldebitlinedetail> journaldebitlinedetail, SynAction synAction)
        {
            try
            {
                using (Journaldebitlinedetails journaldebitlinedetails = new Journaldebitlinedetails(credential.GetConnectionString()))
                {
                    journaldebitlinedetails.Items.AddRange(journaldebitlinedetail);
                    journaldebitlinedetails.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynPurchaseorderlinedetail(CredentialToken credential, List<Purchaseorderlinedetail> purchaseorderlinedetail, SynAction synAction)
        {
            try
            {
                using (Purchaseorderlinedetails purchaseorderlinedetails = new Purchaseorderlinedetails(credential.GetConnectionString()))
                {
                    purchaseorderlinedetails.Items.AddRange(purchaseorderlinedetail);
                    purchaseorderlinedetails.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynRefundappliedtotxndetail(CredentialToken credential, List<Refundappliedtotxndetail> refundappliedtotxndetail, SynAction synAction)
        {
            try
            {
                using (Refundappliedtotxndetails refundappliedtotxndetails = new Refundappliedtotxndetails(credential.GetConnectionString()))
                {
                    refundappliedtotxndetails.Items.AddRange(refundappliedtotxndetail);
                    refundappliedtotxndetails.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynSalesandpurchasedetail(CredentialToken credential, List<Salesandpurchasedetail> salesandpurchasedetail, SynAction synAction)
        {
            try
            {
                using (Salesandpurchasedetails salesandpurchasedetails = new Salesandpurchasedetails(credential.GetConnectionString()))
                {
                    salesandpurchasedetails.Items.AddRange(salesandpurchasedetail);
                    salesandpurchasedetails.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynSalesorpurchasedetail(CredentialToken credential, List<Salesorpurchasedetail> salesorpurchasedetail, SynAction synAction)
        {
            try
            {
                using (Salesorpurchasedetails salesorpurchasedetails = new Salesorpurchasedetails(credential.GetConnectionString()))
                {
                    salesorpurchasedetails.Items.AddRange(salesorpurchasedetail);
                    salesorpurchasedetails.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynSalestaxpaymentchecklinedetail(CredentialToken credential, List<Salestaxpaymentchecklinedetail> salestaxpaymentchecklinedetail, SynAction synAction)
        {
            try
            {
                using (Salestaxpaymentchecklinedetails salestaxpaymentchecklinedetails = new Salestaxpaymentchecklinedetails(credential.GetConnectionString()))
                {
                    salestaxpaymentchecklinedetails.Items.AddRange(salestaxpaymentchecklinedetail);
                    salestaxpaymentchecklinedetails.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynSalesreceiptlinedetail(CredentialToken credential, List<Salesreceiptlinedetail> salesreceiptlinedetail, SynAction synAction)
        {
            try
            {
                using (Salesreceiptlinedetails salesreceiptlinedetails = new Salesreceiptlinedetails(credential.GetConnectionString()))
                {
                    salesreceiptlinedetails.Items.AddRange(salesreceiptlinedetail);
                    salesreceiptlinedetails.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynTxnexpenselinedetail(CredentialToken credential, List<Txnexpenselinedetail> txnexpenselinedetail, SynAction synAction)
        {
            try
            {
                using (Txnexpenselinedetails txnexpenselinedetails = new Txnexpenselinedetails(credential.GetConnectionString()))
                {
                    txnexpenselinedetails.Items.AddRange(txnexpenselinedetail);
                    txnexpenselinedetails.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynTxnitemlinedetail(CredentialToken credential, List<Txnitemlinedetail> txnitemlinedetail, SynAction synAction)
        {
            try
            {
                using (Txnitemlinedetails txnitemlinedetails = new Txnitemlinedetails(credential.GetConnectionString()))
                {
                    txnitemlinedetails.Items.AddRange(txnitemlinedetail);
                    txnitemlinedetails.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynTxnitemgrouplinedetail(CredentialToken credential, List<Txnitemgrouplinedetail> txnitemgrouplinedetail, SynAction synAction)
        {
            try
            {
                using (Txnitemgrouplinedetails txnitemgrouplinedetails = new Txnitemgrouplinedetails(credential.GetConnectionString()))
                {
                    txnitemgrouplinedetails.Items.AddRange(txnitemgrouplinedetail);
                    txnitemgrouplinedetails.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynSalesorderlinedetail(CredentialToken credential, List<Salesorderlinedetail> salesorderlinedetail, SynAction synAction)
        {
            try
            {
                using (Salesorderlinedetails salesorderlinedetails = new Salesorderlinedetails(credential.GetConnectionString()))
                {
                    salesorderlinedetails.Items.AddRange(salesorderlinedetail);
                    salesorderlinedetails.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynInvoicelinegroupdetail(CredentialToken credential, List<Invoicelinegroupdetail> invoicelinegroupdetail, SynAction synAction)
        {
            try
            {
                using (Invoicelinegroupdetails invoicelinegroupdetails = new Invoicelinegroupdetails(credential.GetConnectionString()))
                {
                    invoicelinegroupdetails.Items.AddRange(invoicelinegroupdetail);
                    invoicelinegroupdetails.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynPricelevelperitemdetailSyn(CredentialToken credential, List<Pricelevelperitemdetail> PricelevelperitemdetailList, SynAction synAction)
        {
            try
            {
                using (Pricelevelperitemdetails pricelevelperitemdetails = new Pricelevelperitemdetails(credential.GetConnectionString()))
                {
                    pricelevelperitemdetails.Items.AddRange(PricelevelperitemdetailList);
                    pricelevelperitemdetails.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynPurchaseorderlinegroupdetail(CredentialToken credential, List<Purchaseorderlinegroupdetail> purchaseorderlinegroupdetailList, SynAction synAction)
        {
            try
            {
                using (Purchaseorderlinegroupdetails purchaseorderlinegroupdetails = new Purchaseorderlinegroupdetails(credential.GetConnectionString()))
                {
                    purchaseorderlinegroupdetails.Items.AddRange(purchaseorderlinegroupdetailList);
                    purchaseorderlinegroupdetails.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynItemgroulinedetail(CredentialToken credential, List<Itemgrouplinedetail> itemgrouplinedetail, SynAction synAction)
        {
            try
            {
                using (Itemgrouplinedetails itemgrouplinedetails = new Itemgrouplinedetails(credential.GetConnectionString()))
                {
                    itemgrouplinedetails.Items.AddRange(itemgrouplinedetail);
                    itemgrouplinedetails.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynDataExtDetail(CredentialToken credential, List<DataExtDetail> dataextdetailList, SynAction synAction)
        {
            try
            {
                using (DataExtDetails dataExtDetails = new DataExtDetails(credential.GetConnectionString()))
                {
                    dataExtDetails.Items.AddRange(dataextdetailList);
                    dataExtDetails.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Report
        public bool SynPayrollTransactionReport(CredentialToken credential, List<PayrollTransactionReport> payrolltransactionreport, DateTime? lastmodified, SynAction synAction)
        {
            try
            {
                using (PayrollTransactionReports payrolltransactionreports = new PayrollTransactionReports(credential.GetConnectionString(), lastmodified))
                {
                    payrolltransactionreports.Items.AddRange(payrolltransactionreport);
                    payrolltransactionreports.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynBuildAssemblyReport(CredentialToken credential, List<BuildAssemblyReport> buildassemblyreport, DateTime? lastmodified, SynAction synAction)
        {
            try
            {
                using (BuildAssemblyReports buildassemblyreports = new BuildAssemblyReports(credential.GetConnectionString(), lastmodified))
                {
                    buildassemblyreports.Items.AddRange(buildassemblyreport);
                    buildassemblyreports.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynCOGSReport(CredentialToken credential, List<COGSReport> cogsreport, DateTime? lastmodified, SynAction synAction, List<string> transactiontype)
        {
            try
            {
                using (COGSReports cogsreports = new COGSReports(credential.GetConnectionString(), lastmodified, transactiontype))
                {
                    cogsreports.Items.AddRange(cogsreport);
                    cogsreports.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynTransferReport(CredentialToken credential, List<TransferReport> transferreport, DateTime? lastmodified, SynAction synAction)
        {
            try
            {
                using (TransferReports transferreports = new TransferReports(credential.GetConnectionString(),lastmodified))
                {
                    transferreports.Items.AddRange(transferreport);
                    transferreports.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Delete

        public bool SynFullTable(CredentialToken credential, string tablename)
        {
            try
            {
                SqlConnection connection = new SqlConnection(credential.GetConnectionString());
                connection.Open();
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = string.Format("TRUNCATE TABLE [{0}]", tablename);
                    command.ExecuteNonQuery();
                }
                connection.Close();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SynDelete(CredentialToken credential, string tableName, string primaryKey, List<string> primarykeys, SynAction synAction)
        {
            try
            {
                using (DeleteRecord delete = new DeleteRecord(tableName, primaryKey, primarykeys, synAction, credential.GetConnectionString()))
                {
                    return delete.Execute();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Clear those tables which are in sync process
        /// it also clear plugins tables
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public bool SynFull(CredentialToken credential)
        {
            try
            {
                SqlConnection connection = new SqlConnection(credential.GetConnectionString());
                SqlTransaction Transaction = null;
                try
                {
                    connection.Open();
                    Transaction = connection.BeginTransaction();
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Transaction = Transaction;
                        command.Connection = connection;
                        command.CommandType = System.Data.CommandType.Text;

                        string _xml = GetXMLFromDatabase();
                        StringReader reader = new StringReader(_xml);
                        XDocument xdoc = XDocument.Load(reader);
                        var relation = from s in xdoc.Descendants("Master")
                                       select
                                           new MasterTable { TableName = s.Attribute("TableName").Value, PrimaryKey = s.Attribute("PrimaryKey").Value, Type = s.Attribute("Type").Value, DelType = s.Attribute("DelType").Value, Child = (from p in s.Descendants("Child") select new ChildTable { TableName = p.Attribute("TableName").Value, ForeignKey = p.Attribute("ForeignKey").Value }).ToList() };

                        List<string> Tables = (from s in relation select (s.TableName)).ToList();

                        relation.ToList().ForEach(s =>
                        {
                            s.Child.ForEach(p =>
                            {
                                Tables.Add(p.TableName);
                            });
                        });


                        Tables.Remove(TableName.InventoryOversoldReport);

                        foreach (var table in Tables.Distinct())
                        {
                            command.CommandText = string.Format("TRUNCATE TABLE [{0}]", table);
                            command.ExecuteNonQuery();
                        }

                        Transaction.Commit();
                        connection.Close();
                    }

                    //to clear the records of sharepoint custom tables
                    //we have override the exception as these table may not exists in the database
                    //these tables only exist if user purchase the plugin
                    try
                    {
                        SqlConnection newconnection = new SqlConnection(credential.GetConnectionString());
                        newconnection.Open();
                        using (SqlCommand newcommand = new SqlCommand())
                        {
                            newcommand.Connection = newconnection;
                            newcommand.CommandType = System.Data.CommandType.Text;

                            List<string> sharepointable = new List<string>();
                            sharepointable.Add("QBPushtxnitemgrouplinedetail");
                            sharepointable.Add("QBPushtxnitemlinedetail");
                            sharepointable.Add("QBPushItemreceipt");
                            sharepointable.Add("QBPushPurchaseorderlinegroupdetail");
                            sharepointable.Add("QBPushPurchaseorderlinedetail");
                            sharepointable.Add("QBPushPurchaseorder");

                            sharepointable.ForEach(s =>
                            {
                                try
                                {
                                    newcommand.CommandText = string.Format("TRUNCATE TABLE [{0}]", s);
                                    newcommand.ExecuteNonQuery();
                                }
                                catch { }
                            });
                        }
                        newconnection.Close();
                    }
                    catch { }

                    return true;
                }
                catch (Exception ex)
                {
                    if (Transaction != null)
                        Transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    if (connection.State == System.Data.ConnectionState.Open)
                    {
                        connection.Close();
                    }
                    connection.Dispose();
                    connection = null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region ECFO

        public bool SynECFO_SALESTAX_ACCOUNT_LKUP(CredentialToken credential, List<ECFO_SALESTAX_ACCOUNT_LKUP> ecfo, SynAction synAction)
        {
            try
            {
                using (ECFO_SALESTAX_ACCOUNT_LKUPs ecfos = new ECFO_SALESTAX_ACCOUNT_LKUPs(credential.GetConnectionString()))
                {
                    ecfos.Items.AddRange(ecfo);
                    ecfos.SaveAll(synAction);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region GetXMLFromDatabase
        private string GetXMLFromDatabase()
        {
            using (dbCompanyEntities daobj = new dbCompanyEntities())
            {
                XmlDocument xmldoc = new XmlDocument();
                List<Parent> ParentList = new List<Parent>();

                var ParentQuery = daobj.Parents.Where(p => p.IsActive == true).Select(p => p).ToList();
                foreach (var q in ParentQuery)
                {
                    Parent p = new Parent();
                    p.ParentId = q.ParentID;
                    p.ParentTableName = q.ParentTableName;
                    p.PrimaryKey = q.PrimaryKey;
                    p.Type = q.Type;
                    p.DelType = q.DelType;
                    p.IsActive = q.IsActive;
                    ParentList.Add(p);
                }
                XmlDocument doc = new XmlDocument();
                XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
                doc.AppendChild(docNode);

                XmlNode MastersNode = doc.CreateElement("Masters");
                doc.AppendChild(MastersNode);
                foreach (Parent p in ParentList)
                {
                    XmlNode MasterNode = doc.CreateElement("Master");

                    XmlAttribute TableNameAttribute = doc.CreateAttribute("TableName");
                    XmlAttribute PrimaryKeyAttribute = doc.CreateAttribute("PrimaryKey");
                    XmlAttribute TypeAttribute = doc.CreateAttribute("Type");
                    XmlAttribute DelTypeAttribute = doc.CreateAttribute("DelType");

                    TableNameAttribute.Value = p.ParentTableName;
                    PrimaryKeyAttribute.Value = p.PrimaryKey;
                    TypeAttribute.Value = p.Type;
                    DelTypeAttribute.Value = p.DelType;

                    MasterNode.Attributes.Append(TableNameAttribute);
                    MasterNode.Attributes.Append(PrimaryKeyAttribute);
                    MasterNode.Attributes.Append(TypeAttribute);
                    MasterNode.Attributes.Append(DelTypeAttribute);

                    MastersNode.AppendChild(MasterNode);

                    var ChildQuery = daobj.Children.Where(c => c.ParentID == p.ParentId).Select(c => c).ToList();

                    List<Child> ChildList = new List<Child>();
                    foreach (var c in ChildQuery)
                    {
                        Child _child = new Child();
                        _child.ChildTableName = c.ChildTableName;
                        _child.ForeignKey = c.ForeignKey;
                        ChildList.Add(_child);
                    }
                    if (ChildList.Count != 0)
                    {
                        foreach (Child ch in ChildList)
                        {
                            XmlNode childNode = doc.CreateElement("Child");
                            XmlAttribute CTableNameAttribute = doc.CreateAttribute("TableName");
                            XmlAttribute ForeignKeyAttribute = doc.CreateAttribute("ForeignKey");
                            CTableNameAttribute.Value = ch.ChildTableName;
                            ForeignKeyAttribute.Value = ch.ForeignKey;

                            childNode.Attributes.Append(CTableNameAttribute);
                            childNode.Attributes.Append(ForeignKeyAttribute);
                            MasterNode.AppendChild(childNode);
                        }
                    }
                }
                return doc.InnerXml.ToString();
            }
        }
        #endregion

    }
}

