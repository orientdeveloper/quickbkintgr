﻿using QBSynDal;
using QBSynDal.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using QBSynDal.Model;

namespace QBSynServices
{
    [ServiceContract]
    public interface IQBPush
    {
        [OperationContract(Name = "GetQBPushPurchaseOrderTxnID")]
        List<long> QBPushPurchaseOrderTxnID(CredentialToken credential);

        [OperationContract(Name = "GetQBPushPurchaseOrder")]
        QBPushPurchaseorder GetQBPushPurchaseOrder(CredentialToken credential, long TxnID);

        [OperationContract(Name = "UpdateQBPushPurchaseOrderStatus")]
        bool UpdateQBPushPurchaseOrderStatus(CredentialToken credential, long TxnID, string ErrorMessage, PushStatus Status, DateTime PushDate);

        [OperationContract(Name = "GetQBItemReceiptTxnID")]
        List<long> QBPushItemReceiptTxnID(CredentialToken credential);

        [OperationContract(Name = "GetQBItemReceipt")]
        QBPushItemReceipt GetQBPushItemReceipt(CredentialToken credential, long TxnID);

        [OperationContract(Name = "UpdateQBItemReceiptStatus")]
        bool UpdateQBPushItemReceiptStatus(CredentialToken credential, long TxnID, string ErrorMessage, PushStatus Status, DateTime PushDate);

    }
}
