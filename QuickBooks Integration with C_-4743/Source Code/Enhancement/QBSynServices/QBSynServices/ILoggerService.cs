﻿using QBSynDal.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace QBSynServices
{
    [ServiceContract]
    public interface ILoggerService
    {
        [OperationContract]
        void Log(LogInfo info);             
    }
}
