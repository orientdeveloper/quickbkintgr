﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QBBaseDal;
using System.Data;
using System.Data.SqlClient;
using System.Data.Objects;
using System.Data.Entity;

namespace QBBaseBLL
{
    public class CompanyQBTablesBL
    {
        DBQBSyncAppEntities _context;
        /// <summary>
        /// Constructor
        /// </summary>
        public CompanyQBTablesBL()
        {
            _context = new DBQBSyncAppEntities();
        }

        /// <summary>
        /// Get the company's table detail 
        /// </summary>
        /// <param name="CompanyId"></param>
        /// <param name="TableId"></param>
        /// <returns></returns>
        public CompanyQBTable GetCompanyQBTablesDetailsByCompanyIDTableID(Guid CompanyId, Guid TableId)
        {
            CompanyQBTable _companyqbtable = new CompanyQBTable();
            _companyqbtable = _context.CompanyQBTables.Where(x => x.CompanyID == CompanyId && x.TableID == TableId).Select(x => x).FirstOrDefault();
            return _companyqbtable;
        }

        /// <summary>
        /// get the CompanyQBTables detail based on CompanyTableID.
        /// </summary>
        /// <param name="CompanyTableID"></param>
        /// <returns></returns>
        public CompanyQBTable GetCompanyQBTablesDetailsByCompanyTableID(Guid CompanyTableID)
        {
            CompanyQBTable _companyqbtable = new CompanyQBTable();
            _companyqbtable = _context.CompanyQBTables.Where(x => x.CompanyTableID == CompanyTableID).Select(x => x).FirstOrDefault();
            return _companyqbtable;
        }

        /// <summary>
        /// get CompanyQBtable  list for a company.
        /// </summary>
        /// <param name="CompanyId"></param>
        /// <returns></returns>
        public List<CompanyQBTable> GetCompanyQBTableListByCompanyID(Guid CompanyId)
        {
            List<CompanyQBTable> _companyQBtables = new List<CompanyQBTable>();
            _companyQBtables = _context.CompanyQBTables.Where(x => x.CompanyID == CompanyId).Select(x => x).ToList();
            return _companyQBtables;
        }

        /// <summary>
        /// get the companyQBtable list for a tableId
        /// </summary>
        /// <param name="TableId"></param>
        /// <returns></returns>
        public List<CompanyQBTable> GetCompanyQBTableListByTableID(Guid TableId)
        {
            List<CompanyQBTable> _companyQBtables = new List<CompanyQBTable>();
            _companyQBtables = _context.CompanyQBTables.Where(x => x.TableID == TableId).Select(x => x).ToList();
            return _companyQBtables;
        }

        /// <summary>
        /// Get all CompanyQbTable's data
        /// </summary>
        /// <returns></returns>
        public List<CompanyQBTable> GetAllCompanyQBTable()
        {
            List<CompanyQBTable> _companyQBtables = new List<CompanyQBTable>();
            _companyQBtables = _context.CompanyQBTables.Select(x => x).ToList();
            return _companyQBtables;
        }

        
    }
}
