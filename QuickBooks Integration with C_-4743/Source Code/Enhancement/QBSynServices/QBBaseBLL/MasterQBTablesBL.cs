﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QBBaseDal;
using System.Data;
using System.Data.SqlClient;
using System.Data.Objects;
using System.Data.Entity;

namespace QBBaseBLL
{
    public class MasterQBTablesBL
    {
        DBQBSyncAppEntities _context;
        /// <summary>
        /// Constructor
        /// </summary>
        public MasterQBTablesBL()
        {
            _context = new DBQBSyncAppEntities();
        }

        /// <summary>
        /// Get the table name 
        /// </summary>
        /// <param name="TableID"></param>
        /// <returns></returns>
        public string GetQBTablesByTableID(Guid TableID)
        {
            string _tableName = string.Empty;
            _tableName = _context.MasterQBTables.Where(x => x.TableID == TableID).Select(x => x.TableName).FirstOrDefault();
            return _tableName;
        }

        /// <summary>
        /// Get all Quickbook tables.
        /// </summary>
        /// <returns></returns>
        public List<MasterQBTable> GetAllQBTables()
        {
            List<MasterQBTable> _masterqbtables = new List<MasterQBTable>();
            _masterqbtables = _context.MasterQBTables.Select(x => x).ToList();
            return _masterqbtables;
        }
    }
}
