﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QBBaseDal;
using System.Data;
using System.Data.SqlClient;
using System.Data.Objects;
using System.Data.Entity;

namespace QBBaseBLL
{
    public class CompanyBL
    {
        DBQBSyncAppEntities _context;
        /// <summary>
        /// Constructor
        /// </summary>
        public CompanyBL()
        {
            _context = new DBQBSyncAppEntities();
        }

        /// <summary>
        /// Get the companies list based on userid.
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public List<UserCompany> GetCompaniesByUserID(Guid UserID)
        {
            List<UserCompany> companies = new List<UserCompany>();
            companies = (from company in _context.UserCompanies
                         where company.UserID == UserID
                         select company).ToList();
            return companies;
        }
        /// <summary>
        /// returns all company list.
        /// </summary>
        /// <returns></returns>
        public List<UserCompany> GetAllCompanies()
        {
            List<UserCompany> companies = new List<UserCompany>();
            companies = (from company in _context.UserCompanies
                         select company).ToList();
            return companies;
        }

        /// <summary>
        /// Get connection string for a company.
        /// </summary>
        /// <param name="CompanyId"></param>
        /// <returns></returns>
        public string GetConnectionStringByCompanyID(Guid CompanyId)
        {
            string connectionString = string.Empty;
            var conStr = from company in _context.UserCompanies
                         where company.CompanyID == CompanyId
                         select company;
            if (conStr != null)
                connectionString = conStr.ToString();
            else
                connectionString = string.Empty;
            return connectionString;
        }

        /// <summary>
        /// returns number of companies for a user.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public int GetCompanyCountByUserID(Guid UserId)
        {
            int NumberOfCompanies = 0;
            NumberOfCompanies = _context.UserCompanies.Count(x => x.UserID == UserId);
            return NumberOfCompanies;
        }

        /// <summary>
        /// Get company file path location for a company
        /// </summary>
        /// <param name="CompanyId"></param>
        /// <returns></returns>
        public string GetCompanyFilePathByCompanyID(Guid CompanyId)
        {
            string _CompanyFilePath = string.Empty;
            _CompanyFilePath = _context.UserCompanies.Where(x => x.CompanyID == CompanyId).Select(x=>x.CompanyFilePath).FirstOrDefault();
            return _CompanyFilePath;
        }

        public string GetCompanyNameByCompanyId(Guid CompanyId)
        {
            string companyName = string.Empty;
            companyName = _context.UserCompanies.Where(x => x.CompanyID == CompanyId).Select(x => x.CompanyName).FirstOrDefault();
            return companyName;
        }
    }
}
