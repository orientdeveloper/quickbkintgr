﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QBBaseDal;
using System.Data;
using System.Data.SqlClient;
using System.Data.Objects;

namespace QBBaseBLL
{
    public class UsersBL
    {
        DBQBSyncAppEntities _context;
        /// <summary>
        /// Constructor
        /// </summary>
        public UsersBL()
        {
            _context = new DBQBSyncAppEntities();
        }

        /// <summary>
        /// Method for validate the license key.
        /// </summary>
        /// <param name="LicenseKey"></param>
        /// <returns></returns>
        public bool IsValidLicenseKey(string LicenseKey)
        {

            User u = new User();
            u = _context.Users.Where(us => us.LicenseKey == LicenseKey).FirstOrDefault();

            if (u != null)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Get the user detail based on license key.
        /// </summary>
        /// <param name="LicenseKey"></param>
        /// <returns></returns>
        public User GetUserFromLicense(string LicenseKey)
        {
            User user = new User();
            user = _context.Users.Where(us => us.LicenseKey == LicenseKey).FirstOrDefault();
            return user;
        }


    }
}
