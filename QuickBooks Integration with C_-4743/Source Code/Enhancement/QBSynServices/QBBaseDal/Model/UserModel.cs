﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QBBaseDal.Model
{
    public class UserModel
    {
        public System.Guid UserID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string LicenseKey { get; set; }
        public int CompanyCount { get; set; }
        public virtual ICollection<UserCompanyModel> UserCompanies { get; set; }
    }
}