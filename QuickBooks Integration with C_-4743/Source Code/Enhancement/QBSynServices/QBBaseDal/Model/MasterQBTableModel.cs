﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QBBaseDal.Model
{
    public class MasterQBTableModel
    {
        public System.Guid TableID { get; set; }
        public string TableName { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public bool IsActive { get; set; }

        public virtual ICollection<CompanyQBTableModel> CompanyQBTables { get; set; }
    }
}
