﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QBBaseDal.Model
{
    public class UserCompanyModel
    {
        public System.Guid CompanyID { get; set; }
        public string CompanyName { get; set; }
        public System.Guid UserID { get; set; }
        public string CompanyFilePath { get; set; }
        public string ConnectionString { get; set; }

        public virtual ICollection<CompanyQBTableModel> CompanyQBTables { get; set; }
        public virtual UserModel User { get; set; }
    }
}
