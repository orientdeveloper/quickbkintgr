﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QBBaseDal.Model
{
    public class CompanyQBTableModel
    {
        public Nullable<System.Guid> CompanyTableID { get; set; }
        public System.Guid CompanyID { get; set; }
        public System.Guid TableID { get; set; }
        public Nullable<System.DateTime> LastSyncDate { get; set; }
        public Nullable<System.DateTime> LastRestoreDate { get; set; }
        public Nullable<System.DateTime> LastDeletedSyncDate { get; set; }
        public bool IsActive { get; set; }

        public virtual MasterQBTableModel MasterQBTable { get; set; }
        public virtual UserCompanyModel UserCompany { get; set; }

        public string CompanyName { get; set; }
        public string tableName { get; set; }

    }
}
