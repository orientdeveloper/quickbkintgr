﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace EssentialCFO.SSO.Models
{
    /// <summary>
    /// Enum for LoginResponseReason
    /// </summary>
    [XmlRoot("LoginErrorReason")]
    public enum LoginResponseReason
    {
        /// <summary>
        /// When api account is locked
        /// </summary>
        AccountLocked = 1,
        /// <summary>
        /// When login is valid
        /// </summary>
        ValidLogin = 2,
        /// <summary>
        /// UserId Not Found.
        /// </summary>
        MemberIdNotFound = 3,
        /// <summary>
        /// Parameter is required for request.
        /// </summary>
        EachParameterRequired = 4,
        /// <summary>
        /// Token is invalid
        /// </summary>
        TokenIsInvalid = 5,
        /// <summary>
        /// Internal Server Exception
        /// </summary>
        InternalServerException = 6,
        /// <summary>
        /// Login Details Provided Are InValid 
        /// </summary>
        InValidLoginDetails = 7
    }

    /// <summary>
    /// Login result data contract class
    /// </summary>
    [XmlRoot("LoginResult")]
    public class LoginResult
    {
        /// <summary>
        /// Authenticated status true/false
        /// </summary>
        public bool Authenticated { get; set; }

        /// <summary>
        /// UserId for login user
        /// </summary>
        public string UserID { get; set; }

        /// <summary>
        /// HomePhone
        /// </summary>
        public string HomePhone { get; set; }

        /// <summary>
        /// Mobile
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// OrganizationDescription
        /// </summary>
        public string OrganizationDescription { get; set; }

        /// <summary>
        /// OrganizationName
        /// </summary>
        public string OrganizationName { get; set; }

        /// <summary>
        /// Title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Town
        /// </summary>
        public string Town { get; set; }

        /// <summary>
        /// PostalCode
        /// </summary>
        public string PostalCode { get; set; }

        /// <summary>
        /// City
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// State
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Country
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// LastName
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// LoginResponse
        /// </summary>
        public string LoginResponse { get; set; }

        /// <summary>
        /// LoginResponseReason
        /// </summary>
        public LoginResponseReason LoginResponseReason { get; set; }

        /// <summary>
        /// LoginResult constructor
        /// </summary>
        public LoginResult() { }
    }

    /// <summary>
    /// Login request data contract class
    /// </summary>
    [XmlRoot("LoginRequest")]
    public class LoginRequest
    {
        /// <summary>
        /// Login user name
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Login password
        /// </summary>
        public string Password { get; set; }
    }
}