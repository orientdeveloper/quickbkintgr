﻿using EssentialCFO.SSO.Helper;
using EssentialCFO.SSO.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace SSOTest.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Default/
        [Authorization]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ssologin()
        {
            return Redirect("https://www.essentialcfo.com/_layouts/15/ecfoapi/api.aspx?appid=" + ConfigurationHelper.AppID);
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string Username, string Password)
        {
            LoginResult loginresult = ThirdPartyUserAuth(Username, Password, ConfigurationHelper.AppID);
            if (loginresult != null)
            {
                if (loginresult.Authenticated)
                    Session["UserId"] = 1;
                else
                {
                    switch (loginresult.LoginResponseReason)
                    {
                        case LoginResponseReason.ValidLogin:
                            break;
                        case LoginResponseReason.MemberIdNotFound:
                            break;
                        case LoginResponseReason.EachParameterRequired:
                            break;
                        case LoginResponseReason.TokenIsInvalid:
                            break;
                        case LoginResponseReason.InternalServerException:
                            break;
                        case LoginResponseReason.InValidLoginDetails:
                            break;
                        default:
                            break;
                    }
                }
            }
            return View("List", loginresult);
        }

        public ActionResult Authenticate(string userid, string token, string sig)
        {
            LoginResult loginresult = ThirdPartyAuth(userid, token, sig);
            if (loginresult != null)
            {
                if (loginresult.Authenticated)
                    Session["UserId"] = 1;
                else
                {
                    switch (loginresult.LoginResponseReason)
                    {
                        case LoginResponseReason.ValidLogin:
                            break;
                        case LoginResponseReason.MemberIdNotFound:
                            break;
                        case LoginResponseReason.EachParameterRequired:
                            break;
                        case LoginResponseReason.TokenIsInvalid:
                            break;
                        case LoginResponseReason.InternalServerException:
                            break;
                        case LoginResponseReason.InValidLoginDetails:
                            break;
                        default:
                            break;
                    }
                }
            }
            return View("List", loginresult);
        }

        public LoginResult ThirdPartyUserAuth(string userName, string Password, string appid)
        {
            LoginResult loginresult = null;
            try
            {
                //JSON string representation of the login request object
                string jSon = "{\"UserName\":\"" + userName + "\",\"Password\":\"" + Password + "\"}";
                //URI of the service and expected query string variables.
                string uri = string.Format(ConfigurationHelper.ServiceBaseUrl + "api/LoginSrv?appid={0}", ConfigurationHelper.AppID);

                //Key provided by EO
                byte[] key = Convert.FromBase64String(ConfigurationHelper.AppKey);

                //Create the request pointed to the login service
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);

                //Encode the uri to use as our authentication header
                string encodedUri = EncodeText(key, uri, UTF8Encoding.UTF8);

                //Add encoded text to authentication header
                request.Headers[HttpRequestHeader.Authorization] = encodedUri;

                //Convert JSON object to bytes
                byte[] data = Encoding.UTF8.GetBytes(jSon);

                //Specify request to post the data to the service
                request.Method = "POST";
                //Specify to service that the content will be JSON (could be xml)
                request.ContentType = "application/json";
                //Write bytes to the request stream
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(data, 0, data.Length);
                dataStream.Close();

                //Send request and get response
                WebResponse response = request.GetResponse();

                //Read result to string and deserialize JSON string into login result object.
                string jsonResults = new StreamReader(response.GetResponseStream()).ReadToEnd();
                JavaScriptSerializer ser = new JavaScriptSerializer();
                LoginResult authResult = ser.Deserialize<LoginResult>(jsonResults);
                return authResult;
            }
            catch (Exception ex)
            {
                return loginresult;
            }
        }

        public LoginResult ThirdPartyAuth(string userName, string token, string sig)
        {
            LoginResult loginresult = null;
            try
            {
                byte[] key = Convert.FromBase64String(ConfigurationHelper.AppKey);

                //username is provided in case additional processing on other end is necessary
                //apiSig variable gets passed back to api as is           
                //URL Decode Values Before Checking Signature
                userName = System.Uri.UnescapeDataString(userName);
                token = System.Uri.UnescapeDataString(token);

                //To Check Signature Concat username and token, create hash and compare
                string checkSig = EncodeText(key, userName + token, Encoding.UTF8);

                if (!checkSig.Equals(sig))
                {
                    //Not A Valid Request
                    return loginresult;
                }

                //form url to send to eo api
                string uri = string.Format(ConfigurationHelper.ServiceBaseUrl + "api/LoginSrv?appid={0}&token={1}", ConfigurationHelper.AppID, token);
                HttpWebRequest request = WebRequest.Create(uri) as HttpWebRequest;

                if (ConfigurationHelper.ProxyEnable)
                {
                    request.Proxy.Credentials = new NetworkCredential(ConfigurationHelper.ProxyUserName, ConfigurationHelper.ProxyPassword);
                }

                //encrypt uri value with your key and add as authorization header
                string encodedUri = EncodeText(key, uri, UTF8Encoding.UTF8);

                //add authorization header and set to encoded uri
                request.Headers[HttpRequestHeader.Authorization] = encodedUri;

                //select format of return, either "application/xml" or "application/json"
                request.Accept = "application/json";

                //make request to api server
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                //get response as stream
                Stream apiStream = response.GetResponseStream();
                StreamReader apiSR = new StreamReader(apiStream);

                //Getting results as string
                string results = apiSR.ReadToEnd();
                JavaScriptSerializer ser = new JavaScriptSerializer();
                loginresult = ser.Deserialize<LoginResult>(results);
                apiSR.Close();
                apiStream.Close();
                return loginresult;
            }
            catch (Exception ex)
            {
                return loginresult;
            }
        }

        private string EncodeText(byte[] key, string text, Encoding encoding)
        {
            HMACMD5 hmacMD5 = new HMACMD5(key);
            byte[] textBytes = encoding.GetBytes(text);
            byte[] encodedTextBytes =
                hmacMD5.ComputeHash(textBytes);
            string encodedText =
                Convert.ToBase64String(encodedTextBytes);
            return encodedText;
        }
    }
}