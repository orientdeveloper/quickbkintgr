﻿///
/// Name : ConfigurationHelper 
/// User : 1296 
/// Description : Class for geting the values of appsetting from webconfig.
/// date : 05/02/2014
///

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace EssentialCFO.SSO.Helper
{
    /// <summary>
    /// Class for geting the values of appsetting from webconfig.
    /// </summary>
    public class ConfigurationHelper
    {
        /// <summary>
        /// Property for getting the value of BaseUrl from webconfig appsetting.
        /// </summary>
        public static string AppID
        {
            get
            {
                return ConfigurationManager.AppSettings["AppID"];
            }
        }

        /// <summary>
        /// Property for getting the value of EOBaseURL from webconfig appsetting.
        /// </summary>
        public static string AppKey
        {
            get
            {
                return ConfigurationManager.AppSettings["AppKey"];
            }
        }

        /// <summary>
        /// Property for getting the value of EOClientId from webconfig appsetting.
        /// </summary>
        public static string ServiceUserName
        {
            get
            {
                return ConfigurationManager.AppSettings["ServiceUserName"];
            }
        }

        /// <summary>
        /// Property for getting the value of EOKey from webconfig appsetting.
        /// </summary>
        public static string ServicePassword
        {
            get
            {
                return ConfigurationManager.AppSettings["ServicePassword"];
            }
        }

        /// <summary>
        /// Property for getting the value of ProxyEnable from webconfig appsetting.
        /// </summary>
        public static bool ProxyEnable
        {
            get
            {
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ProxyEnable"]) && ConfigurationManager.AppSettings["ProxyEnable"] == "true")
                    return true;
                else
                    return false;
            }
        }

        /// <summary>
        /// Property for getting the value of ProxyUserName from webconfig appsetting.
        /// </summary>
        public static string ProxyUserName
        {
            get
            {
                return ConfigurationManager.AppSettings["ProxyUserName"];
            }
        }

        /// <summary>
        /// Property for getting the value of ProxyPassword from webconfig appsetting.
        /// </summary>
        public static string ProxyPassword
        {
            get
            {
                return ConfigurationManager.AppSettings["ProxyPassword"];
            }
        }

        /// <summary>
        /// Property for getting the value of AttachmentFolder from webconfig appsetting.
        /// </summary>
        public static long TokenValidHours
        {
            get
            {
                return Convert.ToInt64(ConfigurationManager.AppSettings["TokenValidHours"].ToString());
            }
        }

        /// <summary>
        /// Property for getting the value of LoginUrl from webconfig appsetting.
        /// </summary>
        public static string LoginUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["LoginUrl"];
            }
        }

        /// <summary>
        /// Property for getting the value of LogoutUrl from webconfig appsetting.
        /// </summary>
        public static string LogoutUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["LogoutUrl"];
            }
        }

        /// <summary>
        /// Property for getting the value of ModeratoeEmail from webconfig appsetting.
        /// </summary>
        public static string ServiceBaseUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["ServiceBaseUrl"];
            }
        }
    }
}