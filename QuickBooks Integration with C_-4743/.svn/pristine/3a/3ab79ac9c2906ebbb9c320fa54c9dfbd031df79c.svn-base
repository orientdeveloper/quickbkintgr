//
// Class	:	InvoicelinegroupdetailDal.cs
// Author	:  	Synapse India © 2013
// Date		:	6/20/2013 10:11:50 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;
namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class InvoicelinegroupdetailDal : IDisposable
    {
        #region Class Level Variables
        bool _fullsyn;
        string _mastertable;
        string _idkey;
        invoicelinegroupdetailTableAdapter _adapter;
        #endregion

        #region Constructors / Destructors
        public InvoicelinegroupdetailDal(bool fullsyn, string conString, string mastertable, string idkey)
        {
            _adapter = new invoicelinegroupdetailTableAdapter();
            _adapter.Connection.ConnectionString = conString;
            _fullsyn = fullsyn;
            _idkey = idkey;
            _mastertable = mastertable;
        }
        #endregion

        #region Methods (Public)
        public void Save(List<Invoicelinegroupdetail> Invoicelinegroupdetails)
        {
            try
            {
                QBdb.invoicelinegroupdetailDataTable table = new Dal.QBdb.invoicelinegroupdetailDataTable();

                if (!_fullsyn)
                {
                    _adapter.DeleteBy(_idkey, _mastertable);
                }

                foreach (Invoicelinegroupdetail instance in Invoicelinegroupdetails)
                {                    
                    DataRow dr = table.NewRow();
                    table.Rows.Add(dr);
                    Bind(dr, instance);
                }
                _adapter.Update(table);
            }
            catch 
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Invoicelinegroupdetail invoicelinegroupdetail)
        {
            try
            {
                dr[InvoicelinegroupdetailFields.TxnLineID] = invoicelinegroupdetail.TxnLineID;
                dr[InvoicelinegroupdetailFields.ItemGroupRef_ListID] = invoicelinegroupdetail.ItemGroupRef_ListID;
                dr[InvoicelinegroupdetailFields.ItemGroupRef_FullName] = invoicelinegroupdetail.ItemGroupRef_FullName;
                dr[InvoicelinegroupdetailFields.Desc] = invoicelinegroupdetail.Desc;
                dr[InvoicelinegroupdetailFields.Quantity] = invoicelinegroupdetail.Quantity;
                dr[InvoicelinegroupdetailFields.IsPrintItemsInGroup] = invoicelinegroupdetail.IsPrintItemsInGroup.GetShort().GetDbType();
                dr[InvoicelinegroupdetailFields.TotalAmount] = invoicelinegroupdetail.TotalAmount.GetDbType();
                dr[InvoicelinegroupdetailFields.ServiceDate] = invoicelinegroupdetail.ServiceDate.GetDbType();
                dr[InvoicelinegroupdetailFields.CustomField1] = invoicelinegroupdetail.CustomField1;
                dr[InvoicelinegroupdetailFields.CustomField2] = invoicelinegroupdetail.CustomField2;
                dr[InvoicelinegroupdetailFields.CustomField3] = invoicelinegroupdetail.CustomField3;
                dr[InvoicelinegroupdetailFields.CustomField4] = invoicelinegroupdetail.CustomField4;
                dr[InvoicelinegroupdetailFields.CustomField5] = invoicelinegroupdetail.CustomField5;
                dr[InvoicelinegroupdetailFields.CustomField6] = invoicelinegroupdetail.CustomField6;
                dr[InvoicelinegroupdetailFields.CustomField7] = invoicelinegroupdetail.CustomField7;
                dr[InvoicelinegroupdetailFields.CustomField8] = invoicelinegroupdetail.CustomField8;
                dr[InvoicelinegroupdetailFields.CustomField9] = invoicelinegroupdetail.CustomField9;
                dr[InvoicelinegroupdetailFields.CustomField10] = invoicelinegroupdetail.CustomField10;
                dr[InvoicelinegroupdetailFields.Idkey] = invoicelinegroupdetail.Idkey;
                dr[InvoicelinegroupdetailFields.GroupIDKEY] = invoicelinegroupdetail.GroupIDKEY;
                dr[InvoicelinegroupdetailFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[InvoicelinegroupdetailFields.SynDate] = DBNull.Value;
                dr[InvoicelinegroupdetailFields.TableName] = invoicelinegroupdetail.TableName;
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adapter.Dispose();
                    _adapter = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
