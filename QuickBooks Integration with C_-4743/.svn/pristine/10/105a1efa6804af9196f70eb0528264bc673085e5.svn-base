//
// Class	:	Itemfixedasset.cs
// Author	:  	SynapseIndia © 2013
// Date		:	9/4/2013 10:11:50 AM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization;

namespace QBSynDal
{
    /// <summary>
    /// Class for the properties of the object
    /// </summary>
    public class ItemfixedassetFields
    {
        public const string ListID = "ListID";
        public const string TimeCreated = "TimeCreated";
        public const string TimeModified = "TimeModified";
        public const string EditSequence = "EditSequence";
        public const string Name = "Name";
        public const string IsActive = "IsActive";
        public const string AcquiredAs = "AcquiredAs";
        public const string PurchaseDesc = "PurchaseDesc";
        public const string PurchaseDate = "PurchaseDate";
        public const string PurchaseCost = "PurchaseCost";
        public const string VendorOrPayeeName = "VendorOrPayeeName";
        public const string AssetAccountRef_ListID = "AssetAccountRef_ListID";
        public const string AssetAccountRef_FullName = "AssetAccountRef_FullName";
        public const string AssetDesc = "AssetDesc";
        public const string Location = "Location";
        public const string PONumber = "PONumber";
        public const string SerialNumber = "SerialNumber";
        public const string WarrantyExpDate = "WarrantyExpDate";
        public const string Notes = "Notes";
        public const string AssetNumber = "AssetNumber";
        public const string CostBasis = "CostBasis";
        public const string YearEndAccumulatedDepreciation = "YearEndAccumulatedDepreciation";
        public const string YearEndBookValue = "YearEndBookValue";
        public const string CustomField1 = "CustomField1";
        public const string CustomField2 = "CustomField2";
        public const string CustomField3 = "CustomField3";
        public const string CustomField4 = "CustomField4";
        public const string CustomField5 = "CustomField5";
        public const string CustomField6 = "CustomField6";
        public const string CustomField7 = "CustomField7";
        public const string CustomField8 = "CustomField8";
        public const string CustomField9 = "CustomField9";
        public const string CustomField10 = "CustomField10";
        public const string Status = "Status";
    }
    /// <summary>
    /// Datacontract class for the "itemfixedasset" table.
    /// </summary>
    [DataContract(Name = "ServiceItemfixedasset")]
    public class Itemfixedasset : IModel
    {
        #region Class Level Variables

        private string _listID = null;
        private string _timeCreated = null;
        private string _timeModified = null;
        private string _editSequence = null;
        private string _name = null;
        private bool? _isActive = null;
        private string _acquiredAs = null;
        private string _purchaseDesc = null;
        private DateTime? _purchaseDate = null;
        private string _purchaseCost = null;
        private string _vendorOrPayeeName = null;
        private string _assetAccountRef_ListID = null;
        private string _assetAccountRef_FullName = null;
        private string _assetDesc = null;
        private string _location = null;
        private string _pONumber = null;
        private string _serialNumber = null;
        private DateTime? _warrantyExpDate = null;
        private string _notes = null;
        private string _assetNumber = null;
        private double? _costBasis = null;
        private double? _yearEndAccumulatedDepreciation = null;
        private double? _yearEndBookValue = null;
        private string _customField1 = null;
        private string _customField2 = null;
        private string _customField3 = null;
        private string _customField4 = null;
        private string _customField5 = null;
        private string _customField6 = null;
        private string _customField7 = null;
        private string _customField8 = null;
        private string _customField9 = null;
        private string _customField10 = null;
        private string _status = null;

        #endregion

        #region Properties

        /// <summary>
        /// This property is mapped to the "ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string ListID
        {
            get
            {
                return _listID;
            }
            set
            {
                _listID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeCreated" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string TimeCreated
        {
            get
            {
                return _timeCreated;
            }
            set
            {
                _timeCreated = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "TimeModified" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string TimeModified
        {
            get
            {
                return _timeModified;
            }
            set
            {

                _timeModified = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "EditSequence" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string EditSequence
        {
            get
            {
                return _editSequence;
            }
            set
            {
                _editSequence = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Name" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "IsActive" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public bool? IsActive
        {
            get
            {
                return _isActive;
            }
            set
            {
                _isActive = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AcquiredAs" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string AcquiredAs
        {
            get
            {
                return _acquiredAs;
            }
            set
            {
                _acquiredAs = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "PurchaseDesc" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string PurchaseDesc
        {
            get
            {
                return _purchaseDesc;
            }
            set
            {
                _purchaseDesc = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "PurchaseDate" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public DateTime? PurchaseDate
        {
            get
            {
                return _purchaseDate;
            }
            set
            {
                _purchaseDate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "PurchaseCost" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string PurchaseCost
        {
            get
            {
                return _purchaseCost;
            }
            set
            {
                _purchaseCost = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "VendorOrPayeeName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string VendorOrPayeeName
        {
            get
            {
                return _vendorOrPayeeName;
            }
            set
            {
                _vendorOrPayeeName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AssetAccountRef_ListID" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string AssetAccountRef_ListID
        {
            get
            {
                return _assetAccountRef_ListID;
            }
            set
            {
                _assetAccountRef_ListID = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AssetAccountRef_FullName" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string AssetAccountRef_FullName
        {
            get
            {
                return _assetAccountRef_FullName;
            }
            set
            {
                _assetAccountRef_FullName = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AssetDesc" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string AssetDesc
        {
            get
            {
                return _assetDesc;
            }
            set
            {
                _assetDesc = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Location" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string Location
        {
            get
            {
                return _location;
            }
            set
            {
                _location = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "PONumber" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string PONumber
        {
            get
            {
                return _pONumber;
            }
            set
            {
                _pONumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "SerialNumber" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string SerialNumber
        {
            get
            {
                return _serialNumber;
            }
            set
            {
                _serialNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "WarrantyExpDate" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public DateTime? WarrantyExpDate
        {
            get
            {
                return _warrantyExpDate;
            }
            set
            {
                _warrantyExpDate = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Notes" field. Length must be between 0 and 2000 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string Notes
        {
            get
            {
                return _notes;
            }
            set
            {
                _notes = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "AssetNumber" field. Length must be between 0 and 255 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string AssetNumber
        {
            get
            {
                return _assetNumber;
            }
            set
            {
                _assetNumber = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CostBasis" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public double? CostBasis
        {
            get
            {
                return _costBasis;
            }
            set
            {
                _costBasis = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "YearEndAccumulatedDepreciation" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public double? YearEndAccumulatedDepreciation
        {
            get
            {
                return _yearEndAccumulatedDepreciation;
            }
            set
            {
                _yearEndAccumulatedDepreciation = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "YearEndBookValue" field.  
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public double? YearEndBookValue
        {
            get
            {
                return _yearEndBookValue;
            }
            set
            {
                _yearEndBookValue = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField1" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string CustomField1
        {
            get
            {
                return _customField1;
            }
            set
            {
                _customField1 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField2" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string CustomField2
        {
            get
            {
                return _customField2;
            }
            set
            {
                _customField2 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField3" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string CustomField3
        {
            get
            {
                return _customField3;
            }
            set
            {
                _customField3 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField4" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string CustomField4
        {
            get
            {
                return _customField4;
            }
            set
            {
                _customField4 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField5" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string CustomField5
        {
            get
            {
                return _customField5;
            }
            set
            {
                _customField5 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField6" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string CustomField6
        {
            get
            {
                return _customField6;
            }
            set
            {
                _customField6 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField7" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string CustomField7
        {
            get
            {
                return _customField7;
            }
            set
            {
                _customField7 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField8" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string CustomField8
        {
            get
            {
                return _customField8;
            }
            set
            {
                _customField8 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField9" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string CustomField9
        {
            get
            {
                return _customField9;
            }
            set
            {
                _customField9 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "CustomField10" field. Length must be between 0 and 50 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string CustomField10
        {
            get
            {
                return _customField10;
            }
            set
            {
                _customField10 = value;
            }
        }

        /// <summary>
        /// This property is mapped to the "Status" field. Length must be between 0 and 10 characters. 
        /// Comment found in the Database for this field: .
        /// </summary>
        /// 
        [DataMember]
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        #endregion
    }
}
