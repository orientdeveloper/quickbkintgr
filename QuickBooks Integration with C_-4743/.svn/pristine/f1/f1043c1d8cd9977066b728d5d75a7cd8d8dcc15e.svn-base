//
// Class	:	JournaldebitlinedetailDal.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/6/2013 10:11:52 AM
//

using QBDataFetcher.Dal;
using QBDataFetcher.Dal.QBdbTableAdapters;
using QBDataFetcher.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using QBDataFetcher.Helpers;
using System.Data.Linq;
using System.Linq;
using System.Data;
using QBDataFetcher.Fetcher;

namespace QBDataFetcher.DAL
{
    /// <summary>
    /// Class for the calling fetcher class and getting data and storing in the access database
    /// </summary>
    public class JournaldebitlinedetailDal : IDisposable
    {
        #region Class Level Variables
        bool _fullsyn;
        string _mastertable;
        string _idkey;
        journaldebitlinedetailTableAdapter _adapter;
        List<string> _deletedIds;
        #endregion

        #region Constructors / Destructors
        public JournaldebitlinedetailDal(bool fullsyn, string conString, string mastertable, string idkey)
        {
            _adapter = new journaldebitlinedetailTableAdapter();
            _adapter.Connection.ConnectionString = conString;
            _fullsyn = fullsyn;
            _fullsyn = fullsyn;
            _idkey = idkey;
            _mastertable = mastertable;
        }
        #endregion

        #region Methods (Public)
        public void Save(List<Journaldebitlinedetail> Journaldebitlinedetails)
        {
            try
            {
                QBdb.journaldebitlinedetailDataTable table = new Dal.QBdb.journaldebitlinedetailDataTable();
                if (!_fullsyn)
                {
                    _adapter.DeleteBy(_idkey, _mastertable);
                }

                foreach (Journaldebitlinedetail instance in Journaldebitlinedetails)
                {                    
                    DataRow dr = table.NewRow();
                    table.Rows.Add(dr);
                    Bind(dr, instance);
                }
                _adapter.Update(table);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Method (Private)
        /// <summary>
        /// This method binds the list data in datarow object and also trasform the data according to the access database
        /// </summary>
        /// <param name="dr">Datarow object</param>
        /// <param name="appliedtotxndetail">list object</param>
        private void Bind(DataRow dr, Journaldebitlinedetail journaldebitlinedetail)
        {
            try
            {
                dr[JournaldebitlinedetailFields.TxnLineID] = journaldebitlinedetail.TxnLineID;
                dr[JournaldebitlinedetailFields.AccountRef_ListID] = journaldebitlinedetail.AccountRef_ListID;
                dr[JournaldebitlinedetailFields.AccountRef_FullName] = journaldebitlinedetail.AccountRef_FullName;
                dr[JournaldebitlinedetailFields.Amount] = journaldebitlinedetail.Amount.GetDbType();
                dr[JournaldebitlinedetailFields.Memo] = journaldebitlinedetail.Memo;
                dr[JournaldebitlinedetailFields.EntityRef_ListID] = journaldebitlinedetail.EntityRef_ListID;
                dr[JournaldebitlinedetailFields.EntityRef_FullName] = journaldebitlinedetail.EntityRef_FullName;
                dr[JournaldebitlinedetailFields.ClassRef_ListID] = journaldebitlinedetail.ClassRef_ListID;
                dr[JournaldebitlinedetailFields.ClassRef_FullName] = journaldebitlinedetail.ClassRef_FullName;
                dr[JournaldebitlinedetailFields.ItemSalesTaxRef_ListID] = journaldebitlinedetail.ItemSalesTaxRef_ListID;
                dr[JournaldebitlinedetailFields.ItemSalesTaxRef_FullName] = journaldebitlinedetail.ItemSalesTaxRef_FullName;
                dr[JournaldebitlinedetailFields.BillableStatus] = journaldebitlinedetail.BillableStatus;
                dr[JournaldebitlinedetailFields.CustomField1] = journaldebitlinedetail.CustomField1;
                dr[JournaldebitlinedetailFields.CustomField2] = journaldebitlinedetail.CustomField2;
                dr[JournaldebitlinedetailFields.CustomField3] = journaldebitlinedetail.CustomField3;
                dr[JournaldebitlinedetailFields.CustomField4] = journaldebitlinedetail.CustomField4;
                dr[JournaldebitlinedetailFields.CustomField5] = journaldebitlinedetail.CustomField5;
                dr[JournaldebitlinedetailFields.CustomField6] = journaldebitlinedetail.CustomField6;
                dr[JournaldebitlinedetailFields.CustomField7] = journaldebitlinedetail.CustomField7;
                dr[JournaldebitlinedetailFields.CustomField8] = journaldebitlinedetail.CustomField8;
                dr[JournaldebitlinedetailFields.CustomField9] = journaldebitlinedetail.CustomField9;
                dr[JournaldebitlinedetailFields.CustomField10] = journaldebitlinedetail.CustomField10;
                dr[JournaldebitlinedetailFields.Idkey] = journaldebitlinedetail.Idkey;
                dr[JournaldebitlinedetailFields.GroupIDKEY] = journaldebitlinedetail.GroupIDKEY;
                dr[JournaldebitlinedetailFields.TableName] = journaldebitlinedetail.TableName;
                dr[JournaldebitlinedetailFields.IsSyn] = (short)SynStatus.Synchronize;
                dr[JournaldebitlinedetailFields.SynDate] = DBNull.Value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Implementation IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _adapter.Dispose();
                    _adapter = null;
                    _deletedIds = null;
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
