//
// Class	:	COGSReport.cs
// Author	:  	SynapseIndia © 2013
// Date		:	4/2/2013 10:11:42 AM
//

using QBSynDal.Helper;
using QBSynDal.List.QBSynDalTableAdapters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using QBSynDal.List;
using QBSynDal.Model;

namespace QBSynDal
{
    public class TransferReports : AbstractDalBase
    {
        #region Variable
        DateTime? _lastmodifieddate;
        #endregion

        #region Constructors / Destructors
        public TransferReports(string constr, DateTime? lastmodifieddate)
            : base(constr)
        {
            _lastmodifieddate = lastmodifieddate;
        }
        #endregion

        #region Methods (Public)
        public override void SaveAll(SynAction synAction)
        {
            try
            {
                BeginTransaction();
                transferReportTableAdapter _transferReportTableAdapter = new transferReportTableAdapter();
                _transferReportTableAdapter.Connection = Connection;
                _transferReportTableAdapter.Transaction = Transaction;


                if (synAction == SynAction.InitiateDelete || synAction == SynAction.DoneDelete)
                {
                    if (_lastmodifieddate.HasValue)
                    {
                        _transferReportTableAdapter.DeleteByDate(_lastmodifieddate.Value.Date.Year, _lastmodifieddate.Value.Date.Month, _lastmodifieddate.Value.Date.Day);
                    }
                    else
                    {
                        _transferReportTableAdapter.DeleteAll();
                    }
                }

                QBSynDal.List.QBSynDal.transferReportDataTable _transferReportDataTable = new List.QBSynDal.transferReportDataTable();
                foreach (TransferReport _transferReport in this.Items)
                {
                    DataRow dr = _transferReportDataTable.NewRow();
                    _transferReportDataTable.Rows.Add(dr);
                    Bind(dr, _transferReport);
                }
                _transferReportTableAdapter.Update(_transferReportDataTable);
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw ex;
            }
        }
        #endregion

        #region Method (Private)
        private void Bind(DataRow dr, TransferReport _transferReport)
        {
            try
            {
                dr[TransferReportFields.Date] = _transferReport.Date.GetDbType();
                dr[TransferReportFields.TxnNumber] = _transferReport.TxnNumber.GetDbType();
                dr[TransferReportFields.TxnType] = _transferReport.TxnType;
                dr[TransferReportFields.RefNumber] = _transferReport.RefNumber;
                dr[TransferReportFields.Customer_ListID] = _transferReport.Customer_ListID;
                dr[TransferReportFields.Customer_Name] = _transferReport.Customer_Name;
                dr[TransferReportFields.SourceName] = _transferReport.SourceName;
                dr[TransferReportFields.Account_ListID] = _transferReport.Account_ListID;
                dr[TransferReportFields.Account] = _transferReport.Account;
                dr[TransferReportFields.Class_ListID] = _transferReport.Class_ListID;
                dr[TransferReportFields.Class] = _transferReport.Class;
                dr[TransferReportFields.ClearedStatus] = _transferReport.ClearedStatus;
                dr[TransferReportFields.SplitAccount_ListID] = _transferReport.SplitAccount_ListId;
                dr[TransferReportFields.SplitAccount] = _transferReport.SplitAccount;
                dr[TransferReportFields.Quantity] = _transferReport.Quantity.GetDbType();
                dr[TransferReportFields.Debit] = _transferReport.Debit.GetDbType();
                dr[TransferReportFields.Credit] = _transferReport.Credit.GetDbType();
                dr[TransferReportFields.ModifiedTime] = _transferReport.ModifiedTime.GetDbType();
                dr[TransferReportFields.AccountType] = _transferReport.AccountType;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
